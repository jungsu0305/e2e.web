package com.kt.e2e.Common;

public class Constant {

	public static enum ComponentType {
		NODE("Node"), 
		CLOUD("Cloud"), 
		CONNECTION("Connection"),
		UNKNOWN("Unknown");
		
		private final String componentKey;
		
		ComponentType(String _key) {
			this.componentKey = _key;
		}
		public String Key() {
			return this.componentKey;
		}
	}
	
	public static enum NSDType {
		NSD5G, NSD4G;
	}
	
	public static NSDType getNSDType(String nsd) {
		if(nsd == null || nsd.equals("")) return NSDType.NSD5G;	//default
		if(NSDType.NSD5G.name().toLowerCase().contains(nsd.toLowerCase())) return NSDType.NSD5G;
		if(NSDType.NSD4G.name().toLowerCase().contains(nsd.toLowerCase())) return NSDType.NSD4G;
		return NSDType.NSD5G; //default
	}
	
	public static enum ComponentIconType {
		VNF5G, VNF4G, PNF, CONN, CLOUD
	}
}
