package com.kt.e2e.Common;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.DocumentType;
import org.w3c.dom.EntityReference;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;



public class EquipCmdLoad implements ServletContextListener {
	private static final Logger logger = LoggerFactory.getLogger(EquipCmdLoad.class);
	public static String str ="";
	public static String file_path="./equip-command.xml";
	private static Resource resource;
	ServletContext sc = null;




	public void contextInitialized(ServletContextEvent e)

	{   
		test();
		logger.info("777777777777777777777777777");
        	sc = e.getServletContext();
        
		

		sc.setAttribute("Data", "Value");

	}




	public void contextDestroyed(ServletContextEvent e) 

	{

		sc.setAttribute("Data", null);

	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		readDbToXML();
		Document doc;
		try {

			doc = readFileToDOM();
			doc.getDocumentElement().normalize();

			String rootName = doc.getDocumentElement().getNodeName(); //루트 엘리먼트의 이름을 리턴 합니다.

String workingDir = System.getProperty("user.dir");

System.out.println(workingDir);

test();
			


		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		


	}
	
	
	public static void test(){

		EquipCmdLoad ecl=new EquipCmdLoad();
		String path = ecl.getClass().getResource("/").getPath();

		System.out.println("클래스패스="+path);
		file_path=path+"command/equip-command.xml";

		System.out.println("명령어패스="+file_path);

		Document doc = null;
		try {
			doc = readFileToDOM();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		doc.getDocumentElement().normalize();

		String rootName = doc.getDocumentElement().getNodeName(); //루트 엘리먼트의 이름을 리턴 합니다
		// TODO Auto-generated method stub
		NodeList nodeLst_1= doc.getElementsByTagName("COMMAND-BASIC"); //문서에서 region_id 노드를 전부 찾아 배열로 돌려줍니다.
		Node fstNode = nodeLst_1.item(0); //nodeList의 첫번째 노드를 추출 합니다.
		Element fstElmnt = (Element) fstNode;
		
		NodeList nodeLst_val = fstElmnt.getElementsByTagName("command_help");
		//fstElmnt의 하위에 announce_date태그 네임을 검색하여 배열로 돌려 줍니다
		Element fstNmElmnt = (Element) nodeLst_val.item(0); ////fstNmElmntList의 첫번째 노드를 추출 합니다.
		NodeList fstNm = fstNmElmnt.getChildNodes(); //fstNmElmnt의 하위 노드들 추출
		System.out.println("값 : "+ ((Node) fstNm.item(0)).getNodeValue()); //첫번째 노드의 값 출력
		
		NodeList nodeLst_2 = fstElmnt.getElementsByTagName("command-parameter");
	    System.out.println(nodeLst_2.getLength());
		 fstNode = nodeLst_2.item(5); //nodeList의 첫번째 노드를 추출 합니다.
		fstElmnt = (Element) fstNode;
		NodeList nodeLst_3 = fstElmnt.getElementsByTagName("parameter");
		//fstElmnt의 하위에 announce_date태그 네임을 검색하여 배열로 돌려 줍니다
		 fstNmElmnt = (Element) nodeLst_3.item(0); ////fstNmElmntList의 첫번째 노드를 추출 합니다.
		 fstNm = fstNmElmnt.getChildNodes(); //fstNmElmnt의 하위 노드들 추출
		System.out.println("URL : "+ ((Node) fstNm.item(0)).getNodeValue()); //첫번째 노드의 값 출력
		
		nodeLst_3 = fstElmnt.getElementsByTagName("parameter_type");
		//fstElmnt의 하위에 announce_date태그 네임을 검색하여 배열로 돌려 줍니다
		 fstNmElmnt = (Element) nodeLst_3.item(0); ////fstNmElmntList의 첫번째 노드를 추출 합니다.
		 fstNm = fstNmElmnt.getChildNodes(); //fstNmElmnt의 하위 노드들 추출
		System.out.println("URL : "+ ((Node) fstNm.item(0)).getNodeValue()); //첫번째 노드의 값 출력
		
		nodeLst_3 = fstElmnt.getElementsByTagName("min");
		//fstElmnt의 하위에 announce_date태그 네임을 검색하여 배열로 돌려 줍니다
		fstNmElmnt = (Element) nodeLst_3.item(0); ////fstNmElmntList의 첫번째 노드를 추출 합니다.
		fstNm = fstNmElmnt.getChildNodes(); //fstNmElmnt의 하위 노드들 추출
		System.out.println("URL : "+ ((Node) fstNm.item(0)).getNodeValue()); //첫번째 노드의 값 출력
		
		nodeLst_3 = fstElmnt.getElementsByTagName("max");
		//fstElmnt의 하위에 announce_date태그 네임을 검색하여 배열로 돌려 줍니다
		 fstNmElmnt = (Element) nodeLst_3.item(0); ////fstNmElmntList의 첫번째 노드를 추출 합니다.
		 fstNm = fstNmElmnt.getChildNodes(); //fstNmElmnt의 하위 노드들 추출
		System.out.println("URL : "+ ((Node) fstNm.item(0)).getNodeValue()); //첫번째 노드의 값 출력
	}
	

	public static Connection DbConnect(){
		// TODO Auto-generated method stub

		System.out.println("-------- PostgreSQL "
				+ "JDBC Connection Testing ------------");

		try {

			Class.forName("org.postgresql.Driver");

		} catch (ClassNotFoundException e) {

			System.out.println("Where is your PostgreSQL JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();

		}

		System.out.println("PostgreSQL JDBC Driver Registered!");

		Connection connection = null;

		try {

			connection = DriverManager.getConnection(
					"jdbc:postgresql://175.213.170.13/e2edb", "e2e",
					"e2e123");

		} catch (SQLException e) {

			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();

		}

		if (connection != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}

		return connection;
	}
	
	
	
	public static void readDbToXML(){ 

		Connection con=DbConnect();

		try {


			List list = new ArrayList();

			Statement st = con.createStatement();
			Statement st2 = con.createStatement();

			ResultSet rs = st.executeQuery(getCommadBasicSql());
			while ( rs.next() )
			{
				Map<String,Object> map = new HashMap();
				map.put("gug_equip_type_nm", rs.getString ("gug_equip_type_nm")); //ePCC,MME,HSS,PGW
				map.put("command_comp_type_nm", rs.getString ("command_comp_type_nm")); //
				map.put("command", rs.getString ("command"));
				map.put("command_exec_popup_type_cd", rs.getString ("command_exec_popup_type_cd"));
				map.put("command_help", rs.getString ("command_help"));

				ResultSet rs2 = st2.executeQuery(getCommadParameterSql(rs.getString("equip_command_manage_no")));
				List list2 = new ArrayList();
				while ( rs2.next() )
				{
					Map<String,String> map2 = new HashMap();
					map2.put("parameter", rs2.getString ("parameter")); //ePCC,MME,HSS,PGW
					map2.put("man_opt", rs2.getString ("man_opt")); //
					map2.put("parameter_type", rs2.getString ("parameter_type"));
					map2.put("min", rs2.getString ("min"));
					map2.put("max", rs2.getString ("max"));
					map2.put("enum_list", rs2.getString ("enum_list"));
					list2.add(map2);
				}
				rs2.close();

				map.put("command-parameter", list2);
				list.add(map);


			}
			rs.close();
			st.close();
			st2.close();


			str = resultXMLString(list, "0000");
			System.out.println(str);
			Document doc = null;
			try {
				doc = getStringToDOM(str);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.println(doc.getElementById("command_help"));

			System.out.println(getCommadBasicSql());





			con.close();
			con=null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**

	 * Map 객체의 List 배열을 바탕으로 XML을 만든다.

	 * 2008.11.11 ddakker 추가

	 * @param list    : XML 의 list 하위 item을 생성할 데이터 

	 * @param errCode : result 노드에 들어갈 값 | 0: 성공, 기타 에러코드

	 * @return

	 */

	public static String resultXMLString(List list, String errCode){ 
		CDATASection cdata      = null;
		CDATASection cdata2      = null;

		Text         text       = null;
		String       xmlString  = null;



		try{



			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();

			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();

			Document doc = docBuilder.newDocument();


			Element eRoot = doc.createElement("EQUIP-COMMAND");

			doc.appendChild(eRoot);



			Element eResult = doc.createElement("result");

			eRoot.appendChild(eResult);



			text = doc.createTextNode(errCode);

			eResult.appendChild(text);



			Element eListCnt = doc.createElement("list_cnt");

			eRoot.appendChild(eListCnt);



			text = doc.createTextNode(list.size()+"");

			eListCnt.appendChild(text);



			Element eList = doc.createElement("COMMAND-GROUP");

			eRoot.appendChild(eList);



			int listCnt = list.size();

			for( int i=0; i<listCnt; i++ )
			{
				Map map = (Map)list.get(i);
				Set set = map.keySet();
				Element eItem = doc.createElement("COMMAND-BASIC");
				eList.appendChild(eItem);
				Iterator it = set.iterator();

				while( it.hasNext() ){
					String key   = (String)it.next();
					if(!key.equals("command-parameter"))
					{
						Object value = map.get(key);
						Element eKey = doc.createElement(key);
						eItem.appendChild(eKey);
						cdata = doc.createCDATASection(value==null?"":value.toString());                
						eKey.appendChild(cdata);
					}else{
						List parmaList = (List)map.get("command-parameter");
						int paramListCnt = parmaList.size();

						for( int j=0; j<paramListCnt; j++ )
						{
							Map map2 = (Map)parmaList.get(j);
							Set set2 = map2.keySet();
							Element eParameter = doc.createElement("command-parameter");
							eItem.appendChild(eParameter);
							Iterator it2 = set2.iterator();

							while( it2.hasNext() ){
								String key2   = (String)it2.next();
								Object value2 = map2.get(key2);
								Element eKey2 = doc.createElement(key2);
								eParameter.appendChild(eKey2);
								cdata2 = doc.createCDATASection((value2==null || value2.toString().trim().equals(""))?" ":value2.toString());                
								eKey2.appendChild(cdata2);


							}
						}
					}

				}
			}



			xmlString = getDOMToString(doc);



		}catch(Exception e){

			System.out.println(e);

			xmlString = "error";// faultXMLString("1111", "XML생성에 실패 하였습니다.");



		}               



		return xmlString;

	}



	/**

	 * XML 문자열을 DOM(Document) 객체로 변환

	 * @param xmlStr    XML문자열

	 * @return

	 * @throws Exception

	 */

	private static Document getStringToDOM(String xmlStr) throws Exception {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		DocumentBuilder builder = factory.newDocumentBuilder();

		return builder.parse(new InputSource(new StringReader(xmlStr)));

	}



	/**

	 * DOM(Document) 객체를 XML 문자열로 변환

	 * @param xmlStr    XML문자열

	 * @return

	 * @throws Exception

	 */

	private static String getDOMToString(Document doc) throws Exception {

		TransformerFactory transfac = TransformerFactory.newInstance();
		Transformer trans = transfac.newTransformer();
		trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		trans.setOutputProperty(OutputKeys.INDENT, "yes");


		StringWriter sw = new StringWriter();
		StreamResult result = new StreamResult(sw);
		DOMSource source = new DOMSource(doc);
		trans.transform(source, result);
		writeDOMToFile(doc);
		return sw.toString();

	}

	private static void writeDOMToFile(Document doc) throws Exception {


		TransformerFactory transformerFactory = TransformerFactory.newInstance();

		transformerFactory.setAttribute("indent-number", new Integer(4));
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");

		DOMSource source = new DOMSource(doc);


		FileWriter fw = new FileWriter(file_path);

		//StreamResult result = new StreamResult(new FileOutputStream(new File(file)) );

		StreamResult result = new StreamResult(fw);

		transformer.transform(source, result);

	}
	
	private static Document readFileToDOM() throws Exception {

		File file = new File(file_path);

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		DocumentBuilder db = dbf.newDocumentBuilder();

		Document doc = db.parse(file);
		return doc;


	}






	/**

	 * 장비 기본 명령어 데이타 가져오기

	 * @return String

	 * @throws Exception

	 */

	private static String getCommadBasicSql() {

		StringBuffer sql=new StringBuffer();
		sql.append("SELECT A.equip_command_manage_no ");
		sql.append(",(SELECT com_dtl_cd_nm"                                                     );
		sql.append(" FROM tb_e2e_com_dtl_cd_manage "                                            );
		sql.append(" WHERE 1=1 "                                                                );
		sql.append("   AND COM_GRP_CD='G002' "                                                  );
		sql.append("   AND COM_DTL_CD= A.gug_equip_type_cd ) AS gug_equip_type_nm "             );
		sql.append(",(SELECT com_dtl_cd_nm"                                                     );
		sql.append(" FROM tb_e2e_com_dtl_cd_manage "                                            );
		sql.append(" WHERE 1=1 "                                                                );
		sql.append("   AND COM_GRP_CD='G006' "                                                  );
		sql.append("   AND COM_DTL_CD= A.command_comp_type_cd ) AS command_comp_type_nm "       );
		sql.append(" ,command "                                                                 );
		sql.append(" ,'Y' AS command_exec_popup_type_cd "                                       );
		sql.append(" ,' ' as command_help "                                                     );
		sql.append(" FROM"                                                                      );
		sql.append(" tb_e2e_equip_command_manage A ORDER BY A.equip_command_manage_no"          );  
		return  sql.toString();
	}

	/**

	 * 장비 기본 명령어 데이타 가져오기
	 * @param xmlStr    XML문자열
	 * @return String

	 * @throws Exception

	 */

	private static String getCommadParameterSql(String equip_command_manage_no) {

		StringBuffer sql=new StringBuffer();
		sql.append("SELECT  equip_command_manage_no ");
		sql.append(",parameter ");
		sql.append(",man_opt ");
		sql.append(",parameter_type ");
		sql.append(",min ");
		sql.append(",max ");
		sql.append(",enum_list ");

		sql.append(" FROM tb_e2e_equip_command_parameter_manage "                 );
		sql.append(" WHERE 1=1 "                                                                );
		sql.append("   AND equip_command_manage_no=" +equip_command_manage_no                 );
		sql.append(" ORDER BY man_opt ASC "                 );
System.out.println(sql.toString());
		return  sql.toString();
	}
}
