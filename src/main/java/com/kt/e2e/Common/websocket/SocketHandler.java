package com.kt.e2e.Common.websocket;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

public class SocketHandler extends TextWebSocketHandler implements InitializingBean {

	static final Logger logger = LoggerFactory.getLogger(SocketHandler.class);

	private Set<WebSocketSession> sessionSet = new HashSet<WebSocketSession>();

	@Autowired
	private WebsocketService websocketService;

	public SocketHandler() {
		super();
		logger.info("Create SocketHandler instance!");
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		super.afterConnectionClosed(session, status);
		sessionSet.remove(session);
		logger.info("Remove session!");
		if (sessionSet.size() == 0) {
			websocketService.disabledSocketSubscriber();
		}
	}

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		super.afterConnectionEstablished(session);
		sessionSet.add(session);
		logger.info("Add session!");
		// subscriber 상태를 체크하여 추가or변경
		websocketService.createSocketSubscriber();
	}

	@Override
	public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
		super.handleMessage(session, message);

		logger.info("Receive message:" + message.toString());
	}

	@Override
	public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
		logger.error("Web socket error! " + exception.getMessage());
		afterConnectionClosed(session, new CloseStatus(1000));
	}

	@Override
	public boolean supportsPartialMessages() {
		logger.info("Call method!");
		return super.supportsPartialMessages();
	}

	public void sendMessage(String message) {
		logger.info("Send message : " + message);
		for (WebSocketSession session : sessionSet) {
			if (session.isOpen()) {
				try {
					session.sendMessage(new TextMessage(message));
				} catch (Exception ignored) {
					logger.error("Fail to send message!", ignored);
				}
			}
		}
	}

	@Override
	public void afterPropertiesSet() throws Exception {

	}

}