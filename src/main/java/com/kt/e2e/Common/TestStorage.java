package com.kt.e2e.Common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kt.e2e.Common.Constant.ComponentIconType;
import com.kt.e2e.Common.Constant.NSDType;
import com.kt.e2e.Domain.CloudComponent;
import com.kt.e2e.Domain.ConnectionComponent;
import com.kt.e2e.Domain.NodeComponent;

public class TestStorage {

	public static ArrayList<NodeComponent> nodeList = new ArrayList<NodeComponent>();
	public static ArrayList<ConnectionComponent> connList = new ArrayList<ConnectionComponent>();
	public static ArrayList<CloudComponent> cloudList = new ArrayList<CloudComponent>();
	
	public static HashMap<String, Object> hm = null;
	
	@SuppressWarnings("unchecked")
	public static List<Object> getComponents(String type) {
		if(hm == null) {
			hm = new HashMap<String,Object>();
		}
		
		if(!hm.containsKey(type)) {
			NSDType nsdType = Constant.getNSDType(type);
			ArrayList<Object> list = new ArrayList<Object>();
			list.add(getIcons(nsdType, ComponentIconType.VNF5G));
			list.add(getIcons(nsdType, ComponentIconType.VNF4G));
			list.add(getIcons(nsdType, ComponentIconType.PNF));
			list.add(getIcons(nsdType, ComponentIconType.CONN));
			list.add(getIcons(nsdType, ComponentIconType.CLOUD));
			
			hm.put(type, list);
		}

		return (List<Object>)hm.get(type);
	}
	
	private static List<Object> getIcons(NSDType nsdType, ComponentIconType iconType) {
		List<Object> list = new ArrayList<Object>();
		String type = nsdType.name();
//		switch(nsdType) {
//		case NSD4G:
//			switch(iconType) {
//			case VNF5G:
//				list.add(getIcon("CU", "/images/contents/CU_critical.svg", type));
//				list.add(getIcon("GW-C", "/images/contents/GW_C.svg", type));
//				list.add(getIcon("GW-U", "/images/contents/GW_U.svg", type));
//				list.add(getIcon("SM", "/images/contents/SM.svg", type));
//				
//				list.add(getIcon("CSCF", "/images/contents/CSCF.svg", type));
//				list.add(getIcon("C-SGN", "/images/contents/C_SGN.svg", type));
//				list.add(getIcon("ePCC", "/images/contents/ePCC.svg", type));
//				list.add(getIcon("HSS", "/images/contents/HSS.svg", type));
//				list.add(getIcon("MME", "/images/contents/MME.svg", type));
//				list.add(getIcon("PGW", "/images/contents/PGW.svg", type));
//				list.add(getIcon("SCEF", "/images/contents/SCEF.svg", type));
//				list.add(getIcon("SGW", "/images/contents/SGW.svg", type));
//				
//				list.add(getIcon("IPS", "/images/contents/IPS.svg", type));
//				list.add(getIcon("MEC", "/images/contents/MEC.svg", type));
//				list.add(getIcon("NAT", "/images/contents/NAT.svg", type));
//				list.add(getIcon("SA", "/images/contents/SA.svg", type));
//				break;
//			case VNF4G:
//				list.add(getIcon("CU", "/images/contents/CU_critical.svg", type));
//				list.add(getIcon("GW-C", "/images/contents/GW_C.svg", type));
//				list.add(getIcon("GW-U", "/images/contents/GW_U.svg", type));
//				list.add(getIcon("SM", "/images/contents/SM.svg", type));
//				
//				list.add(getIcon("CSCF", "/images/contents/CSCF.svg", type));
//				list.add(getIcon("C-SGN", "/images/contents/C_SGN.svg", type));
//				list.add(getIcon("ePCC", "/images/contents/ePCC.svg", type));
//				list.add(getIcon("HSS", "/images/contents/HSS.svg", type));
//				list.add(getIcon("MME", "/images/contents/MME.svg", type));
//				list.add(getIcon("PGW", "/images/contents/PGW.svg", type));
//				list.add(getIcon("SCEF", "/images/contents/SCEF.svg", type));
//				list.add(getIcon("SGW", "/images/contents/SGW.svg", type));
//				
//				list.add(getIcon("IPS", "/images/contents/IPS.svg", type));
//				list.add(getIcon("MEC", "/images/contents/MEC.svg", type));
//				list.add(getIcon("NAT", "/images/contents/NAT.svg", type));
//				list.add(getIcon("SA", "/images/contents/SA.svg", type));
//				break;
//			case PNF:
//				list.add(getIcon("MME", "/images/contents/PNF_MME.svg", iconType.name()));
//				list.add(getIcon("PGW", "/images/contents/PNF_PGW.svg", iconType.name()));
//				list.add(getIcon("SGW", "/images/contents/PNF_SGW.svg", iconType.name()));
//				list.add(getIcon("SWITCH", "/images/contents/PNF_SWITCH.svg", iconType.name()));
//				list.add(getIcon("L3_SWITCH", "/images/contents/PNF_L3_SWITCH.svg", iconType.name()));
//				list.add(getIcon("MUX", "/images/contents/PNF_MUX.svg", iconType.name()));
//				break;
//			case CONN:
//				list.add(getIcon("VL", "/images/contents/SM_critical.svg", type));
//				list.add(getIcon("EP", "/images/contents/SM_major.svg", type));
//				break;
//			case CLOUD:
//				list.add(getIcon("Central", "/images/contents/_cloud.png", type));
//				list.add(getIcon("Edge", "/images/contents/_cloud.png", type));
//				list.add(getIcon("Server", "/images/contents/SERVER.svg", type));
//				list.add(getIcon("Storage", "/images/contents/STORAGE.svg", type));
//				break;
//			}
//			break;
//		default:
		//case NSD5G: //TODO 1st NSD5GType
			switch(iconType) {
			case VNF5G:
				list.add(getIcon("CU", "/resources/images/contents/CU.svg", "5G"));
				list.add(getIcon("GW-C", "/resources/images/contents/GW_C.svg", "5G"));
				list.add(getIcon("GW-U", "/resources/images/contents/GW_U.svg", "5G"));
				list.add(getIcon("SM", "/resources/images/contents/SM.svg", "5G"));
//				
//				list.add(getIcon("CSCF", "/images/contents/CSCF.svg", "5G"));
//				list.add(getIcon("C-SGN", "/images/contents/C_SGN.svg", "5G"));
//				list.add(getIcon("ePCC", "/images/contents/ePCC.svg", "5G"));
//				list.add(getIcon("HSS", "/images/contents/HSS.svg", "5G"));
//				list.add(getIcon("MME", "/images/contents/MME.svg", "5G"));
//				list.add(getIcon("PGW", "/images/contents/PGW.svg", "5G"));
//				list.add(getIcon("SCEF", "/images/contents/SCEF.svg", "5G"));
//				list.add(getIcon("SGW", "/images/contents/SGW.svg", "5G"));
//				
//				list.add(getIcon("IPS", "/images/contents/IPS.svg", "5G"));
//				list.add(getIcon("MEC", "/images/contents/MEC.svg", "5G"));
//				list.add(getIcon("NAT", "/images/contents/NAT.svg", "5G"));
//				list.add(getIcon("SA", "/images/contents/SA.svg", "5G"));
				break;
			case VNF4G:
//				list.add(getIcon("CU", "/images/contents/CU.svg", "4G"));
//				list.add(getIcon("GW-C", "/images/contents/GW_C.svg", "4G"));
//				list.add(getIcon("GW-U", "/images/contents/GW_U.svg", "4G"));
//				list.add(getIcon("SM", "/images/contents/SM.svg", "4G"));
				
				list.add(getIcon("CSCF", "/resources/images/contents/CSCF.svg", "4G"));
				list.add(getIcon("C-SGN", "/resources/images/contents/C_SGN.svg", "4G"));
				list.add(getIcon("ePCC", "/resources/images/contents/ePCC.svg", "4G"));
				list.add(getIcon("HSS", "/resources/images/contents/HSS.svg", "4G"));
				list.add(getIcon("MME", "/resources/images/contents/MME.svg", "4G"));
				list.add(getIcon("PGW", "/resources/images/contents/PGW.svg", "4G"));
				list.add(getIcon("SCEF", "/resources/images/contents/SCEF.svg", "4G"));
				list.add(getIcon("SGW", "/resources/images/contents/SGW.svg", "4G"));
				
//				list.add(getIcon("IPS", "/images/contents/IPS.svg", "4G"));
//				list.add(getIcon("MEC", "/images/contents/MEC.svg", "4G"));
//				list.add(getIcon("NAT", "/images/contents/NAT.svg", "4G"));
//				list.add(getIcon("SA", "/images/contents/SA.svg", "4G"));
				break;
			case PNF:
				list.add(getIcon("MME", "/resources/images/contents/PNF_MME.svg", iconType.name()));
				list.add(getIcon("PGW", "/resources/images/contents/PNF_PGW.svg", iconType.name()));
				list.add(getIcon("SGW", "/resources/images/contents/PNF_SGW.svg", iconType.name()));
				list.add(getIcon("SWITCH", "/resources/images/contents/PNF_SWITCH.svg", iconType.name()));
				list.add(getIcon("L3_SWITCH", "/resources/images/contents/PNF_L3_SWITCH.svg", iconType.name()));
				list.add(getIcon("MUX", "/resources/images/contents/PNF_MUX.svg", iconType.name()));
				break;
			case CONN:
//				list.add(getIcon("VL", "/images/contents/SM_critical.svg", type));
//				list.add(getIcon("EP", "/images/contents/SM_major.svg", type));
				break;
			case CLOUD:
//				list.add(getIcon("Central", "/images/contents/_cloud.png", type));
//				list.add(getIcon("Edge", "/images/contents/_cloud.png", type));
//				list.add(getIcon("Server", "/images/contents/SERVER.svg", type));
//				list.add(getIcon("Storage", "/images/contents/STORAGE.svg", type));
				break;
			}
//			break;
//		}
		
		return list;
	}
		
	private static Map<String, Object> getIcon(String name, String path, String type) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("name", name);
		map.put("path", path);
		map.put("type", type);
		return map;
	}
	
	// 기존 시연용
//	private static List<Object> getIcons(NSDType nsdType, ComponentIconType iconType) {
//		List<Object> list = new ArrayList<Object>();
//		String type = nsdType.name();
//		switch(nsdType) {
//		case NSD4G:
//			switch(iconType) {
//			case VNF:
//				list.add(getIcon("CU", "/images/contents/CU.svg", type));
//				list.add(getIcon("GW-C", "/images/contents/GW_C.svg", type));
//				list.add(getIcon("GW-U", "/images/contents/GW_U.svg", type));
//				list.add(getIcon("SM", "/images/contents/SM.svg", type));
//				
//				list.add(getIcon("CSCF", "/images/contents/CSCF.svg", type));
//				list.add(getIcon("C-SGN", "/images/contents/C_SGN.svg", type));
//				list.add(getIcon("ePCC", "/images/contents/ePCC.svg", type));
//				list.add(getIcon("HSS", "/images/contents/HSS.svg", type));
//				list.add(getIcon("MME", "/images/contents/MME.svg", type));
//				list.add(getIcon("PGW", "/images/contents/PGW.svg", type));
//				list.add(getIcon("SCEF", "/images/contents/SCEF.svg", type));
//				list.add(getIcon("SGW", "/images/contents/SGW.svg", type));
//				
//				list.add(getIcon("IPS", "/images/contents/IPS.svg", type));
//				list.add(getIcon("MEC", "/images/contents/MEC.svg", type));
//				list.add(getIcon("NAT", "/images/contents/NAT.svg", type));
//				list.add(getIcon("SA", "/images/contents/SA.svg", type));
//				break;
//			case PNF:
//				list.add(getIcon("MME", "/images/contents/PNF_MME.svg", iconType.name()));
//				list.add(getIcon("PGW", "/images/contents/PNF_PGW.svg", iconType.name()));
//				list.add(getIcon("SGW", "/images/contents/PNF_SGW.svg", iconType.name()));
//				list.add(getIcon("SWITCH", "/images/contents/PNF_SWITCH.svg", iconType.name()));
//				list.add(getIcon("L3_SWITCH", "/images/contents/PNF_L3_SWITCH.svg", iconType.name()));
//				list.add(getIcon("MUX", "/images/contents/PNF_MUX.svg", iconType.name()));
//				break;
//			case CLOUD:
//				list.add(getIcon("Central", "/images/contents/_cloud.png", type));
//				list.add(getIcon("Edge", "/images/contents/_cloud.png", type));
//				list.add(getIcon("Server", "/images/contents/SERVER.svg", type));
//				list.add(getIcon("Storage", "/images/contents/STORAGE.svg", type));
//				break;
//			}
//			break;
//		default:
//		//case NSD5G: //TODO 1st NSD5GType
//			switch(iconType) {
//			case VNF:
//				/*
//				list.add(getIcon("CU90", "/images/contents/CU90.svg", type));
//				list.add(getIcon("CU85", "/images/contents/CU85.svg", type));
//				list.add(getIcon("CU80", "/images/contents/CU80.svg", type));
//				list.add(getIcon("CU75", "/images/contents/CU75.svg", type));
//				list.add(getIcon("CU70", "/images/contents/CU70.svg", type));
//				list.add(getIcon("CU65", "/images/contents/CU65.svg", type));
//				list.add(getIcon("CU60", "/images/contents/CU60.svg", type));
//				list.add(getIcon("CU55", "/images/contents/CU55.svg", type));
//				*/
//				list.add(getIcon("CU", "/images/contents/CU.svg", type));
//				list.add(getIcon("GW-C", "/images/contents/GW_C.svg", type));
//				list.add(getIcon("GW-U", "/images/contents/GW_U.svg", type));
//				list.add(getIcon("SM", "/images/contents/SM.svg", type));
//				
//				list.add(getIcon("CSCF", "/images/contents/CSCF.svg", type));
//				list.add(getIcon("C-SGN", "/images/contents/C_SGN.svg", type));
//				list.add(getIcon("ePCC", "/images/contents/ePCC.svg", type));
//				list.add(getIcon("HSS", "/images/contents/HSS.svg", type));
//				list.add(getIcon("MME", "/images/contents/MME.svg", type));
//				list.add(getIcon("PGW", "/images/contents/PGW.svg", type));
//				list.add(getIcon("SCEF", "/images/contents/SCEF.svg", type));
//				list.add(getIcon("SGW", "/images/contents/SGW.svg", type));
//				
//				list.add(getIcon("IPS", "/images/contents/IPS.svg", type));
//				list.add(getIcon("MEC", "/images/contents/MEC.svg", type));
//				list.add(getIcon("NAT", "/images/contents/NAT.svg", type));
//				list.add(getIcon("SA", "/images/contents/SA.svg", type));
//				break;
//			case PNF:
//				list.add(getIcon("MME", "/images/contents/PNF_MME.svg", iconType.name()));
//				list.add(getIcon("PGW", "/images/contents/PNF_PGW.svg", iconType.name()));
//				list.add(getIcon("SGW", "/images/contents/PNF_SGW.svg", iconType.name()));
//				list.add(getIcon("SWITCH", "/images/contents/PNF_SWITCH.svg", iconType.name()));
//				list.add(getIcon("L3_SWITCH", "/images/contents/PNF_L3_SWITCH.svg", iconType.name()));
//				list.add(getIcon("MUX", "/images/contents/PNF_MUX.svg", iconType.name()));
//				break;
//			case CLOUD:
//				list.add(getIcon("Central", "/images/contents/_cloud.png", type));
//				list.add(getIcon("Edge", "/images/contents/_cloud.png", type));
//				list.add(getIcon("Server", "/images/contents/SERVER.svg", type));
//				list.add(getIcon("Storage", "/images/contents/STORAGE.svg", type));
//				break;
//			}
//			break;
//		}
//		
//		return list;
//	}
	
	
	
}
