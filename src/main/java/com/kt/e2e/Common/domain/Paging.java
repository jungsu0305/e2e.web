package com.kt.e2e.Common.domain;

import java.util.List;

import com.kt.e2e.Common.util.MessageUtil;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Paging을 위한 클래스 .<br>
 * <br>
 * List lists; 해당 오브젝트 리스트를 담는다.<br>
 * int totalCount; 전체 갯수<br>
 * int page=1; 현재 페이지<br>
 * int perPage=10; 페이지당 보일 오브젝트 갯수.<br>
 * int perSize=5; 페이지 블록 크기 [예]  << < 12345 > >>
 */
@XStreamAlias("paging")
@SuppressWarnings("rawtypes")
public class Paging {
	
	
	private List lists;
	private int totalCount;
	private int page=1;
	private int perPage=10;
	private int perSize=10;
//	private int perPage=Integer.parseInt(MessageUtil.getMessage("paging.per_page"));
//	private int perSize=Integer.parseInt(MessageUtil.getMessage("paging.per_size"));
	
	public List getLists() {
		return lists;
	}
	public void setLists(List lists) {
		this.lists = lists;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getPerPage() {
		return perPage;
	}
	public void setPerPage(int perPage) {
		this.perPage = perPage;
	}
	public int getPerSize() {
		return perSize;
	}
	public void setPerSize(int perSize) {
		this.perSize = perSize;
	}
	
}
