package com.kt.e2e.Common.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.net.URLCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class AES256Test {
	public static void main(String[] args) throws UnsupportedEncodingException
	, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException
	, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException
	, EncoderException, DecoderException  {
		// TODO Auto-generated method stub
		String key = AES256Util.getKeys();    //key는 16자 이상
		AES256Util aes256 = new AES256Util(key);
		URLCodec codec = new URLCodec();
		String plainText="MEM201605090243"; //평문
		String encText = codec.encode(aes256.aesEncode(plainText)); //평문을 암호문으르 암호화
		String decText = aes256.aesDecode(codec.decode(encText));//암호문을 평문으로 복호화
		System.out.println("### plainText:"+plainText);
		System.out.println("### encText:"+encText);
		System.out.println("### decText:"+decText);
		 
		System.out.println(aes256.checkAesDecode(encText));


	}

}
