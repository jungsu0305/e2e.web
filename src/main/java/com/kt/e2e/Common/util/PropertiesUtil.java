package com.kt.e2e.Common.util;

import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertiesUtil {
	/** Logger for this class and subclasses */
	private final static Logger logger = LoggerFactory.getLogger(PropertiesUtil.class);

	private static HashMap<String, Properties> propses;
	
	
	public static String get(String prop, String key){
		
		if(load(prop) == true){
			return propses.get(prop).getProperty(key);
		}else{
			return "";
		}
	}
	
	/**
	 * Properties 가 적재되어 있는지 확인
	 * 적재되어 있지 않다면 적재함.
	 * properties 파일이 없을 경우 false return.
	 * 
	 * @param prop
	 * @return
	 */
	private static boolean load(String prop){
	
		// 초기화
		if(propses == null){
			init();
		}
		
		if(propses.containsKey(prop)){
			return true;
		}else{
			try{
				StringBuilder path = new StringBuilder();
				path.append("config/").append(prop).append(".properties");

				System.err.println(Thread.currentThread().getContextClassLoader().getResource("."));
			    InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(path.toString());

			    if(is == null) {
			    	path.append("/config/").append(prop).append(".properties");
			    	is = Thread.currentThread().getContextClassLoader().getResourceAsStream(path.toString());
			    }
			    
			    if(is == null) {
				    path.append("resources/config/").append(prop).append(".properties");
				    is = Thread.currentThread().getContextClassLoader().getResourceAsStream(path.toString());
			    }
			    
			    if(is == null) {
				    path.append("/resources/config/").append(prop).append(".properties");
				    is = Thread.currentThread().getContextClassLoader().getResourceAsStream(path.toString());
			    }
			    
			    Properties props = new Properties();
			    props.load(is);
	
			    propses.put(prop, props);
			    
			    is.close();

			    return true;			    
			}catch(Exception e){
				logger.error(getlogText(e));
				return false;
			}
		}
	}
	
	/**
	 *  초기화
	 *  
	 */
	private static void init(){
		propses = new HashMap<String, Properties>();
	}
	
    public static String getlogText(Exception e) {
    	Writer writer = new StringWriter();
    	e.printStackTrace(new PrintWriter(writer));

    	return writer.toString();
    }
}
