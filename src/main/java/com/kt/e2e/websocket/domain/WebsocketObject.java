package com.kt.e2e.websocket.domain;

public class WebsocketObject {

	private String type;

	private Object object;

	public WebsocketObject(String type, Object object) {
		// TODO Auto-generated constructor stub
		this.type = type;
		this.object = object;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	@Override
	public String toString() {
		return "WebsocketObject [type=" + type + ", object=" + object + "]";
	}
}
