package com.kt.e2e.design.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kt.e2e.Common.Constant.ComponentIconType;
import com.kt.e2e.Common.Constant.ComponentType;
import com.kt.e2e.Domain.CloudComponent;
import com.kt.e2e.Domain.ConnectionComponent;
import com.kt.e2e.Domain.InvNfvoComponent;
import com.kt.e2e.Domain.NodeComponent;
import com.kt.e2e.Domain.NsdComponent;
import com.kt.e2e.Domain.NsdmNsdInfoComponent;
import com.kt.e2e.design.dao.DesignMapper;
import com.kt.e2e.design.domain.DesignComponent;
import com.kt.e2e.inventory.dao.PnfdMapper;
import com.kt.e2e.inventory.dao.VnfdMapper;
import com.kt.e2e.inventory.domain.InventoryDescriptor;
import com.kt.e2e.inventory.domain.Pnfd;
import com.kt.e2e.inventory.domain.Vnfd;

@Service
public class DesignService {

	private static final Logger logger = LoggerFactory.getLogger(DesignService.class);
	
	@Autowired
	private DesignMapper mapper;

	@Autowired
	private VnfdMapper vnfdMapper;
	
	@Autowired
	private PnfdMapper pnfdMapper;
	
	private ComponentType getComponentType(JsonObject jsonObj) {
		String type = jsonObj.get("type").toString();
		if(type.contains(ComponentType.NODE.Key())) return ComponentType.NODE;
		if(type.contains(ComponentType.CLOUD.Key())) return ComponentType.CLOUD;
		if(type.contains(ComponentType.CONNECTION.Key())) return ComponentType.CONNECTION;
		if(type.contains(ComponentType.UNKNOWN.Key())) return ComponentType.UNKNOWN;
		
		//TODO 허용?
		return ComponentType.UNKNOWN;
	}
	
	public boolean isExistComponent(String key) { // 중복여부 체크
		ArrayList<NodeComponent> list = (ArrayList<NodeComponent>)mapper.getNodeComponentsWithKey(key);
		
		if(list.size() > 0) return true;
		return false;
	}
	
	public String saveComponents(String key, boolean isNew, String jsonStr, String jsonTest) {
		
		/* if exist key, then remove */
		boolean isExist = isExistComponent(key);
		if(isNew && isExist) {
			StringBuffer sb = new StringBuffer("failed in duplicated key");
			sb.append(" : ").append(key);
			logger.info(sb.toString());
			return sb.toString();
		}
		
		if(isExist) {
//			mapper.deleteNodeComponent(key);
//			mapper.deleteCloudComponent(key);
//			mapper.deleteConnectionComponent(key);
		}
		
		Gson gson = new Gson();
		JsonParser parser = new JsonParser();
		JsonObject dataObj = (JsonObject)parser.parse(jsonTest);

		try {
			JsonArray arr = (JsonArray)parser.parse(jsonStr);
			// Node 및 연결선 저장
			for(int i=0; i < arr.size(); i++) {
				JsonObject jsonObj = (JsonObject)arr.get(i);
				ComponentType type = getComponentType(jsonObj);

				switch(type) {
				case UNKNOWN:
					
					break;
				case NODE:
//					NodeComponent node = gson.fromJson(jsonObj, NodeComponent.class);
//					saveNodeComponent(key, node);
					break;
				case CONNECTION:
//					ConnectionComponent conn = gson.fromJson(jsonObj, ConnectionComponent.class);
//					saveConnectionComponent(key, conn);
					break;
				case CLOUD:
//					CloudComponent cloud = gson.fromJson(jsonObj, CloudComponent.class);
//					saveCloudComponent(key, cloud);
					break;
				default:
					return "failed in Unknown Type Component";
				}
			}

			Map<String, Object> map = new HashMap<String, Object>();

			// nsd_id 중복여부 체크 (중복 이면 update 아니면 insert)
			int cnt = mapper.getNsdDuplication( dataObj.get("nsdId").toString().replace("\"", "") );
			
			try{
//				logger.debug("@@@ dataObj : " + dataObj);
			    if(dataObj.has("nsdId"))      map.put("nsdId"     , dataObj.get("nsdId").toString().replace("\"", ""));
			    if(dataObj.has("nsType"))     map.put("nsType"    , Integer.parseInt(dataObj.get("nsType").toString().replace("\"", "")) );
			    if(dataObj.has("designer"))   map.put("designer"  , dataObj.get("designer").toString().replace("\"", ""));
			    if(dataObj.has("version"))    map.put("version"   , dataObj.get("version").toString().replace("\"", ""));
			    if(dataObj.has("nsdName"))    map.put("nsdName"   , dataObj.get("nsdName").toString().replace("\"", ""));
//			    if(dataObj.has("vnfdId"))     map.put("vnfdId"    , "{\"vnfdId\": " + dataObj.get("vnfdId").toString() + "}");
			    if(dataObj.has("vnfdId"))     map.put("vnfdId"    , dataObj.get("vnfdId").toString() );
			    if(dataObj.has("targetNfvo")) map.put("targetNfvo", dataObj.get("targetNfvo").toString().replace("\"", ""));
			    if(dataObj.has("nsdDesc"))    map.put("nsdDesc"   , dataObj.get("nsdDesc").toString().replace("\"", ""));
			    if(dataObj.has("nsdfId"))     map.put("nsdfId"    , "{\"nsdfId\": " + dataObj.get("nsdfId").toString() + "}" );

			    int num = 0;
			    
			    if( dataObj.has("vnfIdLnf") ) {
			    	num = Integer.parseInt(dataObj.get("vnfIdLnf").toString().replace("\"", "") );
			    }
			    
			    logger.debug(" #### 갯수 : " + num);
			    
//			    if (num == 0) {
//					if(cnt > 0) {
//						mapper.updateNsd(map);
//					} else {
//						mapper.insertNsd(map);
//					}
//			    }
			    
			    ///////////////////////////////////////////////////////////////////////////
			    if( num == 1 ) {
			    	if( dataObj.has("nsdf") ) {
			    		
			    		String nsdId = "";
			    		String nsdf = "";
			    		String vnfProfileId = "";
			    		String vnfdId = "";
			    		String vnfFlavourId = "";
			    		String minNumberOfInstances = "";
			    		String maxNumberOfInstances = "";
			    		String dependencies = "";
			    		String nsdfName = "";
			    		
			    		// 임시 저장 ( VNF Profile Name, VNFD Name )
			    		String vnfNsdfName = ""; 
			    		String vnfdName = "";
			    		
			    		if(dataObj.has("nsdId")) {
			    			nsdId = dataObj.get("nsdId").toString().replace("\"", "");
			    		}
			    		if(dataObj.has("nsdf")) {
			    			nsdf = dataObj.get("nsdf").toString().replace("\"", "");
			    		}
			    		if(dataObj.has("vnfProfileId")) {
			    			vnfProfileId = dataObj.get("vnfProfileId").toString().replace("\"", ""); 
			    		}
			    		if(dataObj.has("vnfdId")) {
			    			vnfdId = dataObj.get("vnfdId").toString().replace("\"", "");
			    		}
			    		if(dataObj.has("vnfFlavourId")) {
			    			vnfFlavourId = dataObj.get("vnfFlavourId").toString();
			    		}
			    		if(dataObj.has("minNumberOfInstances")) {
			    			minNumberOfInstances = dataObj.get("minNumberOfInstances").toString().replace("\"", "");
			    		}
			    		if(dataObj.has("maxNumberOfInstances")) {
			    			maxNumberOfInstances = dataObj.get("maxNumberOfInstances").toString().replace("\"", "");
			    		}
			    		if(dataObj.has("dependencies")) {
			    			dependencies = dataObj.get("dependencies").toString().replace("\"", "");
			    		}
			    		if(dataObj.has("nsdfName")) {
			    			nsdfName = dataObj.get("nsdfName").toString();	
			    		}
			    		if(dataObj.has("vnfNsdfName")) {
			    			vnfNsdfName = dataObj.get("vnfNsdfName").toString().replace("\"", "");
			    		}
//			    		if(dataObj.has("vnfdName")) {
//			    			vnfdName = dataObj.get("vnfdName").toString().replace("\"", "");
//			    		}
			    		
			    		
				    	String data = "";
				    		
				    					    		
				    		data += "{";
				    		data += "\"nsDf\" : [";

				    		data += "{";

				    		data += " \"nsdfId\" : " + "\"" + nsdId + "\"" + ",";
				    		data += " \"nsdfName\" : " + nsdfName + ",";
				    		data += " \"dependencies\" : " + "\"" + dependencies + "\"" + ",";
//				    		data += " \"vnfNsdfName\" : " + "\"" + vnfNsdfName + "\"" + ",";
//				    		data += " \"vnfdName\" : " + "\"" + vnfdName + "\"" + ",";
				    		data += " \"vnfProfile\" : [{";
				    		data += " \"vnfProfileId\" : " + "\"" + vnfProfileId + "\"" + ",";
				    		data += " \"vnfProfileName\" : " + "\"" + vnfNsdfName + "\"" + ",";
				    		data += " \"vnfdId\" : " + "\"" +  vnfdId + "\"" + ",";
				    		
				    		if( "".equals(vnfFlavourId) ) {
				    			data += " \"flavourId\" : " + "\""+ "\"" + ",";
				    		} else {
				    			data += " \"flavourId\" : " +  vnfFlavourId + ",";
				    		}
				    		
				    		if( "".equals( minNumberOfInstances ) ) {
				    			data += " \"minNumberOfInstances\" : " + "\""+ "\"" + ",";
				    		} else {
				    			data += " \"minNumberOfInstances\" : " + minNumberOfInstances + ",";
				    		}
				    		
				    		if( "".equals( maxNumberOfInstances ) ) {
				    			data += " \"maxNumberOfInstances\" : " + "\""+ "\"" + ",";
				    		} else {
				    			data += " \"maxNumberOfInstances\" : " + maxNumberOfInstances + ",";
				    		}
				    		

				    		data += " \"localAffinityOrAntiAffinityRule\" : [{ \"affinityOrAntiAffinity\" : \"affinity\", \"scope\": \"NFVI-node\"";
				    		data += "}]";
				    		data += "}]";

				    		data += "}";
				    		data += "]}";
				    		
				    	map.put("nsdf", data);
				    	logger.debug("### nsdf : " + map.get("nsdf"));
//						if(cnt > 0) {
//							mapper.updateNsd(map);
//						} else {
//							mapper.insertNsd(map);
//						}
				    }
			    } else if(num > 1) {
				    
				    List vnfList = new ArrayList();
				    
				    for(int i = 0; i < num; i++) {
				    	Map<String, Object> vnfMap = new HashMap<String, Object>();
				    	
				    	JsonArray vnfdNsdfIdArr = dataObj.get("vnfdNsdfId").getAsJsonArray();
	//			    	JsonArray nsdfNameArr = dataObj.get("nsdfName").getAsJsonArray();
				    	JsonArray nsdfNameArr = dataObj.get("nsdf").getAsJsonArray();
				    	JsonArray vnfdIdArr = dataObj.get("vnfdId").getAsJsonArray();
				    	JsonArray vnfProfileIdArr = dataObj.get("vnfProfileId").getAsJsonArray();
				    	JsonArray vnfFlavourIdArr = dataObj.get("vnfFlavourId").getAsJsonArray();
				    	JsonArray minNumberOfInstancesArr = dataObj.get("minNumberOfInstances").getAsJsonArray();
				    	JsonArray maxNumberOfInstancesArr = dataObj.get("maxNumberOfInstances").getAsJsonArray();
				    	JsonArray dependenciesArr = dataObj.get("dependencies").getAsJsonArray();
//				    	logger.debug("★★★ : " + vnfdIdArr);
			    		// 임시 저장 ( VNF Profile Name, VNFD Name )
				    	JsonArray vnfNsdfNameArr = dataObj.get("vnfNsdfName").getAsJsonArray();
				    	JsonArray vnfdNameArr = dataObj.get("vnfdName").getAsJsonArray();
				    	
				    	vnfMap.put("nsdfId", vnfdNsdfIdArr.get(i).toString().replace("\"", "") );
				    	vnfMap.put("nsdfName", nsdfNameArr.get(i).toString().replace("\"", "") );
				    	vnfMap.put("vnfdId", vnfdIdArr.get(i).toString().replace("\"", "") );
				    	vnfMap.put("vnfProfileId", vnfProfileIdArr.get(i).toString().replace("\"", "") );
				    	vnfMap.put("vnfFlavourId", vnfFlavourIdArr.get(i).toString().replace("\"", "") );
				    	vnfMap.put("minNumberOfInstances", minNumberOfInstancesArr.get(i).toString().replace("\"", "") );
				    	vnfMap.put("maxNumberOfInstances", maxNumberOfInstancesArr.get(i).toString().replace("\"", "") );
				    	vnfMap.put("dependencies", dependenciesArr.get(i).toString().replace("\"", "") );
				    	
				    	vnfMap.put("vnfNsdfName", vnfNsdfNameArr.get(i).toString().replace("\"", "") );
				    	vnfMap.put("vnfdName", vnfdNameArr.get(i).toString().replace("\"", "") );
	
				    	vnfList.add(vnfMap);
				    }
				    
				    HashMap dataMap  = new HashMap();
				    HashMap dataMap2 = new HashMap();
				    
				    HashMap temp = new HashMap();
//				    logger.debug("★★★★★ : " + vnfList);
				    
				    for(int j = 0; j < vnfList.size(); j++) {
				    	for(int k = 0; k < vnfList.size(); k++) {
				    		dataMap  = (HashMap)vnfList.get(j);
				    		dataMap2 = (HashMap)vnfList.get(k);
	
				    		if( Integer.parseInt( dataMap.get("dependencies").toString().replace("\"", "") ) < Integer.parseInt(dataMap2.get("dependencies").toString().replace("\"", "")) ) {
				    			
				    			temp = ( HashMap )vnfList.get(j);
					    		vnfList.set(j, ( HashMap )vnfList.get(k));
					    		vnfList.set(k, temp);
				    		}
				    	}
				    }
				    
				    String data = "";
				    
				    for(int i = 0; i < vnfList.size(); i++) {
				    	dataMap   = (HashMap)vnfList.get(i);
				    	
				    	
			    		if(i == 0) {
			    			data += "{";
			    			data += "\"nsDf\" : [";
			    		}
	
		    			if(i != 0) {
		    				data += ",";
		    			}
			    		
		    			data += "{";
		    			
			    		data += " \"nsdfId\" : " + "\"" + dataMap.get("nsdfId").toString() + "\"" + ",";
			    		data += " \"nsdfName\" : " + "\"" + dataMap.get("nsdfName").toString() + "\"" + ",";
			    		data += " \"dependencie\" : " + dataMap.get("dependencies").toString() + ",";
//			    		data += " \"vnfNsdfName\" : " + dataMap.get("vnfNsdfName").toString() + ",";
			    		data += " \"vnfdName\" : " + "\"" + dataMap.get("vnfdName").toString() + "\"" + ",";
			    		data += " \"vnfProfile\" : [{";
			    		data += " \"vnfProfileId\" : " + "\"" + dataMap.get("vnfProfileId").toString() + "\"" + ",";
			    		data += " \"vnfProfileName\" : " + "\"" + dataMap.get("vnfNsdfName").toString() + "\"" + ",";
			    		data += " \"vnfdId\" : " + "\"" + dataMap.get("vnfdId").toString() + "\"" + ",";
		    			
			    		data += " \"flavourId\" : " + "\"" + dataMap.get("vnfFlavourId").toString() + "\"" + ",";
			    		data += " \"minNumberOfInstances\" : " + dataMap.get("minNumberOfInstances").toString().replace("\"", "") + ",";
			    		data += " \"maxNumberOfInstances\" : " + dataMap.get("maxNumberOfInstances").toString().replace("\"", "") + ",";
	
			    		data += " \"localAffinityOrAntiAffinityRule\" : [{ \"affinityOrAntiAffinity\" : \"affinity\", \"scope\": \"NFVI-node\"";
			    		data += "}]";
			    		data += "}]";
	
			    		if(vnfList.size() -1 != i) {
			    			dataMap2  = (HashMap)vnfList.get(i+1);
		    				data += ", \"dependencies\" : [{";
		    				data += "\"primaryId\" : [" + "\"" + dataMap.get("vnfProfileId").toString() + "\"" + "],";
		    				data += "\"secondaryId\" : [" + "\"" + dataMap2.get("vnfProfileId").toString() + "\"" + "]";
		    				data += "}]";
			    		}
			    		
			    		data += "}";
			    		
			    		if(vnfList.size() -1 == i) {
			    			data += "]}";
			    		}
			    		
				    }
	
				    map.put("nsdf", data);
			    	
				    logger.debug("### nsdf : " + map.get("nsdf"));
			    	
//					if(cnt > 0) {
//						mapper.updateNsd(map);
//					} else {
//						mapper.insertNsd(map);
//					}

			    }
			} catch(Exception e){
				e.printStackTrace();
				return "failed in " + e.getMessage();
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
			return "failed in " + ex.getMessage();
			
		}
		return null;
	}
	
	private void saveCloudComponent(String key, CloudComponent cloud) {
		cloud.setKey(key);
		mapper.insertCloudComponent(cloud);
	}

	private void saveConnectionComponent(String key, ConnectionComponent conn) {
		conn.setKey(key);
		mapper.insertConnectionComponent(conn);
	}

	private void saveNodeComponent(String key, NodeComponent node) {
		node.setKey(key);
		mapper.insertNodeComponent(node);
	}

	public String loadComponents(String key) {
		ArrayList<NodeComponent> nodeList = loadNodeComponents(key);
		ArrayList<ConnectionComponent> connList = loadConnComponents(key);
		ArrayList<CloudComponent> cloudList = loadCloudComponents(key);
		
		ArrayList<Object> list = new ArrayList<Object>();
		for(NodeComponent node:nodeList) {
			list.add(node);
		}
		for(CloudComponent cloud:cloudList) {
			list.add(cloud);
		}
		for(ConnectionComponent conn:connList) {
			list.add(conn);
		}
		logger.debug(" #### 노드 : " + list);
		return new Gson().toJson(list);
	}

	private ArrayList<CloudComponent> loadCloudComponents(String key) {
		return (ArrayList<CloudComponent>) mapper.getCloudComponentsWithKey(key);
	}

	private ArrayList<ConnectionComponent> loadConnComponents(String key) {
		return (ArrayList<ConnectionComponent>) mapper.getConnectionComponentsWithKey(key);
	}

	private ArrayList<NodeComponent> loadNodeComponents(String key) {
		return (ArrayList<NodeComponent>) mapper.getNodeComponentsWithKey(key);
	}

	public String getComponentList(String type) {

		List<Vnfd> v5gList = vnfdMapper.getList(1);//5G
		List<Vnfd> v4gList = vnfdMapper.getList(0);//4G
		List<Pnfd> pnfList = pnfdMapper.getPnfList();
		
		//일단 PNF, CONN, CLOUD는 fix!
		//List<InventoryDescriptor> pnfList = getComponentList(ComponentIconType.PNF);
		List<InventoryDescriptor> connList = getComponentList(ComponentIconType.CONN);
		List<InventoryDescriptor> cloudList = getComponentList(ComponentIconType.CLOUD);
		
		List<Object> list = new ArrayList();
		list.add(setTypeList(v5gList));
		list.add(setTypeList(v4gList));
		list.add(setTypeList(pnfList));
		list.add(connList);
		list.add(cloudList);
		
		Gson gson = new Gson();
		return gson.toJson(list);
	}
	
	private List setTypeList(List list) {
		Vnfd vnfd;
		Pnfd pnfd;
		for(Object obj : list) {
			if(obj instanceof Vnfd) {
				vnfd = (Vnfd)obj; 
				switch(vnfd.getNsType()) {
				case 0: vnfd.setType("vnf5g"); break;
				case 1: vnfd.setType("vnf4g"); break;
				default: vnfd.setType("vnf5g"); break;
				}
			} else if(obj instanceof Pnfd) {
				pnfd = (Pnfd)obj;
				pnfd.setType("pnf");
			}
		}
		return list;
	}
	
	public int getDesignListCount(Map<String, Object> map) {			
		return mapper.getDesignListCount(map);
	}
	
	public int getDesignTypeCount(DesignComponent design) {			
		return mapper.getDesignTypeCount(design);
	}
	
	public List<DesignComponent> getDesignList(Map<String, Object> map) throws Exception{			
		return mapper.getDesignList(map);
	}
	
	public String getNsd(Map<String, Object> map) throws Exception{

		List<NsdComponent> list = mapper.getNsd(map); // General Info조회
		Gson gson = new Gson();
		return gson.toJson(list);
	}
	
	public String getNsdmNsdf(Map<String, Object> map) throws Exception{

		List<NsdComponent> list = mapper.getNsdmNsdf(map); // NSD Flavor 조회
		Gson gson = new Gson();
		return gson.toJson(list);
	}
	
	public String getNsdmProfile(Map<String, Object> map) throws Exception{

		List<NsdComponent> list = mapper.getNsdmProfile(map); // NSD의 profile 조회
		Gson gson = new Gson();
		return gson.toJson(list);
	}
	
	public List<NsdmNsdInfoComponent> getNsdmnsdInfo(Map<String, Object> map) throws Exception{			
//		List<NsdComponent> list = mapper.getNsdmnsdInfo(map); // Onboarding 결과 조회
//		Gson gson = new Gson();
//		return gson.toJson(list);
		return mapper.getNsdmnsdInfo(map);
	}
	
//	public void insertNsdmNsdInfo(NsdComponent nsd) throws Exception {
//		mapper.insertNsdmNsdInfo(nsd);
//	}
	
	public int insertNsdmNsdInfo(Map<String, Object> map) throws Exception {			
		return mapper.insertNsdmNsdInfo(map);
	}

	// Onboard 결좌 조회
//	public String getNsdmnsdInfo(Map<String, Object> map) throws Exception{
//		List<NsdComponent> list = mapper.getNsdmnsdInfo(map); // Onboarding 결과 조회
//		Gson gson = new Gson();
//		return gson.toJson(list);
//	}
	
	public Map<String, Object> getOrderNum(String nsdType) throws Exception{
		
		List<HashMap<String, Object>> list = (List<HashMap<String, Object>>) mapper.getOrderNum(nsdType);
		
		Map<String, Object> map = new HashMap<String, Object>(); 
		
		
		for(int i = 0; i < list.size(); i++) {

			if( nsdType.equals(list.get(i).get("now_nsd_id")) ) {
				map.put("next_nsd_id", list.get(i).get("next_nsd_id"));
				map.put("prev_nsd_id", list.get(i).get("prev_nsd_id"));
				map.put("now_nsd_id", list.get(i).get("now_nsd_id"));
			}
		
		}
		
		return map;
	}
	
	public List<InvNfvoComponent> getInvNfvo() throws Exception{
		return mapper.getInvNfvo();
	}

	// Nsd 및 토폴로지 삭제
    public void deleteNsd(String nsdId) throws Exception {

    	// 노드, 클라우드, 커넥션 삭제
		mapper.deleteNodeComponent(nsdId);
		mapper.deleteCloudComponent(nsdId);
		mapper.deleteConnectionComponent(nsdId);

		// nsd 삭제
    	mapper.deleteNsd(nsdId);

    }

    /***********************************************************************************************/
	/*** [[ Temporary for PNF, CONN, CLOUD]]                                                       */
	/***********************************************************************************************/
	private InventoryDescriptor getIcon(String name, String path, String type) {
		InventoryDescriptor d = new InventoryDescriptor();
		d.setName(name);
		d.setPath(path);
		d.setType(type);
		return d;
	}
	
	private List<InventoryDescriptor> getComponentList(ComponentIconType icontype) {
		List<InventoryDescriptor> list = new ArrayList<InventoryDescriptor>();
		String type;
		switch(icontype) {
		case PNF:
			type = "pnf";
			list.add(getIcon("IPS", "/resources/images/contents/IPS.svg", type));
			list.add(getIcon("MEC", "/resources/images/contents/MEC.svg", type));
			list.add(getIcon("MME", "/resources/images/contents/PNF_MME.svg", type));
			list.add(getIcon("NAT", "/resources/images/contents/NAT.svg", type));
			list.add(getIcon("PGW", "/resources/images/contents/PNF_PGW.svg", type));
			list.add(getIcon("SA", "/resources/images/contents/SA.svg", type));
			list.add(getIcon("SGW", "/resources/images/contents/PNF_SGW.svg", type));
			list.add(getIcon("SWITCH", "/resources/images/contents/PNF_SWITCH.svg", type));
			list.add(getIcon("L3_SWITCH", "/resources/images/contents/PNF_L3_SWITCH.svg", type));
			list.add(getIcon("MUX", "/resources/images/contents/PNF_MUX.svg", type));
			break;
		case CONN:
			type = "connectivity";
			list.add(getIcon("VL", "/images/contents/SM_critical.svg", type));
			list.add(getIcon("EP", "/images/contents/SM_major.svg", type));
			break;
		case CLOUD:
			type = "cloud";
			list.add(getIcon("Central", "/images/contents/_cloud.png", type));
			list.add(getIcon("Edge", "/images/contents/_cloud.png", type));
			list.add(getIcon("Server", "/images/contents/SERVER.svg", "server"));
			list.add(getIcon("Storage", "/images/contents/STORAGE.svg", "storage"));
			break;
		}
		
		return list;
	}
    
}
