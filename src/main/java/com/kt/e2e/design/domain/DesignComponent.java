package com.kt.e2e.design.domain;
 
import java.util.Date;

import org.apache.ibatis.type.Alias;
 
@Alias("design")
public class DesignComponent {
 
	// tb_e2eo_nsdm_nsd
	private String nsd_id;
	private String designer;
	private String version;
	private String nsd_name;
	private String nsd_invariant_id;
	private String nested_nsd_id;
	private String vnfd_id;
	private String pnfd_id;
	private String sapd_id;
	private String vld_id;
	private String vnffgd_id;
	private String mon_inf_id;
	private String auto_scale_rule;
	private String lcms;
	private String nsdf;
	private String scaling_aspect_id;
	private String affinity_group_id;
	private String scale_level_id;
	private String default_ns_instance_level_id;
	private String ns_prof_id;
	private String dependencies;
	private String security_id;
	private String nsd_state;
	private String fail_reason;
	private int nsd_usage_state;
	private String user_defined;
	private int ns_type;
	private String target_nfvo;
	private String nsd_desc;
	private String creator;
	private Date created_at;
	
    /*화면 연결부가 변수시작*/
//    private String[] checkNo;
    private String searchOption;
    private String keyword;
    private String searchType;
    private int searchTypeCount;
   
	private String prev_nsd_id;
    private String now_nsd_id;
    private String next_nsd_id;
    
    private int curPage;
    private String orderKey;
    private String colName;
    private String ordOption;
    /*화면 연결부가 변수종료*/

	public String getSearchType() {
		return searchType;
	}
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}
	public String getNsd_id() {
		return nsd_id;
	}
	public void setNsd_id(String nsd_id) {
		this.nsd_id = nsd_id;
	}
	public String getDesigner() {
		return designer;
	}
	public void setDesigner(String designer) {
		this.designer = designer;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getNsd_name() {
		return nsd_name;
	}
	public void setNsd_name(String nsd_name) {
		this.nsd_name = nsd_name;
	}
	public String getNsd_invariant_id() {
		return nsd_invariant_id;
	}
	public void setNsd_invariant_id(String nsd_invariant_id) {
		this.nsd_invariant_id = nsd_invariant_id;
	}
	public String getNested_nsd_id() {
		return nested_nsd_id;
	}
	public void setNested_nsd_id(String nested_nsd_id) {
		this.nested_nsd_id = nested_nsd_id;
	}
	public String getVnfd_id() {
		return vnfd_id;
	}
	public void setVnfd_id(String vnfd_id) {
		this.vnfd_id = vnfd_id;
	}
	public String getPnfd_id() {
		return pnfd_id;
	}
	public void setPnfd_id(String pnfd_id) {
		this.pnfd_id = pnfd_id;
	}
	public String getSapd_id() {
		return sapd_id;
	}
	public void setSapd_id(String sapd_id) {
		this.sapd_id = sapd_id;
	}
	public String getVld_id() {
		return vld_id;
	}
	public void setVld_id(String vld_id) {
		this.vld_id = vld_id;
	}
	public String getVnffgd_id() {
		return vnffgd_id;
	}
	public void setVnffgd_id(String vnffgd_id) {
		this.vnffgd_id = vnffgd_id;
	}
	public String getMon_inf_id() {
		return mon_inf_id;
	}
	public void setMon_inf_id(String mon_inf_id) {
		this.mon_inf_id = mon_inf_id;
	}
	public String getAuto_scale_rule() {
		return auto_scale_rule;
	}
	public void setAuto_scale_rule(String auto_scale_rule) {
		this.auto_scale_rule = auto_scale_rule;
	}
	public String getLcms() {
		return lcms;
	}
	public void setLcms(String lcms) {
		this.lcms = lcms;
	}
	public String getNsdf() {
		return nsdf;
	}
	public void setNsdf(String nsdf) {
		this.nsdf = nsdf;
	}
	public String getScaling_aspect_id() {
		return scaling_aspect_id;
	}
	public void setScaling_aspect_id(String scaling_aspect_id) {
		this.scaling_aspect_id = scaling_aspect_id;
	}
	public String getAffinity_group_id() {
		return affinity_group_id;
	}
	public void setAffinity_group_id(String affinity_group_id) {
		this.affinity_group_id = affinity_group_id;
	}
	public String getScale_level_id() {
		return scale_level_id;
	}
	public void setScale_level_id(String scale_level_id) {
		this.scale_level_id = scale_level_id;
	}
	public String getDefault_ns_instance_level_id() {
		return default_ns_instance_level_id;
	}
	public void setDefault_ns_instance_level_id(String default_ns_instance_level_id) {
		this.default_ns_instance_level_id = default_ns_instance_level_id;
	}
	public String getNs_prof_id() {
		return ns_prof_id;
	}
	public void setNs_prof_id(String ns_prof_id) {
		this.ns_prof_id = ns_prof_id;
	}
	public String getDependencies() {
		return dependencies;
	}
	public void setDependencies(String dependencies) {
		this.dependencies = dependencies;
	}
	public String getSecurity_id() {
		return security_id;
	}
	public void setSecurity_id(String security_id) {
		this.security_id = security_id;
	}
	public String getNsd_state() {
		return nsd_state;
	}
	public void setNsd_state(String nsd_state) {
		this.nsd_state = nsd_state;
	}
	public String getFail_reason() {
		return fail_reason;
	}
	public void setFail_reason(String fail_reason) {
		this.fail_reason = fail_reason;
	}
	public int getNsd_usage_state() {
		return nsd_usage_state;
	}
	public void setNsd_usage_state(int nsd_usage_state) {
		this.nsd_usage_state = nsd_usage_state;
	}
	public String getUser_defined() {
		return user_defined;
	}
	public void setUser_defined(String user_defined) {
		this.user_defined = user_defined;
	}
	public int getNs_type() {
		return ns_type;
	}
	public void setNs_type(int ns_type) {
		this.ns_type = ns_type;
	}
	public String getTarget_nfvo() {
		return target_nfvo;
	}
	public void setTarget_nfvo(String target_nfvo) {
		this.target_nfvo = target_nfvo;
	}
	public String getNsd_desc() {
		return nsd_desc;
	}
	public void setNsd_desc(String nsd_desc) {
		this.nsd_desc = nsd_desc;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	public String getSearchOption() {
		return searchOption;
	}
	public void setSearchOption(String searchOption) {
		this.searchOption = searchOption;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public int getSearchTypeCount() {
		return searchTypeCount;
	}
	public void setSearchTypeCount(int searchTypeCount) {
		this.searchTypeCount = searchTypeCount;
	}
	public String getPrev_nsd_id() {
		return prev_nsd_id;
	}
	public void setPrev_nsd_id(String prev_nsd_id) {
		this.prev_nsd_id = prev_nsd_id;
	}
	public String getNow_nsd_id() {
		return now_nsd_id;
	}
	public void setNow_nsd_id(String now_nsd_id) {
		this.now_nsd_id = now_nsd_id;
	}
	public String getNext_nsd_id() {
		return next_nsd_id;
	}
	public void setNext_nsd_id(String next_nsd_id) {
		this.next_nsd_id = next_nsd_id;
	}
	public int getCurPage() {
		return curPage;
	}
	public void setCurPage(int curPage) {
		this.curPage = curPage;
	}
	public String getOrderKey() {
		return orderKey;
	}
	public void setOrderKey(String orderKey) {
		this.orderKey = orderKey;
	}
	public String getColName() {
		return colName;
	}
	public void setColName(String colName) {
		this.colName = colName;
	}
	public String getOrdOption() {
		return ordOption;
	}
	public void setOrdOption(String ordOption) {
		this.ordOption = ordOption;
	}

	@Override
	public String toString() {
		return "DesignComponent [nsd_id=" + nsd_id + ", designer=" + designer + ", version=" + version
		                        + ", nsd_name=" + nsd_name + ", nsd_invariant_id=" + nsd_invariant_id + ", nested_nsd_id=" + nested_nsd_id
								+ ", vnfd_id=" + vnfd_id + ", pnfd_id=" + pnfd_id + ", sapd_id=" + sapd_id
								+ ", vld_id=" + vld_id + ", vnffgd_id=" + vnffgd_id + ", mon_inf_id=" + mon_inf_id
								+ ", auto_scale_rule=" + auto_scale_rule + ", lcms=" + lcms + ", nsdf=" + nsdf
								+ ", scaling_aspect_id=" + scaling_aspect_id + ", affinity_group_id=" + affinity_group_id + ", scale_level_id=" + scale_level_id
								+ ", default_ns_instance_level_id=" + default_ns_instance_level_id + ", ns_prof_id=" + ns_prof_id + ", dependencies=" + dependencies
								+ ", security_id=" + security_id + ", nsd_state=" + nsd_state + ", fail_reason=" + fail_reason
								+ ", nsd_usage_state=" + nsd_usage_state + ", user_defined=" + user_defined + ", ns_type=" + ns_type
								+ ", target_nfvo=" + target_nfvo + ", nsd_desc=" + nsd_desc + ", creator=" + creator
								+ ", created_at=" + created_at + "]";
	}
	
	
}
