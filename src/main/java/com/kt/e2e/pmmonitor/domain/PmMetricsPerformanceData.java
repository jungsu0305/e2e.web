package com.kt.e2e.pmmonitor.domain;

import java.util.List;

public class PmMetricsPerformanceData {
    private String metricsId;
    private List<PmTimeLinePerformanceData> timeLinePerformanceDatas;

    public PmMetricsPerformanceData() {

    }

    public PmMetricsPerformanceData(String metricsId, List<PmTimeLinePerformanceData> timeLinePerformanceDatas) {
        this.metricsId = metricsId;
        this.timeLinePerformanceDatas = timeLinePerformanceDatas;
    }

	public String getMetricsId() {
		return metricsId;
	}

	public void setMetricsId(String metricsId) {
		this.metricsId = metricsId;
	}

	public List<PmTimeLinePerformanceData> getTimeLinePerformanceDatas() {
		return timeLinePerformanceDatas;
	}

	public void setTimeLinePerformanceDatas(List<PmTimeLinePerformanceData> timeLinePerformanceDatas) {
		this.timeLinePerformanceDatas = timeLinePerformanceDatas;
	}


}
