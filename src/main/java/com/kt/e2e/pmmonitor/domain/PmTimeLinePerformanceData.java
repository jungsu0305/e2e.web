package com.kt.e2e.pmmonitor.domain;

import java.util.List;

public class PmTimeLinePerformanceData {
	private String metricTimestamp;
	private List<PmPerformanceData> performanceDatas;

	public PmTimeLinePerformanceData() {

	}

	public PmTimeLinePerformanceData(String metricTimestamp, List<PmPerformanceData> performanceDatas) {
		this.metricTimestamp = metricTimestamp;
		this.performanceDatas = performanceDatas;
	}

	public String getMetricTimestamp() {
		return metricTimestamp;
	}
	public void setMetricTimestamp(String metricTimestamp) {
		this.metricTimestamp = metricTimestamp;
	}
	public List<PmPerformanceData> getPerformanceDatas() {
		return performanceDatas;
	}
	public void setPerformanceDatas(List<PmPerformanceData> performanceDatas) {
		this.performanceDatas = performanceDatas;
	}

	@Override
	public String toString() {
		return "PmTimeLinePerformanceData [metricTimestamp=" + metricTimestamp + ", performanceDatas="
				+ performanceDatas + "]";
	}

}
