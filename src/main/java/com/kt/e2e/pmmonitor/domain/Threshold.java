package com.kt.e2e.pmmonitor.domain;

public class Threshold {
	private String metricId;
	private Integer value;
	public String getMetricId() {
		return metricId;
	}
	public void setMetricId(String metricId) {
		this.metricId = metricId;
	}
	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "VnfThreshold [metricId=" + metricId + ", value=" + value + "]";
	}
}
