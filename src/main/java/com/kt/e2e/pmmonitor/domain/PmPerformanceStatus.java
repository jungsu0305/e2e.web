package com.kt.e2e.pmmonitor.domain;

public class PmPerformanceStatus {

    private String instanceId;
    private String instanceName;
    private String metricId;
    private String value;
    private String metricTimestamp;
    private String name;

    private String yesterdayVal;
    private String lastWeekVal;

    public PmPerformanceStatus() {

    }

    public PmPerformanceStatus(String instanceId, String instanceName, String metricId, String value) {
        this.instanceId = instanceId;
        this.instanceName = instanceName;
        this.metricId = metricId;
        this.value = value;
    }

    public PmPerformanceStatus(PmPerformanceStatus PmPerformanceStatus, String metricId) {
        this.instanceId = PmPerformanceStatus.getInstanceId();
        this.instanceName = PmPerformanceStatus.getInstanceName();
        this.metricId = metricId;
        this.value = "0";
    }

    public String getInstanceId() {
        return instanceId;
    }
    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }
    public String getInstanceName() {
        return instanceName;
    }
    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }
    public String getMetricId() {
        return metricId;
    }
    public void setMetricId(String metricId) {
        this.metricId = metricId;
    }
    public String getMetricTimestamp() {
        return metricTimestamp;
    }
    public void setMetricTimestamp(String metricTimestamp) {
        this.metricTimestamp = metricTimestamp;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYesterdayVal() {
        return yesterdayVal;
    }

    public void setYesterdayVal(String yesterdayVal) {
        this.yesterdayVal = yesterdayVal;
    }

    public String getLastWeekVal() {
        return lastWeekVal;
    }

    public void setLastWeekVal(String lastWeekVal) {
        this.lastWeekVal = lastWeekVal;
    }

    @Override
    public String toString() {
        return "PmPerformanceStatus [instanceId=" + instanceId + ", instanceName=" + instanceName + ", metricId="
                + metricId + ", value=" + value + ", metricTimestamp=" + metricTimestamp + ", name=" + name
                + ", yesterdayVal=" + yesterdayVal + ", lastWeekVal=" + lastWeekVal + "]";
    }

}
