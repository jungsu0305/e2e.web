package com.kt.e2e.pmmonitor.domain;

import com.kt.e2e.Common.domain.CommonCondition;

public class Monitor extends CommonCondition {

	private String search_type;
	private String search;
	private String orderBy;
	private String lastAlarmTime;
	
	public String getSearch_type() {
		return search_type;
	}

	public void setSearch_type(String search_type) {
		this.search_type = search_type;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getLastAlarmTime() {
		return lastAlarmTime;
	}

	public void setLastAlarmTime(String lastAlarmTime) {
		this.lastAlarmTime = lastAlarmTime;
	}

	@Override
	public String toString() {
		return "Monitor [search_type=" + search_type + ", search=" + search + ", orderBy=" + orderBy
				+ ", lastAlarmTime=" + lastAlarmTime + "]";
	}

}
