package com.kt.e2e.pmmonitor.domain;

public class PmPerformanceData {

	private String instanceId;
	private String instanceName;
	private String metricId;
	private String value;
	private String metricTimestamp;

	private String yesterdayVal;
	private String lastWeekVal;

	public PmPerformanceData() {

	}

	public PmPerformanceData(String instanceId, String instanceName, String metricId, String metricTimestamp,
			String value, String yesterdayVal, String lastWeekVal) {
		this.instanceId = instanceId;
		this.instanceName = instanceName;
		this.metricId = metricId;
		this.value = value;
		this.lastWeekVal = lastWeekVal;
		this.yesterdayVal = yesterdayVal;
		this.metricTimestamp = metricTimestamp;
	}

	public PmPerformanceData(PmPerformanceStatus performanceStatus) {
		this.instanceId = performanceStatus.getInstanceId();
		this.instanceName = performanceStatus.getInstanceName();
		this.metricId = performanceStatus.getMetricId();
		this.value = performanceStatus.getValue();
		this.yesterdayVal = performanceStatus.getYesterdayVal();
		this.lastWeekVal = performanceStatus.getLastWeekVal();
		this.metricTimestamp = performanceStatus.getMetricTimestamp();
	}


	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getInstanceName() {
		return instanceName;
	}
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	public String getMetricId() {
		return metricId;
	}
	public void setMetricId(String metricId) {
		this.metricId = metricId;
	}
	public String getMetricTimestamp() {
		return metricTimestamp;
	}
	public void setMetricTimestamp(String metricTimestamp) {
		this.metricTimestamp = metricTimestamp;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

	public String getYesterdayVal() {
		return yesterdayVal;
	}

	public void setYesterdayVal(String yesterdayVal) {
		this.yesterdayVal = yesterdayVal;
	}

	public String getLastWeekVal() {
		return lastWeekVal;
	}

	public void setLastWeekVal(String lastWeekVal) {
		this.lastWeekVal = lastWeekVal;
	}

	@Override
	public String toString() {
		return "PmPeformanceData [instanceId=" + instanceId + ", instanceName=" + instanceName + ", metricId="
				+ metricId + ", value=" + value + ", metricTimestamp=" + metricTimestamp + ", yesterdayVal="
						+ yesterdayVal + ", lastWeekVal=" + lastWeekVal + "]";
	}

}
