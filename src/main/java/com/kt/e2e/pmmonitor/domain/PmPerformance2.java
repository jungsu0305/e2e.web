package com.kt.e2e.pmmonitor.domain;

public class PmPerformance2 {

	private String objType;
	private String objInstanceId;
	private String timeStamp;
	private String pmType;
	private String pmValue;
	private String createdAt;
	
	public String getObjType() {
		return objType;
	}
	public void setObjType(String objType) {
		this.objType = objType;
	}
	public String getObjInstanceId() {
		return objInstanceId;
	}
	public void setObjInstanceId(String objInstanceId) {
		this.objInstanceId = objInstanceId;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getPmType() {
		return pmType;
	}
	public void setPmType(String pmType) {
		this.pmType = pmType;
	}
	public String getPmValue() {
		return pmValue;
	}
	public void setPmValue(String pmValue) {
		this.pmValue = pmValue;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	
	@Override
	public String toString() {
		return "PmPerformance [objType=" + objType + ", objInstanceId=" + objInstanceId + ", timeStamp=" + timeStamp
				+ ", pmType=" + pmType + ", pmValue=" + pmValue + ", createdAt=" + createdAt + "]";
	}
	
}
