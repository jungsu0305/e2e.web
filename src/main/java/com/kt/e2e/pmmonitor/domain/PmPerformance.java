package com.kt.e2e.pmmonitor.domain;

public class PmPerformance {

	private String objType;
	private String objInstanceId;
	private String objInstanceName;
	private String status;
	private String timeStamp;
	private String cpuUsage;
	private String memoryUsage;
	private String tps;
	private String createdAt;
	
	public String getObjType() {
		return objType;
	}
	public void setObjType(String objType) {
		this.objType = objType;
	}
	public String getObjInstanceId() {
		return objInstanceId;
	}
	public void setObjInstanceId(String objInstanceId) {
		this.objInstanceId = objInstanceId;
	}
	
	public String getObjInstanceName() {
		return objInstanceName;
	}
	public void setObjInstanceName(String objInstanceName) {
		this.objInstanceName = objInstanceName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getCpuUsage() {
		return cpuUsage;
	}
	public void setCpuUsage(String cpuUsage) {
		this.cpuUsage = cpuUsage;
	}
	public String getMemoryUsage() {
		return memoryUsage;
	}
	public void setMemoryUsage(String memoryUsage) {
		this.memoryUsage = memoryUsage;
	}
	public String getTps() {
		return tps;
	}
	public void setTps(String tps) {
		this.tps = tps;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	@Override
	public String toString() {
		return "PmPerformance [objType=" + objType + ", objInstanceId=" + objInstanceId + ", timeStamp=" + timeStamp
				+ ", cpuUsage=" + cpuUsage + ", memoryUsage=" + memoryUsage + ", tps=" + tps + ", createdAt=" + createdAt
				+ "]";
	}
	
	
}
