package com.kt.e2e.pmmonitor.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kt.e2e.pmmonitor.dao.PmPerformanceMonitorMapper;
import com.kt.e2e.pmmonitor.domain.Monitor;
import com.kt.e2e.pmmonitor.domain.PmMetricsPerformanceData;
import com.kt.e2e.pmmonitor.domain.PmPerformance;
import com.kt.e2e.pmmonitor.domain.PmPerformanceData;
import com.kt.e2e.pmmonitor.domain.PmPerformanceStatus;
import com.kt.e2e.pmmonitor.domain.PmTimeLinePerformanceData;
import com.kt.e2e.pmmonitor.domain.Threshold;


@Service
public class PmPerformanceMonitorService {

	
	static final Logger log = LoggerFactory.getLogger(PmPerformanceMonitorService.class);

    @Autowired
    private PmPerformanceMonitorMapper pmPerformanceMonitorMapper;
	
    public int getInstanceCount(Monitor monitor){
		return pmPerformanceMonitorMapper.getInstanceCount(monitor);
	}
    
    
    public List<PmPerformance> listInstance(Monitor monitor, int page, int perPage){
    	int start = ((page - 1) * perPage);
		int end = perPage;
		return pmPerformanceMonitorMapper.listInstance(monitor, start, end);
	}
    
    public String getLastTimestamp(Monitor monitor){
    	return pmPerformanceMonitorMapper.getLastTimestamp(monitor);
    }
    
    
    public List<PmMetricsPerformanceData> getVnfcResources(String instanceId, String objInstanceName) {

    	List<PmPerformanceStatus> pmPerformances = null;
    	List<PmMetricsPerformanceData> pmMetricsPerformanceDatas = null;
        List<PmTimeLinePerformanceData> timeLinePerformanceDatas = null;
        List<PmPerformanceData> pmPerformanceDatas = null;

        try {
        	pmPerformances = new ArrayList<>();
        	pmPerformances = pmPerformanceMonitorMapper.listVnfcGroupPerformanceHalfHourYesterdayLastWeek(instanceId);

	        pmMetricsPerformanceDatas = new ArrayList<>();
	        timeLinePerformanceDatas = new ArrayList<>();
	        pmPerformanceDatas = new ArrayList<>();
	        String metricsId = "";
	        String metricTimestamp = "";

	        if (pmPerformances != null && pmPerformances.size() > 0) {
	        	metricsId = pmPerformances.get(0).getMetricId();
	            metricTimestamp = pmPerformances.get(0).getMetricTimestamp();
	        }

	        for (PmPerformanceStatus vnfcPerformance : pmPerformances) {

	        	String performanceTime = vnfcPerformance.getMetricTimestamp();

	        	if(metricsId.equals(vnfcPerformance.getMetricId())){
	        		if (!metricTimestamp.equals(performanceTime)) {
	    				timeLinePerformanceDatas.add(new PmTimeLinePerformanceData(metricTimestamp, pmPerformanceDatas));
	    				metricTimestamp = performanceTime;
	    				pmPerformanceDatas = new ArrayList<>();
					}
	        	} else {
	        		// 새로운 metrics 정보가 시작되는 시점이라면 지금까지 세팅한 PmPerformanceDatas 를 담는다.
					timeLinePerformanceDatas.add(new PmTimeLinePerformanceData(metricTimestamp, pmPerformanceDatas));
					metricTimestamp = performanceTime;
					pmPerformanceDatas = new ArrayList<>();

					pmMetricsPerformanceDatas.add(new PmMetricsPerformanceData(metricsId, timeLinePerformanceDatas));
					metricsId = vnfcPerformance.getMetricId();
					timeLinePerformanceDatas = new ArrayList<>();
	        	}

	        	pmPerformanceDatas.add(new PmPerformanceData(vnfcPerformance));
	        }

			if (pmPerformanceDatas != null && pmPerformanceDatas.size() > 0) {
				timeLinePerformanceDatas.add(new PmTimeLinePerformanceData(metricTimestamp, pmPerformanceDatas));
				pmMetricsPerformanceDatas.add(new PmMetricsPerformanceData(metricsId, timeLinePerformanceDatas));
			}

        } finally {
			// TODO: handle finally clause
        	pmPerformances = null;
            timeLinePerformanceDatas = null;
            pmPerformanceDatas = null;
		}

        return setTimestampForPerformance(10, null, objInstanceName, pmMetricsPerformanceDatas);
    }
    
    
   public List<PmMetricsPerformanceData> setTimestampForPerformance(
		   int minute
		   , String instanceId
		   , String instanceName
		   , List<PmMetricsPerformanceData> performanceDatas){

	   List<PmMetricsPerformanceData> newPerformanceDatas = null;
	   Date date = null;
	   SimpleDateFormat sdformat = null;
	   List<PmTimeLinePerformanceData> newTimelineDatas = null;
	   try {
		   newPerformanceDatas = new ArrayList<>();
		   // 포맷변경 ( 년월일 시분초)
		   date = new Date();
		   sdformat = new SimpleDateFormat("HH:mm:ss");
		   for(PmMetricsPerformanceData performanceData : performanceDatas){

			   // chart 초기 시작 시간 Setting
			   Calendar cal = Calendar.getInstance();
			   cal.setTime(date);
			   cal.add(Calendar.MINUTE, (-1 * minute));

			   List<PmTimeLinePerformanceData> timelineDatas = performanceData.getTimeLinePerformanceDatas();

			   if(timelineDatas == null || timelineDatas.size() == 0){
				   continue;
			   }

			   String metricId = performanceData.getMetricsId();
			   newTimelineDatas = new ArrayList<>();

			   for(int i = 0; i < minute * 6; i++){
				   boolean isExistTimestamp = false;
				   cal.add(Calendar.SECOND, 10 - (cal.getTime().getSeconds()%10));
				   
				   String timestamp = sdformat.format(cal.getTime());

				   for(PmTimeLinePerformanceData timelineData : timelineDatas){
					   String dataTimestamp = timelineData.getMetricTimestamp();
					   if(timestamp.compareTo(dataTimestamp) == 0){
						   newTimelineDatas.add(timelineData);
						   isExistTimestamp = true;
						   break;
					   } else if(timestamp.compareTo(dataTimestamp) < 0){
						   break;
					   }
				   }

				   if(!isExistTimestamp){
					   List<PmPerformanceData> pmPerformanceDatas = new ArrayList<>();
					   pmPerformanceDatas.add(new PmPerformanceData(instanceId, instanceName, metricId, timestamp, "0", "0", "0"));

					   newTimelineDatas.add(new PmTimeLinePerformanceData(timestamp, pmPerformanceDatas));
				   }
			   }

			   newPerformanceDatas.add(new PmMetricsPerformanceData(metricId, newTimelineDatas));
		   }
		} finally {
			// TODO: handle finally clause
		   date = null;
		   sdformat = null;
		   newTimelineDatas = null;
		}
	   return newPerformanceDatas;
   }
   
   public List<String> listPmType(String objInstanceId){
		return pmPerformanceMonitorMapper.listPmType(objInstanceId);
	}
   
   
   public List<Threshold> getThreshold(String instanceId, String objInstanceName) {
   	return pmPerformanceMonitorMapper.getThreshold(instanceId, objInstanceName);
   }
}
