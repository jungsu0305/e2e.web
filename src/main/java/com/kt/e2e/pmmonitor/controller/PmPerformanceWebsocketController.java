package com.kt.e2e.pmmonitor.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kt.e2e.Common.websocket.SocketHandler;
import com.kt.e2e.websocket.domain.WebsocketObject;


@RequestMapping("/pmwebsocket")
@Controller
public class PmPerformanceWebsocketController {
	static final Logger logger = LoggerFactory.getLogger(PmPerformanceWebsocketController.class);

	@Autowired
	private SocketHandler handler;

	@RequestMapping(value="event", method = RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> sendEventMessage(@RequestBody String event) {

////		ObjectMapper mapper = new ObjectMapper();
//		String message;
//		try {
////			WebsocketObject wo = new WebsocketObject("performance", event);
////			message = mapper.writeValueAsString(wo);
//			message = "{\"type\":\"performance\"}";
//			handler.sendMessage(message);
////			wo = null;
//		} catch (JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} finally {
//			mapper = null;
//			message = null;
//		}
//		
		String message;
		message = "{\"type\":\"performance\"}";
		handler.sendMessage(message);

		return new ResponseEntity<String>(HttpStatus.OK);
	}
}