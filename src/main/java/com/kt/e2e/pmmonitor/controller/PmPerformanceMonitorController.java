package com.kt.e2e.pmmonitor.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kt.e2e.Common.domain.Paging;
import com.kt.e2e.pmmonitor.domain.Monitor;
import com.kt.e2e.pmmonitor.domain.PmMetricsPerformanceData;
import com.kt.e2e.pmmonitor.domain.PmPerformance;
import com.kt.e2e.pmmonitor.domain.Threshold;
import com.kt.e2e.pmmonitor.service.PmPerformanceMonitorService;

@Controller
@RequestMapping("/e2e/pm/")
public class PmPerformanceMonitorController {

	/* the logger */
	static final Logger log = LoggerFactory.getLogger(PmPerformanceMonitorController.class);

	/* ServletContext */
	@Autowired
    private ServletContext servletContext;

	/* service : pmPerformanceMonitorService */
	@Autowired
	private PmPerformanceMonitorService pmPerformanceMonitorService;

	@RequestMapping(value = "/monitor")
	public String monitor2(HttpServletRequest request) {
		return "/pmmonitor/monitor2";
	}

	@RequestMapping(value = "/monitor/popup")
	public String monitor(Monitor monitor, Model model) {
		
		String vnfmWebsocketAddress = (String) servletContext.getAttribute("e2eWebsocketAddress");
		model.addAttribute("e2eWebsocketAddress", vnfmWebsocketAddress);
		
		monitor.setPerPage(10);

		int page = (monitor.getPage() != null || monitor.getPage() != 0) ? monitor.getPage() : 1;
		int perPage = monitor.getPerPage();
		int count = 0;
		Paging paging = null;

		try {
			count = pmPerformanceMonitorService.getInstanceCount(monitor);

			paging = new Paging();
			paging.setTotalCount(count);
			paging.setPage(page);
			paging.setPerPage(perPage);
			

			int lastPage = (count % perPage == 0) ? count / perPage : (count / perPage + 1);

			model.addAttribute("paging", paging);
			model.addAttribute("lastPage", lastPage);
			model.addAttribute("searchStatus", monitor.getSearch());
		} finally {
			// TODO: handle finally clause
			paging = null;
		}
		
		return "/pmmonitor/monitor";
	}
	
	
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public String list(Monitor monitor, Model model) {
		monitor.setPerPage(10);

		int page = monitor.getPage() != null ? monitor.getPage() : 1;
		int perPage = monitor.getPerPage();
		int count = 0;
		Paging paging = null;

		try {
			count = pmPerformanceMonitorService.getInstanceCount(monitor);

			paging = new Paging();
			paging.setTotalCount(count);
			paging.setPage(page);
			paging.setPerPage(perPage);
			if (count > 0) {
				paging.setLists(pmPerformanceMonitorService.listInstance(monitor, page, perPage));
			}
			
			String lastTimeStamp = pmPerformanceMonitorService.getLastTimestamp(monitor);
			if (lastTimeStamp == null || lastTimeStamp.equals("")) {
				SimpleDateFormat sdfNow = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				lastTimeStamp = sdfNow.format(new Date(System.currentTimeMillis()));
			}
			model.addAttribute("lastAlarmTime", lastTimeStamp);

			int lastPage = (count % perPage == 0) ? count / perPage : (count / perPage + 1);

			model.addAttribute("paging", paging);
			model.addAttribute("lastPage", lastPage);
			model.addAttribute("searchStatus", monitor.getSearch());
			
		} finally {
			// TODO: handle finally clause
			paging = null;
		}
		
		return "/pmmonitor/list";
	}
	
	@RequestMapping(value = "/{instanceId}/{objInstanceName}/resource")
	@ResponseBody
	public List<PmMetricsPerformanceData> getVnfcResources(@PathVariable("instanceId") String instanceId, @PathVariable("objInstanceName") String objInstanceName) {
		return pmPerformanceMonitorService.getVnfcResources(instanceId, objInstanceName);
	}
	
	@RequestMapping(value = "/{instanceId}/{objInstanceName}/resource/threshold")
	@ResponseBody
	public List<Threshold> getThreshold(@PathVariable("instanceId") String instanceId, @PathVariable("objInstanceName") String objInstanceName) {
		return pmPerformanceMonitorService.getThreshold(instanceId, objInstanceName);
	}
	
	@RequestMapping(value = "/{instanceId}/listPmType")
	@ResponseBody
	public List<String> listPmType(@PathVariable("instanceId") String instanceId) {
		return pmPerformanceMonitorService.listPmType(instanceId);
	}		
}