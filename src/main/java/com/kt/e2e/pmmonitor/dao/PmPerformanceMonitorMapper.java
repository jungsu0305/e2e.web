package com.kt.e2e.pmmonitor.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.kt.e2e.pmmonitor.domain.Monitor;
import com.kt.e2e.pmmonitor.domain.PmPerformance;
import com.kt.e2e.pmmonitor.domain.PmPerformanceStatus;
import com.kt.e2e.pmmonitor.domain.Threshold;

@Component
public interface PmPerformanceMonitorMapper {

	int getInstanceCount(@Param("monitor") Monitor monitor);

	List<PmPerformance> listInstance(@Param("monitor") Monitor monitor
            , @Param("start") int start
            , @Param("end") int end);

	String getLastTimestamp(@Param("monitor") Monitor monitor);
	
	List<PmPerformanceStatus> listVnfcGroupPerformanceHalfHourYesterdayLastWeek(@Param("objInstanceId") String objInstanceId);
	
	List<String> listPmType(@Param("objInstanceId") String objInstanceId);
	
	List<Threshold> getThreshold(@Param("instanceId") String instanceId, @Param("objInstanceName") String objInstanceName);
}

