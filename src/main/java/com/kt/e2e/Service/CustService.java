package com.kt.e2e.Service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kt.e2e.Dao.CustMapper;
import com.kt.e2e.Domain.CustComponent;

@Service
public class CustService {

	private static final Logger logger = LoggerFactory.getLogger(TopologyEditorService.class);
	
	@Autowired
	private CustMapper mapper;
	
	 //고객사작성
    public int custGetManageNo()throws Exception
    {
    	return mapper.custGetManageNo();
    }
    

    //고객사작성
    public void custInsert(CustComponent vo)throws Exception
    {
    	mapper.custInsert(vo);
    }
    
    //고객사목록(ROWNUM방식)
    public List<CustComponent> custList(Map<String, Object> map) throws Exception
    {
    	return mapper.custList(map);
    }
    
    //고객사목록(OFFSET방식)
    public List<CustComponent> custListOffset(Map<String, Object> map) throws Exception
    {
    	return mapper.custListOffset(map);
    }
    
    
    //고객사 lte_id,apn조회
    public List<CustComponent> getLteidApnJsondata(Map<String, Object> map) throws Exception
    {
    	return mapper.getLteidApnJsondata(map);
    }
    
    
    //고객사보기
    public CustComponent custView(int bno) throws Exception
    {
    	return mapper.custView(bno);
    }
    
    

  //고객사 LTE ID ,APN중복체크
    public CustComponent custLteIdApnCheck(Map<String, Object> map) throws Exception
    {
    	return mapper.custLteIdApnCheck(map);
    }
    
    
    
    //조회수 증가
    public void hitPlus(int bno) throws Exception
    {
    	
		mapper.hitPlus(bno);
    }
    
    //고객사수정
    public void custUpdate(CustComponent vo)throws Exception
    {
    	mapper.custUpdate(vo);
    }
 
    //고객 LTE ID,APN 머지 저장
    public void custMerge(CustComponent vo)throws Exception
    {
    	mapper.custMerge(vo);
    }
    
    //고객사삭제
    public void custDelete(int bno)throws Exception
    {
    	mapper.custDelete(bno);
    }
    
    //목록총 갯수
    public int custCount(Map<String, Object> map)throws Exception
    {
    	return mapper.custCount(map);
    }
	
}
