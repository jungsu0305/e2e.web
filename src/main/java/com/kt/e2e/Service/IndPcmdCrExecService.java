package com.kt.e2e.Service;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kt.e2e.Dao.IndPcmdCrExecMapper;
import com.kt.e2e.Domain.DataSummaryComponent;
import com.kt.e2e.Domain.IndPcmdCrExecComponent;

@Service
public class IndPcmdCrExecService {

	private static final Logger logger = LoggerFactory.getLogger(TopologyEditorService.class);
	
	@Autowired
	private IndPcmdCrExecMapper mapper;
	//명령어생성 마스터 번호 가져오기
	public BigInteger indPcmdCrExecGetCreateNo()throws Exception
    {
    	return mapper.indPcmdCrExecGetCreateNo();
    }
	
	//명령어실행 이력 번호 가져오기
	public int indPcmdCrExecGetHistoryNo(IndPcmdCrExecComponent vo)throws Exception
    {
    	return mapper.indPcmdCrExecGetHistoryNo(vo);
    }
	
	
	
	 //명령어실행 이력작성
    public void indPcmdCrExecHistoryInsert(IndPcmdCrExecComponent vo)throws Exception
    {
    	mapper.indPcmdCrExecHistoryInsert(vo);
    }
	
	 //명령어실행 실행 상태값 저장
    public void indPcmdCrExecHistoryUpdate(IndPcmdCrExecComponent vo)throws Exception
    {
    	mapper.indPcmdCrExecHistoryUpdate(vo);
    }	
	
	
	
    //명령어생성작성
    public void indPcmdCrExecInsert(IndPcmdCrExecComponent vo)throws Exception
    {
    	mapper.indPcmdCrExecInsert(vo);
    }
    

    
    
    
    
    //명령어생성목록(OFFSET방식)
    public List<IndPcmdCrExecComponent> indPcmdCrExecListOffset(Map<String, Object> map) throws Exception
    {
    	return mapper.indPcmdCrExecListOffset(map);
    }
    
    //하나의 명령어생성 이력목록
    public List<IndPcmdCrExecComponent> indPcmdCrExecHisList(Map<String, Object> map) throws Exception
    {
    	return mapper.indPcmdCrExecHisList(map);
    }
    
    
    //명령어생성보기
    public IndPcmdCrExecComponent indPcmdCrExecView(BigInteger work_command_create_no) throws Exception
    {
    	return mapper.indPcmdCrExecView(work_command_create_no);
    }
  
    //명령어생성수정
    public void indPcmdCrExecUpdate(IndPcmdCrExecComponent vo)throws Exception
    {
    	mapper.indPcmdCrExecUpdate(vo);
    }
    

    //명령어생성 실제 데이타삭제
    public void indPcmdCrExecDelete(BigInteger gug_data_manage_no)throws Exception
    {
    	mapper.indPcmdCrExecDelete(gug_data_manage_no);
    }
    
    //목록총 갯수
    public int indPcmdCrExecCount(Map<String, Object> map)throws Exception
    {
    	logger.info("indPcmdCrExecCount");
    	return mapper.indPcmdCrExecCount(map);
    }
    
    //이력목록총 갯수
    public int indPcmdCrExecHisCount(Map<String, Object> map)throws Exception
    {
    	return mapper.indPcmdCrExecHisCount(map);
    }
    
    
    // //명령어실행 상태 카운트 요약화 조회
    public List<IndPcmdCrExecComponent> indPcmdCrExecStatusCount()throws Exception
    {
    	return mapper.indPcmdCrExecStatusCount();
    }
    
    //데이터 요약화 저장
    public void indPcmdCrExecSummaryUpdate(Map<String, Object> map)throws Exception
    {
    	mapper.indPcmdCrExecSummaryUpdate(map);
    }
	
    //데이터 요약화 조회
    public DataSummaryComponent indPcmdCrExecSummarySelect(int summary_repr_no)throws Exception
    {
    	return mapper.indPcmdCrExecSummarySelect(summary_repr_no);
    }
    
    
    
    
    
    
}
