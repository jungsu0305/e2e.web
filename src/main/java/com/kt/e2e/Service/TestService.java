package com.kt.e2e.Service;

import java.util.ArrayList;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kt.e2e.Common.Constant.ComponentType;
import com.kt.e2e.Dao.TestDao;
import com.kt.e2e.Domain.CloudComponent;
import com.kt.e2e.Domain.ConnectionComponent;
import com.kt.e2e.Domain.NodeComponent;

@Service
public class TestService {

	private static final Logger logger = LoggerFactory.getLogger(TestService.class);
	
	@Resource(name="TestDao")
	private TestDao testDao;
	
	// 일단, 3가지 경우만 허용
	private ComponentType getComponentType(JsonObject jsonObj) {
	
		if(!jsonObj.has("type")) return ComponentType.CONNECTION;
		
		String type = jsonObj.get("type").toString();
		if(type.contains(ComponentType.NODE.Key())) return ComponentType.NODE;
		if(type.contains(ComponentType.CONNECTION.Key())) return ComponentType.CONNECTION;
		if(type.contains(ComponentType.CLOUD.Key())) return ComponentType.CLOUD;
		
		//TODO 허용할 것인가....
		return ComponentType.UNKNOWN;
	}
	
	private boolean isExistComponent(String key) {
		logger.debug("checking ..... isNew:" + key);
		ArrayList<NodeComponent> list = (ArrayList<NodeComponent>)testDao.getNodeComponent(key);
		
		if(list.size() > 0) return true;
		return false;
	}
	
	public String saveComponents(String key, boolean isNew, String jsonStr) {
		logger.info("saving Components...");
		
		/* if exist key : fail */
		boolean isExist = isExistComponent(key);
		if(isNew && isExist) {
			StringBuffer sb = new StringBuffer("duplicated key");
			sb.append(" : ").append(key);
			logger.info(sb.toString());
			return sb.toString();
		}
		
		if(isExist) testDao.removeComponents(key);
		
		Gson gson = new Gson();
		JsonParser parser = new JsonParser();
		try {
			JsonArray arr = (JsonArray)parser.parse(jsonStr);
			for(int i=0; i < arr.size(); i++) {
				JsonObject jsonObj = (JsonObject)arr.get(i);
				ComponentType type = getComponentType(jsonObj);
				switch(type) {
				case NODE:
					NodeComponent node = gson.fromJson(jsonObj, NodeComponent.class);
					saveNodeComponent(key, node);
					break;
				case CONNECTION:
					ConnectionComponent conn = gson.fromJson(jsonObj, ConnectionComponent.class);
					saveConnectionComponent(key, conn);
					break;
				case CLOUD:
					CloudComponent cloud = gson.fromJson(jsonObj, CloudComponent.class);
					saveCloudComponent(key, cloud);
					break;
				default:
					return "Unknown Type Component";
				}
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
			return "failed in " + ex.getMessage();
			
		}
		return null;
	}
	
	private void saveCloudComponent(String key, CloudComponent cloud) {
		cloud.setKey(key);
		testDao.insertCloudComponent(cloud);
	}

	private void saveConnectionComponent(String key, ConnectionComponent conn) {
		conn.setKey(key);
		testDao.insertConnComponent(conn);
	}

	private void saveNodeComponent(String key, NodeComponent node) {
		node.setKey(key);
		testDao.insertNodeComponent(node);
	}
	
	public String loadComponents(String key) {
		logger.info("loading Components, key="+key);
		ArrayList<NodeComponent> nodeList = loadNodeComponents(key);
		ArrayList<ConnectionComponent> connList = loadConnComponents(key);
		ArrayList<CloudComponent> cloudList = loadCloudComponents(key);
		
		ArrayList<Object> list = new ArrayList<Object>();
		for(NodeComponent node:nodeList) {
			list.add(node);
		}
		for(CloudComponent cloud:cloudList) {
			list.add(cloud);
		}
		for(ConnectionComponent conn:connList) {
			list.add(conn);
		}
		
		return new Gson().toJson(list);
	}

	private ArrayList<CloudComponent> loadCloudComponents(String key) {
		return (ArrayList<CloudComponent>) testDao.getCloudComponent(key);
	}

	private ArrayList<ConnectionComponent> loadConnComponents(String key) {
		return (ArrayList<ConnectionComponent>) testDao.getConnComponent(key);
	}

	private ArrayList<NodeComponent> loadNodeComponents(String key) {
		return (ArrayList<NodeComponent>) testDao.getNodeComponent(key);
	}
}
