package com.kt.e2e.Service;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kt.e2e.Dao.GugPcmdCrExecMapper;
import com.kt.e2e.Domain.DataSummaryComponent;
import com.kt.e2e.Domain.GugPcmdCrExecComponent;

@Service
public class GugPcmdCrExecService {

	private static final Logger logger = LoggerFactory.getLogger(TopologyEditorService.class);
	
	@Autowired
	private GugPcmdCrExecMapper mapper;
	//명령어생성 마스터 번호 가져오기
	public BigInteger gugPcmdCrExecGetCreateNo()throws Exception
    {
    	return mapper.gugPcmdCrExecGetCreateNo();
    }
	
	//명령어실행 이력 번호 가져오기
	public int gugPcmdCrExecGetHistoryNo(GugPcmdCrExecComponent vo)throws Exception
    {
    	return mapper.gugPcmdCrExecGetHistoryNo(vo);
    }
	
	
	
	 //명령어실행 이력작성
    public void gugPcmdCrExecHistoryInsert(GugPcmdCrExecComponent vo)throws Exception
    {
    	mapper.gugPcmdCrExecHistoryInsert(vo);
    }
	
	 //명령어실행 실행 상태값 저장
    public void gugPcmdCrExecHistoryUpdate(GugPcmdCrExecComponent vo)throws Exception
    {
    	mapper.gugPcmdCrExecHistoryUpdate(vo);
    }	
	
	
	
    //명령어생성작성
    public void gugPcmdCrExecInsert(GugPcmdCrExecComponent vo)throws Exception
    {
    	mapper.gugPcmdCrExecInsert(vo);
    }
    

    
    
    
    
    //명령어생성목록(OFFSET방식)
    public List<GugPcmdCrExecComponent> gugPcmdCrExecListOffset(Map<String, Object> map) throws Exception
    {
    	return mapper.gugPcmdCrExecListOffset(map);
    }
    
    //하나의 명령어생성 이력목록
    public List<GugPcmdCrExecComponent> gugPcmdCrExecHisList(Map<String, Object> map) throws Exception
    {
    	return mapper.gugPcmdCrExecHisList(map);
    }
    
    
    //명령어생성보기
    public GugPcmdCrExecComponent gugPcmdCrExecView(BigInteger work_command_create_no) throws Exception
    {
    	return mapper.gugPcmdCrExecView(work_command_create_no);
    }
  
    //명령어생성수정
    public void gugPcmdCrExecUpdate(GugPcmdCrExecComponent vo)throws Exception
    {
    	mapper.gugPcmdCrExecUpdate(vo);
    }
    

    //명령어생성 실제 데이타삭제
    public void gugPcmdCrExecDelete(BigInteger gug_data_manage_no)throws Exception
    {
    	mapper.gugPcmdCrExecDelete(gug_data_manage_no);
    }
    
    //목록총 갯수
    public int gugPcmdCrExecCount(Map<String, Object> map)throws Exception
    {
    	logger.info("gugPcmdCrExecCount");
    	return mapper.gugPcmdCrExecCount(map);
    }
    
    //이력목록총 갯수
    public int gugPcmdCrExecHisCount(Map<String, Object> map)throws Exception
    {
    	return mapper.gugPcmdCrExecHisCount(map);
    }
    
    
    // //명령어실행 상태 카운트 요약화 조회
    public List<GugPcmdCrExecComponent> gugPcmdCrExecStatusCount()throws Exception
    {
    	return mapper.gugPcmdCrExecStatusCount();
    }
    
    //데이터 요약화 저장
    public void gugPcmdCrExecSummaryUpdate(Map<String, Object> map)throws Exception
    {
    	mapper.gugPcmdCrExecSummaryUpdate(map);
    }
	
    //데이터 요약화 조회
    public DataSummaryComponent gugPcmdCrExecSummarySelect(int summary_repr_no)throws Exception
    {
    	return mapper.gugPcmdCrExecSummarySelect(summary_repr_no);
    }
    
    
    
    
    
    
}
