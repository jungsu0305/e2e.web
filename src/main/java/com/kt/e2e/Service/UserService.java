package com.kt.e2e.Service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kt.e2e.Dao.UserMapper;
import com.kt.e2e.Domain.UserComponent;

@Service
public class UserService {

	private static final Logger logger = LoggerFactory.getLogger(UserService.class);
	
	@Autowired
	private UserMapper mapper;

    //글목록(ROWNUM방식)
    public List<UserComponent> userList(Map<String, Object> map) throws Exception
    {
    	return mapper.userList(map);
    }
    
    //글목록(OFFSET방식)
    public List<UserComponent> userListOffset(Map<String, Object> map) throws Exception
    {
    	return mapper.userListOffset(map);
    }
        
    //목록총 갯수
    public int userCount(Map<String, Object> map)throws Exception
    {
    	return mapper.userCount(map);
    }
	
}
