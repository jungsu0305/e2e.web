package com.kt.e2e.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.kt.e2e.Dao.EquipCmdBasMapper;
import com.kt.e2e.Domain.EquipCmdBasComponent;

@Service
public class EquipCmdBasService {

	private static final Logger logger = LoggerFactory.getLogger(TopologyEditorService.class);
	
	@Autowired
	private EquipCmdBasMapper mapper;
	

    //장비기본명령어작성
    public void equipCmdBasInsert(EquipCmdBasComponent vo)throws Exception
    {
    	mapper.equipCmdBasInsert(vo);
    }
    
    //장비기본명령어목록(ROWNUM방식)
    public List<EquipCmdBasComponent> equipCmdBasList(Map<String, Object> map) throws Exception
    {
    	return mapper.equipCmdBasList(map);
    }
    
    //장비기본명령어목록(OFFSET방식)
    public List<EquipCmdBasComponent> equipCmdBasListOffset(Map<String, Object> map) throws Exception
    {
    	return mapper.equipCmdBasListOffset(map);
    }
    
    
    //장비기본명령어보기
    public EquipCmdBasComponent equipCmdBasView(EquipCmdBasComponent vo) throws Exception
    {
    	return mapper.equipCmdBasView(vo);
    }
    
    //장비기본명령어 ENUM_LIST 콤보 조회
    public List<EquipCmdBasComponent> equipCmdBasComboSelect(EquipCmdBasComponent vo) throws Exception
    {
    	return mapper.equipCmdBasComboSelect(vo);
    }
    
    //장비기본명령어 ENUM_LIST 국데이터 참조맵 조회
    public Map<String,Object> equipCmdBasComboMapSelect(EquipCmdBasComponent vo) throws Exception
    {
    	
    	Map<String,Object> comboArray = new HashMap();

    	List<EquipCmdBasComponent> ecbList=mapper.equipCmdBasComboSelect(vo);

        logger.info("dddddd");
    	for(int i=0;i<ecbList.size();i++)
    	{
    		EquipCmdBasComponent tempVo=new EquipCmdBasComponent();
    		tempVo=ecbList.get(i);
    		if(tempVo.getGug_equip_type_cd().trim().equals("D003")
    		   && tempVo.getCommand().trim().equals("CRTE-IP-POOL")
    		   && tempVo.getParameter().trim().equals("TYPE")
    		   ){
	    			String[] enum_list=tempVo.getEnum_list().trim().split("/");
	    			List combo_pool_type_cd=new ArrayList();
	    			for(int j=0;j<enum_list.length;j++)
	    			{
	    				Map<String,Object> map = new HashMap();
	    				map.put("cd", enum_list[j].trim());
	    				map.put("nm", enum_list[j].trim());
	    				combo_pool_type_cd.add(map);
	    			}
	    			comboArray.put("combo_pool_type_cd", combo_pool_type_cd);
    			
    		}else if(tempVo.getGug_equip_type_cd().trim().equals("D003")
    	    		   && tempVo.getCommand().trim().equals("CRTE-IP-POOL")
    	    		   && tempVo.getParameter().trim().equals("STATIC")
    	    		   ){
    	    			String[] enum_list=tempVo.getEnum_list().trim().split("/");
    	    			List combo_static_cd=new ArrayList();
    	    			for(int j=0;j<enum_list.length;j++)
    	    			{
    	    				Map<String,Object> map = new HashMap();
    	    				map.put("cd", enum_list[j].trim());
    	    				map.put("nm", enum_list[j].trim());
    	    				combo_static_cd.add(map);
    	    			}
    	    			comboArray.put("combo_static_cd", combo_static_cd);
    		}else if(tempVo.getGug_equip_type_cd().trim().equals("D003")
 	    		   && tempVo.getCommand().trim().equals("CRTE-IP-POOL")
 	    		   && tempVo.getParameter().trim().equals("TUNNEL")
 	    		   ){
 	    			String[] enum_list=tempVo.getEnum_list().trim().split("/");
 	    			List combo_tunnel_cd=new ArrayList();
 	    			for(int j=0;j<enum_list.length;j++)
 	    			{
 	    				Map<String,Object> map = new HashMap();
 	    				map.put("cd", enum_list[j].trim());
 	    				map.put("nm", enum_list[j].trim());
 	    				combo_tunnel_cd.add(map);
 	    			}
 	    			comboArray.put("combo_tunnel_cd", combo_tunnel_cd);    	    			
    		}else if(tempVo.getGug_equip_type_cd().trim().equals("D003")
  	    		   && tempVo.getCommand().trim().equals("CRTE-APN")
  	    		   && tempVo.getParameter().trim().equals("ALLOC")
  	    		   ){
  	    			String[] enum_list=tempVo.getEnum_list().trim().split("/");
  	    			List combo_ip_alloc_cd=new ArrayList();
  	    			for(int j=0;j<enum_list.length;j++)
  	    			{
  	    				Map<String,Object> map = new HashMap();
  	    				map.put("cd", enum_list[j].trim());
  	    				map.put("nm", enum_list[j].trim());
  	    				combo_ip_alloc_cd.add(map);
  	    			}
  	    			comboArray.put("combo_ip_alloc_cd", combo_ip_alloc_cd);  
    		}else if(tempVo.getGug_equip_type_cd().trim().equals("D003")
   	    		   && tempVo.getCommand().trim().equals("RTRV-APN-INF")
   	    		   && tempVo.getParameter().trim().equals("VAPN")
   	    		   ){
   	    			String[] enum_list=tempVo.getEnum_list().trim().split("/");
   	    			List combo_rad_ip_alloc_cd=new ArrayList();
   	    			for(int j=0;j<enum_list.length;j++)
   	    			{
   	    				Map<String,Object> map = new HashMap();
   	    				map.put("cd", enum_list[j].trim());
   	    				map.put("nm", enum_list[j].trim());
   	    				combo_rad_ip_alloc_cd.add(map);
   	    			}
   	    			comboArray.put("combo_rad_ip_alloc_cd", combo_rad_ip_alloc_cd);   
   	    			comboArray.put("combo_auth_pcrf_pcc_cd", combo_rad_ip_alloc_cd);   
   	    			comboArray.put("combo_acct_react_cd", combo_rad_ip_alloc_cd);   
   	    		 
   	    			comboArray.put("combo_prirank_cd", combo_rad_ip_alloc_cd); 			
    		}else if(tempVo.getGug_equip_type_cd().trim().equals("D004")
    	    		   && tempVo.getCommand().trim().equals("CRTE-PLTE-INFO")
    	    		   && tempVo.getParameter().trim().equals("MPNAME")
    	    		   ){
    	    			String[] enum_list=tempVo.getEnum_list().trim().split("/");
    	    			List combo_rad_ip_alloc_cd=new ArrayList();
    	    			for(int j=0;j<enum_list.length;j++)
    	    			{
    	    				Map<String,Object> map = new HashMap();
    	    				map.put("cd", enum_list[j].trim());
    	    				map.put("nm", enum_list[j].trim());
    	    				combo_rad_ip_alloc_cd.add(map);
    	    			}   	    			
    	    			comboArray.put("combo_mpname_cd", combo_rad_ip_alloc_cd);  
  	    			
    	    			
    	   }
    		
    		
    	}
    	return comboArray;
    }
    
    
    //장비기본명령어수정
    public void equipCmdBasUpdate(EquipCmdBasComponent vo)throws Exception
    {
    	mapper.equipCmdBasUpdate(vo);
    }
    
    //장비기본명령어삭제
    public void equipCmdBasDelete(int bno)throws Exception
    {
    	mapper.equipCmdBasDelete(bno);
    }
    
    //목록총 갯수
    public int equipCmdBasCount(Map<String, Object> map)throws Exception
    {
    	return mapper.equipCmdBasCount(map);
    }
	
}
