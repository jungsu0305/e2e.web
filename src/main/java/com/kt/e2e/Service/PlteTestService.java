package com.kt.e2e.Service;

import java.util.ArrayList;
import java.util.List;

import org.omg.CORBA.UNKNOWN;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kt.e2e.Common.Constant.ComponentType;
import com.kt.e2e.Common.TestStorage;
import com.kt.e2e.Dao.PlteTestMapper;
import com.kt.e2e.Domain.CloudComponent;
import com.kt.e2e.Domain.ConnectionComponent;
import com.kt.e2e.Domain.NodeComponent;
import com.kt.e2e.Domain.PlteTestComponent;

@Service
public class PlteTestService {

	private static final Logger logger = LoggerFactory.getLogger(PlteTestService.class);
	
	@Autowired
	private PlteTestMapper mapper;
	
	private ComponentType getComponentType(JsonObject jsonObj) {
		String type = jsonObj.get("type").toString();
		if(type.contains(ComponentType.NODE.Key())) return ComponentType.NODE;
		if(type.contains(ComponentType.CLOUD.Key())) return ComponentType.CLOUD;
		if(type.contains(ComponentType.CONNECTION.Key())) return ComponentType.CONNECTION;
		if(type.contains(ComponentType.UNKNOWN.Key())) return ComponentType.UNKNOWN;
		
		//TODO 허용?
		return ComponentType.UNKNOWN;
	}
	
	public boolean isExistComponent(String key) {
		ArrayList<NodeComponent> list = (ArrayList<NodeComponent>)mapper.getNodeComponentsWithKey(key);
		
		if(list.size() > 0) return true;
		return false;
	}
	
	public String saveComponents(String key, boolean isNew, String jsonStr, String jsonTest) {
		
		/* if exist key, then remove */
//		boolean isExist = isExistComponent(key);
//		if(isNew && isExist) {
//			StringBuffer sb = new StringBuffer("failed in duplicated key");
//			sb.append(" : ").append(key);
//			logger.info(sb.toString());
//			return sb.toString();
//		}
		
//		if(isExist) {
//			//mapper.removeComponents(key);
//			mapper.removeNodeComponent(key);
//			mapper.removeCloudComponent(key);
//			mapper.removeConnectionComponent(key);
//		}
		
		Gson gson = new Gson();
		JsonParser parser = new JsonParser();
//		JsonObject test = (JsonObject)parser.parse(jsonTest);
		
//		JSONParser parser = new JSONParser();
//		Object obj = parser.parse(jsonTest);
//		JsonObject test = (JsonObject)(obj);
//		JSONObject jsonObj = (JSONObject)obj;
		try {
			JsonArray arr = (JsonArray)parser.parse(jsonStr);
			JsonArray arrTest = (JsonArray)parser.parse(jsonTest); //(JsonArray)parser.parse(jsonTest);
			
//			for(int i=0; i < arr.size(); i++) {
//				JsonObject jsonObj = (JsonObject)arr.get(i);
//				ComponentType type = getComponentType(jsonObj);
////				
//				switch(type) {
//				case UNKNOWN:
//					
//					break;
//				case NODE:
//					NodeComponent node = gson.fromJson(jsonObj, NodeComponent.class);
//					saveNodeComponent(key, node);
//					break;
//				case CONNECTION:
//					ConnectionComponent conn = gson.fromJson(jsonObj, ConnectionComponent.class);
//					saveConnectionComponent(key, conn);
//					break;
//				case CLOUD:
//					CloudComponent cloud = gson.fromJson(jsonObj, CloudComponent.class);
//					saveCloudComponent(key, cloud);
//					break;
//				default:
//					return "failed in Unknown Type Component";
//				}
//			}
			
//			if(jsonTest.length() > 0) {
//				PlteTestComponent plte = gson.fromJson(jsonTest, PlteTestComponent.class);
//				logger.debug("★★★ plte : " + plte);
//			}
//			test.
			for(int i=0; i < arrTest.size(); i++) {
				JsonObject jsonObj = (JsonObject)arrTest.get(i);
//				ComponentType type = getComponentType(jsonObj);
//				
//				switch(type) {
//				case UNKNOWN:
				
					PlteTestComponent plte = gson.fromJson(jsonObj, PlteTestComponent.class);
					logger.debug("★★★ 키값 : " + key);
					logger.debug("★★★ plte : " + plte);
//					break;
//				case NODE:
//					NodeComponent node = gson.fromJson(jsonObj, NodeComponent.class);
//					saveNodeComponent(key, node);
//					break;
//				case CONNECTION:
//					ConnectionComponent conn = gson.fromJson(jsonObj, ConnectionComponent.class);
//					saveConnectionComponent(key, conn);
//					break;
//				case CLOUD:
//					CloudComponent cloud = gson.fromJson(jsonObj, CloudComponent.class);
//					saveCloudComponent(key, cloud);
//					break;
//				default:
//					return "failed in Unknown Type Component";
//				}
			}
			
			
		} catch (Exception ex) {
			ex.printStackTrace();
			return "failed in " + ex.getMessage();
			
		}
		return null;
	}
	
	private void saveCloudComponent(String key, CloudComponent cloud) {
		cloud.setKey(key);
		mapper.insertCloudComponent(cloud);
	}

	private void saveConnectionComponent(String key, ConnectionComponent conn) {
		conn.setKey(key);
		mapper.insertConnectionComponent(conn);
	}

	private void saveNodeComponent(String key, NodeComponent node) {
		node.setKey(key);
		mapper.insertNodeComponent(node);
	}
	
	public String loadComponents(String key) {
		ArrayList<NodeComponent> nodeList = loadNodeComponents(key);
		ArrayList<ConnectionComponent> connList = loadConnComponents(key);
		ArrayList<CloudComponent> cloudList = loadCloudComponents(key);
		
		ArrayList<Object> list = new ArrayList<Object>();
		for(NodeComponent node:nodeList) {
			list.add(node);
		}
		for(CloudComponent cloud:cloudList) {
			list.add(cloud);
		}
		for(ConnectionComponent conn:connList) {
			list.add(conn);
		}
		return new Gson().toJson(list);
	}

	private ArrayList<CloudComponent> loadCloudComponents(String key) {
		return (ArrayList<CloudComponent>) mapper.getCloudComponentsWithKey(key);
	}

	private ArrayList<ConnectionComponent> loadConnComponents(String key) {
		return (ArrayList<ConnectionComponent>) mapper.getConnectionComponentsWithKey(key);
	}

	private ArrayList<NodeComponent> loadNodeComponents(String key) {
		return (ArrayList<NodeComponent>) mapper.getNodeComponentsWithKey(key);
	}

	public String getComponentList(String type) {
		
		//FROM DB //TODO
		
		//FROM TestStorage
		List<Object> list = TestStorage.getComponents(type); // 좌측 아이콘 정보
		Gson gson = new Gson();
		return gson.toJson(list);
	}
}
