package com.kt.e2e.Service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kt.e2e.Dao.EquipInvenMapper;
import com.kt.e2e.Domain.EquipInvenComponent;

@Service
public class EquipInvenService {

	private static final Logger logger = LoggerFactory.getLogger(TopologyEditorService.class);
	
	@Autowired
	private EquipInvenMapper mapper;
	

    //장비인벤토리작성
    public void equipInvenInsert(EquipInvenComponent vo)throws Exception
    {
    	mapper.equipInvenInsert(vo);
    }
    
    //장비인벤토리목록(ROWNUM방식)
    public List<EquipInvenComponent> equipInvenList(Map<String, Object> map) throws Exception
    {
    	return mapper.equipInvenList(map);
    }
    
    //장비인벤토리목록(OFFSET방식)
    public List<EquipInvenComponent> equipInvenListOffset(Map<String, Object> map) throws Exception
    {
    	return mapper.equipInvenListOffset(map);
    }
    
    
    //장비인벤토리보기
    public EquipInvenComponent equipInvenView(String equip_id) throws Exception
    {
    	return mapper.equipInvenView(equip_id);
    }
    
    
    //장비인벤토리수정
    public void equipInvenUpdate(EquipInvenComponent vo)throws Exception
    {
    	mapper.equipInvenUpdate(vo);
    }
    
    //장비인벤토리삭제
    public void equipInvenDelete(int bno)throws Exception
    {
    	mapper.equipInvenDelete(bno);
    }
    
    //장비인벤토리 목록총 갯수
    public int equipInvenCount(Map<String, Object> map)throws Exception
    {
    	return mapper.equipInvenCount(map);
    }
	
    //EMS 장비관리정보 보기
    public EquipInvenComponent plteConnInfoView(EquipInvenComponent vo)throws Exception
    {
    	return mapper.plteConnInfoView(vo);
    }
    
    //EMS 장비관리정보 머지
    public void plteConnInfoMerge(EquipInvenComponent vo)throws Exception
    {
    	 mapper.plteConnInfoMerge(vo);
    }
    
    //EMS 장비관리정보 삭제
    public void plteConnInfoDelete(EquipInvenComponent vo)throws Exception{
    	 mapper.plteConnInfoDelete(vo);
    }
}
