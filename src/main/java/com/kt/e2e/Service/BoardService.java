package com.kt.e2e.Service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kt.e2e.Dao.BoardMapper;
import com.kt.e2e.Domain.BoardComponent;

@Service
public class BoardService {

	private static final Logger logger = LoggerFactory.getLogger(TopologyEditorService.class);
	
	@Autowired
	private BoardMapper mapper;
	

    //글작성
    public void boardInsert(BoardComponent vo)throws Exception
    {
    	mapper.boardInsert(vo);
    }
    
    //글목록(ROWNUM방식)
    public List<BoardComponent> boardList(Map<String, Object> map) throws Exception
    {
    	return mapper.boardList(map);
    }
    
    //글목록(OFFSET방식)
    public List<BoardComponent> boardListOffset(Map<String, Object> map) throws Exception
    {
    	return mapper.boardListOffset(map);
    }
    
    
    //글보기
    public BoardComponent boardView(int bno) throws Exception
    {
    	return mapper.boardView(bno);
    }
    
    //조회수 증가
    public void hitPlus(int bno) throws Exception
    {
    	
		mapper.hitPlus(bno);
    }
    
    //글수정
    public void boardUpdate(BoardComponent vo)throws Exception
    {
    	mapper.boardUpdate(vo);
    }
    
    //글삭제
    public void boardDelete(int bno)throws Exception
    {
    	mapper.boardDelete(bno);
    }
    
    //목록총 갯수
    public int boardCount(Map<String, Object> map)throws Exception
    {
    	return mapper.boardCount(map);
    }
	
}
