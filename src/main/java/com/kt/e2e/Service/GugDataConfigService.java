package com.kt.e2e.Service;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kt.e2e.Dao.GugDataConfigMapper;
import com.kt.e2e.Domain.DataSummaryComponent;
import com.kt.e2e.Domain.GugDataConfigComponent;
import com.kt.e2e.Domain.GugDataConfigDetSelComponent;

@Service
public class GugDataConfigService {

	private static final Logger logger = LoggerFactory.getLogger(TopologyEditorService.class);
	
	@Autowired
	private GugDataConfigMapper mapper;
	//국데이타 마스터 번호 가져오기
	public BigInteger gugDataConfigGetManageNo()throws Exception
    {
    	return mapper.gugDataConfigGetManageNo();
    }
	
	//국데이타 이력 번호 가져오기
	public int gugDataConfigGetHistoryNo(GugDataConfigComponent vo)throws Exception
    {
    	return mapper.gugDataConfigGetHistoryNo(vo);
    }
	
    //국데이터작성
    public void gugDataConfigInsert(GugDataConfigComponent vo)throws Exception
    {
    	mapper.gugDataConfigInsert(vo);
    }
    
    
    //국데이터작성 MME 디테일
    public void gugDataConfigMmeDetInsert(GugDataConfigDetSelComponent vo)throws Exception
    {
    	mapper.gugDataConfigMmeDetInsert(vo);
    }
    
    //국데이터작성 EPCC 디테일
    public void gugDataConfigEpccDetInsert(GugDataConfigDetSelComponent vo)throws Exception
    {
    	mapper.gugDataConfigEpccDetInsert(vo);
    }
    
    //국데이터작성 PGW 
    public void gugDataConfigPgwInsert(GugDataConfigDetSelComponent vo)throws Exception
    {
    	mapper.gugDataConfigPgwInsert(vo);
		
    }
    
    //데이타 요약화 업데이트
    public void gugDataConfigSummaryUpdate()throws Exception
    {
    	Map<String, Object> map=new HashMap<String, Object>();
    	List<GugDataConfigComponent> gunSummary= gugDataConfigStatusCount();
    	/*'D010','입력중'
    	'D020','입력완료'
    	'D030','변경중'
    	'D040','작업대기'
    	'D050','작업완료'*/
    	/*요약화 초기화*/
	    map.put("summary_type_val_1",0);
		map.put("summary_type_val_2",0);
		map.put("summary_type_val_3",0);
		map.put("summary_type_val_4",0);
		map.put("summary_type_val_5",0);
		map.put("summary_type_val_6",0);
		/*요약화 새로 적용*/
    	for(int i=0;i<gunSummary.size();i++)
    	{
    		GugDataConfigComponent tmpVo=gunSummary.get(i);
    		if(tmpVo.getWork_state_cd().equals("D010"))
    			map.put("summary_type_val_1",tmpVo.getWork_state_cnt());
    		else if(tmpVo.getWork_state_cd().equals("D020"))
    			map.put("summary_type_val_2",tmpVo.getWork_state_cnt());
        	else if(tmpVo.getWork_state_cd().equals("D030"))
    			map.put("summary_type_val_3",tmpVo.getWork_state_cnt());
            else if(tmpVo.getWork_state_cd().equals("D040"))
    			map.put("summary_type_val_4",tmpVo.getWork_state_cnt());
            else if(tmpVo.getWork_state_cd().equals("D050"))
    			map.put("summary_type_val_5",tmpVo.getWork_state_cnt());
            else if(tmpVo.getWork_state_cd().equals("D060"))
    			map.put("summary_type_val_6",tmpVo.getWork_state_cnt());
    	}
    	


    	map.put("summary_repr_no",1);
    	gugDataConfigSummaryUpdate(map);
		
    }
    
    
    
    //국데이터목록(OFFSET방식)
    public List<GugDataConfigComponent> gugDataConfigListOffset(Map<String, Object> map) throws Exception
    {
    	return mapper.gugDataConfigListOffset(map);
    }
    
    //하나의 국데이터 이력목록
    public List<GugDataConfigComponent> gugDataConfigHisList(Map<String, Object> map) throws Exception
    {
    	return mapper.gugDataConfigHisList(map);
    }
    
    
    //국데이터보기
    public GugDataConfigComponent gugDataConfigView(Map<String, Object> map) throws Exception
    {
    	return mapper.gugDataConfigView(map);
    }
    
    public List<GugDataConfigDetSelComponent> gugDataConfigMmeDetView(GugDataConfigComponent vo) throws Exception
    {
    	return mapper.gugDataConfigMmeDetView(vo);
    }
    public List<GugDataConfigDetSelComponent> gugDataConfigEpccDetView(GugDataConfigComponent vo) throws Exception
    {
    	return mapper.gugDataConfigEpccDetView(vo);
    }
    public List<GugDataConfigDetSelComponent> gugDataConfigPgwView(GugDataConfigComponent vo) throws Exception
    {
    	return mapper.gugDataConfigPgwView(vo);
    }
    //국데이터수정
    public void gugDataConfigUpdate(GugDataConfigComponent vo)throws Exception
    {
    	mapper.gugDataConfigUpdate(vo);
    }
    
    //국데이터 선택 실행명령어 수정 
    public void mergeSelectRunCommand(GugDataConfigComponent vo)throws Exception
    {
    	mapper.mergeSelectRunCommand(vo);
    }
    
    //국데이터 선택 실행명령어 목록
    public List<GugDataConfigComponent> commandSelManageList(Map<String, Object> map)throws Exception
    {
    	return mapper.commandSelManageList(map);
    }
    
    //국데이터 선택 실행명령어 삭제 
    public void commandSelManageDelete(Map<String, Object> map)throws Exception
    {
    	mapper.commandSelManageDelete(map);
    }
    
    // 국데이터복사
    public void gugDataConfigInsertCopy(GugDataConfigComponent vo)throws Exception
    {
    	//국데이터복사
    	vo.setWork_state_cd("D010"); //임시저장상태로 저장
    	mapper.gugDataConfigInsertCopy(vo);
    	
    }
    
    // 국데이터국데이터 상태 삭제 등록
    public void gugDataConfigInsertDelete(GugDataConfigComponent vo)throws Exception
    {
    	//국데이터  상태 삭제 이력 등록
    	vo.setWork_state_cd("D060"); //삭제상태로 저장
    	logger.info("getNew_gug_data_manage_no="+vo.getNew_gug_data_manage_no());
    	mapper.gugDataConfigInsertCopy(vo);
    	
    }
    
    //국데이터 실제 데이타삭제
    public void gugDataConfigDelete(BigInteger gug_data_manage_no)throws Exception
    {
    	mapper.gugDataConfigDelete(gug_data_manage_no);
    }
    
    //목록총 갯수
    public int gugDataConfigCount(Map<String, Object> map)throws Exception
    {
    	return mapper.gugDataConfigCount(map);
    }
    
    //이력목록총 갯수
    public int gugDataConfigHisCount(Map<String, Object> map)throws Exception
    {
    	return mapper.gugDataConfigHisCount(map);
    }
    
    
    // //국데이타 상태 카운트 요약화 조회
    public List<GugDataConfigComponent> gugDataConfigStatusCount()throws Exception
    {
    	return mapper.gugDataConfigStatusCount();
    }
    
    //데이터 요약화 저장
    public void gugDataConfigSummaryUpdate(Map<String, Object> map)throws Exception
    {
    	mapper.gugDataConfigSummaryUpdate(map);
    }
	
    //데이터 요약화 조회
    public DataSummaryComponent gugDataConfigSummarySelect(int summary_repr_no)throws Exception
    {
    	return mapper.gugDataConfigSummarySelect(summary_repr_no);
    }
    
    
    
    
    
    
}
