package com.kt.e2e.provisioning.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kt.e2e.provisioning.dao.ProvisionMapper;
import com.kt.e2e.provisioning.domain.ProvisionComponent;
import com.kt.e2e.provisioning.domain.ProvisionNSComponent;
import com.kt.e2e.provisioning.domain.ProvisionNSDComponent;
import com.kt.e2e.provisioning.domain.ProvisionNotification;
import com.kt.e2e.provisioning.domain.ProvisionVNFDComponent;

@Service
public class ProvisionService {

	private static final Logger logger = LoggerFactory.getLogger(ProvisionService.class);
	
	@Autowired
	private ProvisionMapper mapper;

    public List<ProvisionNotification> selectNotificationList(Map<String, Object> map)throws Exception
    {
    	return mapper.selectNotificationList( map );
    }
    
    public int selectNSCount(Map<String, Object> map)throws Exception
    {
    	return mapper.selectNSCount( map );
    }

    public List<ProvisionNSComponent> selectNSList(Map<String, Object> map)throws Exception
    {
    	return mapper.selectNSList( map );
    }

    public int selectNSDCount(Map<String, Object> map)throws Exception
    {
    	return mapper.selectNSDCount( map );
    }

    public List<ProvisionComponent> selectNSDList(Map<String, Object> map)throws Exception
    {
    	return mapper.selectNSDList( map );
    }
    
    public List<ProvisionNSComponent> selectNSData(Map<String, Object> map)throws Exception
    {
    	return mapper.selectNSData( map );
    }
    
    public List<ProvisionNSDComponent> selectNSDData(Map<String, Object> map)throws Exception
    {
    	return mapper.selectNSDData( map );
    }

    public List<ProvisionVNFDComponent> selectVNFDList(List<String> list )throws Exception
    {
    	return mapper.selectVNFDList( list );
    }
    
    public List<ProvisionNSDComponent> selectNSDFList(Map<String, Object> map)throws Exception
    {
    	return mapper.selectNSDFList( map );
    }
    
    public List<ProvisionComponent> selectNSComponentList(Map<String, Object> map)throws Exception
    {
    	return mapper.selectNSComponentList( map );
    }

    public int selectNSId(Map<String, Object> map)throws Exception
    {
    	return mapper.selectNSId( map );
    }

    public int insertNS(Map<String, Object> map)throws Exception
    {
    	return mapper.insertNS( map );
    }
    
    public void updateNS(Map<String, Object> map)throws Exception
    {
    	mapper.updateNS( map );
    }

}
