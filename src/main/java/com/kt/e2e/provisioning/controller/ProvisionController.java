package com.kt.e2e.provisioning.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.UUID;

import org.postgresql.core.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.kt.e2e.Common.CommonPager;
import com.kt.e2e.Controller.BaseController;
import com.kt.e2e.Domain.GugDataConfigComponent;
import com.kt.e2e.provisioning.domain.ProvisionComponent;
import com.kt.e2e.provisioning.domain.ProvisionNSComponent;
import com.kt.e2e.provisioning.domain.ProvisionNSDComponent;
import com.kt.e2e.provisioning.domain.ProvisionNotification;
import com.kt.e2e.provisioning.domain.ProvisionVNFDComponent;
import com.kt.e2e.provisioning.service.ProvisionService;

@Controller
@RequestMapping("/provisioning")
public class ProvisionController extends BaseController {
    
    private static final Logger logger = LoggerFactory.getLogger(ProvisionController.class);

	@Autowired
	private ProvisionService service;
	
    @RequestMapping(method=RequestMethod.GET)
    public ModelAndView provisionList(@RequestParam HashMap paramMap) throws Exception{
    	String search_option 	= (String) paramMap.get("search_option");
    	String search_keyword	= (String) paramMap.get("search_keyword");
    	String check4g 			= (String) paramMap.get("check4g");
    	String check5g 			= (String) paramMap.get("check5g");
    	String check_plte 		= (String) paramMap.get("check_plte");
    	String currentPage 		= (String) paramMap.get("curPage");
    	
        logger.debug( "search_option  :" + search_option  );
        logger.debug( "search_keyword :" + search_keyword );
        logger.debug( "check4g        :" + check4g       );
        logger.debug( "check5g        :" + check5g       );
        logger.debug( "check_plte     :" + check_plte     );
        logger.debug( "currentPage    :" + currentPage     );
        
        
    	ModelAndView mav = new ModelAndView();
    	try {
        	int curPage = 0;
        	if( currentPage == null || "".equals( currentPage )  ){
        		curPage = 1;
        	}else{
        		curPage = Integer.parseInt( currentPage );
        	}
        	
        	logger.debug( "curPage :"	  + curPage       );
        	
            Map<String, Object> map = new HashMap<String, Object>();
        	int count = service.selectNSCount( map );    	
            int pageScale 	= 10; 
            int blockScale 	= 10; 
            
            CommonPager commonPager = new CommonPager( count, curPage, pageScale, blockScale );
            map.put( "start"			, commonPager.getPageBegin() );
            map.put( "end"				, commonPager.getPageEnd()   );
            map.put( "curPage"			, commonPager.getCurPage()   );
            map.put( "pageScale"		, commonPager.getPageScale() );

            map.put( "search_option"	, search_option );
            if( "status".equals( search_option ) ){
            	 logger.debug( "status true");
            	int keyword = 0;
            	try {
            		if( "".equals( search_keyword ) ){
            			keyword = (Integer) null;
            		}else{
            			keyword = Integer.parseInt( search_keyword );
            		}
            		map.put( "search_keyword"	, keyword );
    			} catch (Exception e) {
    				map.put( "search_keyword"	, "" );
    			}
          	  logger.debug( "status : "+map.get("search_keyword"));

            }else{
            	map.put( "search_keyword"	, search_keyword );
            }
            map.put( "check4g"			, check4g );
            map.put( "check5g"			, check5g );
            map.put( "check_plte"		, check_plte );
            
            List<ProvisionNSComponent> list = service.selectNSList( map );  
            
            
            mav.setViewName( "provisioning/provisioningList" );
            mav.addObject( "count"			, count			 ); 
            mav.addObject( "list"			, list			 );
            mav.addObject( "curPag"		 	, curPage        );
            mav.addObject( "search_option"	, search_option  );
            mav.addObject( "search_keyword"	, search_keyword );
            mav.addObject( "check4g"		, check4g		 );
            mav.addObject( "check5g"		, check5g		 );       
            mav.addObject( "check_plte"		, check_plte	 );
            mav.addObject( "commonPager" 	, commonPager  	 );
            logger.debug( "mav : " + mav );
		} catch (Exception e) {
			e.printStackTrace();
		}

        return mav;

    }
   
    @RequestMapping(value="/view/{nsId}/{nsdId}", method=RequestMethod.GET)
    public ModelAndView provision(
    			@PathVariable( "nsId" ) 	String 	nsId,
    			@PathVariable( "nsdId" ) 	String 	nsdId
    		) throws Exception{

    	nsId = String.valueOf(nsId);
    	nsdId = String.valueOf(nsdId);

    	Map<String, Object> map = new HashMap<String, Object>();

    	String nsdUuid = ""; 
    	
    	List<ProvisionNSComponent> currentNsData	= new ArrayList<ProvisionNSComponent>();

    	if( "newNsIns".equals( nsId ) ){
    		nsdUuid = UUID.randomUUID().toString();
    		map.put("nsId", 0);
    	}else{
        	map.put("nsId", Integer.parseInt( nsId ));
        	
        	currentNsData = service.selectNSData( map );
    	}
    	if( "newNsIns".equals( nsdId ) ){
    		map.put("nsdId", "");
    	}else{
    		map.put("nsdId", nsdId);
    	}
    	
    	List<ProvisionNSComponent> list 	= service.selectNSList( map );
    	List<ProvisionComponent> nsdList	= service.selectNSDList( map );
    	List<ProvisionNSDComponent> nsdData	= service.selectNSDData( map );
    	
    	List<ProvisionVNFDComponent> vnfdList = new ArrayList<ProvisionVNFDComponent>();
    	
    	List vnfdIdList = new ArrayList();
    	try {
        	if( nsdData != null ){
        		ProvisionNSDComponent nsdInfo = nsdData.get(0);
        		String vnfdId = nsdInfo.getVnfd_id();
        		logger.debug( "### vnfdId : " + vnfdId );
        		if( vnfdId != null && !"".equals( vnfdId ) ){
        			vnfdIdList = parseToList( vnfdId, "vnfdId" );
        			
        			vnfdList = service.selectVNFDList( vnfdIdList );
        		}
        	}
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
        ModelAndView mav = new ModelAndView();

    	ProvisionNSComponent nsData = null;
    	if( currentNsData != null && currentNsData.size() > 0 ){
    		nsData = currentNsData.get(0);
            mav.addObject( "nsName"	, nsData.getNs_name() 		); 
            mav.addObject( "nsDesc"	, nsData.getNs_desc() 		); 
            mav.addObject( "nsInsId", nsData.getNs_ins_id() 	); 

    	}
    	
        mav.setViewName( "provisioning/provision" );
        mav.addObject( "nsId"		, nsId 			); 
        mav.addObject( "nsdId"		, nsdId 		); 
        mav.addObject( "nsdUuid"	, nsdUuid		);
        mav.addObject( "list"		, list			);
        mav.addObject( "nsdData"	, nsdData		);
        mav.addObject( "nsdList"	, nsdList		);
        mav.addObject( "vnfdList"	, vnfdList		); 

        return mav;    	
    }    

    public List parseToList( String json, String key ){
    	List list = new ArrayList();
    	if( json !=null && !"".equals( json ) ){
        	String resultValue = "";
        	
        	JsonParser parser = new JsonParser();
        	JsonElement element = parser.parse(json);
        	 logger.info( "### [ parseToList ] element : " + element );
        	
        	 if( element.isJsonPrimitive() ){
        	 }else if( element.isJsonArray() ){
        		  JsonArray jsonArray = element.getAsJsonArray();
                  JsonElement jsonElement;
                  for (int i = 0; i < jsonArray.size(); i++) {
                      jsonElement = jsonArray.get(i);
                      String param = jsonElement.toString();
                      param = param.replaceAll("\"", "");
                      list.add( param );
                  }
                  
        	 }else if( element.isJsonObject() ){
        	 }
    	}
    			
    	return list;
    }
    
    @RequestMapping(value="/nsd/{nsdId}", method=RequestMethod.GET)
    @ResponseBody
    public String provisionNsComponent(
    			@PathVariable( "nsdId" ) 	String 	nsdId
    		) throws Exception{
    	
    	Map<String, Object> map = new HashMap<String, Object>();
    	map.put("nsdId", nsdId);
    	
    	List<ProvisionComponent> nsdComponentList = service.selectNSComponentList( map );
    	logger.info( "### nsdComponentList.size() : " + nsdComponentList.size() );
        
        Gson gson = new Gson();
        String s = gson.toJson(nsdComponentList);
		logger.debug(s);
		return s;
    }

    @RequestMapping(value="/saveGeneralInfo", method=RequestMethod.GET)
    @ResponseBody
    public String provisionSaveGeneralInfo(@RequestParam HashMap<String, String> paramMap) throws Exception{

    	String nsId 			= "";
    	String nsdId 			= "";
    	String nsName 			= "";
    	String nsDesc 			= "";
    	String nsState			= "";

    	if( paramMap != null ){
        	nsId 				= String.valueOf( paramMap.get( "nsId" ) );
        	nsdId 				= String.valueOf( paramMap.get( "nsdId" ) );
        	nsName 				= String.valueOf( paramMap.get( "nsName" ) );
        	nsDesc 				= String.valueOf( paramMap.get( "nsDesc" ) );
        	nsState				= String.valueOf( paramMap.get( "nsState" ) );
    	}
    	
    	logger.info( "### nsId : " + nsId );
    	logger.info( "### nsdId : " + nsdId );
    	logger.info( "### nsName : " + nsName );
    	logger.info( "### nsDesc : " + nsDesc );
    	logger.info( "### nsState : " + nsState );
    	
    	Map<String, Object> map = new HashMap<String, Object>();
    	map.put("ns_id"			, nsId);
    	map.put("nsd_id"		, nsdId);
    	map.put("ns_name"		, nsName);
    	map.put("ns_desc"		, nsDesc);
    	if( nsState == null || "".equals( nsState ) || "null".equals( nsState )  ){
    		map.put("ns_state"	, 0);
    	}else{
    		map.put("ns_state"	, Integer.parseInt( nsState ));
    	}
   	
    	if( "newNsIns".equals( nsId ) ){
    		service.insertNS( map );
        	int idVal = service.selectNSId( map );
        	
        	logger.info( "### id Val : " + idVal );
        	
    		map.put("ns_id"			, idVal );
        	map.put("errorCode"		, 0 );
        	map.put("errorMessage"	, "" );
        	map.put("returnMessage"	, "insert" );        	
    	}else{
    		if( !"".equals( nsId ) ){
    			map.put("ns_id"			, Integer.parseInt(nsId));
    			service.updateNS( map );
    		}
    		
        	int idVal = service.selectNSId( map );
        	
        	logger.info( "### idVal : " + idVal );
        	
    		map.put("ns_id"			, idVal );    		
    		map.put("ns_state"		, 0 );    		
        	map.put("errorCode"		, 0 );
        	map.put("errorMessage"	, "" );
        	map.put("returnMessage"	, "update" );
    	}
    	
        Gson gson = new Gson();
        String resultValue = gson.toJson(map);
		logger.debug(resultValue);
		return resultValue;
    }

    @RequestMapping(value="/restfulPost", method=RequestMethod.GET,produces = "application/text; charset=utf8")
    @ResponseBody
    public String testRestfulPost(@RequestParam HashMap<String, String> paramMap) throws Exception{

    	String resultString		= "";
    	try {
    		if( paramMap != null ){

	    	String url 			= String.valueOf( paramMap.get( "url" ) );
	    	String port 		= String.valueOf( paramMap.get( "port" ) );
	    	String method 		= String.valueOf( paramMap.get( "method" ) );
	    	String type 		= String.valueOf( paramMap.get( "type" ) );
	        	URL restUrl = new URL(url);
	        	HttpURLConnection urlcon = (HttpURLConnection)restUrl.openConnection(); 

	        	urlcon.setDoOutput(true);
	        	urlcon.setDoInput(true);
	        	urlcon.setRequestMethod( method.toUpperCase() ); 
	        	urlcon.setRequestProperty("Content-Type", "application/json");

	        	StringBuffer requestParamString = new StringBuffer();
	        	requestParamString.append("{");
	        	
	        	boolean isFirst = true;
	        	for(String key : paramMap.keySet()){
	        		if( !"url".equals( key ) && !"port".equals( key ) && !"method".equals( key ) && !"type".equals( key ) ){
	        			if( !isFirst ){
	        				requestParamString.append(",");
	        			}else{
	        				isFirst = false;
	        			}
	        			
	        			requestParamString.append("\""+key +"\":\""+paramMap.get(key).toString()+"\"");
	        		}
	        	}
	        	requestParamString.append("}");
	        	
	        	String restParam = requestParamString.toString().replaceAll(System.getProperty( "line.separator" ), "");
	        	
	        	logger.info( "### restParam : " + restParam );

	        	OutputStream opstrm = urlcon.getOutputStream();
	        	opstrm.write(restParam.getBytes());
	        	opstrm.flush();
	        	opstrm.close();

	        	
	        	
	        	InputStream in = urlcon.getInputStream();
	        	int resCode = urlcon.getResponseCode(); 
	        	
	        	logger.info( "### resCode : " + resCode );
	        	
	        	StringBuffer sb = new StringBuffer();
	        	Scanner scan = new Scanner( in );
	        	int line = 1;
	        	//sb.append("\"");
	        	while( scan.hasNext() ){
	        		sb.append(scan.nextLine());
	        	}
	        	scan.close();
	        	//sb.append("\"");

	        	Gson gson = new Gson();
	            resultString = gson.toJson(sb.toString());
	            
	            logger.info( "###@@@ resultString : " + resultString );
	            
	            
	            if( resultString != null && !"".equals( resultString ) ){
	            	
	            	
//	            	String ns_ins_id = jsonPropertyToString( resultString, "ns_ins_id" );
//	            	
//	            	logger.info( "#############################################");
//	            	logger.info( "###@@@ ns_ins_id : " + ns_ins_id );
//	            	logger.info( "#############################################");
//	            	JsonParser parse = new JsonParser();
//	            	JsonElement element =  parse.parse( resultString );
//	            	
//	            	if( element != null ){
//	            		String ns_ins_id = element.getAsJsonObject().get("ns_ins_id").getAsString();
//	            	}
//	            	
	            	//DB 업데이트  nsId 필수 있어야함..
            		Map<String, Object> map = new HashMap<String, Object>();
	            	if ("createNS".equals( type ) ){
	            		int nsId 		 = 0; 
	            		int ns_state 	 = 0;
	            		String lc_op_occ = "";
	            		String ns_ins_id = "";
	            		map.put("ns_id"			, nsId );
	            		map.put("ns_ins_id"		, ns_ins_id );
	            		map.put("lc_op_occ"		, lc_op_occ );
	            		map.put("ns_state"		, ns_state );
	        			//service.updateNS( map );
	            		
	            	}else if ("instantiateNS".equals( type ) ){
	            		
	            	}
	            }

    		}
		} catch (Exception e) {
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			sb.append("{");
			sb.append("errorCode:-1,");
			sb.append("errorMessage:"+"\""+e.getMessage()+"\"");
			sb.append("errorLocalMessage:"+"\""+e.getLocalizedMessage()+"\"");
			
			sb.append("}");
			
			logger.info( "### RestCall Error : " + sb.toString() );
			
			Gson gson = new Gson();
            resultString = gson.toJson(sb.toString());
            return resultString;
		}

		return resultString;
    }    
    
    @RequestMapping(value="/saveNsInstance", method=RequestMethod.GET,produces = "application/text; charset=utf8")
    @ResponseBody
    public String saveNsInstance(@RequestParam HashMap<String, String> paramMap) throws Exception{
    	
    	String resultString		= "";
   		Map<String, Object> map = new HashMap<String, Object>();
    	try {
        	if( paramMap != null ){

    	    	int nsId 			= Integer.parseInt( paramMap.get( "nsId" ) );
    	    	String nsdId 		= String.valueOf( paramMap.get( "nsdId" ) );
    	    	String nsInsId 		= String.valueOf( paramMap.get( "nsInsId" ) );
    	    	String lcOpOcc 		= String.valueOf( paramMap.get( "lcOpOcc" ) );
    	    	String flavourId		= String.valueOf( paramMap.get( "flavourId" ) );
    	    	String nsInsLevelId = String.valueOf( paramMap.get( "nsInsLevelId" ) );
    	    	
    	    	
    	    	logger.info( "### [ saveNsInstance ] nsId    : " + nsId );
    	    	logger.info( "### [ saveNsInstance ] nsdId    : " + nsdId );
    	    	logger.info( "### [ saveNsInstance ] nsInsId : " + nsInsId );
    	    	logger.info( "### [ saveNsInstance ] lcOpOcc : " + lcOpOcc );
    	    	logger.info( "### [ saveNsInstance ] flavourId : " + flavourId );
    	    	logger.info( "### [ saveNsInstance ] nsInsLevelId : " + nsInsLevelId );
    	    	
    	    	logger.info( "### [ test 3333!!! ] test : " + ("{\"lcOpOcc\":\""+lcOpOcc+"\"}") );
    	    	
            		map.put("ns_id"				, nsId );
            		map.put("ns_ins_id"			, nsInsId );
            		map.put("lc_op_occ"			, ("{\"lcOpOcc\":\""+lcOpOcc+"\"}") );
//            		map.put("lc_op_occ"			, test );
            		map.put("flavour_id"			, flavourId );
            		map.put("ns_ins_level_id"	, nsInsLevelId );
                	map.put("errorCode"			, 0 );
                	map.put("errorMessage"		, "" );
                	map.put("returnMessage"		, "saveNsInstance" );
        			service.updateNS( map );  
        			
        	        Gson gson = new Gson();
        	        resultString = gson.toJson(map);
        			logger.debug(resultString);
        			
        	}
		} catch (Exception e) {
			e.printStackTrace();
        	map.put("errorCode"		, -1 );
        	map.put("errorMessage"	, "error" );
        	map.put("returnMessage"	, "saveNsInstance" );
	        Gson gson = new Gson();
	        resultString = gson.toJson(map);
			logger.debug(resultString);	
		}

    	return resultString;
    }    
    
    @RequestMapping(value="/orderTracking", method=RequestMethod.GET,produces = "application/text; charset=utf8")
    @ResponseBody
    public String orderTracking(@RequestParam HashMap<String, String> paramMap) throws Exception{

    	String resultString			= "";
    	logger.info( "### paramMap : " + paramMap );
    	if( paramMap != null ){
    		
    		String type 			= paramMap.get( "type" ).toString();
    		String nsInsId 			= paramMap.get( "nsInsId" ).toString();
    	
    		logger.info( "### type : " + type );
    		logger.info( "### nsInsId : " + nsInsId );
    		try {

    			Map<String, Object> map = new HashMap<String, Object>();
    	    	map.put("type"		, type);
    	    	map.put("nsInsId"	, nsInsId);
    	    	
    	    	List<ProvisionNotification> list = service.selectNotificationList( map );
    	    	Map<String, Object> resultMap = new HashMap<String, Object>();

    	    	if( list != null && list.size() > 0 ){
    				ProvisionNotification notiVo = list.get( 0 );
        	    	resultMap.put("nsInsId"			, notiVo.getNs_ins_id());
        	    	resultMap.put("lcOpOcc"			, notiVo.getLc_op_occ());
        	    	resultMap.put("operation"		, notiVo.getOperation());
        	    	resultMap.put("notiType"		, notiVo.getNoti_type());
        	    	resultMap.put("notiStatus"		, notiVo.getNoti_status());
        	    	resultMap.put("operationStatus"	, notiVo.getOperation_status());
        	    	resultMap.put("affectedVnf"		, notiVo.getAffected_vnf());
        	    	resultMap.put("affctedVl"		, notiVo.getAffcted_vl());
        	    	resultMap.put("error"			, notiVo.getError());
        	    	resultMap.put("createAt"		, notiVo.getCreated_at());
        	    	resultMap.put("errorCode"		, 0 );
        	    	resultMap.put("errorMessage"	, "" );
        	    	resultMap.put("returnMessage"	, "complete" );
    			}else{
        	    	resultMap.put("nsInsId"			, nsInsId );
        	    	resultMap.put("lcOpOcc"			, "" );
        	    	resultMap.put("operation"		, -1 );
        	    	resultMap.put("notiType"		, "" );
        	    	resultMap.put("notiStatus"		, -1 );
        	    	resultMap.put("operationStatus"	, "" );
        	    	resultMap.put("affectedVnf"		, "" );
        	    	resultMap.put("affctedVl"		, "" );
        	    	resultMap.put("error"			, "" );
        	    	resultMap.put("createAt"		, "" );    				
        	    	resultMap.put("errorCode"		, 0 );
        	    	resultMap.put("errorMessage"	, "" );
        	    	resultMap.put("returnMessage"	, "tacking" );    				
    			}
    	    	
    			Gson gson = new Gson();
    			resultString = gson.toJson(resultMap);
    			resultString = resultString;
    		} catch (Exception e) {
    			e.printStackTrace();
    			
    			Map<String, Object> resultMap = new HashMap<String, Object>();
    			resultMap.put("errorCode"		, -1 );
    	    	resultMap.put("errorMessage"	, e.getMessage() );
    	    	resultMap.put("returnMessage"	, "error" ); 
    	    	
    			Gson gson = new Gson();
    			resultString = gson.toJson(resultMap);
    			return resultString; 
    		}
    	}
    	logger.info( "### resultString : " + resultString );
    	return resultString;
    }    
}

