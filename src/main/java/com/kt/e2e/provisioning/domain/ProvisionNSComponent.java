package com.kt.e2e.provisioning.domain;

import java.util.Date;

import org.springframework.web.bind.annotation.RequestParam;

public class ProvisionNSComponent {
 
	private String  search_option;
    private String  search_keyword;
    private String  check_plte;
	
    private Integer no;
    private Integer id;
    private String nsd_id;
    private String ns_name;
    private String ns_desc;
    private String ns_ins_id;
    private String flavour_id;
    private String vnf_instance_data;
    private String pnf_id;
    private String vl_id;
    private String vnffgd_id;
    private String sap_id;
    private String nested_ns_ins_id;
    private Integer ns_state;
    private String scale_status;
    private String affinity_rule;
    private String links;
    private String noti_type;
    private String target_nfvo;
    private String creator;
    private Date created_at;
    
    private String version;
    private String nsd_name;
    private Integer ns_type;

	public Integer getNs_type() {
		return ns_type;
	}
	public void setNs_type(Integer ns_type) {
		this.ns_type = ns_type;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getNsd_name() {
		return nsd_name;
	}
	public void setNsd_name(String nsd_name) {
		this.nsd_name = nsd_name;
	}
	public String getSearch_option() {
		return search_option;
	}
	public void setSearch_option(String search_option) {
		this.search_option = search_option;
	}
	public String getSearch_keyword() {
		return search_keyword;
	}
	public void setSearch_keyword(String search_keyword) {
		this.search_keyword = search_keyword;
	}
	public String getCheck_plte() {
		return check_plte;
	}
	public void setCheck_plte(String check_plte) {
		this.check_plte = check_plte;
	}
	
	public Integer getNo() {
		return no;
	}
	public void setNo(Integer no) {
		this.no = no;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNsd_id() {
		return nsd_id;
	}
	public void setNsd_id(String nsd_id) {
		this.nsd_id = nsd_id;
	}
	public String getNs_name() {
		return ns_name;
	}
	public void setNs_name(String ns_name) {
		this.ns_name = ns_name;
	}
	public String getNs_desc() {
		return ns_desc;
	}
	public void setNs_desc(String ns_desc) {
		this.ns_desc = ns_desc;
	}
	public String getNs_ins_id() {
		return ns_ins_id;
	}
	public void setNs_ins_id(String ns_ins_id) {
		this.ns_ins_id = ns_ins_id;
	}
	public String getFlavour_id() {
		return flavour_id;
	}
	public void setFlavour_id(String flavour_id) {
		this.flavour_id = flavour_id;
	}
	public String getVnf_instance_data() {
		return vnf_instance_data;
	}
	public void setVnf_instance_data(String vnf_instance_data) {
		this.vnf_instance_data = vnf_instance_data;
	}
	public String getPnf_id() {
		return pnf_id;
	}
	public void setPnf_id(String pnf_id) {
		this.pnf_id = pnf_id;
	}
	public String getVl_id() {
		return vl_id;
	}
	public void setVl_id(String vl_id) {
		this.vl_id = vl_id;
	}
	public String getVnffgd_id() {
		return vnffgd_id;
	}
	public void setVnffgd_id(String vnffgd_id) {
		this.vnffgd_id = vnffgd_id;
	}
	public String getSap_id() {
		return sap_id;
	}
	public void setSap_id(String sap_id) {
		this.sap_id = sap_id;
	}
	public String getNested_ns_ins_id() {
		return nested_ns_ins_id;
	}
	public void setNested_ns_ins_id(String nested_ns_ins_id) {
		this.nested_ns_ins_id = nested_ns_ins_id;
	}
	public Integer getNs_state() {
		return ns_state;
	}
	public void setNs_state(Integer ns_state) {
		this.ns_state = ns_state;
	}
	public String getScale_status() {
		return scale_status;
	}
	public void setScale_status(String scale_status) {
		this.scale_status = scale_status;
	}
	public String getAffinity_rule() {
		return affinity_rule;
	}
	public void setAffinity_rule(String affinity_rule) {
		this.affinity_rule = affinity_rule;
	}
	public String getLinks() {
		return links;
	}
	public void setLinks(String links) {
		this.links = links;
	}
	public String getNoti_type() {
		return noti_type;
	}
	public void setNoti_type(String noti_type) {
		this.noti_type = noti_type;
	}
	public String getTarget_nfvo() {
		return target_nfvo;
	}
	public void setTarget_nfvo(String target_nfvo) {
		this.target_nfvo = target_nfvo;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

    
}

