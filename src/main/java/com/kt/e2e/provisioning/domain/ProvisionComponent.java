package com.kt.e2e.provisioning.domain;

import java.util.Date;

import org.springframework.web.bind.annotation.RequestParam;

public class ProvisionComponent {
 
	private String  search_option;
    private String  search_keyword;
    private String  check_plte;
	
    private String no;
    private Integer id;
    private String nsd_id;
    private String ns_name;
    private String ns_desc;
    private String ns_ins_id;
    private String flavour_id;
    private String vnf_instance_id;
    private String pnf_id;
    private String vl_id;
    private String vnffgd_id;
    private String sap_id;
    private String nested_ns_ins_id;
    private Integer ns_state;
    private String scale_status;
    private String affinity_rule;
    private String links;
    private String noti_type;
    private String target_nfvo;
    private String creator;
    private String created_at;


    //NSD
    private String nsd_name;
    private String nsd_desc;
    private String designer;
    private String version;
    private String nsd_invariant_id;
    private String nested_nsd_id;
    private String vnfd_id;
    private String pnfd_id;
    private String sapd_id;
    private String vld_id;
    //private String vnffgd_id;
    private String mon_inf_id;
    private String auto_scale_rule;
    private String lcms;
    private String nsdf_id;
    private String scaling_aspect_id;
    private String affinity_group_id;
    private String scale_level_id;
    private String default_ns_instance_level_id;
    private String ns_prof_id;
    private String dependencies;
    private String security_id;
    private String nsd_state;
    
    private String fail_reason;
    private String nsd_usage_state;
    private String user_defined;
    private String ns_type;
    
    private String nsdf_name;
    private String flavour_key;
    private String vnf_prof;
    private String pnf_prof;
    private String vl_prof;
    private int currval;
    
	private Date on_boarding_time;
	private Date create_time;

    public String getNs_desc() {
		return ns_desc;
	}
	public void setNs_desc(String ns_desc) {
		this.ns_desc = ns_desc;
	}
	public String getFlavour_id() {
		return flavour_id;
	}
	public void setFlavour_id(String flavour_id) {
		this.flavour_id = flavour_id;
	}
	public String getVnf_instance_id() {
		return vnf_instance_id;
	}
	public void setVnf_instance_id(String vnf_instance_id) {
		this.vnf_instance_id = vnf_instance_id;
	}
	public String getPnf_id() {
		return pnf_id;
	}
	public void setPnf_id(String pnf_id) {
		this.pnf_id = pnf_id;
	}
	public String getVl_id() {
		return vl_id;
	}
	public void setVl_id(String vl_id) {
		this.vl_id = vl_id;
	}
	public String getSap_id() {
		return sap_id;
	}
	public void setSap_id(String sap_id) {
		this.sap_id = sap_id;
	}
	public String getNested_ns_ins_id() {
		return nested_ns_ins_id;
	}
	public void setNested_ns_ins_id(String nested_ns_ins_id) {
		this.nested_ns_ins_id = nested_ns_ins_id;
	}

	public Integer getNs_state() {
		return ns_state;
	}
	public void setNs_state(Integer ns_state) {
		this.ns_state = ns_state;
	}
	public String getScale_status() {
		return scale_status;
	}
	public void setScale_status(String scale_status) {
		this.scale_status = scale_status;
	}
	public String getAffinity_rule() {
		return affinity_rule;
	}
	public void setAffinity_rule(String affinity_rule) {
		this.affinity_rule = affinity_rule;
	}
	public String getLinks() {
		return links;
	}
	public void setLinks(String links) {
		this.links = links;
	}
	public String getNoti_type() {
		return noti_type;
	}
	public void setNoti_type(String noti_type) {
		this.noti_type = noti_type;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public int getCurrval() {
		return currval;
	}
	public void setCurrval(int currval) {
		this.currval = currval;
	}
	public String getNs_name() {
		return ns_name;
	}
	public void setNs_name(String ns_name) {
		this.ns_name = ns_name;
	}
	public String getNsdf_name() {
		return nsdf_name;
	}
	public void setNsdf_name(String nsdf_name) {
		this.nsdf_name = nsdf_name;
	}
	public String getFlavour_key() {
		return flavour_key;
	}
	public void setFlavour_key(String flavour_key) {
		this.flavour_key = flavour_key;
	}
	public String getVnf_prof() {
		return vnf_prof;
	}
	public void setVnf_prof(String vnf_prof) {
		this.vnf_prof = vnf_prof;
	}
	public String getPnf_prof() {
		return pnf_prof;
	}
	public void setPnf_prof(String pnf_prof) {
		this.pnf_prof = pnf_prof;
	}
	public String getVl_prof() {
		return vl_prof;
	}
	public void setVl_prof(String vl_prof) {
		this.vl_prof = vl_prof;
	}
	public String getDesigner() {
		return designer;
	}
	public void setDesigner(String designer) {
		this.designer = designer;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getNsd_invariant_id() {
		return nsd_invariant_id;
	}
	public void setNsd_invariant_id(String nsd_invariant_id) {
		this.nsd_invariant_id = nsd_invariant_id;
	}
	public String getNested_nsd_id() {
		return nested_nsd_id;
	}
	public void setNested_nsd_id(String nested_nsd_id) {
		this.nested_nsd_id = nested_nsd_id;
	}
	public String getVnfd_id() {
		return vnfd_id;
	}
	public void setVnfd_id(String vnfd_id) {
		this.vnfd_id = vnfd_id;
	}
	public String getPnfd_id() {
		return pnfd_id;
	}
	public void setPnfd_id(String pnfd_id) {
		this.pnfd_id = pnfd_id;
	}
	public String getSapd_id() {
		return sapd_id;
	}
	public void setSapd_id(String sapd_id) {
		this.sapd_id = sapd_id;
	}
	public String getVld_id() {
		return vld_id;
	}
	public void setVld_id(String vld_id) {
		this.vld_id = vld_id;
	}
	public String getVnffgd_id() {
		return vnffgd_id;
	}
	public void setVnffgd_id(String vnffgd_id) {
		this.vnffgd_id = vnffgd_id;
	}
	public String getMon_inf_id() {
		return mon_inf_id;
	}
	public void setMon_inf_id(String mon_inf_id) {
		this.mon_inf_id = mon_inf_id;
	}
	public String getAuto_scale_rule() {
		return auto_scale_rule;
	}
	public void setAuto_scale_rule(String auto_scale_rule) {
		this.auto_scale_rule = auto_scale_rule;
	}
	public String getLcms() {
		return lcms;
	}
	public void setLcms(String lcms) {
		this.lcms = lcms;
	}
	public String getNsdf_id() {
		return nsdf_id;
	}
	public void setNsdf_id(String nsdf_id) {
		this.nsdf_id = nsdf_id;
	}
	public String getScaling_aspect_id() {
		return scaling_aspect_id;
	}
	public void setScaling_aspect_id(String scaling_aspect_id) {
		this.scaling_aspect_id = scaling_aspect_id;
	}
	public String getAffinity_group_id() {
		return affinity_group_id;
	}
	public void setAffinity_group_id(String affinity_group_id) {
		this.affinity_group_id = affinity_group_id;
	}
	public String getScale_level_id() {
		return scale_level_id;
	}
	public void setScale_level_id(String scale_level_id) {
		this.scale_level_id = scale_level_id;
	}
	public String getDefault_ns_instance_level_id() {
		return default_ns_instance_level_id;
	}
	public void setDefault_ns_instance_level_id(String default_ns_instance_level_id) {
		this.default_ns_instance_level_id = default_ns_instance_level_id;
	}
	public String getNs_prof_id() {
		return ns_prof_id;
	}
	public void setNs_prof_id(String ns_prof_id) {
		this.ns_prof_id = ns_prof_id;
	}
	public String getDependencies() {
		return dependencies;
	}
	public void setDependencies(String dependencies) {
		this.dependencies = dependencies;
	}
	public String getSecurity_id() {
		return security_id;
	}
	public void setSecurity_id(String security_id) {
		this.security_id = security_id;
	}
	public String getNsd_state() {
		return nsd_state;
	}
	public void setNsd_state(String nsd_state) {
		this.nsd_state = nsd_state;
	}
	public String getFail_reason() {
		return fail_reason;
	}
	public void setFail_reason(String fail_reason) {
		this.fail_reason = fail_reason;
	}
	public String getNsd_usage_state() {
		return nsd_usage_state;
	}
	public void setNsd_usage_state(String nsd_usage_state) {
		this.nsd_usage_state = nsd_usage_state;
	}
	public String getUser_defined() {
		return user_defined;
	}
	public void setUser_defined(String user_defined) {
		this.user_defined = user_defined;
	}
	public String getNs_type() {
		return ns_type;
	}
	public void setNs_type(String ns_type) {
		this.ns_type = ns_type;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNsd_desc() {
		return nsd_desc;
	}
	public void setNsd_desc(String nsd_desc) {
		this.nsd_desc = nsd_desc;
	}
	public String getNs_ins_id() {
		return ns_ins_id;
	}
	public void setNs_ins_id(String ns_ins_id) {
		this.ns_ins_id = ns_ins_id;
	}
	public String getTarget_nfvo() {
		return target_nfvo;
	}
	public void setTarget_nfvo(String target_nfvo) {
		this.target_nfvo = target_nfvo;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	
	public String getSearch_option() {
		return search_option;
	}
	public void setSearch_option(String search_option) {
		this.search_option = search_option;
	}
	public String getSearch_keyword() {
		return search_keyword;
	}
	public void setSearch_keyword(String search_keyword) {
		this.search_keyword = search_keyword;
	}
	public String getCheck_plte() {
		return check_plte;
	}
	public void setCheck_plte(String check_plte) {
		this.check_plte = check_plte;
	}
	public String getNsd_id() {
		return nsd_id;
	}
	public void setNsd_id(String nsd_id) {
		this.nsd_id = nsd_id;
	}
	public String getNsd_name() {
		return nsd_name;
	}
	public void setNsd_name(String nsd_name) {
		this.nsd_name = nsd_name;
	}
	public Date getOn_boarding_time() {
		return on_boarding_time;
	}
	public void setOn_boarding_time(Date on_boarding_time) {
		this.on_boarding_time = on_boarding_time;
	}
	public Date getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}
	private String  searchOption;
    private String  searchKeyword;
    private String  check4g;
    private String  check5g;
    private String  checkPlte;
    private int 	curPage;
	
    private String nsdId;
    private String nsdName;
    private String value;
    private Date onBoardingTime;
	private Date createTime;
    
    public Date getOnBoardingTime() {
		return onBoardingTime;
	}
	public void setOnBoardingTime(Date onBoardingTime) {
		this.onBoardingTime = onBoardingTime;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getSearchOption() {
		return searchOption;
	}
	public void setSearchOption(String searchOption) {
		this.searchOption = searchOption;
	}
	public String getSearchKeyword() {
		return searchKeyword;
	}
	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}
	public String getCheck4g() {
		return check4g;
	}
	public void setCheck4g(String check4g) {
		this.check4g = check4g;
	}
	public String getCheck5g() {
		return check5g;
	}
	public void setCheck5g(String check5g) {
		this.check5g = check5g;
	}
	public String getCheckPlte() {
		return checkPlte;
	}
	public void setCheckPlte(String checkPlte) {
		this.checkPlte = checkPlte;
	}
	public int getCurPage() {
		return curPage;
	}
	public void setCurPage(int curPage) {
		this.curPage = curPage;
	}
	public String getNsdId() {
		return nsdId;
	}
	public void setNsdId(String nsdId) {
		this.nsdId = nsdId;
	}
	public String getNsdName() {
		return nsdName;
	}
	public void setNsdName(String nsdName) {
		this.nsdName = nsdName;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
    
}

