package com.kt.e2e.provisioning.domain;

import java.util.Date;

import org.springframework.web.bind.annotation.RequestParam;

public class ProvisionNotification {
 
    private String no;
    private Integer id;
    private String ns_ins_id;
    private String lc_op_occ;
    private Integer operation;
    private String noti_type;
    private Integer noti_status;
    private Integer operation_status;
    private String affected_vnf;
    private String affcted_vl;
    private String error;
    private Date created_at;
    private Date create_time;
    
	public Date getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNs_ins_id() {
		return ns_ins_id;
	}
	public void setNs_ins_id(String ns_ins_id) {
		this.ns_ins_id = ns_ins_id;
	}
	public String getLc_op_occ() {
		return lc_op_occ;
	}
	public void setLc_op_occ(String lc_op_occ) {
		this.lc_op_occ = lc_op_occ;
	}
	public Integer getOperation() {
		return operation;
	}
	public void setOperation(Integer operation) {
		this.operation = operation;
	}
	public String getNoti_type() {
		return noti_type;
	}
	public void setNoti_type(String noti_type) {
		this.noti_type = noti_type;
	}
	public Integer getNoti_status() {
		return noti_status;
	}
	public void setNoti_status(Integer noti_status) {
		this.noti_status = noti_status;
	}
	public Integer getOperation_status() {
		return operation_status;
	}
	public void setOperation_status(Integer operation_status) {
		this.operation_status = operation_status;
	}
	public String getAffected_vnf() {
		return affected_vnf;
	}
	public void setAffected_vnf(String affected_vnf) {
		this.affected_vnf = affected_vnf;
	}
	public String getAffcted_vl() {
		return affcted_vl;
	}
	public void setAffcted_vl(String affcted_vl) {
		this.affcted_vl = affcted_vl;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
    
}

