package com.kt.e2e.provisioning.domain;

import java.util.Date;

import org.springframework.web.bind.annotation.RequestParam;

public class ProvisionVNFDComponent {
	String vnfd_name;
	String vnfd_id;
	String vnfd_ver;
	String vnfd_deisgner;
	Integer ns_type;
	String dev_type;
	String vdu;
	String vcd;
	String vsd;
	String df;
	String conf_para;
	public String getVnfd_name() {
		return vnfd_name;
	}
	public void setVnfd_name(String vnfd_name) {
		this.vnfd_name = vnfd_name;
	}
	public String getVnfd_id() {
		return vnfd_id;
	}
	public void setVnfd_id(String vnfd_id) {
		this.vnfd_id = vnfd_id;
	}
	public String getVnfd_ver() {
		return vnfd_ver;
	}
	public void setVnfd_ver(String vnfd_ver) {
		this.vnfd_ver = vnfd_ver;
	}
	public String getVnfd_deisgner() {
		return vnfd_deisgner;
	}
	public void setVnfd_deisgner(String vnfd_deisgner) {
		this.vnfd_deisgner = vnfd_deisgner;
	}
	public Integer getNs_type() {
		return ns_type;
	}
	public void setNs_type(Integer ns_type) {
		this.ns_type = ns_type;
	}
	public String getDev_type() {
		return dev_type;
	}
	public void setDev_type(String dev_type) {
		this.dev_type = dev_type;
	}
	public String getVdu() {
		return vdu;
	}
	public void setVdu(String vdu) {
		this.vdu = vdu;
	}
	public String getVcd() {
		return vcd;
	}
	public void setVcd(String vcd) {
		this.vcd = vcd;
	}
	public String getVsd() {
		return vsd;
	}
	public void setVsd(String vsd) {
		this.vsd = vsd;
	}
	public String getDf() {
		return df;
	}
	public void setDf(String df) {
		this.df = df;
	}
	public String getConf_para() {
		return conf_para;
	}
	public void setConf_para(String conf_para) {
		this.conf_para = conf_para;
	}
	
}

