package com.kt.e2e.provisioning.dao;
 
 
import java.util.List;
import java.util.Map;

import com.kt.e2e.provisioning.domain.ProvisionComponent;
import com.kt.e2e.provisioning.domain.ProvisionNSComponent;
import com.kt.e2e.provisioning.domain.ProvisionNSDComponent;
import com.kt.e2e.provisioning.domain.ProvisionNotification;
import com.kt.e2e.provisioning.domain.ProvisionVNFDComponent;
 
public interface ProvisionMapper {

    public List<ProvisionNotification> selectNotificationList( Map<String, Object> map )throws Exception;

    public int selectNSCount( Map<String, Object> map )throws Exception;
    
    public List<ProvisionNSComponent> selectNSList( Map<String, Object> map )throws Exception;

    public int selectNSDCount( Map<String, Object> map )throws Exception;
    
    public List<ProvisionComponent> selectNSDList( Map<String, Object> map )throws Exception;

    public List<ProvisionNSComponent> selectNSData( Map<String, Object> map )throws Exception;

    public List<ProvisionNSDComponent> selectNSDData( Map<String, Object> map )throws Exception;

    public List<ProvisionVNFDComponent> selectVNFDList( List<String> list )throws Exception;

    public List<ProvisionNSDComponent> selectNSDFList( Map<String, Object> map )throws Exception;

    public List<ProvisionComponent> selectNSComponentList( Map<String, Object> map )throws Exception;
    
    public int insertNS( Map<String, Object> map )throws Exception;

    public int updateNS( Map<String, Object> map )throws Exception;

    public int selectNSId( Map<String, Object> map )throws Exception;
}

