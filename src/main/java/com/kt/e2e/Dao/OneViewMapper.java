package com.kt.e2e.Dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.kt.e2e.Domain.CloudComponent;
import com.kt.e2e.Domain.ConnectionComponent;
import com.kt.e2e.Domain.InvNfvoComponent;
import com.kt.e2e.Domain.NodeComponent;
import com.kt.e2e.Domain.NsdComponent;
import com.kt.e2e.Domain.NsdmNsdInfoComponent;
import com.kt.e2e.Domain.OneViewComponent;

@Component
public interface OneViewMapper {
	int insertNodeComponent(NodeComponent node);             // node 생성
	int insertConnectionComponent(ConnectionComponent conn); // conn 생성
	int insertCloudComponent(CloudComponent cloud);          // cloud 생성
	
	int updateNodeComponent(NodeComponent node);             // node 수정
	int updateConnectionComponent(ConnectionComponent conn); // conn 수정
	int updateCloudComponent(CloudComponent cloud);          // cloud 수정
	
	List<NodeComponent> getNodeComponentsWithKey(@Param("key") String key);             // node 조회
	List<ConnectionComponent> getConnectionComponentsWithKey(@Param("key") String key); // conn 조회
	List<CloudComponent> getCloudComponentsWithKey(@Param("key") String key);           // cloud 조회

	int deleteNodeComponent(@Param("key") String key);       // node 삭제
	int deleteConnectionComponent(@Param("key") String key); // conn 삭제
	int deleteCloudComponent(@Param("key") String key);      // cloud 삭제
		
	
	public List<HashMap<String, Object>> getOrderNum(String nsdType); // 앞, 뒤 관계 조회
	
	int getOneViewListCount(Map<String, Object> map); 										// 디자인 목록 조회 (Count)
	int getOneViewTypeCount(OneViewComponent oneView); 										// 디자인 목록 조회 (타입별 Count)
	public List<OneViewComponent> getOneViewList(Map<String, Object> map)throws Exception; //디자인 목록 조회
	
	public void insertNsd(Map<String, Object> map)throws Exception; // NS Descriptor 추가
	public void updateNsd(Map<String, Object> map)throws Exception; // NS Descriptor 수정
	
	public void insertNsdPlte(Map<String, Object> map)throws Exception; // NS Descriptor P-LTE 추가
	public void updateNsdPlte(Map<String, Object> map)throws Exception; // NS Descriptor P-LTE 수정
	
	public List<NsdComponent> getNsd(Map<String, Object> map)throws Exception; // NS Descriptor(General Info) 조회
	int getNsdDuplication(String nsd_id); // NS Descriptor 중복체크

	public List<NsdmNsdInfoComponent> getNsdmnsdInfo(Map<String, Object> map)throws Exception; // Onboard 결좌 조회
	int insertNsdmNsdInfo(Map<String, Object> map)throws Exception; // Onboard 추가 
	
	
	public List<NsdComponent> getNsdmNsdf(Map<String, Object> map)throws Exception; // NSD의 Flavour 조회
	int getNsdmNsdfDuplication(String nsdf_id); // NSD의 Flavour 중복체크
	public void insertNsdmNsdf(Map<String, Object> map)throws Exception; // NSD의 Flavour 정보 추가
	public void updateNsdmNsdf(Map<String, Object> map)throws Exception; // NSD의 Flavour 정보 수정
	public void deleteNsdmNsdf(Map<String, Object> map)throws Exception; // NSD의 Flavour 정보 삭제

	public List<NsdComponent> getNsdmProfile(Map<String, Object> map)throws Exception; // NSD의 profile 조회 조회
	public void insertNsdmProfile(Map<String, Object> map)throws Exception; // NSD의 profile 조회 추가
	public void updateNsdmProfile(Map<String, Object> map)throws Exception; // NSD의 profile 조회 수정

	public List<InvNfvoComponent> getInvNfvo() throws Exception; // Target System 조회

    public void deleteNsd(String nsdId)throws Exception;    // nsd 삭제
}
