package com.kt.e2e.Dao;
 
 
import java.util.List;
import java.util.Map;

import com.kt.e2e.Domain.EquipCmdBasComponent;
 
public interface EquipCmdBasMapper {
 
    //장비기본명령어작성
    public void equipCmdBasInsert(EquipCmdBasComponent vo)throws Exception;
    
    //장비기본명령어목록(ROWNUM방식)
    public List<EquipCmdBasComponent> equipCmdBasList(Map<String, Object> map)throws Exception;
    
    //장비기본명령어목록(OFFSET방식)
    public List<EquipCmdBasComponent> equipCmdBasListOffset(Map<String, Object> map)throws Exception;
    
    //장비기본명령어보기
    public EquipCmdBasComponent equipCmdBasView(EquipCmdBasComponent vo)throws Exception;

    //장비기본명령어콤보조회
    public List<EquipCmdBasComponent> equipCmdBasComboSelect(EquipCmdBasComponent vo)throws Exception;
    
    //장비기본명령어수정
    public void equipCmdBasUpdate(EquipCmdBasComponent vo)throws Exception;
    
    //장비기본명령어삭제
    public void equipCmdBasDelete(int bno)throws Exception;
    
    //목록총 갯수
    public int equipCmdBasCount(Map<String, Object> map)throws Exception;
}

