package com.kt.e2e.Dao;
 
 
import java.util.List;
import java.util.Map;

import com.kt.e2e.Domain.UserComponent;
 
public interface UserMapper {
    
    //글목록(ROWNUM방식)
    public List<UserComponent> userList(Map<String, Object> map)throws Exception;
    
    //글목록(OFFSET방식)
    public List<UserComponent> userListOffset(Map<String, Object> map)throws Exception;
    
    //목록총 갯수
    public int userCount(Map<String, Object> map)throws Exception;

}

