package com.kt.e2e.Dao;
 
 
import java.util.List;
import java.util.Map;

import com.kt.e2e.Domain.BoardComponent;
 
public interface BoardMapper {
 
    //글작성
    public void boardInsert(BoardComponent vo)throws Exception;
    
    //글목록(ROWNUM방식)
    public List<BoardComponent> boardList(Map<String, Object> map)throws Exception;
    
    //글목록(OFFSET방식)
    public List<BoardComponent> boardListOffset(Map<String, Object> map)throws Exception;
    
    
    //글보기
    public BoardComponent boardView(int bno)throws Exception;
    
    //조회수 증가
    public void hitPlus(int bno)throws Exception;
    
    //글수정
    public void boardUpdate(BoardComponent vo)throws Exception;
    
    //글삭제
    public void boardDelete(int bno)throws Exception;
    
    //목록총 갯수
    public int boardCount(Map<String, Object> map)throws Exception;
}

