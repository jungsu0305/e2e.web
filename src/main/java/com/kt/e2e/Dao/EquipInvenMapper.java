package com.kt.e2e.Dao;
 
 
import java.util.List;
import java.util.Map;

import com.kt.e2e.Domain.EquipInvenComponent;
 
public interface EquipInvenMapper {
 
    //장비인벤토리작성
    public void equipInvenInsert(EquipInvenComponent vo)throws Exception;
    
    //장비인벤토리목록(ROWNUM방식)
    public List<EquipInvenComponent> equipInvenList(Map<String, Object> map)throws Exception;
    
    //장비인벤토리목록(OFFSET방식)
    public List<EquipInvenComponent> equipInvenListOffset(Map<String, Object> map)throws Exception;
    
    
    //장비인벤토리보기
    public EquipInvenComponent equipInvenView(String equip_id)throws Exception;
    
    //장비인벤토리수정
    public void equipInvenUpdate(EquipInvenComponent vo)throws Exception;
    
    //장비인벤토리삭제
    public void equipInvenDelete(int bno)throws Exception;
    
    //장비인벤토리 목록총 갯수
    public int equipInvenCount(Map<String, Object> map)throws Exception;
    
    //EMS 장비관리정보 보기
    public EquipInvenComponent plteConnInfoView(EquipInvenComponent vo)throws Exception;
    
    //EMS 장비관리정보 머지
    public void plteConnInfoMerge(EquipInvenComponent vo)throws Exception;
    
    //EMS 장비관리정보 삭제
    public void plteConnInfoDelete(EquipInvenComponent vo)throws Exception;
}

