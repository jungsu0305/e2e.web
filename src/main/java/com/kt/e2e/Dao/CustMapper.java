package com.kt.e2e.Dao;
 
 
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import com.kt.e2e.Domain.CustComponent;
 
public interface CustMapper {
    //번호 가져오기
    public int custGetManageNo()throws Exception;
 
    //고객작성
    public void custInsert(CustComponent vo)throws Exception;
    
    //고객목록(ROWNUM방식)
    public List<CustComponent> custList(Map<String, Object> map)throws Exception;
    
    //고객목록(OFFSET방식)
    public List<CustComponent> custListOffset(Map<String, Object> map)throws Exception;
    
    //고객사 lte_id,apn조회
    public List<CustComponent> getLteidApnJsondata(Map<String, Object> map)throws Exception;
    
    
    //고객보기
    public CustComponent custView(int bno)throws Exception;
    
    //조회수 증가
    public void hitPlus(int bno)throws Exception;
    
    //고객수정
    public void custUpdate(CustComponent vo)throws Exception;
    
    //고객삭제
    public void custDelete(int bno)throws Exception;
    
    //목록총 갯수
    public int custCount(Map<String, Object> map)throws Exception;
    
    //고객사 LTE ID,APN중복체크
    public CustComponent custLteIdApnCheck(Map<String, Object> map)throws Exception;
    
    //고객 LTE ID,APN 머지 저장
    public void custMerge(CustComponent vo)throws Exception;
    
    
}

