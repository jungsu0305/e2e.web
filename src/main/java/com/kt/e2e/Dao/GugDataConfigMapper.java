package com.kt.e2e.Dao;
 
 
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import com.kt.e2e.Domain.DataSummaryComponent;
import com.kt.e2e.Domain.GugDataConfigComponent;
import com.kt.e2e.Domain.GugDataConfigDetSelComponent;
 
public interface GugDataConfigMapper {
 
	
    //번호 가져오기
    public BigInteger gugDataConfigGetManageNo()throws Exception;
    
    
    //이력번호 가져오기
    public int gugDataConfigGetHistoryNo(GugDataConfigComponent vo)throws Exception;
    
    
    //국데이터작성
    public void gugDataConfigInsert(GugDataConfigComponent vo)throws Exception;
    
    //국데이터작성 MME 디테일
    public void gugDataConfigMmeDetInsert(GugDataConfigDetSelComponent vo)throws Exception;
    
    //국데이터작성 EPCC 디테일
    public void gugDataConfigEpccDetInsert(GugDataConfigDetSelComponent vo)throws Exception;
    
    //국데이터작성 PGW 
    public void gugDataConfigPgwInsert(GugDataConfigDetSelComponent vo)throws Exception;
    
    
    //국데이타 목록(OFFSET방식)
    public List<GugDataConfigComponent> gugDataConfigListOffset(Map<String, Object> map)throws Exception;
 
  
    
    //국데이터이력보기
    public List<GugDataConfigComponent> gugDataConfigHisList(Map<String, Object> map)throws Exception;
    
    //하나의 국데이타 마스터 보기
    public GugDataConfigComponent gugDataConfigView(Map<String, Object> map)throws Exception;

    public List<GugDataConfigDetSelComponent> gugDataConfigMmeDetView(GugDataConfigComponent vo)throws Exception;
    
    public List<GugDataConfigDetSelComponent> gugDataConfigEpccDetView(GugDataConfigComponent vo)throws Exception;
    
    public List<GugDataConfigDetSelComponent> gugDataConfigPgwView(GugDataConfigComponent vo)throws Exception;
    
    
    //국데이터수정
    public void gugDataConfigUpdate(GugDataConfigComponent vo)throws Exception;
    
    //국데이터 선택 실행명령어 수정 
    public void mergeSelectRunCommand(GugDataConfigComponent vo)throws Exception;
    
    //국데이터 선택 실행명령어 목록
    public List<GugDataConfigComponent> commandSelManageList(Map<String, Object> map)throws Exception;
    
    // 국데이터복사,국데이터국데이터 상태 삭제 등록 및
    public void gugDataConfigInsertCopy(GugDataConfigComponent vo)throws Exception;
    
    //국데이터 관련 실제 데이터 삭제
    public void gugDataConfigDelete(BigInteger gug_data_manage_no)throws Exception;
    
    //국데이터 선택 실행명령어 삭제 
    public void commandSelManageDelete(Map<String, Object> map)throws Exception;
    
    //목록총 갯수
    public int gugDataConfigCount(Map<String, Object> map)throws Exception;
    
    //하나의 국데이타 이력목록총 갯수
    public int gugDataConfigHisCount(Map<String, Object> map)throws Exception;
    
    
    //국데이타 상태 카운트 요약화 조회
    public List<GugDataConfigComponent> gugDataConfigStatusCount()throws Exception;
    
    //데이타 요약화 저장
    public void gugDataConfigSummaryUpdate(Map<String, Object> map)throws Exception;
    
    //데이타 요약화 조회
    public DataSummaryComponent gugDataConfigSummarySelect(int summary_repr_no)throws Exception;
    
}

