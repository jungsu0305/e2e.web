package com.kt.e2e.Dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.kt.e2e.Domain.CloudComponent;
import com.kt.e2e.Domain.ConnectionComponent;
import com.kt.e2e.Domain.NodeComponent;

@Component
public interface PlteTestMapper {
	int insertNodeComponent(NodeComponent node);
	int insertConnectionComponent(ConnectionComponent conn);
	int insertCloudComponent(CloudComponent cloud);
	
	int setNodeComponent(NodeComponent node);
	int setConnectionComponent(ConnectionComponent conn);
	int setCloudComponent(CloudComponent cloud);
	
	List<NodeComponent> getNodeComponentsWithKey(@Param("key") String key);
	List<CloudComponent> getCloudComponentsWithKey(@Param("key") String key);
	List<ConnectionComponent> getConnectionComponentsWithKey(@Param("key") String key);
	
	int removeNodeComponent(@Param("key") String key);
	int removeCloudComponent(@Param("key") String key);
	int removeConnectionComponent(@Param("key") String key);
}
