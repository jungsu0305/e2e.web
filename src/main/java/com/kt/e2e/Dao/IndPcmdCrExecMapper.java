package com.kt.e2e.Dao;
 
 
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import com.kt.e2e.Domain.DataSummaryComponent;
import com.kt.e2e.Domain.IndPcmdCrExecComponent;
 
public interface IndPcmdCrExecMapper {
 
	
    //번호 가져오기
    public BigInteger indPcmdCrExecGetCreateNo()throws Exception;
    
    //실행이력번호 가져오기
    public int indPcmdCrExecGetHistoryNo(IndPcmdCrExecComponent vo)throws Exception;
    
    
    //명령어실행 이력작성
    public void indPcmdCrExecHistoryInsert(IndPcmdCrExecComponent vo)throws Exception;
    
    //명령어실행 실행 상태값 저장
    public void indPcmdCrExecHistoryUpdate(IndPcmdCrExecComponent vo)throws Exception;
    
    //명령어생성작성
    public void indPcmdCrExecInsert(IndPcmdCrExecComponent vo)throws Exception;
    
    
    //국데이타 목록(OFFSET방식)
    public List<IndPcmdCrExecComponent> indPcmdCrExecListOffset(Map<String, Object> map)throws Exception;
 
    
    
    //명령어생성이력보기
    public List<IndPcmdCrExecComponent> indPcmdCrExecHisList(Map<String, Object> map)throws Exception;
    
    //하나의 국데이타 마스터 보기
    public IndPcmdCrExecComponent indPcmdCrExecView(BigInteger gug_data_manage_no)throws Exception;

    
    //명령어생성수정
    public void indPcmdCrExecUpdate(IndPcmdCrExecComponent vo)throws Exception;
    
    
    
    //명령어생성복사,명령어생성명령어생성 상태 삭제 등록
    public void indPcmdCrExecInsertCopy(IndPcmdCrExecComponent vo)throws Exception;
    
    //명령어생성 관련 실제 데이터 삭제
    public void indPcmdCrExecDelete(BigInteger gug_data_manage_no)throws Exception;
    
    //목록총 갯수
    public int indPcmdCrExecCount(Map<String, Object> map)throws Exception;
    
    //하나의 국데이타 이력목록총 갯수
    public int indPcmdCrExecHisCount(Map<String, Object> map)throws Exception;
    
    
    //국데이타 상태 카운트 요약화 조회
    public List<IndPcmdCrExecComponent> indPcmdCrExecStatusCount()throws Exception;
    
    //데이타 요약화 저장
    public void indPcmdCrExecSummaryUpdate(Map<String, Object> map)throws Exception;
    
    
    //데이타 요약화 조회
    public DataSummaryComponent indPcmdCrExecSummarySelect(int summary_repr_no)throws Exception;
    
}

