package com.kt.e2e.Dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Repository;

import com.kt.e2e.Domain.CloudComponent;
import com.kt.e2e.Domain.ConnectionComponent;
import com.kt.e2e.Domain.NodeComponent;

@Repository("TestDao")
public class TestDao {

	private static final Logger logger = LoggerFactory.getLogger(TestDao.class);
	
	@Autowired
	protected SqlSession sqlSession;
	
	public int methodTest(int id) {
		logger.debug("param check, id="+id);
		int ret = sqlSession.selectOne("testNamespace.testSelectID");
		logger.debug("try2]ret="+ret);
		return id;
	}

	public int insertNodeComponent(NodeComponent node) {
		try {
			sqlSession.insert("testNamespace.insertNodeComponent", node);
			logger.debug("insert success:" + node.toString());
		
		} catch (DuplicateKeyException dke) {
			sqlSession.update("testNamespace.setNodeComponent", node);
			logger.debug("update node:" + node.toString());
			
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.warn("failed in insertNodeComponent:" + ex.getMessage());
			return -1;
		}
		return 0;
	}

	public int insertConnComponent(ConnectionComponent conn) {
		try {
			sqlSession.insert("testNamespace.insertConnectionComponent", conn);
			logger.debug("insert success:" + conn.toString());
			
		} catch (DuplicateKeyException dke) {
			sqlSession.update("testNamespace.setConnectionComponent", conn);
			logger.debug("update connection:" + conn.toString());
			
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.warn("failed in insertConnComponent:" + ex.getMessage());
			return -1;
		}
		return 0;
		
	}

	public int insertCloudComponent(CloudComponent cloud) {
		try {
			sqlSession.insert("testNamespace.insertCloudComponent", cloud);
			logger.debug("insert success:" + cloud.toString());
			
		} catch (DuplicateKeyException dke) {
			sqlSession.update("testNamespace.setCloudComponent", cloud);
			logger.debug("update cloud:" + cloud.toString());
			
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.warn("failed in insertCloudComponent:" + ex.getMessage());
			return -1;
		}
		return 0;
		
	}

	public List<NodeComponent> getNodeComponent(String key) {
		return sqlSession.selectList("testNamespace.getNodeComponentsWithKey", key);
	}
	
	public List<CloudComponent> getCloudComponent(String key) {
		return sqlSession.selectList("testNamespace.getCloudComponentsWithKey", key);
	}
	
	public List<ConnectionComponent> getConnComponent(String key) {
		return sqlSession.selectList("testNamespace.getConnectionComponentsWithKey", key);
	}
	
	public void removeComponents(String key) {
		sqlSession.delete("testNamespace.removeNodeComponent", key);
		sqlSession.delete("testNamespace.removeCloudComponent", key);
		sqlSession.delete("testNamespace.removeConnectionComponent", key);
	}
}
