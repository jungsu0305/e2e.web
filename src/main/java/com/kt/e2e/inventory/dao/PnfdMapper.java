package com.kt.e2e.inventory.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.kt.e2e.inventory.domain.Pnfd;

@Component
public interface PnfdMapper {

	List<Pnfd> getPnfList();
}
