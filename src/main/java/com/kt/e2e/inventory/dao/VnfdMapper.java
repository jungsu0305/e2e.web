package com.kt.e2e.inventory.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.kt.e2e.inventory.domain.Vnfd;

@Component
public interface VnfdMapper {

	List<Vnfd> getList(@Param("nsType") int nsType);
}
