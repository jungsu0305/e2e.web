package com.kt.e2e.inventory.domain;

import org.apache.ibatis.type.Alias;

@Alias("Vnfd")
public class Vnfd extends InventoryDescriptor {

	String id;
//	String name;
	String version;
	String designer;
	Integer nsType = null;
	String devType = null;
	String vdu;
	String vcd;
	String vsd;
	String df;
	String confPara;
	String vnfd_name;
	String vnfd_id;
	//String path;
	//String type; //4G,5G,PNF,Conn,Cloud for editor
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getDesigner() {
		return designer;
	}
	public void setDesigner(String designer) {
		this.designer = designer;
	}
	public Integer getNsType() {
		return nsType;
	}
	public void setNsType(Integer nsType) {
		this.nsType = nsType;
	}
	public String getDevType() {
		return devType;
	}
	public void setDevType(String devType) {
		this.devType = devType;
	}
	public String getVdu() {
		return vdu;
	}
	public void setVdu(String vdu) {
		this.vdu = vdu;
	}
	public String getVcd() {
		return vcd;
	}
	public void setVcd(String vcd) {
		this.vcd = vcd;
	}
	public String getVsd() {
		return vsd;
	}
	public void setVsd(String vsd) {
		this.vsd = vsd;
	}
	public String getDf() {
		return df;
	}
	public void setDf(String df) {
		this.df = df;
	}
	public String getConfPara() {
		return confPara;
	}
	public void setConfPara(String confPara) {
		this.confPara = confPara;
	}
	public String getVnfd_name() {
		return vnfd_name;
	}
	public void setVnfd_name(String vnfd_name) {
		this.vnfd_name = vnfd_name;
	}
	public String getVnfd_id() {
		return vnfd_id;
	}
	public void setVnfd_id(String vnfd_id) {
		this.vnfd_id = vnfd_id;
	}
	@Override
	public String toString() {
		return "Vnfd [id=" + id + ", version=" + version + ", designer=" + designer + ", nsType=" + nsType
				+ ", devType=" + devType + ", vdu=" + vdu + ", vcd=" + vcd + ", vsd=" + vsd + ", df=" + df
				+ ", confPara=" + confPara + ", name=" + name + ", path=" + path + ", type=" + type + "]";
	}
	
}
