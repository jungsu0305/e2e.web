package com.kt.e2e.inventory.domain;

import org.apache.ibatis.type.Alias;

@Alias("Pnfd")
public class Pnfd extends InventoryDescriptor {

	String id;
	//String name;
	String version;
	String designer;
	String devType = null;
	String confPara;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getDesigner() {
		return designer;
	}
	public void setDesigner(String designer) {
		this.designer = designer;
	}
	public String getDevType() {
		return devType;
	}
	public void setDevType(String devType) {
		this.devType = devType;
	}
	public String getConfPara() {
		return confPara;
	}
	public void setConfPara(String confPara) {
		this.confPara = confPara;
	}
	@Override
	public String toString() {
		return "Pnfd [id=" + id + ", version=" + version + ", designer=" + designer + ", devType=" + devType
				+ ", confPara=" + confPara + ", name=" + name + ", path=" + path + ", type=" + type + "]";
	}
}
