package com.kt.e2e.inventory.domain;

public class InventoryDescriptor {

	String name;
	String path;
	String type;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "InventoryDescriptor [name=" + name + ", path=" + path + ", type=" + type + "]";
	}
	
}
