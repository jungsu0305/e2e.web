package com.kt.e2e.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.kt.e2e.Common.util.PropertiesUtil;

public class ConfigLoaderListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent event) {
		ServletContext sc = event.getServletContext();

		String propName = "e2eConfig";

		String e2eWebsocketAddress = PropertiesUtil.get(propName, "e2e.websocket.address");
		sc.setAttribute("e2eWebsocketAddress", e2eWebsocketAddress);
		
		String e2eNsdId = PropertiesUtil.get(propName, "e2e.alarm.nsdId");
		sc.setAttribute("e2eNsdId", e2eNsdId);
		
		String e2eAlarmNodeKey = PropertiesUtil.get(propName, "e2e.alarm.node.key");
		sc.setAttribute("e2eAlarmNodeKey", e2eAlarmNodeKey);
		
		String e2eNsdName = PropertiesUtil.get(propName, "e2e.alarm.nsdName");
		sc.setAttribute("e2eNsdName", e2eNsdName);
		
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {


	}
}
