package com.kt.e2e.Controller;
 
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.kt.e2e.Common.CommonPager;
import com.kt.e2e.Service.CustService;
import com.kt.e2e.Service.GugDataConfigService;
import com.kt.e2e.Domain.CustComponent;
import com.kt.e2e.Domain.GugDataConfigComponent;
 
@Controller
@RequestMapping("/cust")
public class CustController {
 
    
    private static final Logger logger = LoggerFactory.getLogger(CustController.class);
	@Autowired
	private CustService service;
	
	@Autowired
	private GugDataConfigService gdService;

    //고객사 목록
    @RequestMapping(method=RequestMethod.GET)
    public ModelAndView list(@RequestParam(defaultValue="cust_nm") String searchOption,
            @RequestParam(defaultValue="") String keyword,
            @RequestParam(defaultValue="1") int curPage
             ) throws Exception{
    	
        logger.info("searchOption:"+searchOption);
        logger.info("keyword:"+keyword);
        // 검색옵션, 키워드 맵에 저장
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("searchOption", searchOption);
        map.put("keyword", keyword);
        
        //검색옵션에 맞추어 카운트 가져오기
    	int count =service.custCount(map);
    	
    	logger.info("count:"+count);
        // 페이지 나누기 관련 처리
        int pageScale = 10; // 페이지당 게시물 수
        int blockScale = 10;  // 화면당 페이지 수
        CommonPager commonPager = new CommonPager(count, curPage,pageScale,blockScale);
        map.put("start", commonPager.getPageBegin());
        map.put("end", commonPager.getPageEnd());
        map.put("curPage", commonPager.getCurPage());
        map.put("pageScale", commonPager.getPageScale());
//        List<CustVO> list = service.custList(map); //ROWNUM방식
        List<CustComponent> list = service.custListOffset(map); //OFFSET방식

        
        ModelAndView mav = new ModelAndView();
        mav.setViewName("cust/custManage");
        mav.addObject("count", count); 
        mav.addObject("list", list);
        mav.addObject("curPage", curPage);
        mav.addObject("searchOption", searchOption);
        mav.addObject("keyword", keyword);
        mav.addObject("commonPager", commonPager);
        logger.info("mav:"+ mav);
        return mav;

    }
    
    //고객사 목록안에서 정렬(올림차순/내림차순)
	@RequestMapping(value="/jsondata", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
    public String listJsondata(
            @RequestParam(defaultValue="1") int curPage,
            @RequestParam(defaultValue="") String colName,
            @RequestParam(defaultValue="") String ordOption,
            @RequestParam(defaultValue="") String orderKey
             ) throws Exception{
    	

        // 검색옵션, 키워드 맵에 저장
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("orderKey", orderKey);
        
        //검색옵션에 맞추어 카운트 가져오기
    	int count =service.custCount(map);
    	
    	
        // 페이지 나누기 관련 처리
        int pageScale = 10; // 페이지당 게시물 수
        int blockScale = 10;  // 화면당 페이지 수
        CommonPager commonPager = new CommonPager(count, curPage,pageScale,blockScale);
        map.put("start", commonPager.getPageBegin());
        map.put("end", commonPager.getPageEnd());
        map.put("curPage", commonPager.getCurPage());
        map.put("pageScale", commonPager.getPageScale());
        
        //정렬옵션
        map.put("colName", colName);
        map.put("ordOption", ordOption);

        
        logger.debug(orderKey);

 		Gson gson = new Gson();
		List<CustComponent> list =service.custListOffset(map);
		logger.debug(list.toString());
		String s = gson.toJson(list);
		logger.debug(s);
		return s;

    }



    
    //고객사 작성 페이지(GET)    
    @RequestMapping(value="/post",method=RequestMethod.GET)
    public ModelAndView writeForm() throws Exception{
        
        return new ModelAndView("cust/custWrite");
    }
    
    //고객사 DB에 등록(POST)
    @RequestMapping(value="/post",method=RequestMethod.POST)
    public String write(@ModelAttribute("CustComponent") CustComponent cust) throws Exception{
 
    	
    	cust.setRegist_id("admin");
    	cust.setCust_manage_no(service.custGetManageNo());
    	service.custInsert(cust);
        
        //return "redirect://localhost:8080/cust";
    	HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    	logger.info(request.getContextPath()+":"+request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort());
        return "redirect://"+request.getServerName()+":"+request.getServerPort()+"/cust";

    }
    

    //고객사 상세 페이지
    @RequestMapping(value="/{bno}",method=RequestMethod.GET)
    public ModelAndView view(@PathVariable("bno") int bno) throws Exception{
        
        CustComponent cust = service.custView(bno);
        service.hitPlus(bno);
        
        return new ModelAndView("cust/custView","cust",cust);
    }
    
    //고객사 수정 페이지(GET)
    @RequestMapping(value="/post/{bno}", method=RequestMethod.GET)
    public ModelAndView updateForm(@PathVariable("bno") int bno) throws Exception{
        
        CustComponent cust = service.custView(bno);
        
        return new ModelAndView("cust/custUpdate","cust",cust);
    }
    
    ////고객사 수정 페이지(GET-JSON)
	@RequestMapping(value="/jsondata/{bno}", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
    public String getJsondata(@PathVariable("bno") int bno) throws Exception{

 		Gson gson = new Gson();
		CustComponent cust = service.custView(bno);
		logger.debug(cust.toString());
		String s = gson.toJson(cust);
		logger.debug(s);
		return s;
	}
    

    //고객사 수정(PUT)
    @RequestMapping(value="/post/{bno}", method=RequestMethod.PUT)
    public String update(@ModelAttribute("CustComponent") CustComponent cust,@PathVariable("bno") int bno) throws Exception{
    	logger.debug(String.valueOf(bno));
    	cust.setModify_id("admin");
        service.custUpdate(cust);
        
        //return "redirect://localhost:8080/cust";
    	HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    	logger.info(request.getContextPath()+":"+request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort());
        return "redirect://"+request.getServerName()+":"+request.getServerPort()+"/cust";
    }
    
    //고객사 lte_id,apn조회
	@RequestMapping(value="/getLteidApnJsondata/{cust_nm}", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
    public String getLteidApnJsondata(@PathVariable("cust_nm") String cust_nm) throws Exception{
		logger.debug("getLteidApnJsondata");
	   Map<String, Object> map = new HashMap<String, Object>();
	   map.put("cust_nm", cust_nm);
		List<CustComponent> list =service.getLteidApnJsondata(map);
 		Gson gson = new Gson();
		logger.debug(list.toString());
		String s = gson.toJson(list);
		logger.debug(s);
		return s;
	}
    
    //고객사 AJAX 수정(PUT)
	@RequestMapping(value="/ajaxUpdate",method=RequestMethod.POST,produces = "application/text; charset=utf8")
	@ResponseBody
    public String ajaxUpdate(@ModelAttribute("CustComponent") CustComponent cust) throws Exception{
    	logger.debug("ajaxUpdate");
    	
    	String[] cust_conf_no=cust.getCust_conf_no().split(Pattern.quote("|||"));
    	String[] lte_id=cust.getLte_id().split(Pattern.quote("|||"));
    	String[] apn=cust.getApn().split(Pattern.quote("|||"));
    	
    	if(apn!=null){
	        for(int i=0 ;i<apn.length;i++)
	        {

	        	logger.info("Cust_nm="+cust.getCust_nm());
	        	logger.info("cust_conf_no="+cust_conf_no[i]);
	        	logger.info("lte_id="+lte_id[i]);
	        	logger.info("apn="+apn[i]);
	        	CustComponent  ccm=new CustComponent();
	        	ccm.setCust_nm(cust.getCust_nm());
	        	
	        	ccm.setCust_description(cust.getCust_description());
	        	
	        	if(lte_id[i].length() >0)
	        		ccm.setLte_id(lte_id[i]);
	        	else{
	        		continue;
	        	}
	        	
	        	if(apn[i].length() >0)
	        		ccm.setApn(apn[i]);
	        	else{
	        		continue;
	        	}
	        	ccm.setLte_id(lte_id[i]);
	        	ccm.setApn(apn[i]);
	        	if(!cust_conf_no[i].equals("0"))
	        		ccm.setCust_manage_no(Integer.parseInt(cust_conf_no[i]));
	        	else{
	        		ccm.setCust_manage_no(service.custGetManageNo());
	        	}
	        	ccm.setRegist_id("admin");
	        	ccm.setModify_id("admin");
	        	ccm.setIp_type("");
	        	ccm.setService_type("");
	        	ccm.setDed_line_use_yn("");
	        	logger.info(""+ccm.getCust_manage_no());
	            service.custMerge(ccm);
	        }
        }

		return "";
    }
    
    //고객사 삭제(DELETE)
    @RequestMapping(value="/delete/{bno}", method=RequestMethod.DELETE)
    public String delete(@PathVariable("bno") int bno) throws Exception{
    	logger.info("333");
        service.custDelete(bno);
        gdService.gugDataConfigSummaryUpdate();//데이타 요약화
        //return "redirect://localhost:8080/cust";
    	HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    	logger.info(request.getContextPath()+":"+request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort());
        return "redirect://"+request.getServerName()+":"+request.getServerPort()+"/cust";
        
    }
    
    //고객사 선택삭제(DELETE)
    @RequestMapping(value="/delete/select", method=RequestMethod.DELETE)
    public String delete(@ModelAttribute("CustComponent") CustComponent cust) throws Exception{
    	logger.info("dddddddddd");
    	
        int bno=0;

        String[] checkNo=cust.getCheckNo();
        if(checkNo!=null){
	        for(int i=0 ;i<checkNo.length;i++)
	        {
	            logger.info(checkNo[i]);
	        	bno=Integer.valueOf(checkNo[i]);
	            service.custDelete(bno);
	        }
        }

        gdService.gugDataConfigSummaryUpdate();//데이타 요약화
        //return "redirect://localhost:8080/cust";
    	HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    	logger.info(request.getContextPath()+":"+request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort());
        return "redirect://"+request.getServerName()+":"+request.getServerPort()+"/cust";
    }
   
    //고객사 LTE ID,APN 중복체크
	@RequestMapping(value="/custLteIdApnCheck", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
    public String custLteIdApnCheck(
    		         		 @RequestParam(defaultValue="") String cust_nm,
    		                 @RequestParam(defaultValue="") String lte_id,
    		                 @RequestParam(defaultValue="") String apn
    		                  
    		) throws Exception{
		

		logger.debug("cust_nm="+cust_nm);
		logger.debug("lte_id="+lte_id);
		logger.debug("apn="+apn);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("cust_nm", cust_nm);
        map.put("lte_id", lte_id);
        map.put("apn", apn);
 		Gson gson = new Gson();
		CustComponent cust = service.custLteIdApnCheck(map);
		String s ="";
		if(cust!=null)
		{
			if(!cust_nm.equals("")
				&& !lte_id.equals("")
				&& !apn.equals("")
				){
				map.put("cust_manage_no", cust.getCust_manage_no());
				GugDataConfigComponent gdv=gdService.gugDataConfigView(map);
				if(gdv!=null)
				{
					cust.setMsg("[고객명:"+cust_nm+",LTE_ID:"+lte_id+",APN:"+apn+"] 값으로\n"+
							 "이미 `" + gdv.getProfile_nm()+"`라는 프로파일명으로 국데이터가 등록되어있습니다."
							);
				}else{
					cust.setMsg("");
				}
			}
			
			 s = gson.toJson(cust);
		}
		
		logger.debug(s);
		return s;
	}
    
    
	
    
}

