package com.kt.e2e.Controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.kt.e2e.Service.TopologyEditorService;

@Controller
@RequestMapping(value="/topologyEditor")
public class TopologyEditorController extends BaseController {
	
	private static final Logger logger = LoggerFactory.getLogger(TopologyEditorController.class);
	
	TopologyEditorController() { setBaseName("topologyEditor"); }
	
	@Autowired
	private TopologyEditorService service;
	
	
	//TODO test for Dev
	@RequestMapping(value="/edit/{key}/{subKey}", method=RequestMethod.GET)
	public String test(@PathVariable("key") String key, @PathVariable String subKey, Model model) {
		if(key != null && key.equals("test") && subKey != null && subKey.equals("true")) {
			model.addAttribute("isTest",Boolean.TRUE);
		}
		return index(model);
	}
		
	@RequestMapping(value="/edit/{key}", method=RequestMethod.GET)
	public String edit(@PathVariable("key") String key, Model model) {
		model.addAttribute("key", key);
		return index(model);
	}
	
	@RequestMapping(value="/{type}", method=RequestMethod.GET)
	public String index(@PathVariable("type") String nsdType, Model model) {
		model.addAttribute("type", nsdType);
		return index(model);
	}


	@RequestMapping(value="/", method=RequestMethod.GET)
	public String index(Model model) {
		if(!model.containsAttribute("type")) {
			//NEW
			model.addAttribute("type", "new");
			
			//TODO for test
			model.addAttribute("type", "5g");
		}
		
		return view("edit");
	}
	
}
