package com.kt.e2e.Controller;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.kt.e2e.Common.CommonPager;
import com.kt.e2e.Common.EquipCmdRifCall;
import com.kt.e2e.Service.EquipCmdBasService;
import com.kt.e2e.Service.EquipInvenService;
import com.kt.e2e.Service.GugDataConfigService;
import com.kt.e2e.Service.IndPcmdCrExecService;
import com.kt.e2e.Domain.DataSummaryComponent;
import com.kt.e2e.Domain.EquipCmdBasComponent;
import com.kt.e2e.Domain.EquipInvenComponent;
import com.kt.e2e.Domain.GugDataConfigComponent;
import com.kt.e2e.Domain.GugDataConfigDetSelComponent;
import com.kt.e2e.Domain.IndPcmdCrExecComponent;

@Controller
@RequestMapping("/indPcmdCrExec")
public class IndPcmdCrExecController {


	private static final Logger logger = LoggerFactory.getLogger(IndPcmdCrExecController.class);

	@Autowired
	private IndPcmdCrExecService service;

	@Autowired
	private GugDataConfigService dtService;

	@Autowired
	private EquipInvenService eqService;

	@Autowired
	private EquipCmdBasService ecbService;
	//개별 명령어생성실행 목록
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public ModelAndView list(@RequestParam(defaultValue="modify_dtm") String searchOption,
			@RequestParam(defaultValue="") String keyword_fdate,
			@RequestParam(defaultValue="") String keyword_tdate,
			@RequestParam(defaultValue="profile_nm") String searchOption2,
			@RequestParam(defaultValue="") String keyword2,
			@RequestParam(defaultValue="1") int curPage,
			@RequestParam(defaultValue="") String work_state_cd
			) throws Exception{

		logger.info("searchOption:"+searchOption);
		logger.info("keyword_fdate:"+keyword_fdate);
		logger.info("keyword_tdate:"+keyword_tdate);
		logger.info("searchOption2:"+searchOption2);
		logger.info("keyword2:"+keyword2);
		logger.info("curPage:"+curPage);
		// 검색옵션, 키워드 맵에 저장
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("searchOption", searchOption);
		map.put("keyword_fdate", keyword_fdate);
		map.put("keyword_tdate", keyword_tdate);
		map.put("searchOption2", searchOption2);
		map.put("keyword2", keyword2);
		map.put("work_state_cd", work_state_cd);      
		/*'D010','입력중'
    	'D002','입력완료'
    	'D003','변경중'
    	'D004','작업대기'
    	'D005','작업완료'*/
		map.put("work_states", "('D020','D040','D050')"); 

		//검색옵션에 맞추어 카운트 가져오기
		int count =dtService.gugDataConfigCount(map);

		logger.info("count:"+count);
		// 페이지 나누기 관련 처리
		int pageScale = 10; // 페이지당 게시물 수
		int blockScale = 10;  // 화면당 페이지 수
		CommonPager commonPager = new CommonPager(count, curPage,pageScale,blockScale);
		map.put("start", commonPager.getPageBegin());
		map.put("end", commonPager.getPageEnd());
		map.put("curPage", commonPager.getCurPage());
		map.put("pageScale", commonPager.getPageScale());
		List<GugDataConfigComponent> list = dtService.gugDataConfigListOffset(map); //OFFSET방식
		DataSummaryComponent summaryItem= service.indPcmdCrExecSummarySelect(1); //개별 명령어생성실행 요약화 조회

		ModelAndView mav = new ModelAndView();
		mav.setViewName("indPcmdCrExec/indPcmdCrExecList");
		mav.addObject("count", count); 
		mav.addObject("list", list);
		mav.addObject("summaryItem", summaryItem);
		mav.addObject("curPage", curPage);
		mav.addObject("searchOption", searchOption);
		mav.addObject("keyword_fdate", keyword_fdate);
		mav.addObject("keyword_tdate", keyword_tdate);       
		mav.addObject("searchOption2", searchOption2);
		mav.addObject("keyword2", keyword2);
		mav.addObject("work_state_cd", work_state_cd);
		mav.addObject("commonPager", commonPager);
		logger.info("mav:"+ mav);
		return mav;

	}


	//개별 명령어 생성 목록
	@RequestMapping(value="/list", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
	public String command_list(
			@RequestParam(defaultValue="1") int curPage,
			@RequestParam(defaultValue="") int gug_data_manage_no,
			@RequestParam(defaultValue="") int gug_data_history_no
			) throws Exception{
		logger.info("curPage:"+curPage);
		// 검색옵션, 키워드 맵에 저장
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("gug_data_manage_no", gug_data_manage_no);
		map.put("gug_data_history_no", gug_data_history_no);

		logger.info("count3333333333333333:555");
		//검색옵션에 맞추어 카운트 가져오기
		int count =service.indPcmdCrExecCount(map);
		logger.info("count3333333333333333:"+count);

		// 페이지 나누기 관련 처리
		int pageScale = 10; // 페이지당 게시물 수
		int blockScale = 10;  // 화면당 페이지 수
		CommonPager commonPager = new CommonPager(count, curPage,pageScale,blockScale);

		map.put("start", commonPager.getPageBegin());
		map.put("end", commonPager.getPageEnd());
		// map.put("curPage", commonPager.getCurPage());
		map.put("pageScale", commonPager.getPageScale());
		map.put("count",count);
		List<IndPcmdCrExecComponent> list = service.indPcmdCrExecListOffset(map);



		map.put("list",list);

		Gson gson = new Gson();
		logger.debug(list.toString());
		String s = gson.toJson(map);
		logger.debug(s);
		return s;

	}





	//개별 명령어 생성(POST)
	@RequestMapping(value="/cmd_create",method=RequestMethod.POST,produces = "application/text; charset=utf8")
	@ResponseBody
	public String cmd_create(@ModelAttribute("IndPcmdCrExecComponent") IndPcmdCrExecComponent indPcmdCrExec) throws Exception{
		indPcmdCrExec.setView_gubun("CREATE");
		BigInteger work_command_create_no=indPcmdCrExec.getWork_command_create_no();
		BigInteger compareNo=new BigInteger("0");
		logger.info("check="+work_command_create_no);

		if (work_command_create_no.compareTo(compareNo) == 0){

			indPcmdCrExec.setWork_command_create_no(service.indPcmdCrExecGetCreateNo());

		}
		logger.info("indPcmdCrExec.getWork_command_create_no="+indPcmdCrExec.getWork_command_create_no());


		indPcmdCrExec.setCreater_id("admin");  //세션처리해야 함
	
		String equip_id=indPcmdCrExec.getEquip_id();
		

		indPcmdCrExec.setEquip_id(equip_id);
		indPcmdCrExec.setCreate_command_sentence(indPcmdCrExec.getCreate_command_sentence());
		indPcmdCrExec.setEach_create_yn("Y");  //개별 명령 실행여부
		service.indPcmdCrExecInsert(indPcmdCrExec);

		Gson gson = new Gson();
		logger.debug(indPcmdCrExec.toString());
		String s = gson.toJson(indPcmdCrExec);
		logger.debug(s);
		return s;
	}


	//개별 명령어 생성 할 페이지 뷰(GET)
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView view(@RequestParam(defaultValue="") String gubun) throws Exception{
		Map<String, Object> map= new HashMap<String, Object>();
		List<EquipCmdBasComponent> command_list =ecbService.equipCmdBasListOffset(map);
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("indPcmdCrExec/indPcmdCrExecView");

		EquipCmdBasComponent ecbVo=new EquipCmdBasComponent();
		ecbVo.setParameter_type("ENUM");
		Map<String,Object> ecbMap=ecbService.equipCmdBasComboMapSelect(ecbVo);
		Iterator<String> keys = ecbMap.keySet().iterator();

		while( keys.hasNext() ){

			String key = keys.next();
			mav.addObject(key,(List)ecbMap.get(key));

		}


		// 검색옵션, 키워드 맵에 저장
		map.put("status_name","운용");
		map.put("equip_type","MME");
		List<EquipInvenComponent> equip_list = eqService.equipInvenListOffset(map); //OFFSET방식
		mav.addObject("equip_list", equip_list);
		mav.addObject("command_list", command_list);

		logger.info("mav:"+ mav);
		return mav;
	}
	
	////해당하는 장비 타입에 국장비 조회(GET-JSON)
	@RequestMapping(value="/equip_list", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
	public String equip_list(@RequestParam(defaultValue="") String equip_type,
			@RequestParam(defaultValue="") String equip_id) throws Exception{
		logger.info("ddddddd333");
		Map<String, Object> map= new HashMap<String, Object>();
		Gson gson = new Gson();
		map.put("status_name","운용");
		map.put("equip_type",equip_type);
		map.put("equip_id",equip_id);
		List<EquipInvenComponent> equip_list = eqService.equipInvenListOffset(map); //OFFSET방식

		
		logger.debug(equip_list.toString());
		String s = gson.toJson(equip_list);
		logger.debug(s);
		return s;
	}
	
	////해당하는 장비 명령어와 도움말 파라메터 조회(GET-JSON)
	@RequestMapping(value="/equip_cmd_param_list", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
	public String equip_cmd_param_list(@RequestParam(defaultValue="") String gug_equip_type_cd,
										@RequestParam(defaultValue="") String command
			) throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();
		Gson gson = new Gson();
		EquipCmdBasComponent ecb = new EquipCmdBasComponent();
		ecb.setGug_equip_type_cd(gug_equip_type_cd);
		ecb.setCommand(command);
		EquipCmdBasComponent equip_cmd = ecbService.equipCmdBasView(ecb); //OFFSET방식
		map.put("equip_cmd", equip_cmd);
		List<EquipCmdBasComponent> param_list = ecbService.equipCmdBasComboSelect(ecb); //OFFSET방식
		
		//for(int i=0;i<param_list.size();i++)
		//	logger.info( ((EquipCmdBasComponent)param_list.get(i)).getParameter_seq_no() );
		map.put("param_list", param_list);
		String s = gson.toJson(map);
		logger.debug(s);
		return s;
	}
	
	
	////명령어 검증 체크용 기준 자료 가져오기
	@RequestMapping(value="/select_command_param_list", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
	public String select_command_param_list(
			) throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();
		Gson gson = new Gson();
		EquipCmdBasComponent ecb = new EquipCmdBasComponent();
		List<EquipCmdBasComponent> param_list = ecbService.equipCmdBasComboSelect(ecb); //OFFSET방식
		
		//for(int i=0;i<param_list.size();i++)
		//	logger.info( ((EquipCmdBasComponent)param_list.get(i)).getParameter_seq_no() );
		map.put("param_list", param_list);
		String s = gson.toJson(map);
		logger.debug(s);
		return s;
	}
	
	

	////개별 명령어실행 뷰 메시지 보기(GET-JSON)
	@RequestMapping(value="/view_msg/{work_command_create_no}", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
	public String view_msg(@PathVariable("work_command_create_no") BigInteger work_command_create_no) throws Exception{
		Gson gson = new Gson();
		IndPcmdCrExecComponent indPcmdCrExec = service.indPcmdCrExecView(work_command_create_no);
		 String[] lines = indPcmdCrExec.getExec_return_message().split(Pattern.quote("\r\n"));
	        for (String line : lines) {
	            System.out.println("line =: " + line);
	        }

		
		logger.debug(indPcmdCrExec.toString());
		String s = gson.toJson(indPcmdCrExec);
		logger.debug(s);
		return s;
	}



	//실행(RUN)
	@RequestMapping(value="/run_command",method=RequestMethod.POST,produces = "application/text; charset=utf8")
	@ResponseBody
	public String run_command(@ModelAttribute("IndPcmdCrExecComponent") IndPcmdCrExecComponent indPcmdCrExec) throws Exception{
		

		IndPcmdCrExecComponent vo=new IndPcmdCrExecComponent();

		if (indPcmdCrExec.getWork_command_create_no()== null ){

			vo.setWork_command_create_no(service.indPcmdCrExecGetCreateNo());

			vo.setCreate_command_sentence(indPcmdCrExec.getCreate_command_sentence());
			vo.setCreater_id("admin");  //세션처리해야 함

			vo.setEach_create_yn("Y");  //개별 명령 실행여부
			service.indPcmdCrExecInsert(vo);

		}else{
			 vo=service.indPcmdCrExecView(indPcmdCrExec.getWork_command_create_no());
		}
		
		
		vo.setCreate_command_sentence(indPcmdCrExec.getCreate_command_sentence());
		vo.setEms_pwd(indPcmdCrExec.getEms_pwd());
		vo.setCli_pwd(indPcmdCrExec.getCli_pwd());
		vo.setConfirm_pwd(indPcmdCrExec.getConfirm_pwd());
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("mmc",vo.getCreate_command_sentence());
		if(vo.getEms_pwd()==null || vo.getEms_pwd().equals(""))
		{
			map.put("ems_pwd","NULL");
		}else{
			map.put("ems_pwd",vo.getEms_pwd());
		}
		
		if(vo.getCli_pwd()==null || vo.getCli_pwd().equals(""))
		{
			map.put("cli_pwd","NULL");
		}else{
			map.put("cli_pwd",vo.getCli_pwd());
		}
		
		if(vo.getConfirm_pwd()==null || vo.getConfirm_pwd().equals(""))
		{
			map.put("confirm_pwd","NULL");
		}else{
			map.put("confirm_pwd",vo.getConfirm_pwd());
		}
		
		//실행이력번호 가져오기
		int seq_no=service.indPcmdCrExecGetHistoryNo(vo);
		
		vo.setExec_history_no(seq_no); //실행이력번호
		vo.setExecter_id("admin"); //실행자아이디
		vo.setExec_state_cd("DOO4"); //실행 상태 코드 running
		/*"D001";"ready"
		"D002";"success"
		"D003";"fail"
		"D004";"running"*/
		
		//실행중 상태이력 저장
		service.indPcmdCrExecHistoryInsert(vo);
		
		//RESTIF 실행 요청
		Map<String, Object> reMap=EquipCmdRifCall.callRestIF(map);
		
		//실행완료 후 성공 실패 메시 저장
		vo.setExec_state_cd(String.valueOf(reMap.get("exec_state_cd"))); //실행 상태 코드 success(D002)/fail(D003)
		vo.setExec_return_message (String.valueOf(reMap.get("exec_return_message"))); //실행 리턴 메시지

		service.indPcmdCrExecHistoryUpdate(vo);
		
		
		
		/*  
		GugDataConfigComponent gvo = new GugDataConfigComponent();
		gvo.setGug_data_manage_no(indPcmdCrExec.getGug_data_manage_no());
		gvo.setGug_data_history_no(indPcmdCrExec.getGug_data_history_no());
		gvo.setWork_state_cd("D040"); //작업대기상태
		dtService.gugDataConfigUpdate(gvo);
		dtService.gugDataConfigSummaryUpdate();//데이타 요약화
		*/
		Gson gson = new Gson();
		String s = gson.toJson(vo);
		logger.debug(s);
		return s;
	}

}

