package com.kt.e2e.Controller;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.net.URLCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.kt.e2e.Common.CommonPager;
import com.kt.e2e.Common.util.AES256Util;
import com.kt.e2e.Service.EquipInvenService;
import com.kt.e2e.Domain.EquipInvenComponent;
import com.kt.e2e.Domain.IndPcmdCrExecComponent;
 
@Controller
@RequestMapping("/equipInven")
public class EquipInvenController {
 
    
    private static final Logger logger = LoggerFactory.getLogger(EquipInvenController.class);
	@Autowired
	private EquipInvenService service;

    //장비인벤 목록
    @RequestMapping(method=RequestMethod.GET)
    public ModelAndView list(@RequestParam(defaultValue="equip_name") String searchOption,
            @RequestParam(defaultValue="") String keyword,
            @RequestParam(defaultValue="1") int curPage
             ) throws Exception{
    	
        logger.info("searchOption:"+searchOption);
        logger.info("keyword:"+keyword);
        // 검색옵션, 키워드 맵에 저장
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("searchOption", searchOption);
        map.put("keyword", keyword);
        
        //검색옵션에 맞추어 카운트 가져오기
    	int count =service.equipInvenCount(map);
    	
    	
        // 페이지 나누기 관련 처리
        int pageScale = 10; // 페이지당 게시물 수
        int blockScale = 10;  // 화면당 페이지 수
        CommonPager commonPager = new CommonPager(count, curPage,pageScale,blockScale);
        map.put("start", commonPager.getPageBegin());
        map.put("end", commonPager.getPageEnd());
        map.put("curPage", commonPager.getCurPage());
        map.put("pageScale", commonPager.getPageScale());
//        List<EquipInvenVO> list = service.equipInvenList(map); //ROWNUM방식
        List<EquipInvenComponent> list = service.equipInvenListOffset(map); //OFFSET방식

        
        ModelAndView mav = new ModelAndView();
        mav.setViewName("equipInven/equipInvenManage");
        mav.addObject("count", count); 
        mav.addObject("list", list);
        mav.addObject("curPage", curPage);
        mav.addObject("searchOption", searchOption);
        mav.addObject("keyword", keyword);
        mav.addObject("commonPager", commonPager);
        logger.info("mav:"+ mav);
        return mav;

    }
    
    //장비인벤 목록안에서 정렬(올림차순/내림차순)
	@RequestMapping(value="/jsondata", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
    public String listJsondata(
            @RequestParam(defaultValue="1") int curPage,
            @RequestParam(defaultValue="") String colName,
            @RequestParam(defaultValue="") String ordOption,
            @RequestParam(defaultValue="") String orderKey
             ) throws Exception{
    	

        // 검색옵션, 키워드 맵에 저장
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("orderKey", orderKey);
        
        //검색옵션에 맞추어 카운트 가져오기
    	int count =service.equipInvenCount(map);
    	
    	
        // 페이지 나누기 관련 처리
        int pageScale = 10; // 페이지당 게시물 수
        int blockScale = 10;  // 화면당 페이지 수
        CommonPager commonPager = new CommonPager(count, curPage,pageScale,blockScale);
        map.put("start", commonPager.getPageBegin());
        map.put("end", commonPager.getPageEnd());
        map.put("curPage", commonPager.getCurPage());
        map.put("pageScale", commonPager.getPageScale());
        
        //정렬옵션
        map.put("colName", colName);
        map.put("ordOption", ordOption);

        
        logger.debug(orderKey);

 		Gson gson = new Gson();
		List<EquipInvenComponent> list =service.equipInvenListOffset(map);
		logger.debug(list.toString());
		String s = gson.toJson(list);
		logger.debug(s);
		return s;

    }



    
    //장비인벤 작성 페이지(GET)    
    @RequestMapping(value="/post",method=RequestMethod.GET)
    public ModelAndView writeForm() throws Exception{
        
        return new ModelAndView("equipInven/equipInvenWrite");
    }
    
    //장비인벤 DB에 등록(POST)
    @RequestMapping(value="/post",method=RequestMethod.POST)
    public String write(@ModelAttribute("EquipInvenComponent") EquipInvenComponent equipInven) throws Exception{
 
    	
		
    	service.equipInvenInsert(equipInven);
    	
        
        //return "redirect://localhost:8080/equipInven";
    	HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    	logger.info(request.getContextPath()+":"+request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort());
        return "redirect://"+request.getServerName()+":"+request.getServerPort()+"/equipInven";

    }
    
    
    //장비인벤 수정 페이지(GET)
    @RequestMapping(value="/post/{equip_id}", method=RequestMethod.GET)
    public ModelAndView updateForm(@PathVariable("equip_id") String equip_id) throws Exception{
        
        EquipInvenComponent vo = service.equipInvenView(equip_id);
        
        return new ModelAndView("equipInven/equipInvenUpdate","equipInven",vo);
    }
    
    ////장비인벤 수정 페이지(GET-JSON)
	@RequestMapping(value="/jsondata/{equip_id}", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
    public String getJsondata(@PathVariable("equip_id") String equip_id) throws Exception{

 		Gson gson = new Gson();
		EquipInvenComponent vo = service.equipInvenView(equip_id);
		logger.debug(vo.toString());
		String s = gson.toJson(vo);
		logger.debug(s);
		return s;
	}
    

    //장비인벤 수정(PUT)
    @RequestMapping(value="/post/{bno}", method=RequestMethod.PUT)
    public String update(@ModelAttribute("EquipInvenComponent") EquipInvenComponent equipInven,@PathVariable("bno") int bno) throws Exception{
    	logger.debug(String.valueOf(bno));
        service.equipInvenUpdate(equipInven);
        
        
        
       // return "redirect://localhost:8080/equipInven";
    	HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    	logger.info(request.getContextPath()+":"+request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort());
        return "redirect://"+request.getServerName()+":"+request.getServerPort()+"/equipInven";

    }
    
    //장비인벤 삭제(DELETE)
    @RequestMapping(value="/delete/{bno}", method=RequestMethod.DELETE)
    public String delete(@PathVariable("bno") int bno) throws Exception{
    	logger.info("333");
        service.equipInvenDelete(bno);
        
        //return "redirect://localhost:8080/equipInven";
    	HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    	logger.info(request.getContextPath()+":"+request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort());
        return "redirect://"+request.getServerName()+":"+request.getServerPort()+"/equipInven";

    }
    
    //장비인벤 선택삭제(DELETE)
    @RequestMapping(value="/delete/select", method=RequestMethod.DELETE)
    public String delete(@ModelAttribute("EquipInvenComponent") EquipInvenComponent equipInven) throws Exception{
    	logger.info("dddddddddd");
    	
        int bno=0;

        String[] checkNo=equipInven.getCheckNo();
        if(checkNo!=null){
	        for(int i=0 ;i<checkNo.length;i++)
	        {
	            logger.info(checkNo[i]);
	        	bno=Integer.valueOf(checkNo[i]);
	            service.equipInvenDelete(bno);
	        }
        }
        //return "redirect://localhost:8080/equipInven";
    	HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    	logger.info(request.getContextPath()+":"+request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort());
        return "redirect://"+request.getServerName()+":"+request.getServerPort()+"/equipInven";

    }
    
    ////EMS 장비관리정보 보기
	@RequestMapping(value="/plteConnInfoView", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
    public String plteConnInfoView(
    		 @RequestParam(defaultValue="") String sys_type,
             @RequestParam(defaultValue="") String sys_name
    		) throws Exception{

		EquipInvenComponent ivo=new EquipInvenComponent();
		ivo.setSys_type(sys_type);
		ivo.setSys_name(sys_name);
		
 		Gson gson = new Gson();
		EquipInvenComponent vo = service.plteConnInfoView(ivo);
		
		String key = AES256Util.getKeys();    //key는 16자 이상
		AES256Util aes256 = new AES256Util(key);
		URLCodec codec = new URLCodec();
		logger.info(""+(vo==null));
        String decEms_pwd="";
		String decCli_pw="";
		String decConfirm_pwd="";
		if(vo!=null) {
			if(vo.getPwd_1()==null) {
				decEms_pwd="NULL";
			}else {
				if(vo.getPwd_1().trim().equals("")) {
					decEms_pwd="NULL";
				}else {
					String encEms_pwd=vo.getPwd_1(); //암호문
					if(aes256.checkAesDecode(encEms_pwd))
					{
						try {
							decEms_pwd = aes256.aesDecode(codec.decode(encEms_pwd));//암호문을 평문으로 복호화
						}catch(javax.crypto.IllegalBlockSizeException ex) {
							decEms_pwd=vo.getPwd_1();
						}
					}
					else decEms_pwd=vo.getPwd_1();
				}
			}
			
			if(vo.getCli_pwd()==null) {
				decCli_pw="NULL";
			}else {
				if(vo.getCli_pwd().trim().equals("")) {
					decCli_pw="NULL";
				}else {
					String encCli_pw=vo.getCli_pwd(); //암호문
					if(aes256.checkAesDecode(encCli_pw))
					{
						try {
							decCli_pw = aes256.aesDecode(codec.decode(encCli_pw));//암호문을 평문으로 복호화
						}catch(javax.crypto.IllegalBlockSizeException ex) {
							decCli_pw=vo.getCli_pwd();
						}
					}
					else decCli_pw=vo.getCli_pwd();;
				}
			}
			
			if(vo.getPwd_2()==null) {
				decConfirm_pwd="NULL";
			}else {
				if(vo.getPwd_1().trim().equals("")) {
					decConfirm_pwd="NULL";
				}else {
					String encConfirm_pwd=vo.getPwd_2(); //암호문
					if(aes256.checkAesDecode(encConfirm_pwd))
					{
						try {
							decConfirm_pwd = aes256.aesDecode(codec.decode(encConfirm_pwd));//암호문을 평문으로 복호화
						}catch(javax.crypto.IllegalBlockSizeException ex) {
							decConfirm_pwd=vo.getPwd_2();
						}
					}
					else decConfirm_pwd=vo.getPwd_2();
				}
			}
			vo.setPwd_1(decEms_pwd);
			vo.setCli_pwd(decCli_pw);
			vo.setPwd_2(decConfirm_pwd);
		}
		
		
		
		String s ="";
		if(vo != null)
		{
			s=gson.toJson(vo);
		}
		logger.debug(s);
		return s;
	}
    
    //EMS 장비관리정보 머지
	@RequestMapping(value="/plteConnInfoMerge", method=RequestMethod.POST,produces = "application/text; charset=utf8")
	@ResponseBody
    public String plteConnInfoMerge(@ModelAttribute("EquipInvenComponent") EquipInvenComponent vo) throws Exception{
    	
		String key = AES256Util.getKeys();    //key는 16자 이상
		AES256Util aes256 = new AES256Util(key);
		URLCodec codec = new URLCodec();
		String encEms_pwd="";//평문을 암호문으르 암호화
		String encCli_pw="";//평문을 암호문으르 암호화
		String encConfirm_pwd="";//평문을 암호문으르 암호화
		
		if(vo.getPwd_1()==null) {
			encEms_pwd="";
		}else {
			if(vo.getPwd_1().trim().equals("")) {
				encEms_pwd="";
			}else {
				encEms_pwd = codec.encode(aes256.aesEncode(vo.getPwd_1()));//평문을 암호문으르 암호화
			}
		}
		
		if(vo.getCli_pwd()==null) {
			encCli_pw="";
		}else {
			if(vo.getCli_pwd().trim().equals("")) {
				encCli_pw="";
			}else {
				encCli_pw = codec.encode(aes256.aesEncode(vo.getCli_pwd()));//평문을 암호문으르 암호화
			}
		}
		
		if(vo.getPwd_2()==null) {
			encConfirm_pwd="";
		}else {
			if(vo.getPwd_2().trim().equals("")) {
				encConfirm_pwd="";
			}else {
				encConfirm_pwd = codec.encode(aes256.aesEncode(vo.getPwd_2()));//평문을 암호문으르 암호화
			}
		}
		
	
		vo.setPwd_1(encEms_pwd);
		vo.setCli_pwd(encCli_pw);
		vo.setPwd_2(encConfirm_pwd);
		
		service.plteConnInfoMerge(vo);
		return "";
    
    }
    
    //EMS 장비관리정보 삭제
	@RequestMapping(value="/plteConnInfoDelete", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
    public String plteConnInfoDelete(
   		 @RequestParam(defaultValue="") String sys_type,
         @RequestParam(defaultValue="") String sys_name
    		)throws Exception{
		logger.debug("EMS 장비관리정보 삭제:sys_type="+sys_type+":sys_name="+sys_name);
		EquipInvenComponent vo=new EquipInvenComponent();
		vo.setSys_type(sys_type);
		vo.setSys_name(sys_name);
		
    	service.plteConnInfoDelete(vo);
		return "";
    }
    
   
}

