package com.kt.e2e.Controller;

import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.net.URLCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.kt.e2e.Common.CommonPager;
import com.kt.e2e.Common.EquipCmdRifCall;
import com.kt.e2e.Common.util.AES256Util;
import com.kt.e2e.Service.EquipCmdBasService;
import com.kt.e2e.Service.EquipInvenService;
import com.kt.e2e.Service.GugDataConfigService;
import com.kt.e2e.Service.GugPcmdCrExecService;
import com.kt.e2e.Domain.DataSummaryComponent;
import com.kt.e2e.Domain.EquipCmdBasComponent;
import com.kt.e2e.Domain.EquipInvenComponent;
import com.kt.e2e.Domain.GugDataConfigComponent;
import com.kt.e2e.Domain.GugDataConfigDetSelComponent;
import com.kt.e2e.Domain.GugPcmdCrExecComponent;
import com.kt.e2e.Domain.IndPcmdCrExecComponent;

@Controller
@RequestMapping("/gugPcmdCrExec")
public class GugPcmdCrExecController {


	private static final Logger logger = LoggerFactory.getLogger(GugPcmdCrExecController.class);

	@Autowired
	private GugPcmdCrExecService service;

	@Autowired
	private GugDataConfigService dtService;

	@Autowired
	private EquipInvenService eqService;

	@Autowired
	private EquipCmdBasService ecbService;
	
	
	String item_val_chk[][]
			={//MME
              {"apn_fqdn","MAN","DECIMAL","MME","N","APN",""}  //CRTE-DOMAIN	APN	MAN
              ,{"pgwgrp","MAN","DECIMAL","MME","N","PGWGRP",""}  
              ,{"pgw","OPT","DECIMAL","MME","Y","PGW",""}  
              ,{"ip","OPT","STRING","MME","Y","IP",""}  
              ,{"name","OPT","STRING","MME","Y","NAME",""}  
              ,{"capa","OPT","DECIMAL","MME","Y","CAPA",""}  
              //EPCC
              ,{"pgw_ip","MAN","STRING","EPCC","N","IP_ADDR",""}  
              ,{"sms_recv","OPT","STRING","EPCC","N","",""}  //보류
              ,{"ip_pool","OPT","STRING","EPCC","Y","",""}  //설정하지 안흠- 화면에서 않보이게
               //PGW
              ,{"ip_pool_id","MAN","DECIMAL","PGW","Y","IPPOOL",""}  
              ,{"vr_id","MAN","DECIMAL","PGW","Y","VR",""} //CRTE-IP-POOL	VR	OPT or  CRTE-VLAN-INTF	VRID	MAN
              ,{"pool_type_cd","OPT",	"ENUM","PGW","Y","TYPE",""}        
              ,{"static_cd","OPT",	"ENUM","PGW","Y","STATIC",""}          
              ,{"tunnel_cd","OPT",	"ENUM","PGW","Y","TUNNEL",""}             
              ,{"start_addr","OPT",	"STRING","PGW","Y","IP",""}  
              ,{"vlan","OPT",	"DECIMAL","PGW","Y","VLANID",""}  													
              ,{"ip_addr","OPT",	"STRING","PGW","Y","IPADDR",""}  
              ,{"network","OPT",	"STRING","PGW","Y","NETWORK",""}  
              ,{"gateway","OPT",	"STRING","PGW","Y","GWIP",""}  
              ,{"prirank_cd","OPT",	"ENUM","PGW","Y","우선순위-없음",""}  //확인필요함
              ,{"ifc","OPT",	"STRING","PGW","Y","IFNAME",""}  
              ,{"apn_id","MAN",	"DECIMAL","PGW","Y","APN",""}  
              ,{"apn_name","MAN",	"STRING","PGW","Y","NAME",""}  
              ,{"ip_alloc_cd","OPT",	"ENUM","PGW","Y","ALLOC",""}         
              ,{"rad_ip_alloc_cd","OPT",	"ENUM","PGW","Y","ALLOC",""}    
              ,{"auth_pcrf_pcc_cd","OPT",	"ENUM","PGW","Y","",""} //확인 필요함
              ,{"acct_react_cd","OPT",	"ENUM","PGW","Y","",""}    //확인 필요함     
              ,{"pri_dns_v4","OPT",	"STRING","PGW","Y","PDNS4",""}  
              ,{"rad_auth_id","OPT",	"DECIMAL","PGW","Y","RAUTH",""}  
              ,{"rad_auth_act","OPT",	"STRING","PGW","Y","",""}  //확인 필요함
              ,{"rad_auth_sby","OPT",	"STRING","PGW","Y","",""}    //확인 필요함
              ,{"rad_acct_id","OPT",	"DECIMAL","PGW","Y","RACCT",""}  
              ,{"rad_acct_act","OPT",	"STRING","PGW","Y","",""}    //확인 필요함 
              ,{"rad_acct_sby","OPT",	"STRING","PGW","Y","",""}    //확인 필요함
              ,{"diam_pcfr_id","OPT",	"STRING","PGW","Y","DPCRF",""}    
              ,{"diam_pcfr_act","OPT",	"STRING","PGW","Y","",""}    //확인 필요함 
              ,{"diam_pcfr_sby","OPT",	"STRING","PGW","Y","",""}   //확인 필요함
              ,{"local_pcc_pf_id","OPT",	"STRING","PGW","Y","",""}  //설정하지 않음
              ,{"local_pcc_pf_act","OPT",	"STRING","PGW","Y","",""}  //확인 필요함
              ,{"local_pcc_pf_sby","OPT",	"STRING","PGW","Y","",""}  //확인 필요함
              ,{"sec_dns_v4","OPT",	"STRING","PGW","Y","SDNS4",""}  
              ,{"arp_map_high","OPT",	"DECIMAL","PGW","Y","ARPH",""}  
              ,{"arp_map_med","OPT", "DECIMAL","PGW","Y","ARPM",""}
               //HSS
              ,{"id","MAN",	"DECIMAL","HSS","N","ID",""} 
              ,{"apn","MAN",	"STRING","HSS","N","APN",""}  
              ,{"apnoirepl","OPT",	"STRING","HSS","N","APNOIREPL",""}  
              ,{"pltcc","OPT",	"STRING","HSS","N","PLTECC",""}  
              ,{"uutype","OPT",	"STRING","HSS","N","UUTYPE",""}
              ,{"mpname_cd","OPT",	"ENUM","HSS","N","MPNAME",""}
             };
	
	//명령어생성실행 목록
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView list(@RequestParam(defaultValue="modify_dtm") String searchOption,
			@RequestParam(defaultValue="") String keyword_fdate,
			@RequestParam(defaultValue="") String keyword_tdate,
			@RequestParam(defaultValue="profile_nm") String searchOption2,
			@RequestParam(defaultValue="") String keyword2,
			@RequestParam(defaultValue="1") int curPage,
			@RequestParam(defaultValue="") String work_state_cd
			) throws Exception{

		logger.info("searchOption:"+searchOption);
		logger.info("keyword_fdate:"+keyword_fdate);
		logger.info("keyword_tdate:"+keyword_tdate);
		logger.info("searchOption2:"+searchOption2);
		logger.info("keyword2:"+keyword2);
		logger.info("curPage:"+curPage);
		// 검색옵션, 키워드 맵에 저장
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("searchOption", searchOption);
		map.put("keyword_fdate", keyword_fdate);
		map.put("keyword_tdate", keyword_tdate);
		map.put("searchOption2", searchOption2);
		map.put("keyword2", keyword2);
		map.put("work_state_cd", work_state_cd);      
		/*'D010','입력중'
    	'D020','입력완료'
    	'D030','변경중'
    	'D040','작업대기'
    	'D050','작업완료'*/
		map.put("work_states", "('D020','D040','D050')"); 

		//검색옵션에 맞추어 카운트 가져오기
		int count =dtService.gugDataConfigCount(map);

		logger.info("count:"+count);
		// 페이지 나누기 관련 처리
		int pageScale = 10; // 페이지당 게시물 수
		int blockScale = 10;  // 화면당 페이지 수
		CommonPager commonPager = new CommonPager(count, curPage,pageScale,blockScale);
		map.put("start", commonPager.getPageBegin());
		map.put("end", commonPager.getPageEnd());
		map.put("curPage", commonPager.getCurPage());
		map.put("pageScale", commonPager.getPageScale());
		List<GugDataConfigComponent> list = dtService.gugDataConfigListOffset(map); //OFFSET방식
		DataSummaryComponent summaryItem= service.gugPcmdCrExecSummarySelect(1); //명령어생성실행 요약화 조회


        if(keyword_fdate.length() >=8)
        {
        	keyword_fdate=keyword_fdate.substring(0, 4) + "년 "
						+ keyword_fdate.substring(4, 6) + "월 "
						+ keyword_fdate.substring(6, 8) + "일";
        logger.info("keyword_fdate:"+keyword_fdate.substring(0, 4));
        logger.info("keyword_fdate:"+keyword_fdate.substring(4, 6));
        logger.info("keyword_fdate:"+keyword_fdate.substring(6, 8));
        }
        
        if(keyword_tdate.length() >=8)
        {
        	keyword_tdate=keyword_tdate.substring(0, 4) + "년 "
						+ keyword_tdate.substring(4, 6) + "월 "
						+ keyword_tdate.substring(6, 8) + "일";
        logger.info("keyword_tdate:"+keyword_tdate.substring(0, 4));
        logger.info("keyword_tdate:"+keyword_tdate.substring(4, 6));
        logger.info("keyword_tdate:"+keyword_tdate.substring(6, 8));
        }
		ModelAndView mav = new ModelAndView();
		mav.setViewName("gugPcmdCrExec/gugPcmdCrExecList");
		mav.addObject("count", count); 
		mav.addObject("list", list);
		mav.addObject("summaryItem", summaryItem);
		mav.addObject("curPage", curPage);
		mav.addObject("searchOption", searchOption);
		mav.addObject("keyword_fdate", keyword_fdate);
		mav.addObject("keyword_tdate", keyword_tdate);       
		mav.addObject("searchOption2", searchOption2);
		mav.addObject("keyword2", keyword2);
		mav.addObject("work_state_cd", work_state_cd);
		mav.addObject("commonPager", commonPager);
		logger.info("mav:"+ mav);
		return mav;

	}
	  //국데이타 목록안에서 정렬(올림차순/내림차순)
		@RequestMapping(value="/jsondata", method=RequestMethod.GET,produces = "application/text; charset=utf8")
		@ResponseBody
	    public String listJsondata(
	            @RequestParam(defaultValue="1") int curPage,
	            @RequestParam(defaultValue="") String colName,
	            @RequestParam(defaultValue="") String ordOption,
	            @RequestParam(defaultValue="") String orderKey
	             ) throws Exception{
	    	

	        // 검색옵션, 키워드 맵에 저장
	        Map<String, Object> map = new HashMap<String, Object>();
	        map.put("orderKey", orderKey);
	        
	        //검색옵션에 맞추어 카운트 가져오기
	    	int count =dtService.gugDataConfigCount(map);
	    	
	    	
	        // 페이지 나누기 관련 처리
	        int pageScale = 10; // 페이지당 게시물 수
	        int blockScale = 10;  // 화면당 페이지 수
	        CommonPager commonPager = new CommonPager(count, curPage,pageScale,blockScale);
	        map.put("start", commonPager.getPageBegin());
	        map.put("end", commonPager.getPageEnd());
	        map.put("curPage", commonPager.getCurPage());
	        map.put("pageScale", commonPager.getPageScale());
	        
	        //정렬옵션
	        map.put("colName", colName);
	        map.put("ordOption", ordOption);

	        
	        logger.debug(orderKey);

	 		Gson gson = new Gson();
			List<GugDataConfigComponent> list =dtService.gugDataConfigListOffset(map);
			logger.debug(list.toString());
			String s = gson.toJson(list);
			logger.debug(s);
			return s;

	    }

	//명령어 생성 목록
	@RequestMapping(value="/list", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
	public String command_list(
			@RequestParam(defaultValue="1") int curPage,
			@RequestParam(defaultValue="") int gug_data_manage_no,
			@RequestParam(defaultValue="") int gug_data_history_no
			) throws Exception{
		logger.info("curPage:"+curPage);
		// 검색옵션, 키워드 맵에 저장
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("gug_data_manage_no", gug_data_manage_no);
		map.put("gug_data_history_no", gug_data_history_no);

		//검색옵션에 맞추어 카운트 가져오기
		int count =service.gugPcmdCrExecCount(map);

		// 페이지 나누기 관련 처리
		int pageScale = 1000; // 페이지당 게시물 수
		int blockScale = 10;  // 화면당 페이지 수
		CommonPager commonPager = new CommonPager(count, curPage,pageScale,blockScale);

		map.put("start", commonPager.getPageBegin());
		map.put("end", commonPager.getPageEnd());
		// map.put("curPage", commonPager.getCurPage());
		map.put("pageScale", commonPager.getPageScale());
		map.put("count",count);
		List<GugPcmdCrExecComponent> list = service.gugPcmdCrExecListOffset(map);



		map.put("list",list);

		Gson gson = new Gson();
		logger.debug(list.toString());
		String s = gson.toJson(map);
		logger.debug(s);
		return s;

	}





	//명령어 생성(POST)
	@RequestMapping(value="/cmd_create",method=RequestMethod.POST,produces = "application/text; charset=utf8")
	@ResponseBody
	public String cmd_create(@ModelAttribute("GugPcmdCrExecComponent") GugPcmdCrExecComponent gugPcmdCrExec) throws Exception{
		gugPcmdCrExec.setView_gubun("CREATE");
		BigInteger work_command_create_no=gugPcmdCrExec.getWork_command_create_no();
		BigInteger compareNo=new BigInteger("0");
		int gug_data_history_no=gugPcmdCrExec.getGug_data_history_no();
		logger.info("check="+work_command_create_no);

		if (work_command_create_no.compareTo(compareNo) == 0){

			gugPcmdCrExec.setWork_command_create_no(service.gugPcmdCrExecGetCreateNo());

		}
		logger.info("gugPcmdCrExec.getWork_command_create_no="+gugPcmdCrExec.getWork_command_create_no());
		String[] sel_equip_id=gugPcmdCrExec.getEquip_id().split(Pattern.quote("|||"));
		
		//국데이타 연결 조회
		Class cls = Class.forName("com.kt.e2e.Domain.GugDataConfigComponent");
		Class partypes[] = new Class[1];

		partypes[0] = String.class;
//    	Method meth = cls.getMethod("getPgwgrp", partypes);
//
//    	GugDataConfigComponent methobj = new GugDataConfigComponent();
//
//		Object arglist[] = new Object[2];
//
//		arglist[0] = new String("37");
//		Object retobj = meth.invoke(gugDataConfig, null);
//
//		String retval = (String) retobj;



		Map<String, Object> gug_map = new HashMap<String, Object>();
        gug_map.put("gug_data_manage_no",gugPcmdCrExec.getGug_data_manage_no());
        gug_map.put("gug_data_history_no",null);
        
        GugDataConfigComponent gugDataConfig = dtService.gugDataConfigView(gug_map);
        for(int i=0;i<item_val_chk.length;i++)
        {
        	String screen_param=item_val_chk[i][0];
        	Method meth = cls.getMethod("get" + screen_param.substring(0,1).toUpperCase()+ screen_param.substring(1));
			Object retobj = meth.invoke(gugDataConfig, null);
			if(retobj !=null)
			{
				String retval = (String) retobj;
				logger.info(screen_param + "=" +retval);	
	
	        	item_val_chk[i][6]=retval;
			}else{
				String retval = (String) retobj;
				logger.info(screen_param + "=" +retval);

	        	item_val_chk[i][6]="";
			}
        }
        
        gugDataConfig.setView_gubun("EDIT");
        
        List<GugDataConfigDetSelComponent> mmeDetLst=dtService.gugDataConfigMmeDetView(gugDataConfig);
        if(mmeDetLst.size()==0)
        {
        	GugDataConfigDetSelComponent gdcds=new GugDataConfigDetSelComponent();
        	gdcds.setMme_det_no(1);
        	gdcds.setPgw("");
        	mmeDetLst.add(gdcds);
        	
        }
        
        List<GugDataConfigDetSelComponent> epccDetLst=dtService.gugDataConfigEpccDetView(gugDataConfig);
        if(epccDetLst.size()==0)
        {
        	GugDataConfigDetSelComponent gdcds=new GugDataConfigDetSelComponent();
        	gdcds.setEpcc_det_no(1);
        	epccDetLst.add(gdcds);
        	
        }
        
        List<GugDataConfigDetSelComponent> pgwLst=dtService.gugDataConfigPgwView(gugDataConfig);
        if(pgwLst.size()==0)
        {
        	GugDataConfigDetSelComponent gdcds=new GugDataConfigDetSelComponent();
        	gdcds.setPgw_seq_no(1);
        	pgwLst.add(gdcds);
        	
        }

      /*  gugDataConfig
        mmeDetLst
        epccDetLst
        pgwLst */
        
		//자동 명령어 생성 프로파일 임시하드코딩
//		String command_list[][] = {
//				{"MME","CRTE-PGW-GRP"}
//				,{"MME","CRTE-DOMAIN"}	
//				,{"EPCC","ADD-IP-POOL"}
//				,{"EPCC","ADD-LTEID-APN"}	
//				,{"PGW","CRTE-VR"}
//				,{"PGW","CRTE-IP-POOL"}	
//				,{"HSS","CRTE-PLTE-INFO"}
//		      }; 
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("gug_data_manage_no",gugDataConfig.getGug_data_manage_no());
		List<GugDataConfigComponent> gug_sel_cmd = dtService.commandSelManageList(map); //OFFSET방식		
		int equip_cnt_check=0;
		for(int i=0;i<gug_sel_cmd.size();i++)
		{
			GugDataConfigComponent gdc=new GugDataConfigComponent();
			gdc=gug_sel_cmd.get(i);
			if(gdc.getGug_equip_type_nm().equals(gugPcmdCrExec.getEquip_type()))
			{
				EquipCmdBasComponent ecb = new EquipCmdBasComponent();
				ecb.setGug_equip_type_cd(gugPcmdCrExec.getEquip_type_cd());
				ecb.setGug_equip_type_nm(gugPcmdCrExec.getEquip_type());
				ecb.setCommand(gdc.getCommand());
				equip_cnt_check++;

		//		CRTE-PGW-GRP	PGWGRP	MAN	DECIMAL	0	1023
		//		PGW	MAN	DECIMAL	1	1023
		//		CAPA	MAN	DECIMAL	1	255
		
				gugPcmdCrExec.setCreater_id("admin");  //세션처리해야 함
				
				
				String create_command_sentence="";
				if(sel_equip_id!=null){
					for(int j=0 ;j<sel_equip_id.length;j++)
					{

						
						create_command_sentence=ecb.getCommand() +":SYS_TYPE=" +ecb.getGug_equip_type_nm();
						create_command_sentence+=",SYS_NAME="+sel_equip_id[j];
						List<EquipCmdBasComponent> param_list = ecbService.equipCmdBasComboSelect(ecb); //OFFSET방식
						String commaTxt="";
						for(int k=0;k<param_list.size();k++)
						{
							EquipCmdBasComponent params=new EquipCmdBasComponent();
							params=param_list.get(k);
							String tempValue="";
					        for(int m=0;m<item_val_chk.length;m++)
					        {
					        	if(item_val_chk[m][3].equals(ecb.getGug_equip_type_nm()))
			        			{
					        		if(item_val_chk[m][5].equals(params.getParameter())){
					        			tempValue=item_val_chk[m][6];
					        		}
			        			}
					        }
							if(tempValue.equals(""))
					        {
								commaTxt+="," ;
					       	}else{
								create_command_sentence+=commaTxt +","+ params.getParameter() +"="+tempValue;
								commaTxt="";
					        }
						}
		
						create_command_sentence+=";";
						//DB저장하기 VO SET
						gugPcmdCrExec.setEquip_id(sel_equip_id[j]);
						gugPcmdCrExec.setCreate_command_sentence(create_command_sentence);
						service.gugPcmdCrExecInsert(gugPcmdCrExec);
						create_command_sentence="";
						gugPcmdCrExec.setWork_command_create_no(service.gugPcmdCrExecGetCreateNo());
						logger.info(sel_equip_id[j]);
					}
				}
			}
		}


//		dtService.gugDataConfigSummaryUpdate();//데이타 요약화
		Gson gson = new Gson();
		List<GugPcmdCrExecComponent> list =null;
		String s="";
		if(equip_cnt_check<=0)
		{
			s="국데이타관리에서 " + gugPcmdCrExec.getEquip_type()+ "장비 관련의 선택된 명령어가 없습니다.";
			logger.info("국데이타관리에서 선택된 명령어가 없습니다.");
		}
			
		//		logger.debug(list.toString());	
		//		String s = gson.toJson(list);
		//		logger.debug(s);
		return s;
	}


	//실패한 명령 재명령 생성(POST)
		@RequestMapping(value="/fail_cmd_create",method=RequestMethod.POST,produces = "application/text; charset=utf8")
		@ResponseBody
		public String fail_cmd_create(@ModelAttribute("GugPcmdCrExecComponent") GugPcmdCrExecComponent gugPcmdCrExec) throws Exception{
			BigInteger work_command_create_no=gugPcmdCrExec.getWork_command_create_no();
			BigInteger compareNo=new BigInteger("0");
			logger.info("check="+work_command_create_no);

			if (work_command_create_no.compareTo(compareNo) == 0){

				gugPcmdCrExec.setWork_command_create_no(service.gugPcmdCrExecGetCreateNo());

			}
			logger.info("indPcmdCrExec.getWork_command_create_no="+gugPcmdCrExec.getWork_command_create_no());


			gugPcmdCrExec.setCreater_id("admin");  //세션처리해야 함
		
			String equip_id=gugPcmdCrExec.getEquip_id();
			

			gugPcmdCrExec.setEquip_id(equip_id);
			gugPcmdCrExec.setCreate_command_sentence(gugPcmdCrExec.getCreate_command_sentence());
			service.gugPcmdCrExecUpdate(gugPcmdCrExec);

			Gson gson = new Gson();
			logger.debug(gugPcmdCrExec.toString());
			String s = gson.toJson(gugPcmdCrExec);
			logger.debug(s);
			return s;
		}
	
	//명령어 생성 할 페이지 뷰(GET)
	@RequestMapping(value="/view/{gug_data_manage_no}", method=RequestMethod.GET)
	public ModelAndView editView(@PathVariable("gug_data_manage_no") BigInteger gug_data_manage_no) throws Exception{
        Map<String, Object> gug_map = new HashMap<String, Object>();
        gug_map.put("gug_data_manage_no",gug_data_manage_no);
        gug_map.put("gug_data_history_no",null);
		GugDataConfigComponent gugDataConfig = dtService.gugDataConfigView(gug_map);
		gugDataConfig.setView_gubun("EDIT");

		List<GugDataConfigDetSelComponent> mmeDetLst=dtService.gugDataConfigMmeDetView(gugDataConfig);
		if(mmeDetLst.size()==0)
		{
			GugDataConfigDetSelComponent gdcds=new GugDataConfigDetSelComponent();
			gdcds.setMme_det_no(1);
			gdcds.setPgw("");
			mmeDetLst.add(gdcds);

		}

		List<GugDataConfigDetSelComponent> epccDetLst=dtService.gugDataConfigEpccDetView(gugDataConfig);
		if(epccDetLst.size()==0)
		{
			GugDataConfigDetSelComponent gdcds=new GugDataConfigDetSelComponent();
			gdcds.setEpcc_det_no(1);
			epccDetLst.add(gdcds);

		}

		List<GugDataConfigDetSelComponent> pgwLst=dtService.gugDataConfigPgwView(gugDataConfig);
		if(pgwLst.size()==0)
		{
			GugDataConfigDetSelComponent gdcds=new GugDataConfigDetSelComponent();
			gdcds.setPgw_seq_no(1);
			pgwLst.add(gdcds);

		}

		ModelAndView mav = new ModelAndView();
		mav.setViewName("gugPcmdCrExec/gugPcmdCrExecView");
		mav.addObject("gugDataConfig", gugDataConfig); 
		mav.addObject("mmeDetLst", mmeDetLst); 
		mav.addObject("epccDetLst", epccDetLst);
		mav.addObject("pgwLst", pgwLst);

		EquipCmdBasComponent ecbVo=new EquipCmdBasComponent();
		ecbVo.setParameter_type("ENUM");
		Map<String,Object> ecbMap=ecbService.equipCmdBasComboMapSelect(ecbVo);
		Iterator<String> keys = ecbMap.keySet().iterator();

		while( keys.hasNext() ){

			String key = keys.next();
			mav.addObject(key,(List)ecbMap.get(key));

		}


		// 검색옵션, 키워드 맵에 저장
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status_name","운용");
		map.put("equip_type","MME");
		map.put("gug_data_manage_no",gug_data_manage_no);
		map.put("gug_data_history_no",gugDataConfig.getGug_data_history_no());
		List<EquipInvenComponent> equip_list = eqService.equipInvenListOffset(map); //OFFSET방식
		mav.addObject("equip_list", equip_list);

		logger.info("mav:"+ mav);
		return mav;
	}

	////명령어실행 뷰 메시지 보기(GET-JSON)
	@RequestMapping(value="/view_msg/{work_command_create_no}", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
	public String view_msg(@PathVariable("work_command_create_no") BigInteger work_command_create_no) throws Exception{
		Gson gson = new Gson();
		GugPcmdCrExecComponent gugPcmdCrExec = service.gugPcmdCrExecView(work_command_create_no);
		 String[] lines = gugPcmdCrExec.getExec_return_message().split(Pattern.quote("\r\n"));
	        for (String line : lines) {
	            System.out.println("line =: " + line);
	        }

		
		logger.debug(gugPcmdCrExec.toString());
		String s = gson.toJson(gugPcmdCrExec);
		logger.debug(s);
		return s;
	}
	
	////명령어실행 뷰 메시지 보기(GET-JSON)
	@RequestMapping(value="/equip_list", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
	public String equip_list(@ModelAttribute("GugPcmdCrExecComponent") GugPcmdCrExecComponent vo) throws Exception{
		Gson gson = new Gson();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status_name","운용");
		map.put("equip_type",vo.getEquip_type());
		map.put("gug_data_manage_no",vo.getGug_data_manage_no());
		map.put("gug_data_history_no",vo.getGug_data_history_no());
		List<EquipInvenComponent> equip_list = eqService.equipInvenListOffset(map); //OFFSET방식
		
		logger.debug(equip_list.toString());
		String s = gson.toJson(equip_list);
		logger.debug(s);
		return s;
	}	
	


	//운용자 확인 수정(POST)
	@RequestMapping(value="/oper_confirm",method=RequestMethod.POST,produces = "application/text; charset=utf8")
	@ResponseBody
	public String oper_confirm(@ModelAttribute("GugPcmdCrExecComponent") GugPcmdCrExecComponent gugPcmdCrExec) throws Exception{
		logger.info("gugPcmdCrExec.getEro_cc_yn()=" +gugPcmdCrExec.getEro_cc_yn());
		gugPcmdCrExec.setOperater_id("admin");
		service.gugPcmdCrExecUpdate(gugPcmdCrExec);
		
		GugDataConfigComponent vo = new GugDataConfigComponent();
		vo.setGug_data_manage_no(gugPcmdCrExec.getGug_data_manage_no());
		vo.setGug_data_history_no(gugPcmdCrExec.getGug_data_history_no());

		vo.setWork_state_cd("D040"); //작업대기상태
		dtService.gugDataConfigUpdate(vo);
		dtService.gugDataConfigSummaryUpdate();//데이타 요약화

		return "";
	}

	//실행(RUN)
	@RequestMapping(value="/run_command",method=RequestMethod.POST,produces = "application/text; charset=utf8")
	@ResponseBody
	public String run_command(@ModelAttribute("GugPcmdCrExecComponent") GugPcmdCrExecComponent gugPcmdCrExec) throws Exception{
		logger.info("gugPcmdCrExec.getWork_command_create_no()=" +gugPcmdCrExec.getWork_command_create_no());
		
		GugPcmdCrExecComponent vo=service.gugPcmdCrExecView(gugPcmdCrExec.getWork_command_create_no());
		String key = AES256Util.getKeys();    //key는 16자 이상
		AES256Util aes256 = new AES256Util(key);
		URLCodec codec = new URLCodec();
		String decEms_pwd="";
		String decCli_pw="";
		String decConfirm_pwd="";
		if(vo.getEms_pwd()==null) {
			decEms_pwd="NULL";
		}else {
			if(vo.getEms_pwd().trim().equals("")) {
				decEms_pwd="NULL";
			}else {
				String encEms_pwd=vo.getEms_pwd(); //암호문
				decEms_pwd = aes256.aesDecode(codec.decode(encEms_pwd));//암호문을 평문으로 복호화
			}
		}
		
		if(vo.getCli_pwd()==null) {
			decCli_pw="NULL";
		}else {
			if(vo.getCli_pwd().trim().equals("")) {
				decCli_pw="NULL";
			}else {
				String encCli_pw=vo.getCli_pwd(); //암호문
				decCli_pw = aes256.aesDecode(codec.decode(encCli_pw));//암호문을 평문으로 복호화
			}
		}
		
		if(vo.getConfirm_pwd()==null) {
			decConfirm_pwd="NULL";
		}else {
			if(vo.getConfirm_pwd().trim().equals("")) {
				decConfirm_pwd="NULL";
			}else {
				String encConfirm_pwd=vo.getConfirm_pwd(); //암호문
				decConfirm_pwd = aes256.aesDecode(codec.decode(encConfirm_pwd));//암호문을 평문으로 복호화
			}
		}
		
		
		logger.info("vo.getWork_command_create_no()=" +vo.getWork_command_create_no());
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("mmc",vo.getCreate_command_sentence());
		
		map.put("ems_pwd",decEms_pwd);
		map.put("cli_pwd",decCli_pw);
		map.put("confirm_pwd",decConfirm_pwd);
		

		//실행이력번호 가져오기
		int seq_no=service.gugPcmdCrExecGetHistoryNo(vo);
		
		vo.setExec_history_no(seq_no); //실행이력번호
		vo.setExecter_id("admin"); //실행자아이디
		vo.setExec_state_cd("DOO4"); //실행 상태 코드 running
		/*"D001";"ready"
		"D002";"success"
		"D003";"fail"
		"D004";"running"*/
		
		//실행중 상태 저장
		service.gugPcmdCrExecHistoryInsert(vo);
		
		//RESTIF 실행 요청
		Map<String, Object> reMap=EquipCmdRifCall.callRestIF(map);
		
		//실행완료 후 성공 실패 메시 저장
		vo.setExec_state_cd(String.valueOf(reMap.get("exec_state_cd"))); //실행 상태 코드 success(D002)/fail(D003)
		vo.setExec_return_message (String.valueOf(reMap.get("exec_return_message"))); //실행 리턴 메시지
		vo.setExecter_id("admin");
		service.gugPcmdCrExecHistoryUpdate(vo);
		
		
		
		/*  
		GugDataConfigComponent gvo = new GugDataConfigComponent();
		gvo.setGug_data_manage_no(gugPcmdCrExec.getGug_data_manage_no());
		gvo.setGug_data_history_no(gugPcmdCrExec.getGug_data_history_no());
		gvo.setWork_state_cd("D040"); //작업대기상태
		dtService.gugDataConfigUpdate(gvo);
		dtService.gugDataConfigSummaryUpdate();//데이타 요약화
		*/
		Gson gson = new Gson();
		String s = gson.toJson(reMap);
		logger.debug(s);
		return s;
	}

}

