package com.kt.e2e.Controller;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.kt.e2e.Common.CommonPager;
import com.kt.e2e.Domain.UserComponent;
import com.kt.e2e.Service.UserService;
 
@Controller
@RequestMapping("/user")
public class UserController {
 
    
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private UserService service;

    //게시글 목록
    @RequestMapping(method=RequestMethod.GET)
    public ModelAndView list(@RequestParam(defaultValue="subject") String searchOption,
            @RequestParam(defaultValue="") String keyword,
            @RequestParam(defaultValue="1") int curPage
    		
             ) throws Exception{
    	
        logger.info("searchOption:"+ searchOption);
        logger.info("keyword:"+ keyword);
        // 검색옵션, 키워드 맵에 저장
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("searchOption", searchOption);
        map.put("keyword", keyword);
        
        //검색옵션에 맞추어 카운트 가져오기
    	int count =service.userCount(map);
    	
    	
        // 페이지 나누기 관련 처리
        int pageScale = 4; // 페이지당 게시물 수
        int blockScale = 10;  // 화면당 페이지 수
        CommonPager commonPager = new CommonPager(count, curPage,pageScale,blockScale);
        map.put("start", commonPager.getPageBegin());
        map.put("end", commonPager.getPageEnd());
        map.put("curPage", commonPager.getCurPage());
        map.put("pageScale", commonPager.getPageScale());
//        List<BoardVO> list = service.boardList(map); //ROWNUM방식
        List<UserComponent> list = service.userListOffset(map); //OFFSET방식

        
        ModelAndView mav = new ModelAndView();
        mav.setViewName("user/userList");
        // 댓글의 수를 맵에 저장 : 댓글이 존재하는 게시물의 삭제처리 방지하기 위해 
        mav.addObject("count", count); 
        mav.addObject("list", list);
        mav.addObject("curPage", curPage);
        mav.addObject("searchOption", searchOption);
        mav.addObject("keyword", keyword);
        mav.addObject("commonPager", commonPager);
        logger.info("mav:"+ mav);
        return mav;

    }
    
    //게시글 목록안에서 정렬(올림차순/내림차순)
	@RequestMapping(value="/jsondata", method=RequestMethod.GET)
	@ResponseBody
    public String listJsondata(
            @RequestParam(defaultValue="1") int curPage,
            @RequestParam(defaultValue="") String colName,
            @RequestParam(defaultValue="") String ordOption,
            @RequestParam(defaultValue="") String orderKey
             ) throws Exception{
    	

        // 검색옵션, 키워드 맵에 저장
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("orderKey", orderKey);
        
        //검색옵션에 맞추어 카운트 가져오기
    	int count =service.userCount(map);
    	
    	
        // 페이지 나누기 관련 처리
        int pageScale = 4; // 페이지당 게시물 수
        int blockScale = 10;  // 화면당 페이지 수
        CommonPager commonPager = new CommonPager(count, curPage,pageScale,blockScale);
        map.put("start", commonPager.getPageBegin());
        map.put("end", commonPager.getPageEnd());
        map.put("curPage", commonPager.getCurPage());
        map.put("pageScale", commonPager.getPageScale());
        
        //정렬옵션
        map.put("colName", colName);
        map.put("ordOption", ordOption);

        
        logger.debug(orderKey);
        
//      List<BoardVO> list = service.boardList(map); //ROWNUM방식
 		Gson gson = new Gson();
		List<UserComponent> list =service.userListOffset(map);
		String s = gson.toJson(list);
		logger.debug(s);
		return s;

    }

}

