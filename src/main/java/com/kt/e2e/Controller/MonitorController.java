package com.kt.e2e.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value="/monitoring")
public class MonitorController extends BaseController {

	//private static final Logger logger = LoggerFactory.getLogger(MonitorController.class);
	
	MonitorController() { setBaseName("monitoring"); }
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String monitor() {
		return view("monitor");
	}
}
