package com.kt.e2e.Controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestRestController {

	private static final Logger logger = LoggerFactory.getLogger(TestRestController.class);
	
	@RequestMapping(value="/rest1", method = RequestMethod.POST)
	public String save(@RequestParam Map<String, Object> map) {
		logger.info("restapi call");
		if(map.size() > 0) logger.info("hasKey");
		return "success";
	}
	
}
