package com.kt.e2e.Controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kt.e2e.Service.PlteTestService;

@Controller
@RequestMapping(value="/plteTest")
public class PlteTestController extends BaseController {
	
	private static final Logger logger = LoggerFactory.getLogger(PlteTestController.class);
	
	PlteTestController() { setBaseName("plteTest"); }
	
	@Autowired
	private PlteTestService service;
	
	
	//TODO test for Dev
	@RequestMapping(value="/edit/{key}/{subKey}", method=RequestMethod.GET)
	public String test(@PathVariable("key") String key, @PathVariable String subKey, Model model) {
		if(key != null && key.equals("test") && subKey != null && subKey.equals("true")) {
			model.addAttribute("isTest",Boolean.TRUE);
		}
		return index(model);
	}
		
	@RequestMapping(value="/edit/{key}", method=RequestMethod.GET)
	public String edit(@PathVariable("key") String key, Model model) {
		model.addAttribute("key", key);
		return index(model);
	}
	
	@RequestMapping(value="/{type}", method=RequestMethod.GET)
	public String index(@PathVariable("type") String nsdType, Model model) {
		model.addAttribute("type", nsdType);
		return index(model);
	}


	@RequestMapping(value="/", method=RequestMethod.GET)
	public String index(Model model) {
		if(!model.containsAttribute("type")) {
			//NEW
			model.addAttribute("type", "new");
			
			//TODO for test
			model.addAttribute("type", "5g");
		}
		
		return view("plteTest");
	}
	
	/**
	 * 
	 * @param type - nsdType
	 * @return
	 */
	@RequestMapping(value="/component/list/{type}", method=RequestMethod.GET)
	@ResponseBody
	public String getComponentList(@PathVariable(value="type") String type) {
		return service.getComponentList(type);
	}
	
	@RequestMapping(value="/data/{key}", method=RequestMethod.GET)
	@ResponseBody
	public HashMap<String, Object> getData(@PathVariable String key) {
		logger.debug("called getData with Key="+key);
		HashMap<String, Object> hm = new HashMap<String, Object>();
		String jsonStr = service.loadComponents(key);
		hm.put("data", jsonStr);
		hm.put("err", 0); //TODO set error
		return hm;
	}
	
	@RequestMapping(value="/create/{key}", method=RequestMethod.POST)
	public String createData(@PathVariable String key, @RequestParam Map<String, Object> map) {
		map.put("isNew", Boolean.TRUE);
		return update(key, map);
	}
	
	@RequestMapping(value="/update/{key}", method=RequestMethod.POST)
	public String update(@PathVariable String key, @RequestParam Map<String, Object> map) {
		if(map.size() == 0) return "nodata";
		for(String k : map.keySet()) logger.debug("key="+k);
				
		if(!map.containsKey("data")) return "nodata2";
		
		logger.debug("key????="+key);
		Boolean isNew = map.containsKey("isNew")?(Boolean)map.get("isNew"):false;
//		String[] testArr = (String[])((String) map.get("data")).split("@");
		
		String ret = service.saveComponents(key, isNew, (String) map.get("data"), (String) map.get("data1"));
		
		if(ret == null) return "success";
//		return "error : " + ret;
		return "true";
	}
	
}
