package com.kt.e2e.Controller;

public class BaseController {

	private String basename;
	
	public void setBaseName(String basename) {
		this.basename = basename;
	}
	
	public String view(String filename) {
		return basename + "/" + filename;
	}
}
