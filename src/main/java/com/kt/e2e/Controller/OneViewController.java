package com.kt.e2e.Controller;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kt.e2e.Common.CommonPager;
import com.kt.e2e.Domain.InvNfvoComponent;
import com.kt.e2e.Domain.NsdmNsdInfoComponent;
import com.kt.e2e.Domain.OneViewComponent;
import com.kt.e2e.Service.OneViewService;

@Controller
@RequestMapping(value="/oneView")
public class OneViewController extends BaseController {
	
	private static final Logger logger = LoggerFactory.getLogger(OneViewController.class);
	
	OneViewController() { setBaseName("oneView"); }
	
	@Autowired
	private OneViewService service;

	/**
	 * 디자인 리스트 조회
	 * @param type
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/list", method=RequestMethod.GET)
	@ResponseBody
	public ModelAndView getOneViewList(@RequestParam(defaultValue="nsd_id") String searchOption,
    			@RequestParam(defaultValue="") String keyword,
    		@RequestParam(defaultValue="1") int curPage
    		, OneViewComponent oneView) throws Exception {

        // 페이지 나누기 관련 처리
        int pageScale = 10; // 페이지당 게시물 수
        int blockScale = 10;  // 화면당 페이지 수
		
        logger.info("searchOption:"+searchOption);
        logger.info("keyword:"+keyword);
        // 검색옵션, 키워드 맵에 저장
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("searchOption", searchOption);
        map.put("keyword", keyword);
        
		int count = service.getOneViewListCount(map);

        CommonPager commonPager = new CommonPager(count, curPage,pageScale,blockScale);
        map.put("start", commonPager.getPageBegin());
        map.put("end", commonPager.getPageEnd());
        map.put("curPage", commonPager.getCurPage());
        map.put("pageScale", commonPager.getPageScale());
		
        List<OneViewComponent> list = service.getOneViewList(map); // List 조회
        
        ModelAndView mav = new ModelAndView();
        mav.setViewName("oneView/list");
        mav.addObject("list", list);
        mav.addObject("count", count);
        mav.addObject("curPage", curPage);
        mav.addObject("searchOption", searchOption);
        mav.addObject("keyword", keyword);
        mav.addObject("commonPager", commonPager);

		for(int i = 0; i < 3; i++) {
			oneView.setSearch_type(i);

			int searchCount = service.getOneViewTypeCount(oneView);

			if(i == 0) {
				mav.addObject("typeCount", Integer.toString(searchCount));  // 4G COUNT
			} else if(i == 1) {
				mav.addObject("typeCount2", Integer.toString(searchCount)); // 5G COUNT
			} else {
				mav.addObject("typeCount3", Integer.toString(searchCount)); // P-LTE COUNT
			}
		}
        
        return mav;
	}
	
    // 목록안에서 정렬(올림차순/내림차순)
	@RequestMapping(value="/jsondata", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
    public String listJsondata(@RequestParam(defaultValue="nsd_id") String searchOption,
			@RequestParam(defaultValue="") String keyword,
		@RequestParam(defaultValue="1") int curPage,
        @RequestParam(defaultValue="") String colName,
        @RequestParam(defaultValue="") String ordOption,
        @RequestParam(defaultValue="") String orderKey
             ) throws Exception{
    	

        // 검색옵션, 키워드 맵에 저장
        Map<String, Object> map = new HashMap<String, Object>();
//        map.put("orderKey", orderKey);

        //검색옵션에 맞추어 카운트 가져오기
    	int count = service.getOneViewListCount(map);
    	
        // 페이지 나누기 관련 처리
        int pageScale = 10; // 페이지당 게시물 수
        int blockScale = 10;  // 화면당 페이지 수
        CommonPager commonPager = new CommonPager(count, curPage,pageScale,blockScale);
        map.put("start", commonPager.getPageBegin());
        map.put("end", commonPager.getPageEnd());
        map.put("curPage", commonPager.getCurPage());
        map.put("pageScale", commonPager.getPageScale());
        
        //정렬옵션
        map.put("colName", colName);
        map.put("ordOption", ordOption);
        
//        logger.debug(orderKey);

 		Gson gson = new Gson();
		List<OneViewComponent> list =service.getOneViewList(map);
		logger.debug(list.toString());
		String s = gson.toJson(list);
		logger.debug(s);
		return s;

    }
	
	
	/**
	 * oneView 화면 호출시 앞,뒤 관계 조회
	 * @param nsdType
	 * @param model
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/design/{type}/{lSlide}/{rSlide}", method=RequestMethod.GET)
	public ModelAndView index(@PathVariable("type") String nsdType
			, @PathVariable("lSlide") String lSlide
			, @PathVariable("rSlide") String rSlide
			, Model model) throws Exception {
		
		ModelAndView mav = new ModelAndView();
		
		
		logger.info("lSlide : " + lSlide);
		logger.info("rSlide : " + rSlide);
		
		model.addAttribute("type", nsdType);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map = service.getOrderNum(nsdType); // 앞,뒤 관계 조회

		model.addAttribute("prev_nsd_id", map.get("prev_nsd_id"));
		model.addAttribute("next_nsd_id", map.get("next_nsd_id"));

		mav.addObject("prev_nsd_id", map.get("prev_nsd_id")); // 이전 nsd_id
		mav.addObject("next_nsd_id", map.get("next_nsd_id")); // 이후 nsd_id
		
		logger.info("prev : " + map.get("prev_nsd_id"));
		logger.info("next : " + map.get("next_nsd_id"));
		
		mav.addObject("lSlide", lSlide); // 좌측 슬라이드 on 여부
		mav.addObject("rSlide", rSlide); // 우측 슬라이드 on 여부
		
		mav.setViewName("oneView/design");
		return mav;
	}
	
	/**
	 * oneView 화면 호출시 앞,뒤 관계 조회
	 * @param nsdType
	 * @param model
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/design/{type}/{lSlide}/{rSlide}/{test}", method=RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> testIndex(@PathVariable("type") String nsdType
			, @PathVariable("lSlide") String lSlide
			, @PathVariable("rSlide") String rSlide
			, @PathVariable("test") String test
			, Model model) throws Exception {
		
//		ModelAndView mav = new ModelAndView();
		
		logger.info("lSlide : " + lSlide);
		logger.info("rSlide : " + rSlide);
		
		model.addAttribute("type", nsdType);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map = service.getOrderNum(nsdType); // 앞,뒤 관계 조회

//		model.addAttribute("prev_nsd_id", map.get("prev_nsd_id"));
//		model.addAttribute("next_nsd_id", map.get("next_nsd_id"));

//		mav.addObject("prev_nsd_id", map.get("prev_nsd_id")); // 이전 nsd_id
//		mav.addObject("next_nsd_id", map.get("next_nsd_id")); // 이후 nsd_id
		
//		logger.info("prev : " + map.get("prev_nsd_id"));
//		logger.info("next : " + map.get("next_nsd_id"));
		
//		mav.addObject("lSlide", lSlide); // 좌측 슬라이드 on 여부
//		mav.addObject("rSlide", rSlide); // 우측 슬라이드 on 여부
		
//		mav.setViewName("oneView/design");
		return map;
	}

	/**
	 * oneView 화면 호출
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/design", method=RequestMethod.GET)
	public ModelAndView getDesign(Model model) {
		
		ModelAndView mav = new ModelAndView();
		
		if(!model.containsAttribute("type")) { // 신규 일경우
			mav.addObject("type", "new");
			mav.addObject("uuid", UUID.randomUUID()); // UUID 생성
		}

        mav.setViewName("oneView/design");

        return mav;
	}
	
	/**
	 * 좌측 아이콘 정보 조회
	 * @param type
	 * @return
	 */
	@RequestMapping(value="/design/component/list/{type}", method=RequestMethod.GET)
	@ResponseBody
	public String getComponentList(@PathVariable(value="type") String type) {
		return service.getComponentList(type);
	}
	
	/**
	 * 우측 데이터 및 팝업시 데이터
	 * @param key
	 * @return
	 */
	@RequestMapping(value="/design/data/{key}", method=RequestMethod.GET)
	@ResponseBody
	public String getData(@PathVariable String key) {
		logger.debug("called getData with Key="+key);
		HashMap<String, Object> hm = new HashMap<String, Object>();
		String jsonStr = service.loadComponents(key);
		hm.put("data", jsonStr);
		hm.put("err", 0); //TODO set error
		return new Gson().toJson(hm);
	}
	
	/**
	 * 토플로지 및 정보 생성 또는 업데이트
	 * @param key
	 * @param map
	 * @return
	 */
	@RequestMapping(value="/design/save/{key}", method=RequestMethod.POST)
	@ResponseBody
	public String save(@PathVariable String key, @RequestParam Map<String, Object> map) {
		if(map.size() == 0) return "nodata";
		for(String k : map.keySet()) logger.debug("key="+k);
				
		if(!map.containsKey("data")) return "nodata2";

		Boolean isNew = map.get("isNew").equals("true") ? true : false;

		String ret = service.saveComponents(key, isNew, (String) map.get("data"), (String) map.get("data1"));
		
		if(ret == null) return "success";
		return "error : " + ret;
	}
	
	/**
	 * Boarding 작업
	 * @param key
	 * @return
	 */
    @RequestMapping(value="/design/onBoard/{key}", method=RequestMethod.GET,produces = "application/text; charset=utf8")
    @ResponseBody
    public String onBoard(@RequestParam HashMap<String, String> paramMap) throws Exception{
		
    	String resultString			= "";
    	logger.info( "### paramMap : " + paramMap );
    	if( paramMap != null ){
    		
    		String type 			= paramMap.get( "type" ).toString();
    		String nsInsId 			= paramMap.get( "nsInsId" ).toString();
    	
    		logger.info( "### type : " + type );
    		logger.info( "### nsInsId : " + nsInsId );
    		try {

    			Map<String, Object> map = new HashMap<String, Object>();
    	    	map.put("type"		, type);
    	    	map.put("nsInsId"	, nsInsId);
    	    	
//    	    	List<ProvisionNotification> list = service.selectNotificationList( map );
    	    	Map<String, Object> resultMap = new HashMap<String, Object>();

//    	    	if( list != null && list.size() > 0 ){
//    				ProvisionNotification notiVo = list.get( 0 );
//        	    	resultMap.put("nsInsId"			, notiVo.getNs_ins_id());
//        	    	resultMap.put("lcOpOcc"			, notiVo.getLc_op_occ());
//        	    	resultMap.put("operation"		, notiVo.getOperation());
//        	    	resultMap.put("notiType"		, notiVo.getNoti_type());
//        	    	resultMap.put("notiStatus"		, notiVo.getNoti_status());
//        	    	resultMap.put("operationStatus"	, notiVo.getOperation_status());
//        	    	resultMap.put("affectedVnf"		, notiVo.getAffected_vnf());
//        	    	resultMap.put("affctedVl"		, notiVo.getAffcted_vl());
//        	    	resultMap.put("error"			, notiVo.getError());
//        	    	resultMap.put("createAt"		, notiVo.getCreated_at());
//        	    	resultMap.put("errorCode"		, 0 );
//        	    	resultMap.put("errorMessage"	, "" );
//        	    	resultMap.put("returnMessage"	, "complete" );
//    			}else{
//        	    	resultMap.put("errorCode"		, 0 );
//        	    	resultMap.put("errorMessage"	, "" );
//        	    	resultMap.put("returnMessage"	, "tacking" );    				
//    			}
    	    	
    			Gson gson = new Gson();
    			resultString = gson.toJson(resultMap);
    			
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
    	}
    	logger.info( "### resultString : " + resultString );
    	return resultString;
    }
	
	/**
	 * 상태 변경
	 * @param key
	 * @return
	 */
	@RequestMapping(value="/design/updateStatus/{key}", method=RequestMethod.POST)
	public String updateStatus(@PathVariable(value="key") String key) {
		
		return "true";
	}
	
	/**
	 * General Info 조회
	 * @param type
	 * @return
	 */
	@RequestMapping(value="/design/nsd/{type}", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
	public String getNsdL(@PathVariable(value="type") String type) throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("nsdId", type);

		return service.getNsd(map);
	}

	/**
	 * Deployment Flavor 조회 (NSDF, VNF연관)
	 * @param type
	 * @return
	 */
	@RequestMapping(value="/design/nsdf/{nsdfId}", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
	public String getNsdmNsdf(@PathVariable(value="nsdfId") String nsdfId) throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("nsdfId", nsdfId);

		return service.getNsdmNsdf(map);
	}
	
	/**
	 * Deployment Flavor 조회 (VNF, VL연관)
	 * @param type
	 * @return
	 */
	@RequestMapping(value="/design/vl/{nsdfId}", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
	public String getNsdmProfile(@PathVariable(value="nsdfId") String nsdfId) throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("nsdfId", nsdfId);

		return service.getNsdmProfile(map);
	}

	/**
	 * API호출 (Tracking)
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/design/restfulPost", method=RequestMethod.GET,produces = "application/text; charset=utf8")
    @ResponseBody
    public String testRestfulPost(@RequestParam HashMap<String, String> paramMap) throws Exception{
    	
    	logger.info( "### map scan Start");
    	
    	for(String key : paramMap.keySet()){
    		logger.info( "### model => "+key + " : " + paramMap.get(key).toString());
    	}
    	logger.info( "### map scan End");

    	String resultString		= "";
		if( paramMap != null ){

	    	String url 			= paramMap.get( "url" ).toString();
	    	String port 		= paramMap.get( "port" ).toString();
	    	String method 		= paramMap.get( "method" ).toString();
	    	
	    	logger.debug("### : " + url + " $$$ : " + port + " @@@ " + method);
	    	
	    	try {
	    		
	        	URL restUrl = new URL(url);
	        	HttpURLConnection urlcon = (HttpURLConnection)restUrl.openConnection(); 

	        	urlcon.setDoOutput(true);
	        	urlcon.setDoInput(true);
	        	
	        	urlcon.setRequestMethod("POST"); 
	        	
	        	StringBuffer restParam = new StringBuffer();
	        	boolean isFirst = true;
	        	for(String key : paramMap.keySet()){
	        		if( !"url".equals( key ) && !"port".equals( key ) && !"method".equals( key ) ){
	        			if( !isFirst ){
	        				restParam.append("&");
	        			}else{
	        				isFirst = false;
	        			}
	        			restParam.append(key+"='"+paramMap.get(key).toString()+"'");
	        		}
	        	}
	        	logger.info( "### restParam : "+restParam.toString());
	        	//String str = "nsdId='26bb4cc5-b04b-4b6d-8fad-f15a39db97ce'&nsName='NS-5G Service Basic - PYEONGCHANG'&nsDescription='This Network Service consists of SM, CU, GW - U, GW - C'";
	        	
//	        	PrintWriter out = new PrintWriter( urlcon.getOutputStream() );
//	        	out.println(str);
	        	
	        	OutputStream opstrm = urlcon.getOutputStream();
	        	opstrm.write(restParam.toString().getBytes());
	        	opstrm.flush();
	        	opstrm.close();

	        	InputStream in = urlcon.getInputStream();
	        	int resCode = urlcon.getResponseCode(); 
	        	
	        	logger.info( "### resCode : " + resCode );
	        	
	        	StringBuffer sb = new StringBuffer();
	        	Scanner scan = new Scanner( in );
	        	int line = 1;
	        	while( scan.hasNext() ){
	        		sb.append(scan.nextLine());
	        	}
	        	scan.close();
	        	
	        	logger.info( "### sb : " + sb.toString() );
	        	
	        	
	            Gson gson = new Gson();
	            resultString = gson.toJson(sb.toString());
	            
	            JsonParser parser = new JsonParser();
	            JsonObject test = (JsonObject)parser.parse(sb.toString());

//	            Map<String, Object> map = new HashMap<String, Object>();
	            
//	            map.put("id"			        , test.get("id").toString().replace("\"", "") );
//	            map.put("nsdId"			        , test.get("nsdId").toString().replace("\"", "") );
//	            map.put("nsdName"		        , test.get("nsdName").toString().replace("\"", "") );
//	            map.put("nsdVersion"		    , test.get("nsdVersion").toString().replace("\"", "") );
//	            map.put("nsdDesigner"		    , test.get("nsdDesigner").toString().replace("\"", "") );
//	            map.put("onboardedVnfPkgInfoIds", test.get("onboardedVnfPkgInfoIds").toString().replace("\"", "") );
//	            map.put("nsdInfoState"		    , test.get("nsdInfoState").toString().replace("\"", "") );
//	            map.put("nsd_op_state"		    , test.get("nsdDesigner").toString().replace("\"", "") );
//	            map.put("nsdUsageState"		    , test.get("nsdUsageState").toString().replace("\"", "") );
//	            map.put("userDefinedData"       , test.get("userDefinedData").toString().replace("\"", "") );
//	            map.put("createAt"		           , nsd.getCreated_at() );
//	            logger.info(" ### Insert Result Table : " + map);
	            
//	            service.insertNsdmNsdInfo(map); // Onboarding Insert
	            
			} catch (Exception e) {
//				logger.debug(" ★★★★★ 이ㅣ :" + e);
//				logger.debug(" ★★★★★ 이프린트 : " + e.printStackTrace() );
				e.printStackTrace();
				Map<String, Object> map = new HashMap<String, Object>();
				Gson gson = new Gson();

				map.put("errorMessage", e.getMessage());
				resultString = e.getMessage();
//    			return resultString = gson.toJson(map);
    			return resultString;
//    			resultString = resultString;
				
			}
		}
    	    	
		return resultString;
    }
	
	
	@RequestMapping(value="/design/orderTracking", method=RequestMethod.GET,produces = "application/text; charset=utf8")
    @ResponseBody
    public String orderTracking(@RequestParam HashMap<String, String> paramMap) throws Exception{

    	String resultString			= "";
    	logger.info( "### paramMap : " + paramMap );
    	if( paramMap != null ){
    		
    		String id 			= paramMap.get( "id" ).toString();
    		String nsdId		= paramMap.get( "nsdId" ).toString();
    	
    		logger.info( "### id : "   + id );
    		logger.info( "### nsdId : " + nsdId );
    		try {

    			Map<String, Object> map = new HashMap<String, Object>();
    	    	map.put("id"	, id);
    	    	map.put("nsdId"	, nsdId);
    	    	
    	    	List<NsdmNsdInfoComponent> list = service.getNsdmnsdInfo( map );
    	    	
    	    	Map<String, Object> resultMap = new HashMap<String, Object>();

    	    	if( list != null && list.size() > 0 ){
    	    		NsdmNsdInfoComponent nsdmNsdInfo = list.get( 0 );
    				
        	    	resultMap.put("id"			             , nsdmNsdInfo.getId() );
        	    	resultMap.put("nsd_id"			         , nsdmNsdInfo.getNsd_Id() );
        	    	resultMap.put("nsd_name"		         , nsdmNsdInfo.getNsd_name() );
        	    	resultMap.put("nsd_version"		         , nsdmNsdInfo.getNsd_version() );
        	    	resultMap.put("nsd_designer"		     , nsdmNsdInfo.getNsd_designer() );
        	    	resultMap.put("onboarded_vnf_pkg_inf_ids", nsdmNsdInfo.getOnboarded_vnf_pkg_inf_ids() );
        	    	resultMap.put("nsd_inf_state"		     , nsdmNsdInfo.getNsd_inf_state() );
        	    	resultMap.put("nsd_op_state"		     , nsdmNsdInfo.getNsd_op_state() );
        	    	resultMap.put("nsd_usage_state"			 , nsdmNsdInfo.getNsd_usage_state() );
//        	    	resultMap.put("user_defined"             , nsdmNsdInfo.getUser_defined() );
        	    	resultMap.put("createAt"		         , nsdmNsdInfo.getCreated_at() );
        	    	resultMap.put("errorCode"		         , 0 );
        	    	resultMap.put("errorMessage"	         , "" );
        	    	resultMap.put("returnMessage"	         , "complete" );
    			}else{
        	    	resultMap.put("errorCode"		, 0 );
        	    	resultMap.put("errorMessage"	, "" );
        	    	resultMap.put("returnMessage"	, "tacking" );    				
    			}
    	    	
    			Gson gson = new Gson();
    			resultString = gson.toJson(resultMap);
    			
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
    	}
    	logger.info( "### resultString : " + resultString );
    	return resultString;
    }
	
	/**
	 * Target System 조회
	 * @param type
	 * @return
	 */
	@RequestMapping(value="/design/targetList", method=RequestMethod.GET)
	@ResponseBody
	public String getInvNfvo() throws Exception {
		
 		Gson gson = new Gson();
 		List<InvNfvoComponent> list = service.getInvNfvo(); // NSD의 profile 조회
		logger.debug(list.toString());
		String s = gson.toJson(list);
		logger.debug(s);
		return s;
	}
	
    // NSD 및 토폴로지 삭제
    @RequestMapping(value="/delete/{nsdId}", method=RequestMethod.POST,produces = "application/text; charset=utf8")
    @ResponseBody
    public String deleteNsd(@PathVariable("nsdId") String nsdId) throws Exception{
//        service.deleteNsd(nsdId);
//        
//        return "redirect://localhost:8080/oneView/list";
    	
//		String nsdId = type;
    	service.deleteNsd(nsdId);
		return "";
    	
    }
	
}
