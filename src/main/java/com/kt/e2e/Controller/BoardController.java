package com.kt.e2e.Controller;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.kt.e2e.Common.CommonPager;
import com.kt.e2e.Common.EquipCmdLoad;
import com.kt.e2e.Service.BoardService;
import com.kt.e2e.Domain.BoardComponent;
 
@Controller
@RequestMapping("/board")
public class BoardController {
 
    
    private static final Logger logger = LoggerFactory.getLogger(BoardController.class);
	@Autowired
	private BoardService service;

    //게시글 목록
    @RequestMapping(method=RequestMethod.GET)
    public ModelAndView list(@RequestParam(defaultValue="subject") String searchOption,
            @RequestParam(defaultValue="") String keyword,
            @RequestParam(defaultValue="1") int curPage
             ) throws Exception{
    	
        logger.info("searchOption:"+searchOption);
        logger.info("keyword:"+keyword);
        // 검색옵션, 키워드 맵에 저장
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("searchOption", searchOption);
        map.put("keyword", keyword);
        
        //검색옵션에 맞추어 카운트 가져오기
    	int count =service.boardCount(map);
    	
    	
        // 페이지 나누기 관련 처리
        int pageScale = 2; // 페이지당 게시물 수
        int blockScale = 2;  // 화면당 페이지 수
        CommonPager commonPager = new CommonPager(count, curPage,pageScale,blockScale);
        map.put("start", commonPager.getPageBegin());
        map.put("end", commonPager.getPageEnd());
        map.put("curPage", commonPager.getCurPage());
        map.put("pageScale", commonPager.getPageScale());
//        List<BoardVO> list = service.boardList(map); //ROWNUM방식
        List<BoardComponent> list = service.boardListOffset(map); //OFFSET방식

        
        ModelAndView mav = new ModelAndView();
        mav.setViewName("board/boardList");
        // 댓글의 수를 맵에 저장 : 댓글이 존재하는 게시물의 삭제처리 방지하기 위해 
        mav.addObject("count", count); 
        mav.addObject("list", list);
        mav.addObject("curPage", curPage);
        mav.addObject("searchOption", searchOption);
        mav.addObject("keyword", keyword);
        mav.addObject("commonPager", commonPager);
        logger.info("mav:"+ mav);
        return mav;

    }
    
    //게시글 목록안에서 정렬(올림차순/내림차순)
	@RequestMapping(value="/jsondata", method=RequestMethod.GET)
	@ResponseBody
    public String listJsondata(
            @RequestParam(defaultValue="1") int curPage,
            @RequestParam(defaultValue="") String colName,
            @RequestParam(defaultValue="") String ordOption,
            @RequestParam(defaultValue="") String orderKey
             ) throws Exception{
    	

        // 검색옵션, 키워드 맵에 저장
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("orderKey", orderKey);
        
        //검색옵션에 맞추어 카운트 가져오기
    	int count =service.boardCount(map);
    	
    	
        // 페이지 나누기 관련 처리
        int pageScale = 2; // 페이지당 게시물 수
        int blockScale = 2;  // 화면당 페이지 수
        CommonPager commonPager = new CommonPager(count, curPage,pageScale,blockScale);
        map.put("start", commonPager.getPageBegin());
        map.put("end", commonPager.getPageEnd());
        map.put("curPage", commonPager.getCurPage());
        map.put("pageScale", commonPager.getPageScale());
        
        //정렬옵션
        map.put("colName", colName);
        map.put("ordOption", ordOption);

        
        logger.debug(orderKey);
        
//      List<BoardVO> list = service.boardList(map); //ROWNUM방식
 		Gson gson = new Gson();
		List<BoardComponent> list =service.boardListOffset(map);
		String s = gson.toJson(list);
		logger.debug(s);
		return s;

    }



    
    //게시글 작성 페이지(GET)    
    @RequestMapping(value="/post",method=RequestMethod.GET)
    public ModelAndView writeForm() throws Exception{
		 
		 EquipCmdLoad.test();
		 logger.info("3333333333" +EquipCmdLoad.file_path);
        return new ModelAndView("board/boardWrite");
    }
    
    //게시글 DB에 등록(POST)
    @RequestMapping(value="/post",method=RequestMethod.POST)
    public String write(@ModelAttribute("BoardComponent") BoardComponent board) throws Exception{
 
    	 logger.info("3333333333" +board.getInputNo());

        String[] inputNo=board.getInputNo();
        if(inputNo!=null){
	        for(int i=0 ;i<inputNo.length;i++)
	        {
	            logger.info(inputNo[i]);

	        }
        }
    			
    			
    	service.boardInsert(board);
        
        //return "redirect://localhost:8080/board";
    	HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    	logger.info(request.getContextPath()+":"+request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort());
        return "redirect://"+request.getServerName()+":"+request.getServerPort()+"/board";
    }
    

    //게시글 상세 페이지
    @RequestMapping(value="/{bno}",method=RequestMethod.GET)
    public ModelAndView view(@PathVariable("bno") int bno) throws Exception{
        
        BoardComponent board = service.boardView(bno);
        service.hitPlus(bno);
        
        return new ModelAndView("board/boardView","board",board);
    }
    
    //게시글 수정 페이지(GET)
    @RequestMapping(value="/post/{bno}", method=RequestMethod.GET)
    public ModelAndView updateForm(@PathVariable("bno") int bno) throws Exception{
        
        BoardComponent board = service.boardView(bno);
        
        return new ModelAndView("board/boardUpdate","board",board);
    }
    
    //게시글 수정(PUT)
    @RequestMapping(value="/post/{bno}", method=RequestMethod.PUT)
    public String update(@ModelAttribute("BoardComponent")BoardComponent board,@PathVariable("bno") int bno) throws Exception{
        
        service.boardUpdate(board);
        
        //return "redirect://localhost:8080/board/"+bno;
    	HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    	logger.info(request.getContextPath()+":"+request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort());
        return "redirect://"+request.getServerName()+":"+request.getServerPort()+"/board/"+bno;
    }
    
    //게시글 삭제(DELETE)
    @RequestMapping(value="/delete/{bno}", method=RequestMethod.DELETE)
    public String delete(@PathVariable("bno") int bno) throws Exception{
    	logger.info("333");
        service.boardDelete(bno);
        
        //return "redirect://localhost:8080/board";
    	HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    	logger.info(request.getContextPath()+":"+request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort());
        return "redirect://"+request.getServerName()+":"+request.getServerPort()+"/board";

    }
    
    //게시글 선택삭제(DELETE)
    @RequestMapping(value="/delete/select", method=RequestMethod.DELETE)
    public String delete(@ModelAttribute("BoardComponent")BoardComponent board) throws Exception{
    	
    	
        int bno=0;

        String[] checkNo=board.getCheckNo();
        if(checkNo!=null){
	        for(int i=0 ;i<checkNo.length;i++)
	        {
	            logger.info(checkNo[i]);
	        	bno=Integer.valueOf(checkNo[i]);
	            service.boardDelete(bno);
	        }
        }

        
        //return "redirect://localhost:8080/board";
    	HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    	logger.info(request.getContextPath()+":"+request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort());
        return "redirect://"+request.getServerName()+":"+request.getServerPort()+"/board";

    }
   
}

