package com.kt.e2e.Controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.kt.e2e.Service.TopologyEditorService;

@RestController
@RequestMapping(value= {"/topologyEditor","/provisioning","/monitoring"})
public class TopologyEditorRestController {

	private static final Logger logger = LoggerFactory.getLogger(TopologyEditorController.class);
	
	@Autowired
	private TopologyEditorService editorService;
	
	/**
	 * 
	 * @param type - nsdType
	 * @return
	 */
	@RequestMapping(value="/component/list/{type}", method=RequestMethod.GET)
	public String getComponentList(@PathVariable(value="type") String type) {
		return editorService.getComponentList(type);
	}
	
	/*
	public HashMap<String, Object> getData(@PathVariable String key) {
		logger.debug("called getData with Key="+key);
		HashMap<String, Object> hm = new HashMap<String, Object>();
		String jsonStr = editorService.loadComponents(key);
		hm.put("data", jsonStr);
		hm.put("err", 0); //TODO set error
		return hm;
	}*/
	
	@RequestMapping(value="/data/{key}", method=RequestMethod.GET)
	public String getData(@PathVariable String key) {
		logger.debug("called getData with Key="+key);
		HashMap<String, Object> hm = new HashMap<String, Object>();
		String jsonStr = editorService.loadComponents(key);
		hm.put("data", jsonStr);
		hm.put("err", 0); //TODO set error
		
		return new Gson().toJson(hm);
	}
	
	@RequestMapping(value="/create/{key}", method=RequestMethod.POST)
	public String createData(@PathVariable String key, @RequestParam Map<String, Object> map) {
		map.put("isNew", Boolean.TRUE);
		return update(key, map);
	}
	
	@RequestMapping(value="/update/{key}", method=RequestMethod.POST)
	public String update(@PathVariable String key, @RequestParam Map<String, Object> map) {
		if(map.size() == 0) return "nodata";
		for(String k : map.keySet()) logger.debug("key="+k);
				
		if(!map.containsKey("data")) return "nodata2";
		
		logger.debug("key????="+key);
		Boolean isNew = map.containsKey("isNew")?(Boolean)map.get("isNew"):false;
		String ret = editorService.saveComponents(key, isNew, (String)map.get("data"));
		
		if(ret == null) return "success";
		return "error : " + ret;
	}
	
	/************* file save test, NOT COMPLETED
	@Autowired
	ServletContext context;
	
	@RequestMapping(value="/save/{key}", method=RequestMethod.POST)
	public String save(@PathVariable String key, @RequestParam Map<String, Object> map) {
		String ret;
		try {
			String path = context.getRealPath("resources/data/") + key;
			BufferedWriter writer = new BufferedWriter(new FileWriter(path));
			writer.write((String)map.get("data"));
			writer.close();
			ret = "success";
		} catch (IOException ioe) {
			ret = "ioerror : " + ioe.getMessage();
			logger.error(ret);
		} catch (Exception ex) {
			ret = "exception : " + ex.getMessage();
			logger.warn(ret);
		}
		return "success";
	}
	***/
	
}
