package com.kt.e2e.Controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.kt.e2e.Common.TestStorage;
import com.kt.e2e.Service.TestService;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping(value="/test")
public class TestController {
	
	private static final Logger logger = LoggerFactory.getLogger(TestController.class);
	
	@Autowired
	private TestService testService;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.debug("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "test/test";
	}
	
	@RequestMapping(value = "/1/{test}", method = RequestMethod.GET)
	public String test2(@PathVariable("test") String test, Locale locale, Model model) {
		
		logger.debug("test2");
		model.addAttribute("key","key??");
		model.addAttribute("key","__k");
		model.addAttribute("key",test);
		//logger.debug("test2 called " + test);
		//model.addAttribute("test",test);
		return test3(model);
	}
	
	@RequestMapping(value="/1", method=RequestMethod.GET)
	public String test3(Model model) {
		return "test/test2";
	}
	
	@RequestMapping(value= {"/3/{key}","/2/{id}"})
	public String test4(@PathVariable String key, Model model) {
		logger.debug(key);
		return "test/test2";
	}
			
	@RequestMapping(value="/{key}")
	public String anonymous1(@PathVariable String key, Model model) {
		return "test/" + key;
	}
	
	@RequestMapping(value="/edit", method = RequestMethod.GET)
	public String edit(Model model) {
		logger.info("edit view called");
		model.addAttribute("isNew",Boolean.TRUE);
		return "test/edit";
	}
	
	@RequestMapping(value="/save/{key}/new", method = RequestMethod.POST)
	@ResponseBody
	public String saveNew(@PathVariable("key") String key, @RequestParam Map<String, Object> map) {
		logger.debug("key:"+key+", isNew");
		return save(key, true, map);
	}
		
	@RequestMapping(value="/save/{key}", method = RequestMethod.POST)
	@ResponseBody
	public String update(@PathVariable("key") String key, @RequestParam Map<String, Object> map) {
		//TODO key is null checking
		logger.debug("key:"+key);
		//TODO CHECK KEY
		
		return save(key, false, map);
	}
	
	private String save(String key, boolean isNew, Map<String, Object> map) {
		if(map.size() > 0) {
			String err = testService.saveComponents(key, isNew, (String)map.get(key));
			if(err == null)
				return "success";	//success
			return err;				//error
		}
		return "nodata";			//no data
	}
	
	@RequestMapping(value="/result", method = RequestMethod.GET)
	public String result() {
		logger.info("called result view");
		return "test/result";
	}
	
	@RequestMapping(value="/saved/{key}", method = RequestMethod.GET)
	@ResponseBody
	public HashMap<String, Object> saved(@PathVariable("key") String key) {
		//TODO key is null checking
		logger.info("called saved data, key=" + key);
		HashMap<String, Object> hm = new HashMap<String, Object>();
		String jsonStr = testService.loadComponents(key);
		hm.put(key, jsonStr);
		hm.put("err", 0);
		return hm;
	}
	
	@RequestMapping(value="/data", method=RequestMethod.GET)
	@ResponseBody
	public String getDataJson() {
		logger.debug("enter getData");
		Gson gson = new Gson();
		List<Object> list = TestStorage.getComponents("5g");
		String s = gson.toJson(list);
		logger.debug(s);
		return s;
	}
}
