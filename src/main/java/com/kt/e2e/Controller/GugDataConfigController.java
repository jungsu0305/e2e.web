package com.kt.e2e.Controller;
 
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.kt.e2e.Common.CommonPager;
import com.kt.e2e.Service.CustService;
import com.kt.e2e.Service.EquipCmdBasService;
import com.kt.e2e.Service.GugDataConfigService;
import com.kt.e2e.Service.GugPcmdCrExecService;
import com.kt.e2e.Domain.CustComponent;
import com.kt.e2e.Domain.DataSummaryComponent;
import com.kt.e2e.Domain.EquipCmdBasComponent;
import com.kt.e2e.Domain.GugDataConfigComponent;
import com.kt.e2e.Domain.GugDataConfigDetSelComponent;
import com.kt.e2e.Domain.GugPcmdCrExecComponent;
 
@Controller
@RequestMapping("/gugDataConfig")
public class GugDataConfigController {
 
    
    private static final Logger logger = LoggerFactory.getLogger(GugDataConfigController.class);

	@Autowired
	private GugDataConfigService service;
	
	@Autowired
	private GugPcmdCrExecService cxService;
	
	@Autowired
	private EquipCmdBasService ecbService;
	
	@Autowired
	private CustService custService;
    //국데이타 목록
    @RequestMapping(method=RequestMethod.GET)
    public ModelAndView list(@RequestParam(defaultValue="modify_dtm") String searchOption,
            @RequestParam(defaultValue="") String keyword_fdate,
            @RequestParam(defaultValue="") String keyword_tdate,
            @RequestParam(defaultValue="profile_nm") String searchOption2,
            @RequestParam(defaultValue="") String keyword2,
            @RequestParam(defaultValue="1") int curPage,
            @RequestParam(defaultValue="") String work_state_cd
             ) throws Exception{
    	
        logger.info("searchOption:"+searchOption);
        logger.info("keyword_fdate:"+keyword_fdate);
        logger.info("keyword_tdate:"+keyword_tdate);
        logger.info("searchOption2:"+searchOption2);
        logger.info("keyword2:"+keyword2);
        logger.info("curPage:"+curPage);
//        String replace_list[] ={"년","월","일"," "};
//        for(int i=0;i<replace_list.length;i++)
//        {
//	        keyword_fdate=keyword_fdate.replaceAll(replace_list[i], "");
//	        keyword_tdate=keyword_tdate.replaceAll(replace_list[i], "");
//        }
        // 검색옵션, 키워드 맵에 저장
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("searchOption", searchOption);
        map.put("keyword_fdate", keyword_fdate);
        map.put("keyword_tdate", keyword_tdate);
        map.put("searchOption2", searchOption2);
        map.put("keyword2", keyword2);
        map.put("work_state_cd", work_state_cd);        
        
        //검색옵션에 맞추어 카운트 가져오기
    	int count =service.gugDataConfigCount(map);
    	
    	  logger.info("count:"+count);
        // 페이지 나누기 관련 처리
        int pageScale = 10; // 페이지당 게시물 수
        int blockScale = 10;  // 화면당 페이지 수
        CommonPager commonPager = new CommonPager(count, curPage,pageScale,blockScale);
        map.put("start", commonPager.getPageBegin());
        map.put("end", commonPager.getPageEnd());
        map.put("curPage", commonPager.getCurPage());
        map.put("pageScale", commonPager.getPageScale());
        List<GugDataConfigComponent> list = service.gugDataConfigListOffset(map); //OFFSET방식
        DataSummaryComponent summaryItem= service.gugDataConfigSummarySelect(1); //국데이타 요약화 조회
        
        if(keyword_fdate.length() >=8)
        {
        	keyword_fdate=keyword_fdate.substring(0, 4) + "년 "
						+ keyword_fdate.substring(4, 6) + "월 "
						+ keyword_fdate.substring(6, 8) + "일";
        logger.info("keyword_fdate:"+keyword_fdate.substring(0, 4));
        logger.info("keyword_fdate:"+keyword_fdate.substring(4, 6));
        logger.info("keyword_fdate:"+keyword_fdate.substring(6, 8));
        }
        
        if(keyword_tdate.length() >=8)
        {
        	keyword_tdate=keyword_tdate.substring(0, 4) + "년 "
						+ keyword_tdate.substring(4, 6) + "월 "
						+ keyword_tdate.substring(6, 8) + "일";
        logger.info("keyword_tdate:"+keyword_tdate.substring(0, 4));
        logger.info("keyword_tdate:"+keyword_tdate.substring(4, 6));
        logger.info("keyword_tdate:"+keyword_tdate.substring(6, 8));
        }
        logger.info("keyword_tdate:"+keyword_tdate);
        
        ModelAndView mav = new ModelAndView();
        mav.setViewName("gugDataConfig/gugDataConfigList");
        mav.addObject("count", count); 
        mav.addObject("list", list);
        mav.addObject("summaryItem", summaryItem);
        mav.addObject("curPage", curPage);
        mav.addObject("searchOption", searchOption);
        mav.addObject("keyword_fdate", keyword_fdate);
        mav.addObject("keyword_tdate", keyword_tdate);       
        mav.addObject("searchOption2", searchOption2);
        mav.addObject("keyword2", keyword2);
        mav.addObject("work_state_cd", work_state_cd);
        mav.addObject("commonPager", commonPager);
        logger.info("mav:"+ mav);
        return mav;

    }
    
    
    //하나의 국데이타 항목변경 이력목록
	@RequestMapping(value="/list_his", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
    public String one_gugdate_his_list(
            @RequestParam(defaultValue="") BigInteger gug_data_manage_no,
            @RequestParam(defaultValue="1") int curPage
            
             ) throws Exception{
        Map<String, Object> map = new HashMap<String, Object>();
		map.put("gug_data_manage_no",gug_data_manage_no);
		 GugDataConfigComponent gugDataConfig = service.gugDataConfigView(map);
        logger.info("curPage:"+curPage);
        
        //검색옵션에 맞추어 카운트 가져오기
    	int count =service.gugDataConfigHisCount(map);
    	
    	  logger.info("count:"+count);
        // 페이지 나누기 관련 처리
        int pageScale = 5; // 페이지당 게시물 수
        int blockScale = 10;  // 화면당 페이지 수
        CommonPager commonPager = new CommonPager(count, curPage,pageScale,blockScale);
        map.put("start", commonPager.getPageBegin());
        map.put("end", commonPager.getPageEnd());
        map.put("curPage", commonPager.getCurPage());
        map.put("pageScale", commonPager.getPageScale());
        map.put("count",count);
        List<GugDataConfigComponent> his_list = service.gugDataConfigHisList(map);
        
        map.put("profile_nm", gugDataConfig.getProfile_nm());  
        map.put("work_state_cd", gugDataConfig.getWork_state_cd());    
        map.put("work_state_nm", gugDataConfig.getWork_state_nm());            
        map.put("work_ept_dt", gugDataConfig.getWork_ept_dt());
        map.put("work_ept_start_time", gugDataConfig.getWork_ept_start_time());
        map.put("work_ept_close_time", gugDataConfig.getWork_ept_close_time());
        
        List<GugDataConfigComponent> his_compare_list = new ArrayList<GugDataConfigComponent>();
        for(int i=0;i<his_list.size();i++)
        {
        	GugDataConfigComponent gc=new GugDataConfigComponent();
        	gc=his_list.get(i);
        	his_compare_list.add(gc);
        	GugDataConfigComponent prvGc=new GugDataConfigComponent();
        	if((i+1)<his_list.size())
        	{
        		prvGc=his_list.get(i+1);
        	}
        	
        	if(gc.getVal_chng_col_nm()!=null && (!gc.getVal_chng_col_nm().equals("")) )
        	{
	        	String[] gmm=gc.getVal_chng_col_nm().split(Pattern.quote("|"));
	        	for(int jj=0;jj<gmm.length;jj++)
	        	{
	        		GugDataConfigComponent gmmval=new GugDataConfigComponent();
	        		gmmval.setChng_col_nm("");
        			gmmval.setModify_id("- 국 마스터");
	        		if(gmm[jj].equals("profile_nm")){
		        		gmmval.setChng_col_nm("프로파일명");
	        			gmmval.setPrev_col_val(prvGc.getProfile_nm());
	    				gmmval.setNext_col_val(gc.getProfile_nm());
	        		}else if(gmm[jj].equals("work_ept_dt")){
		        		gmmval.setChng_col_nm("작업예정일");
	        			gmmval.setPrev_col_val(prvGc.getWork_ept_dt());
	    				gmmval.setNext_col_val(gc.getWork_ept_dt());	
	        		}else if(gmm[jj].equals("work_ept_start_time")){
		        		gmmval.setChng_col_nm("작업예정시작일시");
	        			gmmval.setPrev_col_val(prvGc.getWork_ept_start_time());
	    				gmmval.setNext_col_val(gc.getWork_ept_start_time());
		        	}else if(gmm[jj].equals("work_ept_close_time")){
		        		gmmval.setChng_col_nm("작업예정종료일시");
	        			gmmval.setPrev_col_val(prvGc.getWork_ept_close_time());
	    				gmmval.setNext_col_val(gc.getWork_ept_close_time());
		        	}
	        		if((!gmmval.getChng_col_nm().equals(""))
	        			&& gmmval.getChng_col_nm()!=null )
	        			his_compare_list.add(gmmval);
	        		
	        	}
        	}
        	
        	if(gc.getEpcc_val_chng_col_nm()!=null && (!gc.getEpcc_val_chng_col_nm().equals("")) )
        	{
	        	String[] epcc=gc.getEpcc_val_chng_col_nm().split(Pattern.quote("|"));
	        	for(int jj=0;jj<epcc.length;jj++)
	        	{
	
	        		GugDataConfigComponent gmmval=new GugDataConfigComponent();
	        		gmmval.setChng_col_nm(epcc[jj].toUpperCase());
        			gmmval.setModify_id("- EPCC 파라메터");
	        		if(epcc[jj].equals("pgw_ip")){
	        			gmmval.setPrev_col_val(prvGc.getPgw_ip());
	        			gmmval.setNext_col_val(gc.getPgw_ip());
	        		}else if(epcc[jj].equals("sms_recv")){
	        			gmmval.setPrev_col_val(prvGc.getSms_recv());
	        			gmmval.setNext_col_val(gc.getSms_recv());	
	        		}
	        		if((!gmmval.getChng_col_nm().equals(""))
	            			&& gmmval.getChng_col_nm()!=null )
	            			his_compare_list.add(gmmval);
	        		
	        	}
        	}
        	
        	if(gc.getHss_val_chng_col_nm()!=null && (!gc.getHss_val_chng_col_nm().equals("")) )
        	{
	        	String[] hss=gc.getHss_val_chng_col_nm().split(Pattern.quote("|"));
	        	for(int jj=0;jj<hss.length;jj++)
	        	{
	
	        		GugDataConfigComponent gmmval=new GugDataConfigComponent();
	        		gmmval.setChng_col_nm(hss[jj].toUpperCase());
        			gmmval.setModify_id("- HSS 파라메터");
	        		if(hss[jj].equals("id")){
	        			gmmval.setPrev_col_val(prvGc.getId());
	    				gmmval.setNext_col_val(gc.getId());
	        		}else if(hss[jj].equals("apn")){
	        			gmmval.setPrev_col_val(prvGc.getApn());
	    				gmmval.setNext_col_val(gc.getApn());	
	        		}else if(hss[jj].equals("apnoirepl")){
	        			gmmval.setPrev_col_val(prvGc.getApnoirepl());
	    				gmmval.setNext_col_val(gc.getApnoirepl());
		        	}else if(hss[jj].equals("pltcc")){
	        			gmmval.setPrev_col_val(prvGc.getPltcc());
	    				gmmval.setNext_col_val(gc.getPltcc());
		        	}else if(hss[jj].equals("uutype")){
	        			gmmval.setPrev_col_val(prvGc.getUutype());
	    				gmmval.setNext_col_val(gc.getUutype());
		        	}else if(hss[jj].equals("mpname_cd")){
	        			gmmval.setPrev_col_val(prvGc.getMpname_cd());
	    				gmmval.setNext_col_val(gc.getMpname_cd());
		        	}
	        		if((!gmmval.getChng_col_nm().equals(""))
	            			&& gmmval.getChng_col_nm()!=null )
	            			his_compare_list.add(gmmval);
	        		
	        	}
        	}
        	
        	if(gc.getMme_val_chng_col_nm()!=null && (!gc.getMme_val_chng_col_nm().equals("")) )
        	{
	        	String[] mme=gc.getMme_val_chng_col_nm().split(Pattern.quote("|"));
	        	for(int jj=0;jj<mme.length;jj++)
	        	{
	
	        		GugDataConfigComponent gmmval=new GugDataConfigComponent();
	        		gmmval.setChng_col_nm(mme[jj].toUpperCase());
        			gmmval.setModify_id("- MME 파라메터");
	        		if(mme[jj].equals("apn_fqdn")){
	        			gmmval.setPrev_col_val(prvGc.getApn_fqdn());
	    				gmmval.setNext_col_val(gc.getApn_fqdn());
	        		}else if(mme[jj].equals("pgwgrp")){
	        			gmmval.setPrev_col_val(prvGc.getPgwgrp());
	    				gmmval.setNext_col_val(gc.getPgwgrp());	
	        		}
	        		if((!gmmval.getChng_col_nm().equals(""))
	            			&& gmmval.getChng_col_nm()!=null )
	            			his_compare_list.add(gmmval);
	        		
	        	}
        	}
        	logger.info("gc.getVal_chng_col_nm()"+i+"="+gc.getVal_chng_col_nm());
        	logger.info("gc.getEpcc_val_chng_col_nm()"+i+"="+gc.getEpcc_val_chng_col_nm());
        	logger.info("gc.getHss_val_chng_col_nm()"+i+"="+gc.getHss_val_chng_col_nm());
        	logger.info("gc.getMme_val_chng_col_nm()"+i+"="+gc.getMme_val_chng_col_nm());
        }
        
        map.put("list_his",his_compare_list);
        map.put("count", count); 
        map.put("curPage", curPage);
        map.put("commonPager", commonPager);

 		Gson gson = new Gson();
		logger.debug(his_compare_list.toString());
		String s = gson.toJson(map);
		logger.debug(s);
		return s;

    }
    
	   //하나의 국데이타 장비 명령 확인 리스트 목록
		@RequestMapping(value="/list_cmd_ready", method=RequestMethod.GET,produces = "application/text; charset=utf8")
		@ResponseBody
	    public String one_gugdate_cmd_ready_list(
	            @RequestParam(defaultValue="1") int curPage,
				@RequestParam(defaultValue="") BigInteger gug_data_manage_no,
				@RequestParam(defaultValue="") int gug_data_history_no
	            
	             ) throws Exception{
	        Map<String, Object> map = new HashMap<String, Object>();
	        map.put("gug_data_manage_no", gug_data_manage_no);
	        map.put("gug_data_history_no", gug_data_history_no);
	        
	        //검색옵션에 맞추어 카운트 가져오기
	        int count =cxService.gugPcmdCrExecCount(map);
	    	
	    	  logger.info("count:"+count);
	        // 페이지 나누기 관련 처리
	        int pageScale = 10000; // 페이지당 게시물 수
	        int blockScale = 10;  // 화면당 페이지 수
	        CommonPager commonPager = new CommonPager(count, curPage,pageScale,blockScale);
	        map.put("start", commonPager.getPageBegin());
	        map.put("end", commonPager.getPageEnd());
	        map.put("curPage", commonPager.getCurPage());
	        map.put("pageScale", commonPager.getPageScale());
	        map.put("count",count);
	        List<GugPcmdCrExecComponent> list = cxService.gugPcmdCrExecListOffset(map);
	        Map<String, Object> gug_map = new HashMap<String, Object>();
	        gug_map.put("gug_data_manage_no",gug_data_manage_no);
	        gug_map.put("gug_data_history_no",null);
			 GugDataConfigComponent gugDataConfig = service.gugDataConfigView(gug_map);
		        logger.info("curPage:"+curPage);
		        // 검색옵션, 키워드 맵에 저장

		        map.put("profile_nm", gugDataConfig.getProfile_nm());  
		        map.put("work_state_cd", gugDataConfig.getWork_state_cd());    
		        map.put("work_state_nm", gugDataConfig.getWork_state_nm());            
		        map.put("work_ept_dt", gugDataConfig.getWork_ept_dt());
		        map.put("work_ept_start_time", gugDataConfig.getWork_ept_start_time());
		        map.put("work_ept_close_time", gugDataConfig.getWork_ept_close_time());
	        
	        map.put("list_his",list);

	 		Gson gson = new Gson();
			logger.debug(list.toString());
			String s = gson.toJson(map);
			logger.debug(s);
			return s;

	    }
		
		   //하나의 국데이타 장비 명령 실행 리스트 목록
			@RequestMapping(value="/list_cmd_exec", method=RequestMethod.GET,produces = "application/text; charset=utf8")
			@ResponseBody
		    public String one_gugdate_cmd_exec_list(
		            @RequestParam(defaultValue="1") int curPage,
					@RequestParam(defaultValue="") BigInteger gug_data_manage_no,
					@RequestParam(defaultValue="") int gug_data_history_no
		            
		             ) throws Exception{
		        Map<String, Object> map = new HashMap<String, Object>();
		        map.put("gug_data_manage_no", gug_data_manage_no);
		        map.put("gug_data_history_no", gug_data_history_no);
		        
		        //검색옵션에 맞추어 카운트 가져오기
		        int count =cxService.gugPcmdCrExecCount(map);
		    	
		    	  logger.info("count:"+count);
		        // 페이지 나누기 관련 처리
		        int pageScale = 10000; // 페이지당 게시물 수
		        int blockScale = 10;  // 화면당 페이지 수
		        CommonPager commonPager = new CommonPager(count, curPage,pageScale,blockScale);
		        map.put("start", commonPager.getPageBegin());
		        map.put("end", commonPager.getPageEnd());
		        map.put("curPage", commonPager.getCurPage());
		        map.put("pageScale", commonPager.getPageScale());
		        map.put("count",count);
		        List<GugPcmdCrExecComponent> list = cxService.gugPcmdCrExecListOffset(map);
		        Map<String, Object> gug_map = new HashMap<String, Object>();
		        gug_map.put("gug_data_manage_no",gug_data_manage_no);
		        gug_map.put("gug_data_history_no",null);
				 GugDataConfigComponent gugDataConfig = service.gugDataConfigView(gug_map);
			        logger.info("curPage:"+curPage);
			        // 검색옵션, 키워드 맵에 저장

			        map.put("profile_nm", gugDataConfig.getProfile_nm());  
			        map.put("work_state_cd", gugDataConfig.getWork_state_cd());    
			        map.put("work_state_nm", gugDataConfig.getWork_state_nm());            
			        map.put("work_ept_dt", gugDataConfig.getWork_ept_dt());
			        map.put("work_ept_start_time", gugDataConfig.getWork_ept_start_time());
			        map.put("work_ept_close_time", gugDataConfig.getWork_ept_close_time());
			        map.put("work_comp_dt", gugDataConfig.getWork_comp_dt());
		        map.put("list_his",list);

		 		Gson gson = new Gson();
				logger.debug(list.toString());
				String s = gson.toJson(map);
				logger.debug(s);
				return s;

		    }		
	
    //국데이타 목록안에서 정렬(올림차순/내림차순)
	@RequestMapping(value="/jsondata", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
    public String listJsondata(
            @RequestParam(defaultValue="1") int curPage,
            @RequestParam(defaultValue="") String colName,
            @RequestParam(defaultValue="") String ordOption,
            @RequestParam(defaultValue="") String orderKey
             ) throws Exception{
    	

        // 검색옵션, 키워드 맵에 저장
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("orderKey", orderKey);
        
        //검색옵션에 맞추어 카운트 가져오기
    	int count =service.gugDataConfigCount(map);
    	
    	
        // 페이지 나누기 관련 처리
        int pageScale = 10; // 페이지당 게시물 수
        int blockScale = 10;  // 화면당 페이지 수
        CommonPager commonPager = new CommonPager(count, curPage,pageScale,blockScale);
        map.put("start", commonPager.getPageBegin());
        map.put("end", commonPager.getPageEnd());
        map.put("curPage", commonPager.getCurPage());
        map.put("pageScale", commonPager.getPageScale());
        
        //정렬옵션
        map.put("colName", colName);
        map.put("ordOption", ordOption);

        
        logger.debug(orderKey);

 		Gson gson = new Gson();
		List<GugDataConfigComponent> list =service.gugDataConfigListOffset(map);
		logger.debug(list.toString());
		String s = gson.toJson(list);
		logger.debug(s);
		return s;

    }



    
    //국데이타 작성 초기 페이지(GET)    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/writeView",method=RequestMethod.GET)
    public ModelAndView writeView() throws Exception{
    	GugDataConfigComponent gugDataConfig=new GugDataConfigComponent();
    	gugDataConfig.setView_gubun("CREATE"); //입력중
    	BigInteger dno= new BigInteger("0");
    	gugDataConfig.setGug_data_manage_no(dno);
    	gugDataConfig.setGug_data_history_no(0);
    	gugDataConfig.setCust_manage_no(0);
    	  
        List<GugDataConfigDetSelComponent> mmeDetLst=new ArrayList();
        List<GugDataConfigDetSelComponent> epccDetLst=new ArrayList();
        List<GugDataConfigDetSelComponent> pgwLst=new ArrayList();
    	GugDataConfigDetSelComponent gdcds=new GugDataConfigDetSelComponent();
    	gdcds.setMme_det_no(1);
    	gdcds.setEpcc_det_no(1);
    	gdcds.setPgw_seq_no(1);
    	mmeDetLst.add(gdcds);
    	epccDetLst.add(gdcds);
    	pgwLst.add(gdcds);
        	
    	
   	 	ModelAndView mav = new ModelAndView();
        mav.setViewName("gugDataConfig/gugDataConfigWrite");
        mav.addObject("gugDataConfig", gugDataConfig); 
        mav.addObject("mmeDetLst", mmeDetLst); 
        mav.addObject("epccDetLst", epccDetLst);
        mav.addObject("pgwLst", pgwLst);
        
    	EquipCmdBasComponent ecbVo=new EquipCmdBasComponent();
    	ecbVo.setParameter_type("ENUM");
    	Map<String,Object> ecbMap=ecbService.equipCmdBasComboMapSelect(ecbVo);
        Iterator<String> keys = ecbMap.keySet().iterator();

        while( keys.hasNext() ){

            String key = keys.next();
            mav.addObject(key,(List)ecbMap.get(key));

        }

         logger.info("mav:"+ mav);
         return mav;

    }
    
    //국데이타 DB에 등록(POST)
    @RequestMapping(value="/post",method=RequestMethod.POST)
    public String write(@ModelAttribute("GugDataConfigComponent") GugDataConfigComponent gugDataConfig) throws Exception{
        gugDataConfig.setView_gubun("CREATE");
    	BigInteger gug_data_manage_no=gugDataConfig.getGug_data_manage_no();
    	BigInteger compareNo=new BigInteger("0");
    	int gug_data_history_no=gugDataConfig.getGug_data_history_no();
    	logger.info("check="+gug_data_manage_no);
    
    	logger.info("mme_check="+gugDataConfig.getMme_check());
    	logger.info("epcc_check="+gugDataConfig.getEpcc_check());
    	logger.info("pgw_check="+gugDataConfig.getPgw_check());
    	logger.info("hss_check="+gugDataConfig.getHss_check());

    	if (gug_data_manage_no.compareTo(compareNo) == 0){
    		logger.info("check");
    		gugDataConfig.setGug_data_manage_no(service.gugDataConfigGetManageNo());
    		
    	}
    	
    	logger.info("gugDataConfig.getCust_nm()="+gugDataConfig.getCust_nm());
    	
    	
    	if (gugDataConfig.getCust_manage_no()== 0){
    		logger.info("check");
    		gugDataConfig.setCust_manage_no(custService.custGetManageNo());
    		CustComponent vo =new CustComponent();
    		vo.setCust_manage_no(gugDataConfig.getCust_manage_no());
    		vo.setCust_nm(gugDataConfig.getCust_nm());
    		vo.setLte_id(gugDataConfig.getLte_id());
    		vo.setApn(gugDataConfig.getCust_apn());
    		vo.setIp_type(gugDataConfig.getIp_type());
    		vo.setService_type(gugDataConfig.getService_type());
    		vo.setDed_line_use_yn(gugDataConfig.getDed_line_use_yn());
    		vo.setRegist_id("admin");
    		custService.custInsert(vo);
    		
    	}
    	
    	
     logger.info("Gug_data_manage_no="+gugDataConfig.getGug_data_manage_no());
    	if(gug_data_history_no==0)	{
    		gugDataConfig.setGug_data_history_no(1);
    		
    	}else{
    		gugDataConfig.setGug_data_history_no(service.gugDataConfigGetHistoryNo(gugDataConfig));
    	}
    	logger.info("Gug_data_history_no="+gugDataConfig.getGug_data_history_no());
    	gugDataConfig.setWork_ept_dt(gugDataConfig.getWork_ept_dt().replaceAll("-", ""));
    	gugDataConfig.setWork_ept_dt(gugDataConfig.getWork_ept_dt().replaceAll("년", ""));
    	gugDataConfig.setWork_ept_dt(gugDataConfig.getWork_ept_dt().replaceAll("월", ""));
    	gugDataConfig.setWork_ept_dt(gugDataConfig.getWork_ept_dt().replaceAll("일", ""));
    	gugDataConfig.setWork_ept_start_time(gugDataConfig.getWork_ept_start_time().replaceAll(":", ""));
    	gugDataConfig.setWork_ept_close_time(gugDataConfig.getWork_ept_close_time().replaceAll(":", ""));

    	if(gugDataConfig.getId().equals(""))
    	{
    		gugDataConfig.setId("0"	);
    	}
    	gugDataConfig.setRegist_id("admin");  //세션처리해야 함
    	gugDataConfig.setModify_id("admin"); //세션처리해야 함
    	logger.info("gugDataConfig.getSelect_run_command()=" + gugDataConfig.getSelect_run_command());
    	if(!gugDataConfig.getSelect_run_command().equals("")){
	    	String[] select_run_command=gugDataConfig.getSelect_run_command().split(Pattern.quote(":"));
	    	if(select_run_command!=null){
	    		String gug_equip_type_cd_command="(";
		    	logger.info("select_run_command.length=" + select_run_command.length);
		    	for(int i=0;i<select_run_command.length;i++)
		    	{
		    		String[] run_command=select_run_command[i].split(Pattern.quote(","));
		    		gugDataConfig.setGug_equip_type_cd(run_command[0]);
		    		gugDataConfig.setCommand(run_command[1]);
		    		gug_equip_type_cd_command+=(i==0)?("'"+run_command[0]+run_command[1]):("','"+run_command[0]+run_command[1]);
		    		service.mergeSelectRunCommand(gugDataConfig);
		    	}
		    	gug_equip_type_cd_command+="')";
		    	logger.info("gug_equip_type_cd_command="+gug_equip_type_cd_command);
		    	 Map<String, Object> map2 = new HashMap<String, Object>();
		    	 map2.put("gug_data_manage_no",gugDataConfig.getGug_data_manage_no());
		    	 map2.put("gug_equip_type_cd_command",gug_equip_type_cd_command);
		    	service.commandSelManageDelete(map2);
	    	}
    	}
    	logger.info("gugDataConfigInsert");

    	if(gugDataConfig.getGug_data_history_no()>1)
    	{
            Map<String, Object> map = new HashMap<String, Object>();
    		map.put("gug_data_manage_no",gug_data_manage_no);
    		map.put("gug_data_history_no",gugDataConfig.getGug_data_history_no()-1);
    		GugDataConfigComponent gdPrevVo = service.gugDataConfigView(map);
    		gdPrevVo.setWork_ept_dt(gdPrevVo.getWork_ept_dt().replaceAll(" ", ""));
    		gdPrevVo.setWork_ept_dt(gdPrevVo.getWork_ept_dt().replaceAll("년", ""));
    		gdPrevVo.setWork_ept_dt(gdPrevVo.getWork_ept_dt().replaceAll("월", ""));
    		gdPrevVo.setWork_ept_dt(gdPrevVo.getWork_ept_dt().replaceAll("일", ""));
    		gdPrevVo.setWork_ept_start_time(gdPrevVo.getWork_ept_start_time().replaceAll(":", ""));
    		gdPrevVo.setWork_ept_close_time(gdPrevVo.getWork_ept_close_time().replaceAll(":", ""));

    		//tb_e2e_gug_data_manage_master  
    		gugDataConfig.setVal_chng_col_nm("");
//    		if(gdPrevVo.getCust_manage_no() != gugDataConfig.getCust_manage_no())
//    			gugDataConfig.setVal_chng_col_nm(gugDataConfig.getVal_chng_col_nm()+"|cust_manage_no");
    		if(!gdPrevVo.getProfile_nm().equals(gugDataConfig.getProfile_nm()))
    			gugDataConfig.setVal_chng_col_nm(gugDataConfig.getVal_chng_col_nm()+"|profile_nm");
    		if(!gdPrevVo.getWork_ept_dt().equals(gugDataConfig.getWork_ept_dt()))
    			gugDataConfig.setVal_chng_col_nm(gugDataConfig.getVal_chng_col_nm()+"|work_ept_dt");
    		if(!gdPrevVo.getWork_ept_start_time().equals(gugDataConfig.getWork_ept_start_time()))
    			gugDataConfig.setVal_chng_col_nm(gugDataConfig.getVal_chng_col_nm()+"|work_ept_start_time");
    		if(!gdPrevVo.getWork_ept_close_time().equals(gugDataConfig.getWork_ept_close_time()))
    			gugDataConfig.setVal_chng_col_nm(gugDataConfig.getVal_chng_col_nm()+"|work_ept_close_time");    		

    		//tb_e2e_mme_gug_data  
    		if(gugDataConfig.getMme_check()==null)
    		{
    			gugDataConfig.setApn_fqdn("");
    			gugDataConfig.setPgwgrp("");
    		}
    		gugDataConfig.setMme_val_chng_col_nm("");
    		if(!gdPrevVo.getApn_fqdn().equals(gugDataConfig.getApn_fqdn()))
    			gugDataConfig.setMme_val_chng_col_nm(gugDataConfig.getMme_val_chng_col_nm()+"|apn_fqdn");
    		if(!gdPrevVo.getPgwgrp().equals(gugDataConfig.getPgwgrp()))
    			gugDataConfig.setMme_val_chng_col_nm(gugDataConfig.getMme_val_chng_col_nm()+"|pgwgrp");
    		
    		logger.info("gugDataConfig.getMme_val_chng_col_nm()="+gugDataConfig.getMme_val_chng_col_nm());
    		
    		//tb_e2e_epcc_gug_data 
    		if(gugDataConfig.getEpcc_check()==null)
    		{
    			gugDataConfig.setPgw_ip("");
    			gugDataConfig.setSms_recv("");
    		}
    		gugDataConfig.setEpcc_val_chng_col_nm("");
    		if(!gdPrevVo.getPgw_ip().equals(gugDataConfig.getPgw_ip()))
    			gugDataConfig.setEpcc_val_chng_col_nm(gugDataConfig.getEpcc_val_chng_col_nm()+"|pgw_ip");
    		if(!gdPrevVo.getSms_recv().equals(gugDataConfig.getSms_recv()))
    			gugDataConfig.setEpcc_val_chng_col_nm(gugDataConfig.getEpcc_val_chng_col_nm()+"|sms_recv");
    		
    		//tb_e2e_hss_gug_data 
    		if(gugDataConfig.getHss_check()==null)
    		{
    			gugDataConfig.setId("");
    			gugDataConfig.setApn("");
    			gugDataConfig.setApnoirepl("");
    			gugDataConfig.setPltcc("");
    			gugDataConfig.setUutype("");
    			gugDataConfig.setMpname_cd("");
    		}
    		gugDataConfig.setHss_val_chng_col_nm("");
    		if(!gdPrevVo.getId().equals(gugDataConfig.getId()))
    			gugDataConfig.setHss_val_chng_col_nm(gugDataConfig.getHss_val_chng_col_nm()+"|id");
    		if(!gdPrevVo.getApn().equals(gugDataConfig.getApn()))
    			gugDataConfig.setHss_val_chng_col_nm(gugDataConfig.getHss_val_chng_col_nm()+"|apn");
    		if(!gdPrevVo.getApnoirepl().equals(gugDataConfig.getApnoirepl()))
    			gugDataConfig.setHss_val_chng_col_nm(gugDataConfig.getHss_val_chng_col_nm()+"|apnoirepl"); 
    		if(!gdPrevVo.getPltcc().equals(gugDataConfig.getPltcc()))
    			gugDataConfig.setHss_val_chng_col_nm(gugDataConfig.getHss_val_chng_col_nm()+"|pltcc"); 
    		if(!gdPrevVo.getUutype().equals(gugDataConfig.getUutype()))
    			gugDataConfig.setHss_val_chng_col_nm(gugDataConfig.getHss_val_chng_col_nm()+"|uutype"); 
    		if(!gdPrevVo.getMpname_cd().equals(gugDataConfig.getMpname_cd()))
    			gugDataConfig.setHss_val_chng_col_nm(gugDataConfig.getHss_val_chng_col_nm()+"|mpname_cd");     		
    	}
    	service.gugDataConfigInsert(gugDataConfig);
    	
    	logger.info("gugDataConfigMmeDetInsert");
    	 int[] mme_det_no=gugDataConfig.getMme_det_no();
    	 String[] pgw=gugDataConfig.getPgw();
     	logger.info("mme_det_no="+mme_det_no.length);
     	logger.info("pgw="+pgw.length);	 
         if(mme_det_no!=null){
 	        for(int i=0 ;i<mme_det_no.length;i++)
 	        {

 
 	        	GugDataConfigDetSelComponent gdcds=new GugDataConfigDetSelComponent();
 	        	gdcds.setGug_data_manage_no(gugDataConfig.getGug_data_manage_no());
 	        	gdcds.setGug_data_history_no(gugDataConfig.getGug_data_history_no());
 	        	gdcds.setMme_det_no(gugDataConfig.getMme_det_no()[i]);
    	        gdcds.setPgw((gugDataConfig.getPgw().length!=0)?gugDataConfig.getPgw()[i]:"");  
    	        gdcds.setIp((gugDataConfig.getIp().length!=0)?gugDataConfig.getIp()[i]:"");
    	        gdcds.setName((gugDataConfig.getName().length!=0)?gugDataConfig.getName()[i]:"");
    	        gdcds.setCapa((gugDataConfig.getCapa().length!=0)?gugDataConfig.getCapa()[i]:"");
 	        	if(gugDataConfig.getMme_check()==null)
    			{
 	        		gdcds.setPgw("");  
 	        		gdcds.setIp("");
 	        		gdcds.setName("");
 	        		gdcds.setCapa("");
    		
    			}
 	        	gdcds.setMme_det_val_chng_col_nm("");
 	        	service.gugDataConfigMmeDetInsert(gdcds);
 	        }
         }
         
         logger.info("gugDataConfigEpccDetInsert");
    	 int[] epcc_det_no=gugDataConfig.getEpcc_det_no();
         if(epcc_det_no!=null){
 	        for(int i=0 ;i<epcc_det_no.length;i++)
 	        {
 
 	        	GugDataConfigDetSelComponent gdcds=new GugDataConfigDetSelComponent();
 	        	gdcds.setGug_data_manage_no(gugDataConfig.getGug_data_manage_no());
 	        	gdcds.setGug_data_history_no(gugDataConfig.getGug_data_history_no());
 	        	gdcds.setEpcc_det_no(gugDataConfig.getEpcc_det_no()[i]);
    	        gdcds.setIp_pool((gugDataConfig.getIp_pool().length!=0)?gugDataConfig.getIp_pool()[i]:"");  
 	        	if(gugDataConfig.getEpcc_check()==null)
    			{
 	        		gdcds.setIp_pool("");  
    		
    			}
 	        	gdcds.setEpcc_det_val_chng_col_nm("");
 	        	service.gugDataConfigEpccDetInsert(gdcds);
 	        }
         }
         
         logger.info("gugDataConfigPgwInsert");
     	 int[] pgw_seq_no=gugDataConfig.getPgw_seq_no();
     	logger.info("gugDataConfigPgwInsert:pgw_seq_no.length"+pgw_seq_no.length);
          if(pgw_seq_no!=null){
  	        for(int i=0 ;i<pgw_seq_no.length;i++)
  	        {
  	        	logger.info("gugDataConfigPgwInsert"+gugDataConfig.getPgw_seq_no()[i]);
  	        	GugDataConfigDetSelComponent gdcds=new GugDataConfigDetSelComponent();
 	        	gdcds.setGug_data_manage_no(gugDataConfig.getGug_data_manage_no());
 	        	gdcds.setGug_data_history_no(gugDataConfig.getGug_data_history_no());
  	        	gdcds.setPgw_seq_no(gugDataConfig.getPgw_seq_no()[i]);
    	        gdcds.setIp_pool_id            ((gugDataConfig.getIp_pool_id().length!=0)?gugDataConfig.getIp_pool_id()[i]:"");                                               /* PGW 국 데이터.IP POOL ID--numeric(5,0) */ 
	  	        gdcds.setVr_id                 ((gugDataConfig.getVr_id              ().length!=0)?gugDataConfig.getVr_id              ()[i]:"");          /* PGW 국 데이터.VR_ID--numeric(2,0) */ 
	  	        gdcds.setPool_type_cd          ((gugDataConfig.getPool_type_cd       ().length!=0)?gugDataConfig.getPool_type_cd       ()[i]:"");          /* PGW 국 데이터.POOL TYPE 코드--varchar(7) */ 
	  	        gdcds.setStatic_cd             ((gugDataConfig.getStatic_cd          ().length!=0)?gugDataConfig.getStatic_cd          ()[i]:"");          /* PGW 국 데이터.STATIC 코드--varchar(3) */ 
	  	        gdcds.setTunnel_cd             ((gugDataConfig.getTunnel_cd          ().length!=0)?gugDataConfig.getTunnel_cd          ()[i]:"");          /* PGW 국 데이터.TUNNEL 코드--varchar(3) */ 
	  	        gdcds.setStart_addr            ((gugDataConfig.getStart_addr         ().length!=0)?gugDataConfig.getStart_addr         ()[i]:"");          /* PGW 국 데이터.START ADDR--varchar(64) */ 
	  	        gdcds.setVlan                  ((gugDataConfig.getVlan               ().length!=0)?gugDataConfig.getVlan               ()[i]:"");         /* PGW 국 데이터.VLAN--numeric(4,0) */ 
	  	        gdcds.setIp_addr               ((gugDataConfig.getIp_addr            ().length!=0)?gugDataConfig.getIp_addr            ()[i]:"");         /* PGW 국 데이터.IP ADDR--varchar(15) */ 
	  	        gdcds.setNetwork               ((gugDataConfig.getNetwork            ().length!=0)?gugDataConfig.getNetwork            ()[i]:"");         /* PGW 국 데이터.NETWORK--varchar(32) */ 
	  	        gdcds.setGateway               ((gugDataConfig.getGateway            ().length!=0)?gugDataConfig.getGateway            ()[i]:"");         /* PGW 국 데이터.GATEWAY--varchar(64) */ 
	  	        gdcds.setPrirank_cd            ((gugDataConfig.getPrirank_cd         ().length!=0)?gugDataConfig.getPrirank_cd         ()[i]:"");            /* PGW 국 데이터.우선순위 코드--varchar(64) */ 
	  	        gdcds.setIfc                   ((gugDataConfig.getIfc                ().length!=0)?gugDataConfig.getIfc                ()[i]:"");     /* PGW 국 데이터.I/F--varchar(16) */ 
	  	        gdcds.setApn_id                ((gugDataConfig.getApn_id             ().length!=0)?gugDataConfig.getApn_id             ()[i]:"");        /* PGW 국 데이터.APN ID--varchar(64) */ 
	  	        gdcds.setApn_name              ((gugDataConfig.getApn_name           ().length!=0)?gugDataConfig.getApn_name           ()[i]:"");   /* PGW 국 데이터.APN NAME--varchar(32) */ 
	  	        gdcds.setIp_alloc_cd           ((gugDataConfig.getIp_alloc_cd        ().length!=0)?gugDataConfig.getIp_alloc_cd        ()[i]:"");      /* PGW 국 데이터.IP ALLOC 코드--varchar(5) */ 
	  	        gdcds.setRad_ip_alloc_cd       ((gugDataConfig.getRad_ip_alloc_cd    ().length!=0)?gugDataConfig.getRad_ip_alloc_cd    ()[i]:"");          /* PGW 국 데이터.RAD IP ALLOC 코드--varchar(3) */ 
	  	        gdcds.setAuth_pcrf_pcc_cd      ((gugDataConfig.getAuth_pcrf_pcc_cd   ().length!=0)?gugDataConfig.getAuth_pcrf_pcc_cd   ()[i]:"");           /* PGW 국 데이터.AUTH/PCRF/PCC 코드--varchar(3) */ 
	  	        gdcds.setAcct_react_cd         ((gugDataConfig.getAcct_react_cd      ().length!=0)?gugDataConfig.getAcct_react_cd      ()[i]:"");        /* PGW 국 데이터.ACCT/REACT 코드--varchar(3) */ 
	  	        gdcds.setPri_dns_v4            ((gugDataConfig.getPri_dns_v4         ().length!=0)?gugDataConfig.getPri_dns_v4         ()[i]:"");     /* PGW 국 데이터.PRI DNS V4--varchar(64) */ 
	  	        gdcds.setSec_dns_v4            ((gugDataConfig.getSec_dns_v4         ().length!=0)?gugDataConfig.getSec_dns_v4         ()[i]:"");     /* PGW 국 데이터.SEC DNS V4--varchar(64) */ 
	  	        gdcds.setRad_auth_id           ((gugDataConfig.getRad_auth_id        ().length!=0)?gugDataConfig.getRad_auth_id        ()[i]:"");      /* PGW 국 데이터.RAD AUTH ID--varchar(64) */ 
	  	        gdcds.setRad_auth_act          ((gugDataConfig.getRad_auth_act       ().length!=0)?gugDataConfig.getRad_auth_act       ()[i]:"");       /* PGW 국 데이터.RAD AUTH ACT--varchar(64) */ 
	  	        gdcds.setRad_auth_sby          ((gugDataConfig.getRad_auth_sby       ().length!=0)?gugDataConfig.getRad_auth_sby       ()[i]:"");       /* PGW 국 데이터.RAD AUTH SBY--varchar(64) */ 
	  	        gdcds.setRad_acct_id           ((gugDataConfig.getRad_acct_id        ().length!=0)?gugDataConfig.getRad_acct_id        ()[i]:"");      /* PGW 국 데이터.RAD ACCT ID--varchar(64) */ 
	  	        gdcds.setRad_acct_act          ((gugDataConfig.getRad_acct_act       ().length!=0)?gugDataConfig.getRad_acct_act       ()[i]:"");       /* PGW 국 데이터.RAD ACCT ACT--varchar(64) */ 
	  	        gdcds.setRad_acct_sby          ((gugDataConfig.getRad_acct_sby       ().length!=0)?gugDataConfig.getRad_acct_sby       ()[i]:"");       /* PGW 국 데이터.RAD ACCT SBY--varchar(64) */ 
	  	        gdcds.setDiam_pcfr_id          ((gugDataConfig.getDiam_pcfr_id       ().length!=0)?gugDataConfig.getDiam_pcfr_id       ()[i]:"");       /* PGW 국 데이터.DIAM PCFR ID--varchar(64) */ 
	  	        gdcds.setDiam_pcfr_act         ((gugDataConfig.getDiam_pcfr_act      ().length!=0)?gugDataConfig.getDiam_pcfr_act      ()[i]:"");        /* PGW 국 데이터.DIAM PCFR ACT--varchar(64) */ 
	  	        gdcds.setDiam_pcfr_sby         ((gugDataConfig.getDiam_pcfr_sby      ().length!=0)?gugDataConfig.getDiam_pcfr_sby      ()[i]:"");        /* PGW 국 데이터.DIAM PCFR SBY--varchar(64) */ 
	  	        gdcds.setLocal_pcc_pf_id       ((gugDataConfig.getLocal_pcc_pf_id    ().length!=0)?gugDataConfig.getLocal_pcc_pf_id    ()[i]:"");          /* PGW 국 데이터.LOCAL PCC PF ID--varchar(64) */ 
	  	        gdcds.setLocal_pcc_pf_act      ((gugDataConfig.getLocal_pcc_pf_act   ().length!=0)?gugDataConfig.getLocal_pcc_pf_act   ()[i]:"");           /* PGW 국 데이터.LOCAL PCC PF ACT--varchar(64) */ 
	  	        gdcds.setLocal_pcc_pf_sby      ((gugDataConfig.getLocal_pcc_pf_sby   ().length!=0)?gugDataConfig.getLocal_pcc_pf_sby   ()[i]:"");           /* PGW 국 데이터.LOCAL PCC PF SBY--varchar(64) */ 
	  	        gdcds.setRule_base_id          ((gugDataConfig.getRule_base_id       ().length!=0)?gugDataConfig.getRule_base_id       ()[i]:"");       /* PGW 국 데이터.RULE BASE ID--varchar(64) */ 
	  	        gdcds.setArp_map_high          ((gugDataConfig.getArp_map_high       ().length!=0)?gugDataConfig.getArp_map_high       ()[i]:"");       /* PGW 국 데이터.ARP MAP HIGH--varchar(64) */ 
	  	        gdcds.setArp_map_med           ((gugDataConfig.getArp_map_med        ().length!=0)?gugDataConfig.getArp_map_med        ()[i]:"");      /* PGW 국 데이터.ARP MAP MED--varchar(64) */ 
 	        	if(gugDataConfig.getPgw_check() ==null)
    			{
 	      	        gdcds.setIp_pool_id            ("");          /* PGW 국 데이터.IP POOL ID--numeric(5,0) */ 
 		  	        gdcds.setVr_id                 ("");          /* PGW 국 데이터.VR_ID--numeric(2,0) */ 
 		  	        gdcds.setPool_type_cd          ("");          /* PGW 국 데이터.POOL TYPE 코드--varchar(7) */ 
 		  	        gdcds.setStatic_cd             ("");          /* PGW 국 데이터.STATIC 코드--varchar(3) */ 
 		  	        gdcds.setTunnel_cd             ("");          /* PGW 국 데이터.TUNNEL 코드--varchar(3) */ 
 		  	        gdcds.setStart_addr            ("");          /* PGW 국 데이터.START ADDR--varchar(64) */ 
 		  	        gdcds.setVlan                  ("");         /* PGW 국 데이터.VLAN--numeric(4,0) */ 
 		  	        gdcds.setIp_addr               ("");         /* PGW 국 데이터.IP ADDR--varchar(15) */ 
 		  	        gdcds.setNetwork               ("");         /* PGW 국 데이터.NETWORK--varchar(32) */ 
 		  	        gdcds.setGateway               ("");         /* PGW 국 데이터.GATEWAY--varchar(64) */ 
 		  	        gdcds.setPrirank_cd            ("");            /* PGW 국 데이터.우선순위 코드--varchar(64) */ 
 		  	        gdcds.setIfc                   ("");     /* PGW 국 데이터.I/F--varchar(16) */ 
 		  	        gdcds.setApn_id                ("");        /* PGW 국 데이터.APN ID--varchar(64) */ 
 		  	        gdcds.setApn_name              ("");   /* PGW 국 데이터.APN NAME--varchar(32) */ 
 		  	        gdcds.setIp_alloc_cd           ("");      /* PGW 국 데이터.IP ALLOC 코드--varchar(5) */ 
 		  	        gdcds.setRad_ip_alloc_cd       ("");          /* PGW 국 데이터.RAD IP ALLOC 코드--varchar(3) */ 
 		  	        gdcds.setAuth_pcrf_pcc_cd      ("");           /* PGW 국 데이터.AUTH/PCRF/PCC 코드--varchar(3) */ 
 		  	        gdcds.setAcct_react_cd         ("");        /* PGW 국 데이터.ACCT/REACT 코드--varchar(3) */ 
 		  	        gdcds.setPri_dns_v4            ("");     /* PGW 국 데이터.PRI DNS V4--varchar(64) */ 
 		  	        gdcds.setSec_dns_v4            ("");     /* PGW 국 데이터.SEC DNS V4--varchar(64) */ 
 		  	        gdcds.setRad_auth_id           ("");      /* PGW 국 데이터.RAD AUTH ID--varchar(64) */ 
 		  	        gdcds.setRad_auth_act          ("");       /* PGW 국 데이터.RAD AUTH ACT--varchar(64) */ 
 		  	        gdcds.setRad_auth_sby          ("");       /* PGW 국 데이터.RAD AUTH SBY--varchar(64) */ 
 		  	        gdcds.setRad_acct_id           ("");      /* PGW 국 데이터.RAD ACCT ID--varchar(64) */ 
 		  	        gdcds.setRad_acct_act          ("");       /* PGW 국 데이터.RAD ACCT ACT--varchar(64) */ 
 		  	        gdcds.setRad_acct_sby          ("");       /* PGW 국 데이터.RAD ACCT SBY--varchar(64) */ 
 		  	        gdcds.setDiam_pcfr_id          ("");       /* PGW 국 데이터.DIAM PCFR ID--varchar(64) */ 
 		  	        gdcds.setDiam_pcfr_act         ("");        /* PGW 국 데이터.DIAM PCFR ACT--varchar(64) */ 
 		  	        gdcds.setDiam_pcfr_sby         ("");        /* PGW 국 데이터.DIAM PCFR SBY--varchar(64) */ 
 		  	        gdcds.setLocal_pcc_pf_id       ("");          /* PGW 국 데이터.LOCAL PCC PF ID--varchar(64) */ 
 		  	        gdcds.setLocal_pcc_pf_act      ("");           /* PGW 국 데이터.LOCAL PCC PF ACT--varchar(64) */ 
 		  	        gdcds.setLocal_pcc_pf_sby      ("");           /* PGW 국 데이터.LOCAL PCC PF SBY--varchar(64) */ 
 		  	        gdcds.setRule_base_id          ("");       /* PGW 국 데이터.RULE BASE ID--varchar(64) */ 
 		  	        gdcds.setArp_map_high          ("");       /* PGW 국 데이터.ARP MAP HIGH--varchar(64) */ 
 		  	        gdcds.setArp_map_med           ("");      /* PGW 국 데이터.ARP MAP MED--varchar(64) */ 
 	 
    		
    			}
	  	        
	  	        gdcds.setPgw_val_chng_col_nm   ("");              /* PGW 국 데이터.PGW 값이 변경된 칼럼 이름--varchar(300) */     	
  	        	service.gugDataConfigPgwInsert(gdcds);
  	        }
          }
    	
       	
       	service.gugDataConfigSummaryUpdate();//데이타 요약화
        
    	HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    	logger.info(request.getContextPath()+":"+request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort());
        return "redirect://"+request.getServerName()+":"+request.getServerPort()+"/gugDataConfig";
    }
    
    
    //국데이타 수정 페이지(GET)
    @RequestMapping(value="/view/{gug_data_manage_no}/{curPage}", method=RequestMethod.GET)
    public ModelAndView editView(@PathVariable("gug_data_manage_no") BigInteger gug_data_manage_no,
    		         @PathVariable("curPage") int curPage
    		) throws Exception{
    	logger.info("VIEW:gug_data_manage_no=" +gug_data_manage_no);
        Map<String, Object> gug_map = new HashMap<String, Object>();
        gug_map.put("gug_data_manage_no",gug_data_manage_no);
        gug_map.put("gug_data_history_no",null);
        
        GugDataConfigComponent gugDataConfig = service.gugDataConfigView(gug_map);
        if( gugDataConfig.getApn_fqdn() ==null) gugDataConfig.setApn_fqdn("");
        if( gugDataConfig.getPgwgrp() ==null) gugDataConfig.setPgwgrp("");
        if(!gugDataConfig.getApn_fqdn().equals("")
        	|| !gugDataConfig.getPgwgrp().equals("")
           ) 
        {
        	gugDataConfig.setMme_check("on");
        }
        
        if( gugDataConfig.getPgw_ip() ==null) gugDataConfig.setPgw_ip("");
        if(!gugDataConfig.getPgw_ip().equals("")
           ) 
        {
        	gugDataConfig.setEpcc_check("on");
        }
        
       if( gugDataConfig.getId() ==null) gugDataConfig.setId("");
       if( gugDataConfig.getApn() ==null) gugDataConfig.setApn("");
        
        if(!gugDataConfig.getId().equals("")
        	|| !gugDataConfig.getApn().equals("")
                ) 
             {
             	gugDataConfig.setHss_check("on");
             }
        
        
        gugDataConfig.setView_gubun("EDIT");
        
        List<GugDataConfigDetSelComponent> mmeDetLst=service.gugDataConfigMmeDetView(gugDataConfig);

        if(mmeDetLst.size()==0)
        {
        	GugDataConfigDetSelComponent gdcds=new GugDataConfigDetSelComponent();
        	gdcds.setMme_det_no(1);
        	gdcds.setPgw("");
        	mmeDetLst.add(gdcds);
        	
        } 
        
        List<GugDataConfigDetSelComponent> epccDetLst=service.gugDataConfigEpccDetView(gugDataConfig);
        if(epccDetLst.size()==0)
        {
        	GugDataConfigDetSelComponent gdcds=new GugDataConfigDetSelComponent();
        	gdcds.setEpcc_det_no(1);
        	epccDetLst.add(gdcds);
        	
        }
        
        List<GugDataConfigDetSelComponent> pgwLst=service.gugDataConfigPgwView(gugDataConfig);
        for(int i=0;i<pgwLst.size();i++)
        {
        	GugDataConfigDetSelComponent gdcdc=new GugDataConfigDetSelComponent();
        	gdcdc=pgwLst.get(i);
        	String ip_pool_id=(gdcdc.getIp_pool_id()==null)?"":gdcdc.getIp_pool_id();
        	String vr_id=(gdcdc.getVr_id()==null)?"":gdcdc.getVr_id();
        	String apn_id=(gdcdc.getApn_id()==null)?"":gdcdc.getApn_id();
        	String apn_name=(gdcdc.getApn_name()==null)?"":gdcdc.getApn_name();
	        if(!ip_pool_id.equals("")
	                || !vr_id.equals("")
	                || !apn_id.equals("")
	                || !apn_name.equals("")
	               ) 
	                {
	                	gugDataConfig.setPgw_check("on");
	                	break;
	                }
    	}
        if(pgwLst.size()==0)
        {
        	GugDataConfigDetSelComponent gdcds=new GugDataConfigDetSelComponent();
        	gdcds.setPgw_seq_no(1);
        	pgwLst.add(gdcds);
        	
        }
        
   	 	ModelAndView mav = new ModelAndView();
        mav.setViewName("gugDataConfig/gugDataConfigWrite");
        mav.addObject("gugDataConfig", gugDataConfig); 
        mav.addObject("mmeDetLst", mmeDetLst); 
        mav.addObject("epccDetLst", epccDetLst);
        mav.addObject("pgwLst", pgwLst);
        
    	EquipCmdBasComponent ecbVo=new EquipCmdBasComponent();
    	ecbVo.setParameter_type("ENUM");
    	Map<String,Object> ecbMap=ecbService.equipCmdBasComboMapSelect(ecbVo);
        Iterator<String> keys = ecbMap.keySet().iterator();

        while( keys.hasNext() ){

            String key = keys.next();
            mav.addObject(key,(List)ecbMap.get(key));

        }
        
        
        // 검색옵션, 키워드 맵에 저장
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("searchOption", "");
        map.put("keyword_fdate", "");
        map.put("keyword_tdate", "");
        map.put("searchOption2", "");
        map.put("keyword2", "");
        map.put("work_state_cd", "");     
        map.put("gug_data_manage_no", gug_data_manage_no); 
        
        
        //검색옵션에 맞추어 카운트 가져오기
    	int count =service.gugDataConfigHisCount(map);
    	
    	  logger.info("count:"+count);
        // 페이지 나누기 관련 처리
        int pageScale = 5; // 페이지당 게시물 수
        int blockScale = 10;  // 화면당 페이지 수
        CommonPager commonPager = new CommonPager(count, curPage,pageScale,blockScale);
        map.put("start", commonPager.getPageBegin());
        map.put("end", commonPager.getPageEnd());
        map.put("curPage", commonPager.getCurPage());
        map.put("pageScale", commonPager.getPageScale());
        List<GugDataConfigComponent> his_compare_list = new ArrayList<GugDataConfigComponent>();
        List<GugDataConfigComponent> his_list = service.gugDataConfigHisList(map);
        for(int i=0;i<his_list.size();i++)
        {
        	GugDataConfigComponent gc=new GugDataConfigComponent();
        	gc=his_list.get(i);
        	his_compare_list.add(gc);
        	GugDataConfigComponent prvGc=new GugDataConfigComponent();
        	if((i+1)<his_list.size())
        	{
        		prvGc=his_list.get(i+1);
        	}
        	
        	if(gc.getVal_chng_col_nm()!=null && (!gc.getVal_chng_col_nm().equals("")) )
        	{
	        	String[] gmm=gc.getVal_chng_col_nm().split(Pattern.quote("|"));
	        	for(int jj=0;jj<gmm.length;jj++)
	        	{
	        		GugDataConfigComponent gmmval=new GugDataConfigComponent();
	        		gmmval.setChng_col_nm("");
        			gmmval.setModify_id("- 국 마스터");
	        		if(gmm[jj].equals("profile_nm")){
		        		gmmval.setChng_col_nm("프로파일명");
	        			gmmval.setPrev_col_val(prvGc.getProfile_nm());
	    				gmmval.setNext_col_val(gc.getProfile_nm());
	        		}else if(gmm[jj].equals("work_ept_dt")){
		        		gmmval.setChng_col_nm("작업예정일");
	        			gmmval.setPrev_col_val(prvGc.getWork_ept_dt());
	    				gmmval.setNext_col_val(gc.getWork_ept_dt());	
	        		}else if(gmm[jj].equals("work_ept_start_time")){
		        		gmmval.setChng_col_nm("작업예정시작일시");
	        			gmmval.setPrev_col_val(prvGc.getWork_ept_start_time());
	    				gmmval.setNext_col_val(gc.getWork_ept_start_time());
		        	}else if(gmm[jj].equals("work_ept_close_time")){
		        		gmmval.setChng_col_nm("작업예정종료일시");
	        			gmmval.setPrev_col_val(prvGc.getWork_ept_close_time());
	    				gmmval.setNext_col_val(gc.getWork_ept_close_time());
		        	}
	        		if((!gmmval.getChng_col_nm().equals(""))
	        			&& gmmval.getChng_col_nm()!=null )
	        			his_compare_list.add(gmmval);
	        		
	        	}
        	}
        	
        	if(gc.getEpcc_val_chng_col_nm()!=null && (!gc.getEpcc_val_chng_col_nm().equals("")) )
        	{
	        	String[] epcc=gc.getEpcc_val_chng_col_nm().split(Pattern.quote("|"));
	        	for(int jj=0;jj<epcc.length;jj++)
	        	{
	
	        		GugDataConfigComponent gmmval=new GugDataConfigComponent();
	        		gmmval.setChng_col_nm(epcc[jj].toUpperCase());
        			gmmval.setModify_id("- EPCC 파라메터");
	        		if(epcc[jj].equals("pgw_ip")){
	        			gmmval.setPrev_col_val(prvGc.getPgw_ip());
	        			gmmval.setNext_col_val(gc.getPgw_ip());
	        		}else if(epcc[jj].equals("sms_recv")){
	        			gmmval.setPrev_col_val(prvGc.getSms_recv());
	        			gmmval.setNext_col_val(gc.getSms_recv());	
	        		}
	        		if((!gmmval.getChng_col_nm().equals(""))
	            			&& gmmval.getChng_col_nm()!=null )
	            			his_compare_list.add(gmmval);
	        		
	        	}
        	}
        	
        	if(gc.getHss_val_chng_col_nm()!=null && (!gc.getHss_val_chng_col_nm().equals("")) )
        	{
	        	String[] hss=gc.getHss_val_chng_col_nm().split(Pattern.quote("|"));
	        	for(int jj=0;jj<hss.length;jj++)
	        	{
	
	        		GugDataConfigComponent gmmval=new GugDataConfigComponent();
	        		gmmval.setChng_col_nm(hss[jj].toUpperCase());
        			gmmval.setModify_id("- HSS 파라메터");
	        		if(hss[jj].equals("id")){
	        			gmmval.setPrev_col_val(prvGc.getId());
	    				gmmval.setNext_col_val(gc.getId());
	        		}else if(hss[jj].equals("apn")){
	        			gmmval.setPrev_col_val(prvGc.getApn());
	    				gmmval.setNext_col_val(gc.getApn());	
	        		}else if(hss[jj].equals("apnoirepl")){
	        			gmmval.setPrev_col_val(prvGc.getApnoirepl());
	    				gmmval.setNext_col_val(gc.getApnoirepl());
		        	}else if(hss[jj].equals("pltcc")){
	        			gmmval.setPrev_col_val(prvGc.getPltcc());
	    				gmmval.setNext_col_val(gc.getPltcc());
		        	}else if(hss[jj].equals("uutype")){
	        			gmmval.setPrev_col_val(prvGc.getUutype());
	    				gmmval.setNext_col_val(gc.getUutype());
		        	}else if(hss[jj].equals("mpname_cd")){
	        			gmmval.setPrev_col_val(prvGc.getMpname_cd());
	    				gmmval.setNext_col_val(gc.getMpname_cd());
		        	}
	        		if((!gmmval.getChng_col_nm().equals(""))
	            			&& gmmval.getChng_col_nm()!=null )
	            			his_compare_list.add(gmmval);
	        		
	        	}
        	}
        	
        	if(gc.getMme_val_chng_col_nm()!=null && (!gc.getMme_val_chng_col_nm().equals("")) )
        	{
	        	String[] mme=gc.getMme_val_chng_col_nm().split(Pattern.quote("|"));
	        	for(int jj=0;jj<mme.length;jj++)
	        	{
	
	        		GugDataConfigComponent gmmval=new GugDataConfigComponent();
	        		gmmval.setChng_col_nm(mme[jj].toUpperCase());
        			gmmval.setModify_id("- MME 파라메터");
	        		if(mme[jj].equals("apn_fqdn")){
	        			gmmval.setPrev_col_val(prvGc.getApn_fqdn());
	    				gmmval.setNext_col_val(gc.getApn_fqdn());
	        		}else if(mme[jj].equals("pgwgrp")){
	        			gmmval.setPrev_col_val(prvGc.getPgwgrp());
	    				gmmval.setNext_col_val(gc.getPgwgrp());	
	        		}
	        		if((!gmmval.getChng_col_nm().equals(""))
	            			&& gmmval.getChng_col_nm()!=null )
	            			his_compare_list.add(gmmval);
	        		
	        	}
        	}
        	logger.info("gc.getVal_chng_col_nm()"+i+"="+gc.getVal_chng_col_nm());
        	logger.info("gc.getEpcc_val_chng_col_nm()"+i+"="+gc.getEpcc_val_chng_col_nm());
        	logger.info("gc.getHss_val_chng_col_nm()"+i+"="+gc.getHss_val_chng_col_nm());
        	logger.info("gc.getMme_val_chng_col_nm()"+i+"="+gc.getMme_val_chng_col_nm());
        }
        
        mav.addObject("count", count); 
        mav.addObject("his_list", his_compare_list);
        mav.addObject("gug_data_manage_no", gug_data_manage_no);
        mav.addObject("curPage", 1);
        mav.addObject("commonPager", commonPager);
        
        logger.info("mav:"+ mav);
        return mav;
    }
    
    ////국데이타 수정 페이지(GET-JSON)
	@RequestMapping(value="/jsondata/{gug_data_manage_no}", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
    public String getJsondata(@PathVariable("gug_data_manage_no") BigInteger gug_data_manage_no) throws Exception{
        Map<String, Object> gug_map = new HashMap<String, Object>();
        gug_map.put("gug_data_manage_no",gug_data_manage_no);
        gug_map.put("gug_data_history_no",null);
 		Gson gson = new Gson();
		GugDataConfigComponent gugDataConfig = service.gugDataConfigView(gug_map);
		logger.debug(gugDataConfig.toString());
		String s = gson.toJson(gugDataConfig);
		logger.debug(s);
		return s;
	}
    

    //국데이타 수정(PUT)
    @RequestMapping(value="/post/{bno}", method=RequestMethod.PUT)
    public String update(@ModelAttribute("GugDataConfigComponent") GugDataConfigComponent gugDataConfig,@PathVariable("bno") int bno) throws Exception{
    	logger.debug(String.valueOf(bno));
        service.gugDataConfigUpdate(gugDataConfig);
        
        //return "redirect://localhost:8080/gugDataConfig";
    	HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    	logger.info(request.getContextPath()+":"+request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort());
        return "redirect://"+request.getServerName()+":"+request.getServerPort()+"/gugDataConfig";
    }
    
    
    //설계자 승인
    @RequestMapping(value="/design_approval",method=RequestMethod.POST,produces = "application/text; charset=utf8")
	@ResponseBody
	public String oper_confirm(@ModelAttribute("GugDataConfigComponent") GugDataConfigComponent gugDataConfig) throws Exception{
		logger.info("gugDataConfig.getDesigner_aprv_yn()=" +gugDataConfig.getDesigner_aprv_yn());
		 service.gugDataConfigUpdate(gugDataConfig);
//		
//		GugDataConfigComponent vo = new GugDataConfigComponent();
//		vo.setGug_data_manage_no(gugPcmdCrExec.getGug_data_manage_no());
//		vo.setGug_data_history_no(gugPcmdCrExec.getGug_data_history_no());
//		vo.setWork_state_cd("D040"); //작업대기상태
//		dtService.gugDataConfigUpdate(vo);
//		dtService.gugDataConfigSummaryUpdate();//데이타 요약화

		return "";
	}

    //작업완료
    @RequestMapping(value="/work_complete",method=RequestMethod.POST,produces = "application/text; charset=utf8")
	@ResponseBody
	public String work_complete(@ModelAttribute("GugDataConfigComponent") GugDataConfigComponent gugDataConfig) throws Exception{
		logger.info("gugDataConfig.getDesigner_aprv_yn()=" +gugDataConfig.getDesigner_aprv_yn());
		 service.gugDataConfigUpdate(gugDataConfig);
//		
		service.gugDataConfigSummaryUpdate();//데이타 요약화

		return "";
	}
    
    

    
    //국데이타 복사(COPY)
    @RequestMapping(value="/copy/{gug_data_manage_no}", method=RequestMethod.GET)
    public String copy(@PathVariable("gug_data_manage_no") BigInteger gug_data_manage_no) throws Exception{
    	logger.info("333");
    	GugDataConfigComponent gcc=new GugDataConfigComponent();
    	gcc.setGug_data_manage_no(gug_data_manage_no);
    	gcc.setNew_gug_data_manage_no(service.gugDataConfigGetManageNo());
        service.gugDataConfigInsertCopy(gcc);
       	service.gugDataConfigSummaryUpdate();//데이타 요약화
        //return "redirect://localhost:8080/gugDataConfig";
    	HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    	logger.info(request.getContextPath()+":"+request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort());
        return "redirect://"+request.getServerName()+":"+request.getServerPort()+"/gugDataConfig";
    }
    
    //국데이타 삭제(DELETE)
    @RequestMapping(value="/delete/{gug_data_manage_no}", method=RequestMethod.DELETE)
    public String delete(@PathVariable("gug_data_manage_no") BigInteger gug_data_manage_no) throws Exception{
    	logger.info("333");
    	GugDataConfigComponent gcc=new GugDataConfigComponent();
    	gcc.setGug_data_manage_no(gug_data_manage_no);
        service.gugDataConfigInsertDelete(gcc);
       	service.gugDataConfigSummaryUpdate();//데이타 요약화
        //return "redirect://localhost:8080/gugDataConfig";
    	HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    	logger.info(request.getContextPath()+":"+request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort());
        return "redirect://"+request.getServerName()+":"+request.getServerPort()+"/gugDataConfig";
    }
    
    //국데이타 선택삭제(DELETE)
    @RequestMapping(value="/delete/select", method=RequestMethod.DELETE)
    public String delete(@ModelAttribute("GugDataConfigComponent") GugDataConfigComponent gugDataConfig) throws Exception{
    	logger.info("dddddddddd");
    	
        BigInteger bno= new BigInteger("0");

        String[] checkNo=gugDataConfig.getCheckNo();
        if(checkNo!=null){
	        for(int i=0 ;i<checkNo.length;i++)
	        {
	            
	            logger.info(checkNo[i]);
	        	bno=BigInteger.valueOf(Long.valueOf(checkNo[i]));
	        	logger.info(checkNo[i]);
	        	GugDataConfigComponent gcc=new GugDataConfigComponent();
	        	logger.info(checkNo[i]);
	            gcc.setGug_data_manage_no(bno);
	            logger.info(checkNo[i]);
	            service.gugDataConfigInsertDelete(gcc);
	            logger.info(checkNo[i]);
	        }
        }
       	service.gugDataConfigSummaryUpdate();//데이타 요약화
        
        //return "redirect://localhost:8080/gugDataConfig";
    	HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    	logger.info(request.getContextPath()+":"+request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort());
        return "redirect://"+request.getServerName()+":"+request.getServerPort()+"/gugDataConfig";
    }
   
	////해당하는 장비 명령어와 실행할 선택된 명령어 조회(GET-JSON)
	@RequestMapping(value="/equip_cmd_list", method=RequestMethod.GET,produces = "application/text; charset=utf8")
	@ResponseBody
	public String equip_cmd_list(@RequestParam(defaultValue="") String gug_data_manage_no,
										@RequestParam(defaultValue="") String gug_equip_type_cd
			) throws Exception{
		logger.info("gug_data_manage_no="+gug_data_manage_no);
		Map<String, Object> map = new HashMap<String, Object>();
		Gson gson = new Gson();
		map.put("gug_equip_type_cd",gug_equip_type_cd);
		List<EquipCmdBasComponent> equip_cmd = ecbService.equipCmdBasListOffset(map); //OFFSET방식
		map.put("equip_cmd", equip_cmd);
    	BigInteger dno= new BigInteger(gug_data_manage_no);
		map.put("gug_data_manage_no",dno);
		List<GugDataConfigComponent> gug_sel_cmd = service.commandSelManageList(map); //OFFSET방식
		map.put("gug_sel_cmd", gug_sel_cmd);
		
		String s = gson.toJson(map);
		logger.debug(s);
		return s;
	}
}

