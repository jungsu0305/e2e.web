package com.kt.e2e.fmmonitor.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kt.e2e.Common.domain.Paging;
import com.kt.e2e.fmmonitor.domain.FmMonitor;
import com.kt.e2e.fmmonitor.service.FmAlarmMonitorService;


@Controller
@RequestMapping("/e2e/fm/")
public class FmAlarmMonitorController {

	/* the logger */
	static final Logger log = LoggerFactory.getLogger(FmAlarmMonitorController.class);

	/* ServletContext */
	@Autowired
    private ServletContext servletContext;
	
	/* service : fmAlarmMonitorService */
	@Autowired
	private FmAlarmMonitorService fmAlarmMonitorService;

	@RequestMapping(value = "/monitor")
	public String monitor2(HttpServletRequest request) {
		return "/fmmonitor/monitor2";
	}
	
	
	@RequestMapping(value = "/monitor/popup")
	public String monitor(HttpServletRequest request, Model model) {
		String e2eWebsocketAddress = (String) servletContext.getAttribute("e2eWebsocketAddress");
		model.addAttribute("e2eWebsocketAddress", e2eWebsocketAddress);
		
		String e2eNsdId = (String) servletContext.getAttribute("e2eNsdId");
		model.addAttribute("e2eNsdId", e2eNsdId);
		
		return "/fmmonitor/monitor";
	}

	@RequestMapping(value = "alarmList", method = RequestMethod.POST)
	public String alarmList(FmMonitor monitor, Model model) {
		monitor.setPerPage(10);

		int page = monitor.getPage() != null ? monitor.getPage() : 1;
		int perPage = monitor.getPerPage();
		int count = 0;
		Paging paging = null;

		try {
			count = fmAlarmMonitorService.alarmTotalCount(monitor);

			paging = new Paging();
			paging.setTotalCount(count);
			paging.setPage(page);
			paging.setPerPage(perPage);
			if (count > 0) {
				paging.setLists(fmAlarmMonitorService.listAlarm(monitor, page, perPage));
				if (monitor.getLastAlarmTime() != null && monitor.getLastAlarmTime() != "") {
					model.addAttribute("listNewAlarm", fmAlarmMonitorService.listNewAlarm(monitor));
				}
			}
			
			String lastAlarmTime = fmAlarmMonitorService.getLastAlarmTimestamp(monitor);
			if (lastAlarmTime == null || lastAlarmTime.equals("")) {
				SimpleDateFormat sdfNow = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				lastAlarmTime = sdfNow.format(new Date(System.currentTimeMillis()));
			}
			model.addAttribute("lastAlarmTime", lastAlarmTime);

			int lastPage = (count % perPage == 0) ? count / perPage : (count / perPage + 1);

			model.addAttribute("paging", paging);
			model.addAttribute("lastPage", lastPage);
			model.addAttribute("searchStatus", monitor.getSearch());
			model.addAttribute("severityCount", fmAlarmMonitorService.severityCount(monitor));
			model.addAttribute("instanceAlarmLevel", fmAlarmMonitorService.getInstanceAlarmLevel(monitor));
			model.addAttribute("alarmLevel", fmAlarmMonitorService.getAlarmLevel());
			
		} finally {
			paging = null;
		}
		
		return "/fmmonitor/alarmList";
	}
	
	@RequestMapping(value = "getInstanceAlarmLevel", method = RequestMethod.POST)
	@ResponseBody
	public List<Map<String, Object>> getInstanceAlarmLevel(FmMonitor monitor) {
		return fmAlarmMonitorService.getInstanceAlarmLevel(monitor);
	}	
	
	@RequestMapping(value = "/alarmMask")
	@ResponseBody
	public int alarmMask(String[] chk) {
		return fmAlarmMonitorService.updateAlarmMaskFlag(chk, "M");
	}

	@RequestMapping(value = "/alarmUnmask")
	@ResponseBody
	public int alarmUnmask(String[] chk) {
		return fmAlarmMonitorService.updateAlarmMaskFlag(chk, "N");
	}
	
	@RequestMapping(value = "{fmNotificationId}/{alarmId}/alarmDetail", method = RequestMethod.POST)
	public String alarmDetail(@PathVariable(value = "fmNotificationId") String fmNotificationId,
			@PathVariable("alarmId") String alarmId, Model model) {
		model.addAttribute("alarmDetail", fmAlarmMonitorService.alarmDetail(fmNotificationId, alarmId));
		return "/fmmonitor/alarmDetail";
	}
}