package com.kt.e2e.fmmonitor.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import com.kt.e2e.fmmonitor.domain.FmAlarm;
import com.kt.e2e.fmmonitor.domain.FmMonitor;

@Component
public interface FmAlarmMonitorMapper {

	int alarmTotalCount(@Param("monitor") FmMonitor monitor);

	List<FmAlarm> listAlarm(@Param("monitor") FmMonitor monitor
            , @Param("start") int start
            , @Param("end") int end);

	List<FmAlarm> listNewAlarm(@Param("monitor") FmMonitor monitor);

	String getLastAlarmTimestamp(@Param("monitor") FmMonitor monitor);
	
	FmAlarm alarmDetail(
			@Param("fmNotificationId") String fmNotificationId,
			@Param("alarmId") String alarmId);

	List<Map<String, Object>> severityCount(@Param("monitor") FmMonitor monitor);
	
	List<Map<String, Object>> getInstanceAlarmLevel(@Param("monitor") FmMonitor monitor);

    List<String> listVnfcName(String vnfInstanceId);

    List<FmAlarm> listVnfEvents(
    		@Param("vnfInstanceId") String vnfInstanceId,
    		@Param("instanceId") String instanceId);

    int updateAlarmMaskFlag(
    		@Param("fmNotificationId") String fmNotificationId,
    		@Param("alarmId") String alarmId,
    		@Param("maskFlag") String maskFlag);

    int isAliveVnf(@Param("vnfInstanceId") String vnfInstanceId);

    String getAlarmLevel();
}
