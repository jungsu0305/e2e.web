package com.kt.e2e.fmmonitor.domain;

public class FmAlarm {
	
	private int extifType;                             
	private String fmNotificationId;                     
	private String timeStamp;                             
	private String alarmId;                               
	private String objId;     
	private String objInstanceName;
	private String notiType;                              
	private String rootcauseFaultyCompType;             
	private String rootcauseFaultyCompId;               
	private String faultyResourceVimId;                 
	private String faultyResourceId;                     
	private String faultyResourceVimLevelResourceType;
	private int faultyResourceType;                   
	private String alarmRaisedTime;                      
	private String alarmChangedTime;                     
	private String alarmClearedTime;                     
	private int ackState;                              
	private int perceivedSeverity;                     
	private String eventTime;                             
	private int eventType;                             
	private String faultType;                             
	private String probableCause;                         
	private String isRootCause;                          
	private String correlatedAlarmId;                    
	private String faultDetails;                          
	private String createdAt;
	private String maskFlag;
	
	public int getExtifType() {
		return extifType;
	}
	public void setExtifType(int extifType) {
		this.extifType = extifType;
	}
	public String getFmNotificationId() {
		return fmNotificationId;
	}
	public void setFmNotificationId(String fmNotificationId) {
		this.fmNotificationId = fmNotificationId;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getAlarmId() {
		return alarmId;
	}
	public void setAlarmId(String alarmId) {
		this.alarmId = alarmId;
	}
	public String getObjId() {
		return objId;
	}
	public void setObjId(String objId) {
		this.objId = objId;
	}
	
	public String getObjInstanceName() {
		return objInstanceName;
	}
	public void setObjInstanceName(String objInstanceName) {
		this.objInstanceName = objInstanceName;
	}
	public String getNotiType() {
		return notiType;
	}
	public void setNotiType(String notiType) {
		this.notiType = notiType;
	}
	public String getRootcauseFaultyCompType() {
		return rootcauseFaultyCompType;
	}
	public void setRootcauseFaultyCompType(String rootcauseFaultyCompType) {
		this.rootcauseFaultyCompType = rootcauseFaultyCompType;
	}
	public String getRootcauseFaultyCompId() {
		return rootcauseFaultyCompId;
	}
	public void setRootcauseFaultyCompId(String rootcauseFaultyCompId) {
		this.rootcauseFaultyCompId = rootcauseFaultyCompId;
	}
	public String getFaultyResourceVimId() {
		return faultyResourceVimId;
	}
	public void setFaultyResourceVimId(String faultyResourceVimId) {
		this.faultyResourceVimId = faultyResourceVimId;
	}
	public String getFaultyResourceId() {
		return faultyResourceId;
	}
	public void setFaultyResourceId(String faultyResourceId) {
		this.faultyResourceId = faultyResourceId;
	}
	public String getFaultyResourceVimLevelResourceType() {
		return faultyResourceVimLevelResourceType;
	}
	public void setFaultyResourceVimLevelResourceType(String faultyResourceVimLevelResourceType) {
		this.faultyResourceVimLevelResourceType = faultyResourceVimLevelResourceType;
	}
	public int getFaultyResourceType() {
		return faultyResourceType;
	}
	public void setFaultyResourceType(int faultyResourceType) {
		this.faultyResourceType = faultyResourceType;
	}
	public String getAlarmRaisedTime() {
		return alarmRaisedTime;
	}
	public void setAlarmRaisedTime(String alarmRaisedTime) {
		this.alarmRaisedTime = alarmRaisedTime;
	}
	public String getAlarmChangedTime() {
		return alarmChangedTime;
	}
	public void setAlarmChangedTime(String alarmChangedTime) {
		this.alarmChangedTime = alarmChangedTime;
	}
	public String getAlarmClearedTime() {
		return alarmClearedTime;
	}
	public void setAlarmClearedTime(String alarmClearedTime) {
		this.alarmClearedTime = alarmClearedTime;
	}
	public int getAckState() {
		return ackState;
	}
	public void setAckState(int ackState) {
		this.ackState = ackState;
	}
	public int getPerceivedSeverity() {
		return perceivedSeverity;
	}
	public void setPerceivedSeverity(int perceivedSeverity) {
		this.perceivedSeverity = perceivedSeverity;
	}
	public String getEventTime() {
		return eventTime;
	}
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	public int getEventType() {
		return eventType;
	}
	public void setEventType(int eventType) {
		this.eventType = eventType;
	}
	public String getFaultType() {
		return faultType;
	}
	public void setFaultType(String faultType) {
		this.faultType = faultType;
	}
	public String getProbableCause() {
		return probableCause;
	}
	public void setProbableCause(String probableCause) {
		this.probableCause = probableCause;
	}
	public String getIsRootCause() {
		return isRootCause;
	}
	public void setIsRootCause(String isRootCause) {
		this.isRootCause = isRootCause;
	}
	public String getCorrelatedAlarmId() {
		return correlatedAlarmId;
	}
	public void setCorrelatedAlarmId(String correlatedAlarmId) {
		this.correlatedAlarmId = correlatedAlarmId;
	}
	public String getFaultDetails() {
		return faultDetails;
	}
	public void setFaultDetails(String faultDetails) {
		this.faultDetails = faultDetails;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getMaskFlag() {
		return maskFlag;
	}
	public void setMaskFlag(String maskFlag) {
		this.maskFlag = maskFlag;
	}
	@Override
	public String toString() {
		return "FmAlarm [extifType=" + extifType + ", fmNotificationId=" + fmNotificationId + ", timeStamp=" + timeStamp
				+ ", alarmId=" + alarmId + ", objId=" + objId + ", objInstanceName=" + objInstanceName + ", notiType="
				+ notiType + ", rootcauseFaultyCompType=" + rootcauseFaultyCompType + ", rootcauseFaultyCompId="
				+ rootcauseFaultyCompId + ", faultyResourceVimId=" + faultyResourceVimId + ", faultyResourceId="
				+ faultyResourceId + ", faultyResourceVimLevelResourceType=" + faultyResourceVimLevelResourceType
				+ ", faultyResourceType=" + faultyResourceType + ", alarmRaisedTime=" + alarmRaisedTime
				+ ", alarmChangedTime=" + alarmChangedTime + ", alarmClearedTime=" + alarmClearedTime + ", ackState="
				+ ackState + ", perceivedSeverity=" + perceivedSeverity + ", eventTime=" + eventTime + ", eventType="
				+ eventType + ", faultType=" + faultType + ", probableCause=" + probableCause + ", isRootCause="
				+ isRootCause + ", correlatedAlarmId=" + correlatedAlarmId + ", faultDetails=" + faultDetails
				+ ", createdAt=" + createdAt + ", maskFlag=" + maskFlag + "]";
	}
}
