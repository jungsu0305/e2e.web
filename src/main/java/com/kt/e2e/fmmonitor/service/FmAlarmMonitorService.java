package com.kt.e2e.fmmonitor.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kt.e2e.fmmonitor.dao.FmAlarmMonitorMapper;
import com.kt.e2e.fmmonitor.domain.FmAlarm;
import com.kt.e2e.fmmonitor.domain.FmMonitor;

@Service("FmAlarmMonitorService")
public class FmAlarmMonitorService {
	static final Logger log = LoggerFactory.getLogger(FmAlarmMonitorService.class);

    @Autowired
    private FmAlarmMonitorMapper fmAlarmMonitorMapper;

    public int alarmTotalCount(FmMonitor monitor){
		return fmAlarmMonitorMapper.alarmTotalCount(monitor);
	}

    public List<FmAlarm> listAlarm(FmMonitor monitor, int page, int perPage){
    	int start = ((page - 1) * perPage);
		int end = perPage;
		return fmAlarmMonitorMapper.listAlarm(monitor, start, end);
	}

    public List<FmAlarm> listNewAlarm(FmMonitor monitor){
		return fmAlarmMonitorMapper.listNewAlarm(monitor);
	}

    public String getLastAlarmTimestamp(FmMonitor monitor){
    	return fmAlarmMonitorMapper.getLastAlarmTimestamp(monitor);
    }

    public FmAlarm alarmDetail(
    		String fmNotificationId,
    		String alarmId){
		return fmAlarmMonitorMapper.alarmDetail(fmNotificationId, alarmId);
	}

    public List<Map<String, Object>> severityCount(FmMonitor monitor) {
		return fmAlarmMonitorMapper.severityCount(monitor);
	}
    
    
    public List<Map<String, Object>> getInstanceAlarmLevel(FmMonitor monitor) {
		return fmAlarmMonitorMapper.getInstanceAlarmLevel(monitor);
	}

    public String getAlarmLevel() {
    	return fmAlarmMonitorMapper.getAlarmLevel();
    }

   public int updateAlarmMaskFlag(String[] chk, String maskFlag){
		for(String chkStr : chk){
			String fmNotificationId = chkStr.split("_")[0]; 
			String alarmId = chkStr.split("_")[1];
			fmAlarmMonitorMapper.updateAlarmMaskFlag(fmNotificationId, alarmId, maskFlag);
		}
		return 0;
   }

   public boolean isAliveVnf(String vnfInstanceId){
	   if(fmAlarmMonitorMapper.isAliveVnf(vnfInstanceId) > 0){
		   return true;
	   }
	   return false;
   }
}
