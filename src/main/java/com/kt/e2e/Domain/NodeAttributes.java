package com.kt.e2e.Domain;

public class NodeAttributes {

	String id;
	String port;
	
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return this.id;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	@Override
	public String toString() {
		return "NodeAttributes [id=" + id + ", port=" + port + "]";
	}
	
	
}
