package com.kt.e2e.Domain;

import java.sql.Date;
import java.util.ArrayList;

import org.apache.ibatis.type.Alias;

@Alias("NsdmNsdInfo")
public class NsdmNsdInfoComponent extends BaseComponent {

	String id;
	String nsd_id;
	String nsd_name;
	String nsd_version;
	String nsd_designer;	
	String onboarded_vnf_pkg_inf_ids;
	String nsd_inf_state;
	String onboarding_failure_details;
	String nsd_op_state;
	String nsd_usage_state;
	String user_defined;
	Date created_at;
	
	String errorCode;
	String errorMessage;
	String returnMessage;
	
	public String getNsd_Id() {
		return nsd_id;
	}
	public void setNsd_id(String nsd_id) {
		this.nsd_id = nsd_id;
	}

	public String getNsd_name() {
		return nsd_name;
	}
	public void setNsd_name(String nsd_name) {
		this.nsd_name = nsd_name;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getNsd_version() {
		return nsd_version;
	}
	public void setNsd_version(String nsd_version) {
		this.nsd_version = nsd_version;
	}
	
	public String getNsd_designer() {
		return nsd_designer;
	}
	public void setNsd_designer(String nsd_designer) {
		this.nsd_designer = nsd_designer;
	}
	
	public String getOnboarded_vnf_pkg_inf_ids() {
		return onboarded_vnf_pkg_inf_ids;
	}
	public void setOnboarded_vnf_pkg_inf_ids(String onboarded_vnf_pkg_inf_ids) {
		this.onboarded_vnf_pkg_inf_ids = onboarded_vnf_pkg_inf_ids;
	}
	
	public String getNsd_inf_state() {
		return nsd_inf_state;
	}
	public void setNsd_inf_state(String nsd_inf_state) {
		this.nsd_inf_state = nsd_inf_state;
	}
	
	public String getOnboarding_failure_details() {
		return onboarding_failure_details;
	}
	public void setOnboarding_failure_details(String onboarding_failure_details) {
		this.onboarding_failure_details = onboarding_failure_details;
	}
	
	public String getNsd_op_state() {
		return nsd_op_state;
	}
	public void setNsd_op_state(String nsd_op_state) {
		this.nsd_op_state = nsd_op_state;
	}
	
	public String getNsd_usage_state() {
		return nsd_usage_state;
	}
	public void setNsd_usage_state(String nsd_usage_state) {
		this.nsd_usage_state = nsd_usage_state;
	}
	
	public String getUser_defined() {
		return user_defined;
	}
	public void setUser_defined(String user_defined) {
		this.user_defined = user_defined;
	}
	
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public String getReturnMessage() {
		return returnMessage;
	}
	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	@Override
	public String toString() {
		return "NsdmNsdInfoComponent [nsd_id=" + nsd_id 
//				", vnfd_id=" + vnfd_id + ", pnfd_id=" + pnfd_id + ", nsdf_id=" + nsdf_id + ", id=" + id + ", prof_id=" + prof_id +
//				", prof_name=" + prof_name + ", prof_inf=" + prof_inf
				+ "]";
	}
}
