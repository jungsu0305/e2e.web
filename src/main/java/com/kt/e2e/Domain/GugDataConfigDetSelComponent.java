package com.kt.e2e.Domain;
 
import java.math.BigInteger;
import java.util.Date;
 
public class GugDataConfigDetSelComponent {
 
  

    private BigInteger gug_data_manage_no;         /* 국데이타 마스터.국 데이터 관리 번호     -BIGSERIAL   */
    private int gug_data_history_no;               /* 국데이타 마스터.국 데이터 이력 번호     -int4         */
	
    private int epcc_det_no; /* ePCC 국 데이터 디테일.ePCC 디테일 번호--int4 */ 
    private String ip_pool; /* ePCC 국 데이터 디테일.IP POOL--numeric(5,0) */ 
    private String epcc_det_val_chng_col_nm; /* ePCC 국 데이터 디테일.ePCC 디테일 값이 변경된 칼럼 이름--varchar(100) */ 


    private int mme_det_no; /* MME 국 데이터 디테일.MME 디테일 번호--int4 */ 
    private String pgw; /* MME 국 데이터 디테일.PGW--varchar(1023) */ 
    private String ip; /* MME 국 데이터 디테일.IP--varchar(64) */ 
    private String name; /* MME 국 데이터 디테일.NAME--varchar(32) */ 
    private String capa; /* MME 국 데이터 디테일.CAPA--varchar(255) */ 
    private String mme_det_val_chng_col_nm; /* MME 국 데이터 디테일.MME 디테일 값이 변경된 칼럼 이름--varchar(100) */ 


    private int pgw_seq_no; /* PGW 국 데이터.PGW 일련 번호--int4 */ 
    private String ip_pool_id; /* PGW 국 데이터.IP POOL ID--numeric(5,0) */ 
    private String vr_id; /* PGW 국 데이터.VR_ID--numeric(2,0) */ 
    private String pool_type_cd; /* PGW 국 데이터.POOL TYPE 코드--varchar(7) */ 
    private String static_cd; /* PGW 국 데이터.STATIC 코드--varchar(3) */ 
    private String tunnel_cd; /* PGW 국 데이터.TUNNEL 코드--varchar(3) */ 
    private String start_addr; /* PGW 국 데이터.START ADDR--varchar(64) */ 
    private String vlan; /* PGW 국 데이터.VLAN--numeric(4,0) */ 
    private String ip_addr; /* PGW 국 데이터.IP ADDR--varchar(15) */ 
    private String network; /* PGW 국 데이터.NETWORK--varchar(32) */ 
    private String gateway; /* PGW 국 데이터.GATEWAY--varchar(64) */ 
    private String prirank_cd; /* PGW 국 데이터.우선순위 코드--varchar(64) */ 
    private String ifc; /* PGW 국 데이터.I/F--varchar(16) */ 
    private String apn_id; /* PGW 국 데이터.APN ID--varchar(64) */ 
    private String apn_name; /* PGW 국 데이터.APN NAME--varchar(32) */ 
    private String ip_alloc_cd; /* PGW 국 데이터.IP ALLOC 코드--varchar(5) */ 
    private String rad_ip_alloc_cd; /* PGW 국 데이터.RAD IP ALLOC 코드--varchar(3) */ 
    private String auth_pcrf_pcc_cd; /* PGW 국 데이터.AUTH/PCRF/PCC 코드--varchar(3) */ 
    private String acct_react_cd; /* PGW 국 데이터.ACCT/REACT 코드--varchar(3) */ 
    private String pri_dns_v4; /* PGW 국 데이터.PRI DNS V4--varchar(64) */ 
    private String sec_dns_v4; /* PGW 국 데이터.SEC DNS V4--varchar(64) */ 
    private String rad_auth_id; /* PGW 국 데이터.RAD AUTH ID--varchar(64) */ 
    private String rad_auth_act; /* PGW 국 데이터.RAD AUTH ACT--varchar(64) */ 
    private String rad_auth_sby; /* PGW 국 데이터.RAD AUTH SBY--varchar(64) */ 
    private String rad_acct_id; /* PGW 국 데이터.RAD ACCT ID--varchar(64) */ 
    private String rad_acct_act; /* PGW 국 데이터.RAD ACCT ACT--varchar(64) */ 
    private String rad_acct_sby; /* PGW 국 데이터.RAD ACCT SBY--varchar(64) */ 
    private String diam_pcfr_id; /* PGW 국 데이터.DIAM PCFR ID--varchar(64) */ 
    private String diam_pcfr_act; /* PGW 국 데이터.DIAM PCFR ACT--varchar(64) */ 
    private String diam_pcfr_sby; /* PGW 국 데이터.DIAM PCFR SBY--varchar(64) */ 
    private String local_pcc_pf_id; /* PGW 국 데이터.LOCAL PCC PF ID--varchar(64) */ 
    private String local_pcc_pf_act; /* PGW 국 데이터.LOCAL PCC PF ACT--varchar(64) */ 
    private String local_pcc_pf_sby; /* PGW 국 데이터.LOCAL PCC PF SBY--varchar(64) */ 
    private String rule_base_id; /* PGW 국 데이터.RULE BASE ID--varchar(64) */ 
    private String arp_map_high; /* PGW 국 데이터.ARP MAP HIGH--varchar(64) */ 
    private String arp_map_med; /* PGW 국 데이터.ARP MAP MED--varchar(64) */ 
    private String pgw_val_chng_col_nm; /* PGW 국 데이터.PGW 값이 변경된 칼럼 이름--varchar(300) */ 


    
    /*화면 연결부가 변수시작*/
    private String[] checkNo;
    private String searchOption;
    private String keyword_fdate;
    private String keyword_tdate;
    private String keyword;
    private String searchOption2;
    private String keyword2;
    private String view_gubun;
    
    private String work_state_nm;
    private int work_state_cnt;
    /*화면 연결부가 변수종료*/
    
    /*기타*/
    private BigInteger new_gug_data_manage_no;         /* 국데이터 복사시 새성된 새로운 국 데이터 관리 번호     -BIGSERIAL   */

	public BigInteger getGug_data_manage_no() {
		return gug_data_manage_no;
	}

	public void setGug_data_manage_no(BigInteger gug_data_manage_no) {
		this.gug_data_manage_no = gug_data_manage_no;
	}

	public int getGug_data_history_no() {
		return gug_data_history_no;
	}

	public void setGug_data_history_no(int gug_data_history_no) {
		this.gug_data_history_no = gug_data_history_no;
	}

	public int getEpcc_det_no() {
		return epcc_det_no;
	}

	public void setEpcc_det_no(int epcc_det_no) {
		this.epcc_det_no = epcc_det_no;
	}

	public String getIp_pool() {
		return ip_pool;
	}

	public void setIp_pool(String ip_pool) {
		this.ip_pool = ip_pool;
	}

	public String getEpcc_det_val_chng_col_nm() {
		return epcc_det_val_chng_col_nm;
	}

	public void setEpcc_det_val_chng_col_nm(String epcc_det_val_chng_col_nm) {
		this.epcc_det_val_chng_col_nm = epcc_det_val_chng_col_nm;
	}

	public int getMme_det_no() {
		return mme_det_no;
	}

	public void setMme_det_no(int mme_det_no) {
		this.mme_det_no = mme_det_no;
	}

	public String getPgw() {
		return pgw;
	}

	public void setPgw(String pgw) {
		this.pgw = pgw;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCapa() {
		return capa;
	}

	public void setCapa(String capa) {
		this.capa = capa;
	}

	public String getMme_det_val_chng_col_nm() {
		return mme_det_val_chng_col_nm;
	}

	public void setMme_det_val_chng_col_nm(String mme_det_val_chng_col_nm) {
		this.mme_det_val_chng_col_nm = mme_det_val_chng_col_nm;
	}

	public int getPgw_seq_no() {
		return pgw_seq_no;
	}

	public void setPgw_seq_no(int pgw_seq_no) {
		this.pgw_seq_no = pgw_seq_no;
	}

	public String getIp_pool_id() {
		return ip_pool_id;
	}

	public void setIp_pool_id(String ip_pool_id) {
		this.ip_pool_id = ip_pool_id;
	}

	public String getVr_id() {
		return vr_id;
	}

	public void setVr_id(String vr_id) {
		this.vr_id = vr_id;
	}

	public String getPool_type_cd() {
		return pool_type_cd;
	}

	public void setPool_type_cd(String pool_type_cd) {
		this.pool_type_cd = pool_type_cd;
	}

	public String getStatic_cd() {
		return static_cd;
	}

	public void setStatic_cd(String static_cd) {
		this.static_cd = static_cd;
	}

	public String getTunnel_cd() {
		return tunnel_cd;
	}

	public void setTunnel_cd(String tunnel_cd) {
		this.tunnel_cd = tunnel_cd;
	}

	public String getStart_addr() {
		return start_addr;
	}

	public void setStart_addr(String start_addr) {
		this.start_addr = start_addr;
	}

	public String getVlan() {
		return vlan;
	}

	public void setVlan(String vlan) {
		this.vlan = vlan;
	}

	public String getIp_addr() {
		return ip_addr;
	}

	public void setIp_addr(String ip_addr) {
		this.ip_addr = ip_addr;
	}

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getPrirank_cd() {
		return prirank_cd;
	}

	public void setPrirank_cd(String prirank_cd) {
		this.prirank_cd = prirank_cd;
	}

	public String getIfc() {
		return ifc;
	}

	public void setIfc(String ifc) {
		this.ifc = ifc;
	}

	public String getApn_id() {
		return apn_id;
	}

	public void setApn_id(String apn_id) {
		this.apn_id = apn_id;
	}

	public String getApn_name() {
		return apn_name;
	}

	public void setApn_name(String apn_name) {
		this.apn_name = apn_name;
	}

	public String getIp_alloc_cd() {
		return ip_alloc_cd;
	}

	public void setIp_alloc_cd(String ip_alloc_cd) {
		this.ip_alloc_cd = ip_alloc_cd;
	}

	public String getRad_ip_alloc_cd() {
		return rad_ip_alloc_cd;
	}

	public void setRad_ip_alloc_cd(String rad_ip_alloc_cd) {
		this.rad_ip_alloc_cd = rad_ip_alloc_cd;
	}

	public String getAuth_pcrf_pcc_cd() {
		return auth_pcrf_pcc_cd;
	}

	public void setAuth_pcrf_pcc_cd(String auth_pcrf_pcc_cd) {
		this.auth_pcrf_pcc_cd = auth_pcrf_pcc_cd;
	}

	public String getAcct_react_cd() {
		return acct_react_cd;
	}

	public void setAcct_react_cd(String acct_react_cd) {
		this.acct_react_cd = acct_react_cd;
	}

	public String getPri_dns_v4() {
		return pri_dns_v4;
	}

	public void setPri_dns_v4(String pri_dns_v4) {
		this.pri_dns_v4 = pri_dns_v4;
	}

	public String getSec_dns_v4() {
		return sec_dns_v4;
	}

	public void setSec_dns_v4(String sec_dns_v4) {
		this.sec_dns_v4 = sec_dns_v4;
	}

	public String getRad_auth_id() {
		return rad_auth_id;
	}

	public void setRad_auth_id(String rad_auth_id) {
		this.rad_auth_id = rad_auth_id;
	}

	public String getRad_auth_act() {
		return rad_auth_act;
	}

	public void setRad_auth_act(String rad_auth_act) {
		this.rad_auth_act = rad_auth_act;
	}

	public String getRad_auth_sby() {
		return rad_auth_sby;
	}

	public void setRad_auth_sby(String rad_auth_sby) {
		this.rad_auth_sby = rad_auth_sby;
	}

	public String getRad_acct_id() {
		return rad_acct_id;
	}

	public void setRad_acct_id(String rad_acct_id) {
		this.rad_acct_id = rad_acct_id;
	}

	public String getRad_acct_act() {
		return rad_acct_act;
	}

	public void setRad_acct_act(String rad_acct_act) {
		this.rad_acct_act = rad_acct_act;
	}

	public String getRad_acct_sby() {
		return rad_acct_sby;
	}

	public void setRad_acct_sby(String rad_acct_sby) {
		this.rad_acct_sby = rad_acct_sby;
	}

	public String getDiam_pcfr_id() {
		return diam_pcfr_id;
	}

	public void setDiam_pcfr_id(String diam_pcfr_id) {
		this.diam_pcfr_id = diam_pcfr_id;
	}

	public String getDiam_pcfr_act() {
		return diam_pcfr_act;
	}

	public void setDiam_pcfr_act(String diam_pcfr_act) {
		this.diam_pcfr_act = diam_pcfr_act;
	}

	public String getDiam_pcfr_sby() {
		return diam_pcfr_sby;
	}

	public void setDiam_pcfr_sby(String diam_pcfr_sby) {
		this.diam_pcfr_sby = diam_pcfr_sby;
	}

	public String getLocal_pcc_pf_id() {
		return local_pcc_pf_id;
	}

	public void setLocal_pcc_pf_id(String local_pcc_pf_id) {
		this.local_pcc_pf_id = local_pcc_pf_id;
	}

	public String getLocal_pcc_pf_act() {
		return local_pcc_pf_act;
	}

	public void setLocal_pcc_pf_act(String local_pcc_pf_act) {
		this.local_pcc_pf_act = local_pcc_pf_act;
	}

	public String getLocal_pcc_pf_sby() {
		return local_pcc_pf_sby;
	}

	public void setLocal_pcc_pf_sby(String local_pcc_pf_sby) {
		this.local_pcc_pf_sby = local_pcc_pf_sby;
	}

	public String getRule_base_id() {
		return rule_base_id;
	}

	public void setRule_base_id(String rule_base_id) {
		this.rule_base_id = rule_base_id;
	}

	public String getArp_map_high() {
		return arp_map_high;
	}

	public void setArp_map_high(String arp_map_high) {
		this.arp_map_high = arp_map_high;
	}

	public String getArp_map_med() {
		return arp_map_med;
	}

	public void setArp_map_med(String arp_map_med) {
		this.arp_map_med = arp_map_med;
	}

	public String getPgw_val_chng_col_nm() {
		return pgw_val_chng_col_nm;
	}

	public void setPgw_val_chng_col_nm(String pgw_val_chng_col_nm) {
		this.pgw_val_chng_col_nm = pgw_val_chng_col_nm;
	}

	public String[] getCheckNo() {
		return checkNo;
	}

	public void setCheckNo(String[] checkNo) {
		this.checkNo = checkNo;
	}

	public String getSearchOption() {
		return searchOption;
	}

	public void setSearchOption(String searchOption) {
		this.searchOption = searchOption;
	}

	public String getKeyword_fdate() {
		return keyword_fdate;
	}

	public void setKeyword_fdate(String keyword_fdate) {
		this.keyword_fdate = keyword_fdate;
	}

	public String getKeyword_tdate() {
		return keyword_tdate;
	}

	public void setKeyword_tdate(String keyword_tdate) {
		this.keyword_tdate = keyword_tdate;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getSearchOption2() {
		return searchOption2;
	}

	public void setSearchOption2(String searchOption2) {
		this.searchOption2 = searchOption2;
	}

	public String getKeyword2() {
		return keyword2;
	}

	public void setKeyword2(String keyword2) {
		this.keyword2 = keyword2;
	}

	public String getView_gubun() {
		return view_gubun;
	}

	public void setView_gubun(String view_gubun) {
		this.view_gubun = view_gubun;
	}

	public String getWork_state_nm() {
		return work_state_nm;
	}

	public void setWork_state_nm(String work_state_nm) {
		this.work_state_nm = work_state_nm;
	}

	public int getWork_state_cnt() {
		return work_state_cnt;
	}

	public void setWork_state_cnt(int work_state_cnt) {
		this.work_state_cnt = work_state_cnt;
	}

	public BigInteger getNew_gug_data_manage_no() {
		return new_gug_data_manage_no;
	}

	public void setNew_gug_data_manage_no(BigInteger new_gug_data_manage_no) {
		this.new_gug_data_manage_no = new_gug_data_manage_no;
	}
    
      
}
