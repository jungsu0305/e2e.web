package com.kt.e2e.Domain;
 
import java.util.Date;
 
public class UserComponent {
 
    private int bno;
    private String subject;
    private String content;
    private String writer;
    private Date reg_date;
    private int hit;
    
    private String[] checkNo;
    private String searchOption;
    private String keyword;
    private String[] inputNo;
    
    public int getBno() {
        return bno;
    }
 
    public void setBno(int bno) {
        this.bno = bno;
    }
 
    public String getSubject() {
        return subject;
    }
 
    public void setSubject(String subject) {
        this.subject = subject;
    }
 
    public String getContent() {
        return content;
    }
 
    public void setContent(String content) {
        this.content = content;
    }
 
    public String getWriter() {
        return writer;
    }
 
    public void setWriter(String writer) {
        this.writer = writer;
    }
 
    public Date getReg_date() {
        return reg_date;
    }
 
    public void setReg_date(Date reg_date) {
        this.reg_date = reg_date;
    }
 
    public int getHit() {
        return hit;
    }
 
    public void setHit(int hit) {
        this.hit = hit;
    }

	public String[] getCheckNo() {
		return checkNo;
	}

	public void setCheckNo(String[] checkNo) {
		this.checkNo = checkNo;
	}

	public String getSearchOption() {
		return searchOption;
	}

	public void setSearchOption(String searchOption) {
		this.searchOption = searchOption;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String[] getInputNo() {
		return inputNo;
	}

	public void setInputNo(String[] inputNo) {
		this.inputNo = inputNo;
	}


 
    
}
