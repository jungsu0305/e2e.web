package com.kt.e2e.Domain;
 
import java.util.Date;
 
public class CustComponent {
 
  
    private int cust_manage_no;
    private String cust_nm;
    private String apn;
    private String lte_id;
    private String cust_description;
    private String regist_id;
    private Date regist_dtm;
    private String modify_id;
    private Date modify_dtm;
    
    private String ip_type ;
    private String service_type;
    private String ded_line_use_yn;

    //화면 연결 부가 기능
    private String[] checkNo;
    private String searchOption;
    private String keyword;
    private String cust_conf_no;
    private String msg;
 

	public int getCust_manage_no() {
		return cust_manage_no;
	}

	public void setCust_manage_no(int cust_manage_no) {
		this.cust_manage_no = cust_manage_no;
	}

	public String getCust_nm() {
		return cust_nm;
	}

	public void setCust_nm(String cust_nm) {
		this.cust_nm = cust_nm;
	}

	public String getApn() {
		return apn;
	}

	public void setApn(String apn) {
		this.apn = apn;
	}

	public String getLte_id() {
		return lte_id;
	}

	public void setLte_id(String lte_id) {
		this.lte_id = lte_id;
	}

	public String getCust_description() {
		return cust_description;
	}

	public void setCust_description(String cust_description) {
		this.cust_description = cust_description;
	}

	public String getRegist_id() {
		return regist_id;
	}

	public void setRegist_id(String regist_id) {
		this.regist_id = regist_id;
	}

	public Date getRegist_dtm() {
		return regist_dtm;
	}

	public void setRegist_dtm(Date regist_dtm) {
		this.regist_dtm = regist_dtm;
	}

	public String getModify_id() {
		return modify_id;
	}

	public void setModify_id(String modify_id) {
		this.modify_id = modify_id;
	}

	public Date getModify_dtm() {
		return modify_dtm;
	}

	public void setModify_dtm(Date modify_dtm) {
		this.modify_dtm = modify_dtm;
	}

	public String[] getCheckNo() {
		return checkNo;
	}

	public void setCheckNo(String[] checkNo) {
		this.checkNo = checkNo;
	}

	public String getSearchOption() {
		return searchOption;
	}

	public void setSearchOption(String searchOption) {
		this.searchOption = searchOption;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getIp_type() {
		return ip_type;
	}

	public void setIp_type(String ip_type) {
		this.ip_type = ip_type;
	}

	public String getService_type() {
		return service_type;
	}

	public void setService_type(String service_type) {
		this.service_type = service_type;
	}

	public String getDed_line_use_yn() {
		return ded_line_use_yn;
	}

	public void setDed_line_use_yn(String ded_line_use_yn) {
		this.ded_line_use_yn = ded_line_use_yn;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCust_conf_no() {
		return cust_conf_no;
	}

	public void setCust_conf_no(String cust_conf_no) {
		this.cust_conf_no = cust_conf_no;
	}
 
    
}
