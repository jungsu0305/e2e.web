package com.kt.e2e.Domain;

import org.apache.ibatis.type.Alias;

@Alias("Connection")
public class ConnectionComponent extends BaseComponent {

	NodeAttributes source;
	NodeAttributes target;
	
	public NodeAttributes getSource() {
		return source;
	}
	public void setSource(NodeAttributes source) {
		this.source = source;
	}
	public NodeAttributes getTarget() {
		return target;
	}
	public void setTarget(NodeAttributes target) {
		this.target = target;
	}
	public void setTargetId(String id) {
		if(target == null) target = new NodeAttributes();
		target.setId(id);
	}
	public void setTargetPort(String port) {
		if(target == null) target = new NodeAttributes();
		target.setPort(port);
	}
	public void setSourceId(String Id) {
		if(source == null) source = new NodeAttributes();
		source.setId(Id);
	}
	public void setSourcePort(String port) {
		if(source == null) source = new NodeAttributes();
		source.setPort(port);
	}
	@Override
	public String toString() {
		return "ConnectionComponent [source=" + source + ", target=" + target + ", key=" + key + ", id=" + id
				+ ", type=" + type + "]";
	}
	
	
}
