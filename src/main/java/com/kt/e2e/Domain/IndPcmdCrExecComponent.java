package com.kt.e2e.Domain;
 
import java.math.BigInteger;
import java.util.Date;
 
public class IndPcmdCrExecComponent extends GugDataConfigDetSelComponent {
 
  
	private BigInteger work_command_create_no; /* 작업 명령어 생성 관리.작업 명령어 생성 번호--char(18) */ 
//	private BigInteger gug_data_manage_no; /* 작업 명령어 생성 관리.국 데이터 관리 번호--BIGSERIAL */ 
//	private int gug_data_history_no; /* 작업 명령어 생성 관리.국 데이터 이력 번호--int4 */ 
	private String equip_id; /* 작업 명령어 생성 관리.국 장비 관리 번호--serial */ 
	private String equip_name; /* 작업 명령어 생성 관리.장비명--serial */ 
	private String command_type_cd; /* 작업 명령어 생성 관리.명령어 구분 코드--char(18) */ 
	private String work_nm; /* 작업 명령어 생성 관리.작업 이름--char(18) */ 
	private String work_context; /* 작업 명령어 생성 관리.작업 내용--char(18) */ 
	private String create_command_sentence; /* 작업 명령어 생성 관리.생성 명령어 문장--char(18) */ 
	private String work_result; /* 작업 명령어 생성 관리.작업 결과--char(18) */ 
	private String ero_cc_yn; /* 작업 명령어 생성 관리.장비 담당 운용자 명령어 확인 여부--char(18) */ 
	private String each_create_yn; /* 작업 명령어 생성 관리.개별 생성 여부--char(18) */ 
	private String creater_id; /* 작업 명령어 생성 관리.생성자 아이디--char(18) */ 
	private Date create_dtm; /* 작업 명령어 생성 관리.생성 일시--char(18) */ 
	private String operater_id; //운용자 아이디
	
	private int exec_history_no; /* 작업 명령어 실행 이력.실행 이력 번호--char(18) */ 
	private String execter_id; /* 작업 명령어 실행 이력.실행자 아이디--char(18) */ 
	private Date exec_start_dtm; /* 작업 명령어 실행 이력.실행 시작 일시--char(18) */ 
	private Date exec_close_dtm; /* 작업 명령어 실행 이력.실행 종료 일시--char(18) */ 
	private String exec_state_cd; /* 작업 명령어 실행 이력.실행 상태 코드--char(18) */ 
	private String exec_state_nm; /*실행 상태명*/
	private String exec_return_message; /* 작업 명령어 실행 이력.실행 리턴 메시지--varchar(1000) */ 



    private String pgw_ip; /* ePCC 국 데이터.PGW IP--varchar(64) */ 
    private String sms_recv; /* ePCC 국 데이터.SMS 수신--varchar(64) */ 
    private String epcc_val_chng_col_nm; /* ePCC 국 데이터.ePCC 값이 변경된 칼럼 이름--varchar(100) */ 



    private String id; /* HSS 국 데이터.ID--numeric(3,0) */ 
    private String apn; /* HSS 국 데이터.APN--varchar(64) */ 
    private String apnoirepl; /* HSS 국 데이터.APNOIREPL--varchar(64) */ 
    private String pltcc; /* HSS 국 데이터.PLTECC--varchar(5) */ 
    private String uutype; /* HSS 국 데이터.UUTYPE--varchar(4) */ 
    private String mpname_cd; /* HSS 국 데이터.MPNAME 코드--varchar(4) */ 
    private String hss_val_chng_col_nm; /* HSS 국 데이터.HSS 값이 변경된 칼럼 이름--varchar(100) */ 


    private String apn_fqdn; /* MME 국 데이터.APN-FQDN--varchar(128) */ 
    private String pgwgrp; /* MME 국 데이터.PGWGRP--varchar(1023) */ 
    private String mme_val_chng_col_nm; /* MME 국 데이터.MME 값이 변경된 칼럼 이름--varchar(100) */ 

 
 


    
    /*화면 연결부가 변수시작*/
	private String[] sel_equip_id;//체크박스 선택된 장비아이디 form data로 받을시
	private String operater_command_confirm_yn;
	private String equip_type_nm; // 연동된 장비 타입명
	
	private String ems_pwd;
	private String cli_pwd; 
	private String confirm_pwd;
    
    /*화면 연결부가 변수종료*/
	public BigInteger getWork_command_create_no() {
		return work_command_create_no;
	}
	public void setWork_command_create_no(BigInteger work_command_create_no) {
		this.work_command_create_no = work_command_create_no;
	}
//	public BigInteger getGug_data_manage_no() {
//		return gug_data_manage_no;
//	}
//	public void setGug_data_manage_no(BigInteger gug_data_manage_no) {
//		this.gug_data_manage_no = gug_data_manage_no;
//	}
//	public int getGug_data_history_no() {
//		return gug_data_history_no;
//	}
//	public void setGug_data_history_no(int gug_data_history_no) {
//		this.gug_data_history_no = gug_data_history_no;
//	}
	public String getEquip_id() {
		return equip_id;
	}
	public void setEquip_id(String equip_id) {
		this.equip_id = equip_id;
	}
	public String getCommand_type_cd() {
		return command_type_cd;
	}
	public void setCommand_type_cd(String command_type_cd) {
		this.command_type_cd = command_type_cd;
	}
	public String getWork_nm() {
		return work_nm;
	}
	public void setWork_nm(String work_nm) {
		this.work_nm = work_nm;
	}
	public String getWork_context() {
		return work_context;
	}
	public void setWork_context(String work_context) {
		this.work_context = work_context;
	}
	public String getCreate_command_sentence() {
		return create_command_sentence;
	}
	public void setCreate_command_sentence(String create_command_sentence) {
		this.create_command_sentence = create_command_sentence;
	}
	public String getWork_result() {
		return work_result;
	}
	public void setWork_result(String work_result) {
		this.work_result = work_result;
	}
	public String getEro_cc_yn() {
		return ero_cc_yn;
	}
	public void setEro_cc_yn(String ero_cc_yn) {
		this.ero_cc_yn = ero_cc_yn;
	}
	public String getEach_create_yn() {
		return each_create_yn;
	}
	public void setEach_create_yn(String each_create_yn) {
		this.each_create_yn = each_create_yn;
	}
	public String getCreater_id() {
		return creater_id;
	}
	public void setCreater_id(String creater_id) {
		this.creater_id = creater_id;
	}
	public Date getCreate_dtm() {
		return create_dtm;
	}
	public void setCreate_dtm(Date create_dtm) {
		this.create_dtm = create_dtm;
	}
	public int getExec_history_no() {
		return exec_history_no;
	}
	public void setExec_history_no(int exec_history_no) {
		this.exec_history_no = exec_history_no;
	}
	public String getExecter_id() {
		return execter_id;
	}
	public void setExecter_id(String execter_id) {
		this.execter_id = execter_id;
	}
	public Date getExec_start_dtm() {
		return exec_start_dtm;
	}
	public void setExec_start_dtm(Date exec_start_dtm) {
		this.exec_start_dtm = exec_start_dtm;
	}
	public Date getExec_close_dtm() {
		return exec_close_dtm;
	}
	public void setExec_close_dtm(Date exec_close_dtm) {
		this.exec_close_dtm = exec_close_dtm;
	}
	public String getExec_state_cd() {
		return exec_state_cd;
	}
	public void setExec_state_cd(String exec_state_cd) {
		this.exec_state_cd = exec_state_cd;
	}
	public String getExec_return_message() {
		return exec_return_message;
	}
	public void setExec_return_message(String exec_return_message) {
		this.exec_return_message = exec_return_message;
	}

	public String[] getSel_equip_id() {
		return sel_equip_id;
	}
	public void setSel_equip_id(String[] sel_equip_id) {
		this.sel_equip_id = sel_equip_id;
	}
	public String getPgw_ip() {
		return pgw_ip;
	}
	public void setPgw_ip(String pgw_ip) {
		this.pgw_ip = pgw_ip;
	}
	public String getSms_recv() {
		return sms_recv;
	}
	public void setSms_recv(String sms_recv) {
		this.sms_recv = sms_recv;
	}
	public String getEpcc_val_chng_col_nm() {
		return epcc_val_chng_col_nm;
	}
	public void setEpcc_val_chng_col_nm(String epcc_val_chng_col_nm) {
		this.epcc_val_chng_col_nm = epcc_val_chng_col_nm;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getApn() {
		return apn;
	}
	public void setApn(String apn) {
		this.apn = apn;
	}
	public String getApnoirepl() {
		return apnoirepl;
	}
	public void setApnoirepl(String apnoirepl) {
		this.apnoirepl = apnoirepl;
	}
	public String getPltcc() {
		return pltcc;
	}
	public void setPltcc(String pltcc) {
		this.pltcc = pltcc;
	}
	public String getUutype() {
		return uutype;
	}
	public void setUutype(String uutype) {
		this.uutype = uutype;
	}
	public String getMpname_cd() {
		return mpname_cd;
	}
	public void setMpname_cd(String mpname_cd) {
		this.mpname_cd = mpname_cd;
	}
	public String getHss_val_chng_col_nm() {
		return hss_val_chng_col_nm;
	}
	public void setHss_val_chng_col_nm(String hss_val_chng_col_nm) {
		this.hss_val_chng_col_nm = hss_val_chng_col_nm;
	}
	public String getApn_fqdn() {
		return apn_fqdn;
	}
	public void setApn_fqdn(String apn_fqdn) {
		this.apn_fqdn = apn_fqdn;
	}
	public String getPgwgrp() {
		return pgwgrp;
	}
	public void setPgwgrp(String pgwgrp) {
		this.pgwgrp = pgwgrp;
	}
	public String getMme_val_chng_col_nm() {
		return mme_val_chng_col_nm;
	}
	public void setMme_val_chng_col_nm(String mme_val_chng_col_nm) {
		this.mme_val_chng_col_nm = mme_val_chng_col_nm;
	}
	public String getExec_state_nm() {
		return exec_state_nm;
	}
	public void setExec_state_nm(String exec_state_nm) {
		this.exec_state_nm = exec_state_nm;
	}
	public String getEquip_name() {
		return equip_name;
	}
	public void setEquip_name(String equip_name) {
		this.equip_name = equip_name;
	}
	public String getOperater_command_confirm_yn() {
		return operater_command_confirm_yn;
	}
	public void setOperater_command_confirm_yn(String operater_command_confirm_yn) {
		this.operater_command_confirm_yn = operater_command_confirm_yn;
	}
	public String getOperater_id() {
		return operater_id;
	}
	public void setOperater_id(String operater_id) {
		this.operater_id = operater_id;
	}
	public String getEquip_type_nm() {
		return equip_type_nm;
	}
	public void setEquip_type_nm(String equip_type_nm) {
		this.equip_type_nm = equip_type_nm;
	}
	public String getEms_pwd() {
		return ems_pwd;
	}
	public void setEms_pwd(String ems_pwd) {
		this.ems_pwd = ems_pwd;
	}
	public String getCli_pwd() {
		return cli_pwd;
	}
	public void setCli_pwd(String cli_pwd) {
		this.cli_pwd = cli_pwd;
	}
	public String getConfirm_pwd() {
		return confirm_pwd;
	}
	public void setConfirm_pwd(String confirm_pwd) {
		this.confirm_pwd = confirm_pwd;
	}
     
}
