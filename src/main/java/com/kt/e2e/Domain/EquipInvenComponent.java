package com.kt.e2e.Domain;
 
import java.util.Date;
 
public class EquipInvenComponent {
	
    private String equip_id;
    private String equip_name;
    private String status_name;
    private String equip_type;
    private String team_name;
    private String mname ;
    private String mcomp_name;
    private String sup_main_person_name ;
    private String sup_main_person_phone;
    private Date create_time;
    
    //EMS 연결접속정보
    private String sys_type	;		
    private String sys_name	;		
    private String ipaddr_1	;		
    private String id_1		;	
    private String pwd_1	;		
    private String expect_1	;    		          
    private String ipaddr_2	;		     
    private String id_2		;  
    private String pwd_2	;     
    private String expect_2	;  
    private String cli_cmd	; 
    private String cli_id	;	   
    private String cli_pwd	;		    
    private String cli_expect;	    
    private String confirm_expect;		
    private String confirm	;	 
    private String cli_exit ;     

    
    private String[] checkNo;
    private String searchOption;
    private String keyword;
    private int command_create_count;

	public String getEquip_id() {
		return equip_id;
	}

	public void setEquip_id(String equip_id) {
		this.equip_id = equip_id;
	}

	public String getEquip_name() {
		return equip_name;
	}

	public void setEquip_name(String equip_name) {
		this.equip_name = equip_name;
	}

	public String getStatus_name() {
		return status_name;
	}

	public void setStatus_name(String status_name) {
		this.status_name = status_name;
	}

	public String getEquip_type() {
		return equip_type;
	}

	public void setEquip_type(String equip_type) {
		this.equip_type = equip_type;
	}
	

	public String getTeam_name() {
		return team_name;
	}

	public void setTeam_name(String team_name) {
		this.team_name = team_name;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getMcomp_name() {
		return mcomp_name;
	}

	public void setMcomp_name(String mcomp_name) {
		this.mcomp_name = mcomp_name;
	}


	public String getSup_main_person_name() {
		return sup_main_person_name;
	}

	public void setSup_main_person_name(String sup_main_person_name) {
		this.sup_main_person_name = sup_main_person_name;
	}

	public String getSup_main_person_phone() {
		return sup_main_person_phone;
	}

	public void setSup_main_person_phone(String sup_main_person_phone) {
		this.sup_main_person_phone = sup_main_person_phone;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	
	
	
	public String getSys_type() {
		return sys_type;
	}

	public void setSys_type(String sys_type) {
		this.sys_type = sys_type;
	}

	public String getSys_name() {
		return sys_name;
	}

	public void setSys_name(String sys_name) {
		this.sys_name = sys_name;
	}

	public String getIpaddr_1() {
		return ipaddr_1;
	}

	public void setIpaddr_1(String ipaddr_1) {
		this.ipaddr_1 = ipaddr_1;
	}

	public String getId_1() {
		return id_1;
	}

	public void setId_1(String id_1) {
		this.id_1 = id_1;
	}

	public String getPwd_1() {
		return pwd_1;
	}

	public void setPwd_1(String pwd_1) {
		this.pwd_1 = pwd_1;
	}

	public String getExpect_1() {
		return expect_1;
	}

	public void setExpect_1(String expect_1) {
		this.expect_1 = expect_1;
	}

	public String getIpaddr_2() {
		return ipaddr_2;
	}

	public void setIpaddr_2(String ipaddr_2) {
		this.ipaddr_2 = ipaddr_2;
	}

	public String getId_2() {
		return id_2;
	}

	public void setId_2(String id_2) {
		this.id_2 = id_2;
	}

	public String getPwd_2() {
		return pwd_2;
	}

	public void setPwd_2(String pwd_2) {
		this.pwd_2 = pwd_2;
	}

	public String getExpect_2() {
		return expect_2;
	}

	public void setExpect_2(String expect_2) {
		this.expect_2 = expect_2;
	}

	public String getCli_cmd() {
		return cli_cmd;
	}

	public void setCli_cmd(String cli_cmd) {
		this.cli_cmd = cli_cmd;
	}

	public String getCli_id() {
		return cli_id;
	}

	public void setCli_id(String cli_id) {
		this.cli_id = cli_id;
	}

	public String getCli_pwd() {
		return cli_pwd;
	}

	public void setCli_pwd(String cli_pwd) {
		this.cli_pwd = cli_pwd;
	}

	public String getCli_expect() {
		return cli_expect;
	}

	public void setCli_expect(String cli_expect) {
		this.cli_expect = cli_expect;
	}

	public String getConfirm_expect() {
		return confirm_expect;
	}

	public void setConfirm_expect(String confirm_expect) {
		this.confirm_expect = confirm_expect;
	}

	public String getConfirm() {
		return confirm;
	}

	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}

	public String getCli_exit() {
		return cli_exit;
	}

	public void setCli_exit(String cli_exit) {
		this.cli_exit = cli_exit;
	}

	public String[] getCheckNo() {
		return checkNo;
	}

	public void setCheckNo(String[] checkNo) {
		this.checkNo = checkNo;
	}

	public String getSearchOption() {
		return searchOption;
	}

	public void setSearchOption(String searchOption) {
		this.searchOption = searchOption;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int getCommand_create_count() {
		return command_create_count;
	}

	public void setCommand_create_count(int command_create_count) {
		this.command_create_count = command_create_count;
	}


    
}
