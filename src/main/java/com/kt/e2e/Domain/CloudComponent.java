package com.kt.e2e.Domain;

import org.apache.ibatis.type.Alias;

@Alias("Cloud")
public class CloudComponent extends BaseComponent {

	String name;
	Integer width;
	Integer height;
	Integer x;
	Integer y;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getWidth() {
		return width;
	}
	public void setWidth(Integer width) {
		this.width = width;
	}
	public Integer getHeight() {
		return height;
	}
	public void setHeight(Integer height) {
		this.height = height;
	}
	public Integer getX() {
		return x;
	}
	public void setX(Integer x) {
		this.x = x;
	}
	public Integer getY() {
		return y;
	}
	public void setY(Integer y) {
		this.y = y;
	}
	@Override
	public String toString() {
		return "CloudComponent [name=" + name + ", width=" + width + ", height=" + height + ", x=" + x + ", y=" + y
				+ ", key=" + key + ", id=" + id + ", type=" + type + "]";
	}
}
