package com.kt.e2e.Domain;

import org.apache.ibatis.type.Alias;

@Alias("NsProf")
public class NsProfComponent extends BaseComponent {

	String nsProfId;
	String profInf;
	String affintyGroupId;
	
	public String getNsProfId() {
		return nsProfId;
	}
	public void setNsProfId(String nsProfId) {
		this.nsProfId = nsProfId;
	}
	public String getProfInf() {
		return profInf;
	}
	public void setProfInf(String profInf) {
		this.profInf = profInf;
	}
	public String getAffintyGroupId() {
		return affintyGroupId;
	}
	public void setAffintyGroupId(String affintyGroupId) {
		this.affintyGroupId = affintyGroupId;
	}
	@Override
	public String toString() {
		return "NsProfComponent [nsProfId=" + nsProfId + ", profInf=" + profInf + ", affintyGroupId=" + affintyGroupId + "]";
	}
}
