package com.kt.e2e.Domain;
 
import java.math.BigInteger;
import java.util.Date;
 
public class DataSummaryComponent {
 
	private String summary_repr_no; /* 데이타 요약화 관리.요약화 대표 번호--SERIAL */ 
	private String summary_repr_nm; /* 데이타 요약화 관리.요약화 대표 이름--varchar(100) */ 
	private String summary_type_nm_1; /* 데이타 요약화 관리.요약화 구분 이름_1--varchar(100) */ 
	private String summary_type_val_1; /* 데이타 요약화 관리.요약화 구분 값_1--varchar(100) */ 
	private String summary_type_nm_2; /* 데이타 요약화 관리.요약화 구분 이름_2--varchar(100) */ 
	private String summary_type_val_2; /* 데이타 요약화 관리.요약화 구분 값_2--varchar(100) */ 
	private String summary_type_nm_3; /* 데이타 요약화 관리.요약화 구분 이름_3--varchar(100) */ 
	private String summary_type_val_3; /* 데이타 요약화 관리.요약화 구분 값_3--varchar(100) */ 
	private String summary_type_nm_4; /* 데이타 요약화 관리.요약화 구분 이름_4--varchar(100) */ 
	private String summary_type_val_4; /* 데이타 요약화 관리.요약화 구분 값_4--varchar(100) */ 
	private String summary_type_nm_5; /* 데이타 요약화 관리.요약화 구분 이름_5--varchar(100) */ 
	private String summary_type_val_5; /* 데이타 요약화 관리.요약화 구분 값_5--varchar(100) */ 
	private String summary_type_nm_6; /* 데이타 요약화 관리.요약화 구분 이름_6--varchar(100) */ 
	private String summary_type_val_6; /* 데이타 요약화 관리.요약화 구분 값_6--varchar(100) */ 
	private String regist_id; /* 데이타 요약화 관리.등록자 아이디--varchar(30) */ 
	private String regist_dtm; /* 데이타 요약화 관리.등록 일시--date */ 
	private String modify_id; /* 데이타 요약화 관리.수정자 아이디--varchar(30) */ 
	private String modify_dtm; /* 데이타 요약화 관리.수정 일시--date */
	public String getSummary_repr_no() {
		return summary_repr_no;
	}
	public void setSummary_repr_no(String summary_repr_no) {
		this.summary_repr_no = summary_repr_no;
	}
	public String getSummary_repr_nm() {
		return summary_repr_nm;
	}
	public void setSummary_repr_nm(String summary_repr_nm) {
		this.summary_repr_nm = summary_repr_nm;
	}
	public String getSummary_type_nm_1() {
		return summary_type_nm_1;
	}
	public void setSummary_type_nm_1(String summary_type_nm_1) {
		this.summary_type_nm_1 = summary_type_nm_1;
	}
	public String getSummary_type_val_1() {
		return summary_type_val_1;
	}
	public void setSummary_type_val_1(String summary_type_val_1) {
		this.summary_type_val_1 = summary_type_val_1;
	}
	public String getSummary_type_nm_2() {
		return summary_type_nm_2;
	}
	public void setSummary_type_nm_2(String summary_type_nm_2) {
		this.summary_type_nm_2 = summary_type_nm_2;
	}
	public String getSummary_type_val_2() {
		return summary_type_val_2;
	}
	public void setSummary_type_val_2(String summary_type_val_2) {
		this.summary_type_val_2 = summary_type_val_2;
	}
	public String getSummary_type_nm_3() {
		return summary_type_nm_3;
	}
	public void setSummary_type_nm_3(String summary_type_nm_3) {
		this.summary_type_nm_3 = summary_type_nm_3;
	}
	public String getSummary_type_val_3() {
		return summary_type_val_3;
	}
	public void setSummary_type_val_3(String summary_type_val_3) {
		this.summary_type_val_3 = summary_type_val_3;
	}
	public String getSummary_type_nm_4() {
		return summary_type_nm_4;
	}
	public void setSummary_type_nm_4(String summary_type_nm_4) {
		this.summary_type_nm_4 = summary_type_nm_4;
	}
	public String getSummary_type_val_4() {
		return summary_type_val_4;
	}
	public void setSummary_type_val_4(String summary_type_val_4) {
		this.summary_type_val_4 = summary_type_val_4;
	}
	public String getSummary_type_nm_5() {
		return summary_type_nm_5;
	}
	public void setSummary_type_nm_5(String summary_type_nm_5) {
		this.summary_type_nm_5 = summary_type_nm_5;
	}
	public String getSummary_type_val_5() {
		return summary_type_val_5;
	}
	public void setSummary_type_val_5(String summary_type_val_5) {
		this.summary_type_val_5 = summary_type_val_5;
	}
	
	public String getSummary_type_nm_6() {
		return summary_type_nm_6;
	}
	public void setSummary_type_nm_6(String summary_type_nm_6) {
		this.summary_type_nm_6 = summary_type_nm_6;
	}
	public String getSummary_type_val_6() {
		return summary_type_val_6;
	}
	public void setSummary_type_val_6(String summary_type_val_6) {
		this.summary_type_val_6 = summary_type_val_6;
	}
	public String getRegist_id() {
		return regist_id;
	}
	public void setRegist_id(String regist_id) {
		this.regist_id = regist_id;
	}
	public String getRegist_dtm() {
		return regist_dtm;
	}
	public void setRegist_dtm(String regist_dtm) {
		this.regist_dtm = regist_dtm;
	}
	public String getModify_id() {
		return modify_id;
	}
	public void setModify_id(String modify_id) {
		this.modify_id = modify_id;
	}
	public String getModify_dtm() {
		return modify_dtm;
	}
	public void setModify_dtm(String modify_dtm) {
		this.modify_dtm = modify_dtm;
	} 

      
}
