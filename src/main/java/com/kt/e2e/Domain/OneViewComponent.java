package com.kt.e2e.Domain;
 
import java.util.Date;

import org.apache.ibatis.type.Alias;

import com.kt.e2e.Common.CommonPager;
 
@Alias("OneView")
public class OneViewComponent {
 
	private String nsd_id;
	private String designer;
	private String version;
	private String nsd_name;
	private String vnfd_id;
	private String target_nfvo;
	private String ns_type;
	private String nsd_desc;
	private String nsd_state;
	
    /*화면 연결부가 변수시작*/
    private String[] checkNo;
    private String searchOption;
    private String keyword_fdate;
    private String keyword_tdate;
    private String keyword;
    private String searchOption2;
    private String keyword2;
    private String view_gubun;
    private int search_type;
    
    private String prev_nsd_id;
    private String now_nsd_id;
    private String next_nsd_id;
    
    private int curPage;
    private String orderKey;
    private String colName;
    private String ordOption;
    
    
    private String work_state_nm;
    private int work_state_cnt;
    /*화면 연결부가 변수종료*/
    
    private Date time_stamp;
    
	public Date getTime_stamp() {
		return time_stamp;
	}
	public void setTime_stamp(Date time_stamp) {
		this.time_stamp = time_stamp;
	}
	public String getNsd_id() {
		return nsd_id;
	}
	public void setNsd_id(String nsd_id) {
		this.nsd_id = nsd_id;
	}
	
	public String getDesigner() {
		return designer;
	}
	public void setDesigner(String designer) {
		this.designer = designer;
	}
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	
	public String getNsd_name() {
		return nsd_name;
	}
	public void setNsd_name(String nsd_name) {
		this.nsd_name = nsd_name;
	}

	public String getVnfd_id() {
		return vnfd_id;
	}
	public void setVnfd_id(String vnfd_id) {
		this.vnfd_id = vnfd_id;
	}

	public String getTarget_nfvo() {
		return target_nfvo;
	}
	public void setTarget_nfvo(String target_nfvo) {
		this.target_nfvo = target_nfvo;
	}

	public String getNs_type() {
		return ns_type;
	}
	public void setNs_type(String ns_type) {
		this.ns_type = ns_type;
	}

	public String getNsd_desc() {
		return nsd_desc;
	}
	public void setNsd_desc(String nsd_desc) {
		this.nsd_desc = nsd_desc;
	}
	
	public String getNsd_state() {
		return nsd_state;
	}
	public void setNsd_state(String nsd_state) {
		this.nsd_state = nsd_state;
	}
	
    
    
    
    
	public String[] getCheckNo() {
		return checkNo;
	}

	public void setCheckNo(String[] checkNo) {
		this.checkNo = checkNo;
	}

	public String getSearchOption() {
		return searchOption;
	}

	public void setSearchOption(String searchOption) {
		this.searchOption = searchOption;
	}

	public String getKeyword_fdate() {
		return keyword_fdate;
	}

	public void setKeyword_fdate(String keyword_fdate) {
		this.keyword_fdate = keyword_fdate;
	}

	public String getKeyword_tdate() {
		return keyword_tdate;
	}

	public void setKeyword_tdate(String keyword_tdate) {
		this.keyword_tdate = keyword_tdate;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getSearchOption2() {
		return searchOption2;
	}

	public void setSearchOption2(String searchOption2) {
		this.searchOption2 = searchOption2;
	}

	public String getKeyword2() {
		return keyword2;
	}

	public void setKeyword2(String keyword2) {
		this.keyword2 = keyword2;
	}

	public int getCurPage() {
		return curPage;
	}

	public void setCurPage(int curPage) {
		this.curPage = curPage;
	}
	
	public String getOrderKey() {
		return orderKey;
	}

	public void setOrderKey(String orderKey) {
		this.orderKey = orderKey;
	}
	
	public String getColName() {
		return colName;
	}

	public void setColName(String colName) {
		this.colName = colName;
	}
	
	public String getOrdOption() {
		return ordOption;
	}

	public void setOrdOption(String ordOption) {
		this.ordOption = ordOption;
	}
	
	public String getView_gubun() {
		return view_gubun;
	}

	public void setView_gubun(String view_gubun) {
		this.view_gubun = view_gubun;
	}
	
	public int getSearch_type() {
		return search_type;
	}

	public void setSearch_type(int search_type) {
		this.search_type = search_type;
	}
	
	public String getPrev_nsd_id() {
		return prev_nsd_id;
	}

	public void setPrev_nsd_id(String prev_nsd_id) {
		this.prev_nsd_id = prev_nsd_id;
	}
	public String getNow_nsd_id() {
		return now_nsd_id;
	}

	public void setNow_nsd_id(String now_nsd_id) {
		this.now_nsd_id = now_nsd_id;
	}
	public String getNext_nsd_id() {
		return next_nsd_id;
	}

	public void setNext_nsd_id(String next_nsd_id) {
		this.next_nsd_id = next_nsd_id;
	}

	public String getWork_state_nm() {
		return work_state_nm;
	}

	public void setWork_state_nm(String work_state_nm) {
		this.work_state_nm = work_state_nm;
	}

	public int getWork_state_cnt() {
		return work_state_cnt;
	}

	public void setWork_state_cnt(int work_state_cnt) {
		this.work_state_cnt = work_state_cnt;
	}
    
}
