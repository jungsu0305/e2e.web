package com.kt.e2e.Domain;
 
import java.math.BigInteger;
import java.util.Date;

import org.apache.ibatis.type.Alias;
 
@Alias("InvNfvo")
public class InvNfvoComponent {
 
	private String nfvo_name;
	private String nfvo_id;
	private String nfvo_desc;
	private String ip_addr;
	private int port;
    
	public String getNfvo_name() {
		return nfvo_name;
	}
	public void setNfvo_name(String nfvo_name) {
		this.nfvo_name = nfvo_name;
	}
	
	public String getNfvo_id() {
		return nfvo_id;
	}
	public void setNfvo_id(String nfvo_id) {
		this.nfvo_id = nfvo_id;
	}
	
	public String getNfvo_desc() {
		return nfvo_desc;
	}
	public void setNfvo_desc(String nfvo_desc) {
		this.nfvo_desc = nfvo_desc;
	}
	
	public String getIp_addr() {
		return ip_addr;
	}
	public void setIp_addr(String ip_addr) {
		this.ip_addr = ip_addr;
	}

	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}

}
