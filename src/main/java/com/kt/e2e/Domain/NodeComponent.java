package com.kt.e2e.Domain;

import org.apache.ibatis.type.Alias;

@Alias("Node")
public class NodeComponent extends BaseComponent {

	String name;
	String path;
	Integer x;
	Integer y;
	String bgColor;
	String vnfdName;
	String vnfdId;
	String vnfd_id;
	String vnfd_name;
	
	public String getVnfd_id() {
		return vnfd_id;
	}
	public void setVnfd_id(String vnfd_id) {
		this.vnfd_id = vnfd_id;
	}
	public String getVnfd_name() {
		return vnfd_name;
	}
	public void setVnfd_name(String vnfd_name) {
		this.vnfd_name = vnfd_name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public Integer getX() {
		return x;
	}
	public void setX(Integer x) {
		this.x = x;
	}
	public Integer getY() {
		return y;
	}
	public void setY(Integer y) {
		this.y = y;
	}
	public String getBgColor() {
		return bgColor;
	}
	public void setBgColor(String bgColor) {
		this.bgColor = bgColor;
	}
	public String getVnfdName() {
		return vnfdName;
	}
	public void setVnfdName(String vnfdName) {
		this.vnfdName = vnfdName;
	}
	public String getVnfdId() {
		return vnfdId;
	}
	public void setVnfdId(String vnfdId) {
		this.vnfdId = vnfdId;
	}
	
	@Override
	public String toString() {
		return "NodeComponent [name=" + name + ", path=" + path + ", x=" + x + ", y=" + y + ", bgColor=" + bgColor
				+ ", key=" + key + ", id=" + id + ", type=" + type
				+ ", vnfdName=" + vnfdName + ", vnfdId=" + vnfdId + "]";
	}
}
