package com.kt.e2e.Domain;
 
import java.util.Date;
 
public class EquipCmdBasComponent {
	
	private String equip_command_manage_no; /* 장비 명령어 관리.장비 명령어 관리 번호--serial */ 
	private String gug_equip_type_cd; /* 장비 명령어 관리.국 장비 구분 코드--varchar(5) */ 
	private String gug_equip_type_nm; /* 장비타입명*/
	private String command_comp_type_cd; /* 장비 명령어 관리.명령어 회사 구분 코드--char(4) */ 
	private String command; /* 장비 명령어 관리.명령어--varchar(500) */ 
	private String command_help; /* 장비 명령어 관리.명령어 도움말--varchar(1000) */ 
	private String command_exec_popup_type_cd; /* 장비 명령어 관리.명령어 실행시 팝업 구분 코드--char(18) */ 
	private String note_context; /* 장비 명령어 관리.비고 내용--varchar(1000) */ 

	private String parameter; /* 장비 명령어 파라미터 관리.파라미터--varchar(20) */ 
	private String man_opt; /* 장비 명령어 파라미터 관리.MAN/OPT--varchar(5) */ 
	private String parameter_type; /* 장비 명령어 파라미터 관리.파라미터 타입--varchar(10) */ 
	private String min; /* 장비 명령어 파라미터 관리.MIN--int4 */ 
	private String max; /* 장비 명령어 파라미터 관리.MAX--int4 */ 
	private String enum_list; /* 장비 명령어 파라미터 관리.ENUM_LIST--varchar(300) */ 
	private String regist_id; /* 장비 명령어 파라미터 관리.등록자 아이디--varchar(30) */ 
	private Date regist_dtm; /* 장비 명령어 파라미터 관리.등록 일시--date */ 
	private String modify_id; /* 장비 명령어 파라미터 관리.수정자 아이디--varchar(30) */ 
	private Date modify_dtm; /* 장비 명령어 파라미터 관리.수정 일시--date */ 
	private String parameter_seq_no; /* 장비 명령어 파라미터 관리.파라미터 순서번호 */ 
    
    private String[] checkNo;
    private String searchOption;
    private String keyword;
 

	public String getEquip_command_manage_no() {
		return equip_command_manage_no;
	}

	public void setEquip_command_manage_no(String equip_command_manage_no) {
		this.equip_command_manage_no = equip_command_manage_no;
	}

	public String getGug_equip_type_cd() {
		return gug_equip_type_cd;
	}

	public void setGug_equip_type_cd(String gug_equip_type_cd) {
		this.gug_equip_type_cd = gug_equip_type_cd;
	}

	public String getGug_equip_type_nm() {
		return gug_equip_type_nm;
	}

	public void setGug_equip_type_nm(String gug_equip_type_nm) {
		this.gug_equip_type_nm = gug_equip_type_nm;
	}

	public String getCommand_comp_type_cd() {
		return command_comp_type_cd;
	}

	public void setCommand_comp_type_cd(String command_comp_type_cd) {
		this.command_comp_type_cd = command_comp_type_cd;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getCommand_help() {
		return command_help;
	}

	public void setCommand_help(String command_help) {
		this.command_help = command_help;
	}

	public String getCommand_exec_popup_type_cd() {
		return command_exec_popup_type_cd;
	}

	public void setCommand_exec_popup_type_cd(String command_exec_popup_type_cd) {
		this.command_exec_popup_type_cd = command_exec_popup_type_cd;
	}

	public String getNote_context() {
		return note_context;
	}

	public void setNote_context(String note_context) {
		this.note_context = note_context;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getMan_opt() {
		return man_opt;
	}

	public void setMan_opt(String man_opt) {
		this.man_opt = man_opt;
	}

	public String getParameter_type() {
		return parameter_type;
	}

	public void setParameter_type(String parameter_type) {
		this.parameter_type = parameter_type;
	}

	public String getMin() {
		return min;
	}

	public void setMin(String min) {
		this.min = min;
	}

	public String getMax() {
		return max;
	}

	public void setMax(String max) {
		this.max = max;
	}

	public String getEnum_list() {
		return enum_list;
	}

	public void setEnum_list(String enum_list) {
		this.enum_list = enum_list;
	}

	public String getRegist_id() {
		return regist_id;
	}

	public void setRegist_id(String regist_id) {
		this.regist_id = regist_id;
	}

	public Date getRegist_dtm() {
		return regist_dtm;
	}

	public void setRegist_dtm(Date regist_dtm) {
		this.regist_dtm = regist_dtm;
	}

	public String getModify_id() {
		return modify_id;
	}

	public void setModify_id(String modify_id) {
		this.modify_id = modify_id;
	}

	public Date getModify_dtm() {
		return modify_dtm;
	}

	public void setModify_dtm(Date modify_dtm) {
		this.modify_dtm = modify_dtm;
	}

	
	public String getParameter_seq_no() {
		return parameter_seq_no;
	}

	public void setParameter_seq_no(String parameter_seq_no) {
		this.parameter_seq_no = parameter_seq_no;
	}

	public String[] getCheckNo() {
		return checkNo;
	}

	public void setCheckNo(String[] checkNo) {
		this.checkNo = checkNo;
	}

	public String getSearchOption() {
		return searchOption;
	}

	public void setSearchOption(String searchOption) {
		this.searchOption = searchOption;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
 
    
}
