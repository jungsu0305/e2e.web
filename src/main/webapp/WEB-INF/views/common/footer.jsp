<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<footer>
		<!-- footer_nav -->
		<nav class="footer_nav">
			<ul>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">이용약관</a></li>
				<li><a href="#">개인정보처리방침</a></li>
				<li><a href="#">Sitemap</a></li>
			</ul>
		</nav>
		<!-- //footer_nav -->
		<address>㈜케이티  대표이사 황창규   경기도 성남시 분당구 불정로 90 (정자동)   사업자등록번호 : 102-81-42945   통신판매업신고 : 2002-경기성남-0047</address>
	</footer>