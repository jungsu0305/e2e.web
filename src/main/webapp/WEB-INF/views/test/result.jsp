<!DOCTYPE html>
<head>
<title>Topology Editor Demo Result</title>
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport" content="width=device-width, minimum-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="/exam/style.css"></link><!-- sample css -->
	
<script src="/scripts/utils/jquery-1.12.4.js"></script>
<script src="/scripts/utils/jquery-ui-1.8.23.custom.min.js"></script>
<script src="/scripts/utils/jquery.browser.js"></script>
<script src="/scripts/utils/less-1.7.5.min.js"></script>
 
<script src="/scripts/draw2d.v6/shifty.js"></script>
<script src="/scripts/draw2d.v6/raphael.js"></script>
<script src="/scripts/draw2d.v6/jquery.autoresize.js"></script>
<script src="/scripts/draw2d.v6/jquery-touch_punch.js"></script>
<script src="/scripts/draw2d.v6/jquery.contextmenu.js"></script>
<script src="/scripts/draw2d.v6/rgbcolor.js"></script>
<script src="/scripts/draw2d.v6/canvg.js"></script>
<script src="/scripts/draw2d.v6/Class.js"></script>
<script src="/scripts/draw2d.v6/json2.js"></script>
<script src="/scripts/draw2d.v6/pathfinding-browser.min.js"></script>
<script src="/scripts/draw2d.v6/draw2d.js"></script>

<script src="/scripts/Config.js"></script>
<script src="/scripts/Apps.js"></script>
<script src="/scripts/Components.js"></script>


<script type="text/javascript">

// loading app
var app;

$(window).load(function() {
	$("#logo > span:last").on('click', function() {
		app.view.clear();
		app.loading();
	});
	
	app = new e2e.Application("canvas", true);
	var view = app.view;
	var reader = app.reader;

	app.loading = function() {
		/**/
		$.get('/test/saved/data1', function(res) {
			console.log('data:saved',res);
			if(res.err == 0) {
				reader.unmarshal(view, res.data1);
				console.log('loading ok');
			}
		});
		/**/
	};
	
	app.loading();
	/*/
	if(app.isReadOnly) {
		$(".e2e_policy_Port").css('cursor','default');
	}
	/**/
});
</script>

<style>
.e2e_policy_Port { cursor:default; }
</style>

</head>
<body id="container">
	<div id="logo">
		<span>Result</span>
		<span id="save-id" name="save-name" class="save save-class">re-loading</span>
	</div>
	
	<div id="component-container"></div>
	<div id="component-container2"></div>
	
	<div id="canvas" style="width:1280px;height:1024px"></div>
</body>
</html>