<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>test</title>

<script src="/scripts/utils/jquery-1.12.4.js"></script>
<script src="/scripts/utils/jquery-ui-1.8.23.custom.min.js"></script>
<script src="/scripts/utils/jquery.browser.js"></script>
<script src="/scripts/utils/less-1.7.5.min.js"></script>
 
<script src="/scripts/draw2d.v6/shifty.js"></script>
<script src="/scripts/draw2d.v6/raphael.js"></script>
<script src="/scripts/draw2d.v6/jquery.autoresize.js"></script>
<script src="/scripts/draw2d.v6/jquery-touch_punch.js"></script>
<script src="/scripts/draw2d.v6/jquery.contextmenu.js"></script>
<script src="/scripts/draw2d.v6/rgbcolor.js"></script>
<script src="/scripts/draw2d.v6/canvg.js"></script>
<script src="/scripts/draw2d.v6/Class.js"></script>
<script src="/scripts/draw2d.v6/json2.js"></script>
<script src="/scripts/draw2d.v6/pathfinding-browser.min.js"></script>
<script src="/scripts/draw2d.v6/draw2d.js"></script>

<script src="/scripts/Config.js"></script>
<script src="/scripts/Apps.js"></script>
<script src="/scripts/Components.js"></script>

<script src="/scripts/utils/jsrender.js"></script>
<script id="component-template" type="text/x-jsrender">
	<div id="{{:name}}" class="component">
		<img src="{{:path}}">
		<span>{{:name}}</span>
	</div>
</script>

<script type="text/javascript">
var example = {};

/**
 * @class example.connection_locator.LabelConnection
 * 
 * A simple Connection with a label wehich sticks in the middle of the connection..
 *
 * @author Andreas Herz
 * @extend draw2d.Connection
 */
example.Label = draw2d.shape.node.End.extend({
	NAME:"example.Label",
	init:function() { 
		this._super();
		//this.setNodeTitle("test");		//default nodeTitle
		
		this.label1 = new draw2d.shape.basic.Label({text:name});
		this.add(label1, new draw2d.layout.locator.BottomLocator());
	},
	getPersistentAttributes:function() {
		var m = this._super();
		return m;
		
	},
	setPersistentAttributes:function(m) {
		this._super(m);
		//this.setNodeTitle(m.label1.text);
		this.label1 = new draw2d.shape.basic.Label({text:m.label1.text});
		this.add(label1, new draw2d.layout.locator.BottomLocator());
	},
});
example.LabeledEnd = draw2d.shape.node.End.extend({

    NAME : "example.LabeledEnd",

    init:function()
    {
      this._super();
      
      // labels are added via JSON document.
    },

    /**
     * @method 
     * Return an objects with all important attributes for XML or JSON serialization
     * 
     * @returns {Object}
     */
    getPersistentAttributes : function()
    {
        var memento = this._super();
        
        // add all decorations to the memento 
        //
        console.log('getPA');
        memento.labels = [];
        this.children.each(function(i,e){
            var labelJSON = e.figure.getPersistentAttributes();
            labelJSON.locator=e.locator.NAME;
            console.log(labelJSON);
            console.log(e.locator.NAME);
            memento.labels.push(labelJSON);
        });
    
        return memento;
    },
    
    /**
     * @method 
     * Read all attributes from the serialized properties and transfer them into the shape.
     * 
     * @param {Object} memento
     * @returns 
     */
    setPersistentAttributes : function(memento)
    {
        this._super(memento);
        console.log('setPA');
        // remove all decorations created in the constructor of this element
        //
        this.resetChildren();
        
        // and add all children of the JSON document.
        //
        /**/
        $.each(memento.labels, $.proxy(function(i,json){
            // create the figure stored in the JSON
            console.log('type:'+json.type);
            var figure =  eval("new "+json.type+"()");
            
            // apply all attributes
            figure.attr(json)
            
            // instantiate the locator
            var locator =  eval("new "+json.locator+"()");
            
            // add the new figure as child to this figure
            this.add(figure, locator);
        },this));
        /*/
        var label = new draw2d.shape.basic.Label({text:memento.label1.text});
        console.log(label);
        this.add(label, new draw2d.layout.locator.BottomLocator());
        /**/
        console.log('setPA END');
    }
});


var jsonDocument =
    [
    {
        "type": "example.LabeledEnd",
        "id": "ebfb35bb-5767-8155-c804-14bda7759dc2",
        "x": 312,
        "y": 154,
        "width": 50,
        "height": 50,
        "alpha": 1,
        "angle": 0,
        "userData": {},
        "cssClass": "draw2d_shape_node_End",
        "ports": [
            {
                "type": "draw2d.InputPort",
                "id": "2fd4e14e-0248-b419-ce31-8d63f0da322d",
                "width": 10,
                "height": 10,
                "alpha": 1,
                "angle": 0,
                "userData": {},
                "cssClass": "draw2d_InputPort",
                "bgColor": "#4F6870",
                "color": "#1B1B1B",
                "stroke": 1,
                "dasharray": null,
                "maxFanOut": 9007199254740991,
                "name": "input0",
                "port": "draw2d.InputPort",
                "locator": "draw2d.layout.locator.InputPortLocator"
            }
        ],
        "bgColor": "#4D90FE",
        "color": "#000000",
        "stroke": 1,
        "radius": 2,
        "dasharray": null,
        "label1":{
        	"type": "draw2d.shape.basic.Label",
            "id": "db69a8ab-a63b-f4f3-c025-5fae6b370790",
            "x": -5.7578125,
            "y": -23,
            "width": 67.25,
            "height": 21,
            "alpha": 1,
            "angle": 0,
            "userData": {},
            "cssClass": "draw2d_shape_basic_Label",
            "ports": [],
            "bgColor": "none",
            "color": "#1B1B1B",
            "stroke": 1,
            "radius": 0,
            "dasharray": null,
            "text": "My top Label",
            "outlineStroke": 0,
            "outlineColor": "none",
            "fontSize": 9,
            "fontColor": "#080808",
            "fontFamily": null,
            "locator": "draw2d.layout.locator.BottomLocator",
            "editor": "draw2d.ui.LabelInplaceEditor"
        },
        "labels": [
            {
                "type": "draw2d.shape.basic.Label",
                "id": "5c6234b7-4475-22cb-e278-1df506c1e9fe",
                "x": 62,
                "y": 26,
                "width": 83.921875,
                "height": 21,
                "alpha": 1,
                "angle": 0,
                "userData": {},
                "cssClass": "draw2d_shape_basic_Label",
                "ports": [],
                "bgColor": "none",
                "color": "#1B1B1B",
                "stroke": 1,
                "radius": 0,
                "dasharray": null,
                "text": "Draggable Label",
                "outlineStroke": 0,
                "outlineColor": "none",
                "fontSize": 14,
                "fontColor": "#080808",
                "fontFamily": null,
                "locator": "draw2d.layout.locator.DraggableLocator",
                "editor": "draw2d.ui.LabelInplaceEditor"
            },
            {
                "type": "draw2d.shape.basic.Label",
                "id": "db69a8ab-a63b-f4f3-c025-5fae6b370790",
                "x": -5.7578125,
                "y": -23,
                "width": 67.25,
                "height": 21,
                "alpha": 1,
                "angle": 0,
                "userData": {},
                "cssClass": "draw2d_shape_basic_Label",
                "ports": [],
                "bgColor": "none",
                "color": "#1B1B1B",
                "stroke": 1,
                "radius": 0,
                "dasharray": null,
                "text": "My top Label",
                "outlineStroke": 0,
                "outlineColor": "none",
                "fontSize": 9,
                "fontColor": "#080808",
                "fontFamily": null,
                "locator": "draw2d.layout.locator.TopLocator",
                "editor": "draw2d.ui.LabelInplaceEditor"
            }
        ]
    }
    ];
var key = "${key}";
$(window).load(function () {
console.log(1);
console.log(key);
	/*/
    draw2d.shape.basic.Label.inject( {
        clearCache:function() {
                this.portRelayoutRequired=true;
                this.cachedMinWidth  = null;
                this.cachedMinHeight = null;
                this.cachedWidth=null;
                this.cachedHeight=null;
                this.lastAppliedTextAttributes= {};
                return this;
            }
      });
    /**/
	  // create the canvas for the user interaction
	  //
	  var canvas = new draw2d.Canvas("gfx_holder");
	  
	  // unmarshal the JSON document into the canvas 
	  // (load) 
	  var reader = new draw2d.io.json.Reader();
	  reader.unmarshal(canvas, jsonDocument);
	  
	  // display the JSON text in the preview DIV 
	  //
	  displayJSON(canvas);
	  
	  // add an event listener to the Canvas for change notifications.
	  // We just dump the current canvas document as simple text into 
	  // the DIV 
	  
	  //
	  /*/
	  canvas.getCommandStack().addEventListener(function(e){
	      if(e.isPostChangeEvent()){
	          displayJSON(canvas, false);
	      }
	  });
	  /**/
	var d = new draw2d.shape.basic.Circle()
	//var d = new draw2d.shape.basic.Rectangle({width:50, height:100, x:100, y:100});
	  
    var inputLocator  = new draw2d.layout.locator.InputPortLocator();
    var outputLocator = new draw2d.layout.locator.OutputPortLocator();
    /*/
    d.createPort("hybrid",inputLocator);
    d.createPort("hybrid",inputLocator);
    d.createPort("hybrid",inputLocator);
    d.createPort("hybrid",inputLocator);
    d.createPort("hybrid",inputLocator);

    //d.createPort("hybrid",outputLocator);
    //d.createPort("hybrid",outputLocator);
    /**/
    var RIGHT_LOCATOR = new draw2d.layout.locator.RightLocator();
var LEFT_LOCATOR = new draw2d.layout.locator.LeftLocator();
var TOP_LOCATOR = new draw2d.layout.locator.TopLocator();
var BOTTOM_LOCATOR = new draw2d.layout.locator.BottomLocator();
var CENTER_LOCATOR = new draw2d.layout.locator.CenterLocator();
console.log(111);
var CUSTOM_LOCATOR = new e2e.component.Locator(e2e.component.LocatorType.BOTTOM_RIGHT);
console.log(222);
	d.createPort("hybrid", CUSTOM_LOCATOR);
	console.log(333);
	/*/
	d.createPort("hybrid", RIGHT_LOCATOR);
	d.createPort("hybrid", LEFT_LOCATOR);
	d.createPort("hybrid", TOP_LOCATOR);
	d.createPort("hybrid", BOTTOM_LOCATOR);
	/*/
    
    canvas.add(d, 100, 200);
	console.log(444);
    
    canvas.add(new draw2d.shape.basic.Label({text:"Add ports to the shape with a given locator", x:230, y:130}));

	  
	  
});

function displayJSON(canvas, isDisplay){
	/*/
	if(typeof isDisplay !== 'undefined') {
		console.log(canvas);
		return false;
	}
	/**/
    var writer = new draw2d.io.json.Writer();
    writer.marshal(canvas,function(json){
    	console.log(json);
        $("#json").text(JSON.stringify(json, null, 2));
    });
}
</script>
</head>
<body>
<div  onselectstart="javascript:/*IE8 hack*/return false" id="gfx_holder" style="width:3000px; height:3000px;">
</div>

<pre id="json" style="overflow:auto;position:absolute; top:10px; right:10px; width:350; height:500;background:white;border:1px solid gray">
</pre>
</body>
</html>