<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta name="format-detection" content="telephone=no">
<title>KT E2E Infra Orchestrator</title>

<link rel="shortcut icon" href="/resources/images/layout/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="/resources/css/style.css">
<script type="text/javascript" src="/resources/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="/resources/js/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="/resources/js/template.js"></script>


<script type="text/javascript">

	// 정렬클릭시
    function listOrder(colName,ordOption)
    {
    	var searchType = "";
    	var commaFlag  = "";
//     	orderKey+="(";
//   		 $('input:checkbox[name="checkNo"]').each(function() {
//   			searchType += commaFlag+this.value
//   			commaFlag=",";
// 		 });
//   		orderKey+=")";
    	//alert(orderKey);
    	console.log(colName);
    	console.log(ordOption);
        $.ajax({
            type: "get",
           // contentType: "json", //==> 생략가능(RestController이기때문에 가능)
            url: "/oneView/jsondata?colName="+colName
            		  +"&ordOption="+ordOption,
//             		  +"&orderKey="+orderKey,
            		  

            success: function(result){
                	 var dataObj = JSON.parse(result);
//                 	 console.log(dataObj);
                     var output = "";
                     for(var i in dataObj){

                    	 var myDate = new Date(dataObj[i].created_at)
                    	 var yyyy= myDate.getFullYear();
                    	 var mm=String(myDate.getMonth() + 1);
                    	 var dd=String(myDate.getDate());
                    	 var h24=String(myDate.getHours());
                    	 var mi=String(myDate.getMinutes());
                    	 var ss=String(myDate.getSeconds());
                    	 mm=(mm.length==1)?"0"+mm:mm;
                    	 dd=(dd.length==1)?"0"+dd:dd;
                    	 h24=(h24.length==1)?"0"+h24:h24;     
                    	 mi=(mi.length==1)?"0"+mi:mi;
                    	 ss=(ss.length==1)?"0"+ss:ss;
                    	 var date = yyyy  + "." +  mm+ "." + dd + " " + h24 + ":" + mi + ":" + ss;
						 var num = Number(i) + 1
						 
                         output += "<tr style=\"cursor:pointer;\"" + " onclick=\"location.href='/oneView/design/" + dataObj[i].nsd_id + "/btn_act/btn_act'\"" + ">";
                         output += "<td>" + num + "</td>";
                         output += "<td>" + dataObj[i].ns_type_nm +"</td>";
                         output += "<td>" + dataObj[i].nsd_name +"</td>";
                         output += "<td>" + dataObj[i].version +"</td>";
                         output += "<td>" + dataObj[i].nsd_id +"</td>";
                         output += "<td>On-boarded</td>";
                         output += "<td>" + dataObj[i].nsd_state + "</td>";
                         output += "<td>" + date +"</td>";                
//                          output += "<td>" + ((typeof(dataObj[i].regist_id)=="undefined")?"":dataObj[i].regist_id) +"</td>";
                         output += "<tr>";

                     }
                     output += "";

                $("#trData").html(output);
            }
        });
    }

    function list(page){
    	var f=document.form;
        location.href="/design/list/?curPage="+page+"&searchOption=${searchOption}"+"&keyword=${keyword}";
    }

	function fn_create() {
		$("#searchForm").attr({action:'/design/', method:'get'}).submit();
	}

</script>

</head>

<body>

<div id="wrap">
    
    <header>
		<h1 class="logo"><a href="#">KT E2E Infra Orchestrator</a></h1>
		<nav id="gnb">
			<ul>
				<li class="active"><a href="/design/list">DESIGN</a></li><!-- 활성화 class="active" -->
				<li><a href="/provisioning">PROVISIONING</a></li>
				<li><a href="#">MONITORING</a></li>
				<li><a href="#">INVENTORY</a></li>
				<li><a href="#">CONFIGURATION</a></li>
				<li>
					<a href="#">P-LTE</a>
					<ul>
						<li><a href="#">국데이터 관리</a></li>
						<li><a href="#">명령 실행</a></li>
						<li><a href="#">명령 개별 실행</a></li>
						<li><a href="#">고객사 관리</a></li>
						<li><a href="#">장비 관리</a></li>
						<li><a href="#">명령어 관리</a></li>
						<li><a href="#">모니터링</a></li>
						<li><a href="#">이력조회</a></li>
					</ul>
				</li>
			</ul>
		</nav><!-- //gnb -->
		<div class="ad_info">
			<i><img src="/resources/images/layout/img_ad_info.jpg" alt=""></i><!-- image size : 25*25px -->
			<p>admin</p>
		</div><!-- //ad_info -->
    </header>
    
    <div id="container">
		
		<ul id="navigation">
			<li><img src="/resources/images/layout/img_navigation_home.png" alt="home"></li>
			<li>DESIGN</li>
		</ul><!-- //navigation -->
		
		<form name="searchForm" id="searchForm" action="list" method="get">
		
		<div id="contents_wrap">
			<div class="contents">
			
				<div class="search_area">
					<div class="inner">
						<table>
							<caption>검색 테이블</caption>
							<colgroup>
								<col span="1" style="width:15%;">
								<col span="1" style="width:*;">
							</colgroup>
							<tbody>
								<tr>
									<td>
										<select name="searchOption">
											<option value="nsd_id" <c:if test='${searchOption=="nsd_id"}'> selected </c:if> >NSD ID</option>
											<option value="nsd_name" <c:if test='${searchOption=="nsd_name"}'> selected </c:if> >NSD 이름</option>
<%-- 											<option value="category" <c:if test='${searchOption=="category"}'> selected </c:if> >Category</option> --%>
										</select>
									</td>
									<td><input name="keyword" type="text" value="${keyword}"></td>
								</tr>
							</tbody>	
						</table>
						<div class="btn_area">
							<a href="#" onClick="searchData(); return fasle;"class="btn_l btn_co01 on">조회하기</a>
						</div>
					</div>
				</div><!-- //search_area -->
				
				<div class="table_ty">
					<div class="table_head">
						<div class="count">Total Count <span>${count}</span></div>
						<ul class="filter_output">
							<li><input type="checkbox" id="chk4G"   name="chk4G"><em class="cate">4G</em><a href="#"><strong class="num">${typeCount}</strong></a></li>
							<li><input type="checkbox" id="chk5G"   name="chk5G"><em class="cate">5G</em><a href="#"><strong class="num">${typeCount2}</strong></a></li>
							<li><input type="checkbox" id="chkPlte" name="chkPlte"><em class="cate">P-LTE</em><a href="#"><strong class="num">${typeCount3}</strong></a></li>
						</ul>
						 <div class="btn_area">
							<a href="#" onClick="fn_create(); return false;"class="btn_l btn_co01">NSD 생성</a>
						</div>
					</div>
					<table>
						<caption>목록 테이블</caption>
						<colgroup>
							<col span="1" style="width:5%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:8%;">
							<col span="1" style="width:25%;">
							<col span="1" style="width:10%;">
							<col span="2" style="width:15%;">
						</colgroup>
						<thead>
							<tr>
								<th scope="col">No.</th>
								<th scope="col">분류</th>
								<th scope="col">
									NSD 이름
									<div class="btn_align">
										<a href="#" onClick="javascript:listOrder('nsd_name','asc'); return false;"class="up">up</a>
										<a href="#" onClick="javascript:listOrder('nsd_name','desc'); return false;"class="down">down</a>
									</div>
								</th>
								<th scope="col">Version</th>
								<th scope="col">
									NSD ID
									<div class="btn_align">
										<a href="#" onClick="javascript:listOrder('nsd_id','asc'); return false;" class="up">up</a>
										<a href="#" onClick="javascript:listOrder('nsd_id','desc'); return false;" class="down">down</a>
									</div>
								</th>
								<th scope="col">Status</th>
								<th scope="col">
									On-boarded Time
									<div class="btn_align">
										<a href="#" class="up">up</a>
										<a href="#" class="down">down</a>
									</div>
								</th>
								<th scope="col">
									등록일자
									<div class="btn_align">
										<a href="#" onClick="javascript:listOrder('created_at','asc'); return false;"class="up">up</a>
										<a href="#" onClick="javascript:listOrder('created_at','desc'); return false;"class="down">down</a>
									</div>
								</th>
							</tr>
						</thead>
						<tbody id="trData">
							<c:forEach var="list" varStatus="status" items="${list}">
								<tr onclick="location.href='/design/${list.nsd_id}/btn_act/btn_act'" style="cursor:pointer;">
									<td>${status.count}</td>
									<td>${list.ns_type_nm}</td> <!-- 분류 -->
									<td>${list.nsd_name}</td> <!-- nsd name -->
									<td>${list.version}</td> <!-- version -->
									<td>
<%-- 										<a href="/oneView/design/${list.nsd_id}">${list.nsd_id}</a> <!-- nsd_id --> --%>
										${list.nsd_id}
									</td>
									<td>On-boarded</td> <!-- Status -->
									<td>${list.nsd_state}</td> <!-- On-Boarding -->
									<td><fmt:formatDate value="${list.created_at}" pattern="yyyy.MM.dd HH:mm:ss" /></td> <!-- 등록일자 -->
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div><!-- //table_ty -->
				
				<div class="paginate">
				    <!-- **처음페이지로 이동 : 현재 페이지가 1보다 크면  [처음]하이퍼링크를 화면에 출력-->
				    <c:if test="${commonPager.curBlock > 1}">
					<a href="#" onClick="list('1')" class="first">처음 페이지</a>
					</c:if>
				
					
					<!-- **이전페이지 블록으로 이동 : 현재 페이지 블럭이 1보다 크면 [이전]하이퍼링크를 화면에 출력 -->
				   <c:if test="${commonPager.curBlock > 1}">
					<a href="#" onClick="list('${commonPager.prevPage}')" class="prev">이전 페이지</a>
					</c:if>

					
					<ul>
						<!-- **하나의 블럭에서 반복문 수행 시작페이지부터 끝페이지까지 -->
		                <c:forEach var="num" begin="${commonPager.blockBegin}" end="${commonPager.blockEnd}">
		                    <!-- **현재페이지이면 하이퍼링크 제거 -->
		                    <c:choose>
		                        <c:when test="${num == commonPager.curPage}">
		                            <li class="select"><a href="#">${num}</a></li>
		                        </c:when>
		                        <c:otherwise>
		                            <li><a href="#" onClick="list('${num}')">${num}</a></li>
		                        </c:otherwise>
		                    </c:choose>
		                </c:forEach>					

					</ul>
					
					<!-- **다음페이지 블록으로 이동 : 현재 페이지 블럭이 전체 페이지 블럭보다 작거나 같으면 [다음]하이퍼링크를 화면에 출력 -->
	                <c:if test="${commonPager.curBlock < commonPager.totBlock}">
					<a href="#" onClick="list('${commonPager.nextPage}')" class="next">다음 페이지</a>
					</c:if>
					
					<!-- **끝페이지로 이동 : 현재 페이지가 전체 페이지보다 작거나 같으면 [끝]하이퍼링크를 화면에 출력 -->
	                <c:if test="${commonPager.curPage < commonPager.totPage}">
					<a href="#" onClick="list('${commonPager.totPage}')" class="last">마지막 페이지</a>
					</c:if>
				</div><!-- //paginate -->
				
			</div><!-- //contents -->
		</div><!-- //contents_wrap -->
		
		</form>
		
	</div><!-- //container -->
	
	<footer>
		<nav class="footer_nav">
			<ul>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">이용약관</a></li>
				<li><a href="#">개인정보처리방침</a></li>
				<li><a href="#">Sitemap</a></li>
			</ul>
		</nav><!-- //footer_nav -->
		<address>㈜케이티 대표이사 황창규 경기도 성남시 분당구 불정로 90 (정자동) 사업자등록번호 : 102-81-42945 통신판매업신고 : 2002-경기성남-0047</address>
	</footer>
	
</div><!-- //wrap -->

</body>
</html>
