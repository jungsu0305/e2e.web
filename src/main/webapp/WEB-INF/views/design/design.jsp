<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta name="format-detection" content="telephone=no">
<title>KT E2E Infra Orchestrator</title>

<link rel="shortcut icon" href="/resources/images/layout/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="/resources/css/style.css">
<script type="text/javascript" src="/resources/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="/resources/js/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="/resources/js/template.js"></script>
<script type="text/javascript" src="/scripts/utils/viewControlUtil.js"></script>

<script src="/resources/scripts/draw2d.v6/shifty.js"></script>
<script src="/resources/scripts/draw2d.v6/raphael.js"></script>
<script src="/resources/scripts/draw2d.v6/jquery.autoresize.js"></script>
<script src="/resources/scripts/draw2d.v6/jquery-touch_punch.js"></script>
<script src="/resources/scripts/draw2d.v6/jquery.contextmenu.js"></script>
<script src="/resources/scripts/draw2d.v6/rgbcolor.js"></script>
<script src="/resources/scripts/draw2d.v6/canvg.js"></script>
<script src="/resources/scripts/draw2d.v6/Class.js"></script>
<script src="/resources/scripts/draw2d.v6/json2.js"></script>
<script src="/resources/scripts/draw2d.v6/pathfinding-browser.min.js"></script>
<script src="/resources/scripts/draw2d.v6/draw2d.js"></script>

<script src="/resources/scripts/editor/Config.js"></script>
<script src="/resources/scripts/editor/Components.js"></script>
<script src="/resources/scripts/editor/Apps.js"></script>
<script src="/resources/scripts/application.js"></script>

<script src="/resources/scripts/utils/jsrender.js"></script>
<script id="component-template" type="text/x-jsrender">
<li class="component" data-id="{{:name}}" data-type="{{:type}}" data-path="{{:path}}"><a href="#"><i><img src="{{:path}}" alt=""></i>{{:name}}</a></li>
</script>

<script type="text/javascript">
var isTest = "${isTest}";
if(isTest) alert('TEST MODE');

var app;
var nsdType = '${type}';
var vUUID = '${uuid}';
var prevNsdId = '${prev_nsd_id}';
var nextNsdId = '${next_nsd_id}';
var lSlide = '${lSlide}';
var rSlide = '${rSlide}';

var isNew = nsdType === 'new'? true:false;
var componentTypes = conf.componentTypes;
var isTracking			= false;
var trackingTime		= 5000;
var trackingCount 	= 0;

var onBoarding 						= {};
	onBoarding.constSet 			= {};
	onBoarding.constSet.DEFAULT 	= -1;
	onBoarding.constSet.START 		= 0;
	onBoarding.constSet.RESULT	 	= 1;
	onBoarding.constSet.PROCESSING 	= 0;
	onBoarding.constSet.COMPLETED 	= 1;
	onBoarding.constSet.FAILED_TEMP = 2;
	onBoarding.constSet.FAILED 		= 3;
	onBoarding.constSet.ROLLING_BACK = 4;
	onBoarding.constSet.ROLLED_BACK = 5;
	onBoarding.data					= {};
	onBoarding.data.msgCode			= "";
	onBoarding.data.eventMsg		= "";
	onBoarding.data.startTime		= "";
	onBoarding.data.endTime			= "";
	onBoarding.data.duration		= "";
	onBoarding.data.columnList		= ["msgCode", "eventMsg", "startTime", "endTime", "duration"];


$(window).on('load', function() {

	
	//////////////////////////////////////////////////////////////////////////////////////
	// setup designer
	//////////////////////////////////////////////////////////////////////////////////////
	
	// initialize designer ///////////////////////////////////////////////////////////////
	app = new e2e.Application("canvas");
	var view = app.view;

	function getIconName(typeName) {
		if(typeName === 'csgn') return 'C_SGN.svg';
		if(typeName === 'gwu') return 'GW_U.svg';
		if(typeName === 'gwc') return 'GW_C.svg';
		if(typeName === 'epcc') return 'ePCC.svg';
		if(typeName === 'vgw') return 'vGW.svg';
		
		//exception for showing...
		if(typeName === 'hss_5g') return 'HSS_5G.svg';
		if(typeName === 'mme_5g') return 'MME_5G.svg';
		if(typeName === 'vgw_4g') return 'vGW_4G.svg';
		
		return typeName.toUpperCase() + '.svg';
	}

	// 좌측 아이콘 그리기 (4g, 5g, data1)
	app.loadingComponents = function() {
		
		$.get("/oneView/design/component/list/"+nsdType, function(dataFull) {

			var dataObj = JSON.parse(dataFull);

			for(var idx in dataObj) {
				
				var data = dataObj[idx];

				$.each(data, function(i,e) { 
					if(e.path == undefined && e.devType != undefined) {
						e.path = '/resources/images/contents/' + getIconName(e.devType) 
					}
// 					console.log('component:',e);
					app.components.push(e); 
				});
				
				if(idx == 1)      key = "vnf4g";
				else if(idx == 2) key = "pnf_components ul";
				else if(idx == 3) key = "conn_components ul";
				else if(idx == 4) key = "cloud_components ul";
				else              key = "vnf5g";
					
				//e.g) "#vnf5g"				
				$("#" + key).html($.templates("#component-template").render(data));
		
				//e.g) "vnf5g li"
				app.loadingComponent(key + " li");
				
			}
			bindComponentClickEvent();
		});
	}

	// Target Nfvo 조회
	app.lodingTargetInfo = function () {
		$.get("/oneView/design/targetList", function(dataFull) {
			var dataObj = JSON.parse(dataFull);

			var lth = dataObj.length;
			
			if(lth > 0) {

				var output = "";
				
				for(var i = 0; i < lth; i++) {
					output += "<option value=" + dataObj[i].nfvo_name +">" + dataObj[i].nfvo_id + "</option>";
				}
			}

			$("#targetNfvo").append(output);

			if(!isNew) {
				$("#targetNfvo").val(dataObj[0].nfvo_id).attr("selected", "selected");
			}
				
		});
		
	}

	// GeneralInfo 조회
	app.lodingGeneralInfo = function (nsdType) {

		$.get("/oneView/design/nsd/"+nsdType, function(dataFull) {
			event.preventDefault();

			$("#tbodyNsdf").empty();
			$("#tbodyVnf").empty();

			var dataObj = JSON.parse(dataFull);
// console.log(dataObj);
			var profId = "";
			
			if( dataObj[0].prof_id != undefined ){
				profId = JSON.parse(dataObj[0].prof_id.value); // object로 변환	
			} else {
				profId = "";	
			}

			// General Info 구간
			if( dataObj[0].ns_type != undefined ) {
				$("#nsType").val(dataObj[0].ns_type).attr("selected", "selected");
			}
			if( dataObj[0].nsd_name != undefined ) {
				$("[name='nsdName']").eq(0).val(dataObj[0].nsd_name);
			}
			if( dataObj[0].version != undefined ) {
				$("[name='version']").eq(0).val(dataObj[0].version);
			}
			if( dataObj[0].nsd_id != undefined ) {
				$("[name='nsdId']").eq(0).val(dataObj[0].nsd_id);
			}
			if( dataObj[0].target_nfvo != undefined ) {
				$("[name='targetNfvo']").eq(0).val(dataObj[0].target_nfvo).attr("selected", "selected");
			}
			if( dataObj[0].nsd_desc != undefined ) {
				$("[name='nsdDesc']").eq(0).val(dataObj[0].nsd_desc);
			}

			if(dataObj[0].ns_type == 2) { // P-LTE 여부
				$("#titleTab2").hide();
			 	$("#titleTab3").hide();
			} else {
				$("#titleTab2").show();
			 	$("#titleTab3").show();
			 	
			 	// NS Deployment Flavor 만 있는 경우
			 	if( profId == "") {
			 		
			 		if( dataObj[0].vnfd_id != undefined ) {
			 		
						var nsdf_id   = dataObj[0].nsdf_id;
						var nsdf_name = dataObj[0].nsdf_name;
	
						output += "<tr name=\"trNsdf\">";
						output += "<td><input type=\"checkbox\" name=\"chkNsdf\"></td>";
	//						output += "<td><input type=\"text\" class=\"ta_c\" name=\"nsdfName\" value=" + nsdf_name + "></td>";
						output += "<td><input type=\"text\" class=\"ta_c\" name=\"nsdfName\" onChange=\"fn_nsdfChange(this,0)\"></td>";
						output += "<td>";
						output += nsdf_id;
						output += "<input type=\"hidden\" value=" + nsdf_id + " class=\"ta_c\" name=\"nsdfId\">";
						output += "</td>";
						output += "</tr>";
		
						$("#tbodyNsdf").append(output);
						
						$("[name='nsdfName']").eq(0).val(nsdf_name);
			 		}
			 	} else {
			 	
			 	// VNF Deployment Flavor 도 있는 경우
			 	var vnfLth = profId.nsDf.length;
			 	var output = "";
				var convertor = app.convertor;
				var writer = new draw2d.io.json.Writer();
				
			 	for(var i = 0; i < vnfLth; i++) {
			 		
			 		
			 		if( i == 0 ) {
			 			// NSDF 구간
						var nsdf_id   = profId.nsDf[0].nsdfId;
						var nsdf_name = profId.nsDf[0].nsdfName;

						output += "<tr name=\"trNsdf\">";
						output += "<td><input type=\"checkbox\" name=\"chkNsdf\"></td>";
// 						output += "<td><input type=\"text\" class=\"ta_c\" name=\"nsdfName\" value=" + nsdf_name + "></td>";
						output += "<td><input type=\"text\" class=\"ta_c\" name=\"nsdfName\" onChange=\"fn_nsdfChange(this," + i + ")\"></td>";
						output += "<td>";
						output += nsdf_id;
						output += "<input type=\"hidden\" value=" + nsdf_id + " class=\"ta_c\" name=\"nsdfId\" readonly>";
						output += "</td>";
						output += "</tr>";
		
						$("#tbodyNsdf").append(output);
						
						$("[name='nsdfName']").eq(0).val(nsdf_name);
						
						var output2 = "";
						writer.marshal(app.view, function(json) {

							var jvNode = convertor.jsonNode(json);
// 							console.log("!!!!!!!");
// 							console.log(jvNode);
console.log(profId.nsDf[0]);
                                // VNF 구간
								output2 += "<tr name=\"trVnf\">";
						 		output2 += "<td name=\"tdvnfdNsdfId\">";
								output2 += profId.nsDf[0].nsdfName;			 		
						 		output2 += "</td>";
						 		
// 						 		output2 += "<td><input type=\"text\" class=\"ta_c\" name=\"vnfNsdfName\" value=" + profId.nsDf[0].vnfProfile[0].vnfdId + "></td>";
								
						 		output2 += "<td>";
						 		output2 += profId.nsDf[0].vnfProfile[0].vnfProfileName;
						 		output2 += "<input type=\"hidden\" class=\"ta_c\" name=\"vnfNsdfName\" value=" + profId.nsDf[0].vnfProfile[0].vnfProfileName + " readonly>";
						 		output2 +="</td>";
						 		
						 		output2 += "<td>";
						 		output2 += profId.nsDf[0].vnfdName;
						 		output2 += "<input type=\"hidden\" class=\"ta_c\" name=\"vnfdName\" value=" + profId.nsDf[0].vnfdName + " readonly>";
						 		output2 += "<input type=\"hidden\" class=\"ta_c\" name=\"vnfdId\" readonly value=" + profId.nsDf[0].vnfProfile[0].vnfdId + ">";
						 		output2 += "</td>";
						 		output2 += "<td>";
						 		output2 += profId.nsDf[0].vnfProfile[0].vnfProfileId;
						 		output2 += "<input type=\"hidden\" class=\"ta_c\" name=\"vnfProfileId\" value=" + profId.nsDf[0].vnfProfile[0].vnfProfileId + " readonly>";
						 		output2 += "</td>";
						 		
						 		output2 += "<td>";
						 		output2 += "<input type=\"text\" class=\"ta_c\" name=\"vnfFlavourId\" value=" +  profId.nsDf[0].vnfProfile[0].flavourId + ">"
						 		output2 += "<input type=\"hidden\" name=\"delNsdf\" value=" + profId.nsDf[0].nsdfName + ">";
						 		output2 += "</td>";
						 		output2 += "<td><input type=\"text\" class=\"ta_c\" name=\"minNumberOfInstances\" value=" +  profId.nsDf[0].vnfProfile[0].minNumberOfInstances + " onkeydown=\"onlyNumber(this);\" ></td>";
						 		output2 += "<td><input type=\"text\" class=\"ta_c\" name=\"maxNumberOfInstances\" value=" +  profId.nsDf[0].vnfProfile[0].maxNumberOfInstances + " onkeydown=\"onlyNumber(this);\" ></td>";
						 		output2 += "<td><input type=\"text\" class=\"ta_c\" name=\"dependencies\" value=" + profId.nsDf[0].dependencie + " onkeydown=\"onlyNumber(this);\" ></td>";
						 		output2 += "<input type=\"hidden\" class=\"ta_c\" name=\"nsdf\" value=" + profId.nsDf[0].nsdfName + " readonly>";
						 		output2 += "<input type=\"hidden\" class=\"ta_c\" name=\"vnfdNsdfId\" value=" + profId.nsDf[0].nsdfId + " readonly>";
						 		output2 += "</tr>";

						 		$("#tbodyVnf").append(output2);

						});
					} else {
						var output2 = "";
						writer.marshal(app.view, function(json) {

							var jvNode = convertor.jsonNode(json);

								output2 += "<tr name=\"trVnf\">";
						 		output2 += "<td name=\"tdvnfdNsdfId\">";
						 		output2 += profId.nsDf[i].nsdfName;
						 		output2 += "</td>";

						 		output2 += "<td>";
						 		output2 += profId.nsDf[i].vnfProfile[0].vnfProfileName;
						 		output2 += "<input type=\"hidden\" class=\"ta_c\" name=\"vnfNsdfName\" value=" + profId.nsDf[i].vnfProfile[0].vnfProfileName + " readonly>";
						 		output2 += "</td>";
						 		
						 		output2 += "<td>";
						 		output2 += profId.nsDf[i].vnfdName;
						 		output2 += "<input type=\"hidden\" class=\"ta_c\" name=\"vnfdName\" value=" + profId.nsDf[i].vnfdName + " readonly>";
						 		output2 += "<input type=\"hidden\" class=\"ta_c\" name=\"vnfdId\" readonly value=" + profId.nsDf[i].vnfProfile[0].vnfdId + ">";
						 		output2 += "</td>";
						 		output2 += "<td>"
						 		output2 += profId.nsDf[i].vnfProfile[0].vnfProfileId;
						 		output2 += "<input type=\"hidden\" class=\"ta_c\" name=\"vnfProfileId\" value=" + profId.nsDf[i].vnfProfile[0].vnfProfileId + " readonly>";
						 		output2 += "</td>";
						 		
						 		output2 += "<td>";
						 		output2 += "<input type=\"hidden\" name=\"delNsdf\" value=" + profId.nsDf[i].nsdfName + ">";
						 		output2 += "<input type=\"text\" class=\"ta_c\" name=\"vnfFlavourId\" value=" +  profId.nsDf[i].vnfProfile[0].flavourId + ">"

						 		output2 += "</td>";
						 		output2 += "<td><input type=\"text\" class=\"ta_c\" name=\"minNumberOfInstances\" value=" +  profId.nsDf[i].vnfProfile[0].minNumberOfInstances + " onkeydown=\"onlyNumber(this);\"></td>";
						 		output2 += "<td><input type=\"text\" class=\"ta_c\" name=\"maxNumberOfInstances\" value=" +  profId.nsDf[i].vnfProfile[0].maxNumberOfInstances + " onkeydown=\"onlyNumber(this);\"></td>";
						 		output2 += "<td><input type=\"text\" class=\"ta_c\" name=\"dependencies\" value=" + profId.nsDf[i].dependencie + " onkeydown=\"onlyNumber(this);\"></td>";
						 		output2 += "<input type=\"hidden\" class=\"ta_c\" name=\"nsdf\" value=" + profId.nsDf[i].nsdfName + " readonly>";
						 		output2 += "<input type=\"hidden\" class=\"ta_c\" name=\"vnfdNsdfId\" value=" + profId.nsDf[i].nsdfId + " readonly>";
						 		output2 += "</tr>";

						 		$("#tbodyVnf").append(output2);

						});
					}

			 		}
			 	}
			}

		});
		
	}

	// adding left,right arrow button  //////////////////////////////////////////////////
	app.setArrowButton = function() {//alert(isNew);
	
		if(!isNew) {
			
			var btn, x, y;
			var opt = 23;  // half height of icon

			if(! isEmpty(prevNsdId)) {
				$("#prevLi").show();
				$("#prevLi").data("link", "/oneView/design/" + prevNsdId);
//  				btn = app.getArrowButton("prev", "/oneView/design/" + prevNsdId);
//  				x = 100 - opt;
//  				y = (view.getHeight()/2) - 35;
//  				view.add(btn, x, y);
//  				app.prevBtn = btn;
			} else {
				$("#prevLi").hide();
			}
			if(! isEmpty(nextNsdId) ) {
				$("#nextLi").data("link", "/oneView/design/" + nextNsdId);
// 				btn = app.getArrowButton("next", "/oneView/design/" +nextNsdId);
// 				x = view.getWidth() - (100 + opt);
// 				y = (view.getHeight()/2) - 35;
// 				view.add(btn, x, y);
// 				app.nextBtn = btn;
				$("#nextLi").show();
			} else {
				$("#nextLi").hide();
			}
			
			
			
// 			view.add(btn, x, y);
			
// 			if(nsdType == "4g") {
// 				app.prevBtn = btn;
// 			} else if (nsdType == "5g") {
// 				app.nextBtn = btn;
// 			}
			
			//console.log('btn(',x,y,')');

		} else {
// 			var prev_btn = app.getArrowButton("prev", "/oneView/4g");
// 			view.add(prev_btn, 100 - opt, (view.getHeight()/2) - opt);
// 			app.prevBtn = prev_btn;
		}	
	}
	
	//setting connection style ////////////////////////////////////////////////////////////
	app.lazyLoading = function() {
		setTimeout(function() {
			view.installEditPolicy(new draw2d.policy.canvas.CoronaDecorationPolicy());
			console.info('loading policy for port disappear');
		},200);
	}
	
	app.setSize = function(json) {
		
		//check node max
		if(typeof json === "string") {
			json = JSON.parse(json);
		}
		//json = JSON.parse(json);
		var max = {x:0,y:0};
		
		for(var i in json) {
			var e = json[i];
			if(typeof e.x === 'number' && typeof e.width === 'number') {
				if(max.x === 0 || max.x < (e.x + e.width))  max.x = e.x + e.width;				
				if(max.y === 0 || max.y < (e.y + e.height)) max.y = e.y + e.height;
				
			} else if(typeof e.x === 'number') {
				if(max.x === 0 || max.x < (e.x + 50)) max.x = e.x + 50;//offset, 50 1st				
				if(max.y === 0 || max.y < (e.y + 50)) max.y = e.y + 50;
			}
		}
		
		//current window
		var w = view.width();
		var h = view.height();
		
		utils.canvas.set(max, {w:w,h:h});
		
	}
	
	/*
	$(window).scroll(function(event) { 
		didScroll = true;
		
		var w = view.width();
		var h = view.height();
		console.log(112282);
		//app.resizeCanvas(w,h);
	});
	*/

	// 저장된 토폴로지 조회
	app.loadData = function(dataKey, isRestore) {
		var reader = app.reader;
		
		//for restore after destroy
		if(typeof isRestore !== 'undefined' && isRestore) {
			reader.unmarshal(view, app.data);
			return false;
		}
		
		$.get('/oneView/design/data/' + dataKey, function(ret) {
 			var rett = JSON.parse(ret);
			if(rett.err == 0) {
				app.data = rett.data;
				app.setSize(app.data);
				reader.unmarshal(view, app.data);
				app.lodingGeneralInfo(nsdType);
			}
		});
	}
	
	app.reloadCanvas = function(dataKey) {
		view.clear();
		app.setArrowButton();
		app.loadData(dataKey);
		app.lazyLoading();//for default display disable
	};
	
	// 저장 버튼 클릭시
	app.saveData = function() {
		var convertor = app.convertor;
		var store = {};
		var store2 = {};
		var writer = new draw2d.io.json.Writer();

		if (confirm("저장 하시겠습니까?")) {

			writer.marshal(view, function(json) {
				var jv = convertor.json(json);
				var jvNode = convertor.jsonNode(json);
	
				$("[name='nsdfIdLnf']").eq(0).val($("[name='trNsdf']").length);
				$("[name='vnfIdLnf']").eq(0).val($("[name='trVnf']").length);
				$("[name='vlLnf']").eq(0).val($("[name='trVl']").length);
	
				var infoData = $("#oneView").serializeObject(); 
				
				/////////////////////////////////////////////////////////////
				
				store = JSON.stringify(jv);
				store2 = JSON.stringify(infoData);
	// 			console.log(store);
				console.log(store2);
// 	return;
				var url;
	
				if(isNew) {
					url = '/design/save/' + $("[name='nsdId']").eq(0).val();
				} else {
					url = '/design/save/' + nsdType;				
				}
				
				//POST or LOGGING for DEV
				if(isTest) {
					alert('isTest::saved');
					console.log(store);
					return false;
				}
				
				var lSlide = "";
				var rSlide = "";
				
				if( $("#l_btn").attr('class') != "btn_act" ) {
					lSlide = "on"
				} else {
					lSlide = "btn_act"
				}

				if( $("#r_btn").attr('class') != "btn_act" ) {
					rSlide = "on"
				} else {
					rSlide = "btn_act"
				}

				$.post(url, {isNew:isNew, data:store, data1:store2}, function(ret) {
	// 				alert('saved:' + ret);
					if(ret == "success") {
						alert("저장 되었습니다.");
// 						location.href = "/design/" + $("[name='nsdId']").eq(0).val() + "/" + lSlide + "/" + rSlide;
					} else {
						alert("중복된 NSD ID가 존재합니다.");	
					}
				});
			});
		}
	}
	
	app.repositionButtons = function() {
		//btn repositioning
		if(app.prevBtn != null) view.remove(app.prevBtn);
		if(app.nextBtn != null) view.remove(app.nextBtn);
		app.setArrowButton();
	}
	
	app.zoom = function(v) {
		//app.resize(view.width()*v, view.height()*v);
		if(v === 1.0) this.view.setZoom(1.0, true);
		else          this.view.setZoom(this.view.getZoom()*v, true);
	},
	
	//this is dependent on html
	app.find = function(key,level) {
		var levels = ['','_normal','_minor','_major','_warning','_critical'];	
		
		//find node
		var figures = app.view.figures;
		var node = figures.data.filter(function(e) {
			//TODO found with name 1st
			return e.NAME === 'e2e.component.Node'
				&& typeof e.name !== 'undefined' 
				&& e.name === key;			
		});
		
		if(node.length === 0) {
			alert("couldn't found your icon in canvas");			
			return;
		}
		
		//TODO 1st node only...
		var subfigures = node[0].getChildren();
		var icon = subfigures.data.filter(function(e) {
			return e.NAME === 'e2e.component.Icon' && typeof e.path !== 'undefined';
		});
		//console.log("★★★ : " + key);
		//set path
		var newPath = '/images/contents/' + key + levels[level] + '.svg';
		node[0].path = newPath;
		icon[0].setPath(newPath);
	},
	
	
	// 버튼 조절에 따른 Canvas width 조절
	app.resize = function() {

		// 양옆 : 1884 오른쪽 : 1645 기본 : 895
		
		var cLeft  = $("#canvasLeft").width();
		var cRight = $("#canvasRight").width();
		var cWidth = $("#canvas").width();
		var width = "";
		
		if($("#l_btn").attr('class') == "btn_act on" && $("#r_btn").attr('class') == "btn_act on") {
			width = Number(cLeft) + Number(cRight) + Number(cWidth);
		} else if($("#r_btn").attr('class') == "btn_act on") {
			width = Number(cRight) + Number(cWidth);
		} else {
			width = Number(cWidth);
		}
		
		$('svg').attr("width", width + "px");
		view.resize(width);
		app.repositionButtons();

	};
	
	// 우측 상단 버튼 컨트롤 ( 현재는 저장 버튼만 활성화 )
	app.buttonControl = function() {
// 		id="btnSave"  class="btn_l btn_co01"
// 		id="btnBoard" class="btn_l btn_co01"
// 		id="btnAble"  class="btn_l btn_co01"
// 		id="btndel"   class="btn_l btn_co01"
// 최초 save
// save후 save onboard delete
// onboard enable 후 disable
// onboard disable 후 enable delete

// onClick="fn_onBoard();
// onClick="fn_Enable();
// onClick="fn_delete();

			$("#btnSave").attr("class",	"btn_l btn_co01");
			$("#btnBoard").attr("class", "btn_l btn_co02");
			$("#btnAble").attr("class",	"btn_l btn_co02");
			$("#btndel").attr("class",	"btn_l btn_co01");


// 		if(isNew == true) { // 새로작성
// 			$("#btnSave").attr("class",	"btn_l btn_co01");
// 			$("#btnBoard").attr("class", "btn_l btn_co02");
// 			$("#btnAble").attr("class",	"btn_l btn_co02");
// 			$("#btndel").attr("class",	"btn_l btn_co02");
			
			$("#btnBoard").removeAttr("onclick");
			$("#btnAble").removeAttr("onclick");
// 			$("#btndel").removeAttr("onclick");
// 		} else { // save 후
// 			$("#btnSave").attr("class",	"btn_l btn_co01");
// 			$("#btnBoard").attr("class", "btn_l btn_co01");
// 			$("#btnAble").attr("class",	"btn_l btn_co02");
// 			$("#btndel").attr("class",	"btn_l btn_co01");
// 		}
	}
	
	$("#l_btn").click(function() {
		app.resize();
	});
	
	$("#r_btn").click(function() {
		app.resize();
	});
	
	////////////////////////////////////////////////////////////////
	//
	// Application Loading
	//
	////////////////////////////////////////////////////////////////
// 	if(nsdType != 'test') {
	 	
// 	}

// 	 app.setArrowButton();
	 app.loadingComponents();

	if(!isNew) {
		app.loadData(nsdType);
		app.resize();
		
		
	} else {
		
// 		app.lodingTargetInfo();
// 		app.buttonControl(); // 버튼컨트롤
	}
	app.lodingTargetInfo();
	control();
	app.setArrowButton();
	app.lazyLoading();
	app.buttonControl();
	
	/////////////////////////////////////////////////////
	// utils
	/////////////////////
	// 팝업 Config
	utils.vnfConfig = {};
	utils.vnfConfig.load = function(node) {
// 		console.log(node);
//alert(utils.getStorage());
		var networkService = $("#"+utils.getStorage()).data('networkService');
		var componentList  = $("#"+utils.getStorage()).data('componentList');
		var basekey  = "." + utils.getEditableContents();//conf.contents.pclass;
		//console.log(componentList);
		componentList.some(function(e) {
			//TODO, 1st 'name'
			if(e.serverType === node.name) {
				//console.log(e);
				$("#"+utils.getStorage()).data('current-active',e); //store into web
				
				$(basekey + " .vnfConfig dl dd").html(e.serverType);		//serverType
				$(basekey + " .vnfConfig ul li:first input").val(e.name);	//name //TODO temporary
				$(basekey + " .vnfConfig ul li:nth-child(2) input").val(e.vnfpkg); //VNF Package
				$(basekey + " .vnfConfig ul li:nth-child(3) input").val(e.oapip);//OAP IP
				$(basekey + " .vnfConfig ul li:last select").val(e.scale); //scale option!
				
				if(typeof e.apn !== 'undefined') $(".sliceConfig ul li:first input").val(e.apn);
				if(typeof e.ippool !== 'undefined') $(".sliceConfig ul li:last input").val(e.ippool);
				
				$(basekey).dialog('open');
				return true;
			}
		});
	}
	
	utils.vnfConfig.update = function() {
		
		var basekey = "." + utils.getEditableContents();
		
		var currns= $("." + utils.getStaticContents()).data('networkService');
		var curr  = $("#" + utils.getStorage()).data('current-active');
		var next = {
			name :			$(basekey + " .vnfConfig ul li:first input").val(),
			vnfpkg :		$(basekey + " .vnfConfig ul li:nth-child(2) input").val(), 
			oapip :			$(basekey + " .vnfConfig ul li:nth-child(3) input").val(), 
			scale :			$(basekey + " .vnfConfig ul li:last select option:selected").val(),
		}
		
		var nextns = {
			apn: $(".sliceConfig ul li:first input").val(),
			ippool:$(".sliceConfig ul li:last input").val(),
		}
		
		var isChanged = false;
		if(curr.name !== next.name) { 
			curr.name = next.name; isChanged = true;
		}
		if(curr.vnfpkg !== next.vnfpkg) {
			curr.vnfpkg = next.vnfpkg; isChanged = true;
		}
		if(curr.oapip !== next.oapip) {
			curr.oapip = next.oapip; isChanged = true;
		}
		if(curr.scale !== next.scale) {
			curr.scale = next.scale; isChanged = true;
		}
		if(currns.apn !== nextns.apn) {
			curr.apn = nextns.apn; isChanged = true;
		}
		if(currns.ippool !== nextns.ippool) {
			curr.ippool = nextns.ippool; isChanged = true;
		}

		if(isChanged) {
			
			var componentList = $("#"+utils.getStorage()).data('componentList');
			console.log('beforeComponentList:',componentList);
			for(var i in componentList) {
				if(componentList[i].serverType == curr.serverType) {
					console.log('found index:',i);
					componentList[i] = curr;					
				}
			}
			console.log('afterComponentList:',componentList);
		}
	}
		
	///////////////////////////////////////////////////////////////////////////////////
	//
	// function
	//
	///////////////////////////////////////////////////////////////////////////////////

	// set staticContents + ////////////////////////////////////////////
	utils.networkService = {
		title:			'New Design',
		name:			this.title,
		desc:			'Network Service',
		version:		'0.1',
		apn:			{},
		ippool:			'127.0.0.1'
	};
	
	utils.setNetworkService = function(e) {
		$(".staticContents").data('networkService',e);//store into web
		
		var ns = this.networkService;
		if(e !== null) ns = $.extend(ns,e);
		
		$(".staticContents .pop_tit").html(ns.title);//in NS Name
		$(".staticContents .pop_con ul li:first span").html(ns.name);//in Name
		$(".staticContents .pop_con ul li:nth-child(2) span").html(ns.version);
		$(".staticContents .pop_con ul li:nth-child(3) span").html(ns.desc);
		
		//sliceConfig :: (title), APN, IP-POOL
		$(".sliceConfig dl dd").html(ns.title);
		$(".sliceConfig ul li:first input").val(ns.apn);
		$(".sliceConfig ul li:last input").val(ns.ippool);
		
		$("#"+utils.getStorage()).data('networkService', ns);
	}
	
	//TODO NOT DEFAULT, 1st temporary values
	//$.get("/<URL>", function(data) {
	$.getJSON("/data/cloudListSample.json", function(data) { // 우측 데이터 및 팝업시 하단 데이터
		//console.log(data);
	
		if(!isNew) {
			$.each(data, function(i, e) {
				if(e.nsdType === nsdType) {
					utils.setNetworkService(e);
				}
			});
		}
	});
	
// 	utils.draggable = function(component) { // 드래그후 드래그된 아이콘의 아이디
// 		console.log(component.id);
// 		// 드래그 후 등록시 마다 vnfd_id 추가
// 		$("#td_vnfd_id").append("<input type=\"text\" name=\"vnfdId\" value=" + component.id + " />");

// 	}
	
	utils.add = function(figure) {
// 		console.log(figure);	
	}
	
	utils.remove = function(figure) { // 노드 삭제
// 		console.log(figure);
		
		if($("[name='vnfdId']").length > 0) { // 노드 삭제시 vnfd_id도 삭제
			for(var i = 0; i < $("[name='vnfdId']").length; i++) {
				if(figure.id == $("[name='vnfdId']").eq(i).val()) {
					$("[name='vnfdId']").eq(i).remove();
				}
			}
		}
		
	}
	
	
	////////////////////////////////////////////////////////////////
	//
	// Window event
	//
	////////////////////////////////////////////////////////////////
	
	
	//check window size /////////////////////////////
	var rtime;
	var timeout = false;
	var delta = 200;//sample
	function resizeend() {
		if(new Date() - rtime < delta) {
			setTimeout(resizeend, delta);
		} else {
			timeout = false;
			view.resize();
			app.repositionButtons();
		}
	}
	
	$(window).resize(function() {
		rtime = new Date();
		if(timeout === false) {
			timeout = true;
			setTimeout(resizeend, delta);
		}
	})
	
	var scrolling = false;
	$(window).scroll(function(event) {
		console.log('scrolling...::' + scrolling);
		if(scrolling === false) {
			scrolling = true;	
			console.log('scrolling starting...::' + scrolling)
			setTimeout(function() {
				
				var w = view.width();
				var h = view.height();
				console.log('width='+w+', height='+h);
				
				view.resize();
				app.repositionButtons();
				scrolling = false;
				console.log('scrolling ending::'+scrolling);
			},500);
		}
	});

	
	// 좌측 클릭 이벤트
	$(".lnb_m").on('click', function() {
		var type = this.dataset.id;
		
		//function DISABLE
// 		if( type === 'conn') return false;
		
		$.each(componentTypes, function(idx, etype) {
			if(etype == "vnf5g" || etype =="vnf4g") { // vnf 경우 5g 와 4g가 존재
				etype = "vnf";	
			}
			$("#"+etype+"_components").css('display','none');
		});
		$("#"+type+"_components").css('display','block');
	});
	
	////////////////////////////////////////////////////////////////////////
	//
	// POPUP DIALOG END
	//
	////////////////////////////////////////////////////////////////////////
	
	
	
	/// FOR DEV OR TEST ////////////////////////////////////////////////////
	var componentClickCnt = [];
	bindComponentClickEvent = function() {
		
		//TODO 1st key is 'name' !!!!
		$(".component").on('click',function() {
			if(!isTest) return;
			
			var id = $(this).data('id');//TODO key!
			if(typeof componentClickCnt[id] === 'undefined') {
				componentClickCnt[id] = 0;
			}
			
			if(componentClickCnt[id] === 5) componentClickCnt[id] = -1;
			
			componentClickCnt[id]++;
			//TODO TEST
			var level = [
				app.level.Default,
				app.level.Normal,
				app.level.Minor,
				app.level.Major,
				app.level.Warning,
				app.level.Critical,
			][componentClickCnt[id]];
			alert($(this).data('id'));
			app.findIcon($(this).data('id'), componentClickCnt[id]);		
		});
	}
	
	var titleClickCnt  = 0;
	var titleClickCntLimit = 5;
	
	if(isTest) titleClickCntLimit = 1;
	$(".title").on('click', function(){

		titleClickCnt++;
		if(titleClickCnt === titleClickCntLimit) {
			titleClickCnt = 0;
			popupTestDialog();
			//popupFigures();
		}
	});

	/////////////////////////////////////////////////////////////////////////

	   $.fn.serializeObject = function()

	   {

	      var o = {};

	      var a = this.serializeArray();

	      $.each(a, function() {

	          if (o[this.name]) {

	              if (!o[this.name].push) {

	                  o[this.name] = [o[this.name]];

	              }

	              o[this.name].push(this.value || '');

	          } else {

	              o[this.name] = this.value || '';

	          }

	      });

	      return o;

	   };
	
	
	});

	// nsdf 추가
	function fn_addNsdf() {
		
		var output = "";
		var numFocus = $("[name='trNsdf']").length;
		var trVnf = $("[name='trVnf']").length;
		var uuid = generateUUID();
		
		output += "<tr name=\"trNsdf\">"
		output += "<td>";
		output += "<input type=\"checkbox\" name=\"chkNsdf\">";
		output += "</td>";
		output += "<td>";
		output += "<input type=\"text\" class=\"ta_c\" name=\"nsdfName\" onChange=\"fn_nsdfChange(this," + numFocus + ")\">";
		output += "</td>";
		output += "<td>";
		output += uuid;
		output += "<input type=\"hidden\" value=" + uuid + " class=\"ta_c\" name=\"nsdfId\" readonly>";
		output += "</td>";
		output += "</tr>";

		$("#tbodyNsdf").append(output);
		$("[name='nsdfName']").eq(numFocus).focus();

		fn_addVnf(numFocus);
		
	}
	
	// nsdf 삭제
	function fn_delNsdf() {

		// 체크박스 선택 유무 선택행 삭제
		var length = $("[name='chkNsdf']").length;
		var nsdfName = "";
		var vnfLth = "";
		var nsdfLth = "";
		var nsdfId = "";

﻿         if ($("input[name='chkNsdf']").is(":checked")) { 
	﻿            if (confirm("삭제 하시겠습니까?")) {
				
					var convertor = app.convertor;
					var writer = new draw2d.io.json.Writer();
						
					
					writer.marshal(app.view, function(json) {
						var jvNode = convertor.jsonNode(json);
						vnfLth = jvNode.length;
					});

	﻿                for(var i=$("[name='chkNsdf']:checked").length-1; i>-1; i--) {
 					    nsdfName = $("[name='nsdfName']").eq(i).val();
 					    nsdfId = $("[name='nsdfId']").eq(i).val();
						nsdfLth = i;
						
// 						for(var j = $("[name='trVnf']").length-1; j > -1; j--) {
// 							if(nsdfId == $("[name='vnfdNsdfId']").eq(j).val()) {
// 								alert($("[name='vnfdNsdfId']").eq(j).val());
// 								$("[name='trVnf']").eq(j).closest("tr").remove(); // VNF Profile 삭제
// 							}
// 						}
	     					
     					$("[name='chkNsdf']:checked").eq(i).closest("tr").remove(); // NSDF 삭제
	                }
	
					for(var j = $("[name='trVnf']").length-1; j > -1; j --) {
// 						alert(nsdfId);
// 						alert($("[name='vnfdNsdfId']").eq(j).val());
						if(nsdfId == $("[name='vnfdNsdfId']").eq(j).val()) {
							$("[name='trVnf']").eq(j).closest("tr").remove(); // VNF Profile 삭제
						}
					}
					
					if($("input[name='checkALL']").is(":checked")) {
						$("input[name='checkALL']").attr("checked", false);
					}
	            }﻿
	         } else { 
	﻿            alert("삭제할 NSDF를 선택하세요.");  
	         }﻿
	}
	
	
	fn_addVnf = function(num) {

		var convertor = app.convertor;
		var store = {};
		var writer = new draw2d.io.json.Writer();
		var trNsdf = $("[name='trNsdf']").length;
		var nsdfId = $("[name='nsdfId']").eq(num).val();

		if(trNsdf == 0) {
			alert("먼저 NSDF를 추가하세요.");
			return false;
		}
		//alert(num);
		writer.marshal(app.view, function(json) {

			var jvNode = convertor.jsonNode(json);
			console.log("#####");
			console.log(jvNode);
			
			var nodeLth = jvNode.length;
			var output = "";
			var nsdfName = "";
// 			var nsdfId = "";
			var key = "";
			

			for(var i = 0; i < jvNode.length; i++) {
				var uuid = generateUUID();
				var vnfdName = "";
				var vnfdId = "";
				
				if( isEmpty(jvNode[i].vnfdName) ) {
					vnfdName = "";
				} else {
					vnfdName = jvNode[i].vnfdName;
				}
				
				if( isEmpty(jvNode[i].vnfdId) ) {
					vnfdId = "";
				} else {
					vnfdId = jvNode[i].vnfdId;
				}
				
		 		output += "<tr name=\"trVnf\">";
		 		output += "<td name=\"tdvnfdNsdfId\">";
		 		output += "</td>";
		 		
		 		output += "<td>";
		 		output += jvNode[i].name;
		 		output += "<input type=\"hidden\" class=\"ta_c\" name=\"vnfNsdfName\" readonly value=" + jvNode[i].name + ">";
		 		output += "</td>";

		 		output += "<td>";
		 		output += jvNode[i].name;
		 		if(vnfdName == "") {
		 			output += "<input type=\"hidden\" class=\"ta_c\" name=\"vnfdName\" readonly>";	
		 		} else {
		 			output += "<input type=\"hidden\" class=\"ta_c\" name=\"vnfdName\" value='" + vnfdName.toUpperCase() + "' readonly>";
		 		}
		 		
		 		if(vnfdId == "") {
		 			output += "<input type=\"hidden\" class=\"ta_c\" name=\"vnfdId\" readonly>";
		 		} else {
		 			output += "<input type=\"hidden\" class=\"ta_c\" name=\"vnfdId\" value='" + vnfdId + "' readonly>";
		 		}
		 		output += "</td>";
		 		output += "<td>";
		 		output += uuid;
		 		output += "<input type=\"hidden\" class=\"ta_c\" name=\"vnfProfileId\" readonly value=" + uuid + ">";
		 		output += "</td>";
		 		output += "<td>";
		 		output += "<input type=\"text\" class=\"ta_c\" name=\"vnfFlavourId\">"
// 		 		output += "<select name=\"vnfFlavourId\">";
// 		 		for(var l = 0; l < jvNode.length; l++) {
// 		 			output += "<option value=" + jvNode[l].vnfdFlavorId + ">" + jvNode[l].vnfdFlavorId + "</option>";
// 		 		}
// 		 		output += "</select>";
		 		output += "</td>";
		 		output += "<td><input type=\"text\" class=\"ta_c\" name=\"minNumberOfInstances\" onkeydown=\"onlyNumber(this);\"></td>";
		 		output += "<td><input type=\"text\" class=\"ta_c\" name=\"maxNumberOfInstances\" onkeydown=\"onlyNumber(this);\"></td>";
		 		output += "<td><input type=\"text\" class=\"ta_c\" name=\"dependencies\" onkeydown=\"onlyNumber(this);\"></td>";
		 		output += "<input type=\"hidden\" class=\"ta_c\" name=\"nsdf\" readonly>";
		 		output += "<input type=\"hidden\" name=\"vnfdNsdfId\" readonly value=" + nsdfId + ">";
		 		output += "</tr>";
			}

			$("#tbodyVnf").append(output);

		});
		
		
	}
	
	fn_nsdfChange = function(data, numFocus) {
		
 		var trNsdf   = $("[name='trNsdf']").length;
 		var trVnf    = $("[name='trVnf']").length;
 		var nsdfName = data.value;
 		var nsdfId   = "";
		var vnfLth   = "";
 		var convertor = app.convertor;
		var writer = new draw2d.io.json.Writer();
		
		writer.marshal(app.view, function(json) {
			var jvNode = convertor.jsonNode(json);
			vnfLth = jvNode.length;
		});
 		
 		if(nsdfName != "") {
	 		if(trVnf > 0) {

				for(var i = 0; i < $("[name='trNsdf']").length; i++) {
// 					alert(data.value);
					if($("[name='nsdfName']").eq(i).val() == data.value) {
// 						alert(i);
			 			nsdfName = $("[name='nsdfName']").eq(i).val();
			 			nsdfId = $("[name='nsdfId']").eq(i).val();
// 			 			$("select[name='nsdf'] option").eq(i).remove();
					}
				}

		 		for(var j = Number(vnfLth) * (Number(trNsdf-1)); j < trVnf; j++) {
					$("[name='tdvnfdNsdfId']").eq(j).html(nsdfName);
		 			$("[name='nsdf']").eq(j).val(nsdfName);
// 		 			$("[name='vnfdNsdfId']").eq(j).val(nsdfId);
		 		}
	 		}
 		} else {
 			alert("NSDF Flavor Name 을(를) 먼저 입력하세요.");
 		}
	}
	
	// 화살표 이전버튼에 링크 설정
	fn_prev = function() {
		console.log($("#prevLi").data("link"));
		if(! isEmpty($("#prevLi").data("link")) ) {
			
			var lSlide = "";
			var rSlide = "";
			
			if( $("#l_btn").attr('class') != "btn_act" ) {
				lSlide = "on";
			} else {
				lSlide = "btn_act";
			}

			if( $("#r_btn").attr('class') != "btn_act" ) {
				rSlide = "on";
			} else {
				rSlide = "btn_act";
			}
			
			// 앞뒤 데이터 다시 셋팅
			$.get("/oneView/design/" + prevNsdId + "/" + lSlide + "/" + rSlide + "/1", function(dataFull) {

				prevNsdId = dataFull.prev_nsd_id;
				nextNsdId = dataFull.next_nsd_id;
				nsdType = dataFull.now_nsd_id;

				app.reloadCanvas(nsdType);
				app.lodingGeneralInfo(nsdType);
				
			});

		}
		
	}
	
	// 화살표 다음버튼에 링크 설정
	fn_next = function() {
		if(! isEmpty($("#nextLi").data("link")) ) {
			
			var lSlide = "";
			var rSlide = "";
			
			if( $("#l_btn").attr('class') != "btn_act" ) {
				lSlide = "on";
			} else {
				lSlide = "btn_act";
			}
			
			if( $("#r_btn").attr('class') != "btn_act" ) {
				rSlide = "on";
			} else {
				rSlide = "btn_act";
			}

			// 앞뒤 데이터 다시 셋팅
			$.get("/oneView/design/" + nextNsdId + "/" + lSlide + "/" + rSlide + "/1", function(dataFull) {

				prevNsdId = dataFull.prev_nsd_id;
				nextNsdId = dataFull.next_nsd_id;
				nsdType = dataFull.now_nsd_id;

				app.reloadCanvas(nsdType);
				app.lodingGeneralInfo(nsdType);
				
			});

		} 
	}
	
	// NS Type 변경시
	fn_changeType = function(info) {

		if(info.value == "2") {
			$("#titleTab2").hide();
			$("#titleTab3").hide();
		} else {
			$("#titleTab2").show();
			$("#titleTab3").show();
		}
		
	}

	fn_onBoard = function() {

// 		onboarding.onBoardStartTime = setCheckTime("onboarding");
// 		$("#onBoardStartTime").html( onboarding.onBoardStartTime );
		
		callOnBoard( "", $("[name='nsdId']").eq(0).val(), $("[name='nsdName']").eq(0).val(), $("[name='targetNfvo']").eq(0).val() );
	}

	callOnBoard = function( status, nsdId, nsName, targetNfvo ) {
		alert("call OnBoard : "+isTracking);
		
		var callTime = setCheckTime( "onboarding" );
		
		var paramMap = {};

// 		paramMap.url 			= "http://175.213.170.14:8500/e2e/nfvo/nfv1/nsd/v1/ns_descriptors";
		paramMap.url 			= "http://175.213.170.14:8500/e2e/nfvo/" + targetNfvo + "/v1/ns_descriptors";
		paramMap.port 			= "8500";
		paramMap.method 		= "POST";

		paramMap.nsdId 			= nsdId;
		paramMap.nsName 		= nsName;
		paramMap.targetNfvo 	= targetNfvo;
		alert("param : "+createParamString( paramMap ));
		$.ajax({ 
			type: "get", 
			url : "/oneView/design/restfulPost" + createParamString( paramMap ), 
			data: "", 
			dataType:"json", 
			success : function(data, status, xhr) {
// 				alert("create NS result : "+data);
				var dataObj = JSON.parse( data );
// 				console.log(dataObj);
				
	           	 var myDate = new Date()
	        	 var yyyy= myDate.getFullYear();
	        	 var mm=String(myDate.getMonth() + 1);
	        	 var dd=String(myDate.getDate());
	        	 var h24=String(myDate.getHours());
	        	 var mi=String(myDate.getMinutes());
	        	 mm=(mm.length==1)?"0"+mm:mm;
	        	 dd=(dd.length==1)?"0"+dd:dd;
	        	 h24=(h24.length==1)?"0"+h24:h24;     
	        	 mi=(mi.length==1)?"0"+mi:mi;
	        	 var date = yyyy  + "/" +  mm+ "/" + dd + " " + h24 + ":" + mi;
				
				if(dataObj.nsdInfoState != undefined) {
					if(dataObj.nsdInfoState == "CREATED" || dataObj.nsdInfoState == "UPLOADING" || dataObj.nsdInfoState == "PROCESSING") {
					
						$("#onBoardNsdName").text(dataObj.nsd_name);
						$("#onBoardStart").show();
						$("#onBoardStartTime").text(date);
						
						var output = "";
						
						if(dataObj.nsdInfoState == "CREATED") {
							$("#onBoardCreated").attr("class", "active");
							$("#onBoardUploading").attr("class", "");
							$("#onBoardProcessing").attr("class", "");
							$("#onBoardOnboarded").attr("class", "");
							
							output += "<tr>";
							output += "<td>201 " + dataObj.nsd_inf_state + "</td>";
							output += "<td>" + date + " </td>";
							output += "<td>" + date + " </td>";
							output += "<td>" + date + " </td>";
							output += "</tr>";
							
						} else if(dataObj.nsdInfoState == "UPLOADING") {
							$("#onBoardCreated").attr("class", "on");
							$("#onBoardUploading").attr("class", "active");
							$("#onBoardProcessing").attr("class", "");
							$("#onBoardOnboarded").attr("class", "");
						} else if(dataObj.nsdInfoState == "PROCESSING") {
							$("#onBoardCreated").attr("class", "on");
							$("#onBoardUploading").attr("class", "on");
							$("#onBoardProcessing").attr("class", "active");
							$("#onBoardOnboarded").attr("class", "");
						}

						$("#onBoardTable").html(output);
						
// 						isTracking = true;
						
					} else {
						
						$("#onBoardNsdName").text(dataObj.nsd_name);
						$("#onBoardStart").show();
						$("#onBoardStartTime").text(date);
						
// 						isTracking = true;
					}
				}
				
// 				isTracking = true;
// 				alert("nsInstanceId : "+dataObj.nsInstanceId);
				//isTracking 
// 				callInstantiate( "createNS", dataObj.nsInstanceId );
				if( isTracking ){
// 					alert( dataObj.id + " : " + dataObj.nsdId);
 					orderTracking( dataObj.id, dataObj.nsdId );	
				}			
			}, 
			error: function(jqXHR, textStatus, errorThrown) {
// 				console.log(" ※※※※※※※※ " );
// 				console.log(jqXHR);
				alert("error : "+jqXHR.responseText); 
			} 
		});	
	}

	orderTracking = function( id, nsdId ) {
		try {
			var paramMap 		= {};
			paramMap.type		= "orderTracking";
			paramMap.id         = id;
			paramMap.nsdId 	    = nsdId;
			trackingCount++;
			
			var test = "";
			for( var i = 0 ; i < trackingCount ; i++ ){
				test+=".";
			}
// 			console.log(nsdId+" orderTracking Call"+test);
			$.ajax({ 
				type: "get", 
				url : "/oneView/design/orderTracking" + createParamString( paramMap ), 
				data: "", 
				dataType:"json", 
				success : function(data, status, xhr) {
// 					alert("성공");
					var dataObj;
					if( typeof data == "string" ){
						dataObj = JSON.parse( data );
					}else{
						dataObj = data;
					}

		           	 var myDate = new Date()
		        	 var yyyy= myDate.getFullYear();
		        	 var mm=String(myDate.getMonth() + 1);
		        	 var dd=String(myDate.getDate());
		        	 var h24=String(myDate.getHours());
		        	 var mi=String(myDate.getMinutes());
		        	 mm=(mm.length==1)?"0"+mm:mm;
		        	 dd=(dd.length==1)?"0"+dd:dd;
		        	 h24=(h24.length==1)?"0"+h24:h24;     
		        	 mi=(mi.length==1)?"0"+mi:mi;
		        	 var date = yyyy  + "/" +  mm+ "/" + dd + " " + h24 + ":" + mi;

					
// 					console.log("@@ returnMessage : "+dataObj.returnMessage);

					if( dataObj.nsd_inf_state != null && dataObj.nsd_inf_state != "" ){
						isTracking = false;
						
// 						$("#onBoardNsdName").text(dataObj.nsd_name);
// 						$("#onBoardStart").show();
// 						$("#onBoardStartTime").text(dataObj.createAt);
						
						var output = "";
						
						if(dataObj.nsd_inf_state == "CREATED") {
// 							$("#onBoardCreated").attr("class", "active");
// 							$("#onBoardUploading").attr("class", "");
// 							$("#onBoardProcessing").attr("class", "");
// 							$("#onBoardOnboarded").attr("class", "");
							
// 							output += "<tr>";
// 							output += "<td>201 " + dataObj.nsd_inf_state + "</td>";
// 							output += "<td>" + dataObj.createAt + " </td>";
// 							output += "<td>" + dataObj.createAt + " </td>";
// 							output += "<td>" + dataObj.createAt + " </td>";
// 							output += "</tr>";
							$("#onBoardEndTime").html(data);
// 							date
							
						} else if(dataObj.nsd_inf_state == "UPLOADING") {
// 							$("#onBoardCreated").attr("class", "on");
// 							$("#onBoardUploading").attr("class", "active");
// 							$("#onBoardProcessing").attr("class", "");
// 							$("#onBoardOnboarded").attr("class", "");
						} else if(dataObj.nsd_inf_state == "PROCESSING") {
// 							$("#onBoardCreated").attr("class", "on");
// 							$("#onBoardUploading").attr("class", "on");
// 							$("#onBoardProcessing").attr("class", "active");
// 							$("#onBoardOnboarded").attr("class", "");
						} else if(dataObj.nsd_inf_state == "ONBOARDED") {
// 							$("#onBoardCreated").attr("class", "on");
// 							$("#onBoardUploading").attr("class", "on");
// 							$("#onBoardProcessing").attr("class", "on");
// 							$("#onBoardOnboarded").attr("class", "active");
						}
						
						$("#onBoardTable").html(output);
						
						
						alert("Onboarding End !! ");
					}else{
						setTimeout(orderTracking, trackingTime, nsdId);
					}
				}, 
				error: function(jqXHR, textStatus, errorThrown) { 
					alert("error : "+jqXHR.responseText); 
				} 
			});		
		} catch (e) {
			alert(e);
		}

	}

	// 빈값여부 체크
    isEmpty = function(str) {
        
        if(typeof str == "undefined" || str == null || str == "")
            return true;
        else
            return false ;
    }

	// UUID 생성
    generateUUID = function () {
		var d = new Date().getTime();
		var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
			var r = (d + Math.random()*16)%16 | 0;
			d = Math.floor(d/16);
			return (c=='x' ? r : (r&0x7|0x8)).toString(16);
		});
		return uuid;
    }
    
    //모두 체크/모두 미체크의 체크박스 처리
    changeCheckBox = function(flag)
    {
    	if(flag)
    		{
    		 $('input:checkbox[name="chkNsdf"]').each(function() {
    		      this.checked = true; //checked 처리
    		 });


    		}else{
    		
       		 $('input:checkbox[name="chkNsdf"]').each(function() {
    		      this.checked = false; //checked 처리
    		      if(this.checked){//checked 처리된 항목의 값
    		            alert(this.value); 
    		      }
    		 });
    			
    		}
    }
  
    //체크된 로우 삭제처리
    function submitCheckDelete()
    {
    	var f=document.form;
//     	f.cust_manage_no.value="0";
    	checkBoolean=false;
    	alert(1);
    	if (typeof(f.elements['chkNsdf']) != 'undefined') 
    	{
    		if (typeof(f.elements['chkNsdf'].length) == 'undefined')
    		{
    			if(f.elements['chkNsdf'].checked==true)
    			{
    				checkBoolean=true;
    			}
    		}else{
    	        for (i=0; i<f.elements['chkNsdf'].length; i++)
    	        {
    	            if (f.elements['chkNsdf'][i].checked==true)
    	            {
    	            	alert(f.elements['chkNsdf'][i].value);
    	            	checkBoolean=true;
    	            	break;
    	            }
    	        }
    		}

            if(checkBoolean)
           	{
//                f.action="/equipInven/delete/select";
//                f._method.value="DELETE";
//                f.submit();        	
           	}else{
//            		alert('선택된 내용이 없습니다.');

           	}

    	}
    	
    }
    
    // 삭제 버튼 클릭시
    fn_delete = function () {
    	 if (confirm("삭제 하시겠습니까?")) {

    			$.ajax({ 
    				type: "POST", 
    				url : "/oneView/delete/" + $("[name='nsdId']").eq(0).val(),
    				success: function(result) {
    					alert("삭제 되었습니다.");
   	 					location.href="/oneView/list";
    				}, 
    				error: function(jqXHR, textStatus, errorThrown) {
    					alert("error : "+jqXHR.responseText); 
    				} 

    			});
    	 }

    }
    
    // 파라미터 만들기
    createParamString = function( paramMap ) {
    	var param = "";
    	var isFirst = true;
    	
    	if( paramMap != null ){
    		for( var key in paramMap ){
    			if( isFirst ){
    				param += "?";
    				isFirst = false;
    			}else{
    				param += "&";
    			}
    			if( typeof paramMap[ key ] == "number" ){
    				param += key +"=" + paramMap[ key ] ;
    			}else{
    				param += key +"=" + encodeURIComponent(paramMap[ key ]);
    			}
    			
    		}
    	}
    	return param;
    }
    
    function onlyNumber(obj) {
        $(obj).keyup(function(){
             $(this).val($(this).val().replace(/[^0-9]/g,""));
        }); 
    }
    
	//////////////////////////////////////////////////////////////
	control = function() {
		$("#prevLi").hide();
		$("#nextLi").hide();

		// 좌측 슬라이드 접기/펴기
		if(lSlide == "on") {
		 	$("#wrap").toggleClass('slid_fold');
		 	$("#l_btn").toggleClass('on');		
		} else {
		 	$("#wrap").toggleClass('');
		 	$("#l_btn").toggleClass('');		
		}

		// 우측 슬라이드 접기/펴기
		if(rSlide == "on") {
		 	$("#wrap").toggleClass('slide_palette');
		 	$("#r_btn").toggleClass('on');
		} else {
		 	$("#wrap").toggleClass('');
		 	$("#r_btn").toggleClass('');		
		}

		// 신규 일 경우 UUID 셋팅
		if(isNew == true) {
			$("[name='nsdId']").eq(0).val(vUUID);
			$("[name='nsdId']").attr("readonly", true);
		} else {
			$("[name='nsdId']").attr("readonly", true);
		}
	}
    
</script>


</head>


<body>

<div id="wrap">
    
    <header>
		<h1 class="logo"><a href="#">KT E2E Infra Orchestrator</a></h1>
		<nav id="gnb">
			<ul>
				<li class="active"><a href="/oneView/list">DESIGN</a></li><!-- 활성화 class="active" -->
				<li><a href="/provisioning">PROVISIONING</a></li>
				<li><a href="#">MONITORING</a></li>
				<li><a href="#">INVENTORY</a></li>
				<li><a href="#">CONFIGURATION</a></li>
				<li>
					<a href="#">P-LTE</a>
					<ul>
						<li><a href="#">국데이터 관리</a></li>
						<li><a href="#">명령 실행</a></li>
						<li><a href="#">명령 개별 실행</a></li>
						<li><a href="#">고객사 관리</a></li>
						<li><a href="#">장비 관리</a></li>
						<li><a href="#">명령어 관리</a></li>
						<li><a href="#">모니터링</a></li>
						<li><a href="#">이력조회</a></li>
					</ul>
				</li>
			</ul>
		</nav><!-- //gnb -->
		<div class="ad_info">
			<i><img src="/new_resources/resources/images/layout/img_ad_info.jpg" alt=""></i><!-- image size : 25*25px -->
			<p>admin</p>
		</div><!-- //ad_info -->
    </header>
    
    <form name="oneView" id="oneView">
    <input type="hidden" name="nsdfIdLnf" />
    <input type="hidden" name="vnfIdLnf" />
    <input type="hidden" name="vlLnf" />
    
    <div id="container">	
		
		<ul id="navigation">
			<li><img src="/new_resources/resources/images/layout/img_navigation_home.png" alt="home"></li>
			<li>DESIGN</li>
		</ul><!-- //navigation -->
		
		<div id="contents_wrap" class="design_wrap"><!-- design_wrap -->
		
			<div id="lnb" class="lnb_fold">
				<div class="lnb_search">
					<div class="inner">
						<input type="text" placeholder="스텐실명을 입력해주세요.">
						<a href="#" class="btn">검색</a>
					</div>
				</div><!-- //lnb_search -->
				<ul class="lnb_nav" id="canvasLeft">
					<li>
						<a href="#">VNF Component</a>
						<div class="lnb_dep" id="vnf_components">
							<ul class="dep_sub">
								<li>
									<a href="#">4G</a>
									<div class="dep_sub_area">
										<div class="scrollbar-inner">
											<ul id="vnf4g">
											</ul>
										</div>
									</div>
								</li>
								<li>
									<a href="#">5G</a>
									<div class="dep_sub_area">
										<div class="scrollbar-inner">
											<ul id="vnf5g">
											</ul>
										</div>
									</div>
								</li>
							</ul><!-- //dep_sub -->
						</div>
					</li>
					<li>
						<a href="#">PNF Component</a>
						<div class="lnb_dep subarea" id="pnf_components">
							<div class="scrollbar-inner">
								<ul>
								</ul>
							</div>
						</div>
					</li>
					<li>
						<a href="#">Connectivity</a>
						<div class="lnb_dep subarea" id="conn_components">
							<div class="scrollbar-inner">
								<ul>
								</ul>
							</div>
						</div>
					</li>
					<li>
						<a href="#">Cloud</a>
						<div class="lnb_dep subarea" id="cloud_components">
							<div class="scrollbar-inner">
								<ul>
								</ul>
							</div>
						</div>
					</li>
				</ul>
				<button type="button" class="btn_act" id="l_btn">left slide button</button>
			</div><!-- //lnb -->
			
			<div class="contents">
				<nav class="topology_btn">
					<!-- 활성화 : btn_l btn_co01 비활성화 : btn_l btn_co02 -->
					<a href="#" id="btnSave"  class="btn_l btn_co01" onClick="app.saveData(); return false;">Save</a>
					<a href="#" id="btnBoard" class="btn_l btn_co01" onClick="fn_onBoard(); return false;">On-board</a>
					<a href="#" id="btnAble"  class="btn_l btn_co01" onClick="fn_enable(); return false;">Enable</a>
					<a href="#" id="btndel"   class="btn_l btn_co01" onClick="fn_delete(); return false;">Delete</a>
				</nav>
				<div class="topology_canvas" id="canvas">
					<ul class="control_nav">
						<li class="prev" id="prevLi"><a href="#" onClick="fn_prev(); return false;">이전</a></li>
						<li class="next" id="nextLi"><a href="#" onClick="fn_next(); return false;">다음</a></li>
					</ul>
				</div><!-- topology_canvas -->
				<div class="palette" id="canvasRight">
					<div class="inner">
						<ul class="tab tab_nav"> <!-- class="tab_nav" -->
							<li class="on"><a href="#tab01">General Info</a></li>
							<li id="titleTab2"><a href="#tab02">Deployment Flavor</a></li>
							<li id="titleTab3"><a href="#tab03">Process Tracking</a></li>
						</ul><!-- //tab -->
						<div class="tab_con_area scrollbar-inner">
							<div class="tab_con on" id="tab01">
								<div class="table_form03">
									<table>
										<caption></caption>
										<colgroup>
											<col span="1" style="width:25%;">
											<col span="1" style="width:*;">
										</colgroup>
										<tbody>
											<tr>
												<th><span>NS Type</span><i class="required">필수항목</i></th>
												<td colspan="3">
													<select name="nsType" id="nsType" required onChange="fn_changeType(this);" style="width:80px;">
														<option value="0" selected>4G</option>
														<option value="1">5G</option>
														<option value="2">P-LTE</option>
													</select>
												</td>
											</tr>
											<tr>
												<th><span>NSD Name</span><i class="required">필수항목</i></th>
												<td colspan="3">
													<input type="text" name="nsdName" value="" required/>
												</td>
											</tr>
											<tr>
												<th><span>NSD Version</span><i class="required">필수항목</i></th>
												<td colspan="3">
													<input type="text" name="version" value="" required style="width:150px;"/>
												</td>
											</tr>
											<tr>
												<th><span>NSD Designer</span></th>
												<td colspan="3">KT
													<input type="hidden" name="designer" value="KT"/>
												</td>
											</tr>
											<tr>
												<th><span>작성자</span></th>
												<td colspan="3">홍길동
													<input type="hidden" name="designerName" value="홍길동"/>
												</td>
											</tr>
											<tr>
												<th><span>NSD ID</span><i class="required">필수항목</i></th>
												<td colspan="3">
													<input type="text" name="nsdId" value="" required />
												</td>
											</tr>
											<tr>
												<th><span>Target NFVO</span><i class="required">필수항목</i></th>
												<td>
													<select name="targetNfvo" id="targetNfvo" required style="width:120px;">
<!-- 														<option value="HYWA_NFVO1" selected>HYWA_NFVO1</option> -->
<!-- 														<option value="HYWA_NFVO2" >HYWA_NFVO2</option> -->
<!-- 														<option value="HYWA_NFVO3" >HYWA_NFVO3</option> -->
<!-- 														<option value="GURO NFV-O" selected>GURO NFV-O</option> -->
													</select>
												</td>
											</tr>
											<tr>
												<th><span>Description</span></th>
												<td colspan="3"><textarea name="nsdDesc" cols="30" rows="10"></textarea></td>
											</tr>
										</tbody>
									</table>
								</div><!-- //table_form -->
							</div><!-- //tab_con -->
							<div class="tab_con" id="tab02">
								<div class="table_ty">
									<div class="table_head">
										<h6 class="title">NS Deployment Flavor</h6>
										<div class="btn_area">
											<a href="#" class="btn_m btn_co01" onClick="fn_addNsdf(); return false;">추가</a>
											<a href="#" class="btn_m btn_co02" onClick="fn_delNsdf(); return false;">삭제</a>
										</div>
									</div>
									<table id="tableNsdf">
										<caption>목록 테이블</caption>
										<colgroup>
											<col span="1" style="width:6%;">
											<col span="1" style="width:*;">
											<col span="1" style="width:47%;">
										</colgroup>
										<thead>
											<tr>
												<th scope="col"><input type="checkbox" name="checkALL" value="" onchange="changeCheckBox(this.checked)"></th>
												<th scope="col">Flavor Name</th>
												<th scope="col">Flavor ID</th>
											</tr>
										</thead>
										<tbody id="tbodyNsdf">
										</tbody>
									</table>
								</div><!-- //table_ty -->

								<div class="table_ty">
									<div class="table_head">
										<h6 class="title">VNF Deployment Flavor</h6>
<!-- 										<div class="btn_area"> -->
<!-- 											<a href="#" class="btn_m btn_co01" onClick="fn_addVnf(); return false;">추가</a> -->
<!-- 											<a href="#" class="btn_m btn_co02" onClick="fn_delVnf(); return false;">삭제</a> -->
<!-- 										</div> -->
									</div>
									<table>
										<caption>목록 테이블</caption>
										<colgroup>
											<col span="1" style="width:11%;">
											<col span="1" style="width:14%;">
											<col span="1" style="width:*;">
											<col span="2" style="width:14%;">
											<col span="2" style="width:9%;">
											<col span="1" style="width:11%;">
										</colgroup>
										<thead>
											<tr>
<!-- 												<th scope="col"></th> -->
												<th scope="col">NS DF</th>
												<th scope="col">VNF<br>Profile <br>Name</th>
												<th scope="col">VNFD<br>Name</th>
												<th scope="col">VNF<br>Profile ID</th>
												<th scope="col">VNF Flavor ID</th>
												<th scope="col">Min<br>Ins</th>
												<th scope="col">Max<br>Ins</th>
												<th scope="col">Deploy<br>ment<br>order</th>
											</tr>
										</thead>
										<tbody id="tbodyVnf">

										</tbody>
									</table>
								</div><!-- //table_ty -->

<!-- 								<div class="table_ty"> -->
<!-- 									<div class="table_head"> -->
<!-- 										<h6 class="title">Virtual Link</h6> -->
<!-- 										<div class="btn_area"> -->
<!-- 											<a href="#" class="btn_m btn_co01" onClick="fn_addVl(); return false;">추가</a> -->
<!-- 											<a href="#" class="btn_m btn_co02" onClick="fn_delVl(); return false;">삭제</a> -->
<!-- 										</div> -->
<!-- 									</div> -->
<!-- 									<table> -->
<!-- 										<caption>목록 테이블</caption> -->
<!-- 										<colgroup> -->
<!-- 											<col span="1" style="width:6%;"> -->
<!-- 											<col span="3" style="width:15%;"> -->
<!-- 											<col span="1" style="width:*;"> -->
<!-- 										</colgroup> -->
<!-- 										<thead> -->
<!-- 											<tr> -->
<!-- 												<th scope="col"></th> -->
<!-- 												<th scope="col">NSDF</th> -->
<!-- 												<th scope="col">VNF Prof ID</th> -->
<!-- 												<th scope="col">VL Name</th> -->
<!-- 												<th scope="col">VL ID</th> -->
<!-- 											</tr> -->
<!-- 										</thead> -->
<!-- 										<tbody id="tbodyVl"> -->
<!-- 										</tbody> -->
<!-- 									</table> -->
<!-- 								</div>//table_ty -->
							</div><!-- //tab_con -->
							<div class="tab_con" id="tab03">
								<div class="time_list_area">
									<h6 class="title" id="onBoardNsdName"></h6>
									<ul class="time_list" id="onBoardTimeList">
										<li id="onBoardStart" style="display:none;">
											<strong class="tit">Onboard Start Time</strong>
											<span class="txt" id="onBoardStartTime">2018-01-02 :11:00:00</span>
										</li>
										<li id="onBoardEnd" style="display:none;">
											<strong class="tit">Onboard End Time</strong>
											<span class="txt" id="onBoardEndTime">2018-01-02 :11:00:00</span>
										</li>
										<li id="onBoardDuration" style="display:none;">
											<strong class="tit">Duration</strong>
											<span class="txt" id="onBoardDurationTime">5 min</span>
										</li>
									</ul>
								</div><!-- //time_list_area -->
								<div class="step_ty02">
									<ol>
										<li id="onBoardCreated">
											<i>1</i>
											<strong class="word">Created</strong>
										</li>
										<li id="onBoardUploading">
											<i>2</i>
											<strong class="word">Uploading</strong>
										</li>
										<li id="onBoardProcessing">
											<i>3</i>
											<strong class="word">Processing</strong>
										</li>
										<li id="onBoardOnboarded">
											<i>4</i>
											<strong class="word">Onboarded</strong>
										</li>
									</ol>
								</div><!-- //step_ty02 -->
								<div class="table_ty">
									<table>
										<caption>목록  테이블</caption>
										<colgroup>
											<col span="1" style="width:16%;">
											<col span="2" style="width:24%;">
											<col span="1" style="width:20%;">
										</colgroup>
										<thead>
											<tr>
												<th scope="col">Res Code</th>
												<th scope="col">Start Time</th>
												<th scope="col">End Time</th>
												<th scope="col">Duration(sec)</th>
											</tr>
										</thead>
										<tbody id="onBoardTable">
<!-- 											<tr> -->
<!-- 												<td>201 Created</td> -->
<!-- 												<td>2018-01-02 11:00:00</td> -->
<!-- 												<td>2018-01-02 11:00:00</td> -->
<!-- 												<td>30 Sec</td> -->
<!-- 											</tr> -->
<!-- 											<tr> -->
<!-- 												<td>201 Uploaded</td> -->
<!-- 												<td>2018-01-02 11:00:00</td> -->
<!-- 												<td>2018-01-02 11:00:00</td> -->
<!-- 												<td>30 Sec</td> -->
<!-- 											</tr> -->
<!-- 											<tr> -->
<!-- 												<td>201 Completed</td> -->
<!-- 												<td>2018-01-02 11:00:00</td> -->
<!-- 												<td>2018-01-02 11:00:00</td> -->
<!-- 												<td>30 Sec</td> -->
<!-- 											</tr> -->
<!-- 											<tr> -->
<!-- 												<td>201 Onboarded</td> -->
<!-- 												<td>2018-01-02 11:00:00</td> -->
<!-- 												<td>2018-01-02 11:00:00</td> -->
<!-- 												<td>30 Sec</td> -->
<!-- 											</tr> -->
										</tbody>
									</table>
								</div><!-- //table_ty -->
							</div><!-- //tab_con -->
						</div><!-- //scrollbar-inner -->
					</div>
					<button type="button" class="btn_act" id="r_btn">palette slide button</button>
				</div><!-- //palette -->
			</div><!-- //contents -->

		</div><!-- //contents_wrap -->
	</div><!-- //container -->
	
	</form>
	
	<footer class="footer02">
		<nav class="footer_nav">
			<ul>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">이용약관</a></li>
				<li><a href="#">개인정보처리방침</a></li>
				<li><a href="#">Sitemap</a></li>
			</ul>
		</nav><!-- //footer_nav -->
		<address>㈜케이티 대표이사 황창규 경기도 성남시 분당구 불정로 90 (정자동) 사업자등록번호 : 102-81-42945 통신판매업신고 : 2002-경기성남-0047</address>
	</footer>
	
</div><!-- //wrap -->

</body>
</html>
