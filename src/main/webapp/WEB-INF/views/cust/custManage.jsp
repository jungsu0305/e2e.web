<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content= "text/html; charset=UTF-8"> 
<meta name="format-detection" content="telephone=no">
<title>KT E2E Infra Orchestrator</title>
<link rel="shortcut icon" href="/new_resources/resources/images/layout/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="/new_resources/resources/css/style.css">
<script type="text/javascript" src="/new_resources/resources/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/template.js"></script>

<script>	
/*$(function(){
	
	$(function(){pop_open('#pop03')});

});
*/



function data_write()
{
	var f=document.form;
	
	f.cust_nm.value=$("input[name='cust_nm_reg']").val();
	f.apn.value=$("input[name='apn_reg']").val();
	f.lte_id.value=$("input[name='lte_id_reg']").val();
	f.cust_description.value=$("textarea[name='cust_description_reg']").text();
	f.method="POST";
	f.action="/cust/post";
	
	f.submit();
	pop_close('#pop03_0');
}


function data_ajax_update()
{
	var cust_conf_no=[];
	var lte_id=[];
	var apn=[];
	for(var i=0; i < $('input[name="lte_id_mod"]').length;i++)
	{
		for(var j=0; j< $("input[name=\"apn_mod_" + i +"\"]").length;j++)
		{
			cust_conf_no.push($("input[name=\"cust_manage_no_mod_" + i +"\"]").eq(j).val());
			lte_id.push($('input[name="lte_id_mod"]').eq(i).val());
			apn.push($("input[name=\"apn_mod_" + i +"\"]").eq(j).val())
		
		}
	}
	
	
	$.ajax({
		type : "POST",
		url : "/cust/ajaxUpdate",
		contentType:"application/x-www-form-urlencoded;charset=utf-8", //한글 깨짐 방지
		data : {

			cust_nm : $("input[name='cust_nm_mod']").val(),
			cust_conf_no :cust_conf_no.join('|||'),
			apn : apn.join('|||'),
			lte_id : lte_id.join('|||'),
			
			cust_description : $("textarea[name='cust_description_mod']").text()
		 
		},
		dataType : "text",
		cache: false, 
		success : function(result) { 		
			if(result!="")
			{
			 var dataObj = JSON.parse(result);
	
			}
			 alert('고객정보 변경에 완료했습니다');
			 
		},
		error : function(result, status, err) {
			alert('고객정보 변경에 실패했습니다');
			pop_close('#pop03');
		},
	 	beforeSend: function() {
	 		pop_open('#pop03');
			
	 		//myLayout.cells("b").progressOn();
		},		
		complete : function() {
			//myLayout.cells("b").progressOff();
	 		pop_close('#pop03');
	 		searchData();
		}
		
	});
	
	
}


function data_update()
{
	var f=document.form;
	
	f.cust_manage_no.value=$("input[name='cust_manage_no_mod']").val();
	f.cust_nm.value=$("input[name='cust_nm_mod']").val();
	f.apn.value=$("input[name='apn_mod']").val();
	f.lte_id.value=$("input[name='lte_id_mod']").val();
	f.cust_description.value=$("textarea[name='cust_description_mod']").text();
	f.method="POST";
	f._method.value="PUT";
	f.action="/cust/post/"+$("input[name='cust_manage_no_mod']").val();
	
	f.submit();
	pop_close('#pop03');
}

function data_delete()
{
	var f=document.form;
	
	f.cust_manage_no.value=$("input[name='cust_manage_no_mod']").val();
	f.method="POST";
	f._method.value="DELETE";
	f.action="/cust/delete/"+$("input[name='cust_manage_no_mod']").val();

	f.submit();
	pop_close('#pop03');
}

//모두 체크/모두 미체크의 체크박스 처리
function changeCheckBox(flag)
{
	if(flag)
		{
		 $('input:checkbox[name="checkNo"]').each(function() {
		      this.checked = true; //checked 처리
		 });


		}else{
		
   		 $('input:checkbox[name="checkNo"]').each(function() {
		      this.checked = false; //checked 처리
		      if(this.checked){//checked 처리된 항목의 값
		            alert(this.value); 
		      }
		 });
			
		}
}

//체크된 로우 삭제처리
function submitCheckDelete()
{
	var f=document.form;
	f.cust_manage_no.value="0";
	checkBoolean=false;
	
	if (typeof(f.elements['checkNo']) != 'undefined') 
	{
		if (typeof(f.elements['checkNo'].length) == 'undefined')
		{
			if(f.elements['checkNo'].checked==true)
			{
				checkBoolean=true;
			}
		}else{
	        for (i=0; i<f.elements['checkNo'].length; i++)
	        {
	            if (f.elements['checkNo'][i].checked==true)
	            {
	            	//alert(f.elements['checkNo'][i].value);
	            	checkBoolean=true;
	            	break;
	            }
	        }
		}
        
        if(checkBoolean)
       	{
           f.action="/cust/delete/select";
           f._method.value="DELETE";
           f.submit();        	
       	}else{
       		alert('선택된 내용이 없습니다.');

       	}

	}
	
}
//action="/board/post/${board.bno}" method="PUT">

// 정렬클릭시
    function listOrder(colName,ordOption)
    {
    	var orderKey="";
    	var commaFlag="";
    	orderKey+="(";
  		 $('input:checkbox[name="checkNo"]').each(function() {
  			orderKey+=commaFlag+this.value
  			commaFlag=",";
		 });
  		orderKey+=")";
    	//alert(orderKey);
        $.ajax({
            type: "get",
           // contentType: "json", //==> 생략가능(RestController이기때문에 가능)
            url: "cust/jsondata?colName="+colName
            		  +"&ordOption="+ordOption+"&orderKey="+orderKey,
            success: function(result){

              // alert(result)
                	 var dataObj = JSON.parse(result);
                     var output = "";
                     for(var i in dataObj){

                    	 var myDate = new Date(dataObj[i].regist_dtm)
                    	 var yyyy= myDate.getFullYear();
                    	 var mm=String(myDate.getMonth() + 1);
                    	 var dd=String(myDate.getDate());
                    	 var h24=String(myDate.getHours());
                    	 var mi=String(myDate.getMinutes());
                    	 mm=(mm.length==1)?"0"+mm:mm;
                    	 dd=(dd.length==1)?"0"+dd:dd;
                    	 h24=(h24.length==1)?"0"+h24:h24;     
                    	 mi=(mi.length==1)?"0"+mi:mi;
                    	// var date = yyyy  + "/" +  mm+ "/" + dd + " " + h24 + ":" + mi;
                    	 var date = yyyy  + "." +  mm+ "." + dd;
                         output += "<tr>";
                         output += "<td><input type=checkbox name='checkNo' value='" + dataObj[i].cust_manage_no +"'></td>";
                         output += "<td><a href=\"#\" onClick=\"popup_control('2','open','" + dataObj[i].cust_manage_no +"');\" title=\"" + dataObj[i].cust_nm+ "\" class=\"pop_btn\">" 
                                      + dataObj[i].cust_nm +"</a></td>";
                         //<td><a href="#pop03" title="${board.cust_nm}" class="pop_btn">${board.cust_nm}</a></td>
                         output += "<td>" + dataObj[i].lte_id +"</td>";
                         output += "<td>" + dataObj[i].apn +"</td>";
                         output += "<td>" + date +"</td>";                
                         output += "<td>" + ((typeof(dataObj[i].regist_id)=="undefined")?"":dataObj[i].regist_id) +"</td>";
                         output += "<tr>";
                      

                     }
                     output += "";

                $("#trData").html(output);
            }
        });
    }
    

function popup_control(gubun,oflag,okey)
{
	
	try {
	
	var f=document.form;
	if(gubun=="1")
	{
		if(oflag=="open")
		{
			pop_open('#pop03_0');
			$("input[name='cust_nm_reg']").focus();

		}else{
			pop_close('#pop03_0');
		}
	}else if(gubun=="2"){
		if(oflag=="open")
		{
			pop_open('#pop03');
			 $.ajax({
		            type: "get",
		           // contentType: "json", //==> 생략가능(RestController이기때문에 가능)
		            url: "/cust/getLteidApnJsondata/"+okey,
		            success: function(result){
	                	 var dataObj = JSON.parse(result);

	                	 var output = "";
	                	 output+="<li class=\"li\">";
                		 output+="<strong class=\"tit\">고객사명</strong>";
                		 output+="<input type=\"text\" name=\"cust_nm_mod\"  readonly value=\"\">";
                		 output+="</li>";
                		 output+="<li class=\"li\">";
                		 output+="<strong class=\"tit\">Description</strong>";
                		 output+="<textarea cols=\"5\" rows=\"10\"  name=\"cust_description_mod\" style=\"height:115px;\"></textarea>";
                		 output+="</li>";
	                	 $("#ulLteId").html(output);
	        			 var tmp_lte_id = "";
	        			 var lte_id_cnt=-1;
	        			 var sub_apn_cnt=0;
	                     for(var i in dataObj){
	                    	 if(i==0)
	                    	 {
		                    	 $("input[name='cust_nm_mod']").val(dataObj[i].cust_nm);
		                    	 $("textarea[name='cust_description_mod']").text(dataObj[i].cust_description);
	                          }
	                    	 if(tmp_lte_id != dataObj[i].lte_id)
                    		 {
	                    		 lte_id_cnt++;
	                    		tmp_lte_id=dataObj[i].lte_id;
                    			addLteId();
                    			sub_apn_cnt=0;
                    			$('input[name="lte_id_mod"]').eq(lte_id_cnt).val(dataObj[i].lte_id);
                    			$("input[name=\"cust_manage_no_mod_" + lte_id_cnt +"\"]").eq(sub_apn_cnt).val(dataObj[i].cust_manage_no);
	                    	 	$("input[name=\"apn_mod_" + lte_id_cnt +"\"]").eq(sub_apn_cnt).val(dataObj[i].apn);
	                    	 	
	                    	 	sub_apn_cnt++;
                    		 }else if(tmp_lte_id == dataObj[i].lte_id){
                    			addApn(lte_id_cnt);
                    			$("input[name=\"cust_manage_no_mod_" + lte_id_cnt +"\"]").eq(sub_apn_cnt).val(dataObj[i].cust_manage_no);
	                    	 	$("input[name=\"apn_mod_" + lte_id_cnt +"\"]").eq(sub_apn_cnt).val(dataObj[i].apn);
	                    	 	sub_apn_cnt++;
                    		 }
	                    	

	                     }
	                		                     
		            }
		        });
			
		}else{
			pop_close('#pop03');
		}
	}
	}
	catch(err) {
	    alert(err.message);
	}finally {
        
    }
	
}

function list(page){
	var f=document.form;
    location.href="cust?curPage="+page+"&searchOption=${searchOption}"+"&keyword=${keyword}";
}

function searchData(){
	var f=document.form;
	var searchOption=f.searchOption.value;
	var keyword=f.keyword.value;
	//alert(searchOption +"--"+keyword);
    location.href="cust?curPage=1&searchOption=" + searchOption +"&keyword=" +keyword;
}

function addLteId(){
	//$("#lte_id_apn_ul li:eq(" + 1 +")").clone().html()
	var ulApn_length=$('ul[id="ulApn"]').length;
	
	var lte_html="<li class=\"li ty_flex\">"
			+ "<strong class=\"tit\">LTE ID</strong>"
			+ "<div class=\"btn_list\">"
			+ "	<input type=\"text\" name=\"lte_id_mod\" value=\"\">"
			+ "	<a href=\"#\" onClick=\"addLteId()\" class=\"btn_m btn_co03 on\">추가</a>"
			+ "</div>"
			+ "</li>";
	var apn_html="<li class=\"li ty_flex\">"
			+ "<ul id=\"ulApn\" class=\"inp_list\">"
			+ "<li class=\"li\">"
			+ "	<em class=\"s_tit\">APN<input type=\"hidden\" name=\"cust_manage_no_mod_" + ulApn_length + "\" value=\"0\" ></em>"
			+ "	<div><input type=\"text\" name=\"apn_mod_" + ulApn_length +"\"></div>"
			+ "</li>"
			+ "</ul>"
			+ "<div class=\"btn_area\">"
			+ "<a href=\"#\" onClick=\"addApn(" + ulApn_length + ")\" class=\"btn_m btn_co03 on\">추가</a>"
			+ "</div>"
			+ "</li>";
			
			$.liLength=$("#ulLteId li").length;
			$("#ulLteId li:eq(" + ($.liLength-1) +")").before(lte_html);
			$.liLength=$("#ulLteId li").length;
			$("#ulLteId li:eq(" + ($.liLength-1) +")").before(apn_html);
			
			//alert($('ul[id="ulApn"]').eq(1).html());
			
			//alert($("#ulApn:eq(1) li").length);
}


function addApn(idx){
	//$("#lte_id_apn_ul li:eq(" + 1 +")").clone().html()
	var apn_html="<li class=\"li\">"
			+ "	<em class=\"s_tit\">APN<input type=\"hidden\" name=\"cust_manage_no_mod_" + idx + "\" value=\"0\" ></em>"
			+ "	<div><input type=\"text\" name=\"apn_mod_" + idx + "\"></div>"
			+ "</li>";
			
			$('ul[id="ulApn"]').eq(idx).append(apn_html);
			

			
}
</script>

</head>

<body>

<div id="wrap">

    <header>
		<h1 class="logo"><a href="#">KT E2E Infra Orchestrator</a></h1>
		<nav id="gnb">
			<ul>
				<li><a href="#">DESIGN</a></li>
				<li><a href="#">PROVISIONING</a></li>
				<li><a href="#">MONITORING</a></li>
				<li><a href="#">INVENTORY</a></li>
				<li><a href="#">CONFIGURATION</a></li>
				<li class="active"><!-- 활성화 class="active" -->
					<a href="#">P-LTE</a>
					<ul>
						<li><a href="/gugDataConfig">국데이터 관리</a></li><!-- 활성화 class="active" -->
						<li><a href="/gugPcmdCrExec">명령 실행</a></li>		
						<li><a href="/indPcmdCrExec">운용자 명령어</a></li>
						<li class="active"><a href="/cust">고객사 관리</a></li>
						<li><a href="/equipInven">장비 관리</a></li>
						<li><a href="#">모니터링</a></li>
						<li><a href="#">이력조회</a></li>
					</ul>
				</li>
			</ul>
		</nav><!-- //gnb -->
		<div class="ad_info">
			<i><img src="/new_resources/resources/images/layout/img_ad_info.jpg" alt=""></i><!-- image size : 25*25px -->
			<p>admin</p>
		</div><!-- //ad_info -->
    </header>
    
    <div id="container">	
		
		<ul id="navigation">
			<li><img src="/new_resources/resources/images/layout/img_navigation_home.png" alt="home"></li>
			<li>고객사 관리</li>
		</ul><!-- //navigation -->
		
		<div id="contents_wrap">
			<div class="contents">
			    <form:form commandName="CustComponent" name="form" action="" onsubmit="return false;">
		    	<input type="hidden" name="_method" value=""><!-- RESTFUL 메소드 오버라이드를 위해  -->
				<input type="hidden" name="cust_manage_no" value="0"><!-- 숫자형으므로 기본 숫자값 줘야함  -->
				<input type="hidden" name="cust_nm">
				<input type="hidden" name="apn">
				<input type="hidden" name="lte_id">
				<textarea name="cust_description" cols="" rows="" style="display:none;"></textarea>
				<div class="search_area">
					<div class="inner">
						<table>
							<caption>검색 테이블</caption>
							<colgroup>
								<col span="1" style="width:15%;">
								<col span="1" style="width:*;">
							</colgroup>
							<tbody>
								<tr>
									<td>
										<select name="searchOption">
									     	<option value="cust_nm" <c:if test='${searchOption=="cust_nm"}'> selected </c:if> >고객사명</option>
	    	                                <option value="lte_id" <c:if test='${searchOption=="lte_id"}'> selected </c:if> >LTE ID</option>
	    	                                <option value="cust_id"  <c:if test='${searchOption=="cust_id"}'> selected </c:if> >고객사</option>
	    	                                <option value="regist_id"  <c:if test='${searchOption=="regist_id"}'> selected </c:if> >등록자</option>
										</select>
									</td>
									<td><input name="keyword" type="text" value="${keyword}"></td>
								</tr>
							</tbody>	
						</table>
						<div class="btn_area">
							<a href="#" onClick="searchData()" class="btn_l btn_co01 on">조회하기</a>
						</div>
					</div>
				</div><!-- //search_area -->
								
				<div class="table_ty">
					<div class="table_head">
						<div class="count">Total Count <span>${count}</span></div>
						<div class="btn_area">
							<!-- <a href="#" onClick="popup_control('1','open','0');" class="btn_l btn_co01 pop_btn">고객사등록</a> -->
							<a href="#" onClick="submitCheckDelete()" class="btn_l btn_co02">선택삭제</a>
						</div>
					</div>
					<table>
						<caption></caption>
						<colgroup>
							<col span="1" style="width:5%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:15%;">
							<col span="1" style="width:15%;">
							<col span="1" style="width:15%;">
							<col span="1" style="width:15%;">
						</colgroup>
						<thead>
							<tr>
								<th scope="col"><input type="checkbox" name="checkALL" value="" onchange="changeCheckBox(this.checked)"></th>
								<th scope="col">고객사명
								     <div class="btn_align">
										<a  href="javascript:listOrder('cust_nm','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('cust_nm','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">LTE ID
									<div class="btn_align">
										<a  href="javascript:listOrder('lte_id','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('lte_id','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">APN
									<div class="btn_align">
										<a  href="javascript:listOrder('apn','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('apn','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">등록일자
									<div class="btn_align">
										<a  href="javascript:listOrder('regist_dtm','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('regist_dtm','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">등록자
									<div class="btn_align">
										<a  href="javascript:listOrder('regist_id','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('regist_id','desc')" title="내림차순"  class="down">down</a>
									</div>								
								</th>
							</tr>
						</thead>
						<tbody id="trData" style="border:0px">
				        <c:forEach var="board" items="${list}">
				        <tr>
				            <td><input type=checkbox name="checkNo" value="${board.cust_manage_no}"></td>
				            <td><a href="#" onClick="popup_control('2','open','${board.cust_nm}');" title="${board.cust_nm}" class="pop_btn">${board.cust_nm}</a></td>
				            <td>${board.lte_id}</td>
				            <td>${board.apn}</td>
				            <td><fmt:formatDate value="${board.regist_dtm}" pattern="yyyy.MM.dd" /></td>
				            <td>${board.regist_id}</td>
				        </tr>
				
				        </c:forEach>
				         </tbody>
					</table>
				</div><!-- //table_ty -->
				
				<div class="paginate">
				    <!-- **처음페이지로 이동 : 현재 페이지가 1보다 크면  [처음]하이퍼링크를 화면에 출력-->
				    <c:if test="${commonPager.curBlock > 1}">
					<a href="#" onClick="list('1')" class="first">처음 페이지</a>
					</c:if>
				
					
					<!-- **이전페이지 블록으로 이동 : 현재 페이지 블럭이 1보다 크면 [이전]하이퍼링크를 화면에 출력 -->
				   <c:if test="${commonPager.curBlock > 1}">
					<a href="#" onClick="list('${commonPager.prevPage}')" class="prev">이전 페이지</a>
					</c:if>

					
					<ul>
						<!-- **하나의 블럭에서 반복문 수행 시작페이지부터 끝페이지까지 -->
		                <c:forEach var="num" begin="${commonPager.blockBegin}" end="${commonPager.blockEnd}">
		                    <!-- **현재페이지이면 하이퍼링크 제거 -->
		                    <c:choose>
		                        <c:when test="${num == commonPager.curPage}">
		                            <li class="select"><a href="#">${num}</a></li>
		                        </c:when>
		                        <c:otherwise>
		                            <li><a href="#" onClick="list('${num}')">${num}</a></li>
		                        </c:otherwise>
		                    </c:choose>
		                </c:forEach>					

					</ul>
					
					<!-- **다음페이지 블록으로 이동 : 현재 페이지 블럭이 전체 페이지 블럭보다 작거나 같으면 [다음]하이퍼링크를 화면에 출력 -->
	                <c:if test="${commonPager.curBlock < commonPager.totBlock}">
					<a href="#" onClick="list('${commonPager.nextPage}')" class="next">다음 페이지</a>
					</c:if>
					
					<!-- **끝페이지로 이동 : 현재 페이지가 전체 페이지보다 작거나 같으면 [끝]하이퍼링크를 화면에 출력 -->
	                <c:if test="${commonPager.curPage < commonPager.totPage}">
					<a href="#" onClick="list('${commonPager.totPage}')" class="last">마지막 페이지</a>
					</c:if>
				</div><!-- //paginate -->
				</form:form>
			</div><!-- //contents -->
		</div><!-- //contents_wrap -->
	</div><!-- //container -->
	
	<footer>
		<nav class="footer_nav">
			<ul>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">이용약관</a></li>
				<li><a href="#">개인정보처리방침</a></li>
				<li><a href="#">Sitemap</a></li>
			</ul>
		</nav><!-- //footer_nav -->
		<address>㈜케이티 대표이사 황창규 경기도 성남시 분당구 불정로 90 (정자동) 사업자등록번호 : 102-81-42945 통신판매업신고 : 2002-경기성남-0047</address>
	</footer>
	
</div><!-- //wrap -->

<div class="popup_mask"></div>

<div class="popup pop_cs_data_chagne" id="pop03">
	<div class="pop_tit">고객사 정보 변경</div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">
			
			<ul id="ulLteId" class="data_list_ty">
				<li class="li">
					<strong class="tit">고객사명</strong>
					<input type="text" name="cust_nm_mod"  readonly value="현대중공업">
				</li>
				<!-- 
				<li class="li ty_flex">
					<strong class="tit">LTE ID</strong>
					<div class="btn_list">
						<input type="text" name="lte_id_mod" value="h.ktlet.com ">
						<a href="#" onClick="addLteId()" class="btn_m btn_co03 on">추가</a>
					</div>
				</li>
				<li class="li ty_flex">
					<ul  id="ulApn" class="inp_list">
						<li class="li">
							<em class="s_tit">APN<input type="hidden" name="cust_manage_no_mod_0" value=""></em>
							<div><input type="text" name="apn_mod_0"></div>
						</li>

					</ul>
					<div class="btn_area">
						<a href="#" onClick="addApn(0)" class="btn_m btn_co03 on">추가</a>
					</div>
				</li>
				 -->
				<li class="li">
					<strong class="tit">Description</strong>
					<textarea cols="5" rows="10"  name="cust_description_mod" style="height:115px;">현대중공업 고객사 정보 변경 2017.11.11 </textarea>
				</li>
			</ul>
			<!-- //data_list_ty -->
			
		</div><!-- //scrollbar-inner -->
		
		<div class="btn_area">
			<a href="#" onClick="data_ajax_update();pop_close('#pop03');" class="btn_l btn_co01">변경</a>
			<!-- <a  href="#" onclick="data_delete();pop_close('#pop03');" class="btn_l btn_co02">삭제</a> -->
		</div>
	</div><!-- //pop_con_wrap -->
	<a href="javascript:void(0);alert(4);" class="pop_close">팝업닫기</a>
</div><!-- //popup -->


<div class="popup" id="pop03_0">
	<div class="pop_tit">고객사 신규 등록</div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">
			
			<div class="table_form vTop">
				<table>
					<caption></caption>
					<colgroup>
						<col span="1" style="width:30%;">
						<col span="1" style="width:*;">
					</colgroup>
					<tbody>

						<tr>
							<th><span>고객사명</span></th>
							<td>
							<input type="text" name="cust_nm_reg"  id="cust_nm_reg"  value="">
							</td>
						</tr>
						<tr>
							<th><span>APN</span></th>
							<td><input name="apn_reg" type="text" value=""></td>
						</tr>
						<tr>
							<th><span>LTE ID</span></th>
							<td><input name="lte_id_reg" type="text" value=""></td>
						</tr>
						<tr>
							<th><span>Description</span></th>
							<td><textarea name="cust_description_reg" cols="" rows=""></textarea></td>
						</tr>
					</tbody>	
				</table>
			</div><!-- //table_form -->
			
		</div><!-- //scrollbar-inner -->
		
		<div class="btn_area">
			<a href="#" onClick="data_write();" class="btn_m btn_co01">등록</a>
		</div>
	</div><!-- //pop_con_wrap -->
	<a href="javascript:void(0);" class="pop_close" title="close">close</a>

</div><!-- //popup -->
	
</body>
</html>
