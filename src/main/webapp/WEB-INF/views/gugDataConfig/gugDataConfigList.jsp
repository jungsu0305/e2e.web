<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content= "text/html; charset=UTF-8"> 
<meta name="format-detection" content="telephone=no">
<title>KT E2E Infra Orchestrator</title>
<link rel="shortcut icon" href="/new_resources/resources/images/layout/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="/new_resources/resources/css/style.css">
<style>
/* step_pr_ty */
.step_pr_ty {position:relative;width:875px;margin:30px 10px 10px 150px;/*border: solid 1px #000000;*/}
/*.step_pr_ty:before {position:absolute;left:68px;top:17px;width:726px;height:2px;background:#ffffff;content:"";}*/
.step_pr_ty ol {display:block;width:100%;text-align:justify;}
/*.step_pr_ty li {position:relative;display:inline-block;width:150px;text-align:center;vertical-align:top;border: solid 1px #000000;padding-top:45px}*/
.step_pr_ty li.pgc {position:relative;display:inline-block;width:90px}
.step_pr_ty li.normal {position:relative;display:inline-block;width:130px;text-align:center;vertical-align:top;/*border: solid 1px #000000;*/padding-top:45px}
.step_pr_ty li.normal i {display:block;position:absolute;z-index=2;left:80px;top:-45px;width:30px;height:30px;margin-left:-15px;background:#D9D9D9;border:3px solid #BFBFBF;border-radius:100%;box-sizing:border-box;color:#000;line-height:28px;font-weight:bold;overflow:hidden;}
.step_pr_ty li.normal .word {display:block;width:150px;margin-top:-45px;padding-top:0px;padding-left:35px;text-align:left;color:#000;font-size:14px;line-height:18px;/*border: solid 1px #000000;*/}
.step_pr_ty li.normal .word2 {display:block;width:150px;margin-top:-50px;padding-top:0px;padding-left:28px;text-align:left;color:#000;font-size:14px;line-height:18px}


.step_pr_ty li.active {position:relative;display:inline-block;width:150px;text-align:center;vertical-align:top;/*border: solid 1px #000000;*/padding-top:45px}
.step_pr_ty li.active i {display:block;position:absolute;left:85px;top:-60px;width:30px;height:30px;margin-left:-15px;background:#FF0000;border:3px solid #BFBFBF;border-radius:100%;box-sizing:border-box;color:#000;line-height:28px;font-weight:bold;overflow:hidden;}
.step_pr_ty li.active .word{display:block;width:150px;margin-top:-50px;padding-top:0px;padding-left:28px;text-align:left;color:#000;font-size:20px;line-height:18px}
.step_pr_ty li.active .word2{display:block;width:150px;margin-top:-50px;padding-top:0px;padding-left:10px;text-align:left;color:#000;font-size:20px;line-height:18px}

.step_pr_ty li.on {position:relative;display:inline-block;width:130px;text-align:center;vertical-align:top;/*border: solid 1px #000000;*/padding-top:45px}
.step_pr_ty li.on i   {display:block;position:absolute;left:80px;top:-48px;width:30px;height:30px;margin-left:-15px;background:#00B050;border:3px solid #BFBFBF;border-radius:100%;box-sizing:border-box;color:#000;line-height:28px;font-weight:bold;overflow:hidden;}
.step_pr_ty li.on .word {display:block;width:150px;margin-top:-45px;padding-top:0px;padding-left:33px;text-align:left;color:#000;font-size:14px;line-height:18px;/*border: solid 1px #000000;*/}
.step_pr_ty li.on .word2 {display:block;width:150px;margin-top:-50px;padding-top:0px;padding-left:26px;text-align:left;color:#000;font-size:14px;line-height:18px}

.step_pr_ty li.complete {position:relative;display:inline-block;width:150px;text-align:center;vertical-align:top;/*border: solid 1px #000000;*/padding-top:45px}
.step_pr_ty li.complete i {display:block;position:absolute;left:85px;top:-40px;width:30px;height:30px;margin-left:-15px;background:#00B050;border:3px solid #BFBFBF;border-radius:100%;box-sizing:border-box;color:#000;line-height:28px;font-weight:bold;overflow:hidden;}
.step_pr_ty li.complete .word{display:block;width:150px;margin-top:-50px;padding-top:0px;padding-left:28px;text-align:left;color:#000;font-size:20px;line-height:18px}
.step_pr_ty li.complete .word2{display:block;width:150px;margin-top:-50px;padding-top:0px;padding-left:10px;text-align:left;color:#000;font-size:20px;line-height:18px}

/*.step_pr_ty ol:after {display:inline-block;width:99%;height:0;content:"";}*/
.step_pr_ty li.normal .circle_text { position:absolute; margin-left:-110px; margin-top:0px; width:100px;
							 top:90px;
							 text-align:center;
							 text-valign:top;
							 line-height:20px; 
							 font-size:12px;
							 /*border: solid 1px #000000;*/}
.step_pr_ty li.active .circle_text { position:absolute; margin-left:-120px; margin-top:0px; width:100px;
							 top:95px;
							 text-align:center;
							 text-valign:top;
							 line-height:20px; 
							 font-size:14px;
							 /*border: solid 1px #000000;*/}
.step_pr_ty li.on .circle_text { position:absolute; margin-left:-110px; margin-top:0px; width:100px;
							 top:90px;
							 text-align:center;
							 text-valign:top;
							 line-height:20px; 
							 font-size:13px;
							 /*border: solid 1px #000000;*/}							 							

 
.step_pr_ty li.complete .circle_text { position:absolute; margin-left:-120px; margin-top:0px; width:100px;
							 top:70px;
							 text-align:center;
							 text-valign:top;
							 line-height:20px; 
							 font-size:14px;
							 /*border: solid 1px #000000;*/}
 
 
 .circle_normal {
  margin-top:30px;

}

 .circle_active {
  margin-top:18px;

}

 .circle_on {
  margin-top:30px;

}

 .circle_complete {
  margin-top:18px;

}


.box {padding-left:25px;width:60px; height:60px;text-valign:middle;margin:0 0 0 0;display:inline-block;/*border: solid 3px #000000;*/}

.guide {float:left;width:15px; display:inline-block;/*border: solid 3px #000000;*/}
.guide + .guide {margin-left:4px;}

.circle_progress_normal {
  margin-top:90px;
  width:15px;
  height:15px;
  background:#BFBFBF;
  border:0px solid #BFBFBF;
  border-radius:30px;
}

.circle_progress_next {
  margin-top:90px;
  width:15px;
  height:15px;
  background:#FF0000;
  border:0px solid #FF0000;
  border-radius:30px;
}
 
.triangle_right_normal { 
  margin-top:85px;
    width: 0; 
    height: 0; 
    border-top: 13px solid transparent; 
    border-bottom:13px solid transparent; 
    border-left: 26px solid #BFBFBF; 
} 

.triangle_right_next { 
  margin-top:85px;
    width: 0; 
    height: 0; 
    border-top: 13px solid transparent; 
    border-bottom:13px solid transparent; 
    border-left: 26px solid #FF0000; 
} 

 </style>

<script type="text/javascript" src="/new_resources/resources/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/template.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/circle-progress.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/raphael.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/morris.js"></script>
<script type="text/javascript" >

function onlyNumber(obj) {
        return $(obj).val().replace(/[^0-9]/g,"");

    //obj.value.length
}

function onlyDateCheck(obj) {
         //$(this).val($(this).val().replace(/^(19|20)\d{2}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[0-1])$/,""));
        var str= obj.value.replace(/[^0-9]/g,"")
         var dayRegExp = /^(19|20)\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[0-1])$/;
        if( str.match(dayRegExp)==null)
        	return false;
        else
            return true;
    	

}

//모두 체크/모두 미체크의 체크박스 처리
function changeCheckBox(flag)
{
	if(flag)
		{
		 $('input:checkbox[name="checkNo"]').each(function() {
		      this.checked = true; //checked 처리
		 });


		}else{
		
   		 $('input:checkbox[name="checkNo"]').each(function() {
		      this.checked = false; //checked 처리
		      if(this.checked){//checked 처리된 항목의 값
		            alert(this.value); 
		      }
		 });
			
		}
}

//체크된 로우 삭제처리
function submitCheckDelete()
{
	var f=document.form;
	checkBoolean=false;
	
	if (typeof(f.elements['checkNo']) != 'undefined') 
	{
		if (typeof(f.elements['checkNo'].length) == 'undefined')
		{
			if(f.elements['checkNo'].checked==true)
			{
				checkBoolean=true;
			}
		}else{
	        for (i=0; i<f.elements['checkNo'].length; i++)
	        {
	            if (f.elements['checkNo'][i].checked==true)
	            {
	            	//alert(f.elements['checkNo'][i].value);
	            	checkBoolean=true;
	            	break;
	            }
	        }
		}
        
        if(checkBoolean)
       	{
           f.action="/gugDataConfig/delete/select";
   	  	   f._method.value="DELETE"
   			f.method="POST";
           f.submit();        	
       	}else{
       		alert('선택된 내용이 없습니다.');

       	}

	}
	
}

// 정렬클릭시
    function listOrder(colName,ordOption)
    {
    	var orderKey="";
    	var commaFlag="";
    	orderKey+="(";
  		 $('input:checkbox[name="checkNo"]').each(function() {
  			orderKey+=commaFlag+this.value
  			commaFlag=",";
		 });
  		orderKey+=")";
    	//alert(orderKey);
        $.ajax({
            type: "get",
           // contentType: "json", //==> 생략가능(RestController이기때문에 가능)
            url: "gugDataConfig/jsondata?colName="+colName
            		  +"&ordOption="+ordOption+"&orderKey="+orderKey,
            success: function(result){
                	 var dataObj = JSON.parse(result);
                     var output = "";
                     for(var i in dataObj){

                    	 var myDate = new Date(dataObj[i].modify_dtm)
                    	 var yyyy= myDate.getFullYear();
                    	 var mm=String(myDate.getMonth() + 1);
                    	 var dd=String(myDate.getDate());
                    	 var h24=String(myDate.getHours());
                    	 var mi=String(myDate.getMinutes());
                    	 mm=(mm.length==1)?"0"+mm:mm;
                    	 dd=(dd.length==1)?"0"+dd:dd;
                    	 h24=(h24.length==1)?"0"+h24:h24;     
                    	 mi=(mi.length==1)?"0"+mi:mi;
                    	 var date = yyyy  + "/" +  mm+ "/" + dd + " " + h24 + ":" + mi;
                    	 //var date = yyyy  + "." +  mm+ "." + dd;
                         output += "<tr>";
                         output += "<td><input type=checkbox name='checkNo' value='" + dataObj[i].gug_data_manage_no +"'></td>";
                         output += "<td><a href=\"/gugDataConfig/view/" +  dataObj[i].gug_data_manage_no + "/1\" onClick=\"\" title=\"" + dataObj[i].profile_nm+ "\" class=\"pop_btn\">" 
                                      + dataObj[i].profile_nm +"</a></td>";
                         output += "<td>" + dataObj[i].lte_id +"</td>";
                         output += "<td>" + dataObj[i].cust_nm +"</td>";
                         output += "<td>" + dataObj[i].work_state_nm +"</td>";
                         output += "<td>" + dataObj[i].work_ept_dt +" " + dataObj[i].work_ept_start_time + "</td>";
                         output += "<td>" + date +"</td>";                
                         output += "<td>" + ((typeof(dataObj[i].regist_id)=="undefined")?"":dataObj[i].regist_id) +"</td>";
                         if(dataObj[i].work_state_cd=="D060"){
            
                         	output += "<td><a href=\"#\" onClick=\"alert('삭제된 작업파일입니다.')\" class=\"btn_s btn_co03 off\">확인내역</a></td>";
                         }else{
							output += "<td><a href=\"#\" onClick=\"progress_pop('" +dataObj[i].gug_data_manage_no +"','" +dataObj[i].gug_data_history_no +"','" 
									+dataObj[i].work_state_cd+"','" +dataObj[i].designer_aprv_yn + "');\" class=\"btn_s btn_co03 on pop_btn\">확인내역</a></td>";                        	 
                         }
                         output += "<tr>";
                         
                     }
                     output += "";

                $("#trData").html(output);
            }
        });
    }
    
    
    
    // 국데이터 복사
    function dataCopy()
    {
     	var f=document.form;
    	var copyKey="";
    	var cnt=0	;
  		 $('input:checkbox[name="checkNo"]').each(function() {
  			 if(this.checked){
  				copyKey=this.value;
  				cnt=cnt+1;
  			 }
  			if(1 < cnt )
			{
			    return false;
			}

  			
		 });
  		 
		if(cnt <= 0 || 1 < cnt )
		{
 				alert("국데이타 복사를 위해서 첵크박스 하나만 체크하세요.");
			
		}else{
        	location.href="gugDataConfig/copy/"+copyKey;
			
		}

     	
     
    }
    
 
    
    function list(page){
    	var f=document.form;
        location.href="gugDataConfig?curPage="+page+"&searchOption=${searchOption}" +
        		             "&keyword_fdate=${keyword_fdate}&keyword_tdate=${keyword_tdate}"+
        		             "&searchOption2=${searchOption2}&keyword2=${keyword2}"+
        		             "&work_state_cd=${work_state_cd}";
    }
    
    function list_state(state_gubun){
    	var f=document.form;
    	location.href="gugDataConfig?curPage=1" +
        "&work_state_cd="+state_gubun;
       /* location.href="gugDataConfig?curPage=1&searchOption=${searchOption}" +
        		             "&keyword_fdate=${keyword_fdate}&keyword_tdate=${keyword_tdate}"+
        		             "&searchOption2=${searchOption2}&keyword2=${keyword2}" +
        		             "&work_state_cd="+state_gubun; */
    }
    
    
    function searchData(){
    	var f=document.form;
    	var searchOption=f.searchOption.value;
    	var keyword_fdate=onlyNumber(f.keyword_fdate);   
    	var keyword_tdate=onlyNumber(f.keyword_tdate);
    	var searchOption2=f.searchOption2.value;
    	var keyword2=f.keyword2.value;
    	if(onlyDateCheck(f.keyword_fdate)!=true && keyword_fdate !="")
    	{
    		alert("유효한 날짜가 아닙니다.")
    		f.keyword_fdate.focus()
    		return false;
    	
    	}
    	if(onlyDateCheck(f.keyword_tdate)!=true && keyword_tdate !="")
    	{
    		alert("유효한 날짜가 아닙니다.")
    		f.keyword_tdate.focus()
    		return false;
    	
    	}
    	
    	//alert(searchOption +"--"+keyword);
        location.href="gugDataConfig?curPage=1&searchOption=" + searchOption +"&keyword_fdate=" +keyword_fdate
                     +"&keyword_tdate=" +keyword_tdate+"&searchOption2=" +searchOption2+"&keyword2=" +keyword2;
    }
    

    function progress_pop(gug_data_manage_no, gug_data_history_no, stat_cd,designer_aprv_yn)
    {
    	$("input[name='gug_data_manage_no']").val(gug_data_manage_no);
    	$("input[name='gug_data_history_no']").val(gug_data_history_no);
    	
    	if(stat_cd=="D060")
    	{
    		alert("삭제된 작업파일입니다.");
    		return;
    	
    	}else if(stat_cd=="D010" || stat_cd=="D020" || stat_cd=="D030"){
    		
    		
    	    pop_open('#pop_first_progress');
    	    select_first_progress(gug_data_manage_no,1);
    	    
    	}else if(stat_cd=="D040"){
    		
    		if(designer_aprv_yn=="Y")
   			{
	       	    pop_open('#pop_exec_progress');
	       		select_exec_progress(gug_data_manage_no, gug_data_history_no);
   			}else{
		   	    pop_open('#pop_ready_progress');
		   	    select_ready_progress(gug_data_manage_no, gug_data_history_no);
   			}
    	}else if(stat_cd=="D050"){
    		pop_open('#pop_last_progress');
    		select_last_progress(gug_data_manage_no, gug_data_history_no);
    		
    	}

    }
    
    //설계자 승인
    function approval_update()
    {
    	$.ajax({
    		type : "POST",
    		url : "/gugDataConfig/design_approval",
    		//contentType:"application/x-www-form-urlencoded;charset=utf-8", //한글 깨짐 방지
    		data : {
    			gug_data_manage_no : $("input[name='gug_data_manage_no']").val(),
    			gug_data_history_no  : $("input[name='gug_data_history_no']").val(), 
    			designer_aprv_yn : "Y"
    		 
    		},
    		dataType : "text",
    		cache: false, 
    		success : function(result) { 			 
                alert("작업 승인을 완료했습니다.");
                pop_close('#pop_ready_progress');
                listOrder('','');
            /*
                var jsonObj = JSON.parse(result);
    			alert(jsonObj.model.alarm_cd); */

    		//	alarmGrid.load(result,"json")		
    		//	alarmGrid.load("${alarmJSON}","json")		
    		//	alarmGrid.load("../common/data.json","json");
    		//	alarmGrid.loadXMLString(result);			
    		},
    		error : function(result, status, err) {
    			alert('작업 승인을 실패했습니다');
    		},
    	 	beforeSend: function() {
    	 		//myLayout.cells("b").progressOn();
    		},		
    		complete : function() {
    			//myLayout.cells("b").progressOff();
    		}
    	});

    }
  
    //작업완료 확인
    function work_complete_update()
    {
    	$.ajax({
    		type : "POST",
    		url : "/gugDataConfig/work_complete",
    		//contentType:"application/x-www-form-urlencoded;charset=utf-8", //한글 깨짐 방지
    		data : {
    			gug_data_manage_no : $("input[name='gug_data_manage_no']").val(),
    			gug_data_history_no  : $("input[name='gug_data_history_no']").val(), 
    			work_state_cd : "D050"
    		 
    		},
    		dataType : "text",
    		cache: false, 
    		success : function(result) { 			 
                alert("작업완료확인을 성공했습니다..");
                
                pop_close('#pop_exec_progress');
                searchData();
                //listOrder('','');
            /*
                var jsonObj = JSON.parse(result);
    			alert(jsonObj.model.alarm_cd); */

    		//	alarmGrid.load(result,"json")		
    		//	alarmGrid.load("${alarmJSON}","json")		
    		//	alarmGrid.load("../common/data.json","json");
    		//	alarmGrid.loadXMLString(result);			
    		},
    		error : function(result, status, err) {
    			alert('작업완료확인을 실패했습니다');
    		},
    	 	beforeSend: function() {
    	 		//myLayout.cells("b").progressOn();
    		},		
    		complete : function() {
    			//myLayout.cells("b").progressOff();
    		}
    	});

    }
    


    function select_first_progress(gug_data_manage_no,page_no)
    {
        $.ajax({
            type: "get",
           // contentType: "json", //==> 생략가능(RestController이기때문에 가능)
            url: "/gugDataConfig/list_his?gug_data_manage_no="+gug_data_manage_no+"&curPage="+page_no ,
            success: function(result){
                	 var dataObj = JSON.parse(result);
                     var output="";
                     $("strong[id='first_state_name']").html(dataObj.work_state_nm + "<i></i>"); 
                     $("div[id='first_title_profile_nm']").text("진행현황_" + dataObj.profile_nm);
                     $("strong[id='first_profile_nm']").text(dataObj.profile_nm);
                     $("strong[id='first_work_ept_dt']").text(dataObj.work_ept_dt + " " + dataObj.work_ept_start_time );

					var Oval = dataObj.list_his;
                     for(var i in Oval){
						var modify_dtm="";
						if(typeof(Oval[i].modify_dtm)=="undefined")
						{
							modify_dtm="";
	                     }else{
	                    	 var myDate = new Date(Oval[i].modify_dtm)
	                    	 var yyyy= myDate.getFullYear();
	                    	 var mm=String(myDate.getMonth() + 1);
	                    	 var dd=String(myDate.getDate());
	                    	 var h24=String(myDate.getHours());
	                    	 var mi=String(myDate.getMinutes());
	                    	 mm=(mm.length==1)?"0"+mm:mm;
	                    	 dd=(dd.length==1)?"0"+dd:dd;
	                    	 h24=(h24.length==1)?"0"+h24:h24;     
	                    	 mi=(mi.length==1)?"0"+mi:mi;
	                    	 modify_dtm = yyyy  + "." +  mm+ "." + dd + " " + h24 + ":" + mi;
	                    	 //var date = yyyy  + "." +  mm+ "." + dd;
	                     }
						var change_reason_full=(typeof(Oval[i].change_reason)=="undefined")?"":Oval[i].change_reason;
						var change_reason="";
						if(change_reason_full.length >5)
						{
							change_reason=change_reason_full.substr(0,4);
						}else{
							change_reason=change_reason_full;
						}
						
						
                         output += "<tr>";
                         output += "<td>" + ((typeof(Oval[i].work_state_nm)=="undefined")?"":Oval[i].work_state_nm) + "</td>";
                         output += "<td>" +modify_dtm+"</td>";
                         output += "<td>" + ((typeof(Oval[i].modify_id)=="undefined")?"":Oval[i].modify_id) +"</td>";
                         output += "<td>" + ((typeof(Oval[i].chng_col_nm)=="undefined")?"":Oval[i].chng_col_nm) +"</td>";
                         output += "<td>" + ((typeof(Oval[i].prev_col_val)=="undefined")?"":Oval[i].prev_col_val) +"</td>";
                         output += "<td>" + ((typeof(Oval[i].next_col_val)=="undefined")?"":Oval[i].next_col_val) + "</td>";
                         output += "<td><span title=\"" +change_reason_full +"\">" + change_reason +"</td>";                
                       
                         output += "<tr>";
                         
                         
                     }
                     output += "";

                $("#firstTrData").html(output);
                
                var paging_html="";
			   if( dataObj.commonPager.curBlock > 1){
				   paging_html+="<a href=\"#\" onClick=\"select_first_progress(" +gug_data_manage_no +",'1')\" class=\"first\">처음 페이지</a>";
			   }
			
			   if( dataObj.commonPager.curBlock > 1){
				   paging_html+="<a href=\"#\" onClick=\"select_first_progress(" +gug_data_manage_no +",'" + dataObj.commonPager.prevPage +"')\" class=\"prev\">이전 페이지</a>";
			   }
				
			   paging_html+="<ul>";
			   for(var i=dataObj.commonPager.blockBegin;i<=dataObj.commonPager.blockEnd;i++ )
			   {
			   		if(i==dataObj.commonPager.curPage)
		   			{
		   				paging_html+="<li class=\"select\"><a href=\"#\">" + i + "</a></li>";
		   			}else{
		   				paging_html+="<li><a href=\"#\" onClick=\"select_first_progress(" +gug_data_manage_no +",'" + i + "')\">" + i +"</a></li>";
		   			}
			   }

			   paging_html+="</ul>";
				
			   if( dataObj.commonPager.curBlock < dataObj.commonPager.totBlock){
				   paging_html+="<a href=\"#\" onClick=\"select_first_progress(" +gug_data_manage_no +",'" + dataObj.commonPager.nextPage + "')\" class=\"next\">다음 페이지</a>";
			   }
				
			   if( dataObj.commonPager.curPage < dataObj.commonPager.totPage){
				   paging_html+="<a href=\"#\" onClick=\"select_first_progress(" +gug_data_manage_no +",'" + dataObj.commonPager.totPage + "')\" class=\"last\">마지막 페이지</a>";
			   }
               $("#first_paginate").html(paging_html);
               var active_value=0;
               if(dataObj.work_state_nm=="입력중")
               {
            	   active_value=0.3;
               }else if(dataObj.work_state_nm=="변경중"){
            	   active_value=0.6;
               }else if(dataObj.work_state_nm=="입력완료"){
            	   active_value=1;
            	   $("div[id='first_pgc_circle_1']").attr("class","circle_progress_next");
                   $("div[id='first_pgc_triangle_1']").attr("class","triangle_right_next");
                   
                   
               }
               
               $(".step_pr_ty li.active .circle_text").css("top","95px");
               $(".step_pr_ty li.active i").css("top","-60px");
               
               circle_progress(active_value);
            }
        });

    }


    function select_ready_progress(gug_data_manage_no,gug_data_history_no)
    {
    	
        $.ajax({
            type: "get",
           // contentType: "json", //==> 생략가능(RestController이기때문에 가능)
			url: "/gugDataConfig/list_cmd_ready?"+"curPage=1&gug_data_manage_no="+gug_data_manage_no +
			        "&gug_data_history_no="+ gug_data_history_no
			        ,
            success: function(result){
                	 var dataObj = JSON.parse(result);
                     var output="";
                     $("strong[id='ready_state_name']").html(dataObj.work_state_nm +"("+ dataObj.count+ "/" + dataObj.count + ")<i></i>"); //작업대기(3/6)
                     $("strong[id='ready_exec_state_name']").text("작업예정(" + dataObj.work_ept_dt+ ")"); //작업예정(201.12.25)
                     
			         $("div[id='ready_title_profile_nm']").text("진행현황_" + dataObj.profile_nm);
			         $("strong[id='ready_profile_nm']").text(dataObj.profile_nm);
			         $("strong[id='ready_work_ept_dt']").text(dataObj.work_ept_dt + " " + dataObj.work_ept_start_time );

					var Oval = dataObj.list_his;
                     for(var i in Oval){
                    	 var myDate = new Date(Oval[i].create_dtm)
                    	 var yyyy= myDate.getFullYear();
                    	 var mm=String(myDate.getMonth() + 1);
                    	 var dd=String(myDate.getDate());
                    	 var h24=String(myDate.getHours());
                    	 var mi=String(myDate.getMinutes());
                    	 mm=(mm.length==1)?"0"+mm:mm;
                    	 dd=(dd.length==1)?"0"+dd:dd;
                    	 h24=(h24.length==1)?"0"+h24:h24;     
                    	 mi=(mi.length==1)?"0"+mi:mi;
                    	 var date = yyyy  + "." +  mm+ "." + dd + " " + h24 + ":" + mi;
                    	 //var date = yyyy  + "." +  mm+ "." + dd;
 
                         output += "<tr>";
                         output += "<td>" + Oval[i].equip_name+ "</td>";
                         output += "<td>" + "확인"+"</td>";
                         output += "<td>" + date +"</td>";  //확인일자
                         output += "<td>" + ((typeof(Oval[i].modify_id)=="undefined")?"":Oval[i].modify_id) +"</td>";  //운용자아이디               
                       
                         output += "<tr>";
                         
                     }
                     output += "";

                $("#readyTrData").html(output);
                var active_value=dataObj.count / dataObj.count;
                if(active_value==1)
                {
                 	$("div[id='ready_pgc_circle_2']").attr("class","circle_progress_next");
                    $("div[id='ready_pgc_triangle_2']").attr("class","triangle_right_next");   	
                }

                $(".step_pr_ty li.active .circle_text").css("top","95px");
                $(".step_pr_ty li.active i").css("top","-60px");
                
                circle_progress(active_value);
            }
        });

    }
    

    function select_exec_progress(gug_data_manage_no,gug_data_history_no)
    {
    	
    	
        $.ajax({
            type: "get",
			url: "/gugDataConfig/list_cmd_exec?"+"curPage=1&gug_data_manage_no="+gug_data_manage_no +
			        "&gug_data_history_no="+ gug_data_history_no
			        ,
            success: function(result){
                	 var dataObj = JSON.parse(result);
                     var output="";
			         $("div[id='exec_title_profile_nm']").text("진행현황_" + dataObj.profile_nm);
                     $("strong[id='exec_state_name']").html("확인완료("+ dataObj.count+ "/" + dataObj.count + ")<i></i>"); //확인완료(3/6)

			         $("strong[id='exec_profile_nm']").text(dataObj.profile_nm);
			         $("strong[id='exec_work_ept_dt']").text(dataObj.work_ept_dt + " " + dataObj.work_ept_start_time );
                    var exec_cnt=0;
					var Oval = dataObj.list_his;
                     for(var i in Oval){
                    	 var myDate = new Date(Oval[i].create_dtm)
                    	 var yyyy= myDate.getFullYear();
                    	 var mm=String(myDate.getMonth() + 1);
                    	 var dd=String(myDate.getDate());
                    	 var h24=String(myDate.getHours());
                    	 var mi=String(myDate.getMinutes());
                    	 mm=(mm.length==1)?"0"+mm:mm;
                    	 dd=(dd.length==1)?"0"+dd:dd;
                    	 h24=(h24.length==1)?"0"+h24:h24;     
                    	 mi=(mi.length==1)?"0"+mi:mi;
                    	 var date = yyyy  + "." +  mm+ "." + dd + " " + h24 + ":" + mi;
                    	 var exec_date="";
                    	 if((typeof(Oval[i].exec_start_dtm)=="undefined"))
                   		 {
                   		 	 exec_date="-";
                   		 }else{
                   			exec_cnt++;
                   			 var myDate = new Date(Oval[i].exec_start_dtm)
	                       	 var yyyy= myDate.getFullYear();
	                       	 var mm=String(myDate.getMonth() + 1);
	                       	 var dd=String(myDate.getDate());
	                       	 var h24=String(myDate.getHours());
	                       	 var mi=String(myDate.getMinutes());
	                       	 mm=(mm.length==1)?"0"+mm:mm;
	                       	 dd=(dd.length==1)?"0"+dd:dd;
	                       	 h24=(h24.length==1)?"0"+h24:h24;     
	                       	 mi=(mi.length==1)?"0"+mi:mi;
	                       	 exec_date = yyyy  + "." +  mm+ "." + dd + " " + h24 + ":" + mi;
                   		 }
                    	 
                    	 //var date = yyyy  + "." +  mm+ "." + dd;
 
                         output += "<tr>";
                         output += "<td>" + Oval[i].equip_name + "</td>";
                         output += "<td>" + Oval[i].exec_state_nm +"</td>";
                         output += "<td>" + exec_date +"</td>";  //실행일자
                         output += "<td>" + ((typeof(Oval[i].execter_id)=="undefined")?"":Oval[i].execter_id) +"</td>";  //운용자아이디               
                       
                         output += "<tr>";
                         
                     }
                     output += "";

                $("#execTrData").html(output);
                if(exec_cnt==dataObj.count)
               	{
                	$("strong[id='exec_exec_state_name']").html("명령실행완료(" + exec_cnt + "/"
	            		    + dataObj.count+ ")<i></i>"); //명령실행중(6/6)
               	
               	}else{
	                $("strong[id='exec_exec_state_name']").html("명령실행중(" + exec_cnt + "/"
	            		    + dataObj.count+ ")<i></i>"); //명령실행중(3/6)
               	}
                var active_value=exec_cnt / dataObj.count;
                if(active_value==1)
                {
                 	$("div[id='exec_pgc_circle_3']").attr("class","circle_progress_next");
                    $("div[id='exec_pgc_triangle_3']").attr("class","triangle_right_next");   
                    $("a[id='work_complete_btn']").attr("class","btn_l btn_co01 on");   	
                    $("a[id='work_complete_btn']").attr("onClick","work_complete_update();");   

                }else{
                 	$("div[id='exec_pgc_circle_3']").attr("class","circle_progress_normal");
                    $("div[id='exec_pgc_triangle_3']").attr("class","triangle_right_normal");   
                    $("a[id='work_complete_btn']").attr("class","btn_l btn_co01 off");  
                    $("a[id='work_complete_btn']").attr("onClick","alert('명령실행이 완료상태가 아니므로 작업완료 확인을 할 수 없습니다.');");   
                	
                }
                

                $(".step_pr_ty li.active .circle_text").css("top","80px");
                $(".step_pr_ty li.active i").css("top","-50px");
                circle_progress(active_value);
                
            }
        });

    }    
    

    function select_last_progress(gug_data_manage_no,gug_data_history_no)
    {
    	
    	
        $.ajax({
            type: "get",
			url: "/gugDataConfig/list_cmd_exec?"+"curPage=1&gug_data_manage_no="+gug_data_manage_no +
			        "&gug_data_history_no="+ gug_data_history_no
			        ,
            success: function(result){
                	 var dataObj = JSON.parse(result);
                     var output="";last_state_name
			         $("div[id='last_title_profile_nm']").text("진행현황_" + dataObj.profile_nm);
                     $("strong[id='last_state_name']").html("확인완료("+ dataObj.count+ "/" + dataObj.count + ")<i></i>"); //확인완료(3/6)

			         $("strong[id='last_profile_nm']").text(dataObj.profile_nm);
			         $("strong[id='last_work_ept_dt']").text(dataObj.work_ept_dt + " " + dataObj.work_ept_start_time );
                    var exec_cnt=0;
					var Oval = dataObj.list_his;
                     for(var i in Oval){
                    	 var myDate = new Date(Oval[i].create_dtm)
                    	 var yyyy= myDate.getFullYear();
                    	 var mm=String(myDate.getMonth() + 1);
                    	 var dd=String(myDate.getDate());
                    	 var h24=String(myDate.getHours());
                    	 var mi=String(myDate.getMinutes());
                    	 mm=(mm.length==1)?"0"+mm:mm;
                    	 dd=(dd.length==1)?"0"+dd:dd;
                    	 h24=(h24.length==1)?"0"+h24:h24;     
                    	 mi=(mi.length==1)?"0"+mi:mi;
                    	 var date = yyyy  + "." +  mm+ "." + dd + " " + h24 + ":" + mi;
                    	 var exec_date="";
                    	 if((typeof(Oval[i].exec_start_dtm)=="undefined"))
                   		 {
                   		 	 exec_date="-";
                   		 }else{
                   			exec_cnt++;
                   			 var myDate = new Date(Oval[i].exec_start_dtm)
	                       	 var yyyy= myDate.getFullYear();
	                       	 var mm=String(myDate.getMonth() + 1);
	                       	 var dd=String(myDate.getDate());
	                       	 var h24=String(myDate.getHours());
	                       	 var mi=String(myDate.getMinutes());
	                       	 mm=(mm.length==1)?"0"+mm:mm;
	                       	 dd=(dd.length==1)?"0"+dd:dd;
	                       	 h24=(h24.length==1)?"0"+h24:h24;     
	                       	 mi=(mi.length==1)?"0"+mi:mi;
	                       	 exec_date = yyyy  + "." +  mm+ "." + dd + " " + h24 + ":" + mi;
                   		 }
                    	 
                    	 //var date = yyyy  + "." +  mm+ "." + dd;
 
                         output += "<tr>";
                         output += "<td>" + Oval[i].equip_name + "</td>";
                         output += "<td>" + Oval[i].exec_state_nm +"</td>";
                         output += "<td>" + exec_date +"</td>";  //실행일자
                         output += "<td>" + ((typeof(Oval[i].execter_id)=="undefined")?"":Oval[i].execter_id) +"</td>";  //운용자아이디               
                       
                         output += "<tr>";
                         
                     }
                     output += "";

                $("#lastTrData").html(output);
                if(exec_cnt==dataObj.count)
               	{
                	$("strong[id='last_exec_name']").html("명령실행완료(" + exec_cnt + "/"
	            		    + dataObj.count+ ")<i></i>"); //명령실행중(6/6)
               	
               	}else{
	                $("strong[id='last_exec_name']").html("명령실행중(" + exec_cnt + "/"
	            		    + dataObj.count+ ")<i></i>"); //명령실행중(3/6)
               	}
                

                var myDate = new Date(dataObj.work_comp_dt)
           	 var yyyy= myDate.getFullYear();
           	 var mm=String(myDate.getMonth() + 1);
           	 var dd=String(myDate.getDate());
           	 var h24=String(myDate.getHours());
           	 var mi=String(myDate.getMinutes());
           	 mm=(mm.length==1)?"0"+mm:mm;
           	 dd=(dd.length==1)?"0"+dd:dd;
           	 h24=(h24.length==1)?"0"+h24:h24;     
           	 mi=(mi.length==1)?"0"+mi:mi;
           	 var comp_date = yyyy  + "." +  mm+ "." + dd + " " + h24 + ":" + mi;
             $("strong[id='last_work_complete_name']").html(comp_date + "<br>완료확인<i></i>"); //명령실행중(3/6)
             circle_progress();
            }
        });

    }    
    

function circle_progress(active_value)
{
	$('.circle_on').circleProgress({      //들어갈 div class명을 넣어주세요
		value: 1,    //진행된 수를 넣어주세요. 1이 100기준입니다.
		size: 110,       //도넛의 크기를 결정해줍니다.
		thickness:2,
		fill: {
		 gradient: ["red", "orange"]  //도넛의 색을 결정해줍니다.
		 }
		 }).on('circle-animation-progress', function(event, progress) {    //라벨을 넣어줍니다.
		 //$(this).find('strong').html(parseInt(100 * 0.3) + '%<i></i>');
		 });
	$('.circle_active').circleProgress({      //들어갈 div class명을 넣어주세요
		value: active_value,    //진행된 수를 넣어주세요. 1이 100기준입니다.
		size: 140,       //도넛의 크기를 결정해줍니다.
		thickness:3,
		fill: {
		 gradient: ["red", "orange"]  //도넛의 색을 결정해줍니다.
		 }
		 }).on('circle-animation-progress', function(event, progress) {    //라벨을 넣어줍니다.
		// $(this).find('strong').html("<i></i>");
		 });
	$('.circle_normal').circleProgress({      //들어갈 div class명을 넣어주세요
		value: 1,    //진행된 수를 넣어주세요. 1이 100기준입니다.
		size: 110,       //도넛의 크기를 결정해줍니다.
		thickness:2,
		animation: true,
		fill: {
		 gradient: ["grey", "grey"]  //도넛의 색을 결정해줍니다.
		 }
		 }).on('circle-animation-progress', function(event, progress) {    //라벨을 넣어줍니다.
		 //$(this).find('strong').html(parseInt(100 * 1) + '%<i></i>');
		 });
	$('.circle_complete').circleProgress({      //들어갈 div class명을 넣어주세요
		value: 1,    //진행된 수를 넣어주세요. 1이 100기준입니다.
		size: 140,       //도넛의 크기를 결정해줍니다.
		thickness:3,
		fill: {
		 gradient: ["red", "orange"]  //도넛의 색을 결정해줍니다.
		 }
		 }).on('circle-animation-progress', function(event, progress) {    //라벨을 넣어줍니다.
		// $(this).find('strong').html("<i></i>");
		 });	
	
}
    // pop_close('#pop_exec_progress');
    //pop_first_progress
    //pop_ready_progress    
    //pop_exec_progress
    //pop_last_progress    

    

</script>

</head>

<body>
<div id="wrap">
    
    <header>
		<h1 class="logo"><a href="#">KT E2E Infra Orchestrator</a></h1>
		<nav id="gnb">
			<ul>
				<li><a href="/oneView/list">DESIGN</a></li>
				<li><a href="/provisioning">PROVISIONING</a></li>
				<li><a href="#">MONITORING</a></li>
				<li><a href="#">INVENTORY</a></li>
				<li><a href="#">CONFIGURATION</a></li>
				<li class="active"><!-- 활성화 class="active" -->
					<a href="#">P-LTE</a>
					<ul>
						<li class="active"><a href="/gugDataConfig">국데이터 관리</a></li><!-- 활성화 class="active" -->
						<li><a href="/gugPcmdCrExec">명령 실행</a></li>		
						<li><a href="/indPcmdCrExec">운용자 명령어</a></li>
						<li><a href="/cust">고객사 관리</a></li>
						<li><a href="/equipInven">장비 관리</a></li>
						<li><a href="#">모니터링</a></li>
						<li><a href="#">이력조회</a></li>
					</ul>
				</li>
			</ul>
		</nav><!-- //gnb -->
		<div class="ad_info">
			<i><img src="/new_resources/resources/images/layout/img_ad_info.jpg" alt=""></i><!-- image size : 25*25px -->
			<p>admin</p>
		</div><!-- //ad_info -->
    </header>
    
    <div id="container">	
		
		<ul id="navigation">
			<li><img src="/new_resources/resources/images/layout/img_navigation_home.png" alt="home"></li>
			<li>국데이터 조회</li>
		</ul><!-- //navigation -->
		
		<div id="contents_wrap">
			<div class="contents">
			 <form:form commandName="GugDataConfigComponent" name="form" action="" onsubmit="return false;">
			 <input type="hidden" name="_method" value=""><!-- RESTFUL 메소드 오버라이드를 위해  -->
			 <input type="hidden" name="gug_data_manage_no" value="0"><!-- 숫자형이므로 0 초기값 줘야함  -->
			 <input type="hidden" name="gug_data_history_no" value="0"><!--숫자형이므로 0 초기값 줘야함  -->
				<div class="search_area">
					<div class="inner">
						<table>
							<caption>검색 테이블</caption>
							<colgroup>
								<col span="1" style="width:15%;">
								<col span="1" style="width:*;">
							</colgroup>
							<tbody>
								<tr>
									<td>
										<select name="searchOption">
									     	<option value="modify_dtm" <c:if test='${searchOption=="modify_dtm"}'> selected </c:if> >최종변경일</option>
	    	                                <option value="work_ept_dt" <c:if test='${searchOption=="work_ept_dt"}'> selected </c:if> >작업예정일</option>
 									    </select>	
									</td>
									<td class="date">
										<div class="date_area">
											<input type="text" name="keyword_fdate"  value="${keyword_fdate}" class="select_date">
											<span class="b_br">~</span>
											<input type="text" name="keyword_tdate"  value="${keyword_tdate}"   class="select_date">
										</div>
									</td>
								</tr>
								<tr>
									<td>
									<select name="searchOption2">
								     	<option value="profile_nm" <c:if test='${searchOption2=="profile_nm"}'> selected </c:if> >파일명</option>
    	                                <option value="lte_id" <c:if test='${searchOption2=="lte_id"}'> selected </c:if> >LTE ID</option>
								     	<option value="cust_nm" <c:if test='${searchOption2=="cust_nm"}'> selected </c:if> >고객사</option>
    	                                <option value="regist_id" <c:if test='${searchOption2=="regist_id"}'> selected </c:if> >등록자</option>									
									</select>

									</td>
									<td><input type="text" name="keyword2"  value="${keyword2}"></td>
								</tr>
							</tbody>	
						</table>
						<div class="btn_area">
							<a href="#" onClick="searchData()" class="btn_l btn_co01 on ty02">조회하기</a>
						</div>
					</div>
				</div><!-- //search_area -->
				
				<div class="table_ty">
					<div class="table_head">
						<div class="count">Total Count <span>${count}</span></div>
						<ul class="status_board">
							<li><a href="#" onClick="list_state('D010')">입력중 <strong>${summaryItem.summary_type_val_1}</strong></a></li>
							<li><a href="#" onClick="list_state('D030')">변경중 <strong>${summaryItem.summary_type_val_3}</strong></a></li>
							<li><a href="#" onClick="list_state('D020')">입력완료 <strong>${summaryItem.summary_type_val_2}</strong></a></li>
							<li><a href="#" onClick="list_state('D060')">삭제 <strong>${summaryItem.summary_type_val_6}</strong></a></li>
							<li><a href="#" onClick="list_state('D040')">작업대기 <strong>${summaryItem.summary_type_val_4}</strong></a></li>
							<li><a href="#" onClick="list_state('D050')">작업완료 <strong>${summaryItem.summary_type_val_5}</strong></a></li>
						</ul>
						 <div class="btn_area">
							<a href="/gugDataConfig/writeView" class="btn_l btn_co01">신규등록</a>
							<a href="#" onClick="dataCopy()" class="btn_l btn_co01">국데이터 복사</a>
							<a href="#" onClick="submitCheckDelete()" class="btn_l btn_co02">선택삭제</a>
						</div>
					</div>
					<table>
						<caption>목록 테이블</caption>
						<colgroup>
							<col span="1" style="width:5%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:15%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:12%;">
							<col span="1" style="width:12%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:10%;">
						</colgroup>
						<thead>
							<tr>
								<th scope="col"><input type="checkbox"  name="checkALL" value="" onchange="changeCheckBox(this.checked)"></th>
								<th scope="col">
									파일명
									<div class="btn_align">
										<a  href="javascript:listOrder('profile_nm','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('profile_nm','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">
									LTE ID
									<div class="btn_align">
										<a  href="javascript:listOrder('lte_id','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('lte_id','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">
									고객사
									<div class="btn_align">
										<a  href="javascript:listOrder('cust_nm','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('cust_nm','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">
									상태
									<div class="btn_align">
										<a  href="javascript:listOrder('work_state_nm','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('work_state_nm','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">작업(예정)일</th>
								<th scope="col">
									최종변경일
									<div class="btn_align">
										<a  href="javascript:listOrder('modify_dtm','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('modify_dtm','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">
									등록자
									<div class="btn_align">
										<a  href="javascript:listOrder('regist_id','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('regist_id','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">운용자확인</th>
							</tr>
						</thead>
						<tbody id="trData" style="border:0px">
				        <c:forEach var="board" items="${list}">
							<tr>
								<td><input type="checkbox" name="checkNo" value="${board.gug_data_manage_no}"></td>
								<td><a href="/gugDataConfig/view/${board.gug_data_manage_no}/1" onclick="" title="${board.profile_nm}">${board.profile_nm}</a></td>
								<td>${board.lte_id}</td>
								<td>${board.cust_nm}</td>
								<td>${board.work_state_nm} </td>
								<td>${board.work_ept_dt} ${board.work_ept_start_time}</td>
								<td><fmt:formatDate value="${board.modify_dtm}" pattern="yyyy.MM.dd HH:mm:ss" /></td>
								<td>${board.regist_id}</td>
								<c:choose>
							    <c:when test="${board.work_state_cd eq 'D060'}">
								<td><a href="#" onClick="alert('삭제된 작업파일입니다.')"  class="btn_s btn_co03 off">진행현황</a></td>

							    </c:when>
								<c:otherwise>
								<td><a href="#" onClick="progress_pop('${board.gug_data_manage_no}','${board.gug_data_history_no}','${board.work_state_cd}','${board.designer_aprv_yn}');" class="btn_s btn_co03 on pop_btn">진행현황</a></td>

							    </c:otherwise>
							</c:choose>
					

							</tr>
						 </c:forEach>
				         </tbody>	
						
						</tbody>
					</table>
				</div><!-- //table_ty -->
				
				<div class="paginate">
				    <!-- **처음페이지로 이동 : 현재 페이지가 1보다 크면  [처음]하이퍼링크를 화면에 출력-->
				    <c:if test="${commonPager.curBlock > 1}">
					<a href="#" onClick="list('1')" class="first">처음 페이지</a>
					</c:if>
				
					
					<!-- **이전페이지 블록으로 이동 : 현재 페이지 블럭이 1보다 크면 [이전]하이퍼링크를 화면에 출력 -->
				   <c:if test="${commonPager.curBlock > 1}">
					<a href="#" onClick="list('${commonPager.prevPage}')" class="prev">이전 페이지</a>
					</c:if>

					
					<ul>
						<!-- **하나의 블럭에서 반복문 수행 시작페이지부터 끝페이지까지 -->
		                <c:forEach var="num" begin="${commonPager.blockBegin}" end="${commonPager.blockEnd}">
		                    <!-- **현재페이지이면 하이퍼링크 제거 -->
		                    <c:choose>
		                        <c:when test="${num == commonPager.curPage}">
		                            <li class="select"><a href="#">${num}</a></li>
		                        </c:when>
		                        <c:otherwise>
		                            <li><a href="#" onClick="list('${num}')">${num}</a></li>
		                        </c:otherwise>
		                    </c:choose>
		                </c:forEach>					

					</ul>
					
					<!-- **다음페이지 블록으로 이동 : 현재 페이지 블럭이 전체 페이지 블럭보다 작거나 같으면 [다음]하이퍼링크를 화면에 출력 -->
	                <c:if test="${commonPager.curBlock < commonPager.totBlock}">
					<a href="#" onClick="list('${commonPager.nextPage}')" class="next">다음 페이지</a>
					</c:if>
					
					<!-- **끝페이지로 이동 : 현재 페이지가 전체 페이지보다 작거나 같으면 [끝]하이퍼링크를 화면에 출력 -->
	                <c:if test="${commonPager.curPage < commonPager.totPage}">
					<a href="#" onClick="list('${commonPager.totPage}')" class="last">마지막 페이지</a>
					</c:if>
				</div><!-- //paginate -->
				</form:form>
			</div><!-- //contents -->
		</div><!-- //contents_wrap -->
	</div><!-- //container -->
	
	<footer>
		<nav class="footer_nav">
			<ul>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">이용약관</a></li>
				<li><a href="#">개인정보처리방침</a></li>
				<li><a href="#">Sitemap</a></li>
			</ul>
		</nav><!-- //footer_nav -->
		<address>㈜케이티 대표이사 황창규 경기도 성남시 분당구 불정로 90 (정자동) 사업자등록번호 : 102-81-42945 통신판매업신고 : 2002-경기성남-0047</address>
	</footer>
	
</div><!-- //wrap -->

<div class="popup_mask"></div>
<div class="popup pop_progress" id="pop_first_progress">
	<div class="pop_tit" id="first_title_profile_nm">진행현황_현대중공업#1</div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">
			<div class="step_pr_ty" >
				<ol>
					<li class="active">
					<strong class="word2">국데이터 생성</strong>
						<div class="circle_active">
						<strong class="circle_text"  id="first_state_name"></strong> 
						</div>	
					</li>
					<li class="pgc">
						<div class="box">
							  <div class="guide">
							    <div id="first_pgc_circle_1" class="circle_progress_normal"></div>
							  </div>  
							  <div class="guide">
							    <div id="first_pgc_triangle_1" class="triangle_right_normal"></div>
							  </div> 
						</div>
					</li>
					<li class="normal">
						<strong class="word">작업준비 </strong>
						<div class="circle_normal">
						<strong class="circle_text"><i></i></strong> 
						</div>	
					</li>
					<li class="pgc">
						<div class="box">
							  <div class="guide">
							    <div id="first_pgc_circle_2" class="circle_progress_normal"></div>
							  </div>  
							  <div class="guide">
							    <div id="first_pgc_triangle_2" class="triangle_right_normal"></div>
							  </div> 
						</div>
					</li>					
					<li class="normal">
						<strong class="word">작업실행 </strong>
						<div class="circle_normal">
						<strong class="circle_text"><i></i></strong> 
						</div>							
					</li>
					<li class="pgc">
						<div class="box">
							  <div class="guide">
							    <div id="first_pgc_circle_3" class="circle_progress_normal"></div>
							  </div>  
							  <div class="guide">
							    <div id="first_pgc_triangle_3" class="triangle_right_normal"></div>
							  </div> 
						</div>
					</li>					
					<li class="normal">
						<strong class="word">작업완료</strong>
						<div class="circle_normal">
						<strong class="circle_text"><i></i></strong> 
						</div>	
					</li>
				</ol>
			</div><!-- //step_pr_ty -->
			<!-- 
			<div class="step_ty">
				<ol>
					<li class="active">
						<i>1</i>
						<strong class="word">국데이터 생성</strong>
						<span class="s_txt" id="first_state_name">변경중</span>
					</li>
					<li>
						<i>2</i>
						<strong class="word">작업준비 </strong>
					</li>
					<li>
						<i>3</i>
						<strong class="word">작업실행 </strong>
					</li>
					<li>
						<i>4</i>
						<strong class="word">작업완료</strong>
					</li>
				</ol>
			</div>
			 -->
			<div class="table_ty">
				<div class="table_head02">
					<ul class="status_board ty02">
						<li>국데이터명 <strong id="first_profile_nm">현대중공업_profile#1  </strong></li>
						<li>작업예정일 <strong id="first_work_ept_dt">2017.12.25 AM 01:00  </strong></li>
					</ul>
				</div>
				<table>
					<caption>목록  테이블</caption>
					<colgroup>
						<col span="1" style="width:15%;">
						<col span="1" style="width:*;">
						<col span="1" style="width:10%;">
						<col span="1" style="width:10%;">
						<col span="1" style="width:12%;">
						<col span="1" style="width:12%;">
						<col span="1" style="width:10%;">
					</colgroup>
					<thead>
						<tr>
							<th scope="col">국데이터 상태</th>
							<th scope="col">변경일자</th>
							<th scope="col">변경자</th>
							<th scope="col">항목</th>
							<th scope="col">변경전</th>
							<th scope="col">변경후</th>
							<th scope="col">변경사유</th>
						</tr>
					</thead>
					<tbody id="firstTrData" style="border:0px">
						<tr>
							<td>저장완료</td>
							<td>2017.11.13 11:00:00</td>
							<td>user1</td>
							<td>IP Pool</td>
							<td>10.3.71.0/24</td>
							<td>10.3.71.0/24</td>
							<td>IP 재할당</td>
						</tr>
					 </tbody>	
				</table>
			</div><!-- //table_ty -->

			<div class="paginate" id="first_paginate">
				<a href="#" class="first">처음 페이지</a>
				<a href="#" class="prev">이전 페이지</a>
				<ul>
					<li class="select"><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">6</a></li>
					<li><a href="#">7</a></li>
					<li><a href="#">8</a></li>
					<li><a href="#">9</a></li>
					<li><a href="#">10</a></li>
				</ul>
				<a href="#" class="next">다음 페이지</a>
				<a href="#" class="last">마지막 페이지</a>
			</div><!-- //paginate -->
			
		</div><!-- //scrollbar-inner -->

	</div><!-- //pop_con_wrap -->
	<a href="javascript:pop_close('#pop_first_progress');" class="pop_close">팝업닫기</a>
</div><!-- //popup -->

<div class="popup pop_progress" id="pop_ready_progress">
	<div class="pop_tit" id="ready_title_profile_nm">진행현황_현대중공업#1</div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">
			<div class="step_pr_ty" >
				<ol>
					<li class="on">
					<strong class="word2">국데이터 생성</strong>
						<div class="circle_on">
						<strong class="circle_text"  id="first_state_name">입력완료<i></i></strong> 
						</div>	
					</li>
					<li class="pgc">
						<div class="box">
							  <div class="guide">
							    <div id="ready_pgc_circle_1" class="circle_progress_normal"></div>
							  </div>  
							  <div class="guide">
							    <div id="ready_pgc_triangle_1" class="triangle_right_normal"></div>
							  </div> 
						</div>
					</li>
					<li class="active">
						<strong class="word">작업준비 </strong>
						<div class="circle_active">
						<strong class="circle_text" id="ready_state_name"><i></i></strong> 
						</div>	
					</li>
					<li class="pgc">
						<div class="box">
							  <div class="guide">
							    <div id="ready_pgc_circle_2" class="circle_progress_normal"></div>
							  </div>  
							  <div class="guide">
							    <div id="ready_pgc_triangle_2" class="triangle_right_normal"></div>
							  </div> 
						</div>
					</li>					
					<li class="normal">
						<strong class="word">작업실행 </strong>
						<div class="circle_normal">
						<strong class="circle_text" id="ready_exec_state_name"><i></i></strong> 
						</div>							
					</li>
					<li class="pgc">
						<div class="box">
							  <div class="guide">
							    <div id="ready_pgc_circle_3" class="circle_progress_normal"></div>
							  </div>  
							  <div class="guide">
							    <div id="ready_pgc_triangle_3" class="triangle_right_normal"></div>
							  </div> 
						</div>
					</li>					
					<li class="normal">
						<strong class="word">작업완료</strong>
						<div class="circle_normal">
						<strong class="circle_text"><i></i></strong> 
						</div>	
					</li>
				</ol>
			</div><!-- //step_pr_ty -->
			<!-- 		
			<div class="step_ty">
				<ol>
					<li class="on">
						<i>1</i>
						<strong class="word">국데이터 생성</strong>
						<span class="s_txt">입력완료</span>
					</li>
					<li class="active">
						<i>2</i>
						<strong class="word">작업준비</strong>
						<span class="s_txt" id="ready_state_name">작업대기(3/6)</span>
					</li>
					<li>
						<i>3</i>
						<strong class="word">작업실행</strong>
						<span class="s_txt" id="ready_exec_state_name">작업예정(201.12.25)</span>
					</li>
					<li>
						<i>4</i>
						<strong class="word">작업완료</strong>
					</li>
				</ol>
			</div>
 			-->	
			
			<div class="table_ty">
				<div class="table_head02">
					<ul class="status_board ty02">
						<li>국데이터명 <strong id="ready_profile_nm">현대중공업_profile#1  </strong></li>
						<li>작업예정일 <strong id="ready_work_ept_dt">  </strong></li>
					</ul>
				</div>
				<table>
					<caption>목록  테이블</caption>
					<colgroup>
						<col span="1" style="width:*;">
						<col span="1" style="width:20%;">
						<col span="1" style="width:20%;">
						<col span="1" style="width:20%;">
					</colgroup>
					<thead>
						<tr>
							<th scope="col">작업대상</th>
							<th scope="col">확인상태</th>
							<th scope="col">확인일자</th>
							<th scope="col">운용자</th>
						</tr>
					</thead>
					<tbody id="readyTrData" style="border:0px">
						<!--  <tr>
							<td>MME_혜화_1</td>
							<td>확인</td>
							<td>2017.11.20 11:00:00</td>
							<td>운용자</td>
						</tr>
 							-->
					</tbody>	
				</table>
			</div><!-- //table_ty -->
			
		</div><!-- //scrollbar-inner -->
		
		<div class="btn_area">
			<a href="javascript:approval_update()" class="btn_l btn_co01">승인하기</a>
		</div>
	</div><!-- //pop_con_wrap -->
	<a href="javascript:void(0);" class="pop_close">팝업닫기</a>
</div><!-- //popup -->

<div class="popup pop_progress" id="pop_exec_progress">
	<div class="pop_tit" id="exec_title_profile_nm">진행현황_현대중공업#1</div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">
			<div class="step_pr_ty" >
				<ol>
					<li class="on">
					<strong class="word2">국데이터 생성</strong>
						<div class="circle_on">
						<strong class="circle_text" >입력완료<i></i></strong> 
						</div>	
					</li>
					<li class="pgc">
						<div class="box">
							  <div class="guide">
							    <div id="exec_pgc_circle_1" class="circle_progress_normal"></div>
							  </div>  
							  <div class="guide">
							    <div id="exec_pgc_triangle_1" class="triangle_right_normal"></div>
							  </div> 
						</div>
					</li>
					<li class="on">
						<strong class="word">작업준비 </strong>
						<div class="circle_on">
						<strong class="circle_text"  id="exec_state_name"><i></i></strong> 
						</div>	
					</li>
					<li class="pgc">
						<div class="box">
							  <div class="guide">
							    <div id="exec_pgc_circle_2" class="circle_progress_normal"></div>
							  </div>  
							  <div class="guide">
							    <div id="exec_pgc_triangle_2" class="triangle_right_normal"></div>
							  </div> 
						</div>
					</li>					
					<li class="active">
						<strong class="word">작업실행 </strong>
						<div class="circle_active">
						<strong class="circle_text"  id="exec_exec_state_name"><i></i></strong> 
						</div>							
					</li>
					<li class="pgc">
						<div class="box">
							  <div class="guide">
							    <div id="exec_pgc_circle_3" class="circle_progress_normal"></div>
							  </div>  
							  <div class="guide">
							    <div id="exec_pgc_triangle_3" class="triangle_right_normal"></div>
							  </div> 
						</div>
					</li>					
					<li class="normal">
						<strong class="word">작업완료</strong>
						<div class="circle_normal">
						<strong class="circle_text"><i></i></strong> 
						</div>	
					</li>
				</ol>
			</div><!-- //step_pr_ty -->		
			<!-- 
			<div class="step_ty">
				<ol>
					<li class="on">
						<i>1</i>
						<strong class="word">국데이터 생성</strong>
						<span class="s_txt">입력완료</span>
					</li>
					<li class="on">
						<i>2</i>
						<strong class="word">작업준비</strong>
						<span class="s_txt" id="exec_state_name">확인완료(6/6)</span>
					</li>
					<li class="active">
						<i>3</i>
						<strong class="word">작업실행</strong>
						<span class="s_txt" id="exec_exec_state_name">명령실행중(3/6)</span>					
					</li>
					<li>
						<i>4</i>
						<strong class="word">작업완료</strong>
					</li>
				</ol>
			</div>
			 -->	
			
			<div class="table_ty">
				<div class="table_head02">
					<ul class="status_board ty02">
						<li>국데이터명 <strong id="exec_profile_nm">현대중공업_profile#1  </strong></li>
						<li>작업예정일 <strong id="exec_work_ept_dt">2017.12.25 AM 01:00  </strong></li>
					</ul>
				</div>
				<table>
					<caption>목록  테이블</caption>
					<colgroup>
						<col span="1" style="width:*;">
						<col span="1" style="width:20%;">
						<col span="1" style="width:20%;">
						<col span="1" style="width:20%;">
					</colgroup>
					<thead>
						<tr>
							<th scope="col">작업대상</th>
							<th scope="col">작업상태</th>
							<th scope="col">실행일시</th>
							<th scope="col">운용자</th>
						</tr>
					</thead>
					<tbody id="execTrData" style="border:0px">
						<!--<tr>
							<td>MME_혜화_1</td>
							<td>fail</td>
							<td>-</td>
							<td>운용자</td>
						</tr>
						 
						<tr>
							<td>MME_혜화_1</td>
							<td>확인</td>
							<td>2017.11.20 11:00:00</td>
							<td>운용자</td>
						</tr>
						-->
					</tbody>	
				</table>
			</div><!-- //table_ty -->
		</div><!-- //scrollbar-inner -->
		<div class="btn_area">
			<a href="javascript:void(0);" onClick="work_complete_update();" id="work_complete_btn" class="btn_l btn_co01 off">작업완료 확인</a>
		</div>
	</div><!-- //pop_con_wrap -->
	<a href="javascript:void(0);" class="pop_close">팝업닫기</a>
</div><!-- //popup -->

<div class="popup pop_progress" id="pop_last_progress">
	<div class="pop_tit" id="last_title_profile_nm">진행현황_현대중공업#1</div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">
			<div class="step_pr_ty" >
				<ol>
					<li class="on">
					<strong class="word2">국데이터 생성</strong>
						<div class="circle_on">
						<strong class="circle_text" >입력완료<i></i></strong> 
						</div>	
					</li>
					<li class="pgc">
						<div class="box">
							  <div class="guide">
							    <div id="last_pgc_circle_1" class="circle_progress_normal"></div>
							  </div>  
							  <div class="guide">
							    <div id="last_pgc_triangle_1" class="triangle_right_normal"></div>
							  </div> 
						</div>
					</li>
					<li class="on">
						<strong class="word">작업준비 </strong>
						<div class="circle_on">
						<strong class="circle_text"  id="last_state_name"><i></i></strong> 
						</div>	
					</li>
					<li class="pgc">
						<div class="box">
							  <div class="guide">
							    <div id="last_pgc_circle_2" class="circle_progress_normal"></div>
							  </div>  
							  <div class="guide">
							    <div id="last_pgc_triangle_2" class="triangle_right_normal"></div>
							  </div> 
						</div>
					</li>					
					<li class="on">
						<strong class="word">작업실행 </strong>
						<div class="circle_on">
						<strong class="circle_text"  id="last_exec_name"><i></i></strong> 
						</div>							
					</li>
					<li class="pgc">
						<div class="box">
							  <div class="guide">
							    <div id="last_pgc_circle_3" class="circle_progress_normal"></div>
							  </div>  
							  <div class="guide">
							    <div id="last_pgc_triangle_3" class="triangle_right_normal"></div>
							  </div> 
						</div>
					</li>					
					<li class="complete">
						<strong class="word">작업완료</strong>
						<div class="circle_complete">
						<strong class="circle_text" id="last_work_complete_name"><i></i></strong> 
						</div>	
					</li>
				</ol>
			</div><!-- //step_pr_ty -->					
			
			<!-- 
			<div class="step_ty">
				<ol>
					<li class="on">
						<i>1</i>
						<strong class="word">국데이터 생성</strong>
						<span class="s_txt">입력완료</span>
					</li>
					<li class="on">
						<i>2</i>
						<strong class="word">작업준비</strong>
						<span class="s_txt" id="last_state_name">확인완료(6/6)</span>
					</li>
					<li class="on">
						<i>3</i>
						<strong class="word">작업실행</strong>
						<span class="s_txt" id="last_exec_name">명령실행(3/6)</span>
					</li>
					<li class="on">
						<i>4</i>
						<strong class="word">작업완료</strong>
						<span class="s_txt" id="last_work_complete_name">2017.12.26 11:00:00</span>
					</li>
				</ol>
			</div>
             -->
			<div class="table_ty">
				<div class="table_head02">
					<ul class="status_board ty02">
						<li>국데이터명 <strong id="last_profile_nm">현대중공업_profile#1  </strong></li>
						<li>작업예정일 <strong id="last_work_ept_dt">2017.12.25 AM 01:00  </strong></li>
					</ul>
				</div>
				<table>
					<caption>목록  테이블</caption>
					<colgroup>
						<col span="1" style="width:*;">
						<col span="1" style="width:20%;">
						<col span="1" style="width:20%;">
						<col span="1" style="width:20%;">
					</colgroup>
					<thead>
						<tr>
							<th scope="col">작업대상</th>
							<th scope="col">작업상태</th>
							<th scope="col">실행일시</th>
							<th scope="col">운용자</th>
						</tr>
					</thead>
					<tbody id="lastTrData" style="border:0px">
						<!--<tr>
							<td>MME_혜화_1</td>
							<td>fail</td>
							<td>-</td>
							<td>운용자</td>
						</tr>
						 
						<tr>
							<td>MME_혜화_1</td>
							<td>확인</td>
							<td>2017.11.20 11:00:00</td>
							<td>운용자</td>
						</tr>
						-->
					</tbody>	
				</table>
			</div><!-- //table_ty -->
		</div><!-- //scrollbar-inner -->

	</div><!-- //pop_con_wrap -->
</div><!-- //popup -->
</body>
</html>