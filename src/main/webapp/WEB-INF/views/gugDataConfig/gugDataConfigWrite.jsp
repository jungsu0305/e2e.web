<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content= "text/html; charset=UTF-8"> 
<meta name="format-detection" content="telephone=no">
<title>KT E2E Infra Orchestrator</title>
<link rel="shortcut icon" href="/new_resources/resources/images/layout/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="/new_resources/resources/css/style.css">
<script type="text/javascript" src="/new_resources/resources/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/template.js"></script>
<script type="text/javascript">
var VIEW_GUBUN="${gugDataConfig.view_gubun}";
var item_val_chk=[//MME
                  ["apn_fqdn","MAN","DECIMAL",0,100,1,"MME","N","APN"]  //CRTE-DOMAIN	APN	MAN

                  ,["pgwgrp","MAN","DECIMAL",0,1023,1,'MME',"N","PGWGRP"]  
                  ,["pgw","OPT","DECIMAL",1,1023,1,'MME',"Y","PGW"]  
                  ,["ip","OPT","STRING",1,64,1,'MME',"Y","IP"]  
                  ,["name","OPT","STRING",0,100,1,'MME',"Y","NAME"]  
                  ,["capa","OPT","DECIMAL",1,255,1,'MME',"Y","CAPA"]  
                  //EPCC
                  ,["pgw_ip","MAN","STRING",7,15,1,'EPCC',"N","IP_ADDR"]  
                  ,["sms_recv","OPT","STRING",0,100,1,'EPCC',"N",""]  //보류
                  ,["ip_pool","OPT","STRING",0,100,1,'EPCC',"Y",""]  //설정하지 안흠- 화면에서 않보이게
                   //PGW
                  ,["ip_pool_id","MAN","DECIMAL",0, 11999 ,1,'PGW',"Y","IPPOOL"]  
                  ,["vr_id","MAN","DECIMAL",0,31,1,'PGW',"Y","VR"] //CRTE-IP-POOL	VR	OPT or  CRTE-VLAN-INTF	VRID	MAN
                  ,["pool_type_cd","OPT",	"ENUM", 0,0,1,"PGW","Y","TYPE"]        
                  ,["static_cd","OPT",	"ENUM", 0,0,1,"PGW","Y","STATIC"]          
                  ,["tunnel_cd","OPT",	"ENUM", 0,0,1,"PGW","Y","TUNNEL"]             
                  ,["start_addr","OPT",	"STRING", 1,64,1,"PGW","Y","IP"]  
                  ,["vlan","OPT",	"DECIMAL", 1,4094,1,"PGW","Y","VLANID"]  													
                  ,["ip_addr","OPT",	"STRING", 1,64,1,"PGW","Y","IPADDR"]  
                  ,["network","OPT",	"STRING", 1,32,1,"PGW","Y","NETWORK"]  
                  ,["gateway","OPT",	"STRING", 1,64,1,"PGW","Y","GWIP"]  
                  ,["prirank_cd","OPT",	"ENUM", 0,100,1,"PGW","Y","우선순위-없음"]  //확인필요함
                  ,["ifc","OPT",	"STRING", 1,16,1,"PGW","Y","IFNAME"]  
                  ,["apn_id","MAN",	"DECIMAL", 0,100,1,"PGW","Y","APN"]  
                  ,["apn_name","MAN",	"STRING", 1,32,1,"PGW","NAME"]  
                  ,["ip_alloc_cd","OPT",	"ENUM", 0,100,1,"PGW","Y","ALLOC"]         
                  ,["rad_ip_alloc_cd","OPT",	"ENUM", 0,100,1,"PGW","Y","ALLOC"]    
                  ,["auth_pcrf_pcc_cd","OPT",	"ENUM", 0,100,1,"PGW","Y",""] //확인 필요함
                  ,["acct_react_cd","OPT",	"ENUM", 0,100,1,"PGW","Y",""]    //확인 필요함     
                  ,["pri_dns_v4","OPT",	"STRING", 0,100,1,"PGW","Y","PDNS4"]  
                  ,["rad_auth_id","OPT",	"DECIMAL", 0,100,1,"PGW","Y","RAUTH"]  
                  ,["rad_auth_act","OPT",	"STRING", 0,100,1,"PGW","Y",""]  //확인 필요함
                  ,["rad_auth_sby","OPT",	"STRING", 0,100,1,"PGW","Y",""]    //확인 필요함
                  ,["rad_acct_id","OPT",	"DECIMAL", 0,15,1,"PGW","Y","RACCT"]  
                  ,["rad_acct_act","OPT",	"STRING", 0,100,1,"PGW","Y",""]    //확인 필요함 
                  ,["rad_acct_sby","OPT",	"STRING", 0,100,1,"PGW","Y",""]    //확인 필요함
                  ,["diam_pcfr_id","OPT",	"STRING", 0,15,1,"PGW","Y","DPCRF"]    
                  ,["diam_pcfr_act","OPT",	"STRING", 0,100,1,"PGW","Y",""]    //확인 필요함 
                  ,["diam_pcfr_sby","OPT",	"STRING", 0,100,1,"PGW","Y",""]   //확인 필요함
                  ,["local_pcc_pf_id","OPT",	"STRING", 1,10,1,"PGW","Y",""]  //설정하지 않음
                  ,["local_pcc_pf_act","OPT",	"STRING", 0,100,1,"PGW","Y",""]  //확인 필요함
                  ,["local_pcc_pf_sby","OPT",	"STRING", 0,100,1,"PGW","Y",""]  //확인 필요함
                  ,["sec_dns_v4","OPT",	"STRING", 0,100,1,"PGW","Y","SDNS4"]  
                  ,["arp_map_high","OPT",	"DECIMAL", 1,14,1,"PGW","Y","ARPH"]  
                  ,["arp_map_med","OPT",	"DECIMAL", 2,15,1,"PGW","Y","ARPM"]
                   //HSS
                  ,["id","MAN",	"DECIMAL", 0,999,1,"HSS","N","ID"] 
                  ,["apn","MAN",	"STRING", 1,64,1,"HSS","N","APN"]  
                  ,["apnoirepl","OPT",	"STRING", 1,64,1,"HSS","N","APNOIREPL"]  
                  ,["pltcc","OPT",	"STRING", 1,5,1,"HSS","N","PLTECC"]  
                  ,["uutype","OPT",	"STRING", 1,4,1,"HSS","N","UUTYPE"]
                  ,["mpname_cd","OPT",	"ENUM", 1,10,1,"HSS","N","MPNAME"]
                 ];
                   

function onlyNumber(obj) {
    $(obj).keyup(function(){
         $(this).val($(this).val().replace(/[^0-9]/g,""));

          
    }); 
    
    //obj.value.length
}

function onlyDateCheck(obj) {
         //$(this).val($(this).val().replace(/^(19|20)\d{2}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[0-1])$/,""));
        var str= obj.value.replace(/[^0-9]/g,"")
         var dayRegExp = /^(19|20)\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[0-1])$/;
        if( str.match(dayRegExp)==null)
        	return false;
        else
            return true;
    	

}

function onlyTimeCheck(obj) {
    //$(this).val($(this).val().replace(/^(19|20)\d{2}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[0-1])$/,""));
   var str= obj.value.replace(/[^0-9]/g,"")
    var dayRegExp = /^(0[0-9]|1[0-9]|2[0-3])(0[0-9]|[12345][0-9])$/;
   if( str.match(dayRegExp)==null)
   	return false;
   else
       return true;
	

}

//국데이타 파라메터 검증 체크
function gug_parameter_check(f)
{
	for(var i=0; i < item_val_chk.length;i++)
	{
		var parameter=item_val_chk[i][0];
		var man_opt=item_val_chk[i][1];
		var parameter_type=item_val_chk[i][2];
		var min=item_val_chk[i][3];
		var max=item_val_chk[i][4];
		var parameter_seq_no=item_val_chk[i][5];
		var equip_type=item_val_chk[i][6];
		var multi_yn=item_val_chk[i][7];
		var p_val_length=0;
		var p_val="";
		var p_val_check_yn=false;
		if(f.mme_check.checked==true && equip_type=="MME")
			{
			p_val_check_yn=true;
			}else if(f.epcc_check.checked==true && equip_type=="EPCC"){
				p_val_check_yn=true;
			}else if(f.pgw_check.checked==true && equip_type=="PGW"){
				p_val_check_yn=true;
			}else if(f.hss_check.checked==true && equip_type=="HSS"){
			    			
			
			p_val_check_yn=true;
		}
	    if(multi_yn=="Y")
		{
	    	var param_u_name="";
	    	var each_flag=true;
	    	if(parameter_type=="ENUM")
	    	{
	    		param_u_name="select[name='" + parameter + "']";
	    	}else{
	    		param_u_name="input[name='" + parameter + "']";
	
	    	}
	    	$(param_u_name).each(function() {
	    		
			    	p_val_length=$.trim($(this).val()).length;
			    	p_val=$.trim($(this).val());
		    	
		    	if(man_opt=="MAN")
		   		{
		    		if(p_val_length==0)
		    		{
		    			if(p_val_check_yn){
	    	    			alert(parameter +"의 파라메터가 주요값이므로 입력하세요");
	    	    			$(this).focus();
	    	    			each_flag=false;
	    	    			return false;
		    			}
		    		}else{
		    			if(parameter_type=="STRING")
		    			{
		    				if(p_val_length < Number(min))
		   					{
		    					if(p_val_check_yn){
	    	    					alert(parameter +"의 파라메터는 최소 " +min+ "자리입니다.\n현재 " +p_val_length+ "자리입니다.");
	    	    	    			$(this).focus();
	    	    	    			each_flag=false;
	    	    	    			return false;
		    					}
		   					}else if(p_val_length > Number(max)){
		   						if(p_val_check_yn){
	    	    					alert(parameter +"의 파라메터는 최대 " +max+ "자리입니다.\n현재 " +p_val_length+ "자리입니다.");
	    	    	    			$(this).focus();
	    	    	    			each_flag=false;
	    	    	    			return false;
		   						}
		   					}
		    			}else if(parameter_type=="DECIMAL"){
		    				if($.isNumeric( p_val)==false){
		    					if(p_val_check_yn){
	    	    					alert(parameter +"의 파라메터는 숫자값만 입력받습니다.\n현재 " +p_val+ " 값입니다.");
	    	    	    			$(this).focus();
	    	    	    			each_flag=false;
	    	    	    			return false;
		    					}
		    				}if(Number(p_val) < Number(min)){
		    					if(p_val_check_yn){
	    	    					alert(parameter +"의 파라메터는 최소값이 " +min+ "부터입니다.\n현재 " +p_val+ " 값입니다.");
	    	    	    			$(this).focus();
	    	    	    			each_flag=false;
	    	    	    			return false;
		    					}
		   					}else if(Number(p_val) > Number(max)){
		   						if(p_val_check_yn){
	    	    					alert(parameter +"의 파라메터는 최대값이 " +max+ "까지입니다.\n현재 " +p_val+ " 값입니다.");
	    	    	    			$(this).focus();
	    	    	    			each_flag=false;
	    	    	    			return false;
		   						}
		   					}
		    				
		    			}
		    		}
		   		
		   		}else{
		   			
		   			if(p_val_length != 0)
		    		{
		    			if(parameter_type=="STRING")
		    			{
		    				if(p_val_length < Number(min))
		   					{
		   						if(p_val_check_yn){
	    	    					alert(parameter +"의 파라메터는 최소 " +min+ "자리입니다.\n현재 " +p_val_length+ "자리입니다.");
	    	    	    			$(this).focus();
	    	    	    			each_flag=false;
	    	    	    			return false;
		   						}
		   					}else if(p_val_length > Number(max)){
		   						if(p_val_check_yn){
	    	    					alert(parameter +"의 파라메터는 최대 " +max+ "자리입니다.\n현재 " +p_val_length+ "자리입니다.");
	    	    	    			$(this).focus();
	    	    	    			each_flag=false;
	    	    	    			return false;
		   						}
		   					}
		    			}else if(parameter_type=="DECIMAL"){
		    				if($.isNumeric( p_val)==false){
		   						if(p_val_check_yn){
	    	    					alert(parameter +"의 파라메터는 숫자값만 입력받습니다.\n현재 " +p_val+ " 값입니다.");
	    	    	    			$(this).focus();
	    	    	    			each_flag=false;
	    	    	    			return false;
		   						}
		    				}if(Number(p_val) < Number(min)){
		   						if(p_val_check_yn){
	    	    					alert(parameter +"의 파라메터는 최소값이 " +min+ "부터입니다.\n현재 " +p_val+ " 값입니다.");
	    	    	    			$(this).focus();
	    	    	    			each_flag=false;
	    	    	    			return false;
		   						}
		   					}else if(Number(p_val) > Number(max)){
		   						if(p_val_check_yn){
	    	    					alert(parameter +"의 파라메터는 최대값이 " +max+ "까지입니다.\n현재 " +p_val+ " 값입니다.");
	    	    	    			$(this).focus();
	    	    	    			each_flag=false;
	    	    	    			return false;
		   						}
		   					}
		    				
		    			}
		    		}
		   		}
	    		
	    		
	
	    	});  //each(function() end
	
	    	if(each_flag==false)
	    		return false;
	    	
		}else{
	    	if(parameter_type=="ENUM")
	    	{
		    	p_val_length=$.trim($("select[name='" + parameter + "']").val()).length;
		    	p_val=$.trim($("select[name='" + parameter + "']").val());
	    	}else{
		    	p_val_length=$.trim($("input[name='" + parameter + "']").val()).length;
		    	p_val=$.trim($("input[name='" + parameter + "']").val());
	    	}
	    	
	    	
	    	if(man_opt=="MAN")
	   		{
	    		if(p_val_length==0)
	    		{
	    			if(p_val_check_yn){
		    			alert(parameter +"의 파라메터가 주요값이므로 입력하세요");
		    			$("input[name='" + parameter + "']").focus();
		    			return false;
	    			}
	    		}else{
	    			if(parameter_type=="STRING")
	    			{
	    				if(p_val_length < Number(min))
	   					{
	    					if(p_val_check_yn){
		    					alert(parameter +"의 파라메터는 최소 " +min+ "자리입니다.\n현재 " +p_val_length+ "자리입니다.");
		    	    			$("input[name='" + parameter + "']").focus();
		    	    			return false;
	    					}
	   					}else if(p_val_length > Number(max)){
	   						if(p_val_check_yn){
		    					alert(parameter +"의 파라메터는 최대 " +max+ "자리입니다.\n현재 " +p_val_length+ "자리입니다.");
		    	    			$("input[name='" + parameter + "']").focus();
		    	    			return false;
	   						}
	   					}
	    			}else if(parameter_type=="DECIMAL"){
	    				if($.isNumeric(p_val)==false){
	    					if(p_val_check_yn){
		    					alert(parameter +"의 파라메터는 숫자값만 입력받습니다.\n현재 " +p_val+ " 값입니다.");
		    	    			$("input[name='" + parameter + "']").focus();
		    	    			return false;
	    					}
	    				}if(Number(p_val) < Number(min)){
	    					if(p_val_check_yn){
		    					alert(parameter +"의 파라메터는 최소값이 " +min+ "부터입니다.\n현재 " +p_val+ " 값입니다.");
		    	    			$("input[name='" + parameter + "']").focus();
		    	    			return false;
	    					}
	   					}else if(Number(p_val) > Number(max)){
	   						if(p_val_check_yn){
		    					alert(parameter +"의 파라메터는 최대값이 " +max+ "까지입니다.\n현재 " +p_val+ " 값입니다.");
		    	    			$("input[name='" + parameter + "']").focus();
		    	    			return false;
	   						}
	   					}
	    				
	    			}
	    		}
	   		
	   		}else{
	   			
	   			if(p_val_length != 0)
	    		{
	    			if(parameter_type=="STRING")
	    			{
	    				if(p_val_length < Number(min))
	   					{
	    					if(p_val_check_yn){
		    					alert(parameter +"의 파라메터는 최소 " +min+ "자리입니다.\n현재 " +p_val_length+ "자리입니다.");
		    	    			$("input[name='" + parameter + "']").focus();
		    	    			return false;
	    					}
	   					}else if(p_val_length > Number(max)){
	   						if(p_val_check_yn){
		    					alert(parameter +"의 파라메터는 최대 " +max+ "자리입니다.\n현재 " +p_val_length+ "자리입니다.");
		    	    			$("input[name='" + parameter + "']").focus();
		    	    			return false;
	   						}
	   					}
	    			}else if(parameter_type=="DECIMAL"){
	    				if($.isNumeric( p_val)==false){
	    					if(p_val_check_yn){
		    					alert(parameter +"의 파라메터는 숫자값만 입력받습니다.\n현재 " +p_val+ " 값입니다.");
		    	    			$("input[name='" + parameter + "']").focus();
		    	    			return false;
	    					}
	    				}if(Number(p_val) < Number(min)){
	    					if(p_val_check_yn){
		    					alert(parameter +"의 파라메터는 최소값이 " +min+ "부터입니다.\n현재 " +p_val+ " 값입니다.");
		    	    			$("input[name='" + parameter + "']").focus();
		    	    			return false;
	    					}
	   					}else if(Number(p_val) > Number(max)){
	   						if(p_val_check_yn){
		    					alert(parameter +"의 파라메터는 최대값이 " +max+ "까지입니다.\n현재 " +p_val+ " 값입니다.");
		    	    			$("input[name='" + parameter + "']").focus();
		    	    			return false;
	   						}
	   					}
	    				
	    			}
	    		}
	   		}
		
		}
		
		
	}
}
function data_write(save_type)
{
	/*'D010','입력중'
	'D020','입력완료'
	'D030','변경중'
	'D040','작업대기'
	'D050','작업완료'*/
	var f=document.form;
	//alert(f.gug_data_manage_no.value);
	if(f.gug_data_manage_no.value=="")
	{
		f.gug_data_manage_no.value="0";
	}
	if(f.gug_data_history_no.value=="")
	{
		f.gug_data_history_no.value="0";
	}
	if(f.parent_gug_data_manage_no.value=="")
	{
		f.parent_gug_data_manage_no.value="0";
	}
	if(f.cust_manage_no.value=="")
	{
		f.cust_manage_no.value="0";
	}
	
	
	
	
	var cust_nm=$('input[name="cust_nm"]').val();
	if(cust_nm=="")
	{
		alert("고객사명을 입력하여 검색을 해주세요");
		$('input[name="cust_nm"]').focus();
		return false;
	}
	
	if( cust_nm != cust_nm_check)
	{
		alert("검색 확인된 고객명이 아니므로 고객 검색을 다시 해주세요.");
		$('input[name="cust_nm"]').focus();
		return false;
	}
	var lte_id=$('input[name="lte_id"]').val();
	if(lte_id=="")
	{
		 if(new_cust_yn=="N"){
				alert("LTE ID를 입력하여 중복확인을 해주세요");
		 }else{
				alert("LTE ID를 입력해주세요");
		 }


		$('input[name="lte_id"]').focus();
		return false;
	}
	
	
	if(lte_id != lte_id_check && new_cust_yn=="N")
	{
		alert("중복 확인된 LTE ID가 아니므로 LTE ID 중복확인을 다시 해주세요");
		$('input[name="lte_id"]').focus();
		return false;
	}	
	var apn=$('input[name="cust_apn"]').val();
	if(apn=="")
	{
		 if(new_cust_yn=="N"){
				alert("APN를 입력하여 중복확인을 해주세요");
		 }else{
				alert("APN를 입력해주세요");
		 }
		$('input[name="cust_apn"]').focus();
		return false;
	}
	
	if(apn != cust_apn_check && new_cust_yn=="N")
	{
		alert("중복 확인된 APN이 아니므로 APN 중복확인을 다시 해주세요");
		$('input[name="cust_apn"]').focus();
		return false;
	}	
	
	
	if(f.profile_nm.value=="")
	{

		alert("FILE NAME을 입력하세요.")
		f.profile_nm.focus()
		return false;
	}
	
	
	
	if(f.work_ept_dt.value=="")
	{

		alert("작업예정일자를 입력하세요.")
		f.work_ept_dt.focus()
		return false;
	}
	
	if(onlyDateCheck(f.work_ept_dt)!=true)
	{
		alert("유효한 날짜가 아닙니다.")
		f.work_ept_dt.focus()
		return false;
	
	}

	if(f.work_ept_start_time.value=="")
	{

		alert("작업예정시간부터를 입력하세요.")
		f.work_ept_start_time.focus()
		return false;
	}
	
	if(onlyTimeCheck(f.work_ept_start_time)!=true)
	{
		alert("작업예정시간부터가 유효한 시간 값이 아닙니다.")
		f.work_ept_start_time.focus()
		return false;
	
	}
	
	if(f.work_ept_close_time.value=="")
	{

		alert("작업예정시간까지를 입력하세요.")
		f.work_ept_close_time.focus()
		return false;
	}
	
	if(onlyTimeCheck(f.work_ept_close_time)!=true)
	{
		alert("작업예정시간까지가 유효한 시간 값이 아닙니다.")
		f.work_ept_close_time.focus()
		return false;
	
	}
	
	if(gug_parameter_check(f)==false)
	{
		return false;
	}
	
	

	
	var save_flag=true;
	if(save_type=="D010")	
	{
		if(confirm("임시저장하시겠습니까?"))
		{
			 save_flag=true;
				
		}else{
			 save_flag=false;
		}
	}
	
	
	if(save_type=="D030")	
	{
		//alert($("input[name='im_pwdd']").val());
		f.gug_data_pwd.value= $("input[name='im_pwdd']").val();
		f.change_reason.value=$("textarea[name='im_change_reason']").text();

	
	}
	
	if(save_flag){
		f.work_ept_dt.value=f.work_ept_dt.value.replace(/[^0-9]/g,"");
		f.work_ept_start_time.value=f.work_ept_start_time.value.replace(/[^0-9]/g,"");
		f.work_ept_close_time.value=f.work_ept_close_time.value.replace(/[^0-9]/g,"");
		f.work_state_cd.value=save_type;
		f._method.value="POST"
		f.method="POST";
		f.action="/gugDataConfig/post";
		
		f.submit();
	}
}

function data_change()
{
	
	pop_open("#pop_data_change");
	


}

function data_delete()
{
	/*'D010','입력중'
	'D020','입력완료'
	'D030','변경중'
	'D040','작업대기'
	'D050','작업완료'*/
	if(confirm("해당 국 데이타를 삭제하시겠습니까?"))
	{
		var f=document.form;
		f._method.value="DELETE"
		f.method="POST";
		f.action="/gugDataConfig/delete/${gugDataConfig.gug_data_manage_no}";
		
		f.submit();
	}
}

//MME 디테일 추가 버튼 클릭시
function mmeAdd()
{
	// item 의 최대번호 구하기 
	//var lastItemNo = $("#mmeTrData tr:last").attr("class").replace("item", "");
	//var newitem = $("#mmeTrData tr:eq(1)").clone();
	//	var newitem = $("#mmeTrData tr:last").clone();
	// ("#mmeTrData").add(newitem);
	var det_no=$("#mmeTrData tr:last>td:first>input[name='mme_det_no']").val()
	$.trClone = $("#mmeTrData tr:last").clone().html();
                $.newTr = $("<tr>"+$.trClone+"</tr>");
                // append
                $("#mmeTrData").append($.newTr);
                $("#mmeTrData tr:last>td:first>input[name='mme_det_no']").val(Number(det_no)+1);
           // alert($("#mmeTrData tr:last>td:first>input[name='mme_det_no']").val());

}

//EPCC 디테일 추가 버튼 클릭시
function epccAdd()
{
	// item 의 최대번호 구하기 
	//var lastItemNo = $("#mmeTrData tr:last").attr("class").replace("item", "");
	//var newitem = $("#mmeTrData tr:eq(1)").clone();
	//	var newitem = $("#mmeTrData tr:last").clone();
	// ("#mmeTrData").add(newitem);
	var det_no=$("#epccTrData tr:last>td:first>input[name='epcc_det_no']").val()
	//alert(det_no);
	$.trClone = $("#epccTrData tr:last").clone().html();
                $.newTr = $("<tr>"+$.trClone+"</tr>");
                // append
                $("#epccTrData").append($.newTr);
                $("#epccTrData tr:last>td:first>input[name='epcc_det_no']").val(Number(det_no)+1);
           // alert($("#mmeTrData tr:last>td:first>input[name='mme_det_no']").val());

}


//PGW 디테일 추가 버튼 클릭시
function pgwAdd()
{
	// item 의 최대번호 구하기 
	//var lastItemNo = $("#mmeTrData tr:last").attr("class").replace("item", "");
	//var newitem = $("#mmeTrData tr:eq(1)").clone();
	//	var newitem = $("#mmeTrData tr:last").clone();
	// ("#mmeTrData").add(newitem);

	$.trLength=$("#pgwTrData tr").length;
	var det_no=0;

	if($.trLength < 8)
		$.termLength=7;
	else
		$.termLength=8;
	
	det_no=$("#pgwTrData tr:eq(" +($.trLength - $.termLength)+ ") > td:first > input[name='pgw_seq_no']").val();

	$.trClone = "<tr><td colspan=11 style=\"margin:0;padding:0;height:2px;background-color:#d1d2d4;\"></td></tr>";
	for(i=0;i<8;i++){
		$.trClone += "<tr>"+$("#pgwTrData tr:eq(" + i +")").clone().html()+"</tr>";
	}
     // append
     $("#pgwTrData").append($.trClone );
     $.trLength=$("#pgwTrData tr").length;
 	if($.trLength < 8)
		$.termLength=7;
	else
		$.termLength=8;
     $("#pgwTrData tr:eq(" +($.trLength - $.termLength)+ ") > td:first > input[name='pgw_seq_no']").val(Number(det_no)+1);
// alert($("#mmeTrData tr:last>td:first>input[name='mme_det_no']").val());

}

function list(page){
	var f=document.form;
    location.href="/gugDataConfig/view/${gugDataConfig.gug_data_manage_no}/"+page;
}

var cust_nm_check="${gugDataConfig.cust_nm}";
var new_cust_yn="N";

//고객사명 검색체크
function custNmCheck()
{
	
	var cust_nm=$('input[name="cust_nm"]').val();
	if(cust_nm=="")
	{
		alert("고객사명을 입력하여 검색을 해주세요");
		$('input[name="cust_nm"]').focus();
		return false;
	}
	 $.ajax({
	        type: "get",

	        url: "/cust/custLteIdApnCheck?cust_nm="+ encodeURI(cust_nm)
	        		,
	    	contentType: "application/x-www-form-urlencoded; charset=UTF-8",
	        success: function(result){
	        	if(cust_nm_check!=cust_nm){
	        		$('input[name="lte_id"]').val("");
	        		$('input[name="cust_apn"]').val("");
	        	}
	        	if(result=="")
        		{
        			if(confirm(cust_nm+"의 고객사명이 존재 하지 않습니다.\n현재 국데이타와 같이 등록 하시겠습니까?"))
        			{
        				cust_nm_check=cust_nm;
        				new_cust_yn="Y";
        				$('input[name="cust_nm"]')
        				 $("a[id='btn_lte_id']").attr("style","visibility:hidden");
        				 $("a[id='btn_cust_apn']").attr("style","visibility:hidden");
        			}else{
        				new_cust_yn="N";
        				$('input[name="cust_nm"]').val("");
        				$('input[name="cust_nm"]').focus();
	     				cust_nm_check="";
       				 $("a[id='btn_lte_id']").attr("style","visibility:visible");
    				 $("a[id='btn_cust_apn']").attr("style","visibility:visible");
        			}
        		}else{
            	 var dataObj = JSON.parse(result);
            	 $("a[id='btn_lte_id']").attr("style","visibility:visible");
				 $("a[id='btn_cust_apn']").attr("style","visibility:visible");
            	 //alert(dataObj.cust_manage_no);
	            	 if(confirm(cust_nm+"의 고객사명이 존재 합니다..\n현재 국데이타를 등록하는데 사용 하시겠습니까?"))
	     			{
	            		 $('input[name="cust_manage_no"]').val(dataObj.cust_manage_no);
	            		 
	     				cust_nm_check=cust_nm;

	     				
	     			}else{
	     				cust_nm_check="";
	     				$('input[name="cust_nm"]').val("");
	     				$('input[name="cust_nm"]').focus();
	
	     			}
	     				new_cust_yn="N";
        		}
	        	

	        },
			error : function(result, status, err) {
				alert('고객검색 중복확인을 하는데 실패했습니다.');
			}
	    });
}

var lte_id_check="${gugDataConfig.lte_id}";
//LTEID중복확인
function custLteIdCheck()
{
	
	var cust_nm=$('input[name="cust_nm"]').val();
	if(cust_nm=="")
	{
		alert("고객사명을 입력하여 검색을 해주세요");
		$('input[name="cust_nm"]').focus();
		return false;
	}
	
	if( cust_nm != cust_nm_check)
	{
		alert("검색 확인된 고객명이 아니므로 고객 검색을 다시 해주세요.");
		$('input[name="cust_nm"]').focus();
		return false;
	}
	var lte_id=$('input[name="lte_id"]').val();
	if(lte_id=="")
	{
		alert("LTE ID를 입력하여 중복확인을 해주세요");
		$('input[name="lte_id"]').focus();
		return false;
	}

	 $.ajax({
	        type: "get",

	        url: "/cust/custLteIdApnCheck?cust_nm="+ encodeURI(cust_nm)
	        		+ "&lte_id="+ lte_id
	        		,
	    	contentType: "application/x-www-form-urlencoded; charset=UTF-8",
	        success: function(result){
	        	if(result=="")
        		{
	        		if(new_cust_yn=="N")
	        		{
	        			if(confirm(lte_id+"의  LTE ID가 존재 하지 않습니다.\n현재 국데이타와 같이 등록 하시겠습니까?"))
	        			{
	        				lte_id_check=lte_id;
	        				$('input[name="cust_manage_no"]').val("0");
	        			}else{
	        				$('input[name="lte_id"]').val("");
	        				$('input[name="lte_id"]').focus();
	        				lte_id_check="";
	        			}
	        		}else{
	        			lte_id_check=lte_id;
	        		}
        		}else{
            	 var dataObj = JSON.parse(result);
	            	 if(confirm(lte_id+"의 LTE ID가 존재 합니다..\n현재 국데이타를 등록하는데 사용 하시겠습니까?"))
	     			{
	            		 lte_id_check=dataObj.lte_id;
	     				
	     			}else{
	     				$('input[name="lte_id"]').val("");
	     				$('input[name="lte_id"]').focus();
	     				lte_id_check="";
	     			}
        		}
	        },
			error : function(result, status, err) {
				alert('LTE ID 중복확인을 하는데 실패했습니다.');
			}
	    });
}


var cust_apn_check="${gugDataConfig.cust_apn}";

//APN중복체크
function custApnCheck()
{
	
	var cust_nm=$('input[name="cust_nm"]').val();
	if(cust_nm=="")
	{
		alert("고객사명을 입력하여 검색을 해주세요");
		$('input[name="cust_nm"]').focus();
		return false;
	}
	
	if( cust_nm != cust_nm_check)
	{
		alert("검색 확인된 고객명이 아니므로 고객 검색을 다시 해주세요.");
		$('input[name="cust_nm"]').focus();
		return false;
	}
	var lte_id=$('input[name="lte_id"]').val();
	if(lte_id=="")
	{
		alert("LTE ID를 입력하여 중복확인을 해주세요");
		$('input[name="lte_id"]').focus();
		return false;
	}
	if(lte_id != lte_id_check && new_cust_yn=="N")
	{
		alert("중복 확인된 LTE ID가 아니므로 LTE ID 중복확인을 다시 해주세요");
		$('input[name="lte_id"]').focus();
		return false;
	}	
	var apn=$('input[name="cust_apn"]').val();
	if(apn=="")
	{
		 if(new_cust_yn=="N"){
				alert("APN를 입력하여 중복확인을 해주세요");
		 }else{
				alert("APN를 입력해주세요");
		 }
		$('input[name="cust_apn"]').focus();
		return false;
	}
	 $.ajax({
	        type: "get",

	        url: "/cust/custLteIdApnCheck?cust_nm="+ encodeURI(cust_nm)
	        		+ "&lte_id="+ lte_id
	        		+ "&apn="+ apn
	        		,
	    	contentType: "application/x-www-form-urlencoded; charset=UTF-8",
	        success: function(result){
	        	if(result=="")
        		{
	        		if(new_cust_yn=="N")
	        		{
	        			if(confirm(apn+" 값의 APN이 존재 하지 않습니다.\n현재 국데이타와 같이 등록 하시겠습니까?"))
	        			{
	        				cust_apn_check=apn;
	        				$('input[name="cust_manage_no"]').val("0");
	        			}else{
	        				$('input[name="cust_apn"]').val("");
	        				$('input[name="cust_apn"]').focus();
	        				$('input[name="cust_manage_no"]').val("0");
	        				cust_apn_check="";
	        			}
	        		}else{
	        			cust_apn_check=apn;
	        		}

        		}else{
            	 var dataObj = JSON.parse(result);
            	     if(dataObj.msg!="")
           	    	 {
            	    	 alert(dataObj.msg);
		     				$('input[name="cust_apn"]').val("");
	        				$('input[name="cust_manage_no"]').val("0");
		     				cust_apn_check="";
		     				$('input[name="cust_apn"]').focus();
           	    	 
           	    	 }else{
		            	 if(confirm(apn+" 값의 APN이 존재 합니다..\n현재 국데이타를 등록하는데 사용 하시겠습니까?"))
		     			{
		            		 $('input[name="cust_manage_no"]').val(dataObj.cust_manage_no);
		     				cust_apn_check=apn;
		     				
		     			}else{
		     				$('input[name="cust_apn"]').val("");
	        				$('input[name="cust_manage_no"]').val("0");
		     				cust_apn_check="";
		     				$('input[name="cust_apn"]').focus();
	
		     			}
           	    	}
        		}
	        },
			error : function(result, status, err) {
				alert('APN 중복확인을 하는데 실패했습니다.');
			}
	    });
}

$( document ).ready(function() {
	if(VIEW_GUBUN=="EDIT")
	{
		 $("input[name='cust_nm']").attr("disabled",true);
		 $("input[name='cust_apn']").attr("disabled",true);
		 $("input[name='lte_id']").attr("disabled",true);
		 $("a[id='btn_cust_nm']").attr("disabled",true);
		 $("a[id='btn_lte_id']").attr("disabled",true);
		 $("a[id='btn_cust_apn']").attr("disabled",true);
		 $("a[id='btn_cust_nm']").attr("style","visibility:hidden");
		 $("a[id='btn_lte_id']").attr("style","visibility:hidden");
		 $("a[id='btn_cust_apn']").attr("style","visibility:hidden");
		 $("a[id='btn_cust_nm']").attr("class","btn_m btn_co03 off");
		 $("a[id='btn_lte_id']").attr("class","btn_m btn_co03 off");
		 $("a[id='btn_cust_apn']").attr("class","btn_m btn_co03 off");
	}

});

var left_command=[];
var right_command=[];
function pop_view_command(gug_data_manage_no)
{
    $.ajax({
        type: "get",
       // contentType: "json", //==> 생략가능(RestController이기때문에 가능)
        url: "/gugDataConfig/equip_cmd_list?gug_data_manage_no="+gug_data_manage_no
        		 + "&gug_equip_type_cd="
        		// + "&gug_equip_type_cd=D001"
        		,
        success: function(result){
            	 var dataObj = JSON.parse(result);
            	 var cmd_base=dataObj.equip_cmd;
            	 var gug_sel_cmd=dataObj.gug_sel_cmd;
            	 var left_html_txt="";
            	 var right_html_txt="";
            	if(left_command.length<=0)
            	{
            		
	            	 for(var i in cmd_base){
	            		 //left
	            		 var temp=[];
	            		 temp.push(cmd_base[i].gug_equip_type_cd);
	            		 temp.push(cmd_base[i].command);
	            		 left_command.push(temp);
	
	
	            	 }
	            	 
	            	 for(var j in gug_sel_cmd){
	            		 //left
	            		 var temp=[];
	            		 temp.push(gug_sel_cmd[j].gug_equip_type_cd);
	            		 temp.push(gug_sel_cmd[j].command);
	            		 right_command.push(temp);
	
	
	            	 }
            		 
            		 
              		 for(var i=0;i<right_command.length;i++)
            		 {
               			right_html_txt+="<li><a href=\"#none\" onClick=\"right_click('"+right_command[i][1]+"')\" >"+right_command[i][1] + "</a></li>";

              			for(var j=(left_command.length-1);0<=j;j--)
	               		 {
              				if(right_command[i][1]==left_command[j][1])
           					{
              					left_command.splice(j,1);
              					break;
           					}
	               		 }

            		 }
              		 for(var i=0;i<left_command.length;i++)
              		 {
              			left_html_txt+="<li><a href=\"#none\" onClick=\"left_click('"+left_command[i][1]+"')\" >"+left_command[i][1] + "</a></li>";
              		 }


	            	 $("ul[id='left_commands']").html(left_html_txt);
	            	 $("ul[id='right_commands']").html(right_html_txt);
            	}
            	if(right_command.length<=0)
            	{
            	 $("ul[id='right_commands']").html("");
            	}
                 pop_open('#pop_ex_command');
        },
		error : function(result, status, err) {
			alert('뷰메시지를 여는데 실패했습니다.');
		}
    });
}

var left_value="";
function left_click(cValue)
{

	left_value=cValue;

}

var right_value="";
function right_click(cValue)
{
	
	right_value=cValue;
}

function right_move()
{
	if(left_value=="")
	{
		alert("Select Commnd List에서 명령어를 하나 선택하세요");
		left_value="";
		right_value="";
		return false;
	
	}
	
	 var temp=[];
	 for(var i=0;i<left_command.length;i++)
	 {
		if( left_command[i][1] == left_value)
		{
			temp.push(left_command[i][0]);
			temp.push(left_command[i][1]);
			left_command.splice(i,1);
			break;
		}
	 }

	right_command.push(temp);
	left_value="";
	right_value="";
	 var html_txt="";
	 for(var i=0;i<left_command.length;i++)
	 {
		html_txt+="<li><a href=\"#none\" onClick=\"left_click('"+left_command[i][1]+"')\" >"+left_command[i][1] + "</a></li>";
	 }
	 $("ul[id='left_commands']").html(html_txt);
	 
	 html_txt="";
	 for(var i=0;i<right_command.length;i++)
	 {
		html_txt+="<li><a href=\"#none\" onClick=\"right_click('"+right_command[i][1]+"')\" >"+right_command[i][1] + "</a></li>";
	 }
	 $("ul[id='right_commands']").html(html_txt);
	 
	 $("input[name='select_run_command']").val(right_command.join(":"));

	// alert($("input[name='select_run_command']").val());

}


function left_move()
{
	if(right_value=="")
	{
		alert("Run Commnd List에서 명령어를 하나 선택하세요");
		left_value="";
		right_value="";
		return false;
	
	}
	 var temp=[];
	 for(var i=0;i<right_command.length;i++)
	 {
		if( right_command[i][1] == right_value)
		{
			temp.push(right_command[i][0]);
			temp.push(right_command[i][1]);
			right_command.splice(i,1);
			break;
		}
	 }
	left_command.push(temp);
	left_value="";
	right_value="";
	var html_txt="";
	 for(var i=0;i<left_command.length;i++)
	 {
		html_txt+="<li><a href=\"#none\" onClick=\"left_click('"+left_command[i][1]+"')\" >"+left_command[i][1] + "</a></li>";
	 }
	 $("ul[id='left_commands']").html(html_txt);
	 
	 html_txt="";
	 for(var i=0;i<right_command.length;i++)
	 {
		html_txt+="<li><a href=\"#none\" onClick=\"right_click('"+right_command[i][1]+"')\" >"+right_command[i][1] + "</a></li>";
	 }
	 $("ul[id='right_commands']").html(html_txt);

	 $("input[name='select_run_command']").val(right_command.join(":"));
}
</script>

</head>

<body>
<div id="wrap">
    
    <header>
		<h1 class="logo"><a href="#">KT E2E Infra Orchestrator</a></h1>
		<nav id="gnb">
			<ul>
				<li><a href="#">DESIGN</a></li>
				<li><a href="#">PROVISIONING</a></li>
				<li><a href="#">MONITORING</a></li>
				<li><a href="#">INVENTORY</a></li>
				<li><a href="#">CONFIGURATION</a></li>
				<li class="active"><!-- 활성화 class="active" -->
					<a href="#">P-LTE</a>
					<ul>
						<li class="active"><a href="/gugDataConfig">국데이터 관리</a></li><!-- 활성화 class="active" -->
						<li><a href="/gugPcmdCrExec">명령 실행</a></li>		
						<li><a href="/indPcmdCrExec">운용자 명령어</a></li>
						<li><a href="/cust">고객사 관리</a></li>
						<li><a href="/equipInven">장비 관리</a></li>
						<li><a href="#">모니터링</a></li>
						<li><a href="#">이력조회</a></li>
					</ul>
				</li>
			</ul>
		</nav><!-- //gnb -->
		<div class="ad_info">
			<i><img src="/new_resources/resources/images/layout/img_ad_info.jpg" alt=""></i><!-- image size : 25*25px -->
			<p>admin</p>
		</div><!-- //ad_info -->
    </header>
    
    <div id="container">	
		
		<ul id="navigation">
			<li><img src="/new_resources/resources/images/layout/img_navigation_home.png" alt="home"></li>
			<li>국데이터 조회</li>
			<li>PROFILE#1</li>
		</ul><!-- //navigation -->
		
		<div id="contents_wrap">
			<div class="contents">
			<form:form commandName="GugDataConfigComponent" name="form" action="" onsubmit="return false;">
	    	<input type="hidden" name="_method" value=""><!-- RESTFUL 메소드 오버라이드를 위해  -->
			<input type="hidden" name="gug_data_manage_no" value="${gugDataConfig.gug_data_manage_no}">
			<input type="hidden" name="gug_data_history_no" value="${gugDataConfig.gug_data_history_no}">
			<input type="hidden" name="parent_gug_data_manage_no" value="${gugDataConfig.parent_gug_data_manage_no}">
			<input type="hidden" name="gug_data_pwd" value="">
			<input type="hidden" name="change_reason" value="">
			<input type="hidden" name="select_run_command" value="">
			
			
			<input type="hidden" name="cust_manage_no" value="${gugDataConfig.cust_manage_no}">
		    <input type="hidden" name="work_state_cd" value="">
				<div class="table_head03"><!-- 180103 수정 -->
					<h3 class="title"><span>고객사 정보</span></h3>
					<div class="right">
						<a href="#" onClick="pop_view_command(${gugDataConfig.gug_data_manage_no})" class="btn_l btn_co01">실행 명령어 선택</a>
					</div>
				</div><!-- //180103 수정 -->
				<div class="table_form bg">
					<table>
						<caption>검색 테이블</caption>
						<colgroup>
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
						</colgroup>
						<tbody>
							<tr>
								<th><span>고객사명</span></th>
								<td>
									<div class="btn_list">
										<input type="text" name="cust_nm" value="${gugDataConfig.cust_nm}">
										<a href="#" onclick="custNmCheck()" id="btn_cust_nm" class="btn_m btn_co03 on">검색</a>
									</div>
								</td>
								<td>&nbsp;</td>
								<th><span>LTE ID</span></th>
								<td>
									<div class="btn_list">
										<input type="text" name="lte_id" value="${gugDataConfig.lte_id}">
										<a href="#" onclick="custLteIdCheck()" id="btn_lte_id" class="btn_m btn_co03 on">중복확인</a>
									</div>
								</td>
								<td>&nbsp;</td>
								<th><span>vAPN</span></th>
								<td>
									<div class="btn_list">
										<input type="text" name="cust_apn" value="${gugDataConfig.cust_apn}">
										<a href="#"  onclick="custApnCheck()" id="btn_cust_apn" class="btn_m btn_co03 on">중복확인</a>
									</div>
								</td>
							</tr>
							<tr>
								<th><span>IP 유형</span></th>
								<td>
									<select name="ip_type">
										<option value=""></option>
										<option value="고정IP" <c:if test='${gugDataConfig.ip_type=="고정IP"}'> selected </c:if> >고정IP</option>
										<option value="유동IP" <c:if test='${gugDataConfig.ip_type=="유동IP"}'> selected </c:if> >유동IP</option>
									</select>
								</td>
								<td>&nbsp;</td>
								<th><span>서비스타입</span></th>
								<td>
									<select name="service_type">
										<option value=""></option>									
										<option value="전국형" <c:if test='${gugDataConfig.service_type=="전국형"}'> selected </c:if> >전국형</option>
										<option value="ZONE형" <c:if test='${gugDataConfig.service_type=="ZONE형"}'> selected </c:if> >ZONE형</option>

									</select>
								</td>
								<td>&nbsp;</td>
								<th><span>전용회선 사용여부</span></th>
								<td>
									<select name="ded_line_use_yn">
										<option value=""></option>									
										<option value="전용회선" <c:if test='${gugDataConfig.ded_line_use_yn=="전용회선"}'> selected </c:if> >전용회선</option>										
										<option value="인터넷" <c:if test='${gugDataConfig.ded_line_use_yn=="인터넷"}'> selected </c:if> >인터넷</option>										

									</select>
								</td>
							</tr>
						</tbody>	
					</table>
				</div><!-- //table_form bg -->

				<div class="table_form02">
					<table border=0>
						<caption>검색 테이블</caption>
						<colgroup>
							<col span="1" style="width:8%;">
							<col span="1" style="width:200px;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:8%;">
							<col span="1" style="width:110px;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:90px">
							<col span="1" style="width:60px;">
							<col span="1" style="width:20px;">
							<col span="1" style="width:90px;">
							<col span="1" style="width:20%;">
						</colgroup>
						<tbody>
							<tr>
								<th><span>FILE NAME</span></th>
								<td>
									<input type="text" name="profile_nm" value="${gugDataConfig.profile_nm}" maxlength=50>
								</td>
								<td>&nbsp;</td>
								<th><span>작업예정일</span></th>
								<td>
									<div class="date_area02">
										<input type="text" name="work_ept_dt" value="${gugDataConfig.work_ept_dt}" class="select_date" maxlength=10>
									</div>
								</td>
								<td>&nbsp;</td>
								<th><span>작업예정시작시간</span></th>
								<td>
									<div class="time_area">
										<input type="text" name="work_ept_start_time" value="${gugDataConfig.work_ept_start_time}" maxlength=5>
									</div>
								</td>
								<td>
										<span class="b_br">~</span>
								</td>

								<th><span>작업예정종료시간</span></th>
								<td>
									<div class="time_area">
										<input type="text" name="work_ept_close_time" value="${gugDataConfig.work_ept_close_time}" maxlength=5>
									</div>
								</td>
							</tr>
						</tbody>	
					</table>
				</div><!-- //table_form02 -->

				<h5 class="title"><label><input type="checkbox" name="mme_check" value="on" <c:if test="${gugDataConfig.mme_check eq 'on'}"> checked</c:if> >MME</label></h5>
				<div class="table_form ty_flex">
					<table>
						<caption>입력 테이블</caption>
						<colgroup>
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
						</colgroup>
						<tbody>
							<tr>
								<th><span>APN-FQDN</span></th>
								<td><input type="text" name="apn_fqdn" value="${gugDataConfig.apn_fqdn}"></td>
								<td>&nbsp;</td>
								<th><span>PGWGRP</span></th>
								<td><input type="text" name="pgwgrp" value="${gugDataConfig.pgwgrp}"></td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="table_form ty02 ty_flex bgc">
					<table>
						<caption>입력 테이블</caption>
						<colgroup>
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
						</colgroup>

						<tbody id="mmeTrData" style="border:0px">
							<c:forEach var="mmeDetLst" items="${mmeDetLst}">
							<tr>
								<th><span>PGW</span></th>
								<td><input type="text" name="pgw" value="${mmeDetLst.pgw}"><input type="hidden" name="mme_det_no" value="${mmeDetLst.mme_det_no}"></td>
								<td>&nbsp;</td>
								<th><span>IP</span></th>
								<td><input type="text" name="ip" value="${mmeDetLst.ip}"></td>
								<td>&nbsp;</td>
								<th><span>NAME</span></th>
								<td><input type="text" name="name" value="${mmeDetLst.name}"></td>
								<td>&nbsp;</td>
								<th><span>CAPA</span></th>
								<td><input type="text" name="capa" value="${mmeDetLst.capa}"></td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
					<div class="btn_area">
						<a href="#" onClick="mmeAdd()" class="btn_m btn_co03 on">추가</a>
					</div>
				</div>
				<!-- //table_form -->

				<h5 class="title"><label><input type="checkbox" name="epcc_check" value="on" <c:if test="${gugDataConfig.epcc_check eq 'on'}"> checked</c:if>>ePCC</label></h5>
				<div class="table_form ty_flex">
					<table>
						<caption>입력 테이블</caption>
						<colgroup>
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
						</colgroup>
						<tbody>
							<tr>
								<th><span>PGW IP</span></th>
								<td><input type="text" name="pgw_ip" value="${gugDataConfig.pgw_ip}"></td>
								<td>&nbsp;</td>
								<th><span>SMS 수신</span></th>
								<td><input type="text" name="sms_recv" value="${gugDataConfig.sms_recv}"></td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="table_form ty02 ty_flex bgc">
					<table>
						<caption>입력 테이블</caption>
						<colgroup>
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
						</colgroup>
						<tbody id="epccTrData" style="border:0px">
						<c:forEach var="epccDetLst" items="${epccDetLst}">
							<tr>
								<th><span>IP POOL</span></th>
								<td><input type="text" name="ip_pool" value="${epccDetLst.ip_pool}"><input type="hidden" name="epcc_det_no" value="${epccDetLst.epcc_det_no}"></td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
					<div class="btn_area">
						<a href="#" onClick="epccAdd()" class="btn_m btn_co03 on">추가</a>
					</div>
				</div>
				<!-- //table_form -->

				<h5 class="title"><label><input type="checkbox" name="pgw_check" value="on"  <c:if test="${gugDataConfig.pgw_check eq 'on'}"> checked</c:if> >PGW </label></h5>
				<div class="table_form ty03 ty_flex bgc">
					<table>
						<caption>입력 테이블</caption>
						<colgroup>
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
						</colgroup>
												
						<tbody id="pgwTrData" style="border:0px">
	                     <c:forEach var="pgwLst" items="${pgwLst}" varStatus="status">
	                       <c:if test="${status.count > 1}">
	             	           <tr><td colspan=11 style="margin:0;padding:0;height:2px;background-color:#d1d2d4;"></td></tr>
							</c:if>
							<tr>
								<th><span>IP POOL ID</span><input type="hidden" name="rule_base_id" value="0"></th>
								<td><input type="text" name="ip_pool_id" value="${pgwLst.ip_pool_id}"><input type="hidden" name="pgw_seq_no" value="${pgwLst.pgw_seq_no}"></td>
								<td>&nbsp;</td>
								<th><span>VR_ID</span></th>
								<td><input type="text" name="vr_id" value="${pgwLst.vr_id}"></td>
								<td>&nbsp;</td>
								<th><span>POOL TYPE</span></th>
								<td>
									<select name="pool_type_cd">
									<option value="" ></option>
									<c:forEach var="cbd" items="${combo_pool_type_cd}" varStatus="status">
										<option value="${cbd.cd}" ${cbd.cd==pgwLst.pool_type_cd?"selected":""} >${cbd.nm}</option>
									</c:forEach>
									<!-- option value="ddddd" selected >dddd</option> -->
									</select>
								</td>
								<td>&nbsp;</td>
								<th><span>STATIC</span></th>
								<td>
									<select name="static_cd">
									<option value="" ></option>									
									<c:forEach var="cbd" items="${combo_static_cd}">
										<option value="${cbd.cd}" ${cbd.cd==pgwLst.static_cd?"selected":""} >${cbd.nm}</option>
									</c:forEach>
									</select>
								</td>
							</tr>
							<tr>
								<th><span>TUNNEL</span></th>
								<td>
									<select name="tunnel_cd">
									<option value="" ></option>									
									<c:forEach var="cbd" items="${combo_tunnel_cd}">
										<option value="${cbd.cd}" ${cbd.cd==pgwLst.tunnel_cd?"selected":""} >${cbd.nm}</option>
									</c:forEach>
									</select>
								</td>
								<td>&nbsp;</td>
								<th><span>START ADDR</span></th>
								<td><input type="text" name="start_addr" value="${pgwLst.start_addr}"></td>
								<td>&nbsp;</td>
								<th><span>VLAN</span></th>
								<td><input type="text" name="vlan" value="${pgwLst.vlan}"></td>
								<td>&nbsp;</td>
								<th><span>IP ADDR</span></th>
								<td><input type="text" name="ip_addr" value="${pgwLst.ip_addr}"></td>
							</tr>
							<tr>
								<th><span>NETWORK</span></th>
								<td><input type="text" name="network" value="${pgwLst.network}"></td>
								<td>&nbsp;</td>
								<th><span>GATEWAY</span></th>
								<td><input type="text" name="gateway" value="${pgwLst.gateway}"></td>
								<td>&nbsp;</td>
								<th><span>우선순위</span></th>
								<td>
									<select name="prirank_cd">
									<option value="" ></option>									
									<c:forEach var="cbd" items="${combo_prirank_cd}">
										<option value="${cbd.cd}" ${cbd.cd==pgwLst.prirank_cd?"selected":""} >${cbd.nm}</option>
									</c:forEach>
									</select>
								</td>
								<td>&nbsp;</td>
								<th><span>I / F</span></th>
								<td><input type="text" name="ifc" value="${pgwLst.ifc}"></td>
							</tr>
							<tr>
								<th><span>APN ID</span></th>
								<td><input type="text" name="apn_id" value="${pgwLst.apn_id}"></td>
								<td>&nbsp;</td>
								<th><!--<span>VR ID</span>--></th>
								<td><!--<input type="text" name="vr_id2">--></td>
								<td>&nbsp;</td>
								<th><span>APN NAME</span></th>
								<td><input type="text" name="apn_name" value="${pgwLst.apn_name}"></td>
								<td>&nbsp;</td>
								<th><span>IP ALLOC</span></th>
								<td>
									<select name="ip_alloc_cd">
									<option value="" ></option>									
									<c:forEach var="cbd" items="${combo_ip_alloc_cd}">
										<option value="${cbd.cd}" ${cbd.cd==pgwLst.ip_alloc_cd?"selected":""} >${cbd.nm}</option>
									</c:forEach>									
									</select>
								</td>
							</tr>
							<tr>
								<th><span>RAD IP ALLOC</span></th>
								<td>
									<select name="rad_ip_alloc_cd">
									<option value="" ></option>									
									<c:forEach var="cbd" items="${combo_rad_ip_alloc_cd}">
										<option value="${cbd.cd}" ${cbd.cd==pgwLst.rad_ip_alloc_cd?"selected":""} >${cbd.nm}</option>
									</c:forEach>	
									</select>
								</td>
								<td>&nbsp;</td>
								<th><span>AUTH / PCRF /PCC</span></th>
								<td>
									<select name="auth_pcrf_pcc_cd">
									<option value="" ></option>									
									<c:forEach var="cbd" items="${combo_auth_pcrf_pcc_cd}">
										<option value="${cbd.cd}" ${cbd.cd==pgwLst.auth_pcrf_pcc_cd?"selected":""} >${cbd.nm}</option>
									</c:forEach>	
									</select>
								</td>
								<td>&nbsp;</td>
								<th><span>ACCT/REACT</span></th>
								<td>
									<select name="acct_react_cd">
									<option value="" ></option>									
									<c:forEach var="cbd" items="${combo_acct_react_cd}">
										<option value="${cbd.cd}" ${cbd.cd==pgwLst.acct_react_cd?"selected":""} >${cbd.nm}</option>
									</c:forEach>	
									</select>
								</td>
								<td>&nbsp;</td>
								<th><span>PRI DNS V4</span></th>
								<td><input type="text" name="pri_dns_v4" value="${pgwLst.pri_dns_v4}"></td>
							</tr>
							<tr>
								<th><span>RAD AUTH ID</span></th>
								<td>
									<input type="text" name="rad_auth_id" value="${pgwLst.rad_auth_id}">
									<ul class="inp_list">
										<li class="li">
											<em class="s_tit">ACT</em>
											<div><input type="text" name="rad_auth_act" value="${pgwLst.rad_auth_act}"></div>
										</li>
										<li class="li">
											<em class="s_tit">SBY</em>
											<div><input type="text" name="rad_auth_sby" value="${pgwLst.rad_auth_sby}"></div>
										</li>
									</ul>
								</td>
								<td>&nbsp;</td>
								<th><span>RAD ACCT ID</span></th>
								<td>
									<input type="text" name="rad_acct_id" value="${pgwLst.rad_acct_id}">
									<ul class="inp_list">
										<li class="li">
											<em class="s_tit">ACT</em>
											<div><input type="text" name="rad_acct_act" value="${pgwLst.rad_acct_act}"></div>
										</li>
										<li class="li">
											<em class="s_tit">SBY</em>
											<div><input type="text" name="rad_acct_sby" value="${pgwLst.rad_acct_sby}"></div>
										</li>
									</ul>
								</td>
								<td>&nbsp;</td>
								<th><span>DIAM PCFR ID</span></th>
								<td>
									<input type="text" name="diam_pcfr_id" value="${pgwLst.diam_pcfr_id}">
									<ul class="inp_list">
										<li class="li">
											<em class="s_tit">ACT</em>
											<div><input type="text" name="diam_pcfr_act" value="${pgwLst.diam_pcfr_act}"></div>
										</li>
										<li class="li">
											<em class="s_tit">SBY</em>
											<div><input type="text" name="diam_pcfr_sby" value="${pgwLst.diam_pcfr_sby}"></div>
										</li>
									</ul>
								</td>
								<td>&nbsp;</td>
								<th><span>LOCAL PCC PF ID</span></th>
								<td>
									<input type="text" name="local_pcc_pf_id" value="${pgwLst.local_pcc_pf_id}">
									<ul class="inp_list">
										<li class="li">
											<em class="s_tit">ACT</em>
											<div><input type="text" name="local_pcc_pf_act" value="${pgwLst.local_pcc_pf_act}"></div>
										</li>
										<li class="li">
											<em class="s_tit">SBY</em>
											<div><input type="text" name="local_pcc_pf_sby" value="${pgwLst.local_pcc_pf_sby}"></div>
										</li>
									</ul>
								</td>
							</tr>
							<tr>
								<th><span>SEC DNS V4</span></th>
								<td><input type="text" name="sec_dns_v4" value="${pgwLst.sec_dns_v4}"></td>
								<td>&nbsp;</td>
								<th><span>ARP MAP HIGH</span></th>
								<td><input type="text" name="arp_map_high" value="${pgwLst.arp_map_high}"></td>
								<td>&nbsp;</td>
								<th><span>ARP MAP MED</span></th>
								<td><input type="text" name="arp_map_med" value="${pgwLst.arp_map_med}"></td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
							</tr>
							</c:forEach>
				
						</tbody>
					</table>
					<div class="btn_area">
						<a href="#" onClick="pgwAdd()" class="btn_m btn_co03 on">추가</a>
					</div>
				</div>
				<!-- //table_form -->

				<h5 class="title"><label><input type="checkbox" name="hss_check" value="on" <c:if test="${gugDataConfig.hss_check eq 'on'}"> checked</c:if> >HSS</label></h5>
				<div class="table_form ty_flex">
					<table>
						<caption>입력 테이블</caption>
						<colgroup>
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
						</colgroup>
						<tbody>                
							<tr>
								<th><span>ID</span></th>
								<td><input type="text" name="id" value="${gugDataConfig.id}" maxlength=3></td>
								<td>&nbsp;</td>
								<th><span>APN</span></th>
								<td><input type="text" name="apn" value="${gugDataConfig.apn}" maxlength=64></td>
								<td>&nbsp;</td>
								<th><span>APNOIREPL</span></th>
								<td><input type="text" name="apnoirepl" value="${gugDataConfig.apnoirepl}" maxlength=64></td>
								<td>&nbsp;</td>
								<th><span>PLTECC</span></th>
								<td><input type="text" name="pltcc" value="${gugDataConfig.pltcc}" maxlength=5></td>
							</tr>
							<tr>
								<th><span>UUTYPE</span></th>
								<td><input type="text" name="uutype" value="${gugDataConfig.uutype}" maxlength=5></td>
								<td>&nbsp;</td>
								<th><span>MPNAME</span></th>
								<td>
									<select name="mpname_cd">
									<option value="" ></option>									
									<c:forEach var="cbd" items="${combo_mpname_cd}">
										<option value="${cbd.cd}" ${cbd.cd==gugDataConfig.mpname_cd?"selected":""} >${cbd.nm}</option>
									</c:forEach>	
									</select>
								</td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //table_form ${gugDataConfig.view_gubun}-->
				<div class="btn_area ty02">
				    <c:if test="${gugDataConfig.view_gubun =='CREATE'}">
					        <a href="#" onClick="data_write('D010')" class="btn_l btn_co01">임시저장</a>
					</c:if>
					<c:if test="${gugDataConfig.view_gubun =='EDIT'}">
					 
					 	<c:if test="${gugDataConfig.work_state_cd eq 'D010'}">
						        <a href="#" onClick="data_write('D010')" class="btn_l btn_co01">임시저장</a>
						</c:if>
						<c:if test="${gugDataConfig.work_state_cd eq 'D020' || gugDataConfig.work_state_cd eq 'D030'}">
						        <a href="#" onClick="data_change()" class="btn_l btn_co01">변경저장</a>
						</c:if>
						<c:if test="${gugDataConfig.work_state_cd =='D010' || gugDataConfig.work_state_cd =='D030' }">
						
								<a href="#" onClick="data_write('D020')"class="btn_l btn_co01">입력완료</a>
						</c:if>
						<a href="/gugDataConfig/copy/${gugDataConfig.gug_data_manage_no}" class="btn_l btn_co01">국데이터 복사</a>
						<c:if test="${gugDataConfig.work_state_cd !='D040' && gugDataConfig.work_state_cd !='D050' && gugDataConfig.work_state_cd !='D060'  }">
						<a href="#" onClick="data_delete()" class="btn_l btn_co02">삭제</a>
						</c:if>
					</c:if>
				</div><!-- //btn_area -->
				<c:if test="${gugDataConfig.view_gubun =='EDIT'}">
				<h3 class="title"><span>이력조회</span></h3>
				<div class="table_ty">
					<table>
						<caption>목록 테이블</caption>
						<colgroup>
							<col span="1" style="width:15%;">
							<col span="1" style="width:*;">
							<col span="5" style="width:12%;">
						</colgroup>
						<thead>
							<tr>
								<th scope="col">국데이터 상태</th>
								<th scope="col">변경일자</th>
								<th scope="col">변경자/구분</th>
								<th scope="col">항목</th>
								<th scope="col">변경전</th>
								<th scope="col">변경후</th>
								<th scope="col">변경사유</th>
							</tr>
						</thead>
						<tbody id="trData" style="border:0px">
				        <c:forEach var="board" items="${his_list}">
							 <c:choose>
							 <c:when test="${fn:length(board.work_state_nm)>2}">
							      <tr style="font-weight:bold;">
							  </c:when>
				               <c:otherwise>
							       <tr style="font-weight:normal;">
							  </c:otherwise>
							  </c:choose>
							    <td>${board.work_state_nm}</td>
								<td><fmt:formatDate value="${board.modify_dtm}" pattern="yyyy.MM.dd hh:mm" /></td>
								<td>${board.modify_id}</td>
								<td>${board.chng_col_nm}</td>
								<td>${board.prev_col_val}</td>
								<td>${board.next_col_val}</td>

								<td><span title="${board.change_reason}">
								   <c:choose>

									 <c:when test="${fn:length(board.change_reason)>5}">
									      ${fn:substring(board.change_reason,0,5)}...
									  </c:when>
									 <c:when test="${fn:length(board.change_reason)==0}">
									      
									 </c:when>
						               <c:otherwise>
									  ${board.change_reason}
									  </c:otherwise>
									  </c:choose>
								</span></td>
							</tr>
						 </c:forEach>
				         </tbody>	

					</table>
				</div><!-- //table_ty -->
				
				<div class="paginate">
					    <!-- **처음페이지로 이동 : 현재 페이지가 1보다 크면  [처음]하이퍼링크를 화면에 출력-->
				    <c:if test="${commonPager.curBlock > 1}">
					<a href="#" onClick="list('1')" class="first">처음 페이지</a>
					</c:if>
				
					
					<!-- **이전페이지 블록으로 이동 : 현재 페이지 블럭이 1보다 크면 [이전]하이퍼링크를 화면에 출력 -->
				   <c:if test="${commonPager.curBlock > 1}">
					<a href="#" onClick="list('${commonPager.prevPage}')" class="prev">이전 페이지</a>
					</c:if>

					
					<ul>
						<!-- **하나의 블럭에서 반복문 수행 시작페이지부터 끝페이지까지 -->
		                <c:forEach var="num" begin="${commonPager.blockBegin}" end="${commonPager.blockEnd}">
		                    <!-- **현재페이지이면 하이퍼링크 제거 -->
		                    <c:choose>
		                        <c:when test="${num == commonPager.curPage}">
		                            <li class="select"><a href="#">${num}</a></li>
		                        </c:when>
		                        <c:otherwise>
		                            <li><a href="#" onClick="list('${num}')">${num}</a></li>
		                        </c:otherwise>
		                    </c:choose>
		                </c:forEach>					

					</ul>
					
					<!-- **다음페이지 블록으로 이동 : 현재 페이지 블럭이 전체 페이지 블럭보다 작거나 같으면 [다음]하이퍼링크를 화면에 출력 -->
	                <c:if test="${commonPager.curBlock < commonPager.totBlock}">
					<a href="#" onClick="list('${commonPager.nextPage}')" class="next">다음 페이지</a>
					</c:if>
					
					<!-- **끝페이지로 이동 : 현재 페이지가 전체 페이지보다 작거나 같으면 [끝]하이퍼링크를 화면에 출력 -->
	                <c:if test="${commonPager.curPage < commonPager.totPage}">
					<a href="#" onClick="list('${commonPager.totPage}')" class="last">마지막 페이지</a>
					</c:if>
				</div><!-- //paginate -->
				</c:if>
				</form:form>
			</div><!-- //contents -->
		</div><!-- //contents_wrap -->
	</div><!-- //container -->
	
	<footer>
		<nav class="footer_nav">
			<ul>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">이용약관</a></li>
				<li><a href="#">개인정보처리방침</a></li>
				<li><a href="#">Sitemap</a></li>
			</ul>
		</nav><!-- //footer_nav -->
		<address>㈜케이티 대표이사 황창규 경기도 성남시 분당구 불정로 90 (정자동) 사업자등록번호 : 102-81-42945 통신판매업신고 : 2002-경기성남-0047</address>
	</footer>
	
</div><!-- //wrap -->

<div class="popup pop_progress" id="pop_data_change">
	<div class="pop_tit">국데이터 변경</div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">
			
			<div class="table_form vTop">
				<table>
					<caption>입력 테이블</caption>
					<colgroup>
						<col span="1" style="width:30%;">
						<col span="1" style="width:*;">
					</colgroup>
					<tbody>
						<tr>
							<th><span>로그인 유저</span></th>
							<td>user1</td>
						</tr>
						<tr>
							<th><span>비밀번호</span></th>
							<td><input type="password" name="im_pwdd"></td>
						</tr>
						<tr>
							<th><span>변경사유</span></th>
							<td><textarea cols="3" rows="5" name="im_change_reason"></textarea></td>
						</tr>
					</tbody>	
				</table>
			</div><!-- //table_form -->
			
		</div><!-- //scrollbar-inner -->
		
		<div class="btn_area">
			<a href="#" onClick="data_write('D030');pop_close('#pop_data_change');" class="btn_l btn_co01">변경</a>
		</div>
	</div><!-- //pop_con_wrap -->
	<a href="javascript:void(0);" class="pop_close">팝업닫기</a>
</div><!-- //popup -->


<div class="popup pop_ex_command" id="pop_ex_command">
	<div class="pop_tit">실행 명령어 선택</div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">
			
			<div class="command_area">
				<div class="box">
					<strong class="tit">Select Command List</strong>
					<div class="cont">
						<div class="scrollbar-inner">
							<ul id="left_commands">
								<li><a href="#none">CRTE-VR</a></li>
								<li><a href="#none">RTRV-VR-INF</a></li>
								<li><a href="#none">CRTE-IP-POOL</a></li>
								<li><a href="#none">RTRV-IP-POOL</a></li>
								<li><a href="#none">CRTE-VLAN-INTF</a></li>
								<li><a href="#none">CRTE-INTF-IP</a></li>
								<li><a href="#none">RTRV-INTF-IP</a></li>
								<li><a href="#none">CRTE-IP-ROUTE</a></li>
								<li><a href="#none">RTRV-IP-ROUTE</a></li>
								<li><a href="#none">CRTE-APN</a></li>
								<li><a href="#none">COPY-APN</a></li>
								<li><a href="#none">RTRV-APN-INF</a></li>
								<li><a href="#none">CHG-APN-INF</a></li>
								<li><a href="#none">CHG-APN-SVC</a></li>
								<li><a href="#none">CHG-APN-QOS</a></li>
								<li><a href="#none">CRTE-VAPN-CC</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="btn_move">
					<button type="button" onClick="right_move()" class="next">다음</button>
					<button type="button" onClick="left_move()" class="before">이전</button>
				</div>
				<div class="box right">
					<strong class="tit">Run Command List</strong>
					<div class="cont">
						<div class="scrollbar-inner">
							<ul id="right_commands">
								<li><a href="#none">CRTE-VR</a></li>
								<li><a href="#none">CRTE-IP-POOL</a></li>
								<li><a href="#none">CRTE-VLAN-INTF</a></li>
								<li><a href="#none">CRTE-INTF-IP</a></li>
								<li><a href="#none">CRTE-IP-ROUTE</a></li>
								<li><a href="#none">CRTE-APN</a></li>
								<li><a href="#none">CRTE-VAPN-CC</a></li>
								<li><a href="#none">CRTE-VR</a></li>
								<li><a href="#none">CRTE-IP-POOL</a></li>
								<li><a href="#none">CRTE-VLAN-INTF</a></li>
								<li><a href="#none">CRTE-INTF-IP</a></li>
								<li><a href="#none">CRTE-IP-ROUTE</a></li>
								<li><a href="#none">CRTE-APN</a></li>
								<li><a href="#none">CRTE-VAPN-CC</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div><!-- //command_area -->
			
		</div><!-- //scrollbar-inner -->
		
		<div class="btn_area">
			<a href="javascript:pop_close('.popup');" class="btn_l btn_co01">확인</a>
		</div>
	</div><!-- //pop_con_wrap -->
	<a href="javascript:void(0);" class="pop_close">팝업닫기</a>
</div><!-- //popup -->
</body>
</html>