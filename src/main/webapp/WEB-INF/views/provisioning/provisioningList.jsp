<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content= "text/html; charset=UTF-8"> 
<meta name="format-detection" content="telephone=no">
<title>KT E2E Infra Orchestrator</title>
<link rel="shortcut icon" href="/new_resources/resources/images/layout/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="/new_resources/resources/css/style.css">
<script type="text/javascript" src="/new_resources/resources/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/template.js"></script>
<script type="text/javascript" src="/resources/scripts/provisioning/provisioningListMain.js"></script>
<script type="text/javascript" src="/resources/scripts/utils/viewControlUtil.js"></script>
</head>

<body>
<div id="wrap">
    
    <header>
		<h1 class="logo"><a href="#">KT E2E Infra Orchestrator</a></h1>
		<nav id="gnb">
			<ul>
				<li><a href="/oneView/list">DESIGN</a></li>
				<li class="active"><a href="/provisioning">PROVISIONING</a></li>
				<li><a href="#">MONITORING</a></li>
				<li><a href="#">INVENTORY</a></li>
				<li><a href="#">CONFIGURATION</a></li>
				<li>
					<a href="#">P-LTE</a>
					<ul>
						<li><a href="/gugDataConfig">국데이터 관리</a></li><!-- 활성화 class="active" -->
						<li><a href="/gugPcmdCrExec">명령 실행</a></li>		
						<li><a href="/indPcmdCrExec">명령 개별 실행</a></li>
						<li><a href="/cust">고객사 관리</a></li>
						<li><a href="/equipInven">장비 관리</a></li>
						<li><a href="#">명령어 관리</a></li>
						<li><a href="#">모니터링</a></li>
						<li><a href="#">이력조회</a></li>
					</ul>
				</li>
			</ul>
		</nav><!-- //gnb -->
		<div class="ad_info">
			<i><img src="/new_resources/resources/images/layout/img_ad_info.jpg" alt=""></i><!-- image size : 25*25px -->
			<p>admin</p>
		</div><!-- //ad_info -->
    </header>
    
    <div id="container">
		
		<ul id="navigation">
			<li><img src="/new_resources/resources/images/layout/img_navigation_home.png" alt="home"></li>
			<li>PROVISIONING</li>
		</ul><!-- //navigation -->
		
		<div id="contents_wrap">
			<div class="contents">
			
				<div class="search_area">
					<div class="inner">
						<table>
							<caption>검색 테이블</caption>
							<colgroup>
								<col span="1" style="width:15%;">
								<col span="1" style="width:*;">
							</colgroup>
							<tbody>
								<tr>
									<td>
										<select id="search_option" >
											<option value="ns_name" >NS Name</option>
											<option value="status">Status</option>
										</select>
									</td>
									<td><input id="search_keyword" type="text"></td>
								</tr>
							</tbody>	
						</table>
						<div class="btn_area" id="search">
							<a href="#" class="btn_l btn_co01 on">조회하기</a>
						</div>
					</div>
				</div><!-- //search_area -->
				
				<div class="table_ty">
					<div class="table_head">
						<div class="count">Total Count <span>0</span></div>
						<ul class="filter_output">
							<li><input type="checkbox"><em class="cate">4G</em><a href="#"><strong class="num">0</strong></a></li>
							<li><input type="checkbox"><em class="cate">5G</em><a href="#"><strong class="num">0</strong></a></li>
							<li><input type="checkbox"><em class="cate">P-LTE</em><a href="#"><strong class="num">0</strong></a></li>
						</ul>
						<div class="btn_area" id="nsInsCreate">
							<a href="#" class="btn_l btn_co01">NS INS 생성</a>
						</div>
					</div>
					<table>
						<caption>목록 테이블</caption>
						<colgroup>
							<col span="1" style="width:5%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:15%;">
							<col span="2" style="width:15%;">
							<col span="1" style="width:*%;">
							<col span="1" style="width:15%;">
						</colgroup>
						<thead>
							<tr>
								<th scope="col">No.</th>
								<th scope="col">분류</th>
								<th scope="col">
									NS Name
									<div class="btn_align">
										<a href="#" class="up">up</a>
										<a href="#" class="down">down</a>
									</div>
								</th>
								<th scope="col">
									NSD Version
									<div class="btn_align">
										<a href="#" class="up">up</a>
										<a href="#" class="down">down</a>
									</div>
								</th>
								<th scope="col">NS Description</th>
								<th scope="col">
									Status
									<div class="btn_align">
										<a href="#" class="up">up</a>
										<a href="#" class="down">down</a>
									</div>
								</th>
								<th scope="col">
									Last Event Time
									<div class="btn_align">
										<a href="#" class="up">up</a>
										<a href="#" class="down">down</a>
									</div>
								</th>
							</tr>
						</thead>
						<tbody id="tbody">
						</tbody>
					</table>
				</div><!-- //table_ty -->
				
				<div class="paginate">
					<a href="#" class="first">처음 페이지</a>
					<a href="#" class="prev">이전 페이지</a>
					<ul>
						<li class="select"><a href="#">1</a></li>
					</ul>
					<a href="#" class="next">다음 페이지</a>
					<a href="#" class="last">마지막 페이지</a>
				</div><!-- //paginate -->
				
			</div><!-- //contents -->
		</div><!-- //contents_wrap -->
	</div><!-- //container -->
	
	<footer>
		<nav class="footer_nav">
			<ul>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">이용약관</a></li>
				<li><a href="#">개인정보처리방침</a></li>
				<li><a href="#">Sitemap</a></li>
			</ul>
		</nav><!-- //footer_nav -->
		<address>㈜케이티 대표이사 황창규 경기도 성남시 분당구 불정로 90 (정자동) 사업자등록번호 : 102-81-42945 통신판매업신고 : 2002-경기성남-0047</address>
	</footer>
</div><!-- //wrap -->
<script>
$( window ).load( function() {
 	var count 			= '<c:out value="${count}"/>';
 	var searchOption	= '<c:out value="${search_option}"/>';
	var dataList 		= [];
	var columnList 		= [ "no", "ns_type_name", "ns_name", "nsdIdVerion", "ns_desc", "ns_state", "created_at" ];

	<c:forEach var="board" items="${list}" varStatus ="status">

	var data = {};
		data.no 			= "${board.no}";
		data.id 			= "${board.id}";
		data.ns_type 		= "${board.ns_type}";
		data.nsd_id 		= "${board.nsd_id}";
		data.ns_name 		= "${board.ns_name}";
		data.ns_desc 		= "${board.ns_desc}";
		data.ns_state 		= "${board.ns_state}";
		data.version		= "${board.version}";
		data.nsd_name		= "${board.nsd_name}";
		data.ns_ins_id		= "${board.ns_ins_id}";
		data.nsdIdVerion	= "${board.nsd_name} ${board.version}";
		data.created_at		= "${board.created_at}";
		data.key			= "${board.id}";
	
		data.ns_type_name	= checkNSType( "${board.ns_type}" );
	
	dataList[ "${status.index}" ] = data;
	//	dataList[ "${status.index}" ] = { no:"${board.no}", id:"${board.id}", ns_type:"${board.ns_type}", nsd_id: "${board.nsd_id}", ns_name: "${board.ns_name}", ns_desc: "${board.ns_desc}", ns_state: "${board.ns_state}", version: "${board.version}", nsd_name: "${board.nsd_name}", ns_ins_id: "${board.ns_ins_id}", nsdIdVerion: "${board.nsd_name} ${board.version}", created_at:"${board.created_at}", key:"${board.id}" };
	</c:forEach>	

	var pageList = [];
	<c:forEach var="num" begin="${commonPager.blockBegin}" end="${commonPager.blockEnd}" varStatus="status">
		pageList[ "${status.index}" ] = "<li>${num}</li>";
	</c:forEach>	
	
	initProvisioningList( count, searchOption, dataList, columnList, pageList );
});

</script>
</body>
</html>