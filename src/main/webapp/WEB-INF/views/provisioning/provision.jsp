<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<title>KT E2E Infra Orchestrator</title>
<link rel="shortcut icon" href="/resources/images/layout/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="/resources/css/style.css">
<script type="text/javascript" src="/new_resources/resources/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/template.js"></script>
<script type="text/javascript" src="/resources/scripts/provisioning/provisioningMain.js"></script>
<script type="text/javascript" src="/resources/scripts/provisioning/provisioningThreshold.js"></script>
<script type="text/javascript" src="/resources/scripts/utils/viewControlUtil.js"></script>
 
<script src="/resources/scripts/draw2d.v6/shifty.js"></script>
<script src="/resources/scripts/draw2d.v6/raphael.js"></script>
<script src="/resources/scripts/draw2d.v6/jquery.autoresize.js"></script>
<script src="/resources/scripts/draw2d.v6/jquery-touch_punch.js"></script>
<script src="/resources/scripts/draw2d.v6/jquery.contextmenu.js"></script>
<script src="/resources/scripts/draw2d.v6/rgbcolor.js"></script>
<script src="/resources/scripts/draw2d.v6/canvg.js"></script>
<script src="/resources/scripts/draw2d.v6/Class.js"></script>
<script src="/resources/scripts/draw2d.v6/json2.js"></script>
<script src="/resources/scripts/draw2d.v6/pathfinding-browser.min.js"></script>
<script src="/resources/scripts/draw2d.v6/draw2d.js"></script>
<script src="/resources/scripts/draw2d.v6/draw2d.js"></script>
<script src="/resources/scripts/editor/Config.js"></script>
<script src="/resources/scripts/editor/Components.js"></script>
<script src="/resources/scripts/editor/Apps.js"></script>
<script src="/resources/scripts/application.js"></script>
<script src="/resources/scripts/utils/jsrender.js"></script>
<script src="/resources/scripts/editor/jquery.ajax-cross-origin.min.js"></script>

</head>

<body>
<div id="wrap">
    
    <header>
		<h1 class="logo"><a href="#">KT E2E Infra Orchestrator</a></h1>
		<nav id="gnb">
			<ul>
				<li><a href="/oneView/list">DESIGN</a></li>
				<li class="active"><a href="/provisioning/">PROVISIONING</a></li>
				<li><a href="#">MONITORING</a></li>
				<li><a href="#">INVENTORY</a></li>
				<li><a href="#">CONFIGURATION</a></li>
				<li>
					<a href="#">P-LTE</a>
					<ul>
						<li><a href="#">국데이터 관리</a></li>
						<li><a href="#">명령 실행</a></li>
						<li><a href="#">명령 개별 실행</a></li>
						<li><a href="#">고객사 관리</a></li>
						<li><a href="#">장비 관리</a></li>
						<li><a href="#">명령어 관리</a></li>
						<li><a href="#">모니터링</a></li>
						<li><a href="#">이력조회</a></li>
					</ul>
				</li>
			</ul>
		</nav><!-- //gnb -->
		<div class="ad_info">
			<i><img src="/new_resources/resources/images/layout/img_ad_info.jpg" alt=""></i><!-- image size : 25*25px -->
			<p>admin</p>
		</div><!-- //ad_info -->
    </header>
    
    <div id="container">	
		
		<ul id="navigation">
			<li><img src="/new_resources/resources/images/layout/img_navigation_home.png" alt="home"></li>
			<li>DESIGN</li>
		</ul><!-- //navigation -->
		
		<div id="contents_wrap" class="design_wrap"><!-- design_wrap -->
		
			<div id="lnb" class="lnb_fold">
				<div class="lnb_search">
					<div class="inner">
						<input type="text" placeholder="NSD명을 입력해주세요">
						<a href="#" class="btn">검색</a>
					</div>
				</div><!-- //lnb_search -->
				<ul class="lnb_nav">
					<li>
						<a href="#">NS List</a>
						<div class="lnb_dep menu_list"><!-- menu_list -->
							<div class="scrollbar-inner">
								<ul>
								</ul>
							</div>
						</div>
					</li>
				</ul>
				<button type="button" class="btn_act">left slide button</button>
			</div><!-- //lnb -->
			
			<div class="contents">
				<nav class="topology_btn">
						<a href="#" id="manualScale" class="btn_l btn_co01">Manual Scale</a>
						<a href="#" id="nsSave" class="btn_l btn_co01">Save</a>
						<a href="#" id="nsProvisioning" class="btn_l btn_co02">Provisioning</a>
						<a href="#" id="nsEnable" class="btn_l btn_co02">Enable</a>
						<a href="#" id="nsTerminate" class="btn_l btn_co02">Terminate</a>					
				</nav>
				<div class="topology_canvas">
<!-- 					<ul class="control_nav"> -->
<!-- 						<li class="prev"><a href="#">이전</a></li> -->
<!-- 						<li class="next"><a href="#">다음</a></li> -->
<!-- 					</ul> -->
					<div  id="canvas"></div>
<!-- 					<div  id="test" style="border:1px solid red; width:1500px; height:900px;"></div> -->
					
				</div><!-- topology_canvas -->

				<form:form commandName="ProvisionComponent" name="form" action="" onsubmit="return false;">
				<input type="hidden" id="nsId" value="${nsId}">
				<input type="hidden" id="nsdUuid" value="${nsdUuid}">
				<div class="palette" >
					<div class="inner">
						<ul class="tab tab_nav"> <!-- class="tab_nav" -->
							<li class="on"><a href="#tab01">General Info</a></li>
							<!-- 라니안 임시 주석 -->
<!-- 							<li><a href="#tab02">NS Config Params</a></li> -->
<!-- 							<li><a href="#tab03">VNF Config Params</a></li> -->
							<li><a href="#tab04">Threshold</a></li>
							<li><a href="#tab05">Order Tracking</a></li>
						</ul><!-- //tab -->
						<div class="tab_con_area scrollbar-inner">
							<div class="tab_con on" id="tab01">
								<div class="table_form03">
									<table>
										<caption>입력 테이블</caption>
										<colgroup>
											<col span="1" style="width:22%;">
											<col span="1" style="width:*;">
											<col span="1" style="width:22%;">
											<col span="1" style="width:*;">
										</colgroup>
										<tbody>
											<tr>
												<th><span>NSD</span></th>
												<td colspan="3">
													<select id="nsdCombo">
													</select>
												</td>
											</tr>
											<tr>
												<th><span>NSD ID</span></th>
												<td colspan="3" id="nsdId">${nsdId}</td>
											</tr>
											<tr>
												<th><span>NS Name</span><i class="required">필수항목</i></th>
												<td colspan="3"><input id="nsName" type="text" value="${nsName}"></td>
											</tr>
											<tr>
												<th><span>NS Instance ID</span></th>
												<td colspan="3" id="nsInsId" >${nsInsId}</td>
											</tr>
											<tr>
												<th><span>Target NFVO</span></th>
												<td colspan="3">
<!-- 													<div class="txt_list"> -->
<!-- 														<span class="txt">123E4567-E89B-12D3-A456-426655441111</span> -->
<!-- 														<span class="btn"> -->
<!-- 														<a href="#" class="btn_m btn_co03 on">VIM 선택</a> -->
<!-- 														</span> -->
<!-- 													</div> -->
													<select id="targetNFVO">
													</select>
													
												</td>
											</tr>
											<tr>
												<th><span>Start Time</span></th>
												<td colspan="3">
													<div class="date_time_area">
														<span class="date">
															<input type="text" id="startTime_year" class="select_date">
														</span>
														<span class="time_area">
															<input type="text" id="startTime_hour" value="01:00">
															<span class="b_br">:</span>
															<input type="text" id="startTime_minuate" value="05:00">
															<span class="b_br">:</span>
															<input type="text" id="startTime_second" value="05:00">
														</span>
													</div>
												</td>
											</tr>
											<tr>
												<th><span>NS Description</span></th>
												<td colspan="3">
													<textarea id="nsDesc" cols="30" rows="2" placeholder="비고사항">${nsDesc}</textarea>
												</td>
											</tr>
										</tbody>
									</table>
								</div><!-- //table_form03 -->

								<h6 class="title">NS Component</h6>
								<div class="table_ty">
									<table>
										<caption>목록 테이블</caption>
										<colgroup>
											<col span="1" style="width:20%;">
											<col span="1" style="width:30%;">
											<col span="1" style="width:*;">
<%-- 											<col span="2" style="width:22%;"> --%>
										</colgroup>
										<thead>
											<tr>
												<th scope="col" rowspan="2">Type</th>
												<th scope="col" rowspan="2">Profile Name</th>
												<th scope="col" rowspan="2">VNFD Name</th>
<!-- 												<th scope="col" colspan="2">Affinity or AntiAffinity Rule</th> -->
											</tr>
											<!-- 라니안 -->
<!-- 											<tr> -->
<!-- 												<th scope="col" class="btml">NFVI NODE</th> -->
<!-- 												<th scope="col" class="btml">NFVI POP</th> -->
<!-- 											</tr> -->
										</thead>
										<tbody id = "nsComponent">
<!-- 											<tr> -->
<!-- 												<td>4g</td> -->
<!-- 												<td>4G Demo</td> -->
<!-- 												<td>123E4567-E89B-12D3-A456- 426655442222</td> -->
<!-- 												<td> -->
<!-- 													<select> -->
<!-- 														<option>Affinity</option> -->
<!-- 														<option>Anti-affinity</option> -->
<!-- 													</select> -->
<!-- 												</td> -->
<!-- 												<td> -->
<!-- 													<select> -->
<!-- 														<option>NFVI Node</option> -->
<!-- 														<option>NFVI Pop</option> -->
<!-- 														<option>etc</option> -->
<!-- 													</select> -->
<!-- 												</td> -->
<!-- 											</tr> -->
<!-- 											<tr> -->
<!-- 												<td>4g</td> -->
<!-- 												<td>4G Demo</td> -->
<!-- 												<td>123E4567-E89B-12D3-A456- 426655442222</td> -->
<!-- 												<td> -->
<!-- 													<select> -->
<!-- 														<option>Affinity</option> -->
<!-- 														<option>Anti-affinity</option> -->
<!-- 													</select> -->
<!-- 												</td> -->
<!-- 												<td> -->
<!-- 													<select> -->
<!-- 														<option>NFVI Node</option> -->
<!-- 														<option>NFVI Pop</option> -->
<!-- 														<option>etc</option> -->
<!-- 													</select> -->
<!-- 												</td> -->
<!-- 											</tr> -->
										</tbody>
									</table>
								</div><!-- //table_ty -->

								<h6 class="title">NS Deployment Flavor</h6><!-- 180117 수정 -->
								<div class="table_ty">
									<table>
										<caption>목록 테이블</caption>
										<colgroup>
											<col span="2" style="width:50%;">
										</colgroup>
										<thead>
											<tr>
												<th scope="col">Flavor Name</th>
												<th scope="col">NS Instance Level ID</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													<select id="nsDeploymentFlavor">
													</select>
												</td>
												<td><input type="text" id="nsInstanceLevelId" value="None"></td>
											</tr>
										</tbody>
									</table>
								</div><!-- //table_ty -->
							</div><!-- //tab_con -->
							<div class="tab_con" id="tab02">
								tab02
							</div><!-- //tab_con -->
							<div class="tab_con" id="tab03">
								tab03
							</div><!-- //tab_con -->
							<div class="tab_con" id="tab04">
								<div class="table_ty">
									<div class="table_head">
										<div class="btn_area">
											<a href="#" class="btn_m btn_co01">Save</a>
											<a href="#" class="btn_m btn_co01">Add</a>
											<a href="#" class="btn_m btn_co02">Delete</a>
										</div>
									</div>
									<table>
										<caption>목록 테이블</caption>
										<colgroup>
											<col span="1" style="width:6%;">
											<col span="1" style="width:*;">
											<col span="3" style="width:20%;">
										</colgroup>
										<thead>
											<tr>
												<th scope="col"><input type="checkbox"></th>
												<th scope="col">Metric</th>
												<th scope="col">Scale Type</th>
												<th scope="col">Value</th>
												<th scope="col">Duration(sec)</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><input type="checkbox"></td>
												<td>CPU.usage</td>
												<td>Scale In</td>
												<td>30</td>
												<td>1000</td>
											</tr>
											<tr>
												<td><input type="checkbox"></td>
												<td>CPU.usage</td>
												<td>Scale In</td>
												<td>30</td>
												<td>1000</td>
											</tr>
											<tr>
												<td><input type="checkbox"></td>
												<td>CPU.usage</td>
												<td>Scale In</td>
												<td>30</td>
												<td>1000</td>
											</tr>
										</tbody>
									</table>
								</div><!-- //table_ty -->
							</div><!-- //tab_con -->
							<div class="tab_con" id="tab05">
								<div class="time_list_area">
									<h6 class="title" id ="orderTrackingNSName">${nsName}</h6>
									<ul class="time_list">
										<li>
											<strong class="tit">Provisioning Start Time</strong>
											<span class="txt"  id="provisioningStartTime"></span>
										</li>
										<li>
											<strong class="tit">Provisioning End Time</strong>
											<span class="txt"  id="provisioningEndTime"></span>
										</li>
										<li>
											<strong class="tit">Duration</strong>
											<span class="txt"  id="provisioningDuration"></span>
										</li>
										<li>
											<strong class="tit">Polling refresh</strong>
											<span class="txt"  id="provisioningPollimgRefresh"></span>
										</li>
									</ul>
								</div><!-- //time_list_area -->
								<div class="step_ty02 ty02">
									<ol>
										<li class="on" id="trackingState_validation" >
											<i>1</i>
											<strong class="word">Validation</strong>
										</li>
										<li id="trackingState_createNS">
											<i>2</i>
											<strong class="word">Create NS</strong>
										</li>
										<li id="trackingState_instantiateNS">
											<i>3</i>
											<strong class="word">Instantiate<br>NS</strong>
										</li>
										<li id="trackingState_instantiateNSStart">
											<i>4</i>
											<strong class="word">Instantiate<br>NS Start</strong>
										</li>
										<li id="trackingState_instantiateNSResult">
											<i>5</i>
											<strong class="word">Instantiate<br>NS Result</strong>
										</li>
									</ol>
								</div><!-- //step_ty02 -->
								<div class="table_ty">
									<table>
										<caption>목록  테이블</caption>
										<colgroup>
											<col span="1" style="width:*;">
											<col span="2" style="width:13%;">
											<col span="2" style="width:24%;">
											<col span="1" style="width:16%;">
										</colgroup>
										<thead>
											<tr>
												<th scope="col">Event</th>
												<th scope="col">Direction</th>
												<th scope="col">Result</th>
												<th scope="col">Start Time</th>
												<th scope="col">End Time</th>
												<th scope="col">Duration(sec)</th>
											</tr>
										</thead>
										<tbody id="trackingListTbody">
										</tbody>
									</table>
								</div><!-- //table_ty -->
							</div><!-- //tab_con -->
						</div><!-- //scrollbar-inner -->
					</div>
					<button type="button" class="btn_act">palette slide button</button>
				</div><!-- //palette -->
				</form:form>
			</div><!-- //contents -->

		</div><!-- //contents_wrap -->
	</div><!-- //container -->
	
	<footer class="footer02">
		<nav class="footer_nav">
			<ul>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">이용약관</a></li>
				<li><a href="#">개인정보처리방침</a></li>
				<li><a href="#">Sitemap</a></li>
			</ul>
		</nav><!-- //footer_nav -->
		<address>㈜케이티 대표이사 황창규 경기도 성남시 분당구 불정로 90 (정자동) 사업자등록번호 : 102-81-42945 통신판매업신고 : 2002-경기성남-0047</address>
	</footer>
	
</div><!-- //wrap -->


<div class="popup_mask"></div>

<div class="popup pop_manual_scale" id="popManualScale">
	<div class="pop_tit">Manual Scale</div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">
			
			<div class="scale_tit_area">
				<strong class="tit">Scale Type</strong>
				<div class="sel_area">
					<select id="scaleType">
						<option value="scaleNs">SCALE_NS</option>
						<option value="scaleVnf">SCALE_VNF</option>
					</select>
				</div>
				<div class="btn_area">
					<a href="#" id="buttonScale" class="btn_m btn_co01">Scale</a>
					<a href="#" id="buttonAdd" class="btn_m btn_co01">Add</a>
					<a href="#" id="buttonDelete" class="btn_m btn_co02">Delete</a>
				</div>
			</div>

			<div class="table_ty">
				<table>
					<caption>목록 테이블</caption>
					<colgroup>
						<col span="1" style="width:7%;">
						<col span="3" style="width:28%;">
						<col span="1" style="width:*;">
					</colgroup>
					<thead>
						<tr>
							<th scope="col"><input type="checkbox"></th>
							<th scope="col">VNF Instance</th>
							<th scope="col">Scale Type</th>
							<th scope="col">Aspect ID</th>
							<th scope="col">Step</th>
						</tr>
					</thead>
					<tbody id="scalePopTbody">
						<tr>
							<td><input type="checkbox"></td>
							<td>
								<select>
									<option>GURO_MME#1</option>
								</select>
							</td>
							<td>
								<select>
									<option>Scale Out</option>
								</select>
							</td>
							<td><input type="text" value="NS-ASP-001"></td>
							<td><input type="text" value="1" class="ta_c"></td>
						</tr>
						<tr>
							<td><input type="checkbox"></td>
							<td>
								<select>
									<option>GURO_MME#1</option>
								</select>
							</td>
							<td>
								<select>
									<option>Scale Out</option>
								</select>
							</td>
							<td><input type="text" value="NS-ASP-001"></td>
							<td><input type="text" value="2" class="ta_c"></td>
						</tr>
						<tr>
							<td><input type="checkbox"></td>
							<td>
								<select>
									<option>GURO_MME#1</option>
								</select>
							</td>
							<td>
								<select>
									<option>Scale Out</option>
								</select>
							</td>
							<td><input type="text" value="NS-ASP-001"></td>
							<td><input type="text" value="3" class="ta_c"></td>
						</tr>
						<tr>
							<td><input type="checkbox"></td>
							<td>
								<select>
									<option>GURO_MME#1</option>
								</select>
							</td>
							<td>
								<select>
									<option>Scale Out</option>
								</select>
							</td>
							<td><input type="text" value="NS-ASP-001"></td>
							<td><input type="text" value="4" class="ta_c"></td>
						</tr>
						<tr>
							<td><input type="checkbox"></td>
							<td>
								<select>
									<option>GURO_MME#1</option>
								</select>
							</td>
							<td>
								<select>
									<option>Scale Out</option>
								</select>
							</td>
							<td><input type="text" value="NS-ASP-001"></td>
							<td><input type="text" value="5" class="ta_c"></td>
						</tr>
						<tr>
							<td><input type="checkbox"></td>
							<td>
								<select>
									<option>GURO_MME#1</option>
								</select>
							</td>
							<td>
								<select>
									<option>Scale Out</option>
								</select>
							</td>
							<td><input type="text" value="NS-ASP-001"></td>
							<td><input type="text" value="5" class="ta_c"></td>
						</tr>
						<tr>
							<td><input type="checkbox"></td>
							<td>
								<select>
									<option>GURO_MME#1</option>
								</select>
							</td>
							<td>
								<select>
									<option>Scale Out</option>
								</select>
							</td>
							<td><input type="text" value="NS-ASP-001"></td>
							<td><input type="text" value="5" class="ta_c"></td>
						</tr>
						<tr>
							<td><input type="checkbox"></td>
							<td>
								<select>
									<option>GURO_MME#1</option>
								</select>
							</td>
							<td>
								<select>
									<option>Scale Out</option>
								</select>
							</td>
							<td><input type="text" value="NS-ASP-001"></td>
							<td><input type="text" value="5" class="ta_c"></td>
						</tr>
						<tr>
							<td><input type="checkbox"></td>
							<td>
								<select>
									<option>GURO_MME#1</option>
								</select>
							</td>
							<td>
								<select>
									<option>Scale Out</option>
								</select>
							</td>
							<td><input type="text" value="NS-ASP-001"></td>
							<td><input type="text" value="5" class="ta_c"></td>
						</tr>
						<tr>
							<td><input type="checkbox"></td>
							<td>
								<select>
									<option>GURO_MME#1</option>
								</select>
							</td>
							<td>
								<select>
									<option>Scale Out</option>
								</select>
							</td>
							<td><input type="text" value="NS-ASP-001"></td>
							<td><input type="text" value="5" class="ta_c"></td>
						</tr>
						<tr>
							<td><input type="checkbox"></td>
							<td>
								<select>
									<option>GURO_MME#1</option>
								</select>
							</td>
							<td>
								<select>
									<option>Scale Out</option>
								</select>
							</td>
							<td><input type="text" value="NS-ASP-001"></td>
							<td><input type="text" value="5" class="ta_c"></td>
						</tr>
						<tr>
							<td><input type="checkbox"></td>
							<td>
								<select>
									<option>GURO_MME#1</option>
								</select>
							</td>
							<td>
								<select>
									<option>Scale Out</option>
								</select>
							</td>
							<td><input type="text" value="NS-ASP-001"></td>
							<td><input type="text" value="5" class="ta_c"></td>
						</tr>
						<tr>
							<td><input type="checkbox"></td>
							<td>
								<select>
									<option>GURO_MME#1</option>
								</select>
							</td>
							<td>
								<select>
									<option>Scale Out</option>
								</select>
							</td>
							<td><input type="text" value="NS-ASP-001"></td>
							<td><input type="text" value="5" class="ta_c"></td>
						</tr>

					</tbody>
				</table>
			</div><!-- //table_ty -->
			<div class="date_time_area ty02">
				<strong class="tit">Scale Time</strong>
				<span class="date">
					<input type="text" class="select_date">
				</span>
				<span class="time_area">
					<input type="text" value="01:00">
					<span class="b_br">:</span>
					<input type="text" value="05:00">
					<span class="b_br">:</span>
					<input type="text" value="05:00">
				</span>
			</div>
			
		</div><!-- //scrollbar-inner -->
	</div><!-- //pop_con_wrap -->
	<a href="javascript:void(0);" class="pop_close">팝업닫기</a>
</div><!-- //popup -->


<div class="popup pop_event_msg" id="popOrderTracking" >
	<div class="pop_tit">I/F RQ &amp; RS Messages</div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">
			
			<div class="pop_con_tit">Request</div>
			<div class="textarea_box">
				<textarea id="requestParam" cols="5" rows="13" readonly wrap="off">{
  "nsFlavourId": "NS-FLAVOR-5G Service-0021",
  "vnfInstanceData": [{
  "vnfInstanceId": "e4b9d024 - e20c - 436 b - 8 ae5 - a45ba065783b",vnfProfileId": "VNF-SM-001-0001"
  }],
  "additionalParamForNs": [{
  "AddParam1forNS": "5 G Network Service Additional Params"
  }],
  "additionalParamForVnf": [{
  "vnfProfileId": "VNF - SM - 001 - 0001 ",
  "additionalParams": [{
	"Nam": "HH - SM01",
	"VNFPackageId": "5 GSMPack_Ver1 .1",
	"OAM IP": "211.220 .13 .243",
	"Scale Option": "Auto"
  }]
  }],
				</textarea>
			</div>
			<div class="pop_con_tit">Response</div>
			<div class="textarea_box">
				<textarea id="responseParam" cols="5" rows="13" readonly wrap="off">{
  "nsFlavourId": "NS-FLAVOR-5G Service-0021",
  "vnfInstanceData": [{
  "vnfInstanceId": "e4b9d024 - e20c - 436 b - 8 ae5 - a45ba065783b",vnfProfileId": "VNF-SM-001-0001"
  }],
  "additionalParamForNs": [{
  "AddParam1forNS": "5 G Network Service Additional Params"
  }],
  "additionalParamForVnf": [{
  "vnfProfileId": "VNF - SM - 001 - 0001 ",
  "additionalParams": [{
	"Nam": "HH - SM01",
	"VNFPackageId": "5 GSMPack_Ver1 .1",
	"OAM IP": "211.220 .13 .243",
	"Scale Option": "Auto"
  }]
  }],
				</textarea>
			</div>
			
		</div><!-- //scrollbar-inner -->
		
		<div class="btn_area">
			<a href="javascript:pop_close('.popup');" class="btn_l btn_co01">확인</a>
		</div>
	</div><!-- //pop_con_wrap -->
	<a href="javascript:void(0);" class="pop_close">팝업닫기</a>
</div><!-- //popup -->

<script>
$(window).ready(function(){
	var dataList 	= [];
	<c:forEach var="board" items="${list}" varStatus ="status">
		var data = {};
			data.no 			= "${board.no}";
			data.id 			= "${board.id}";
			data.nsd_id 		= "${board.nsd_id}";
			data.ns_name 		= "${board.ns_name}";
			data.ns_state 		= "${board.ns_state}";
			data.ns_ins_id 		= "${board.ns_ins_id}";
			data.created_at 	= "${board.created_at}";
			data.key 			= "${board.id}";
			
			data.ns_state_name 	= checkNSState( "${board.ns_state}" );
		dataList[ "${status.index}" ] = data;
		//dataList[ "${status.index}" ] = { no:"${board.no}", id:"${board.id}", nsd_id: "${board.nsd_id}", ns_name: "${board.ns_name}", ns_state: "${board.ns_state}", ns_ins_id: "${board.ns_ins_id}", created_at:"${board.created_at}", key:"${board.id}" };
	</c:forEach>	
	
	var menuObject 		= $(".menu_list").find("ul");
	var nsdList			= [];
	var nsdIdList		= [];
	var nsComponentList	= [];
	
	<c:forEach var="board" items="${nsdList}" varStatus ="status">
		nsdList[ "${status.index}" ] 	= { nsd_id: "${board.nsd_id}", nsd_name: "${board.nsd_name}", nsd_desc: "${board.nsd_desc}" };
		nsdIdList[ "${board.nsd_id}" ] 	= { nsd_id: "${board.nsd_id}", nsd_name: "${board.nsd_name}", nsd_desc: "${board.nsd_desc}" };
	</c:forEach>	
	
	<c:forEach var="board" items="${nsComponentList}" varStatus ="status">
	nsComponentList[ "${board.nsd_id}" ] 	= { nsd_id: "${board.nsd_id}", nsd_name: "${board.nsd_name}", nsd_desc: "${board.nsd_desc}" };
	</c:forEach>	
	
	//vnfdList
	var vnfdList 	= [];
	<c:forEach var="vnfd" items="${vnfdList}" varStatus ="status">
		var data = {};
		//vnfd_name,vnfd_id,vnfd_ver,vnfd_deisgner,ns_type,dev_type,vdu,vcd,vsd,df,conf_para
			data.vnfd_name 		= "${vnfd.vnfd_name}";
			data.vnfd_id 		= "${vnfd.vnfd_id}";
			data.vnfd_ver 		= "${vnfd.vnfd_ver}";
			data.vnfd_deisgner 	= "${vnfd.vnfd_deisgner}";
			data.ns_type 		= "${vnfd.ns_type}";
			data.dev_type 		= "${vnfd.dev_type}";
			data.vdu 			= "${vnfd.vdu}";
			data.vcd 			= "${vnfd.vcd}";
			data.vsd 			= "${vnfd.vsd}";
			data.df 			= "${vnfd.df}";
			data.conf_para 		= "${vnfd.conf_para}";
			data.key 			= "${vnfd.vnfd_id}";
			
			data.ns_type_name 	= checkNSType( "${vnfd.ns_type}" );
		vnfdList[ "${status.index}" ] = data;
	</c:forEach>
	
	var nsdData	= {};
	<c:forEach var="nsd" items="${nsdData}" varStatus ="status">
			nsdData.nsd_id 			= "${nsd.nsd_id}";
			nsdData.designer 		= "${nsd.designer}";
			nsdData.nsd_name 		= "${nsd.nsd_name}";
			nsdData.nsdf 			= '${nsd.nsdf}';
			nsdData.ns_type 		= "${nsd.ns_type}";
			nsdData.target_nfvo		= "${nsd.target_nfvo}";
			
	</c:forEach>
	
	initProvision( menuObject, dataList, nsdList, nsdIdList, vnfdList, nsdData );
 	
});

// $( window ).resize(function() {
// 	  console.log("### remove window.innerHeight : "+window.innerHeight);
// // 	  $("#canvas").html("");
// 		$("#canvas").css("height",300);
// 		$("#canvas").css("width", 300);
// 		$("#canvas:first-child").css("width", 300);
// 		$("#canvas:first-child").css("height", 300);
		
// 		console.log( "#### first_child"+ $("#canvas:first-child").css("width") );
// 		$("#canvas:first-child").attr("height", 300);
// 		$("#canvasfirst-child").attr("width", 300);
	  
// 	});
	
</script>
</body>
</html>