<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content= "text/html; charset=UTF-8"> 
<meta name="format-detection" content="telephone=no">
<title>KT E2E Infra Orchestrator</title>
<link rel="shortcut icon" href="/new_resources/resources/images/layout/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="/new_resources/resources/css/style.css">
<script type="text/javascript" src="/new_resources/resources/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/template.js"></script>

<script type="text/javascript">	
/*
$(function(){
	
	$(function(){pop_open('#pop03')});

});
*/



function data_write()
{
	var f=document.form;
	
	f.cust_nm.value=f.cust_nm_reg.value;
	f.apn.value=f.apn_reg.value;
	f.lte_id.value=f.lte_id_reg.value;
	f.cust_description.value=f.cust_description_reg.value;
	f.method="POST";
	f.action="/equipInven/post";
	
	f.submit();
	pop_close('#pop03_0');
}


function data_update()
{
	var f=document.form;
	
	f.cust_manage_no.value=f.cust_manage_no_mod.value;
	f.cust_nm.value=f.cust_nm_mod.value;
	f.apn.value=f.apn_mod.value;
	f.lte_id.value=f.lte_id_mod.value;
	f.cust_description.value=f.cust_description_mod.value;
	f.method="POST";
	f._method.value="PUT";
	f.action="/equipInven/post/"+f.cust_manage_no_mod.value;
	
	f.submit();
	pop_close('#pop03');
}

function data_delete()
{
	var f=document.form;
	
	f.cust_manage_no.value=f.cust_manage_no_mod.value;
	f.method="POST";
	f._method.value="DELETE";
	f.action="/equipInven/delete/"+f.cust_manage_no_mod.value;

	f.submit();
	pop_close('#pop03');
}

//모두 체크/모두 미체크의 체크박스 처리
function changeCheckBox(flag)
{
	if(flag)
		{
		 $('input:checkbox[name="checkNo"]').each(function() {
		      this.checked = true; //checked 처리
		 });


		}else{
		
   		 $('input:checkbox[name="checkNo"]').each(function() {
		      this.checked = false; //checked 처리
		      if(this.checked){//checked 처리된 항목의 값
		            alert(this.value); 
		      }
		 });
			
		}
}

//체크된 로우 삭제처리
function submitCheckDelete()
{
	var f=document.form;
	f.cust_manage_no.value="0";
	checkBoolean=false;
	
	if (typeof(f.elements['checkNo']) != 'undefined') 
	{
		if (typeof(f.elements['checkNo'].length) == 'undefined')
		{
			if(f.elements['checkNo'].checked==true)
			{
				checkBoolean=true;
			}
		}else{
	        for (i=0; i<f.elements['checkNo'].length; i++)
	        {
	            if (f.elements['checkNo'][i].checked==true)
	            {
	            	alert(f.elements['checkNo'][i].value);
	            	checkBoolean=true;
	            	break;
	            }
	        }
		}
        
        if(checkBoolean)
       	{
           f.action="/equipInven/delete/select";
           f._method.value="DELETE";
           f.submit();        	
       	}else{
       		alert('선택된 내용이 없습니다.');

       	}

	}
	
}

// 정렬클릭시
    function listOrder(colName,ordOption)
    {
    	var orderKey="";
    	var commaFlag="";
    	orderKey+="(";
  		 $('input:checkbox[name="checkNo"]').each(function() {
  			orderKey+=commaFlag+"'" + this.value + "'";
  			commaFlag=",";
		 });
  		orderKey+=")";
    	//alert(orderKey);
        $.ajax({
            type: "get",
           // contentType: "json", //==> 생략가능(RestController이기때문에 가능)
            url: "/equipInven/jsondata?colName="+colName
            		  +"&ordOption="+ordOption+"&orderKey="+orderKey,
            success: function(result){

              // alert(result)
                	 var dataObj = JSON.parse(result);
                     var output = "";
                     for(var i in dataObj){
                    	 var myDate = new Date(dataObj[i].create_time)
                    	 var yyyy= myDate.getFullYear();
                    	 var mm=String(myDate.getMonth() + 1);
                    	 var dd=String(myDate.getDate());
                    	 var h24=String(myDate.getHours());
                    	 var mi=String(myDate.getMinutes());
                    	 mm=(mm.length==1)?"0"+mm:mm;
                    	 dd=(dd.length==1)?"0"+dd:dd;
                    	 h24=(h24.length==1)?"0"+h24:h24;     
                    	 mi=(mi.length==1)?"0"+mi:mi;
                    	// var date = yyyy  + "/" +  mm+ "/" + dd + " " + h24 + ":" + mi;
                    	 var date = yyyy  + "." +  mm+ "." + dd;
                         output += "<tr>";
                         output += "<td  style=\"visibility:hidden\"><input type=checkbox name='checkNo' value='" + dataObj[i].equip_id +"'></td>";
                         output += "<td><a href=\"#\" onClick=\"popup_control('2','open','" + dataObj[i].equip_id +"');\" title=\"" + dataObj[i].equip_id + "\" class=\"pop_btn\">" 
                                      + dataObj[i].equip_id +"</a></td>";
                         output += "<td><a href=\"#\" onClick=\"popup_control('2','open','" + dataObj[i].equip_id +"');\" title=\"" + dataObj[i].equip_name+ "\" class=\"pop_btn\">" 
                         + dataObj[i].equip_name +"</a></td>";
                         output += "<td>" + dataObj[i].status_name + "</td>";
                         output += "<td>" + dataObj[i].equip_type + "</td>";
                         output += "<td>" + dataObj[i].team_name + "</td>";
                         output += "<td>" + dataObj[i].sup_main_person_name +"</td>";
                         output += "<td>" + dataObj[i].sup_main_person_phone +"</td>";
                         output += "<td>";
                         output += "	<a href=\"#\" onClick=\"plteConnInfoView('" + dataObj[i].equip_type +"','" + dataObj[i].equip_id +"','" + dataObj[i].equip_name +"');\" class=\"btn_s btn_co03 pop_btn on\">관리</a> ";
                         output += "	<a href=\"#\" onClick=\"plteConnInfoDelete('" + dataObj[i].equip_type +"','" + dataObj[i].equip_id +"');\"  class=\"btn_s btn_co03\">삭제</a> ";
	                     output += "</td>";
                         output += "<tr>";

                     }
                     output += "";

                $("#trData").html(output);
            }
        });
    }
    

function popup_control(gubun,oflag,okey)
{
	
	try {
	
		var f=document.form;
		if(gubun=="1")
		{
			if(oflag=="open")
			{
				pop_open('#pop03_0');
				$("input[name='equip_name_reg']").focus();
			}else{
				pop_close('#pop03_0');
			}
		}else if(gubun=="2"){
			if(oflag=="open")
			{
				//alert(44);
				pop_open('#pop03');
				 $.ajax({
			            type: "get",
			           // contentType: "json", //==> 생략가능(RestController이기때문에 가능)
			            url: "equipInven/jsondata/"+okey,
			            success: function(result){
		                	 var dataObj = JSON.parse(result);
		                	 $("input[name='equip_id_mod']").val(dataObj.equip_id);
		                	 $("input[name='equip_name_mod']").val(dataObj.equip_name);
		                	 $("input[name='status_name_mod']").val(dataObj.status_name);
		                	 $("input[name='equip_type_mod']").val(dataObj.equip_type);
		                	 $("input[name='mname_mod']").val(dataObj.mname);
		                	 $("input[name='mcomp_name_mod']").val(dataObj.mcomp_name);
	
			            }
			        });
				
			}else{
				pop_close('#pop03');
			}
		}
	}catch(err) {
	    alert(err.message);
	}finally {
        
    }
	
}

//EMS접속정보 조회
function plteConnInfoView(sys_type,sys_id,sys_name){
	pop_open('#con_info_pop');
	 $("input[name='sys_type']").val(sys_type);
   	 $("input[name='sys_name']").val(sys_id);
	 $("strong[id='sys_type_title']").text(sys_type);
   	 $("strong[id='sys_name_title']").text(sys_name); 
	$.ajax({
        type: "get",
        data : {
			sys_type :  sys_type,
			sys_name : sys_id
		 
		},
		dataType : "text",
		cache: false, 
        url: "/equipInven/plteConnInfoView",
        success: function(result){
        	if(result!=""){

				var dataObj = JSON.parse(result);
				$("input[name='ipaddr_1']").val(dataObj.ipaddr_1)      ;
				$("input[name='id_1']").val(dataObj.id_1)          ;
				$("input[name='pwd_1']").val(dataObj.pwd_1)         ;
				$("input[name='expect_1']").val(dataObj.expect_1)	     ;  
				$("input[name='ipaddr_2']").val(dataObj.ipaddr_2)      ;
				$("input[name='id_2']").val(dataObj.id_2)          ;
				$("input[name='pwd_2']").val(dataObj.pwd_2)         ;
				$("input[name='expect_2']").val(dataObj.expect_2)      ;
				$("input[name='cli_cmd']").val(dataObj.cli_cmd)       ;
				$("input[name='cli_id']").val(dataObj.cli_id)        ;
				$("input[name='cli_pwd']").val(dataObj.cli_pwd)       ;
				$("input[name='cli_expect']").val(dataObj.cli_expect)    ;
				$("input[name='confirm_expect']").val(dataObj.confirm_expect);
				$("input[name='confirm']").val(dataObj.confirm)       ;                         
				$("input[name='cli_exit']").val(dataObj.cli_exit)      ;  
           	 }else{
				$("input[name='ipaddr_1']").val("")      ;
				$("input[name='id_1']").val("")          ;
				$("input[name='pwd_1']").val("")         ;
				$("input[name='expect_1']").val("")	     ;  
				$("input[name='ipaddr_2']").val("")      ;
				$("input[name='id_2']").val("")          ;
				$("input[name='pwd_2']").val("")         ;
				$("input[name='expect_2']").val("")      ;
				$("input[name='cli_cmd']").val("")       ;
				$("input[name='cli_id']").val("")        ;
				$("input[name='cli_pwd']").val("")       ;
				$("input[name='cli_expect']").val("")    ;
				$("input[name='confirm_expect']").val("");
				$("input[name='confirm']").val("")       ;                         
				$("input[name='cli_exit']").val("")      ;  
           	 }



        }
    });
}

//EMS접속정보 삭제
function plteConnInfoDelete(sys_type,sys_id){
	$.ajax({
        type: "get",
        data : {
			sys_type :  sys_type,
			sys_name : sys_id
		 
		},
		dataType : "text",
		cache: false, 
        url: "/equipInven/plteConnInfoDelete",
        success: function(result){
        	 alert("EMS 접속정보 관리를 삭제하였습니다.");

        }
    });
}

//EMS접속정보 저장
function plteConnInfoMerge(){
	$.ajax({
        type: "post",
        data : {
        	   sys_type	   	  : $("input[name='sys_type']").val(),
        	   sys_name		  : $("input[name='sys_name']").val(),
        	   ipaddr_1		  : $("input[name='ipaddr_1']").val(),
        	   id_1			  : $("input[name='id_1']").val(),
        	   pwd_1		  : $("input[name='pwd_1']").val(),
        	   expect_1	      : $("input[name='expect_1']").val(),	          
        	   ipaddr_2		  : $("input[name='ipaddr_2']").val(),   
        	   id_2		      : $("input[name='id_2']").val(),
        	   pwd_2	      : $("input[name='pwd_2']").val(),
        	   expect_2	      : $("input[name='expect_2']").val(),
        	   cli_cmd	      : $("input[name='cli_cmd']").val(),
        	   cli_id		  : $("input[name='cli_id']").val(),
        	   cli_pwd		  : $("input[name='cli_pwd']").val(),
        	   cli_expect	  : $("input[name='cli_expect']").val(),
        	   confirm_expect : $("input[name='confirm_expect']").val(),	
        	   confirm		  : $("input[name='confirm']").val(),
        	   cli_exit       : $("input[name='cli_exit']").val()   
		},
		dataType : "text",
		cache: false, 
        url: "/equipInven/plteConnInfoMerge",
        success: function(result){
        	 alert("EMS 접속정보 관리를 저장하였습니다.");
        	 pop_close('#con_info_pop');

        }
    });
}


function list(page){
	var f=document.form;
    location.href="/equipInven?curPage="+page+"&searchOption=${searchOption}"+"&keyword=${keyword}";
}

function searchData(){
	var f=document.form;
	var searchOption=f.searchOption.value;
	var keyword=f.keyword.value;
	//alert(searchOption +"--"+keyword);
    location.href="equipInven?curPage=1&searchOption=" + searchOption +"&keyword=" +keyword;
}
</script>

</head>

<body>

<div id="wrap">

    <header>
		<h1 class="logo"><a href="#">KT E2E Infra Orchestrator</a></h1>
		<nav id="gnb">
			<ul>
				<li><a href="#">DESIGN</a></li>
				<li><a href="#">PROVISIONING</a></li>
				<li><a href="#">MONITORING</a></li>
				<li><a href="#">INVENTORY</a></li>
				<li><a href="#">CONFIGURATION</a></li>
				<li class="active"><!-- 활성화 class="active" -->
					<a href="#">P-LTE</a>
					<ul>
						<li><a href="/gugDataConfig">국데이터 관리</a></li><!-- 활성화 class="active" -->
						<li><a href="/gugPcmdCrExec">명령 실행</a></li>		
						<li><a href="/indPcmdCrExec">운용자 명령어</a></li>
						<li><a href="/cust">고객사 관리</a></li>
						<li class="active"><a href="/equipInven">장비 관리</a></li>
						<li><a href="#">모니터링</a></li>
						<li><a href="#">이력조회</a></li>
					</ul>
				</li>
			</ul>
		</nav><!-- //gnb -->
		<div class="ad_info">
			<i><img src="/new_resources/resources/images/layout/img_ad_info.jpg" alt=""></i><!-- image size : 25*25px -->
			<p>admin</p>
		</div><!-- //ad_info -->
    </header>
    
    <div id="container">	
		
		<ul id="navigation">
			<li><img src="/new_resources/resources/images/layout/img_navigation_home.png" alt="home"></li>
			<li>장비 관리</li>
		</ul><!-- //navigation -->
		
		<div id="contents_wrap">
			<div class="contents">
			<form:form commandName="EquipInvenComponent" name="form" action="" onsubmit="return false;">
	    	<input type="hidden" name="_method" value=""><!-- RESTFUL 메소드 오버라이드를 위해  -->
			<input type="hidden" name="equip_id" value=""><!-- 숫자형으므로 기본 숫자값 줘야함  -->
			<input type="hidden" name="equip_name">
			<input type="hidden" name="status_name">
			<input type="hidden" name="equip_type">
			<input type="hidden" name="mname">
			<input type="hidden" name="mcomp_name">
			<div class="search_area">
					<div class="inner">
						<table>
							<caption>검색 테이블</caption>
							<colgroup>
								<col span="1" style="width:15%;">
								<col span="1" style="width:*;">
							</colgroup>
							<tbody>
								<tr>
									<td>
										<select name="searchOption">
									     	<option value="equip_name" <c:if test='${searchOption=="equip_name"}'> selected </c:if> >장비명</option>
	    	                                <option value="equip_id" <c:if test='${searchOption=="equip_id"}'> selected </c:if> >장비 ID</option>
	    	                                <option value="equip_type" <c:if test='${searchOption=="equip_type"}'> selected </c:if> >운용팀명</option>
	    	                                <option value="sup_main_person_name" <c:if test='${searchOption=="sup_main_person_name"}'> selected </c:if> >담당자</option>
										</select>
									</td>
									<td><input name="keyword" type="text" value="${keyword}"></td>
								</tr>
							</tbody>	
						</table>
						<div class="btn_area">
							<a href="#"  onClick="searchData()" class="btn_l btn_co01 on">조회하기</a>
						</div>
					</div>
				</div><!-- //search_area -->			
				
				<div class="table_ty">
					<div class="count">Total Count <span>${count}</span></div>
					<!-- <div class="btn_area">
						<a href="#" onClick="popup_control('1','open','0');" class="btn_m btn_co01 pop_btn">장비등록</a>
						<a href="#" onClick="submitCheckDelete()" class="btn_m btn_co02">선택삭제</a>
					</div>
					-->
					<table>
						<caption></caption>
						<colgroup>
							<col span="1" style="width:0%;visibility:hidden" >
							<col span="6" style="width:*;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:20%;">
						</colgroup>
						<thead>
							<tr>
								<th scope="col" style="visibility:hidden"><input type="checkbox" name="checkALL" value="" onchange="changeCheckBox(this.checked)"></th> 
								<th scope="col">장비ID 
								     <div class="btn_align">
										<a  href="javascript:listOrder('equip_id','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('equip_id','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">장비명 
								     <div class="btn_align">
										<a  href="javascript:listOrder('equip_name','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('equip_name','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">장비상태 
								     <div class="btn_align">
										<a  href="javascript:listOrder('status_name','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('status_name','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">장비유형
								     <div class="btn_align">
										<a  href="javascript:listOrder('equip_type','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('equip_type','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">운용팀명
								     <div class="btn_align">
										<a  href="javascript:listOrder('equip_type','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('equip_type','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">담당자
								     <div class="btn_align">
										<a  href="javascript:listOrder('sup_main_person_name','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('sup_main_person_name','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>								
								<th scope="col">연락처</th>
								<th scope="col">EMS 접속정보 관리</th>
							</tr>
						</thead>
						<tbody id="trData" style="border:0px">
				        <c:forEach var="board" items="${list}">
				        <tr>
				            <td style="visibility:hidden"><input type=checkbox name="checkNo" value="${board.equip_id}"></td>  
				            <td><a href="#" onClick="popup_control('2','open','${board.equip_id}');" title="${board.equip_id}" class="pop_btn">${board.equip_id}</a></td>
				            <td><a href="#" onClick="popup_control('2','open','${board.equip_id}');" title="${board.equip_id}" class="pop_btn">${board.equip_name}</a></td>

				            <td>${board.status_name}</td>

				            <td>${board.equip_type}</td>
				            <td>${board.team_name}</td>
				            <td>${board.sup_main_person_name}</td>
				            <td>${board.sup_main_person_phone}</td>
							<td>
								<a href="#" onClick="plteConnInfoView('${board.equip_type}','${board.equip_id}','${board.equip_name}');" class="btn_s btn_co03 pop_btn on">관리</a>
								<a href="#" onClick="plteConnInfoDelete('${board.equip_type}','${board.equip_id}');"  class="btn_s btn_co03">삭제</a>
							</td>
				        </tr>
				
				        </c:forEach>
				         </tbody>
					</table>
				</div><!-- //table_ty -->
				
				<div class="paginate">
				    <!-- **처음페이지로 이동 : 현재 페이지가 1보다 크면  [처음]하이퍼링크를 화면에 출력-->
				    <c:if test="${commonPager.curBlock > 1}">
					<a href="#" onClick="list('1')" class="first">처음 페이지</a>
					</c:if>
				
					
					<!-- **이전페이지 블록으로 이동 : 현재 페이지 블럭이 1보다 크면 [이전]하이퍼링크를 화면에 출력 -->
				   <c:if test="${commonPager.curBlock > 1}">
					<a href="#" onClick="list('${commonPager.prevPage}')" class="prev">이전 페이지</a>
					</c:if>

					
					<ul>
						<!-- **하나의 블럭에서 반복문 수행 시작페이지부터 끝페이지까지 -->
		                <c:forEach var="num" begin="${commonPager.blockBegin}" end="${commonPager.blockEnd}">
		                    <!-- **현재페이지이면 하이퍼링크 제거 -->
		                    <c:choose>
		                        <c:when test="${num == commonPager.curPage}">
		                            <li class="select"><a href="#">${num}</a></li>
		                        </c:when>
		                        <c:otherwise>
		                            <li><a href="#" onClick="list('${num}')">${num}</a></li>
		                        </c:otherwise>
		                    </c:choose>
		                </c:forEach>					

					</ul>
					
					<!-- **다음페이지 블록으로 이동 : 현재 페이지 블럭이 전체 페이지 블럭보다 작거나 같으면 [다음]하이퍼링크를 화면에 출력 -->
	                <c:if test="${commonPager.curBlock < commonPager.totBlock}">
					<a href="#" onClick="list('${commonPager.nextPage}')" class="next">다음 페이지</a>
					</c:if>
					
					<!-- **끝페이지로 이동 : 현재 페이지가 전체 페이지보다 작거나 같으면 [끝]하이퍼링크를 화면에 출력 -->
	                <c:if test="${commonPager.curPage < commonPager.totPage}">
					<a href="#" onClick="list('${commonPager.totPage}')" class="last">마지막 페이지</a>
					</c:if>
				</div><!-- //paginate -->
				
			</div><!-- //contents -->
		</div><!-- //contents_wrap -->
	</div><!-- //container -->
	
	<footer>
		<nav class="footer_nav">
			<ul>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">이용약관</a></li>
				<li><a href="#">개인정보처리방침</a></li>
				<li><a href="#">Sitemap</a></li>
			</ul>
		</nav><!-- //footer_nav -->
		<address>㈜케이티 대표이사 황창규 경기도 성남시 분당구 불정로 90 (정자동) 사업자등록번호 : 102-81-42945 통신판매업신고 : 2002-경기성남-0047</address>
	</footer>
	
</div><!-- //wrap -->

<div class="popup_mask"></div>

<div class="popup" id="pop03">
	<div class="pop_tit">장비 정보 </div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">

			<div class="table_form vTop">
				<table>
					<caption></caption>
					<colgroup>
						<col span="1" style="width:30%;">
						<col span="1" style="width:*;">
					</colgroup>
					<tbody>
					  <input type="hidden" name="equip_id_mod" value="">
						<tr>
							<th><span>장비명</span></th>
							<td><input name="equip_name_mod" type="text" value="현대중공업"></td>
						</tr>
						<tr>
							<th><span>장비상태명</span></th>
							<td><input name="status_name_mod" type="text" value="PNC_BUSAN_001"></td>
						</tr>
						<tr>
							<th><span>장비타입</span></th>
							<td><input name="equip_type_mod" type="text" value="h.ktlet.com"></td>
						</tr>
						<tr>
							<th><span>모델명</span></th>
							<td><input name="mname_mod" type="text" value="h.ktlet.com"></td>
						</tr>
						<tr>
							<th><span>모델회사명</span></th>
							<td><input name="mcomp_name_mod" type="text" value="h.ktlet.com"></td>
						</tr>						
					</tbody>	
				</table>
			</div><!-- //table_form -->
			
		</div><!-- //scrollbar-inner -->
		<!--  
		<div class="btn_area">
			<a href="#" onClick="data_update();pop_close('#pop03');" class="btn_m btn_co01">변경</a>
			<a href="javascript:void(0);" onclick="data_delete();" class="btn_m btn_co02">삭제</a>
		</div> -->
	</div><!-- //pop_con_wrap -->
	<a href="javascript:void(0);" class="pop_close" title="close">close</a>
</div><!-- //popup -->

<div class="popup" id="pop03_0">
	<div class="pop_tit">장비 신규 등록</div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">
			
			<div class="table_form vTop">
				<table>
					<caption></caption>
					<colgroup>
						<col span="1" style="width:30%;">
						<col span="1" style="width:*;">
					</colgroup>
					<tbody>

						<tr>
							<th><span>고객사명</span></th>
							<td>
							<input type="text" name="cust_nm_reg"  id="cust_nm_reg"  value="">
							</td>
						</tr>
						<tr>
							<th><span>APN</span></th>
							<td><input name="apn_reg" type="text" value=""></td>
						</tr>
						<tr>
							<th><span>LTE ID</span></th>
							<td><input name="lte_id_reg" type="text" value=""></td>
						</tr>
						<tr>
							<th><span>Description</span></th>
							<td><textarea name="cust_description_reg" cols="" rows=""></textarea></td>
						</tr>
					</tbody>	
				</table>
			</div><!-- //table_form -->
			
		</div><!-- //scrollbar-inner -->
		
		<div class="btn_area">
			<a href="#" onClick="data_write();" class="btn_m btn_co01">등록</a>
		</div>
	</div><!-- //pop_con_wrap -->
	<a href="javascript:void(0);" class="pop_close" title="close">close</a>

</div><!-- //popup -->

<div class="popup pop_ems_connection" id="con_info_pop">
	<div class="pop_tit">EMS 접속정보 관리</div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">
			<input type="hidden" name="sys_type" >
			<input type="hidden" name="sys_name" >
			<div class="table_form vTop">
				<div class="table_head02">
					<ul class="status_board ty03">
						<li>장비 유형 <strong id="sys_type_title">MME </strong></li>
						<li>장비명 <strong id="sys_name_title">용인_MME#1</strong></li>
					</ul>
				</div>
				<table>
					<caption>입력 테이블</caption>
					<colgroup>
						<col span="1" style="width:17%;">
						<col span="1" style="width:32%;">
						<col span="1" style="width:2%;">
						<col span="1" style="width:17%;">
						<col span="1" style="width:32%;">
					</colgroup>
					<tbody>
						<tr>
							<th><span>IP 주소_1</span></th>
							<td><input type="text" name="ipaddr_1" value="255.255.255.1"></td>
							<td>&nbsp;</td>
							<th><span>IP 주소_2</span></th>
							<td><input type="text" name="ipaddr_2" value="255.255.255.1"></td>
						</tr>
						<tr>
							<th><span>ID_1</span></th>
							<td><input type="text" name="id_1" value="yginmme1"></td>
							<td>&nbsp;</td>
							<th><span>ID_2</span></th>
							<td><input type="text" name="id_2" value="yginmme1"></td>
						</tr>
						<tr>
							<th><span>PW_1</span></th>
							<td><input type="password" name="pwd_1" value="yginmme1"></td>
							<td>&nbsp;</td>
							<th><span>PW_2</span></th>
							<td><input type="password" name="pwd_2" value="yginmme1"></td>
						</tr>
						<tr>
							<th><span>Expect_1</span></th>
							<td><input type="text" name="expect_1" value="yginmme1"></td>
							<td>&nbsp;</td>
							<th><span>Expect_2</span></th>
							<td><input type="text" name="expect_2" value="yginmme1"></td>
						</tr>
						<tr>
							<th><span>CLI cmd</span></th>
							<td colspan="4"><input type="text" name="cli_cmd" value="abcdefg"></td>
						</tr>
						<tr>
							<th><span>CLI ID</span></th>
							<td><input type="text" name="cli_id" value="yginmme1"></td>
							<td>&nbsp;</td>
							<th><span>CLI PW</span></th>
							<td><input type="password" name="cli_pwd" value="yginmme1"></td>
						</tr>
						<tr>
							<th><span>CLI Expect</span></th>
							<td><input type="text" name="cli_expect" value="y"></td>
							<td>&nbsp;</td>
							<th><span>Confirm Expect</span></th>
							<td><input type="password" name="confirm_expect" value="yginmme1"></td>
						</tr>
						<tr>
							<th><span>Confirm</span></th>
							<td><input type="text" name="confirm" value="y"></td>
							<td>&nbsp;</td>
							<th><span>CLI Exit</span></th>
							<td><input type="text" name="cli_exit" value="y"></td>
						</tr>
					</tbody>
				</table>
			</div><!-- //table_form -->
			
		</div><!-- //scrollbar-inner -->
		
		<div class="btn_area">
			<a href="javascript:plteConnInfoMerge();" class="btn_l btn_co01">확인</a>
		</div>
	</div><!-- //pop_con_wrap -->
	<a href="javascript:void(0);" class="pop_close">팝업닫기</a>
</div><!-- //popup -->
	</form:form>
</body>
</html>
