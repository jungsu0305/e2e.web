<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content= "text/html; charset=UTF-8"> 
<meta name="format-detection" content="telephone=no">
<title>KT E2E Infra Orchestrator</title>
<link rel="shortcut icon" href="/new_resources/resources/images/layout/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="/new_resources/resources/css/style.css">
<script type="text/javascript" src="/new_resources/resources/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/template.js"></script>

<script type="text/javascript" >


function onlyNumber(obj) {
        return $(obj).val().replace(/[^0-9]/g,"");

    //obj.value.length
}

function onlyDateCheck(obj) {
         //$(this).val($(this).val().replace(/^(19|20)\d{2}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[0-1])$/,""));
        var str= obj.value.replace(/[^0-9]/g,"")
         var dayRegExp = /^(19|20)\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[0-1])$/;
        if( str.match(dayRegExp)==null)
        	return false;
        else
            return true;
    	

}
function progress_pop()
{
	pop_open('#pop_last_progress');

}



// pop_close('#pop_exec_progress');
//pop_first_progress
//pop_ready_progress    
//pop_exec_progress
//pop_last_progress
//모두 체크/모두 미체크의 체크박스 처리
function changeCheckBox(flag)
{
	if(flag)
		{
		 $('input:checkbox[name="checkNo"]').each(function() {
		      this.checked = true; //checked 처리
		 });


		}else{
		
   		 $('input:checkbox[name="checkNo"]').each(function() {
		      this.checked = false; //checked 처리
		      if(this.checked){//checked 처리된 항목의 값
		            alert(this.value); 
		      }
		 });
			
		}
}

//체크된 로우 삭제처리
function submitCheckDelete()
{
	var f=document.form;
	checkBoolean=false;
	
	if (typeof(f.elements['checkNo']) != 'undefined') 
	{
		if (typeof(f.elements['checkNo'].length) == 'undefined')
		{
			if(f.elements['checkNo'].checked==true)
			{
				checkBoolean=true;
			}
		}else{
	        for (i=0; i<f.elements['checkNo'].length; i++)
	        {
	            if (f.elements['checkNo'][i].checked==true)
	            {
	            	//alert(f.elements['checkNo'][i].value);
	            	checkBoolean=true;
	            	break;
	            }
	        }
		}
        
        if(checkBoolean)
       	{
           f.action="/gugPcmdCrExec/delete/select";
           f._method.value="DELETE";
           f.submit();        	
       	}else{
       		alert('선택된 내용이 없습니다.');

       	}

	}
	
}

// 정렬클릭시
    function listOrder(colName,ordOption)
    {
    	var orderKey="";
    	var commaFlag="";
    	orderKey+="(";
  		 $('input:checkbox[name="checkNo"]').each(function() {
  			orderKey+=commaFlag+this.value
  			commaFlag=",";
		 });
  		orderKey+=")";
    	//alert(orderKey);
        $.ajax({
            type: "get",
           // contentType: "json", //==> 생략가능(RestController이기때문에 가능)
            url: "gugPcmdCrExec/jsondata?colName="+colName
            		  +"&ordOption="+ordOption+"&orderKey="+orderKey,
            success: function(result){
                	 var dataObj = JSON.parse(result);
                     var output = "";
                     for(var i in dataObj){

                    	 var myDate = new Date(dataObj[i].modify_dtm)
                    	 var yyyy= myDate.getFullYear();
                    	 var mm=String(myDate.getMonth() + 1);
                    	 var dd=String(myDate.getDate());
                    	 var h24=String(myDate.getHours());
                    	 var mi=String(myDate.getMinutes());
                    	 mm=(mm.length==1)?"0"+mm:mm;
                    	 dd=(dd.length==1)?"0"+dd:dd;
                    	 h24=(h24.length==1)?"0"+h24:h24;     
                    	 mi=(mi.length==1)?"0"+mi:mi;
                    	 var date = yyyy  + "/" +  mm+ "/" + dd + " " + h24 + ":" + mi;
                    	 //var date = yyyy  + "." +  mm+ "." + dd;
                         output += "<tr>";
                         output += "<td><input type=checkbox name='checkNo' value='" + dataObj[i].gug_data_manage_no +"'></td>";

                         output += "<td><a href=\"/gugPcmdCrExec/view/" + dataObj[i].gug_data_manage_no + "\" onClick=\"\" title=\"" + dataObj[i].profile_nm+ "\" >" 
                                      + dataObj[i].profile_nm +"</a></td>";
                         output += "<td>" + dataObj[i].lte_id +"</td>";
                         output += "<td>" + dataObj[i].cust_nm +"</td>";
                         output += "<td>" + dataObj[i].work_state_nm +"</td>";
                         output += "<td>" + dataObj[i].work_ept_dt +" " + dataObj[i].work_ept_start_time + "</td>";
                         output += "<td>" + date +"</td>";                
                         output += "<td>" + ((typeof(dataObj[i].regist_id)=="undefined")?"":dataObj[i].regist_id) +"</td>";
                         output += "<tr>";

                     }
                     output += "";

                $("#trData").html(output);
            }
        });
    }
    
    
    
    // 국데이터 복사
    function dataCopy()
    {
     	var f=document.form;
    	var copyKey="";
    	var cnt=0	;
  		 $('input:checkbox[name="checkNo"]').each(function() {
  			 if(this.checked){
  				copyKey=this.value;
  				cnt=cnt+1;
  			 }
  			if(1 < cnt )
			{
			    return false;
			}

  			
		 });
  		 
		if(cnt <= 0 || 1 < cnt )
		{
 				alert("국데이타 복사를 위해서 첵크박스 하나만 체크하세요."+copyKey);
			
		}else{
        	location.href="gugPcmdCrExec/copy/"+copyKey;
			
		}

     	
     
    }
    
 
    
    function list(page){
    	var f=document.form;
        location.href="gugPcmdCrExec?curPage="+page+"&searchOption=${searchOption}" +
        		             "&keyword_fdate=${keyword_fdate}&keyword_tdate=${keyword_tdate}"+
        		             "&searchOption2=${searchOption2}&keyword2=${keyword2}"+
        		             "&work_state_cd=${work_state_cd}";
    }
    
    function list_state(state_gubun){
    	var f=document.form;
    	location.href="/gugPcmdCrExec?curPage=1" +
        "&work_state_cd="+state_gubun;
       /* location.href="gugPcmdCrExec?curPage=1&searchOption=${searchOption}" +
        		             "&keyword_fdate=${keyword_fdate}&keyword_tdate=${keyword_tdate}"+
        		             "&searchOption2=${searchOption2}&keyword2=${keyword2}" +
        		             "&work_state_cd="+state_gubun; */
    }
    
    function searchData(){
    	var f=document.form;
    	var searchOption=f.searchOption.value;
    	var keyword_fdate=onlyNumber(f.keyword_fdate);   
    	var keyword_tdate=onlyNumber(f.keyword_tdate);
    	var searchOption2=f.searchOption2.value;
    	var keyword2=f.keyword2.value;
    	if(onlyDateCheck(f.keyword_fdate)!=true && keyword_fdate !="")
    	{
    		alert("유효한 날짜가 아닙니다.")
    		f.keyword_fdate.focus()
    		return false;
    	
    	}
    	if(onlyDateCheck(f.keyword_tdate)!=true && keyword_tdate !="")
    	{
    		alert("유효한 날짜가 아닙니다.")
    		f.keyword_tdate.focus()
    		return false;
    	
    	}
    	//alert(searchOption +"--"+keyword);
        location.href="/gugPcmdCrExec?curPage=1&searchOption=" + searchOption +"&keyword_fdate=" +keyword_fdate
                     +"&keyword_tdate=" +keyword_tdate+"&searchOption2=" +searchOption2+"&keyword2=" +keyword2;
    }
    

</script>

</head>

<body>
<div id="wrap">
    
    <header>
		<h1 class="logo"><a href="#">KT E2E Infra Orchestrator</a></h1>
		<nav id="gnb">
			<ul>
				<li><a href="#">DESIGN</a></li>
				<li><a href="#">PROVISIONING</a></li>
				<li><a href="#">MONITORING</a></li>
				<li><a href="#">INVENTORY</a></li>
				<li><a href="#">CONFIGURATION</a></li>
				<li class="active"><!-- 활성화 class="active" -->
					<a href="#">P-LTE</a>
					<ul>
						<li><a href="/gugDataConfig">국데이터 관리</a></li><!-- 활성화 class="active" -->
						<li class="active"><a href="/gugPcmdCrExec">명령 실행</a></li>		
						<li><a href="/indPcmdCrExec">운용자 명령어</a></li>
						<li><a href="/cust">고객사 관리</a></li>
						<li><a href="/equipInven">장비 관리</a></li>
						<li><a href="#">모니터링</a></li>
						<li><a href="#">이력조회</a></li>
					</ul>
				</li>
			</ul>
		</nav><!-- //gnb -->
		<div class="ad_info">
			<i><img src="/new_resources/resources/images/layout/img_ad_info.jpg" alt=""></i><!-- image size : 25*25px -->
			<p>admin</p>
		</div><!-- //ad_info -->
    </header>
    
    <div id="container">	
		
		<ul id="navigation">
			<li><img src="/new_resources/resources/images/layout/img_navigation_home.png" alt="home"></li>
			<li>명령실행 > 국데이터 조회</li>
		</ul><!-- //navigation -->
		
		<div id="contents_wrap">
			<div class="contents">
			 <form:form commandName="GugDataConfigComponent" name="form" action="" onsubmit="return false;">
			 <input type="hidden" name="_method" value=""><!-- RESTFUL 메소드 오버라이드를 위해  -->
				<div class="search_area">
					<div class="inner">
						<table>
							<caption>검색 테이블</caption>
							<colgroup>
								<col span="1" style="width:15%;">
								<col span="1" style="width:*;">
							</colgroup>
							<tbody>
								<tr>
									<td>
										<select name="searchOption">
									     	<option value="modify_dtm" <c:if test='${searchOption=="modify_dtm"}'> selected </c:if> >최종변경일</option>
	    	                                <option value="work_ept_dt" <c:if test='${searchOption=="work_ept_dt"}'> selected </c:if> >작업예정일</option>
 									    </select>	
									</td>
									<td class="date">
										<div class="date_area">
											<input type="text" name="keyword_fdate"  value="${keyword_fdate}" class="select_date">
											<span class="b_br">~</span>
											<input type="text" name="keyword_tdate"  value="${keyword_tdate}"   class="select_date">
										</div>
									</td>
								</tr>
								<tr>
									<td>
									<select name="searchOption2">
								     	<option value="profile_nm" <c:if test='${searchOption2=="profile_nm"}'> selected </c:if> >파일명</option>
    	                                <option value="lte_id" <c:if test='${searchOption2=="lte_id"}'> selected </c:if> >LTE ID</option>
								     	<option value="cust_nm" <c:if test='${searchOption2=="cust_nm"}'> selected </c:if> >고객사</option>
    	                                <option value="regist_id" <c:if test='${searchOption2=="regist_id"}'> selected </c:if> >등록자</option>									
									</select>

									</td>
									<td><input type="text" name="keyword2"  value="${keyword2}"></td>
								</tr>
							</tbody>	
						</table>
						<div class="btn_area">
							<a href="#" onClick="searchData()" class="btn_l btn_co01 on ty02">조회하기</a>
						</div>
					</div>
				</div><!-- //search_area -->
				
				<div class="table_ty">
					<div class="table_head">
						<div class="count">Total Count <span>${count}</span></div>
						<ul class="status_board">
							<!-- <li><a href="#" onClick="list_state('D010')">입력중 <strong>${summaryItem.summary_type_val_1}</strong></a></li> -->
							<!-- <li><a href="#" onClick="list_state('D030')">변경중 <strong>${summaryItem.summary_type_val_3}</strong></a></li> -->
							<li><a href="#" onClick="list_state('D020')">입력완료 <strong>${summaryItem.summary_type_val_2}</strong></a></li>
							<!-- <li><a href="#" onClick="list_state('D060')">삭제 <strong>${summaryItem.summary_type_val_6}</strong></a></li> -->
							<li><a href="#" onClick="list_state('D040')">작업대기 <strong>${summaryItem.summary_type_val_4}</strong></a></li>
							<li><a href="#" onClick="list_state('D050')">작업완료 <strong>${summaryItem.summary_type_val_5}</strong></a></li>
						</ul>

					</div>
					<table>
						<caption>목록 테이블</caption>
						<colgroup>
							<col span="1" style="width:5%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:15%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:12%;">
							<col span="1" style="width:12%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:10%;">
						</colgroup>
						<thead>
							<tr>
								<th scope="col"><input type="checkbox"  name="checkALL" value="" onchange="changeCheckBox(this.checked)"></th>
								<th scope="col">
									파일명
									<div class="btn_align">
										<a  href="javascript:listOrder('profile_nm','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('profile_nm','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">
									LTE ID
									<div class="btn_align">
										<a  href="javascript:listOrder('lte_id','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('lte_id','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">
									고객사
									<div class="btn_align">
										<a  href="javascript:listOrder('cust_nm','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('cust_nm','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">
									상태
									<div class="btn_align">
										<a  href="javascript:listOrder('work_state_nm','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('work_state_nm','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">작업(예정)일</th>
								<th scope="col">
									최종변경일
									<div class="btn_align">
										<a  href="javascript:listOrder('modify_dtm','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('modify_dtm','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">
									등록자
									<div class="btn_align">
										<a  href="javascript:listOrder('regist_id','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('regist_id','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
							</tr>
						</thead>
						<tbody id="trData" style="border:0px">
				        <c:forEach var="board" items="${list}">
							<tr>
								<td><input type="checkbox" name="checkNo" value="${board.gug_data_manage_no}"></td>
								<td><a href="/gugPcmdCrExec/view/${board.gug_data_manage_no}" onclick="" title="${board.profile_nm}">${board.profile_nm}</a></td>
								<td>${board.lte_id}</td>
								<td>${board.cust_nm}</td>
								<td>${board.work_state_nm} </td>
								<td>${board.work_ept_dt} ${board.work_ept_start_time}</td>
								<td><fmt:formatDate value="${board.modify_dtm}" pattern="yyyy.MM.dd HH:mm:ss" /></td>
								<td>${board.regist_id}</td>
							</tr>
						 </c:forEach>
				         </tbody>	
						
						</tbody>
					</table>
				</div><!-- //table_ty -->
				
				<div class="paginate">
				    <!-- **처음페이지로 이동 : 현재 페이지가 1보다 크면  [처음]하이퍼링크를 화면에 출력-->
				    <c:if test="${commonPager.curBlock > 1}">
					<a href="#" onClick="list('1')" class="first">처음 페이지</a>
					</c:if>
				
					
					<!-- **이전페이지 블록으로 이동 : 현재 페이지 블럭이 1보다 크면 [이전]하이퍼링크를 화면에 출력 -->
				   <c:if test="${commonPager.curBlock > 1}">
					<a href="#" onClick="list('${commonPager.prevPage}')" class="prev">이전 페이지</a>
					</c:if>

					
					<ul>
						<!-- **하나의 블럭에서 반복문 수행 시작페이지부터 끝페이지까지 -->
		                <c:forEach var="num" begin="${commonPager.blockBegin}" end="${commonPager.blockEnd}">
		                    <!-- **현재페이지이면 하이퍼링크 제거 -->
		                    <c:choose>
		                        <c:when test="${num == commonPager.curPage}">
		                            <li class="select"><a href="#">${num}</a></li>
		                        </c:when>
		                        <c:otherwise>
		                            <li><a href="#" onClick="list('${num}')">${num}</a></li>
		                        </c:otherwise>
		                    </c:choose>
		                </c:forEach>					

					</ul>
					
					<!-- **다음페이지 블록으로 이동 : 현재 페이지 블럭이 전체 페이지 블럭보다 작거나 같으면 [다음]하이퍼링크를 화면에 출력 -->
	                <c:if test="${commonPager.curBlock < commonPager.totBlock}">
					<a href="#" onClick="list('${commonPager.nextPage}')" class="next">다음 페이지</a>
					</c:if>
					
					<!-- **끝페이지로 이동 : 현재 페이지가 전체 페이지보다 작거나 같으면 [끝]하이퍼링크를 화면에 출력 -->
	                <c:if test="${commonPager.curPage < commonPager.totPage}">
					<a href="#" onClick="list('${commonPager.totPage}')" class="last">마지막 페이지</a>
					</c:if>
				</div><!-- //paginate -->
				</form:form>
			</div><!-- //contents -->
		</div><!-- //contents_wrap -->
	</div><!-- //container -->
	
	<footer>
		<nav class="footer_nav">
			<ul>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">이용약관</a></li>
				<li><a href="#">개인정보처리방침</a></li>
				<li><a href="#">Sitemap</a></li>
			</ul>
		</nav><!-- //footer_nav -->
		<address>㈜케이티 대표이사 황창규 경기도 성남시 분당구 불정로 90 (정자동) 사업자등록번호 : 102-81-42945 통신판매업신고 : 2002-경기성남-0047</address>
	</footer>
	
</div><!-- //wrap -->

<div class="popup_mask"></div>
<div class="popup pop_progress" id="pop_first_progress">
	<div class="pop_tit">진행현황_현대중공업#1</div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">
			
			<div class="step_ty">
				<ol>
					<li class="active">
						<i>1</i>
						<strong class="word">국데이터 생성</strong>
						<span class="s_txt">변경중</span>
					</li>
					<li>
						<i>2</i>
						<strong class="word">작업준비 </strong>
					</li>
					<li>
						<i>3</i>
						<strong class="word">작업실행 </strong>
					</li>
					<li>
						<i>4</i>
						<strong class="word">작업완료</strong>
					</li>
				</ol>
			</div><!-- //step_ty -->
			
			<div class="table_ty">
				<div class="table_head02">
					<ul class="status_board ty02">
						<li>국데이터명 <strong>현대중공업_profile#1  </strong></li>
						<li>작업예정일 <strong>2017.12.25 AM 01:00  </strong></li>
					</ul>
				</div>
				<table>
					<caption>목록  테이블</caption>
					<colgroup>
						<col span="1" style="width:15%;">
						<col span="1" style="width:*;">
						<col span="1" style="width:10%;">
						<col span="1" style="width:10%;">
						<col span="1" style="width:12%;">
						<col span="1" style="width:12%;">
						<col span="1" style="width:10%;">
					</colgroup>
					<thead>
						<tr>
							<th scope="col">국데이터 상태</th>
							<th scope="col">변경일자</th>
							<th scope="col">변경자</th>
							<th scope="col">항목</th>
							<th scope="col">변경전</th>
							<th scope="col">변경후</th>
							<th scope="col">변경사유</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>저장완료</td>
							<td>2017.11.13 11:00:00</td>
							<td>user1</td>
							<td>IP Pool</td>
							<td>10.3.71.0/24</td>
							<td>10.3.71.0/24</td>
							<td>IP 재할당</td>
						</tr>
						<tr>
							<td>저장완료</td>
							<td>2017.11.13 11:00:00</td>
							<td>user1</td>
							<td>IP Pool</td>
							<td>10.3.71.0/24</td>
							<td>10.3.71.0/24</td>
							<td>IP 재할당</td>
						</tr>
						<tr>
							<td>저장완료</td>
							<td>2017.11.13 11:00:00</td>
							<td>user1</td>
							<td>IP Pool</td>
							<td>10.3.71.0/24</td>
							<td>10.3.71.0/24</td>
							<td>IP 재할당</td>
						</tr>
						<tr>
							<td>저장완료</td>
							<td>2017.11.13 11:00:00</td>
							<td>user1</td>
							<td>IP Pool</td>
							<td>10.3.71.0/24</td>
							<td>10.3.71.0/24</td>
							<td>IP 재할당</td>
						</tr>
						<tr>
							<td>저장완료</td>
							<td>2017.11.13 11:00:00</td>
							<td>user1</td>
							<td>IP Pool</td>
							<td>10.3.71.0/24</td>
							<td>10.3.71.0/24</td>
							<td>IP 재할당</td>
						</tr>
					</tbody>	
				</table>
			</div><!-- //table_ty -->

			<div class="paginate">
				<a href="#" class="first">처음 페이지</a>
				<a href="#" class="prev">이전 페이지</a>
				<ul>
					<li class="select"><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">6</a></li>
					<li><a href="#">7</a></li>
					<li><a href="#">8</a></li>
					<li><a href="#">9</a></li>
					<li><a href="#">10</a></li>
				</ul>
				<a href="#" class="next">다음 페이지</a>
				<a href="#" class="last">마지막 페이지</a>
			</div><!-- //paginate -->
			
		</div><!-- //scrollbar-inner -->

	</div><!-- //pop_con_wrap -->
	<a href="javascript:pop_close('#pop_first_progress');" class="pop_close">팝업닫기</a>
</div><!-- //popup -->

<div class="popup pop_progress" id="pop_ready_progress">
	<div class="pop_tit">진행현황_현대중공업#1</div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">
			
			<div class="step_ty">
				<ol>
					<li class="on">
						<i>1</i>
						<strong class="word">국데이터 생성</strong>
						<span class="s_txt">입력완료</span>
					</li>
					<li class="active">
						<i>2</i>
						<strong class="word">작업준비</strong>
						<span class="s_txt">작업대기(3/6)</span>
					</li>
					<li>
						<i>3</i>
						<strong class="word">작업실행</strong>
						<span class="s_txt">작업예정(201.12.25)</span>
					</li>
					<li>
						<i>4</i>
						<strong class="word">작업완료</strong>
					</li>
				</ol>
			</div><!-- //step_ty -->
			
			<div class="table_ty">
				<div class="table_head02">
					<ul class="status_board ty02">
						<li>국데이터명 <strong>현대중공업_profile#1  </strong></li>
						<li>작업예정일 <strong>2017.12.25 AM 01:00  </strong></li>
					</ul>
				</div>
				<table>
					<caption>목록  테이블</caption>
					<colgroup>
						<col span="1" style="width:*;">
						<col span="1" style="width:20%;">
						<col span="1" style="width:20%;">
						<col span="1" style="width:20%;">
					</colgroup>
					<thead>
						<tr>
							<th scope="col">작업대상</th>
							<th scope="col">확인상태</th>
							<th scope="col">확인일자</th>
							<th scope="col">운용자</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>MME_혜화_1</td>
							<td>확인</td>
							<td>2017.11.20 11:00:00</td>
							<td>운용자</td>
						</tr>
						<tr>
							<td>MME_혜화_1</td>
							<td>확인</td>
							<td>2017.11.20 11:00:00</td>
							<td>운용자</td>
						</tr>
						<tr>
							<td>MME_혜화_1</td>
							<td>확인</td>
							<td>2017.11.20 11:00:00</td>
							<td>운용자</td>
						</tr>
						<tr>
							<td>MME_혜화_1</td>
							<td>확인</td>
							<td>2017.11.20 11:00:00</td>
							<td>운용자</td>
						</tr>
						<tr>
							<td>MME_혜화_1</td>
							<td>확인</td>
							<td>2017.11.20 11:00:00</td>
							<td>운용자</td>
						</tr>
					</tbody>	
				</table>
			</div><!-- //table_ty -->
			
		</div><!-- //scrollbar-inner -->
		
		<div class="btn_area">
			<a href="javascript:pop_close('#pop_ready_progress');" class="btn_l btn_co01">승인하기</a>
		</div>
	</div><!-- //pop_con_wrap -->
	<a href="javascript:void(0);" class="pop_close">팝업닫기</a>
</div><!-- //popup -->

<div class="popup pop_progress" id="pop_exec_progress">
	<div class="pop_tit">진행현황_현대중공업#1</div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">
			
			<div class="step_ty">
				<ol>
					<li class="on">
						<i>1</i>
						<strong class="word">국데이터 생성</strong>
						<span class="s_txt">입력완료</span>
					</li>
					<li class="on">
						<i>2</i>
						<strong class="word">작업준비</strong>
						<span class="s_txt">확인완료(6/6)</span>
					</li>
					<li class="active">
						<i>3</i>
						<strong class="word">작업실행</strong>
						<span class="s_txt">명령실행중(3/6)</span>
					</li>
					<li>
						<i>4</i>
						<strong class="word">작업완료</strong>
					</li>
				</ol>
			</div><!-- //step_ty -->
			
			<div class="table_ty">
				<div class="table_head02">
					<ul class="status_board ty02">
						<li>국데이터명 <strong>현대중공업_profile#1  </strong></li>
						<li>작업예정일 <strong>2017.12.25 AM 01:00  </strong></li>
					</ul>
				</div>
				<table>
					<caption>목록  테이블</caption>
					<colgroup>
						<col span="1" style="width:*;">
						<col span="1" style="width:20%;">
						<col span="1" style="width:20%;">
						<col span="1" style="width:20%;">
					</colgroup>
					<thead>
						<tr>
							<th scope="col">작업대상</th>
							<th scope="col">확인상태</th>
							<th scope="col">확인일자</th>
							<th scope="col">운용자</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>MME_혜화_1</td>
							<td>확인</td>
							<td>-</td>
							<td>운용자</td>
						</tr>
						<tr>
							<td>MME_혜화_1</td>
							<td>확인</td>
							<td>-</td>
							<td>운용자</td>
						</tr>
						<tr>
							<td>MME_혜화_1</td>
							<td>확인</td>
							<td>2017.11.20 11:00:00</td>
							<td>운용자</td>
						</tr>
						<tr>
							<td>MME_혜화_1</td>
							<td>확인</td>
							<td>2017.11.20 11:00:00</td>
							<td>운용자</td>
						</tr>
						<tr>
							<td>MME_혜화_1</td>
							<td>확인</td>
							<td>2017.11.20 11:00:00</td>
							<td>운용자</td>
						</tr>
					</tbody>	
				</table>
			</div><!-- //table_ty -->
			
		</div><!-- //scrollbar-inner -->
		
	</div><!-- //pop_con_wrap -->
	<a href="javascript:void(0);" class="pop_close">팝업닫기</a>
</div><!-- //popup -->

<div class="popup pop_progress" id="pop_last_progress">
	<div class="pop_tit">진행현황_현대중공업#1</div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">
			
			<div class="step_ty">
				<ol>
					<li class="on">
						<i>1</i>
						<strong class="word">국데이터 생성</strong>
						<span class="s_txt">입력완료</span>
					</li>
					<li class="on">
						<i>2</i>
						<strong class="word">작업준비</strong>
						<span class="s_txt">확인완료(6/6)</span>
					</li>
					<li class="on">
						<i>3</i>
						<strong class="word">작업실행</strong>
						<span class="s_txt">명령실행중(3/6)</span>
					</li>
					<li class="on">
						<i>4</i>
						<strong class="word">작업완료</strong>
						<span class="s_txt">2017.12.26 11:00:00</span>
					</li>
				</ol>
			</div><!-- //step_ty -->
			
			<div class="table_ty">
				<div class="table_head02">
					<ul class="status_board ty02">
						<li>국데이터명 <strong>현대중공업_profile#1  </strong></li>
						<li>작업예정일 <strong>2017.12.25 AM 01:00  </strong></li>
					</ul>
				</div>
				<table>
					<caption>목록  테이블</caption>
					<colgroup>
						<col span="1" style="width:*;">
						<col span="1" style="width:20%;">
						<col span="1" style="width:20%;">
						<col span="1" style="width:20%;">
					</colgroup>
					<thead>
						<tr>
							<th scope="col">작업대상</th>
							<th scope="col">확인상태</th>
							<th scope="col">확인일자</th>
							<th scope="col">운용자</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>MME_혜화_1</td>
							<td>확인</td>
							<td>-</td>
							<td>운용자</td>
						</tr>
						<tr>
							<td>MME_혜화_1</td>
							<td>확인</td>
							<td>-</td>
							<td>운용자</td>
						</tr>
						<tr>
							<td>MME_혜화_1</td>
							<td>확인</td>
							<td>2017.11.20 11:00:00</td>
							<td>운용자</td>
						</tr>
						<tr>
							<td>MME_혜화_1</td>
							<td>확인</td>
							<td>2017.11.20 11:00:00</td>
							<td>운용자</td>
						</tr>
						<tr>
							<td>MME_혜화_1</td>
							<td>확인</td>
							<td>2017.11.20 11:00:00</td>
							<td>운용자</td>
						</tr>
					</tbody>	
				</table>
			</div><!-- //table_ty -->
			
		</div><!-- //scrollbar-inner -->
		
		<div class="btn_area">
			<a href="javascript:pop_close('.popup');" class="btn_l btn_co01">확인</a>
		</div>
	</div><!-- //pop_con_wrap -->
	<a href="javascript:void(0);" class="pop_close">팝업닫기</a>
</div><!-- //popup -->

</body>
</html>