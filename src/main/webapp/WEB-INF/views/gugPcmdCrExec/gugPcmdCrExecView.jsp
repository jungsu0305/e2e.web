<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content= "text/html; charset=UTF-8"> 
<meta name="format-detection" content="telephone=no">
<title>KT E2E Infra Orchestrator</title>
<link rel="shortcut icon" href="/new_resources/resources/images/layout/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="/new_resources/resources/css/style.css">
<script type="text/javascript" src="/new_resources/resources/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/template.js"></script>

<script type="text/javascript">
var lists = [];
var select_equip_type="MME";
var select_equip_type_cd="D001";

var item_val_chk=[//MME
                  ["apn_fqdn","MAN","DECIMAL",0,100,1,"MME","N","APN"]  //CRTE-DOMAIN	APN	MAN

                  ,["pgwgrp","MAN","DECIMAL",0,1023,1,'MME',"N","PGWGRP"]  
                  ,["pgw","OPT","DECIMAL",1,1023,1,'MME',"Y","PGW"]  
                  ,["ip","OPT","STRING",1,64,1,'MME',"Y","IP"]  
                  ,["name","OPT","STRING",0,100,1,'MME',"Y","NAME"]  
                  ,["capa","OPT","DECIMAL",1,255,1,'MME',"Y","CAPA"]  
                  //EPCC
                  ,["pgw_ip","MAN","STRING",7,15,1,'EPCC',"N","IP_ADDR"]  
                  ,["sms_recv","OPT","STRING",0,100,1,'EPCC',"N",""]  //보류
                  ,["ip_pool","OPT","STRING",0,100,1,'EPCC',"Y",""]  //설정하지 안흠- 화면에서 않보이게
                   //PGW
                  ,["ip_pool_id","MAN","DECIMAL",0, 11999 ,1,'PGW',"Y","IPPOOL"]  
                  ,["vr_id","MAN","DECIMAL",0,31,1,'PGW',"Y","VR"] //CRTE-IP-POOL	VR	OPT or  CRTE-VLAN-INTF	VRID	MAN
                  ,["pool_type_cd","OPT",	"ENUM", 0,0,1,"PGW","Y","TYPE"]        
                  ,["static_cd","OPT",	"ENUM", 0,0,1,"PGW","Y","STATIC"]          
                  ,["tunnel_cd","OPT",	"ENUM", 0,0,1,"PGW","Y","TUNNEL"]             
                  ,["start_addr","OPT",	"STRING", 1,64,1,"PGW","Y","IP"]  
                  ,["vlan","OPT",	"DECIMAL", 1,4094,1,"PGW","Y","VLANID"]  													
                  ,["ip_addr","OPT",	"STRING", 1,64,1,"PGW","Y","IPADDR"]  
                  ,["network","OPT",	"STRING", 1,32,1,"PGW","Y","NETWORK"]  
                  ,["gateway","OPT",	"STRING", 1,64,1,"PGW","Y","GWIP"]  
                  ,["prirank_cd","OPT",	"ENUM", 0,100,1,"PGW","Y","우선순위-없음"]  //확인필요함
                  ,["ifc","OPT",	"STRING", 1,16,1,"PGW","Y","IFNAME"]  
                  ,["apn_id","MAN",	"DECIMAL", 0,100,1,"PGW","Y","APN"]  
                  ,["apn_name","MAN",	"STRING", 1,32,1,"PGW","NAME"]  
                  ,["ip_alloc_cd","OPT",	"ENUM", 0,100,1,"PGW","Y","ALLOC"]         
                  ,["rad_ip_alloc_cd","OPT",	"ENUM", 0,100,1,"PGW","Y","ALLOC"]    
                  ,["auth_pcrf_pcc_cd","OPT",	"ENUM", 0,100,1,"PGW","Y",""] //확인 필요함
                  ,["acct_react_cd","OPT",	"ENUM", 0,100,1,"PGW","Y",""]    //확인 필요함     
                  ,["pri_dns_v4","OPT",	"STRING", 0,100,1,"PGW","Y","PDNS4"]  
                  ,["rad_auth_id","OPT",	"DECIMAL", 0,100,1,"PGW","Y","RAUTH"]  
                  ,["rad_auth_act","OPT",	"STRING", 0,100,1,"PGW","Y",""]  //확인 필요함
                  ,["rad_auth_sby","OPT",	"STRING", 0,100,1,"PGW","Y",""]    //확인 필요함
                  ,["rad_acct_id","OPT",	"DECIMAL", 0,15,1,"PGW","Y","RACCT"]  
                  ,["rad_acct_act","OPT",	"STRING", 0,100,1,"PGW","Y",""]    //확인 필요함 
                  ,["rad_acct_sby","OPT",	"STRING", 0,100,1,"PGW","Y",""]    //확인 필요함
                  ,["diam_pcfr_id","OPT",	"STRING", 0,15,1,"PGW","Y","DPCRF"]    
                  ,["diam_pcfr_act","OPT",	"STRING", 0,100,1,"PGW","Y",""]    //확인 필요함 
                  ,["diam_pcfr_sby","OPT",	"STRING", 0,100,1,"PGW","Y",""]   //확인 필요함
                  ,["local_pcc_pf_id","OPT",	"STRING", 1,10,1,"PGW","Y",""]  //설정하지 않음
                  ,["local_pcc_pf_act","OPT",	"STRING", 0,100,1,"PGW","Y",""]  //확인 필요함
                  ,["local_pcc_pf_sby","OPT",	"STRING", 0,100,1,"PGW","Y",""]  //확인 필요함
                  ,["sec_dns_v4","OPT",	"STRING", 0,100,1,"PGW","Y","SDNS4"]  
                  ,["arp_map_high","OPT",	"DECIMAL", 1,14,1,"PGW","Y","ARPH"]  
                  ,["arp_map_med","OPT",	"DECIMAL", 2,15,1,"PGW","Y","ARPM"]
                   //HSS
                  ,["id","MAN",	"DECIMAL", 0,999,1,"HSS","N","ID"] 
                  ,["apn","MAN",	"STRING", 1,64,1,"HSS","N","APN"]  
                  ,["apnoirepl","OPT",	"STRING", 1,64,1,"HSS","N","APNOIREPL"]  
                  ,["pltcc","OPT",	"STRING", 1,5,1,"HSS","N","PLTECC"]  
                  ,["uutype","OPT",	"STRING", 1,4,1,"HSS","N","UUTYPE"]
                  ,["mpname_cd","OPT",	"ENUM", 1,10,1,"HSS","N","MPNAME"]
                 ];
function command_create()
{

	lists = [];
	var cnt=0;
	 $('input:checkbox[name="sel_equip_id"]:checked').each(function() {
		 if (!$(this).is( ":disabled" )){
	      cnt=1;
	      return false;
		 }
	 });
    if(cnt <=0)
    {
    	alert('새로 선택되어 활성화된 Target System이 없습니다.');
    	return false;
    }
	

	  $("input:checkbox[name='sel_equip_id']:checked").each(function(i){   //jQuery로 for문 돌면서 check 된값 배열에 담는다
		  if (!$(this).is( ":disabled" )){
		  	lists.push($(this).val());
		  }
	  });
	$.ajax({
		type : "POST",
		url : "/gugPcmdCrExec/cmd_create",
		//contentType:"application/x-www-form-urlencoded;charset=utf-8", //한글 깨짐 방지
		data : {
			work_command_create_no : 0 ,
			gug_data_manage_no : "${gugDataConfig.gug_data_manage_no}",
			gug_data_history_no  : "${gugDataConfig.gug_data_history_no}" ,
            equip_type : select_equip_type,
            equip_type_cd : select_equip_type_cd,
			pgw : $("td[id='pgw']").text(),
			name : $("td[id='name']").text(),
			ip : $("td[id='ip']").text(),
			apn_fqdn : $("td[id='apn_fqdn']").text(),
			capa : $("td[id='capa']").text(),
			pgwgrp : $("td[id='pgwgrp']").text(),	

			equip_id: lists.join('|||')
		 
		},
		dataType : "text",
		cache: false, 
		success : function(result) { 			
			
			if(result=="")
			{
				select_command_list(2);
	            //alert("생성을 완료했습니다.");
	            $("input:checkbox[name='sel_equip_id']:checked").each(function(i){   //jQuery로 for문 돌면서 check 된값 배열에 담는다
	            	if (!$(this).is( ":disabled" )){
	          		  $(this).prop( "disabled", true );
	            	}
	          	  });
			}else{
				pop_close("#preload_create_pop");
				alert(result);
			}
		},
		error : function(result, status, err) {
	 		pop_close("#preload_create_pop");
			alert('생성에 실패했습니다');
		},
	 	beforeSend: function() {
	 		pop_open("#preload_create_pop");
		},		
		complete : function() {
	 		pop_close("#preload_create_pop");

		}
	});
}




function select_command_list(gubun)
{

	dupEquipName="";
    $.ajax({
        type: "get",
       // contentType: "json", //==> 생략가능(RestController이기때문에 가능)
        url: "/gugPcmdCrExec/list?"+"curPage=1&gug_data_manage_no=${gugDataConfig.gug_data_manage_no}" +
        "&gug_data_history_no=${gugDataConfig.gug_data_history_no}"
        ,
        success: function(result){
            	 var dataObj = JSON.parse(result);
                 var output="";

				var Oval = dataObj.list;
				
                /*gubun=1 이면순수한 리스트 조회시*/
                if(gubun==1){
                
					for(var i in Oval){
						output+=list_td(Oval,i);
	                 }
	                  $("#trData").html(output);
					    if(run_all_flag)
		      			{
		      				
					    	$("#trData").ready(function() {
					    		if($("td[id='id_state']").length > run_idx){
			      					$("td[id='id_state']").eq(run_idx).attr("style","background-color:white");
				      				$("td[id='id_button']").eq(run_idx).attr("style","background-color:white");
				      				console.log(run_idx);
				      				if(Oval[run_idx].exec_state_nm=="fail" && stop_skip_flag==true)
			      					{
				      					run_idx=0;
				      					run_all_flag=false;
				      					return false;
			      					}
				      					
				      				$("#id_button").ready(function() {
				      				run_idx++;
	
				      				run_all();
				      				});
					    		}else{
			      					run_idx=0;
			      					run_all_flag=false;
			      					return false;
					    		}
		      				});
		      			}else{
		      				run_idx=0;
		      			}
                }else if(gubun==2) /*생성된 명령어 리스트 조회에서 앞에 추가*/
                {
                    for( var i=0;i<lists.length;i++)
                  	 {
    					for(var j in Oval){
    					 if(lists[i]==Oval[j].equip_id){
    						 output+=list_td(Oval,j);
	    					}
	   	                 }
                  	 }
                    
                	$("#trData").prepend(output);
                }else if(gubun==3){
                	var id_state_html="";
                	var id_button_html="";
					for(var i in Oval){
						if(Oval[i].work_command_create_no==r_work_command_create_no)
						{
						 $("td[id='id_command_sentence']").eq(run_idx).html(Oval[i].create_command_sentence);
							
				         if(Oval[i].exec_state_nm=="fail")
				         {

				        	 id_state_html = "<a href=\"javascript:void(0);\" id=\"fail_btn\" " 
				         	        + "onClick=\"pop_view_in_command(" + Oval[i].work_command_create_no + ",'" + Oval[i].equip_type_cd  
				         	        		+"','" + Oval[i].create_command_sentence+"','" + Oval[i].equip_id+"',this)\">"
				         	       + Oval[i].exec_state_nm +"</a>";
				         }else{
				        	 id_state_html = "<a href=\"javascript:void(0);\" id=\"fail_btn\" >" + Oval[i].exec_state_nm +"</a>";
				         }
				         
				         if(Oval[i].exec_state_nm=="ready")
				         {
				        	 id_button_html += "	<a href=\"javascript:void(0);\" id=\"run_cmd\" onClick=\"run_command(" + Oval[i].work_command_create_no + ",this)\" class=\"btn_s btn_co03 on\">Run</a> ";
				         }else{
				        	 id_button_html += "	<a href=\"javascript:void(0);\"  class=\"btn_s btn_co03 off\">Run</a> "; 
				         }
				         if(Oval[i].exec_state_nm=="fail")
				         {
				        	 id_button_html += "	<a href=\"javascript:void(0);\" id=\"run_cmd\" onClick=\"run_command(" + Oval[i].work_command_create_no + ",this)\" class=\"btn_s btn_co03 on\">Retry</a> ";
				         }else{
				        	 if(Oval[i].exec_state_nm !="ready" && Oval[i].exec_state_nm !="fail" )
				       		 {
				        		 id_button_html += "	<a href=\"javascript:void(0);\"  id=\"run_cmd\"  class=\"btn_s btn_co03 off\">Retry</a> ";
				       		 }else{
				       			id_button_html += "	<a href=\"javascript:void(0);\"   class=\"btn_s btn_co03 off\">Retry</a> ";
				       		 }
				         }
				         
				         if(Oval[i].exec_state_nm=="ready" || Oval[i].exec_state_nm=="fail" )
				    	 {
				        	 var skip_onoff="off";
				    	   	 for(var ii=0;ii< skip_lists.length;ii++)
				    		 {
				    	   		if(Oval[i].work_command_create_no==skip_lists[ii])
				    	   		{
				    	   			skip_onoff=	"on";
				    	   			break;
				    	   		}
				    			
				    		 }
				    	   	id_button_html += "	<a href=\"javascript:void(0);\" id=\"skip_btn\" onClick=\"btn_skip_process(" + Oval[i].work_command_create_no + ",this)\" class=\"btn_s btn_co03 "+ skip_onoff +"\">Skip</a> ";

				    	 }else{
				    		 id_button_html += "     <a href=\"javascript:void(0);\" id=\"skip_btn\" class=\"btn_s btn_co03 off\">Skip</a>";
				    	 }
				         
				         if(Oval[i].exec_state_nm=="fail" || Oval[i].exec_state_nm=="success")
				         {
				        	 id_button_html += "		<a href=\"javascript:void(0);\" onClick=\"pop_view_msgs(" + Oval[i].work_command_create_no +")\" class=\"btn_s btn_co03 on\">View msg</a> ";
				    	 }else{
				    		 id_button_html += "		<a href=\"javascript:void(0);\" class=\"btn_s btn_co03 off\">View msg</a> ";
				    	 }
				         
						break;
						}
	                 }
					//$('tr[id="trids"]').eq(run_idx).html(output);
	                 // $("#trData").html(output);
					    if(run_all_flag)
		      			{
		      				
					    		if($("td[id='id_state']").length > run_idx){
			      					$("td[id='id_state']").eq(run_idx).attr("style","background-color:white");
				      				$("td[id='id_button']").eq(run_idx).attr("style","background-color:white");
				      				$("td[id='id_state']").eq(run_idx).html(id_state_html);
				      				$("td[id='id_button']").eq(run_idx).html(id_button_html);
				      				console.log(run_idx);
				      				if(Oval[run_idx].exec_state_nm=="fail" && stop_skip_flag==true)
			      					{
				      					run_idx=0;
				      					run_all_flag=false;
				      					return false;
			      					}
				      					
				      				$("#id_button").ready(function() {
				      				run_idx++;
	
				      				run_all();
				      				});
					    		}else{
			      					run_idx=0;
			      					run_all_flag=false;
			      					return false;
					    		}
		      			}else{
		      				$("td[id='id_state']").eq(run_idx).attr("style","background-color:white");
		      				$("td[id='id_button']").eq(run_idx).attr("style","background-color:white");
		      				$("td[id='id_state']").eq(run_idx).html(id_state_html);
		      				$("td[id='id_button']").eq(run_idx).html(id_button_html);
		      				run_idx=0;
		      			}
                }


            

        }
    });

}


var dupEquipName="";
function list_td(Oval,i){
	var output="";
	 if(Oval[i].exec_state_nm=="ready" || Oval[i].exec_state_nm=="fail" )
	 {
		output += "<tr id=\"trids\" style=\"background-color:#000000\">";
	 }else{
	 	output += "<tr id=\"trids\">";
	 }
	 if(dupEquipName==Oval[i].equip_name)
	 {
		 output += "<td></td>";
	 }else{
		 output += "<td><input type=checkbox name=\"checkEquipName\"  onchange=\"changeCheckEN(this.checked, this.value)\" value=\"" + Oval[i].equip_name + "\"></td>";
	 }
	 
	 output += "<td><input type=checkbox name=\"checkNo\" value=\"" + Oval[i].work_command_create_no + "\"></td>";
     output += "<td>" + Oval[i].equip_type + "</td>";
     output += "<td>" + Oval[i].equip_name + "</td>";
     output += "<td id=\"id_command_sentence\" style=\"text-align:left\">" + Oval[i].create_command_sentence +"</td>";
     if(Oval[i].exec_state_nm=="ready" || Oval[i].exec_state_nm=="fail")
     {
         if(Oval[i].exec_state_nm=="fail")
         {
        	 console.log(Oval[i].equip_type_cd);
         	output += "<td id=\"id_state\"><a href=\"javascript:void(0);\" id=\"fail_btn\" " 
     	        + "onClick=\"pop_view_in_command(" + Oval[i].work_command_create_no + ",'" + Oval[i].equip_type_cd  
	        		+"','" + Oval[i].create_command_sentence+"','" + Oval[i].equip_id+"',this)\">"         	      
         	        + Oval[i].exec_state_nm +"</a></td>";
         }else{
 	     	output += "<td id=\"id_state\"><a href=\"javascript:void(0);\" id=\"fail_btn\" >" + Oval[i].exec_state_nm +"</a></td>";
         }
	     output += "<td id=\"id_button\">";
     }else{
         output += "<td id=\"id_state\"><a href=\"javascript:void(0);\" id=\"fail_btn\" >" + Oval[i].exec_state_nm +"</a></td>";
         output += "<td id=\"id_button\">";
     }
     if(Oval[i].exec_state_nm=="ready")
     {
     	output += "	<a href=\"javascript:void(0);\" id=\"run_cmd\" onClick=\"run_command(" + Oval[i].work_command_create_no + ",this)\" class=\"btn_s btn_co03 on\">Run</a> ";
     }else{
    	 output += "	<a href=\"javascript:void(0);\" class=\"btn_s btn_co03 off\">Run</a> "; 
     }
     if(Oval[i].exec_state_nm=="fail")
     {
     	output += "	<a href=\"javascript:void(0);\" id=\"run_cmd\" onClick=\"run_command(" + Oval[i].work_command_create_no + ",this)\" class=\"btn_s btn_co03 on\">Retry</a> ";
     }else{
    	 if(Oval[i].exec_state_nm !="ready" && Oval[i].exec_state_nm !="fail" )
   		 {
    		 output += "	<a href=\"javascript:void(0);\"  id=\"run_cmd\"  class=\"btn_s btn_co03 off\">Retry</a> ";
   		 }else{
   			 output += "	<a href=\"javascript:void(0);\"   class=\"btn_s btn_co03 off\">Retry</a> ";
   		 }
    	  
     }
     
     if(Oval[i].exec_state_nm=="ready" || Oval[i].exec_state_nm=="fail" )
	 {
    	 var skip_onoff="off";
	   	 for(var ii=0;ii< skip_lists.length;ii++)
		 {
	   		if(Oval[i].work_command_create_no==skip_lists[ii])
	   		{
	   			skip_onoff=	"on";
	   			break;
	   		}
			
		 }
         output += "	<a href=\"javascript:void(0);\" id=\"skip_btn\" onClick=\"btn_skip_process(" + Oval[i].work_command_create_no + ",this)\" class=\"btn_s btn_co03 "+ skip_onoff +"\">Skip</a> ";

	 }else{
	 	output += "     <a href=\"javascript:void(0);\" id=\"skip_btn\" class=\"btn_s btn_co03 off\">Skip</a>";
	 }
     
     if(Oval[i].exec_state_nm=="fail" || Oval[i].exec_state_nm=="success")
     {
     	output += "		<a href=\"javascript:void(0);\" onClick=\"pop_view_msgs(" + Oval[i].work_command_create_no +")\" class=\"btn_s btn_co03 on\">View msg</a> ";
	 }else{
	    output += "		<a href=\"javascript:void(0);\" class=\"btn_s btn_co03 off\">View msg</a> ";
	 }
     output += "</td>";               
     output += "<tr>";
     dupEquipName=Oval[i].equip_name;
     return output;
	
}

//운용자 명령어 확인
function oper_confirm()
{
	$.ajax({
		type : "POST",
		url : "/gugPcmdCrExec/oper_confirm",
		//contentType:"application/x-www-form-urlencoded;charset=utf-8", //한글 깨짐 방지
		data : {
			work_command_create_no : 0 ,
			gug_data_manage_no : "${gugDataConfig.gug_data_manage_no}",
			gug_data_history_no  : "${gugDataConfig.gug_data_history_no}", 
			ero_cc_yn : "Y"
		 
		},
		dataType : "text",
		cache: false, 
		success : function(result) { 			 
            alert("명령어 확인을 완료했습니다.");
            select_command_list(1);
        /*
            var jsonObj = JSON.parse(result);
			alert(jsonObj.model.alarm_cd); */

		//	alarmGrid.load(result,"json")		
		//	alarmGrid.load("${alarmJSON}","json")		
		//	alarmGrid.load("../common/data.json","json");
		//	alarmGrid.loadXMLString(result);			
		},
		error : function(result, status, err) {
			alert('명령어 확인에 실패했습니다');
		},
	 	beforeSend: function() {
	 		//myLayout.cells("b").progressOn();
		},		
		complete : function() {
			//myLayout.cells("b").progressOff();
		}
	});
}

var auto_skip_flag=true;
var stop_skip_flag=false;
var run_all_flag=false;
var run_idx=0;
function auto_stop_skip_process(flag)
{
	if(flag==1)
	{
		auto_skip_flag=true;
		stop_skip_flag=false;
		$("a[id='skip_process_btn']").html("Auto Skip<i class=\"autoskip\"></i>");
	}else{
		auto_skip_flag=false;
		stop_skip_flag=true;
		$("a[id='skip_process_btn']").html("Stop Skip<i class=\"autoskip\"></i>");
	}
}

var skip_lists = [];
function btn_skip_process(cr_no, obj)
{
	var skip_idx=($('a#skip_btn').index($(obj)));
	if($(obj).attr("class")=="btn_s btn_co03 off")
	{
		$(obj).attr("class","btn_s btn_co03 on");
		skip_lists.push(cr_no);
	}
	else
	{
		for(var i=0;i< skip_lists.length;i++)
		 {
	   		if(cr_no==skip_lists[i])
	   		{
	   			skip_lists.splice(i,1);
	   			break;
	   		}
			
		 }
		$(obj).attr("class","btn_s btn_co03 off");
		

	}
	$(obj).focus();
}	

/*
function run_all_fast()
{
	
	var myTimer =null;
	for(var i=0;i<$("a[id='run_cmd']").length;i++)
	{
		if(auto_skip_flag) break;
		
		$("tr[id='trids']").eq(i).attr("style","display:none");
		setTimeout($("a[id='run_cmd']").eq(i).click(), 3000);
		
		//$("a[id='run_cmd']").eq(i).focus();
		//$("a[id='run_cmd']").eq(i).click();
	
	}
	if(auto_skip_flag) auto_skip_flag=false;
}*/

function run_all()
{
	//alert(run_idx);
	run_all_flag=true;
	//pop_close("#preload_pop");
	if(run_idx < $("a[id='run_cmd']").length )
	{
		
		if($("a[id='skip_btn']").eq(run_idx).attr("class")=="btn_s btn_co03 on")
		{
			 run_idx++;
			 run_all();
		}
		else if(typeof($("a[id='run_cmd']").eq(run_idx).attr("onClick"))=="undefined")
		{
			 run_idx++;
			 run_all();
		}else{

			$("a[id='run_cmd']").eq(run_idx).click();
		}
		
	
	}else{
		//if(auto_skip_flag) auto_skip_flag=false;
		run_idx=0;
		run_all_flag=false;
	}
}

//명령어 실행
function run_command(work_command_create_no,obj)
{
	r_work_command_create_no=work_command_create_no;
	
	var request =$.ajax({
		type : "POST",
		url : "/gugPcmdCrExec/run_command",
		//contentType:"application/x-www-form-urlencoded;charset=utf-8", //한글 깨짐 방지
		data : {
			work_command_create_no : work_command_create_no ,
			gug_data_manage_no : "${gugDataConfig.gug_data_manage_no}",
			gug_data_history_no  : "${gugDataConfig.gug_data_history_no}"

		 
		},
		dataType : "text",
		cache: false, 
		async: true,
		success : function(result) { 			 
            //alert("명령어 실행을 완료했습니다.");
            select_command_list(3);
        /*
            var jsonObj = JSON.parse(result);
			alert(jsonObj.model.alarm_cd); */

		//	alarmGrid.load(result,"json")		
		//	alarmGrid.load("${alarmJSON}","json")		
		//	alarmGrid.load("../common/data.json","json");
		//	alarmGrid.loadXMLString(result);			
		},
		error : function(result, status, err) {
			//pop_close("#preload_pop");
			alert('명령어 실행에 실패했습니다');
		},
	 	beforeSend: function() {
	 		if(run_all_flag){
				$("a[id='run_cmd']").eq(run_idx).focus();
				$("td[id='id_state']").eq(run_idx).text("running");
				$("td[id='id_button']").eq(run_idx).text("");
				$("td[id='id_state']").eq(run_idx).attr("style","background-color:red");
				$("td[id='id_button']").eq(run_idx).attr("style","background-color:red");
			}else{
				run_idx=($('a#run_cmd').index($(obj)));
				$("a[id='run_cmd']").eq(run_idx).focus();
				$("td[id='id_state']").eq(run_idx).text("running");
				$("td[id='id_button']").eq(run_idx).text("");
				$("td[id='id_state']").eq(run_idx).attr("style","background-color:red");
				$("td[id='id_button']").eq(run_idx).attr("style","background-color:red");
				
			}
	 		//myLayout.cells("b").progressOn();
		},		
		complete : function() {
			//pop_close("#preload_pop");

		}
	});
}


function pop_view_msgs(work_command_create_no)
{
    $.ajax({
        type: "get",
       // contentType: "json", //==> 생략가능(RestController이기때문에 가능)
        url: "/gugPcmdCrExec/view_msg/"+work_command_create_no ,
        success: function(result){
            	 var dataObj = JSON.parse(result);
            	 var cmd_base_nm=dataObj.create_command_sentence.split("\:");
                 var output="";
                 $("div[id='cmd_base_nm']").text(cmd_base_nm[0]); 
                 $("input[name='equip_name']").val(dataObj.equip_name);
                 $("input[name='create_command_sentence']").val(dataObj.create_command_sentence);
                 $("textarea[name='exec_return_message']").text(dataObj.exec_return_message);
                 pop_open('#pop_view_message');
        },
		error : function(result, status, err) {
			alert('뷰메시지를 여는데 실패했습니다.');
		}
    });

}

var param_lists = [];
var r_work_command_create_no=0;
var r_equip_type="";
var r_equip_id="";
var row_id=0;
//실패한 내역 선택시
function pop_view_in_command(work_command_create_no,etype_cd,cmd,equip_id,obj)
{
	/*
	alert(work_command_create_no);
	alert(etype_cd);
	alert(cmd);
	*/
	row_id=$('a[id="fail_btn"]').index($(obj));
	//$("a[id='run_cmd']").eq(row_id).click();
	//alert(row_id);
	r_work_command_create_no=work_command_create_no;
	r_equip_type=etype_cd;
	r_equip_id=equip_id;
	 $('textarea[id="in_create_command_sentence"]').text(cmd);
	param_lists = [];
	
	var gug_v_lists = [];
	var basis_cmd=cmd.split(":");
	var basis_parameter=basis_cmd[1].trim();
	var slt_parameter=basis_parameter.split(",");
	for(var i=0; i<slt_parameter.length;i++)
	{
		if(slt_parameter[i]!="")
		{
			var sltnm=slt_parameter[i].split("=");
			var temp=[];
			temp.push(sltnm[0]);
			temp.push(sltnm[1].replace(/;/gi, ""));
			gug_v_lists.push(temp);
		}
	}
	
	//alert(basis_cmd[0].trim());
	//return false;
		  
	$("div[id='cmd_title']").text(basis_cmd[0].trim()); 
	$.ajax({
	       type: "get",
	       url: "/indPcmdCrExec/equip_cmd_param_list?gug_equip_type_cd="+ etype_cd +
	    		   "&command=" + basis_cmd[0].trim()
	    		   ,
	       success: function(result){
	           	var dataObj = JSON.parse(result);
	           	 
	           	var ecd =  dataObj.equip_cmd;
	           	var command_help="";
	           	var text_with_br ="";
	           	if(typeof(ecd.command_help)!="undefined")
	           	{
	           	 	text_with_br = ecd.command_help.replace(/\r\n/g,"<br>");
	           	//var normalized_Enters = ecd.command_help.replace(/\r|\/g, "\\r\");
		            //var text_with_br = normalized_Enters.replace(/\\r\/g, "<br />");
		       }
	           	var command_help=((typeof(ecd.command_help)=="undefined")? "" :text_with_br) ;
	            var helpData_txt="";
	           	helpData_txt+="<tr>";
	           	helpData_txt+="<td colspan=2>"+ command_help  +"</td>";
	           	helpData_txt+="<tr>";
	           	 $("tbody[id='helpData']").html(helpData_txt);
	           	 
	           	 var manData_txt="";
	           	 var optData_txt="";
	           	var pri = dataObj.param_list;
	           	 for(var i in pri){
	
	           		var param_items = [];
	           		param_items.push(pri[i].parameter.trim());
	           		param_items.push((typeof(pri[i].man_opt)=="undefined")?"OPT":pri[i].man_opt.trim());	  
	           		param_items.push((typeof(pri[i].parameter_type)=="undefined")?"STRING":pri[i].parameter_type.trim());	 
	           		param_items.push((typeof(pri[i].min)=="undefined")?"1":pri[i].min.trim());	 
					param_items.push((typeof(pri[i].max)=="undefined")?"99999999999":pri[i].max.trim());
	           		param_items.push((typeof(pri[i].parameter_seq_no)=="undefined")?"1":pri[i].parameter_seq_no.trim());

	           		param_lists.push(param_items);
	           		var p_value="";
	           		for(var k=0; k<gug_v_lists.length;k++)
	           		{
	           			if(gug_v_lists[k][0]==param_items[0])
           				{
	           				p_value=gug_v_lists[k][1];
           				}
	           			console.log(gug_v_lists[k][0] + "=" +gug_v_lists[k][1]);
	           		}
	           		 
	           		 if(param_items[1]=="MAN")
           			 {
	           			if(param_items[2]=="ENUM")
	           			{
		           			manData_txt+="</tr>";
		           			manData_txt+="<tr>";
		           			manData_txt+="<th><span style='color:red;'>" + pri[i].parameter + "</span></th>";
		           			manData_txt+="<td>";
		           			manData_txt+="<select name=\"" + pri[i].parameter + "\">";
		           			manData_txt+="<option value=\"\" ></option>";
		           			var tmpOp=pri[i].enum_list.split("/");
		           			for(var j=0;j<tmpOp.length;j++){
		           				if(p_value==tmpOp[j].trim())
	           					{
			           				manData_txt+="<option value=\"" + tmpOp[j].trim() +"\" selected >" + tmpOp[j].trim() + "</option>";
	           					}else{
	           						manData_txt+="<option value=\"" + tmpOp[j].trim() +"\" >" + tmpOp[j].trim() + "</option>";
	           					}

		           			}
		           			
		           			manData_txt+="</select>";
		           			manData_txt+="</td>";
		           			manData_txt+="</tr>";		           			
		           			
	           			}else if(param_items[2]=="STRING"){
	           			
		           			manData_txt+="<tr>";
		           			manData_txt+="<th><span style='color:red;'>" + pri[i].parameter + "</span></th>";
		           			manData_txt+="<td><input type=\"text\" " +
		           			                  " name=\"" + pri[i].parameter.trim() + "\"" +
		           			                  " minlength=\"" + ((typeof(pri[i].min)=="undefined")?"1":pri[i].min.trim()) + "\"" +
		           			                  " maxlength=\"" + ((typeof(pri[i].max)=="undefined")?"99999999999":pri[i].max.trim()) + "\"" +
		           			                 " value=\"" + p_value + "\"></td>";
		           			manData_txt+="</tr>";	
	           			
	           			}else{
		           			manData_txt+="<tr>";
		           			manData_txt+="<th><span style='color:red;'>" + pri[i].parameter + "</span></th>";
		           			manData_txt+="<td><input type=\"text\" name=\"" + pri[i].parameter + "\" value=\"" + p_value + "\"></td>";
		           			manData_txt+="</tr>";
	           				
	           			}
           			 }else {
 	           			if(param_items[2]=="ENUM")
	           			{
	 	           			optData_txt+="<tr>";
	 	           			optData_txt+="<th><span>" + pri[i].parameter + "</span></th>";
	 	           			optData_txt+="<td>";
	 	           			optData_txt+="<select name=\"" + pri[i].parameter + "\">";
	 	           		    optData_txt+="<option value=\"\" ></option>";
		           			var tmpOp=pri[i].enum_list.split("/");
		           			for(var j=0;j<tmpOp.length;j++){
		           				if(p_value==tmpOp[j].trim())
	           					{
		           					optData_txt+="<option value=\"" + tmpOp[j].trim() +"\" selected >" + tmpOp[j].trim() + "</option>";
	           					}else{
	           						optData_txt+="<option value=\"" + tmpOp[j].trim() +"\" >" + tmpOp[j].trim() + "</option>";
	           					}
		           			}


	 	          			optData_txt+="</select>";
	 	         			optData_txt+="</td>";
	 	           			optData_txt+="</tr>";
	 	           		
	           			}else if(param_items[2]=="STRING"){
	           				optData_txt+="<tr>";
	           				optData_txt+="<th><span>" + pri[i].parameter + "</span></th>";
	           				optData_txt+="<td><input type=\"text\" " +
		           			                  " name=\"" + pri[i].parameter.trim() + "\"" +
		           			                  " minlength=\"" + ((typeof(pri[i].min)=="undefined")?"1":pri[i].min.trim()) + "\"" +
		           			                  " maxlength=\"" + ((typeof(pri[i].max)=="undefined")?"99999999999":pri[i].max.trim()) + "\"" +
		           			                  " value=\"" + p_value + "\"></td>";
		           			optData_txt+="</tr>";	
		           			
	           			}else{
	 	           			optData_txt+="<tr>";
	 	           			optData_txt+="<th><span>" + pri[i].parameter + "</span></th>";
	 	           			optData_txt+="<td><input type=\"text\" name=\"" + pri[i].parameter + "\" value=\"" + p_value + "\"></td>";
	 	           			optData_txt+="</tr>";	           				
	           			}
           			 }
	           		 
	           		
	           		//html_txt +=", " + dataObj[i].parameter + ":" + dataObj[i].parameter_type;
	           		 
	           		 //html_txt +="<li><input name=\"sel_equip_id\" type=\"radio\" value=\"" + dataObj[i].equip_id + "\">" +
	           		   //   "<label for=\"ck_1\">" + dataObj[i].equip_name + "</label></li>";
	
	           	 }
	           	 
	           	 //alert(html_txt);
	           $("tbody[id='manData']").html(manData_txt); 
	           $("tbody[id='optData']").html(optData_txt); 
	           
	           //$("h3[id='result_msg_title']").html("<span>Result Message</span> "+cmd); 
	           
	          // $("input[name='equip_name']").val("");
	           
	           pop_open('#in_create_pop');
	           
	       },

		error : function(result, status, err) {
			alert('해당하는 명령어의 파라메터를 가져오는데 실패했습니다.');
		}
	   });
}



//탭에 해당 장비구분명 선택시
function gug_select_equip_type(etype_nm,etype_cd)
{
	select_equip_type=etype_nm;
	select_equip_type_cd=etype_cd;
//	r_equip_type=etype_nm;
	$("li[id='liMME']").attr("class","off");
	$("li[id='liEPCC']").attr("class","off");
	$("li[id='liPGW']").attr("class","off");
	$("li[id='liHSS']").attr("class","off");
	$("li[id='li"+ etype_nm + "']").attr("class","on");
	

	$("table[id='trMMEGugData']").attr("style","display:none");
	$("table[id='trEPCCGugData']").attr("style","display:none");
	$("table[id='trPGWGugData']").attr("style","display:none");
	$("table[id='trHSSGugData']").attr("style","display:none");
	
	$("table[id='tr" + etype_nm + "GugData']").attr("style","display:block");
	
	//alert(333);
 	$.ajax({
	 	type: "get",
        data : {
        	equip_type :  etype_nm,
        	gug_data_manage_no : "${gugDataConfig.gug_data_manage_no}",
        	gug_data_history_no : "${gugDataConfig.gug_data_history_no}"
		},
		dataType : "text",
		cache: false, 
        url: "/gugPcmdCrExec/equip_list" ,
        success: function(result){
            	 var dataObj = JSON.parse(result);
            	 var html_txt="";
            	 for(var i in dataObj){
            		 if(dataObj[i].command_create_count > 0)
            		 {
            			 html_txt +="<li><input name=\"sel_equip_id\" type=\"checkbox\" value=\"" +dataObj[i].equip_id+ "\" disabled checked>"

            		 }else{
            			 html_txt +="<li><input name=\"sel_equip_id\" type=\"checkbox\" value=\"" +dataObj[i].equip_id+ "\">";

            		 }
            		 html_txt +="<label for=\"ck_1\">" +dataObj[i].equip_name+ "</label></li>";
            
            	 }
            	 
            $("ul[id='equip_list']").html(html_txt); 

        },
		error : function(result, status, err) {
			alert('Target System을 여는데 실패했습니다.');
		}
    });
}


//모두 체크/모두 미체크의 체크박스 처리
function changeCheckBox(flag)
{

	
	 $('input:checkbox[name="checkNo"]').each(function() {
	      this.checked = flag; //checked 처리
	      $(this).attr("disabled",false);
	 });
	 
	 $('input:checkbox[name="checkEquipName"]').each(function() {
	      this.checked = flag; //checked 처리
	      $(this).attr("disabled",false);
	 });
	
	 $('input:checkbox[name="checkEquipNameALL"]').prop("checked",flag); 
}

//모두 체크/모두 미체크의 체크박스 처리
function changeCheckEquipNameBox(flag)
{
	 $('input:checkbox[name="checkEquipName"]').each(function() {
	      this.checked = flag; //checked 처리
	      $(this).attr("disabled",false);
	 });
	
	 $('input:checkbox[name="checkNo"]').each(function() {
	      this.checked = flag; //checked 처리
	      $(this).attr("disabled",false);
	 });
	
	 $('input:checkbox[name="checkALL"]').prop("checked",flag); 

}	

//모두 체크/모두 미체크의 체크박스 처리
function changeCheckEN(flag,vv)
{
	 $('input:checkbox[name="checkNo"]').each(function() {
		 var j=$('input:checkbox[name="checkNo"]').index($(this));
		 var eqName2=$('tr[id="trids"]').eq(j).text();
		 if(eqName2.indexOf(vv) > -1)
		 {
	      $('input:checkbox[name="checkNo"]').eq(j).prop("checked",flag);
	      $('input:checkbox[name="checkNo"]').eq(j).attr("disabled",flag);
		 }
	 });

}	

//최상위 이동
function topMove()
{
	// item 의 최대번호 구하기 
	//var lastItemNo = $("#mmeTrData tr:last").attr("class").replace("item", "");
	//var newitem = $("#mmeTrData tr:eq(1)").clone();
	//	var newitem = $("#mmeTrData tr:last").clone();
	// ("#mmeTrData").add(newitem);
	var eqName2="";
	$('input:checkbox[name="checkNo"]').each(function() {
		 if(this.checked == true)
		 {
			 var j=$('input:checkbox[name="checkNo"]').index($(this));
			  eqName2+="<tr id=\"trids\">" + $('tr[id="trids"]').eq(j).clone().html() +"</tr>";
			  $('tr[id="trids"]').eq(j).remove();
		}
	 });
	$("#trData").prepend(eqName2);
/*	var det_no=$("#mmeTrData tr:last>td:first>input[name='mme_det_no']").val()
	$.trClone = $("#mmeTrData tr:last").clone().html();
                $.newTr = $("<tr>"+$.trClone+"</tr>");
                // append
                $("#mmeTrData").append($.newTr);
                $("#mmeTrData tr:last>td:first>input[name='mme_det_no']").val(Number(det_no)+1);*/
           // alert($("#mmeTrData tr:last>td:first>input[name='mme_det_no']").val());

}

//최하위이동
function bottomMove()
{
	var eqName2="";
	$('input:checkbox[name="checkNo"]').each(function() {
		 if(this.checked == true)
		 {
			 var j=$('input:checkbox[name="checkNo"]').index($(this));
			  eqName2+="<tr id=\"trids\">" + $('tr[id="trids"]').eq(j).clone().html() +"</tr>";
			  $('tr[id="trids"]').eq(j).remove();
		}
	 });
	$("#trData").append(eqName2);


}

//이전이동
function prevMove()
{
	var group_flag=true;
	var equip_names = [];
	var content = [];
	var move_inx=[];
	
	$('input:checkbox[name="checkEquipName"]').each(function() {
			var vv=this.value
			 $('input:checkbox[name="checkNo"]').each(function() {
				 var j=$('input:checkbox[name="checkNo"]').index($(this));
				 var eqName2=$('tr[id="trids"]').eq(j).text();
				 if(eqName2.indexOf(vv) > -1)
				 {
					 equip_names.push(vv);
					 content.push( "<tr id=\"trids\">" + $('tr[id="trids"]').eq(j).clone().html() +"</tr>");
				 }
			 });
			//장비별 체크박스 여부
			if(this.checked == true)
			 {
				group_flag=true;
				var vv=this.value
				 $('input:checkbox[name="checkNo"]').each(function() {
					 var j=$('input:checkbox[name="checkNo"]').index($(this));
					 var eqName2=$('tr[id="trids"]').eq(j).text();
					 if(eqName2.indexOf(vv) > -1)
					 {
						 move_inx.push(j);
					 }
				 });
			}
			
	 });
	

	//단건 체크박스 체크여부
	if(move_inx.length<=0){
		$('input:checkbox[name="checkNo"]').each(function() {
			 if(this.checked == true)
			 {
				 group_flag=false;
				 move_inx.push($('input:checkbox[name="checkNo"]').index($(this)));
			 }
		});
	}
	if(move_inx.length <=0)
	{
		alert("선택된 생성 명령어가 없습니다.");
		return false;
	}
	
	
	var prev_i=0;  //다음 인덱스
	var temp_equip_name=equip_names[move_inx[0] - 1];
	//alert(temp_equip_name);
	if(group_flag==true){
		for(var i=(move_inx[0] - 1);i>=0;i--)
		{
			console.log(equip_names[i]);
			if(equip_names[i] != temp_equip_name )
			{
				console.log("dddd="+i);
				prev_i=i;
				break;
			}
		}
	}else{
		
		prev_i=(move_inx[0] - 2);
	}
	//alert(prev_i);
	var first_html_text="";
	var move_html_text="";
	var swap_html_text="";
	var last_html_text="";
	if(prev_i==0)prev_i=-1;
	for(var i=0;i< equip_names.length;i++)
	{
		if(i <= prev_i )
		{
			first_html_text+=content[i];
		}
		if(i>=move_inx[0] && i <= move_inx[ move_inx.length-1])
		{
			move_html_text+=content[i];
		}
		
		if(i > prev_i && i<move_inx[0]  )
		{
			swap_html_text+=content[i];
		}
		if(i > move_inx[move_inx.length-1] )
		{
			last_html_text+=content[i];
		}
	}

	
	
	$("#trData").html(first_html_text+  move_html_text + swap_html_text + last_html_text);
	//alert($('tr[id="trids"]').length);



}

//다음칸이동
function nextMove()
{
	var group_flag=true;
	var equip_names = [];
	var content = [];
	var move_inx=[];
	
	$('input:checkbox[name="checkEquipName"]').each(function() {
			var vv=this.value
			 $('input:checkbox[name="checkNo"]').each(function() {
				 var j=$('input:checkbox[name="checkNo"]').index($(this));
				 var eqName2=$('tr[id="trids"]').eq(j).text();
				 if(eqName2.indexOf(vv) > -1)
				 {
					 equip_names.push(vv);
					 content.push( "<tr id=\"trids\">" + $('tr[id="trids"]').eq(j).clone().html() +"</tr>");
				 }
			 });
			//장비별 체크박스 여부
			if(this.checked == true)
			 {
				group_flag=true;
				var vv=this.value
				 $('input:checkbox[name="checkNo"]').each(function() {
					 var j=$('input:checkbox[name="checkNo"]').index($(this));
					 var eqName2=$('tr[id="trids"]').eq(j).text();
					 if(eqName2.indexOf(vv) > -1)
					 {
						 move_inx.push(j);
					 }
				 });
			}
			
	 });
	

	//단건 체크박스 체크여부
	if(move_inx.length<=0){
		$('input:checkbox[name="checkNo"]').each(function() {
			 if(this.checked == true)
			 {
				 group_flag=false;
				 move_inx.push($('input:checkbox[name="checkNo"]').index($(this)));
			 }
		});
	}
	
	if(move_inx.length <=0)
	{
		alert("선택된 생성 명령어가 없습니다.");
		return false;
	}
	
	var next_i=0;  //다음 인덱스
	var temp_equip_name=equip_names[move_inx[move_inx.length-1] + 1];
	if(group_flag==true){
		for(var i=(move_inx[move_inx.length-1] + 1);i< equip_names.length;i++)
		{
			if(equip_names[i] != temp_equip_name )
			{
				console.log(i);
				next_i=i;
				break;
			}
		}
	}else{
		
		next_i=(move_inx[move_inx.length-1] + 2);
	}
	if(next_i==0)next_i=(equip_names.length);
	var first_html_text="";
	var move_html_text="";
	var swap_html_text="";
	var last_html_text="";
	for(var i=0;i< equip_names.length;i++)
	{
		if(i <move_inx[0] )
		{
			first_html_text+=content[i];
		}
		if(i>=move_inx[0] && i <= move_inx[ move_inx.length-1])
		{
			move_html_text+=content[i];
		}
		if(i>move_inx[ move_inx.length-1] && i < next_i)
		{
			swap_html_text+=content[i];
		}
		if(i >= next_i )
		{
			last_html_text+=content[i];
		}
	}

	
	
	$("#trData").html(first_html_text+swap_html_text + move_html_text + last_html_text);
	//alert($('tr[id="trids"]').length);



}


$( document ).ready(function() {
	//$("table[id='trMMEGugData']").attr("style","display:none");
	$("table[id='trEPCCGugData']").attr("style","display:none");
	$("table[id='trPGWGugData']").attr("style","display:none");
	$("table[id='trHSSGugData']").attr("style","display:none");
	select_command_list(1);
    console.log( "ready!" );


});

//실패난거 개별 명령어 생성
function individual_command_create()
{

    
	var create_command_sentence="";
	create_command_sentence=$("div[id='cmd_title']").text() +":";
	create_command_sentence+="SYS_TYPE="+r_equip_type;
	create_command_sentence+=",SYS_NAME="+ r_equip_id;
    
    /*
param_items.push(pri[i].parameter.trim());
param_items.push(pri[i].man_opt.trim());	  
param_items.push(pri[i].parameter_type.trim());	 
param_items.push((typeof(pri[i].min)=="undefined")?"":pri[i].min.trim());	 
param_items.push((typeof(pri[i].max)=="undefined")?"":pri[i].max.trim());	
    */
	var commaTxt="";
    for(var i=0; i < param_lists.length;i++)
    {
    	var parameter=param_lists[i][0];
    	var man_opt=param_lists[i][1];
    	var parameter_type=param_lists[i][2];
    	var min=param_lists[i][3];
    	var max=param_lists[i][4];
    	var parameter_seq_no=param_lists[i][5];
    	var p_val_length=0;
    	var p_val="";

    	if(parameter_type=="ENUM")
    	{
	    	p_val_length=$.trim($("select[name='" + parameter + "']").val()).length;
	    	p_val=$.trim($("select[name='" + parameter + "']").val());
    	}else{
	    	p_val_length=$.trim($("input[name='" + parameter + "']").val()).length;
	    	p_val=$.trim($("input[name='" + parameter + "']").val());
    	}
    	if(man_opt=="MAN")
   		{
    		if(p_val_length==0)
    		{
    			alert(parameter +"의 파라메터가 주요값이므로 입력하세요");
    			$("input[name='" + parameter + "']").focus();
    			return false;
    		}else{
    			if(parameter_type=="STRING")
    			{
    				if(p_val_length < Number(min))
   					{
    					alert(parameter +"의 파라메터는 최소 " +min+ "자리입니다.\n현재 " +p_val_length+ "자리입니다.");
    	    			$("input[name='" + parameter + "']").focus();
    	    			return false;
   					}else if(p_val_length > Number(max)){
    					alert(parameter +"의 파라메터는 최대 " +max+ "자리입니다.\n현재 " +p_val_length+ "자리입니다.");
    	    			$("input[name='" + parameter + "']").focus();
    	    			return false;
   					}
    			}else if(parameter_type=="DECIMAL"){
    				if($.isNumeric( p_val)==false){
    					alert(parameter +"의 파라메터는 숫자값만 입력받습니다.\n현재 " +p_val+ " 값입니다.");
    	    			$("input[name='" + parameter + "']").focus();
    	    			return false;
    				}if(Number(p_val) < Number(min)){
    					alert(parameter +"의 파라메터는 최소값이 " +min+ "부터입니다.\n현재 " +p_val+ " 값입니다.");
    	    			$("input[name='" + parameter + "']").focus();
    	    			return false;
   					}else if(Number(p_val) > Number(max)){
    					alert(parameter +"의 파라메터는 최대값이 " +max+ "까지입니다.\n현재 " +p_val+ " 값입니다.");
    	    			$("input[name='" + parameter + "']").focus();
    	    			return false;
   					}
    				
    			}
    		}
   		
   		}else{
   			
   			if(p_val_length != 0)
    		{
    			if(parameter_type=="STRING")
    			{
    				if(p_val_length < Number(min))
   					{
    					alert(parameter +"의 파라메터는 최소 " +min+ "자리입니다.\n현재 " +p_val_length+ "자리입니다.");
    	    			$("input[name='" + parameter + "']").focus();
    	    			return false;
   					}else if(p_val_length > Number(max)){
    					alert(parameter +"의 파라메터는 최대 " +max+ "자리입니다.\n현재 " +p_val_length+ "자리입니다.");
    	    			$("input[name='" + parameter + "']").focus();
    	    			return false;
   					}
    			}else if(parameter_type=="DECIMAL"){
    				if($.isNumeric( p_val)==false){
    					alert(parameter +"의 파라메터는 숫자값만 입력받습니다.\n현재 " +p_val+ " 값입니다.");
    	    			$("input[name='" + parameter + "']").focus();
    	    			return false;
    				}if(Number(p_val) < Number(min)){
    					alert(parameter +"의 파라메터는 최소값이 " +min+ "부터입니다.\n현재 " +p_val+ " 값입니다.");
    	    			$("input[name='" + parameter + "']").focus();
    	    			return false;
   					}else if(Number(p_val) > Number(max)){
    					alert(parameter +"의 파라메터는 최대값이 " +max+ "까지입니다.\n현재 " +p_val+ " 값입니다.");
    	    			$("input[name='" + parameter + "']").focus();
    	    			return false;
   					}
    				
    			}
    		}
   		}
    	
        if(p_val_length==0)
       	{
			commaTxt+="," ;
       	}else{
			create_command_sentence+= commaTxt+ ","+parameter + "="+p_val;
			commaTxt="";
       	}
    }

    create_command_sentence+=";";
    

    //alert(create_command_sentence);
    //return false;	
	$.ajax({
		type : "POST",
		url : "/gugPcmdCrExec/fail_cmd_create",
		data : {
			work_command_create_no : r_work_command_create_no,
			gug_data_manage_no : "${gugDataConfig.gug_data_manage_no}",
			gug_data_history_no  : "${gugDataConfig.gug_data_history_no}",
			create_command_sentence : create_command_sentence,
			equip_type_nm : r_equip_type,
			equip_id: r_equip_id
		 
		},
		dataType : "text",
		cache: false, 
		success : function(result) { 	
			var dataObj = JSON.parse(result);
			$('textarea[id="in_create_command_sentence"]').text(dataObj.create_command_sentence);
	            $("input:hidden[name='work_command_create_no']").val(dataObj.work_command_create_no);
            alert("생성을 완료했습니다.");

		},
		error : function(result, status, err) {
			alert('생성에 실패했습니다');
		},
	 	beforeSend: function() {
	 		//myLayout.cells("b").progressOn();
		},		
		complete : function() {
			//myLayout.cells("b").progressOff();
		}
	});
	
}


function individual_run_command()
{
	
	var request =$.ajax({
		type : "POST",
		url : "/gugPcmdCrExec/run_command",
		//contentType:"application/x-www-form-urlencoded;charset=utf-8", //한글 깨짐 방지
		data : {
			work_command_create_no : r_work_command_create_no ,
			gug_data_manage_no : "${gugDataConfig.gug_data_manage_no}",
			gug_data_history_no  : "${gugDataConfig.gug_data_history_no}"

		 
		},
		dataType : "text",
		cache: false, 
		async: true,
		success : function(result) { 
			var dataObj = JSON.parse(result);
			if(dataObj.exec_state_cd=="D002")
			{
				alert("명령어 실행 결과가 성공입니다.");
			}else{
				alert("명령어 실행 결과가 실패입니다.\n *실패메시지----------- \n"+dataObj.exec_return_message);

			}
            
            select_command_list(3);
        /*
            var jsonObj = JSON.parse(result);
			alert(jsonObj.model.alarm_cd); */

		//	alarmGrid.load(result,"json")		
		//	alarmGrid.load("${alarmJSON}","json")		
		//	alarmGrid.load("../common/data.json","json");
		//	alarmGrid.loadXMLString(result);			
		},
		error : function(result, status, err) {
			//pop_close("#preload_pop");
			alert('명령어 실행에 실패했습니다');
		},
	 	beforeSend: function() {
				run_idx=row_id;
				$("a[id='run_cmd']").eq(run_idx).focus();
				$("td[id='id_state']").eq(run_idx).text("running");
				$("td[id='id_button']").eq(run_idx).text("");
				$("td[id='id_state']").eq(run_idx).attr("style","background-color:red");
				$("td[id='id_button']").eq(run_idx).attr("style","background-color:red");
	 		//myLayout.cells("b").progressOn();
		},		
		complete : function() {
			//pop_close("#preload_pop");

		}
	});
	//$("a[id='run_cmd']").eq(row_id).click();
}



</script>
</head>

<body onload="//command_create()">
<div id="wrap">
    
    <header>
		<h1 class="logo"><a href="#">KT E2E Infra Orchestrator</a></h1>
		<nav id="gnb">
			<ul>
				<li><a href="#">DESIGN</a></li>
				<li><a href="#">PROVISIONING</a></li>
				<li><a href="#">MONITORING</a></li>
				<li><a href="#">INVENTORY</a></li>
				<li><a href="#">CONFIGURATION</a></li>
				<li class="active"><!-- 활성화 class="active" -->
					<a href="#">P-LTE</a>
					<ul>
						<li><a href="/gugDataConfig">국데이터 관리</a></li><!-- 활성화 class="active" -->
						<li class="active"><a href="/gugPcmdCrExec">명령 실행</a></li>		
						<li><a href="/indPcmdCrExec">운용자 명령어</a></li>
						<li><a href="/cust">고객사 관리</a></li>
						<li><a href="/equipInven">장비 관리</a></li>
						<li><a href="#">모니터링</a></li>
						<li><a href="#">이력조회</a></li>
					</ul>
				</li>
			</ul>
		</nav><!-- //gnb -->
		<div class="ad_info">
			<i><img src="/new_resources/resources/images/layout/img_ad_info.jpg" alt=""></i><!-- image size : 25*25px -->
			<p>admin</p>
		</div><!-- //ad_info -->
    </header>
    
    <div id="container">	
		
		<ul id="navigation">
			<li><img src="/new_resources/resources/images/layout/img_navigation_home.png" alt="home"></li>
			<li>국데이터 관리</li>
			<li>명령 실행</li>
			<li>${gugDataConfig.profile_nm}</li>
		</ul><!-- //navigation -->
		
		<div id="contents_wrap">
			<div class="contents">
			
				<ul class="tab">
					<li id="liMME" class="on"><a href="#" onClick="gug_select_equip_type('MME','D001')">MME</a></li>
					<li id="liEPCC"><a href="#" onClick="gug_select_equip_type('EPCC','D002')">ePCC</a></li>
					<li id="liPGW"><a href="#" onClick="gug_select_equip_type('PGW','D003')">PGW</a></li>
					<li id="liHSS"><a href="#" onClick="gug_select_equip_type('HSS','D004')">HSS</a></li>
				</ul><!-- //tab -->
				
				<div class="float_area">
					<div class="f_box fl">
						<div class="table_form texList scrollbar-inner">
							<table id="trMMEGugData">
								<caption>정보 테이블</caption>
								<colgroup>
									<col span="1" style="width:18%;">
									<col span="1" style="width:42%;">
									<col span="1" style="width:18%;">
									<col span="1" style="width:22%;">
								</colgroup>
								<tbody>
								
									<tr>
										<th><span>고객사</span></th>
										<td colspan=3>${gugDataConfig.cust_nm}</td>

									</tr>
									<tr>
										<th><span>APN-FQDN</span></th>
										<td id="apn_fqdn">${gugDataConfig.apn_fqdn}</td>
										<th><span>PGWGRP</span></th>
										<td id="pgwgrp">${gugDataConfig.pgwgrp}</td>


									</tr>
									<c:forEach var="mmeDetLst" items="${mmeDetLst}">
									<tr>
										<th><span>PGW</span></th>
										<td id="pgw">${mmeDetLst.pgw}</td>
										<th><span>IP</span></th>
										<td id="ip">${mmeDetLst.ip}</td>

									</tr>
									<tr>
										<th><span>NAME</span></th>
										<td id="name">${mmeDetLst.name}</td>
										<th><span>CAPA</span></th>
										<td id="capa">${mmeDetLst.capa}</td>

									</tr>
									</c:forEach>
								</tbody>
								</table>	
								<table id="trEPCCGugData">
								<caption>정보 테이블</caption>
								<colgroup>
									<col span="1" style="width:11%;">
									<col span="1" style="width:22%;">
									<col span="1" style="width:11%;">
									<col span="1" style="width:22%;">
									<col span="1" style="width:11%;">
									<col span="1" style="width:22%;">									
								</colgroup>
								<tbody>
								
									<tr>
										<th><span>고객사</span></th>
										<td colspan=5>${gugDataConfig.cust_nm}</td>

									</tr>
									<tr>
										<th><span>PGW IP</span></th>
										<td id="pgw_ip" colspan=2>${gugDataConfig.pgw_ip}</td>
										<th><span>SMS 수신</span></th>
										<td id="sms_recv" colspan=2>${gugDataConfig.sms_recv}</td>


									</tr>
									<c:forEach var="epccDetLst" items="${epccDetLst}">
									<tr>
										<th><span>IP POOL</span></th>
										<td id="ip_pool" colspan=5>${epccDetLst.ip_pool}</td>

									</tr>
									</c:forEach>
									
								</tbody>		
								</table>
								
								<table id="trPGWGugData">
								<caption>정보 테이블</caption>
								<colgroup>
									<col span="1" style="width:20%;">
									<col span="1" style="width:20%;">
									<col span="1" style="width:15%;">
									<col span="1" style="width:20%;">
									<col span="1" style="width:15%;">
									<col span="1" style="width:20%;">
								</colgroup>
								<tbody>
								
									<tr>
										<th><span>고객사</span></th>
										<td colspan=3>${gugDataConfig.cust_nm}</td>

									</tr>
									<c:forEach var="pgwLst" items="${pgwLst}">
									<tr>
										<th><span>IP POOL ID</span></th>
										<td id="ip_pool_id" colspan=2>${pgwLst.ip_pool_id}</td>
										<th><span>VR_ID</span></th>
										<td id="vr_id" colspan=2>${pgwLst.vr_id}</td>
									</tr>
									<tr>
										<th><span>POOL TYPE</span></th>
										<td id="pool_type_cd" colspan=2>${pgwLst.pool_type_cd}</td>
										<th><span>STATIC</span></th>
										<td id="static_cd" colspan=2>${pgwLst.static_cd}</td>
									</tr>
									<tr>
										<th><span>TUNNEL</span></th>
										<td id="tunnel_cd" colspan=2>${pgwLst.tunnel_cd}</td>
										<th><span>START ADDR</span></th>
										<td id="start_addr" colspan=2>${pgwLst.start_addr}</td>
									</tr>		
									<tr>
										<th><span>VLAN</span></th>
										<td id="vlan" colspan=2>${pgwLst.vlan}</td>
										<th><span>IP ADDR</span></th>
										<td id="ip_addr" colspan=2>${pgwLst.ip_addr}</td>
									</tr>								
									<tr>
										<th><span>NETWORK</span></th>
										<td id="network" colspan=2>${pgwLst.network}</td>
										<th><span>GATEWAY</span></th>
										<td id="gateway" colspan=2>${pgwLst.gateway}</td>
									</tr>		
									<tr>
										<th><span>우선순위</span></th>
										<td id="prirank_cd" colspan=2>${pgwLst.prirank_cd}</td>
										<th><span>I / F</span></th>
										<td id="ifc" colspan=2>${pgwLst.ifc}</td>
									</tr>	
									<tr>
										<th><span>APN ID</span></th>
										<td id="apn_id" colspan=2>${pgwLst.apn_id}</td>
										<th><span>APN NAME</span></th>
										<td id="apn_name" colspan=2>${pgwLst.apn_name}</td>
									</tr>		
									<tr>
										<th><span>IP ALLOC</span></th>
										<td id="ip_alloc_cd" colspan=2>${pgwLst.ip_alloc_cd}</td>
										<th><span>RAD IP ALLOC</span></th>
										<td id="rad_ip_alloc_cd" colspan=2>${pgwLst.rad_ip_alloc_cd}</td>
									</tr>		
									<tr>
										<th><span>AUTH / PCRF /PCC</span></th>
										<td id="auth_pcrf_pcc_cd" colspan=2>${pgwLst.auth_pcrf_pcc_cd}</td>
										<th><span>ACCT/REACT</span></th>
										<td id="acct_react_cd" colspan=2>${pgwLst.acct_react_cd}</td>
									</tr>		
									<tr>
										<th><span>PRI DNS V4</span></th>
										<td id="pri_dns_v4" colspan=5>${pgwLst.pri_dns_v4}</td>
									</tr>	
									<tr>
										<th><span>RAD AUTH ID</span></th>
										<td id="rad_auth_id">${pgwLst.rad_auth_id}</td>
										<th><span>- ACT</span></th>
										<td id="rad_auth_act">${pgwLst.rad_auth_act}</td>
										<th><span>- SBY</span></th>
										<td id="rad_auth_sby">${pgwLst.rad_auth_sby}</td>
									</tr>		
									<tr>
										<th><span>RAD ACCT ID</span></th>
										<td id="rad_acct_id">${pgwLst.rad_acct_id}</td>
										<th><span>- ACT</span></th>
										<td id="rad_acct_act">${pgwLst.rad_acct_act}</td>
										<th><span>- SBY</span></th>
										<td id="rad_acct_sby">${pgwLst.rad_acct_sby}</td>
									</tr>	
									<tr>
										<th><span>DIAM PCFR ID</span></th>
										<td id="diam_pcfr_id">${pgwLst.diam_pcfr_id}</td>
										<th><span>- ACT</span></th>
										<td id="diam_pcfr_act">${pgwLst.diam_pcfr_act}</td>
										<th><span>- SBY</span></th>
										<td id="diam_pcfr_sby">${pgwLst.diam_pcfr_sby}</td>
									</tr>		
									<tr>
										<th><span>LOCAL PCC PF ID</span></th>
										<td id="local_pcc_pf_id">${pgwLst.local_pcc_pf_id}</td>
										<th><span>- ACT</span></th>
										<td id="local_pcc_pf_act">${pgwLst.local_pcc_pf_act}</td>
										<th><span>- SBY</span></th>
										<td id="local_pcc_pf_sby">${pgwLst.local_pcc_pf_sby}</td>
									</tr>	
									<tr>
										<th><span>SEC DNS V4</span></th>
										<td id="sec_dns_v4">${pgwLst.sec_dns_v4}</td>
										<th><span>ARP MAP HIGH</span></th>
										<td id="arp_map_high">${pgwLst.arp_map_high}</td>
										<th><span>ARP MAP MED</span></th>
										<td id="arp_map_med">${pgwLst.arp_map_med}</td>
									</tr>
									</c:forEach>																																															
								</tbody>	
								</table>								
								<table id="trHSSGugData">
								<caption>정보 테이블</caption>
								<colgroup>
									<col span="1" style="width:18%;">
									<col span="1" style="width:42%;">
									<col span="1" style="width:18%;">
									<col span="1" style="width:22%;">
								</colgroup>
								<tbody>
								
									<tr>
										<th><span>고객사</span></th>
										<td colspan=3>${gugDataConfig.cust_nm}</td>

									</tr>
									<tr>
										<th><span>ID</span></th>
										<td id="id" >${gugDataConfig.id}</td>
										<th><span>APN</span></th>
										<td id="apn" >${gugDataConfig.apn}</td>
									</tr>
									<tr>
										<th><span>APNOIREPL</span></th>
										<td id="apnoirepl">${gugDataConfig.apnoirepl}</td>
										<th><span>PLTECC</span></th>
										<td id="pltcc">${gugDataConfig.pltcc}</td>
									</tr>
									<tr>
										<th><span>UUTYPE</span></th>
										<td id="uutype">${gugDataConfig.uutype}</td>
										<th><span>MPNAME</span></th>
										<td id="mpname_cd">${gugDataConfig.mpname_cd}</td>
									</tr>		
																																																						
								</tbody>
								
							</table>
						</div><!-- //table_form scrollbar-inner -->
					</div><!-- //f_box -->
					
					<div class="f_box fr">
						<h4 class="title">Target System </h4>
						<div class="checkbox_area scrollbar-inner">
							<ul id="equip_list">
							<c:forEach var="equip" items="${equip_list}">
							<c:choose>
		                        <c:when test="${equip.command_create_count > 0}">
		                           	<li><input name="sel_equip_id" type="checkbox" value="${equip.equip_id}" disabled checked><label for="ck_1">${equip.equip_name}</label></li>

		                        </c:when>
		                        <c:otherwise>
		                           	<li><input name="sel_equip_id" type="checkbox" value="${equip.equip_id}"><label for="ck_1">${equip.equip_name}</label></li>

		                        </c:otherwise>
		                    </c:choose>
							</c:forEach>	
							 
							</ul>
						</div><!-- //checkbox_area scrollbar-inner -->
					</div><!-- //f_box -->
					
				</div><!-- //float_area -->
				
				<div class="btn_area">
					<a href="#" onClick="command_create();" class="btn_l btn_co01">명령어 생성</a>
				</div>
				
				<h3 class="title"><span>Run Command List</span></h3>
				<div class="table_ty">
					<div class="table_head">
						<div class="btn_area">
							<a href="javascript:void(0);" class="btn_updown first" onClick="topMove();">최상위 단계</a>
							<a href="javascript:void(0);" class="btn_updown prev" onClick="prevMove();">이전 단계</a>
							<a href="javascript:void(0);" class="btn_updown next" onClick="nextMove();">다음 단계</a>
							<a href="javascript:void(0);" class="btn_updown last" onClick="bottomMove();">최하위 단계</a>
							<a href="javascript:void(0);" class="btn_l btn_co02 on" onClick="run_all()">Run All</a>
							<div class="skip_area">
								<a href="javascript:void(0);" id="skip_process_btn" class="btn_l btn_co02">Auto Skip<i class="autoskip"></i></a>
								<ul class="list">
									<li><a href="javascript:void(0);" onClick="auto_stop_skip_process(1)">Auto Skip</a></li>
									<li><a href="javascript:void(0);" onClick="auto_stop_skip_process(2)">Stop Skip</a></li>
								</ul>
							</div>
							<a href="#" onClick="oper_confirm()" class="btn_l btn_co01 on">명령어 확인</a>
						</div>
					</div>
					<table>
						<caption>목록 테이블</caption>
						<colgroup>
							<col span="1" style="width:5%;">
							<col span="1" style="width:5%;">
							<col span="1" style="width:5%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:30%;">
						</colgroup>
						<thead>
							<tr>
								<th scope="col"><input type="checkbox" name="checkEquipNameALL" value="" onchange="changeCheckEquipNameBox(this.checked)"></th>
								<th scope="col"><input type="checkbox" name="checkALL" value="" onchange="changeCheckBox(this.checked)"></th>
								<th scope="col">Type</th>
								<th scope="col">Target</th>
								<th scope="col">Command</th>
								<th scope="col">Status</th>
								<th scope="col">Actions</th>
							</tr>
						</thead>
						<tbody id="trData" style="border:0px">
							<!-- 
							<tr>
								<td>MME2</td>
								<td>CRTE-PGW-GRP:PGWGRP=601,PGW=104,CAPA=100;</td>
								<td>success</td>
								<td>
									<a href="#" class="btn_s btn_co03">Run</a>
									<a href="#" class="btn_s btn_co03">Retry</a>
									<a href="#" class="btn_s btn_co03">Skip</a>
									<a href="#" class="btn_s btn_co03">View msg</a>
								</td>
							</tr>
							<tr class="on">
								<td>MME2</td>
								<td>CRTE-PGW-GRP:PGWGRP=601,PGW=104,CAPA=100;</td>
								<td>Running...</td>
								<td></td>
							</tr>
							<tr>
								<td>MME2</td>
								<td>CRTE-PGW-GRP:PGWGRP=601,PGW=104,CAPA=100;</td>
								<td><span class="point01">fail</span></td>
								<td>
									<a href="#" class="btn_s btn_co03">Run</a>
									<a href="#" class="btn_s btn_co03">Retry</a>
									<a href="#" class="btn_s btn_co03 on">Skip</a>
									<a href="#" class="btn_s btn_co03">View msg</a>
								</td>
							</tr>
							<tr>
								<td>MME2</td>
								<td>CRTE-PGW-GRP:PGWGRP=601,PGW=104,CAPA=100;</td>
								<td>success</td>
								<td>
									<a href="#" class="btn_s btn_co03 on">Run</a>
									<a href="#" class="btn_s btn_co03 on">Retry</a>
									<a href="#" class="btn_s btn_co03 on">Skip</a>
									<a href="#" class="btn_s btn_co03">View msg</a>
								</td>
							</tr>
							 -->
						</tbody>	
					</table>
				</div><!-- //table_ty -->
				
			</div><!-- //contents -->
		</div><!-- //contents_wrap -->
	</div><!-- //container -->
	
	<footer>
		<nav class="footer_nav">
			<ul>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">이용약관</a></li>
				<li><a href="#">개인정보처리방침</a></li>
				<li><a href="#">Sitemap</a></li>
			</ul>
		</nav><!-- //footer_nav -->
		<address>㈜케이티 대표이사 황창규 경기도 성남시 분당구 불정로 90 (정자동) 사업자등록번호 : 102-81-42945 통신판매업신고 : 2002-경기성남-0047</address>
	</footer>
	
</div><!-- //wrap -->
<div class="popup_mask"></div>

<div class="popup pop_progress" id="pop_view_message">
<!-- <div class="popup pop_view_message" id="pop_view_message">  -->
	<div class="pop_tit">View Message</div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">
			
			<div class="pop_con_tit" id="cmd_base_nm">CRTE-PGW-GRP</div>
			
			<div class="table_form vTop">
				<table>
					<caption>입력 테이블</caption>
					<colgroup>
						<col span="1" style="width:30%;">
						<col span="1" style="width:*;">
					</colgroup>
					<tbody>
						<tr>
							<th><span>Target System</span></th>
							<td><input type="text" name="equip_name" value="YGIN_HSS2" readonly="true"></td>
						</tr>
						<tr>
							<th><span>Command</span></th>
							<td><input type="text" name="create_command_sentence"  value="CRTE-PGW-GRP:PGWGRP=601,PGW=104,CAPA=100;"  readonly="true"></td>
						</tr>
						<tr>
							<th><span>Return Msg</span></th>
							<td>
							<textarea cols="5" rows="10" name="exec_return_message"  readonly="true">400 Bad Request Required information for the request was missing, or the request had syntactical errors, or the request contains a malformed access token or malformed credentials.In the present document, this code is also used as "catch-all" code for client errors.
							</textarea>
							</td>
						</tr>
					</tbody>	
				</table>
			</div><!-- //table_form -->
			
		</div><!-- //scrollbar-inner -->
		
	</div><!-- //pop_con_wrap -->
	<a href="javascript:void(0);" class="pop_close">팝업닫기</a>
</div><!-- //popup -->
<div class="popup" style="width:300px;" id="preload_pop">
	<div class="pop_tit" id="preload_title">실행중</div>
	<div class="pop_con_wrap">
	<table>
		<tbody>
			<tr>
				<td><marquee style="width:200px;">실행중&lt;&lt;&lt;&lt;실행중&lt;&lt;&lt;&lt;실행중&lt;&lt;&lt;&lt;실행중&lt;&lt;&lt;&lt;</marquee></td>
			</tr>
		</tbody>
	</table>
	</div><!-- //pop_con_wrap -->
</div>
<div class="popup" style="width:300px;" id="preload_create_pop">
	<div class="pop_tit" id="preload_title">생성중</div>
	<div class="pop_con_wrap">
	<table>
		<tbody>
			<tr>
				<td><marquee direction="right" style="width:200px;">생성중&gt;&gt;&gt;&gt;생성중&gt;&gt;&gt;&gt;생성중&gt;&gt;&gt;&gt;생성중&gt;&gt;&gt;&gt;</marquee></td>
			</tr>
		</tbody>
	</table>
	</div><!-- //pop_con_wrap -->
</div>


<div class="popup pop_each_action" id="in_create_pop">
	<div class="pop_tit">명령 개별 실행</div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">
			
			<div class="pop_con_tit" id="cmd_title">CRTE_PLTE_INFO</div>
			
			<div class="float_area ty02">
				<div class="group fl">
					<div class="f_box">
						<div class="table_form scrollbar-inner"><!-- 180105 수정 -->
							<table>
								<caption>입력 테이블</caption>
								<colgroup>
									<col span="1" style="width:34%;">
									<col span="1" style="width:*;">
								</colgroup>
								<tbody id="manData" style="border:0px">
									<tr>
										<th><span>ID</span></th>
										<td><input type="text"></td>
									</tr>
									<tr>
										<th><span>APN</span></th>
										<td><input type="text"></td>
									</tr>
									<tr>
										<th><span>APN2</span></th>
										<td><input type="text"></td>
									</tr>
									<tr>
										<th><span>APN3</span></th>
										<td><input type="text"></td>
									</tr>
								</tbody>	
							</table>
						</div><!-- //table_form scrollbar-inner -->
					</div><!-- //f_box -->
					<div class="f_box ">
						<div class="table_form scrollbar-inner"><!-- 180105 수정 -->
							<table>
								<caption>입력 테이블</caption>
								<colgroup>
									<col span="1" style="width:34%;">
									<col span="1" style="width:*;">
								</colgroup>
								<tbody id="optData" style="border:0px">
									<tr>
										<th><span>APNOIREPL</span></th>
										<td><input type="text"></td>
									</tr>
									<tr>
										<th><span>PLTEECC</span></th>
										<td><input type="text"></td>
									</tr>
									<tr>
										<th><span>UUTYPE</span></th>
										<td><input type="text"></td>
									</tr>
									<tr>
										<th><span>MPNAME</span></th>
										<td><input type="text"></td>
									</tr>
									<tr>
										<th><span>MPNAME2</span></th>
										<td><input type="text"></td>
									</tr>
									<tr>
										<th><span>MPNAME3</span></th>
										<td><input type="text"></td>
									</tr>
									<tr>
										<th><span>MPNAME4</span></th>
										<td><input type="text"></td>
									</tr>
								</tbody>	
							</table>
						</div><!-- //table_form scrollbar-inner -->
					</div><!-- //f_box -->
				</div>

				<div class="group fl">
					<div class="f_box">
						<h4 class="title">MMC Help</h4>
						<div class="table_ty02 scrollbar-inner">
							<table>
								<caption>정보 테이블</caption>
								<colgroup>
									<col span="1" style="width:26%;">
									<col span="1" style="width:*;">
								</colgroup>
								<tbody id="helpData" style="border:0px">
									
								</tbody>	
							</table>
						</div><!-- //table_ty02 scrollbar-inner -->
					</div><!-- //f_box -->
				</div>

				<div class="group">
					<div class="f_box bgNo">
						<textarea cols="5" rows="3" id="in_create_command_sentence" readonly>CRTE-PGW-GRP:PGWGRP=601,PGW=104,CAPA=100;</textarea>
					</div><!-- //f_box -->
				</div>
			</div><!-- //float_area -->
			
		</div><!-- //scrollbar-inner -->
		
	<div class="btn_area">
			<a href="javascript:void(0);" onClick="individual_command_create()" class="btn_l btn_co01">생성 완료</a>
			<a href="javascript:void(0);" onClick="individual_run_command()" class="btn_l btn_co01">Run</a>
		</div>
	</div><!-- //pop_con_wrap -->
	<a href="javascript:void(0);" class="pop_close">팝업닫기</a>
</div><!-- //popup -->

</body>
</html>