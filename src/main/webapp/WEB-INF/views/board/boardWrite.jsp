<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content= "text/html; charset=UTF-8">
<!-- BootStrap CDN -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<title>게시글 작성</title>
<script>
function board_write()
{
	document.form.submit();
}
</script>
</head>
<body>
    <h3>게시글 작성</h3>
    <div style="padding : 30px;">
        <form  name=form method="POST" action="/board/post" onsubmit="return false;">
          <div class="form-group">
            <label>제목</label>
            <input type="text" name="subject" class="form-control">
          </div>
          <div class="form-group">
            <label>작성자</label>
            <input type="text" name="writer" class="form-control">
          </div>
          <div class="form-group">
            <label>내용</label>
            <textarea name="content" class="form-control" rows="5"></textarea>
             <textarea name="inputNo" class="form-control" rows="5"></textarea>
          </div>
          <div class="form-group">
            <label>멀티이름값1</label>
            <input type="text" name="inputNo" class="form-control">
          </div>
          <div class="form-group">
            <label>멀티이름값2</label>
            <input type="text" name="inputNo" class="form-control">
          </div>          
          
          <button type="submit" class="btn btn-default" onclick='board_write()'>작성</button>
          <button type="button" class="btn btn-default" onclick='location.href="/board"'>목록</button>
          
          <button id='btn-add-row'>행 추가하기</button>
<button id='btn-delete-row'>행 삭제하기</button>
<hr>

<table id="mytable" border="1" cellspacing="3">
  <tr>
    <th>제목</th>
    <th>일시</th>
  </tr>
  <tbody></tbody>
</table>

<script src="//code.jquery.com/jquery.min.js"></script>
<script>
  $('#btn-add-row').click(function() {
    var time = new Date().toLocaleTimeString();
    $('#mytable > tbody:last').append('<tr><td><input type="text" name="inputNo" class="form-control"> </td><td><input type="text" name="inputNo" class="form-control"> </td><td>' + time + '</td></tr>');
  });
  $('#btn-delete-row').click(function() {
    $('#mytable > tbody:last > tr:last').remove();
  });
</script>
        </form>
    </div>
</body>
</html>
