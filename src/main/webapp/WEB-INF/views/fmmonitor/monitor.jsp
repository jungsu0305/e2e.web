<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<title>KT E2E Infra Orchestrator</title>
<link rel="shortcut icon" href="/resources/images/layout/favicon.ico"
	type="image/x-icon">
<link rel="stylesheet" type="text/css" href="/resources/css/style.css">
<script type="text/javascript" src="/resources/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-ui.js"></script>
<script type="text/javascript"
	src="/resources/js/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="/resources/js/template.js"></script>

<script src="/resources/scripts/draw2d.v6/shifty.js"></script>
<script src="/resources/scripts/draw2d.v6/raphael.js"></script>
<script src="/resources/scripts/draw2d.v6/jquery.autoresize.js"></script>
<script src="/resources/scripts/draw2d.v6/jquery-touch_punch.js"></script>
<script src="/resources/scripts/draw2d.v6/jquery.contextmenu.js"></script>
<script src="/resources/scripts/draw2d.v6/rgbcolor.js"></script>
<script src="/resources/scripts/draw2d.v6/canvg.js"></script>
<script src="/resources/scripts/draw2d.v6/Class.js"></script>
<script src="/resources/scripts/draw2d.v6/json2.js"></script>
<script src="/resources/scripts/draw2d.v6/pathfinding-browser.min.js"></script>
<script src="/resources/scripts/draw2d.v6/draw2d.js"></script>
<script src="/resources/scripts/draw2d.v6/draw2d.js"></script>
<script src="/resources/scripts/editor/Config.js"></script>
<script src="/resources/scripts/editor/Components.js"></script>
<script src="/resources/scripts/editor/Apps.js"></script>
<script src="/resources/scripts/application.js"></script>
<script src="/resources/scripts/utils/jsrender.js"></script>
<script src="/resources/scripts/editor/jquery.ajax-cross-origin.min.js"></script>


<script type="text/javascript">

	var app;

	$(window).on('load', function() {

		//this.window.resizeTo(500, 200);

		//////////////////////////////////////////////////////////////////////////////////////
		//
		// setup designer
		//
		//////////////////////////////////////////////////////////////////////////////////////

		// initialize designer ///////////////////////////////////////////////////////////////
	});

	function editorReload(app, utils, nsdId) {
		try {
			var view = app.view;
			var reader = app.reader;
			$.get('/topologyEditor/data/' + nsdId, function(ret) {
				jsonTotal = JSON.parse(ret);
				//$("#canvas").css("min-height", 400);
				if (jsonTotal.err == 0) {
					view.clear();
					app.data = jsonTotal.data;
					json = JSON.parse(app.data);
					var max = {
						x : 0,
						y : 0
					};
					for (var i in json) {
						var e = json[i];
						if (typeof e.x === 'number' && typeof e.width === 'number') {
							if (max.x === 0 || max.x < (e.x + e.width))
								max.x = e.x + e.width;
							if (max.y === 0 || max.y < (e.y + e.height))
								max.y = e.y + e.height;

						} else if (typeof e.x === 'number') {
							if (max.x === 0 || max.x < (e.x + 50))
								max.x = e.x + 50; //offset, 50 1st				
							if (max.y === 0 || max.y < (e.y + 50))
								max.y = e.y + 50;
						}
					}

					//current window
					var w = view.width();
					var h = view.height();

					utils.canvas.set(max, {
						w : w,
						h : h
					});
					reader.unmarshal(view, jsonTotal.data);

					view.resize(1500, 800);
					view.setZoom(1.5, false);
					view.installEditPolicy(new draw2d.policy.canvas.ReadOnlySelectionPolicy());
				}
			//$("#canvas").css("min-height", 400);
			});

		//fnSeverityCount();
		} catch (e) {
			console.log(e);
		}
	}



	var audio1;
	var audio2;
	var audio3;
	var isAlarmViewReload = true; //알람 가시 ON & OFF
	var isAlarmSoundReload = true; //알람 사운드 ON & OFF
	var isAlarmPlayReload = true; //알람 플레이 ON & OFF
	var isMute = false;
	var isPause = false;

	//초기 동작
	$(document).ready(function() {

		app = new e2e.Application("canvas");
		var nsdId = "${e2eNsdId}";

		editorReload(app, utils, nsdId);

		app.find = function(key, level) {
			//var levels = [ '', '_normal', '_minor', '_major', '_warning', '_critical' ];
			var levels = [ '_critical', '_major', '_minor', '', '_normal'];

			//find node
			var figures = app.view.figures;
			var node = figures.data.filter(function(e) {
				//TODO found with name 1st
				return e.NAME === 'e2e.component.Node'
					&& typeof e.name !== 'undefined'
					&& e.name === key;
			});

			if (node.length === 0) {
				//alert("couldn't found your icon in canvas");			
				return;
			}

			//TODO 1st node only...
			var subfigures = node[0].getChildren();
			var icon = subfigures.data.filter(function(e) {
				return e.NAME === 'e2e.component.Icon' && typeof e.path !== 'undefined';
			});

			var imageKey = key.replace('-', '_');
			//set path
			var newPath = '/resources/images/contents/' + imageKey + levels[level] + '.svg';
			node[0].path = newPath;
			icon[0].setPath(newPath);
		}

		$("#btn_mask").click(function() {
			fnCheckAlarmConfirm(true);
		});

		//unmask
		$("#btn_unmask").click(function() {
			fnCheckAlarmConfirm(false);
		});

		//알람 팝업 on 클릭시
		$("#btn_viewOn").click(function() {
			//console.log("btn_viewOn");
			$("#btn_viewOff").show();
			$("#btn_viewOn").hide();
		});

		//알람 팝업 off 버튼 클릭시
		$("#btn_viewOff").click(function() {
			$("#btn_viewOff").hide();
			$("#btn_viewOn").show();
		});


		//sound 버튼 ON 클릭시
		$("#btn_soundOn").click(function() {
			//console.log("btn_soundOn");
			$("#btn_soundOff").show();
			$("#btn_soundOn").hide();

			isMute = true;
		});

		//sound 버튼 OFF 클릭시
		$("#btn_soundOff").click(function() {
			//console.log("btn_soundOff");
			$("#btn_soundOff").hide();
			$("#btn_soundOn").show();

			isMute = false;
		});


		//restart 버튼 클릭시
		$("#btn_playOn").click(function() {

			$("#btn_playOff").show();
			$("#btn_playOn").hide();

			//isAlarmViewReload = true;
			isPause = true;

			//console.log("btn_playOn >> !" + isAlarmPlayReload);
			fnInitializeAlarm();

		});

		//pause 버튼 클릭시
		$("#btn_playOff").click(function() {
			$("#btn_playOff").hide();
			$("#btn_playOn").show();

			//isAlarmViewReload = false;
			//isAlarmPlayReload = false;
			//console.log("btn_playOff >> !" + isAlarmPlayReload);
			isPause = false;
		});


		//알람 Detail
		/* $(document).on("dblclick", "#alarmList tr", function() {
  			//var param = new Object();
			var key = $(this).attr("id");
			//var alarmId = $(this).attr("alarmId");

			var result =
				$.ajax({
					url : '/e2e/fm/' + key + '/alarmDetail',
					//data : param,
					type : "POST",
					dataType : "html",
					cache : false
				});

			result.done(function(data) {
				alert(data);
				$.modal(data, {onShow: function (dialog) {
					dialog.container.draggable({handle: "#pop_alarmDetail", containment: "body"});
					}
				});
				$('#simplemodal-overlay').css('display','none'); //Overlay Div's display = none
			});

			result.fail(function(xhr, status) {
			});

			param = null;
			result.onreadystatechange = null;
			result.abort = null;
			result= null;
		});		 */

		initWebsocket();
		fnInitializeAlarm(true);
		fnIntervalAlarmSound();
	//fnIntervalAlarmList();
	});


	/**
	 * Ack 설정 함수
	 */
	function fnMaskAlarm() {
		var param = $("#checkAlarmForm").serialize();
		var result = $.ajax({
			url : "/e2e/fm/alarmMask",
			dataType : "json",
			data : param,
			type : 'POST',
			cache : false
		});
		result.done(function(data) {
			//console.log(data);
			if (data == 0) {
				$("#btn_playOff").hide();
				$("#btn_playOn").show();

				isPause = false;
				fnInitializeAlarm(true);
			}
			data = null;
		});
		result.fail(function(xhr, status) {});

		param = null;
		result = null;
	}

	/**
	 * unMask 설정 함수
	 */
	function fnUnmaskAlarm() {
		var param = $("#checkAlarmForm").serialize();
		var result = $.ajax({
			url : "/e2e/fm//alarmUnmask",
			dataType : "json",
			data : param,
			type : 'POST',
			cache : false
		});
		result.done(function(data) {
			if (data == 0) {
				$("#btn_playOff").hide();
				$("#btn_playOn").show();

				isPause = false;
				
				fnInitializeAlarm(true);
			}
			data = null;
		});
		result.fail(function(xhr, status) {});

		param = null;
		result = null;
	}

	/**
	 * mask & unmask  캔슬 함수
	 */
	function fnCancelAlarmConfirm() {
		$("input:checkbox[name='chk']").prop("checked", false);
		$("input:checkbox[name='chk_list']").prop("checked", false);
	}

	function fnCheckAlarmConfirm(isBoolean) {
		if ($("#alarmList").children("tr").length == 0) {
			alert("Please select Alarm(s)");
			//openAlertModal("", "Please select Alarm(s)", null, null);
			return;
		} else {
			if ($("input:checkbox[name='chk']:checked").length <= 0) {
				alert("Please select Alarm(s)");
				//openAlertModal("", "Please select Alarm(s)", null, null);
				return;
			}
		}

		if (isBoolean) {
			fnMaskAlarm();
			fnCancelAlarmConfirm();
		//openConfirmModal("Mask", "", fnMaskAlarm, fnCancelAlarmConfirm, "OK", "CANCEL");
		} else {
			fnUnmaskAlarm();
			fnCancelAlarmConfirm();
		//openConfirmModal("UnMask", "", fnUnmaskAlarm, fnCancelAlarmConfirm, "OK", "CANCEL");
		}
	}

	// Alarm 선택시
	function selectAlarm(objId, objInstanceName, timeStamp, notiType, probableCause, faultDetails, isRootCause, alarmRaisedTime, alarmClearedTime, severity, eventType) {
		$("#alarm_detail_timestamp").text(timeStamp);
		$("#alarm_detail_instance_name").text(objInstanceName);
		$("#alarm_detail_noti_type").text(notiType);
		$("#alarm_detail_probable_cause").text(probableCause);
		$("#alarm_detail_fault_detail").text(faultDetails);
		$("#alarm_detail_is_root_cause").text(isRootCause);
		$("#alarm_detail_creared_time").text(alarmClearedTime);
		$("#alarm_detail_raised_time").text(alarmRaisedTime);

		if (severity == 0)
			$("#alarm_detail_perceived_severity").text('Critical');
		else if (severity == 1)
			$("#alarm_detail_perceived_severity").text('Major');
		else if (severity == 2)
			$("#alarm_detail_perceived_severity").text('Minor');
		else if (severity == 3)
			$("#alarm_detail_perceived_severity").text('Warning');
		else if (severity == 4)
			$("#alarm_detail_perceived_severity").text('Indeterminate');
		else if (severity == 5)
			$("#alarm_detail_perceived_severity").text('Cleared');

		if (eventType == 0)
			$("#alarm_detail_event_type").text('COMMUNICATIONS_ALARM');
		else if (eventType == 1)
			$("#alarm_detail_event_type").text('PROCESSING_ERROR_ALARM');
		else if (eventType == 2)
			$("#alarm_detail_event_type").text('ENVIRONMENTAL_ALARM');
		else if (eventType == 3)
			$("#alarm_detail_event_type").text('QOS_ALARM');
		else if (eventType == 4)
			$("#alarm_detail_event_type").text('EQUIPMENT_ALARM');
		else if (eventType == 5)
			$("#alarm_detail_event_type").text('EVENT');


		pop_open('.pop_alarm_detail');
	}

	// 체크박스 전체선택/해제 기능
	function selectAll() {
		if ($("#chk_list").is(":checked")) {
			$("input[name=chk]").prop("checked", true);
		} else {
			$("input[name=chk]").prop("checked", false);
		}
	}

	/**
	 * 알람 조회
	 */
	function fnInitializeAlarm(searchCheckFlag) {
		/*  		if(!isAlarmPlayReload){
						return;
				} */

		$("input:checkbox[name='chk_list']").prop("checked", false);

		var param;
		// searchCheckFlag - search 한 이력 체크 후 알맞는 검색 정보를 Setting 해줌
		if (searchCheckFlag) {
			// 직전에 search한 내역을 그대로 넣어준다. (search 한 내역이 없다면 빈 문자열로 setting)
			// 이유 : refresh 되기 직전에 search 박스에 text 넣으면, search 버튼을 누르지 않더라도 해당 text로 검색이되며 refresh 되기 때문
			var searchStatus = $("input[name=searchStatus]").attr("value")
			// 최초 호출 시에는 해당 값을 읽어 올 수 없으므로 빈칸으로 세팅
			if (typeof searchStatus == "undefined") {
				searchStatus = "";
			}
			param = $("#checkAlarmForm :input[name!=search]").serialize();
			param += '&search=' + searchStatus;
			searchStatus = null;
		} else {
			// 검색 버튼을 클릭할 경우에는 직전에 선택했던 페이지를 제외하고 1로 setting 해준다
			param = $("#checkAlarmForm :input[name!=page]").serialize();
			param += '&page=1';
			$("#lastAlarmTime").val("");
		}

		param += '&lastAlarmTime=' + $("#lastAlarmTime").val();

		var result = $.ajax({
			url : "/e2e/fm/alarmList.ajax",
			dataType : "html",
			data : null,
			data : param,
			type : "POST",
			cache : false
		});

		result.done(function(data) {
			if (data != null) {
				//console.log(data);
				$("#alarmList").html(data);
				fnSeverityCount();
				fnPlayAudio();
				getInstanceAlarmLevel();
			}
		});

		result.fail(function(xhr, status) {
			console.log(status);
		});

		param = null;
		result = null;
	}
	
	function getInstanceAlarmLevel() {
		var result = $.ajax({
			url : "/e2e/fm/getInstanceAlarmLevel.ajax",
			dataType : "json",
			data : null,
			type : 'POST',
			cache : false
		});
		result.done(function(data) {
			//console.log(data);
			if (data != null) {
				fnInstanceLevel(data);
			}
			data = null;
		});
		result.fail(function(xhr, status) {});

		param = null;
		result = null;
	}
	
	function fnInstanceLevel(data) {
		for(var i = 0; i < data.length; i++)
		{
			if(data[i].level == 0 || data[i].level == 1 || data[i].level == 2) 
			{
				console.log(data[i].vnfname+ "   " + data[i].level);
				this.app.find(data[i].vnfname, data[i].level);
			}
			else {
				this.app.find(data[i].vnfname, 3);
			}
		}
	}

	/**
	 * Severity Count 조회
	 */
	function fnSeverityCount() {
		var critical_count = $("input[name=critical_count]").attr("value");
		var major_count = $("input[name=major_count]").attr("value");
		var minor_count = $("input[name=minor_count]").attr("value");
		var indeterminate_count = $("input[name=indeterminate_count]").attr("value");
		var cleared_count = $("input[name=cleared_count]").attr("value");
		var warning_count = $("input[name=warning_count]").attr("value");

		critical_count = validNumber2(critical_count);
		major_count = validNumber2(major_count);
		minor_count = validNumber2(minor_count);
		indeterminate_count = validNumber2(indeterminate_count);
		cleared_count = validNumber2(cleared_count);
		warning_count = validNumber2(warning_count);

		$("#critical_count").text(critical_count);
		$("#major_count").text(major_count);
		$("#minor_count").text(minor_count);
		$("#warning_count").text(warning_count);
		$("#cleared_count").text(cleared_count);
		$("#indeterminate_count").text(indeterminate_count);

		//알람 총 카운터
		var alarm_totalcount = $("input[name=alarm_totalcount]").attr("value");
		$("#alarm_totalcount").text(alarm_totalcount);

		//마지막 페이지
		var lastPage = $("input[name=lastPage]").attr("value");
		if (lastPage == "0")
			lastPage = 1;
		$("#last_page").text(lastPage);

		//현재 페이지
		var realPage = $("input[name=realPage]").attr("value");
		$("input[name=page]").val(realPage);
		$("li[name=page]").html(realPage);

/* 		setTimeout(function() {
			if ((critical_count + major_count + minor_count) == 0) {
				this.app.find("${e2eAlarmNodeKey}", 0);
			} else {
				this.app.find("${e2eAlarmNodeKey}", 3);
			}
			critical_count = null;
			major_count = null;
			minor_count = null;
			information_count = null;
			alarm_totalcount = null;
		}, 1000); */


		lastPage = null;
		realPage = null;
	}

	function validNumber2(num) {
		if (typeof num == "undefined") {
			return 0;
		} else if (num == null) {
			return 0;
		}
		return num;
	}

	function setLastAlarmTime(timestamp) {
		$("#lastAlarmTime").val(timestamp);
		timestamp = null;
	}

	//alarm 전부 제거
	function fnAlarmListAllClose() {
		//console.log("fnAlarmListAllClose");
		$.modal.close();
		$("#alarmPopupAlive").val("N");
		$("#alarm_list").empty(); //calarm_modal.close 위로 가면 안지워짐...
	}

	function fnPlayAudio() {
		var severityName = $("input[name=alarmLevel]").attr("value");
		//console.log(file_flag);
		//console.log(code);

		if (isMute) return;
		if (isPause) return;

		try {
			//audio1 = $("#system_" + system_name).get(0);
			audio2 = $("#level_" + severityName).get(0);
			//audio3 = $("#alarm_" + code).get(0);

			if ((audio2.error == null) && (audio2 != undefined)) { //에러 아니면
				audio2.play();
			}
		} catch (e) {
			//if (window.console && console.log("Error:" + e));
		} finally {
			severityName = null;
		}
	}

	var intervalAlarmSound; //알람
	function fnIntervalAlarmSound() {
		intervalAlarmSound = setInterval(function() {
			//if (isAlarmSoundReload) fnInitializeSound();
			//fnPlayAudio(perceivedSeverity);
			if (isAlarmSoundReload) fnPlayAudio();
			clearInterval(intervalAlarmSound);
			intervalAlarmSound = null;
			fnIntervalAlarmSound();
		}, 3 * 1000);
	}

	var intervalAlarmList; //알람
	function fnIntervalAlarmList() {
		intervalAlarmList = setInterval(function() {
			//if (isAlarmSoundReload) fnInitializeSound();
			//fnPlayAudio(perceivedSeverity);
			fnInitializeAlarm(true)
			clearInterval(intervalAlarmList);
			intervalAlarmList = null;
			fnIntervalAlarmList();
		}, 5 * 1000);
	}

	/**
	 * 페이지 이동
	 */
	function fnPageMove(value) {
		var page = $("input[name=realPage]").attr("value");
		var lastPage = $("input[name=lastPage]").attr("value");
		page = Number(page);
		lastPage = Number(lastPage);
		//console.log(page +"/"+lastPage);
		if (value == "first") {
			$("input[name=page]").val(1);
			$("li[name=page]").html(1);
		} else if (value == "before") {
			if (page > 1) $("input[name=page]").val(page - 1);
			if (page > 1) $("input[li=page]").val(page - 1);
		} else if (value == "next") {
			if (page < lastPage) $("input[name=page]").val(page + 1);
			if (page < lastPage) $("li[name=page]").html(page + 1);
		} else if (value == "last") {
			$("input[name=page]").val(lastPage);
			$("li[name=page]").html(lastPage);
		}
		fnInitializeAlarm(true);

		value = null;
		page = null;
		lastPage = null;
	}


	//####################################################
	//	Websocket 함수
	//####################################################

	//init Websocket
	var disconnWebsocket;
	var websocket = null;
	function initWebsocket() {
		var wsUri = "ws://" + "${e2eWebsocketAddress}" + "/websocket";
		//var wsUri = "ws://localhost/websocket";

		// 재접속 요청시 중복 연결을 방지하기 위해 기존 Websocket close
		if (websocket != null) {
			websocket.close();
		}

		websocket = new WebSocket(wsUri);
		websocket.onopen = function(evt) {
			onOpen(evt)
		};
		websocket.onmessage = function(evt) {
			onMessage(evt)
		};
		websocket.onerror = function(evt) {
			onError(evt)
		};
		websocket.onclose = function(evt) {
			onClose(evt);
		};

		function onOpen(evt) {
			disconnWebsocket = null;
			writeToSocketStatus("websocket connected!");
		}
		function onMessage(evt) {
			writeToMonitor(evt.data);
		}
		function onError(evt) {
			writeToSocketStatus('ERROR: ' + evt.data);
		}
		function onClose(evt) {
		}
		function doSend(message) {
			writeToSocketStatus("Message Sent: " + message);
			websocket.send(message);
		}
		function writeToSocketStatus(message) {
			console.log(" >> " + message);
		}
		function writeToMonitor(message) {
			var obj = JSON.parse(message);
			if (obj.type == "fault") {
				$("#btn_playOff").hide();
				$("#btn_playOn").show();

				isPause = false;
				fnInitializeAlarm(true);
			}
		}
	}
</script>


</head>

<body class="wpop">


	<div class="win_pop pop_inquiry_conError">
		<!-- 180119 수정 -->
		<div class="pop_tit">
			<em id="title"><em> Alarm &amp; Event</em>
		</div>
		<div class="pop_con_wrap">

			<div class="inquiry_result">
				<div class="area" style="background: #4c4c4e">
					<strong class="tit"><em id="sub_title"
						style="color: white;">${e2eNsdName}</em></strong>
					<div id="canvas"
						style="position: relative; min-height: 400px; background: #f4f4f4 url(/resources/images/layout/cont_bg.png) repeat;">
					</div>
				</div>
			</div>
			<!-- //inquiry_result -->

			<form id="checkAlarmForm" name="checkAlarmForm" method="post">
				<div class="operating_area">
					<div class="left">
						<span class="op_total_count"> <span class="count">
								Total Count <strong><em id="alarm_totalcount">0</em></strong>
						</span> <span class="op_value"> 
									<i class="ico_value a"><em id="cleared_count">0</em></i> 
									<i class="ico_value b"><em id="warning_count">0</em></i> 
									<i class="ico_value c"><em id="minor_count">0</em></i> 
									<i class="ico_value d"><em id="major_count">0</em></i> 
									<i class="ico_value e"><em id="critical_count">0</em></i> 
									<i class="ico_value f"><em id="indeterminate_count">0</em></i>
						</span>
						</span>
					</div>
					<div class="page_nav">
						<a href="javascript:fnPageMove('first');" class="first">처음
							페이지</a> <a href="javascript:fnPageMove('before');" class="prev">이전
							페이지</a>
						<ul>
							<input type="hidden" name="page" value="1" readonly />
							<li class="current" name="page">1</li>
							<li><small>/</small><span id="last_page">1</span></li>
						</ul>
						<a href="javascript:fnPageMove('next');" class="next">다음 페이지</a> <a
							href="javascript:fnPageMove('last');" class="last">마지막 페이지</a>
					</div>
					<div class="right">
						<span class="op_mask_area"> <a href="#" id="btn_mask"
							title="Mask">Mask</a> <a href="#" id="btn_unmask" title="Mask">Unmask</a>
						</span> <span class="op_play_area"> <!-- <a href="#" id="btn_viewOn" class="btn_eye">off</a>
					<a href="#" id="btn_viewOff" class="btn_eye off" style="display:none;">off</a> -->
							<a href="#" id="btn_soundOn" class="btn_sound">sound</a> <a
							href="#" id="btn_soundOff" class="btn_sound off"
							style="display: none;">sound</a> <a href="#" id="btn_playOn"
							class="btn_pause">pause</a> <a href="#" id="btn_playOff"
							class="btn_pause off" style="display: none;">pause</a>
						</span>
					</div>
				</div>
				<!-- //operating_area -->
				<div class="table_ty scroll_equipment02 scrollbar-inner">
					<table>
						<caption>목록 테이블</caption>
						<colgroup>
							<col span="1" style="width: 5%;">
							<col span="1" style="width: 8%;">
							<col span="1" style="width: 8%;">
							<col span="1" style="width: 8%;">
							<col span="1" style="width: 15%;">
							<col span="1" style="width: 10%;">
							<col span="1" style="width: 12%;">
							<col span="1" style="width: 30%;">
						</colgroup>
						<thead>
							<tr>
								<th scope="col"><input type="checkbox" id="chk_list"
									name="chk_list" onclick="selectAll()"></th>
								<th scope="col">Mask</th>
								<th scope="col">Cleared</th>
								<th scope="col">Type</th>
								<th scope="col">Date/Time</th>
								<th scope="col">Instance Name</th>
								<th scope="col">Event</th>
								<th scope="col">Event Message</th>
							</tr>
						</thead>
						<tbody id="alarmList">
						</tbody>
					</table>
				</div>
				<!-- //table_ty -->
			</form>
			<div class="btn_area">
				<a href="javascript:self.close();" class="btn_l btn_co01">확인</a>
				<!-- <a href="javascript:pop_close('.popup');" class="btn_l btn_co01">확인</a> -->
			</div>
		</div>
		<!-- //pop_con_wrap -->
	</div>
	<!-- //popup -->


	<audio id="level_Critical" src="/resources/audio/Critical.wav">HTML5
		audio not supported
	</audio>
	<audio id="level_Major" src="/resources/audio/Major.wav">HTML5
		audio not supported
	</audio>
	<audio id="level_Minor" src="/resources/audio/Minor.wav">HTML5
		audio not supported
	</audio>
	<audio id="level_Normal" src="/resources/audio/Normal.wav">HTML5
		audio not supported
	</audio>

	<input type="hidden" id="lastAlarmTime" name="lastAlarmTime" value="" />

	<div class="popup_mask"></div>
	<div class="popup pop_alarm_detail">
		<div class="pop_tit">Alarm Detail</div>
		<div class="pop_con_wrap">
			<div class="scrollbar-inner">
				<div class="table_form vTop ty02">
					<table>
						<caption>내역 테이블</caption>
						<colgroup>
							<col span="1" style="width: 30%;">
							<col span="1" style="width: *;">
						</colgroup>
						<tbody>
							<tr>
								<th><span>Date / Time</span></th>
								<td><em id="alarm_detail_timestamp"></em></td>
							</tr>
							<tr>
								<th><span>Instance name</span></th>
								<td><em id="alarm_detail_instance_name"></em></td>
							</tr>
							<tr>
								<th><span>Perceived Severity</span></th>
								<td><em id="alarm_detail_perceived_severity"></em></td>
							</tr>
							<tr>
								<th><span>Noti Type</span></th>
								<td><em id="alarm_detail_noti_type"></em></td>
							</tr>
							<tr>
								<th><span>Event</span></th>
								<td><em id="alarm_detail_event_type"></em></td>
							</tr>
							<tr>
								<th><span>Is Root Cause</span></th>
								<td><em id="alarm_detail_is_root_cause"></em></td>
							</tr>
							<tr>
								<th><span>Probable cause</span></th>
								<td><em id="alarm_detail_probable_cause"></em></td>
							</tr>
							<tr>
								<th><span>Fault Details</span></th>
								<td><em id="alarm_detail_fault_detail"></em></td>
							</tr>
							<tr>
								<th><span>Raised Time</span></th>
								<td><em id="alarm_detail_raised_time"></em></td>
							</tr>
							<tr>
								<th><span>Cleared Time</span></th>
								<td><em id="alarm_detail_creared_time"></em></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //table_form -->
			</div>
			<!-- //scrollbar-inner -->
			<div class="btn_area">
				<a href="javascript:pop_close('.popup');" class="btn_l btn_co01">확인</a>
			</div>
		</div>
		<!-- //pop_con_wrap -->
		<a href="javascript:void(0);" class="pop_close">팝업닫기</a>
</body>
</html>