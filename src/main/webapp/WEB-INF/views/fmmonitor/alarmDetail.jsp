<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!--alarm detail popup-->
<div class="popup_mask"></div>

<div class="popup pop_alarm_detail">
	<div class="pop_tit">Alarm Detail </div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">
			
			<div class="table_form vTop ty02">
				<table>
					<caption>내역 테이블</caption>
					<colgroup>
						<col span="1" style="width:30%;">
						<col span="1" style="width:*;">
					</colgroup>
					<tbody>
						<tr>
							<th><span>Date / Time</span></th>
							<td>2017-09-25 11:26:28</td>
						</tr>
						<tr>
							<th><span>Instance name</span></th>
							<td>vSCEF</td>
						</tr>
						<tr>
							<th><span>Instance type</span></th>
							<td>NFVO</td>
						</tr>
						<tr>
							<th><span>Event</span></th>
							<td>FVO Notification</td>
						</tr>
						<tr>
							<th><span>Probable cause</span></th>
							<td>-</td>
						</tr>
						<tr>
							<th><span>Specific problem</span></th>
							<td>400 Bad Request Required information for the request was missing,or the request had syntactical errors, or the request contains a malformed access token or.</td>
						</tr>
					</tbody>
				</table>
			</div><!-- //table_form -->
			
		</div><!-- //scrollbar-inner -->
		<div class="btn_area">
			<a href="javascript:pop_close('.popup');" class="btn_l btn_co01">확인</a>
		</div>
	</div><!-- //pop_con_wrap -->
	<a href="javascript:void(0);" class="pop_close">팝업닫기</a>
</div><!-- //popup -->