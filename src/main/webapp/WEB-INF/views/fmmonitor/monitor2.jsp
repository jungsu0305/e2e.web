<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<title>KT E2E Infra Orchestrator</title>
<link rel="shortcut icon" href="/resources/images/layout/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="/resources/css/style.css">
<script type="text/javascript" src="/resources/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="/resources/js/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="/resources/js/template.js"></script>
</head>

<body>
<script>
window.open("/e2e/fm/monitor/popup", "Fault Monitoring", 'width=1080,height=925', '_blank' );
setTimeout(function(){top.window.opener = top;top.window.open('','_parent','');top.window.close();});
</script>

</body>
</html>