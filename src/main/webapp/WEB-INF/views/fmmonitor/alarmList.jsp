<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${paging.lists}" var="i" varStatus="status">
	<tr id="${i.fmNotificationId}/${i.alarmId}" style="cursor:pointer;"
	ondblclick="selectAlarm('${i.objId}', '${i.objInstanceName}', '${i.timeStamp}', '${i.notiType}', '${i.probableCause}', '${i.faultDetails}', '${i.isRootCause}', '${i.alarmRaisedTime}', '${i.alarmClearedTime}', '${i.perceivedSeverity}', '${i.eventType}')">
		<td ><input type="checkbox" id="check_${i.fmNotificationId}_${i.alarmId}" name="chk" value="${i.fmNotificationId}_${i.alarmId}"><label for="check_${i.fmNotificationId}_${i.alarmId}"></label></td>
		<td>
			<c:if test="${i.maskFlag == 'M'}"><i class="ico_mask"></i></c:if>
		</td>
		<td>
			<c:if test="${i.alarmClearedTime ne null}"><i class="ico_mask"></i></c:if>
		</td>
		<td id="tr_severity_name" ><i class="
       		<c:if test="${i.perceivedSeverity == 0}">ico_value e</c:if>
       		<c:if test="${i.perceivedSeverity == 1}">ico_value d</c:if>
       		<c:if test="${i.perceivedSeverity == 2}">ico_value c</c:if>
       		<c:if test="${i.perceivedSeverity == 3}">ico_value b</c:if>
       		<c:if test="${i.perceivedSeverity == 4}">ico_value f</c:if>
       		<c:if test="${i.perceivedSeverity == 5}">ico_value a</c:if>
       		"></i>
       		<c:if test="${i.perceivedSeverity == 0}">Critical</c:if>
       		<c:if test="${i.perceivedSeverity == 1}">Major</c:if>
       		<c:if test="${i.perceivedSeverity == 2}">Minor</c:if>
       		<c:if test="${i.perceivedSeverity == 3}">Warning</c:if>
       		<c:if test="${i.perceivedSeverity == 4}">Indeterminate</c:if>
       		<c:if test="${i.perceivedSeverity == 5}">Cleared</c:if>
       		</td>
		<td>${i.timeStamp}</td>
		<%-- <td>${e2eAlarmNodeKey}</td> --%>
		<td>${i.objInstanceName}</td>
		<td>${i.notiType}</td>
		<td>${i.faultDetails}</td>
	</tr>
</c:forEach>

<input type="hidden" name="searchStatus" value="${searchStatus}" />
<input type="hidden" name="listSize" value="${fn:length(paging.lists)}" />
<input type="hidden" name="lastPage" value="${lastPage}" />
<input type="hidden" name="realPage" value="${paging.page}" />
<input type="hidden" name="alarmLevel" value="${alarmLevel}" />
<input type="hidden" name="alarm_totalcount" value="<fmt:formatNumber value="${paging.totalCount}" pattern="#,###"/>" />

<!-- 알람 Count 조회 저장 -->
<c:forEach items="${severityCount}" var="i" varStatus="status">
	<input type="hidden" name="${i.severityname}_count" value="${i.cnt}" />
</c:forEach>

<script type="text/javascript">
	$(document).ready(function() {
		setLastAlarmTime("${lastAlarmTime}");
	});
</script>