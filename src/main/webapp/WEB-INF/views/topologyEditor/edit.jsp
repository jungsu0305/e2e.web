<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta name="format-detection" content="telephone=no">
<title>KT E2E Infra Orchestrator</title>
<link rel="stylesheet" type="text/css" href="/css/style.css">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<!-- 
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.js"></script>
 -->
<script type="text/javascript" src="/scripts/utils/jquery-1.12.4.js"></script>
 
<script type="text/javascript" src="/scripts/utils/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="/scripts/utils/template.js"></script>
<script src="/scripts/utils/jquery-ui-1.8.23.custom.min.js"></script>
<!-- 
<script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 -->
<script src="/scripts/utils/jquery.browser.js"></script>
<!-- 
<script src="/scripts/utils/jquery.layout.js"></script>
 -->
<script src="/scripts/utils/less-1.7.5.min.js"></script>
 
<script src="/scripts/draw2d.v6/shifty.js"></script>
<script src="/scripts/draw2d.v6/raphael.js"></script>
<script src="/scripts/draw2d.v6/jquery.autoresize.js"></script>
<script src="/scripts/draw2d.v6/jquery-touch_punch.js"></script>
<script src="/scripts/draw2d.v6/jquery.contextmenu.js"></script>
<script src="/scripts/draw2d.v6/rgbcolor.js"></script>
<script src="/scripts/draw2d.v6/canvg.js"></script>
<script src="/scripts/draw2d.v6/Class.js"></script>
<script src="/scripts/draw2d.v6/json2.js"></script>
<script src="/scripts/draw2d.v6/pathfinding-browser.min.js"></script>
<script src="/scripts/draw2d.v6/draw2d.js"></script>

<script src="/scripts/editor/Config.js"></script>
<script src="/scripts/editor/Components.js"></script>
<script src="/scripts/editor/Apps.js"></script>
<script src="/scripts/application.js"></script>

<script src="/scripts/utils/jsrender.js"></script>
<script id="component-template" type="text/x-jsrender">
<li class="component" data-id="{{:name}}" data-type="{{:type}}" data-path="{{:path}}"><a href="#"><i><img src="{{:path}}" alt=""></i>{{:name}}</a></li>
</script>

<script type="text/javascript">
var isTest = "${isTest}";
if(isTest) alert('TEST MODE');

var app;
var nsdType = '${type}';
var isNew = nsdType === 'new'? true:false;
var componentTypes = conf.componentTypes;

//$(window).load(function(){
$(window).on('load', function(){
	
	if(nsdType === '') nsdType = '5g';

	//////////////////////////////////////////////////////////////////////////////////////
	//
	// setup designer
	//
	//////////////////////////////////////////////////////////////////////////////////////
	
	// initialize designer ///////////////////////////////////////////////////////////////
	app = new e2e.Application("canvas");
	var view = app.view;

	// 좌측 아이콘 그리기 (4g, 5g, data1)
	app.loadingComponents = function() {
// 	console.log(nsdType);
		$.get("/topologyEditor/component/list/"+nsdType, function(dataFull) {

			var dataObj = JSON.parse(dataFull);
// 	console.log(dataObj);
			for(var idx in dataObj) {
// 				console.log(componentTypes[idx]);
			//$.each(dataObj, function(idx, data) {
				var data = dataObj[idx];

				$.each(data, function(i,e) { /*console.log('component:',e);*/ app.components.push(e); });
				console.log(data)
				var key1 = "#" + componentTypes[idx] + "_components ul"; 

				var key2 = componentTypes[idx] + "_components li";

				$(key1).html($.templates("#component-template").render(data));
				// ul에 li방식으로 넣기 (가져오는 데이터는 TestStorage에서 임시로 만들어줌)
				app.loadingComponent(key2);
			//});
			}
			bindComponentClickEvent();
		});
	}
	
	// adding left,right arrow button  //////////////////////////////////////////////////
	app.setArrowButton = function() {//alert(isNew);
		if(!isNew) {
			
			var btn, x, y;
			var opt = 23;  // half height of icon
			if(nsdType == "4g") {
				btn = app.getArrowButton("prev", "/topologyEditor/5g");
				x = 100 - opt;
			} else if (nsdType == "5g") {
				btn = app.getArrowButton("next", "/topologyEditor/4g");
				x = view.getWidth() - (100 + opt);
			} 
// 			else if (nsdType == "test") {
// 				btn = app.getArrowButton("next", "/topologyEditor/4g");
// 				x = view.getWidth() - (100 + opt);
// 			}

			
			y = (view.getHeight()/2) - 35;
			
			view.add(btn, x, y);
			
			if(nsdType == "4g") {
				app.prevBtn = btn;
			} else if (nsdType == "5g") {
				app.nextBtn = btn;
			}
			
			//console.log('btn(',x,y,')');

		} else {
// 			var prev_btn = app.getArrowButton("prev", "/topologyEditor/4g");
// 			view.add(prev_btn, 100 - opt, (view.getHeight()/2) - opt);
// 			app.prevBtn = prev_btn;
		}	
	}
	
	
	//setting connection style ////////////////////////////////////////////////////////////
	app.lazyLoading = function() {
		setTimeout(function() {
			view.installEditPolicy(new draw2d.policy.canvas.CoronaDecorationPolicy());
			console.info('loading policy for port disappear');
		},200);
	}
	
	app.setSize = function(json) {
		
		//check node max 
		json = JSON.parse(json);
		var max = {x:0,y:0};
		for(var i in json) {
			var e = json[i];			
			if(typeof e.x === 'number' && typeof e.width === 'number') {
				if(max.x === 0 || max.x < (e.x + e.width))  max.x = e.x + e.width;				
				if(max.y === 0 || max.y < (e.y + e.height)) max.y = e.y + e.height;
				
			} else if(typeof e.x === 'number') {
				if(max.x === 0 || max.x < (e.x + 50)) max.x = e.x + 50;//offset, 50 1st				
				if(max.y === 0 || max.y < (e.y + 50)) max.y = e.y + 50;
			}
		}
		
		//current window
		var w = view.width();
		var h = view.height();
		
		utils.canvas.set(max, {w:w,h:h});
		
	}
	
	app.loadData= function(dataKey, isRestore) {
		var reader = app.reader;
		
		//for restore after destroy
		if(typeof isRestore !== 'undefined' && isRestore) {
			reader.unmarshal(view, app.data);
			return false;
		}
		
		$.get('/topologyEditor/data/' + dataKey, function(ret) {
// 			console.log("★★★");
			//console.log(ret.data);
			if(ret.err == 0) {
				app.data = ret.data;
				app.setSize(app.data);
				reader.unmarshal(view, ret.data);
			}
		});
	}
	
	app.reloadCanvas = function(dataKey) {
		view.clear();
		app.setArrowButton();
		app.loadData(dataKey);
		app.lazyLoading();//for default display disable
	};
	
	app.saveData = function() {
		var convertor = app.convertor;
		var store = {};
		var writer = new draw2d.io.json.Writer();
		writer.marshal(view, function(json) {
			var jv = convertor.json(json);
			store = JSON.stringify(jv);
			console.log(store);
			/**/
			//url2 createOrUpdate
			var url;
			if(isNew) {
				/*
				var newName = $(".title input").val();
				console.log(newName);
				if(newName == null || newName.length == 0 || newName === '') {
					alert("input new title");
					return false;
				}
				*/
				url = '/topologyEditor/create/' + nsdType;
			} else {
				url = '/topologyEditor/update/' + nsdType;
			}
			console.log('url:',url);
			
			//POST or LOGGING for DEV
			if(isTest) {
				alert('isTest::saved');
				console.log(store);
				return false;
			}
			
			$.post(url, {data:store}, function(ret) {
				alert('saved:' + ret);
			});
		});
	}
	
	app.repositionButtons = function() {
		//btn repositioning
		if(app.prevBtn != null) view.remove(app.prevBtn);
		if(app.nextBtn != null) view.remove(app.nextBtn);
		app.setArrowButton();
	}
	
	app.zoom = function(v) {
		//app.resize(view.width()*v, view.height()*v);
		if(v === 1.0) this.view.setZoom(1.0, true);
		else          this.view.setZoom(this.view.getZoom()*v, true);
	},
	
	//this is dependent on html
	app.find = function(key,level) {
		var levels = ['','_normal','_minor','_major','_warning','_critical'];	
		
		//find node
		var figures = app.view.figures;
		var node = figures.data.filter(function(e) {
			//TODO found with name 1st
			return e.NAME === 'e2e.component.Node'
				&& typeof e.name !== 'undefined' 
				&& e.name === key;			
		});
		
		if(node.length === 0) {
			alert("couldn't found your icon in canvas");			
			return;
		}
		
		//TODO 1st node only...
		var subfigures = node[0].getChildren();
		var icon = subfigures.data.filter(function(e) {
			return e.NAME === 'e2e.component.Icon' && typeof e.path !== 'undefined';
		});
		//console.log("★★★ : " + key);
		//set path
		var newPath = '/images/contents/' + key + levels[level] + '.svg';
		node[0].path = newPath;
		icon[0].setPath(newPath);
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////

	
	
	////////////////////////////////////////////////////////////////
	//
	// Application Loading
	//
	////////////////////////////////////////////////////////////////
	if(nsdType != 'test') {
	 	app.setArrowButton();
	}

	app.loadingComponents();
	
	if(!isNew) app.loadData(nsdType);
	
	app.lazyLoading();
	
	//last settings //////////////////////////////////
	//app.setPortDisappearPolicy();
	
	/////////////////////////////////////////////////////
	// utils
	/////////////////////
	// 팝업 Config
	utils.vnfConfig = {};
	utils.vnfConfig.load = function(node) {
// 		console.log(node);
//alert(utils.getStorage());
		var networkService = $("#"+utils.getStorage()).data('networkService');
		var componentList  = $("#"+utils.getStorage()).data('componentList');
		var basekey  = "." + utils.getEditableContents();//conf.contents.pclass;
		//console.log(componentList);
		componentList.some(function(e) {
			//TODO, 1st 'name'
			if(e.serverType === node.name) {
				//console.log(e);
				$("#"+utils.getStorage()).data('current-active',e); //store into web
				
				$(basekey + " .vnfConfig dl dd").html(e.serverType);		//serverType
				$(basekey + " .vnfConfig ul li:first input").val(e.name);	//name //TODO temporary
				$(basekey + " .vnfConfig ul li:nth-child(2) input").val(e.vnfpkg); //VNF Package
				$(basekey + " .vnfConfig ul li:nth-child(3) input").val(e.oapip);//OAP IP
				$(basekey + " .vnfConfig ul li:last select").val(e.scale); //scale option!
				
				if(typeof e.apn !== 'undefined') $(".sliceConfig ul li:first input").val(e.apn);
				if(typeof e.ippool !== 'undefined') $(".sliceConfig ul li:last input").val(e.ippool);
				
				$(basekey).dialog('open');
				return true;
			}
		});
	}
	
	utils.vnfConfig.update = function() {
		
		var basekey = "." + utils.getEditableContents();
		
		var currns= $("." + utils.getStaticContents()).data('networkService');
		var curr  = $("#" + utils.getStorage()).data('current-active');
		var next = {
			name :			$(basekey + " .vnfConfig ul li:first input").val(),
			vnfpkg :		$(basekey + " .vnfConfig ul li:nth-child(2) input").val(), 
			oapip :			$(basekey + " .vnfConfig ul li:nth-child(3) input").val(), 
			scale :			$(basekey + " .vnfConfig ul li:last select option:selected").val(),
		}
		
		var nextns = {
			apn: $(".sliceConfig ul li:first input").val(),
			ippool:$(".sliceConfig ul li:last input").val(),
		}
		
		var isChanged = false;
		if(curr.name !== next.name) { 
			curr.name = next.name; isChanged = true;
		}
		if(curr.vnfpkg !== next.vnfpkg) {
			curr.vnfpkg = next.vnfpkg; isChanged = true;
		}
		if(curr.oapip !== next.oapip) {
			curr.oapip = next.oapip; isChanged = true;
		}
		if(curr.scale !== next.scale) {
			curr.scale = next.scale; isChanged = true;
		}
		if(currns.apn !== nextns.apn) {
			curr.apn = nextns.apn; isChanged = true;
		}
		if(currns.ippool !== nextns.ippool) {
			curr.ippool = nextns.ippool; isChanged = true;
		}

		if(isChanged) {
			
			var componentList = $("#"+utils.getStorage()).data('componentList');
			console.log('beforeComponentList:',componentList);
			for(var i in componentList) {
				if(componentList[i].serverType == curr.serverType) {
					console.log('found index:',i);
					componentList[i] = curr;					
				}
			}
			console.log('afterComponentList:',componentList);
		}
	}
		
	///////////////////////////////////////////////////////////////////////////////////
	//
	// function
	//
	///////////////////////////////////////////////////////////////////////////////////

	// set staticContents + ////////////////////////////////////////////
	utils.networkService = {
		title:			'New Design',
		name:			this.title,
		desc:			'Network Service',
		version:		'0.1',
		apn:			{},
		ippool:			'127.0.0.1'
	};
	
	utils.setNetworkService = function(e) {
		$(".staticContents").data('networkService',e);//store into web
		
		var ns = this.networkService;
		if(e !== null) ns = $.extend(ns,e);
		
		$(".staticContents .pop_tit").html(ns.title);//in NS Name
		$(".staticContents .pop_con ul li:first span").html(ns.name);//in Name
		$(".staticContents .pop_con ul li:nth-child(2) span").html(ns.version);
		$(".staticContents .pop_con ul li:nth-child(3) span").html(ns.desc);
		
		//sliceConfig :: (title), APN, IP-POOL
		$(".sliceConfig dl dd").html(ns.title);
		$(".sliceConfig ul li:first input").val(ns.apn);
		$(".sliceConfig ul li:last input").val(ns.ippool);
		
		$("#"+utils.getStorage()).data('networkService', ns);
	}
	
	//TODO NOT DEFAULT, 1st temporary values
	//$.get("/<URL>", function(data) {
	$.getJSON("/data/cloudListSample.json", function(data) { // 우측 데이터 및 팝업시 하단 데이터
		//console.log(data);
	
		if(!isNew) {
			$.each(data, function(i, e) {
				if(e.nsdType === nsdType) {
					utils.setNetworkService(e);
				}
			});
		}
	});
	
	//TODO ONT DEFAULT, 1st temporary values
	//$.get("/<URL>", function(data) {n 
	$.getJSON("/data/componentListSample.json", function(data) { // 팝업시 데이터
// 		alert("#"+utils.getStorage());
// 		console.log(data);
		$("#"+utils.getStorage()).data('componentList',data);
// 		console.log($("#"+utils.getStorage()).data('componentList'));
	});
	
	
	////////////////////////////////////////////////////////////////
	//
	// Window event
	//
	////////////////////////////////////////////////////////////////
	
	
	//check window size /////////////////////////////
	var rtime;
	var timeout = false;
	var delta = 200;//sample
	function resizeend() {
		if(new Date() - rtime < delta) {
			setTimeout(resizeend, delta);
		} else {
			timeout = false;
			view.resize();
			app.repositionButtons();
		}
	}
	
	$(window).resize(function() {
		rtime = new Date();
		if(timeout === false) {
			timeout = true;
			setTimeout(resizeend, delta);
		}
	})


	// window onclick for button refresh ///////////////////////////////
	/*/
	window.onclick = function(e) {
		
	}
	/**/
	
	//////////////////////////////////////////////////////////////////////////////////////
	//
	// view(html) event
	//
	//////////////////////////////////////////////////////////////////////////////////////
	
	// 'save' button //////////////////////////////////////////////////////////////////////
	$(".cont_btn a:first").on('click',function(){
		app.saveData();
	});
	
	
	// 'save & provisioning' button ///////////////////////////////////////////////////////
	$(".cont_btn a:nth-child(2)").on('click',function(){
		alert('save&provisioning');
	});
	
	
	// 'refresh' button ///////////////////////////////////////////////////////////////////
	$(".cont_btn a:last").on('click',function(){
		app.reloadCanvas(nsdType);
	});
	
	
	//components close/open ////////////////////////////////////////////////
	$(".lnb_m").on('click', function() {
		var type = this.dataset.id;
		
		//function DISABLE
		if( type === 'conn') return false;
		
		$.each(componentTypes, function(idx, etype) {
			$("#"+etype+"_components").css('display','none');
		});
		
		$("#"+type+"_components").css('display','block');
	});

	// 회사소개 ///////////////////////////////////////////////////////////////
	$(".footer_nav ul li:nth-child(1) a").on('click',function(){
		console.log(1);
	});
	
	
	
	////////////////////////////////////////////////////////////////////////
	//
	// POPUP DIALOG START
	//
	////////////////////////////////////////////////////////////////////////
	
	//DEFAULT utils.setContentsClass('editableContents','staticContents','components');
	app.popup = $(".editableContents");

	/*///RESERVED
	var dialogCloseStates = conf.dialogCloseStates;
	var dialogCloseState  = dialogCloseStates.CLOSE;
	/**/
	
	dialogClose = function() {
	}
	
	dialogUpdate = function() {
		
		/*/var title = $('.' + utils.getEditableContents() + ' .pop_tit').html();
		var name  = $('.' + utils.getEditableContents() + ' .pop_con ul li:first-child input').val();
		var v = 'dialogClosed:\n title:' + title +'\n name :' + name;/**/
		
		utils.vnfConfig.update();
		$(this).dialog('close');
	}
	
	dialogCancel = function(){
		$(this).dialog('close');
	}
	
	var editOrNewButtonText = "Save";
	if(!isNew) editOrNewButtonText = "Update";
	app.popup.dialog({
		dialogClass:"e2e-dialog",		
		autoOpen:isTest,
		resizable:false,
		width: 470,
		show:{effect:'blind',duration:600},
		hide:{effect:'blind',duration:600},
		close:dialogClose
		/**/
		,buttons:[
			{text: editOrNewButtonText,click:dialogUpdate},
			{text:'Cancel',click:dialogCancel}
		]
		/**/
	});
	
	//NS Configuration onOff
	$('.btn_pop_updown').click(function() {
		if(this.className.includes('on')) {
			$(this).prev('.pop_con_wrap').slideUp();
		} else {
			$(this).prev('.pop_con_wrap').slideDown();
		}
		
		$(this).toggleClass('on');
	});
	
		
	////////////////////////////////////////////////////////////////////////
	//
	// POPUP DIALOG END
	//
	////////////////////////////////////////////////////////////////////////
	
	
	
	/// FOR DEV OR TEST ////////////////////////////////////////////////////
	var componentClickCnt = [];
	bindComponentClickEvent = function() {
		
		//TODO 1st key is 'name' !!!!
		$(".component").on('click',function() {
			if(!isTest) return;
			
			var id = $(this).data('id');//TODO key!
			if(typeof componentClickCnt[id] === 'undefined') {
				componentClickCnt[id] = 0;
			}
			
			if(componentClickCnt[id] === 5) componentClickCnt[id] = -1;
			
			componentClickCnt[id]++;
			//TODO TEST
			var level = [
				app.level.Default,
				app.level.Normal,
				app.level.Minor,
				app.level.Major,
				app.level.Warning,
				app.level.Critical,
			][componentClickCnt[id]];
			alert($(this).data('id'));
			app.findIcon($(this).data('id'), componentClickCnt[id]);		
		});
	}
	
	var titleClickCnt  = 0;
	var titleClickCntLimit = 5;
	
	if(isTest) titleClickCntLimit = 1;
	$(".title").on('click', function(){

		titleClickCnt++;
		if(titleClickCnt === titleClickCntLimit) {
			titleClickCnt = 0;
			popupTestDialog();
			//popupFigures();
		}
	});

	var testPopupDialogKey = ".testDialog";
	$(testPopupDialogKey).dialog({
		autoOpen:false,			
		resizable:false,
		height:'auto',
		width:400,
		modal:true,
		buttons: {
			"zoomIn":function() { app.zoom(1.3); },
			"1:1":function() { app.zoom(1.0); },
			"zoomOut":function() { app.zoom(0.7); },
		}
	});
	
	function popupTestDialog() {
		console.log('called popupTestDialog');
		$(testPopupDialogKey).dialog('open');
	}
	
	/////////////////////////////////////////////////////////////////////////
});
</script>
<style>
.hidden { visibility:none; }
</style>

</head>


<body>

<!-- wrap -->
<div id="wrap">
    <!-- header -->
    <header>
		<h1 class="logo"><a href="/topologyEditor/">KT E2E Infra Orchestrator</a></h1>
		<!-- gnb -->
		<nav id="gnb" class="gnb">
			<ul>
				<li class="on"><a href="/topologyEditor/">DESIGN</a></li><!-- 활성화 class="on" -->
				<li><a href="/provisioning/">PROVISIONING </a></li>
				<li><a href="/monitoring/">MONITORING</a></li>
				<li class="hidden"><a href="#">INVENTORY</a></li>
				<li class="hidden"><a href="#">CONFIGURATION</a></li>
			</ul>
		</nav>
		<!-- //gnb -->
		<!-- ad_info -->
		<div class="ad_info">
			<i><img src="/images/layout/img_ad_info.jpg" alt=""></i><!-- image size : 25*25px -->
			<p>admin</p>
		</div>
		<!-- //ad_info -->
    </header>
    <!-- //header -->
    
    <!-- container -->
    <div id="container">	
		<!-- LNB -->
		<div id="lnb">
			<!-- lnb_search -->
			<div class="lnb_search">
				<div class="inner">
					<input type="text" placeholder="스텐실명을 입력해주세요.">
					<a href="#" class="btn">검색</a>
				</div>
			</div>
			<!-- //lnb_search -->
			<!-- lnb_nav -->
			<nav class="lnb_nav">
				<ul id="components">
					<li>
						<a href="#" class="lnb_m" data-id="vnf"><span>VNF Component</span></a>
						<!-- lnb_dep -->
						<div class="lnb_dep" id="vnf_components" style="display:block;">
							<div class="scrollbar-inner">
								<ul>
									<!-- icon image size : 56*56px css에서 고정 
									<li><a href="#"><i><img src="/images/contents/pgw.png" alt=""></i>PGW</a></li>
									<li><a href="#"><i><img src="/images/contents/sgw.png" alt=""></i>SGW</a></li>
									<li><a href="#"><i><img src="/images/contents/nat.png" alt=""></i>NAT</a></li>
									<li><a href="#"><i><img src="/images/contents/ips.png" alt=""></i>IPS</a></li>
									-->
								</ul>
							</div>
						</div>
						<!-- //lnb_dep -->
					</li>
					<li>
						<a href="#" class="lnb_m" data-id="pnf"><span>PNF Component</span></a>
						<div class="lnb_dep" id="pnf_components" style="display:none;">
							<div class="scrollbar-inner"><ul></ul></div>
						</div>
					</li>
					<li>
						<a href="#" class="lnb_m" data-id="conn"><span>Connectivity</span></a>
						<div class="lnb_dep" id="conn_components" style="display:none;">
							<div class="scrollbar-inner"><ul></ul></div>
						</div>
					</li>
					<li>
						<a href="#" class="lnb_m" data-id="cloud"><span>Cloud</span></a>
						<div class="lnb_dep" id="cloud_components" style="display:none;">
							<div class="scrollbar-inner"><ul></ul></div>
						</div>
					</li>
				</ul>
			</nav>
			<!-- //lnb_nav -->

		</div>
		<!-- //LNB -->
		
		<!-- cont_area -->
		<section class="cont_area">
			<h2 class="title"> <span>Network</span> <span>Service</span> <span>Designer</span></h2>
			<!-- cont_btn -->
			<nav class="cont_btn">
				<a href="#" class="btn_ty">Save</a>
				<a href="#" class="btn_ty">Save &amp; Provisioning</a>
				<a href="#" class="btn_ty02">Refresh</a>
			</nav>
			<!-- //cont_btn -->

			<!-- cont -->
			<!-- 
			<div class="cont" id="canvas" style="width:1480px; height:680px;">
			 -->
			 <div class="cont" id="canvas" style="min-width:660px; min-height:600px;">
				<!-- control_nav -->
				<!-- 
				<ul class="control_nav">
					<li class="prev"><a href="#">이전</a></li>
					<li class="next"><a href="#">다음</a></li>
				</ul>
				 -->
				<!-- //control_nav -->
				
				<!-- popup 17.11.16 추가 -->
				<div class="popup staticContents" id="pop01">
					<div class="pop_tit">New</div>
					<div class="pop_con_wrap">
						<div class="pop_con">
							<ul>
								<li>
									<strong>Name</strong><span>New</span>
								</li>
								<li>
									<strong>Version</strong><span>1.0</span>
								</li>
								<li>
									<strong>Desc</strong><span>New</span>
								</li>
							</ul>
						</div>						
					
					</div><!-- //pop_con -->
					<a href="javascript:void(0);" class="btn_pop_updown">open close</a>
				</div><!-- //popup -->
				
					<div class="popup editableContents" id="pop02" title="E2E Configuration">
						<div class="pop_tit">E2E Configuration</div>
						<div class="pop_con_wrap">
							<div class="pop_con vnfConfig">
								<dl>
									<dt>VNF Config.</dt>
									<dd>MME</dd>
								</dl>
								<ul>
									<li>
										<strong>Name</strong><input type="text" placeholder="ex) Umyun-MME#1" value="">
									</li>
									<li>
										<strong>VNF package</strong><input type="text" placeholder="ex) vMME ver1.0" value="">
									</li>
									<li>
										<strong>OAP IP</strong><input type="text" placeholder="ex) 172.21.21.154" value="">
									</li>
									<li>
										<strong>Scale Option</strong>
										<select>
											<option value="auto">auto</option>
											<option value="manual">manual</option>
										</select>
									</li>
								</ul>
							</div>
							<div class="pop_con sliceConfig">
								<dl>
									<dt>Slice Config.</dt>
									<dd>4G Service - PyoungChang</dd>
								</dl>
								<ul>
									<li><strong>APN</strong><input type="text" placeholder="ex) 4g_pyoungChang.private.lte.com" value="JJ"></li>
									<li><strong>IP-Pool</strong><input type="text" placeholder="ex) 172.10.15.0/24" value=""></li>
								</ul>
							</div>
						</div><!-- //pop_con -->
						<!-- TEST
						<div class="pop_tit2">
							<a href="#" class="btn_save">Cancel</a>
							<a href="#" class="btn_save btn_opt">Save</a>
						</div>
						 -->
						<a href="javascript:void(0)" class="btn_pop_close">close</a>
					</div><!-- //popup -->
					
					
					<!-- //17.11.16 추가 -->
			</div>
			<!-- //cont -->
		</section>
		<!-- //cont_area -->
	</div>
	<!-- //container -->
	
	<!-- test? -->
	<div title="test menu" class="testDialog">
		
	</div>
	
	<!-- footer -->
	<footer>
		<!-- footer_nav -->
		<nav class="footer_nav">
			<ul>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">이용약관</a></li>
				<li><a href="#">개인정보처리방침</a></li>
				<li><a href="#">Sitemap</a></li>
			</ul>
		</nav>
		<!-- //footer_nav -->
		<address>㈜케이티  대표이사 황창규   경기도 성남시 분당구 불정로 90 (정자동)   사업자등록번호 : 102-81-42945   통신판매업신고 : 2002-경기성남-0047</address>
	</footer>
	n<!-- //footer -->
</div>
<!-- //wrap -->

</body>
</html>
