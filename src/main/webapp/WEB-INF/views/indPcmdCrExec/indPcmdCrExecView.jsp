<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content= "text/html; charset=UTF-8"> 
<meta name="format-detection" content="telephone=no">
<title>KT E2E Infra Orchestrator</title>
<link rel="shortcut icon" href="/new_resources/resources/images/layout/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="/new_resources/resources/css/style.css">
<script type="text/javascript" src="/new_resources/resources/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/template.js"></script>
<script type="text/javascript">
var lists = [];
var r_equip_type="MME";
var param_lists = [];
var all_param_lists = []; //전체 명령어 검증 체크용
//onkeydown="onlyNumber(this) 숫자값만 입력 받게
function onlyNumber(obj) {
    $(obj).keyup(function(){
         $(this).val($(this).val().replace(/[^0-9]/g,""));
    }); 
}

//명령어 생성 시작
function command_create_first()
{
	init_value();
	init_btn(true);
	command_create();
	init_btn(false);
}

//명령어 생성
function command_create()
{

	 
	var cnt=0;
	 $('input:radio[name="sel_equip_id"]:checked').each(function() {
		 cnt=1;
	 });
	 
    if(cnt <=0)
    {
    	alert('선택되어진 Target System이 없습니다.');
    	return false;
    }

	var create_command_sentence="";
	create_command_sentence=$("h2[id='cmd_title']").text() +":";
	create_command_sentence+="SYS_TYPE="+r_equip_type;
	create_command_sentence+=",SYS_NAME="+ $('input:radio[name="sel_equip_id"]:checked').val();
    
    /*
param_items.push(pri[i].parameter.trim());
param_items.push(pri[i].man_opt.trim());	  
param_items.push(pri[i].parameter_type.trim());	 
param_items.push((typeof(pri[i].min)=="undefined")?"":pri[i].min.trim());	 
param_items.push((typeof(pri[i].max)=="undefined")?"":pri[i].max.trim());	
    */
	var commaTxt="";
    for(var i=0; i < param_lists.length;i++)
    {
    	var parameter=param_lists[i][0];
    	var man_opt=param_lists[i][1];
    	var parameter_type=param_lists[i][2];
    	var min=param_lists[i][3];
    	var max=param_lists[i][4];
    	var parameter_seq_no=param_lists[i][5];
    	var p_val_length=0;
    	var p_val="";

    	if(parameter_type=="ENUM")
    	{
	    	p_val_length=$.trim($("select[name='" + parameter + "']").val()).length;
	    	p_val=$.trim($("select[name='" + parameter + "']").val());
    	}else{
	    	p_val_length=$.trim($("input[name='" + parameter + "']").val()).length;
	    	p_val=$.trim($("input[name='" + parameter + "']").val());
    	}
    	if(man_opt=="MAN")
   		{
    		if(p_val_length==0)
    		{
    			alert(parameter +"의 파라메터가 주요값이므로 입력하세요");
    			$("input[name='" + parameter + "']").focus();
    			return false;
    		}else{
    			if(parameter_type=="STRING")
    			{
    				if(p_val_length < Number(min))
   					{
    					alert(parameter +"의 파라메터는 최소 " +min+ "자리입니다.\n현재 " +p_val_length+ "자리입니다.");
    	    			$("input[name='" + parameter + "']").focus();
    	    			return false;
   					}else if(p_val_length > Number(max)){
    					alert(parameter +"의 파라메터는 최대 " +max+ "자리입니다.\n현재 " +p_val_length+ "자리입니다.");
    	    			$("input[name='" + parameter + "']").focus();
    	    			return false;
   					}
    			}else if(parameter_type=="DECIMAL"){
    				if($.isNumeric( p_val)==false){
    					alert(parameter +"의 파라메터는 숫자값만 입력받습니다.\n현재 " +p_val+ " 값입니다.");
    	    			$("input[name='" + parameter + "']").focus();
    	    			return false;
    				}if(Number(p_val) < Number(min)){
    					alert(parameter +"의 파라메터는 최소값이 " +min+ "부터입니다.\n현재 " +p_val+ " 값입니다.");
    	    			$("input[name='" + parameter + "']").focus();
    	    			return false;
   					}else if(Number(p_val) > Number(max)){
    					alert(parameter +"의 파라메터는 최대값이 " +max+ "까지입니다.\n현재 " +p_val+ " 값입니다.");
    	    			$("input[name='" + parameter + "']").focus();
    	    			return false;
   					}
    				
    			}
    		}
   		
   		}else{
   			
   			if(p_val_length != 0)
    		{
    			if(parameter_type=="STRING")
    			{
    				if(p_val_length < Number(min))
   					{
    					alert(parameter +"의 파라메터는 최소 " +min+ "자리입니다.\n현재 " +p_val_length+ "자리입니다.");
    	    			$("input[name='" + parameter + "']").focus();
    	    			return false;
   					}else if(p_val_length > Number(max)){
    					alert(parameter +"의 파라메터는 최대 " +max+ "자리입니다.\n현재 " +p_val_length+ "자리입니다.");
    	    			$("input[name='" + parameter + "']").focus();
    	    			return false;
   					}
    			}else if(parameter_type=="DECIMAL"){
    				if($.isNumeric( p_val)==false){
    					alert(parameter +"의 파라메터는 숫자값만 입력받습니다.\n현재 " +p_val+ " 값입니다.");
    	    			$("input[name='" + parameter + "']").focus();
    	    			return false;
    				}if(Number(p_val) < Number(min)){
    					alert(parameter +"의 파라메터는 최소값이 " +min+ "부터입니다.\n현재 " +p_val+ " 값입니다.");
    	    			$("input[name='" + parameter + "']").focus();
    	    			return false;
   					}else if(Number(p_val) > Number(max)){
    					alert(parameter +"의 파라메터는 최대값이 " +max+ "까지입니다.\n현재 " +p_val+ " 값입니다.");
    	    			$("input[name='" + parameter + "']").focus();
    	    			return false;
   					}
    				
    			}
    		}
   		}
    	
        if(p_val_length==0)
       	{
			commaTxt+="," ;
       	}else{
			create_command_sentence+= commaTxt+ ","+parameter + "="+p_val;
			commaTxt="";
       	}
    }

    create_command_sentence+=";";
    

    //alert(create_command_sentence);
    //return false;	
	$.ajax({
		type : "POST",
		url : "/indPcmdCrExec/cmd_create",
		data : {
			work_command_create_no : 0 ,
			create_command_sentence : create_command_sentence,
			equip_type_nm : r_equip_type,
			equip_id: $('input:radio[name="sel_equip_id"]:checked').val()
		 
		},
		dataType : "text",
		cache: false, 
		success : function(result) { 	
			var dataObj = JSON.parse(result);
			 $('textarea[id="create_command_sentence"]').text( dataObj.create_command_sentence );
	            $("input:hidden[name='work_command_create_no']").val(dataObj.work_command_create_no);
            alert("생성을 완료했습니다.");

		},
		error : function(result, status, err) {
			alert('생성에 실패했습니다');
		},
	 	beforeSend: function() {
	 		//myLayout.cells("b").progressOn();
		},		
		complete : function() {
			//myLayout.cells("b").progressOff();
		}
	});
	
}

var sys_pop_name="";
function check_equip_name(vv){
	$('td[id="sys_pop_name"]').text(vv);
	
	sys_pop_name=vv;
}


//왼쪽에 해당 명령어 선택시
function ind_select_command_list(etype_cd,cmd)
{
	 $('textarea[id="create_command_sentence"]').text("");
	param_lists = [];
		  
	$("h2[id='cmd_title']").text(cmd); 
	$.ajax({
	       type: "get",
	       url: "/indPcmdCrExec/equip_cmd_param_list?gug_equip_type_cd="+ etype_cd +
	    		   "&command=" + cmd
	    		   ,
	       success: function(result){
	           	var dataObj = JSON.parse(result);
	           	 
	           	var ecd =  dataObj.equip_cmd;
	           	var command_help="";
	           	var text_with_br ="";
	           	if(typeof(ecd.command_help)!="undefined")
	           	{
	           	 	text_with_br = ecd.command_help.replace(/\r\n/g,"<br>");
	           	//var normalized_Enters = ecd.command_help.replace(/\r|\/g, "\\r\");
		            //var text_with_br = normalized_Enters.replace(/\\r\/g, "<br />");
		       }
	           	var command_help=((typeof(ecd.command_help)=="undefined")? "" :text_with_br) ;
	            var helpData_txt="";
	           	helpData_txt+="<tr>";
	           	helpData_txt+="<td colspan=2>"+ command_help  +"</td>";
	           	helpData_txt+="<tr>";
	           	 $("tbody[id='helpData']").html(helpData_txt);
	           	 
	           	 var manData_txt="";
	           	 var optData_txt="";
	           	var pri = dataObj.param_list;
	           	 for(var i in pri){
	
	           		var param_items = [];
	           		param_items.push(pri[i].parameter.trim());
	           		param_items.push((typeof(pri[i].man_opt)=="undefined")?"OPT":pri[i].man_opt.trim());	  
	           		param_items.push((typeof(pri[i].parameter_type)=="undefined")?"STRING":pri[i].parameter_type.trim());	 
	           		param_items.push((typeof(pri[i].min)=="undefined")?"1":pri[i].min.trim());	 
					param_items.push((typeof(pri[i].max)=="undefined")?"99999999999":pri[i].max.trim());
	           		param_items.push((typeof(pri[i].parameter_seq_no)=="undefined")?"1":pri[i].parameter_seq_no.trim());

	           		param_lists.push(param_items);
	           		 
	           		 if(param_items[1]=="MAN")
           			 {
	           			if(param_items[2]=="ENUM")
	           			{
		           			manData_txt+="</tr>";
		           			manData_txt+="<tr>";
		           			manData_txt+="<th><span style='color:red;'>" + pri[i].parameter + "</span></th>";
		           			manData_txt+="<td>";
		           			manData_txt+="<select name=\"" + pri[i].parameter + "\">";
		           			manData_txt+="<option value=\"\" ></option>";
		           			var tmpOp=pri[i].enum_list.split("/");
		           			for(var i=0;i<tmpOp.length;i++){
		           				manData_txt+="<option value=\"" + tmpOp[i].trim() +"\" >" + tmpOp[i].trim() + "</option>";
		           			}
		           			
		           			manData_txt+="</select>";
		           			manData_txt+="</td>";
		           			manData_txt+="</tr>";		           			
		           			
	           			}else if(param_items[2]=="STRING"){
	           			
		           			manData_txt+="<tr>";
		           			manData_txt+="<th><span style='color:red;'>" + pri[i].parameter + "</span></th>";
		           			manData_txt+="<td><input type=\"text\" " +
		           			                  " name=\"" + pri[i].parameter.trim() + "\"" +
		           			                  " minlength=\"" + ((typeof(pri[i].min)=="undefined")?"1":pri[i].min.trim()) + "\"" +
		           			                  " maxlength=\"" + ((typeof(pri[i].max)=="undefined")?"99999999999":pri[i].max.trim()) + "\"" +
		           			                 "></td>";
		           			manData_txt+="</tr>";	
	           			
	           			}else{
		           			manData_txt+="<tr>";
		           			manData_txt+="<th><span style='color:red;'>" + pri[i].parameter + "</span></th>";
		           			manData_txt+="<td><input type=\"text\" name=\"" + pri[i].parameter + "\"></td>";
		           			manData_txt+="</tr>";
	           				
	           			}
           			 }else {
 	           			if(param_items[2]=="ENUM")
	           			{
	 	           			optData_txt+="<tr>";
	 	           			optData_txt+="<th><span>" + pri[i].parameter + "</span></th>";
	 	           			optData_txt+="<td>";
	 	           			optData_txt+="<select name=\"" + pri[i].parameter + "\">";
	 	           		    optData_txt+="<option value=\"\" ></option>";
		           			var tmpOp=pri[i].enum_list.split("/");
		           			for(var i=0;i<tmpOp.length;i++){
	 	           				optData_txt+="<option value=\"" + tmpOp[i].trim() +"\" >" + tmpOp[i].trim() + "</option>";
		           			}
		           			
	 	          			optData_txt+="</select>";
	 	         			optData_txt+="</td>";
	 	           			optData_txt+="</tr>";
	 	           		
	           			}else if(param_items[2]=="STRING"){
	           				optData_txt+="<tr>";
	           				optData_txt+="<th><span>" + pri[i].parameter + "</span></th>";
	           				optData_txt+="<td><input type=\"text\" " +
		           			                  " name=\"" + pri[i].parameter.trim() + "\"" +
		           			                  " minlength=\"" + ((typeof(pri[i].min)=="undefined")?"1":pri[i].min.trim()) + "\"" +
		           			                  " maxlength=\"" + ((typeof(pri[i].max)=="undefined")?"99999999999":pri[i].max.trim()) + "\"" +
		           			                  " ></td>";
		           			optData_txt+="</tr>";	
		           			
	           			}else{
	 	           			optData_txt+="<tr>";
	 	           			optData_txt+="<th><span>" + pri[i].parameter + "</span></th>";
	 	           			optData_txt+="<td><input type=\"text\" name=\"" + pri[i].parameter + "\"></td>";
	 	           			optData_txt+="</tr>";	           				
	           			}
           			 }
	           		 
	           		
	           		//html_txt +=", " + dataObj[i].parameter + ":" + dataObj[i].parameter_type;
	           		 
	           		 //html_txt +="<li><input name=\"sel_equip_id\" type=\"radio\" value=\"" + dataObj[i].equip_id + "\">" +
	           		   //   "<label for=\"ck_1\">" + dataObj[i].equip_name + "</label></li>";
	
	           	 }
	           	 
	           	 //alert(html_txt);
	           $("tbody[id='manData']").html(manData_txt); 
	           $("tbody[id='optData']").html(optData_txt); 
	           
	           $("h3[id='result_msg_title']").html("<span>Result Message</span> "+cmd); 
	           
	           $("input[name='equip_name']").val("");
	           $("input[name='return_create_command_sentence']").val(""); 
	           $("textarea[name='exec_return_message']").text(""); 
	           

	           
	       },

		error : function(result, status, err) {
			alert('해당하는 명령어의 파라메터를 가져오는데 실패했습니다.');
		}
	   });
}

//왼쪽에 해당 장비구분명 선택시
function ind_select_equip_type(etype_nm)
{
	 $('textarea[id="create_command_sentence"]').text("");
	r_equip_type=etype_nm;
	 $.ajax({
	        type: "get",
	        url: "/indPcmdCrExec/equip_list?equip_type="+ etype_nm ,
	        success: function(result){
	            	 var dataObj = JSON.parse(result);
	            	 var html_txt="";
	            	 for(var i in dataObj){
	            		 html_txt +="<li><input name=\"sel_equip_id\" type=\"radio\" value=\"" + dataObj[i].equip_id + "\">" +
	            		      "<label for=\"ck_1\">" + dataObj[i].equip_name + "</label></li>";

	            	 }
	            	 
	            $("ul[id='equip_radio_content']").html(html_txt); 

	        },
			error : function(result, status, err) {
				alert('Target System을 여는데 실패했습니다.');
			}
	    });
}


function run_command_first()
{
	init_value();
	init_btn(true);
	run_command();
}

function run_command_ready()
{
	var command_text=$.trim($("textarea[id='create_command_sentence']").text());
	if(command_text=="")
	{
		alert("명령어가 없습니다.\n명령어 생성후 시도하세요.");
		$("textarea[id='create_command_sentence']").focus();
		return false;
	}
	if(command_text.indexOf('SYS_TYPE')==-1)
	{
		alert("장비 타입명[SYS_TYPE]이 명령어에 없습니다.");
		$("textarea[id='create_command_sentence']").focus();
		return false;
	}
	
	if(command_text.indexOf('SYS_NAME')==-1)
	{
		alert("장비아이디(이름)[SYS_NAME]이 명령어에 없습니다.");
		$("textarea[id='create_command_sentence']").focus();
		return false;
	}
	if(command_text.indexOf(':')==-1)
	{
		alert("명령어 포맷에 콜론(:)이 없습니다.");
		$("textarea[id='create_command_sentence']").focus();
		return false;
	}

	if(command_text.indexOf('DIS') == 0 
		|| command_text.indexOf('CRTE-PLTE-INFO') > -1
		|| command_text.indexOf('CHG-PLTE-INFO') > -1		
		|| command_text.indexOf('DEL-PLTE-INFO') > -1	
	)
	{
		$("th[id='th_cmd_confirm']").html("");
		$("td[id='td_cmd_confirm']").html("");

	}else{
		$("th[id='th_cmd_confirm']").html("<span>명령어 Confirm</span>");
		$("td[id='td_cmd_confirm']").html("<input type=\"password\" name=\"confirm_pwd\" value=\"\">");
	}
	
	var base_cmd="";
	var base_params="";
	var sys_type="";
	var sys_id="";
	var colonSlt=command_text.split(":");
	
	for (var i=0;i<colonSlt.length;i++)
	{
		if(i==0) base_cmd=colonSlt[i]
		if(i==1) base_params=colonSlt[i]
	
	}
	var jjj=0;
 	 for(var i=0; i < all_param_lists.length;i++)
	 {
 		 if (base_cmd==all_param_lists[i][6])break;
 		  jjj=i;
/*
	    	var parameter=all_param_lists[i][0];
	    	var man_opt=all_param_lists[i][1];
	    	var parameter_type=all_param_lists[i][2];
	    	var min=all_param_lists[i][3];
	    	var max=all_param_lists[i][4];
	    	var parameter_seq_no=all_param_lists[i][5];
		  	var command=all_param_lists[i][6];*/
		  //	alert(all_param_lists[i][0]);
	 }
 	
 	 if(jjj==(all_param_lists.length-1))
	 {
	 	alert("존재하지 않은 명령어입니다.")
	 	return false;
	 }
 	
	var param_items=base_params.split(",");
	for (var i=0;i<param_items.length;i++)
	{
		var prams=param_items[i].split("=");
		if(prams[0].trim()=="SYS_TYPE")sys_type=prams[1].trim();
		if(prams[0].trim()=="SYS_NAME")sys_id=prams[1].trim();
	
	}
	$("div[id='access_title']").text(sys_type+" 접속 승인")
	
	 $.ajax({
	        type: "get",
	        url: "/indPcmdCrExec/equip_list?equip_type="+ sys_type + "&equip_id="+ sys_id ,
	        success: function(result){
	            	 var dataObj = JSON.parse(result);
	            	 var html_txt="";
	            	 for(var i in dataObj){
	            		 $('td[id="sys_pop_name"]').text( dataObj[i].equip_name);

	            	 }
	            	 
	        },
			error : function(result, status, err) {
				alert('Target System을 여는데 실패했습니다.');
			}
	    });
	
	
	
	
	$.ajax({
        type: "get",
        data : {
			sys_type :  sys_type,
			sys_name : sys_id
		 
		},
		dataType : "text",
		cache: false, 
        url: "/equipInven/plteConnInfoView",
        success: function(result){
        	if(result!=""){

				var dataObj = JSON.parse(result);
				$("input:password[name='ems_pwd']").val(dataObj.pwd_1);
				$("input:password[name='cli_pwd']").val(dataObj.cli_pwd);
				if(command_text.indexOf('DIS') != 0
					|| command_text.indexOf('CRTE-PLTE-INFO') == -1
					|| command_text.indexOf('CHG-PLTE-INFO') == -1		
					|| command_text.indexOf('DEL-PLTE-INFO') == -1		
				)
				{
					$("input:password[name='confirm_pwd']").val(dataObj.confirm);
					$("input:password[name='confirm_pwd']").val("");
				}
				
				$("input:password[name='ems_pwd']").val("");
				$("input:password[name='cli_pwd']").val("");
			 
           	 }else{
 				$("input:password[name='ems_pwd']").val("");
				$("input:password[name='cli_pwd']").val("");
				if(command_text.indexOf('DIS') != 0
					|| command_text.indexOf('CRTE-PLTE-INFO') == -1
					|| command_text.indexOf('CHG-PLTE-INFO') == -1		
					|| command_text.indexOf('DEL-PLTE-INFO') == -1	
				)
				{
					$("input:password[name='confirm_pwd']").val("");  
				}
           	 }
        	/*$("input[name='ipaddr_1']").val(dataObj.ipaddr_1)      ;
			$("input[name='id_1']").val(dataObj.id_1)          ;
			$("input[name='pwd_1']").val(dataObj.pwd_1)         ;
			$("input[name='expect_1']").val(dataObj.expect_1)	     ;  
			$("input[name='ipaddr_2']").val(dataObj.ipaddr_2)      ;
			$("input[name='id_2']").val(dataObj.id_2)          ;
			$("input[name='pwd_2']").val(dataObj.pwd_2)         ;
			$("input[name='expect_2']").val(dataObj.expect_2)      ;
			$("input[name='cli_cmd']").val(dataObj.cli_cmd)       ;
			$("input[name='cli_id']").val(dataObj.cli_id)        ;
			$("input[name='cli_pwd']").val(dataObj.cli_pwd)       ;
			$("input[name='cli_expect']").val(dataObj.cli_expect)    ;
			$("input[name='confirm_expect']").val(dataObj.confirm_expect);
			$("input[name='confirm']").val(dataObj.confirm)       ;                         
			$("input[name='cli_exit']").val(dataObj.cli_exit)      ;  
      */

        }
    });
	init_btn(true);
	pop_open("#mme_pop");
}

function init_btn(flag)
{
	 $("a[id='btn_create']").attr("disabled",flag);
	 $("a[id='btn_run']").attr("disabled",flag);
	 

	
}

function init_value()
{
	$("input:text[name='return_create_command_sentence']").val("");
	$("input:text[name='return_create_command_sentence']").attr("title","");
	$("textarea[name='exec_return_message']").text("");
	$("input:text[name='equip_name']").val("");
}

//명령어 실행
function run_command()
{
	
	$.ajax({
		type : "POST",
		url : "/indPcmdCrExec/run_command",
		//contentType:"application/x-www-form-urlencoded;charset=utf-8", //한글 깨짐 방지
		data : {
			work_command_create_no : $("input:hidden[name='work_command_create_no']").val(),
			create_command_sentence : $("textarea[id='create_command_sentence']").text(),
			ems_pwd : $("input:password[name='ems_pwd']").val(),
			cli_pwd : $("input:password[name='cli_pwd']").val(),
			confirm_pwd : $("input:password[name='confirm_pwd']").val()
		 
		},
		dataType : "text",
		cache: false, 
		success : function(result) { 		
			 var dataObj = JSON.parse(result);
			 $("input:text[name='return_create_command_sentence']").val(dataObj.create_command_sentence);
			 $("input:text[name='return_create_command_sentence']").attr("title",dataObj.create_command_sentence);
			 $("textarea[name='exec_return_message']").text(dataObj.exec_return_message);
			 $("input:text[name='equip_name']").val(dataObj.equip_name);
           // alert("명령어 실행을 완료했습니다.");
		},
		error : function(result, status, err) {
			alert('명령어 실행에 실패했습니다');
			init_btn(false);
			pop_close("#preload_pop");
		},
	 	beforeSend: function() {
	 		init_btn(true);

			pop_open("#preload_pop");
	 		//myLayout.cells("b").progressOn();
		},		
		complete : function() {
			//myLayout.cells("b").progressOff();
	 		init_btn(false);
			pop_close("#preload_pop");
		}
		
	});
	pop_close("#mme_pop");
}


//명령어 검증 체크용 기준 자료 가져오기
function select_command_param_list()
{
	all_param_lists = [];

	$.ajax({
	       type: "get",
	       url: "/indPcmdCrExec/select_command_param_list"
	    		   ,
	       success: function(result){
	           	var dataObj = JSON.parse(result);
	           	 
	           	var pri = dataObj.param_list;
	           	 for(var i in pri){
	
	           		var param_items = [];

	           		param_items.push(pri[i].parameter.trim());
	           		param_items.push((typeof(pri[i].man_opt)=="undefined")?"OPT":pri[i].man_opt.trim());	  
	           		param_items.push((typeof(pri[i].parameter_type)=="undefined")?"STRING":pri[i].parameter_type.trim());	 
	           		param_items.push((typeof(pri[i].min)=="undefined")?"1":pri[i].min.trim());	 
					param_items.push((typeof(pri[i].max)=="undefined")?"99999999999":pri[i].max.trim());
	           		param_items.push((typeof(pri[i].parameter_seq_no)=="undefined")?"1":pri[i].parameter_seq_no.trim());
	           		param_items.push(pri[i].command.trim());
					all_param_lists.push(param_items);
	           		
	
	           	 }

	           
	       },

		error : function(result, status, err) {
			alert('명령어 검증 체크용 기준 자료 가져오는데 실패했습니다.');
		}
	   });
	
	// 
	
	 
}

$( document ).ready(function() {
	ind_select_command_list("D001","CRTE-PGW-GRP");
	select_command_param_list();
	//select_command_list(1);
    //console.log( "ready!" );

});

window.onload = function(){
	  var el = document.getElementById('preload_pop');
	  el.style.display = 'none';
	};


</script>
</head>

<body onload="//command_create()">
<div id="wrap">
    
    <header>
		<h1 class="logo"><a href="#">KT E2E Infra Orchestrator</a></h1>
		<nav id="gnb">
			<ul>
				<li><a href="#">DESIGN</a></li>
				<li><a href="#">PROVISIONING</a></li>
				<li><a href="#">MONITORING</a></li>
				<li><a href="#">INVENTORY</a></li>
				<li><a href="#">CONFIGURATION</a></li>
				<li class="active"><!-- 활성화 class="active" -->
					<a href="#">P-LTE</a>
					<ul>
						<li><a href="/gugDataConfig">국데이터 관리</a></li><!-- 활성화 class="active" -->
						<li><a href="/gugPcmdCrExec">명령 실행</a></li>		
						<li class="active"><a href="/indPcmdCrExec">운용자 명령어</a></li>
						<li><a href="/cust">고객사 관리</a></li>
						<li><a href="/equipInven">장비 관리</a></li>
						<li><a href="#">모니터링</a></li>
						<li><a href="#">이력조회</a></li>
					</ul>
				</li>
			</ul>
		</nav><!-- //gnb -->
		<div class="ad_info">
			<i><img src="/new_resources/resources/images/layout/img_ad_info.jpg" alt=""></i><!-- image size : 25*25px -->
			<p>admin</p>
		</div><!-- //ad_info -->
    </header>
    
    <div id="container">	
		
		<ul id="navigation">
			<li><img src="/new_resources/resources/images/layout/img_navigation_home.png" alt="home"></li>
			<li>운용자 명령어</li>
		</ul><!-- //navigation -->
		
		<div id="contents_wrap">
	<!-- 	"D001";"MME"
"D002";"ePCC"
"D003";"PGW"
"D004";"HSS"
		 -->
		 <input	type="hidden" name="work_command_create_no">
			<div id="lnb">
				<strong class="lnb_title">Command List</strong>
				<ul class="lnb_nav">
					<li>
						<a href="#" onclick="ind_select_equip_type('MME');">MME</a>
						<div class="lnb_dep">
							<div class="scrollbar-inner">
								<ul>
		 						<c:forEach var="clist" items="${command_list}">
		 							<c:if test="${clist.gug_equip_type_cd eq 'D001'  }">
									<li><a href="#" onclick="ind_select_command_list('${clist.gug_equip_type_cd}','${clist.command}')">${clist.command}</a></li>
									</c:if>
								 </c:forEach>
								</ul>
							</div>
						</div>
					</li>
					<li>
						<a href="#" onclick="ind_select_equip_type('PGW');">PGW</a>
						<div class="lnb_dep">
							<div class="scrollbar-inner">
								<ul>
		 						<c:forEach var="clist" items="${command_list}">
		 							<c:if test="${clist.gug_equip_type_cd eq 'D003'  }">
									<li><a href="#" onclick="ind_select_command_list('${clist.gug_equip_type_cd}','${clist.command}')">${clist.command}</a></li>
									</c:if>
								 </c:forEach>
								</ul>
							</div>
						</div>
					</li>
					<li>
						<a href="#" onclick="ind_select_equip_type('EPCC');">ePPC</a>
						<div class="lnb_dep">
							<div class="scrollbar-inner">
								<ul>
		 						<c:forEach var="clist" items="${command_list}">
		 							<c:if test="${clist.gug_equip_type_cd eq 'D002'  }">
									<li><a href="#" onclick="ind_select_command_list('${clist.gug_equip_type_cd}','${clist.command}')">${clist.command}</a></li>
									</c:if>
								 </c:forEach>
								</ul>
							</div>
						</div>
					</li>
					<li>
						<a href="#" onclick="ind_select_equip_type('HSS');">HSS(아리엘)</a>
						<div class="lnb_dep">
							<div class="scrollbar-inner">
								<ul>
		 						<c:forEach var="clist" items="${command_list}">
		 							<c:if test="${clist.gug_equip_type_cd eq 'D004' and clist.command_comp_type_cd eq 'D001'  }">
									<li><a href="#" onclick="ind_select_command_list('${clist.gug_equip_type_cd}','${clist.command}')">${clist.command}</a></li>
									</c:if>
								 </c:forEach>
								</ul>
							</div>
						</div>
					</li>
					<li>
						<a href="#" onclick="ind_select_equip_type('HSS');">HSS(이루온)</a>
						<div class="lnb_dep">
							<div class="scrollbar-inner">
								<ul>
		 						<c:forEach var="clist" items="${command_list}">
		 							<c:if test="${clist.gug_equip_type_cd eq 'D004' and clist.command_comp_type_cd eq 'D002'  }">
									<li><a href="#" onclick="ind_select_command_list('${clist.gug_equip_type_cd}','${clist.command}')">${clist.command}</a></li>
									</c:if>
								 </c:forEach>
								</ul>
							</div>
						</div>
					</li>
					<li>
						<a href="#" onclick="ind_select_equip_type('WNAT');">WNAT</a>
						<div class="lnb_dep">
							<div class="scrollbar-inner">
								<ul>
		 						<c:forEach var="clist" items="${command_list}">
		 							<c:if test="${clist.gug_equip_type_cd eq 'D005'  }">
									<li><a href="#" onclick="ind_select_command_list('${clist.gug_equip_type_cd}','${clist.command}')">${clist.command}</a></li>
									</c:if>
								 </c:forEach>
								</ul>
							</div>
						</div>
					</li>
				</ul>
			</div><!-- //lnb -->
			
			<div class="contents">
				
				<h2 class="title" id="cmd_title">CRTE_PLTE_INFO</h2>
				
				<div class="float_area ty02">
					<div class="group fl">
						<div class="f_box">
						<div class="table_form scrollbar-inner"><!-- 180105 수정 -->
								<table>
									<caption>입력 테이블</caption>
									<colgroup>
										<col span="1" style="width:34%;">
										<col span="1" style="width:*;">
									</colgroup>
									<tbody id="manData" style="border:0px">
										<tr>
											<th><span>ID</span></th>
											<td><input type="text"></td>
										</tr>
										<tr>
											<th><span>APN</span></th>
											<td><input type="text"></td>
										</tr>
										<tr>
											<th><span>APN2</span></th>
											<td><input type="text"></td>
										</tr>
										<tr>
											<th><span>APN3</span></th>
											<td><input type="text"></td>
										</tr>
									</tbody>
								</table>
							</div><!-- //table_form scrollbar-inner -->
						</div><!-- //f_box -->
						
						<div class="f_box ">
							<div class="table_form scrollbar-inner"><!-- 180105 수정 -->
								<table>
									<caption>입력 테이블</caption>
									<colgroup>
										<col span="1" style="width:34%;">
										<col span="1" style="width:*;">
									</colgroup>
									<tbody id="optData" style="border:0px">
										<tr>
											<th><span>APNOIREPL</span></th>
											<td><input type="text"></td>
										</tr>
										<tr>
											<th><span>PLTEECC</span></th>
											<td><input type="text"></td>
										</tr>
										<tr>
											<th><span>UUTYPE</span></th>
											<td><input type="text"></td>
										</tr>
										<tr>
											<th><span>MPNAME</span></th>
											<td><input type="text"></td>
										</tr>
										<tr>
											<th><span>MPNAME2</span></th>
											<td><input type="text"></td>
										</tr>
									</tbody>
								</table>
							</div><!-- //table_form scrollbar-inner -->
							
						</div><!-- //f_box -->
					</div>

					<div class="group fl">
						<div class="f_box">
							<h4 class="title">MMC Help</h4>
							<div class="table_ty02 scrollbar-inner">
								<table>
									<caption>정보 테이블</caption>
									<colgroup>
										<col span="1" style="width:26%;">
										<col span="1" style="width:*;">
									</colgroup>
									<tbody id="helpData" style="border:0px">
										<tr>
											<th>Command</th>
											<td>CRTE-BLACK-LIST [STARTIMEI] [,TAC_FAC]</td>
										</tr>
										<tr>
											<th>Description</th>
											<td>Mobile BLACK COLOR LIST INFORMATION Create</td>
										</tr>
										<tr>
											<th>Example</th>
											<td>crte-black-list 11112222, 333333, 333344, fault(b), 11</td>
										</tr>
										<tr>
											<th>Class</th>
											<td>Super User</td>
										</tr>
										<tr>
											<th>Parameters</th>
											<td>STARTIMEI = STRING(14~14) :IMEI TAC_FAC = STRING(8~) : TAC_FAC STARTSRN = STRING(6~6) : START Serial Number ENDSRN = STRING(6~6) : END Serial Number STS = ENUM(FAULT(B), STOLEN(B), CORRUPTED(B),</td>
										</tr>
									</tbody>	
								</table>
							</div><!-- //table_ty02 scrollbar-inner -->
						</div><!-- //f_box -->
					</div>

					<div class="group fr">
						<div class="f_box">
							<h4 class="title">Target System </h4>
							<div class="checkbox_area scrollbar-inner">
								<ul id="equip_radio_content">
							<c:forEach var="equip" items="${equip_list}">
		                           	<li><input name="sel_equip_id" type="radio" value="${equip.equip_id}" onClick="check_equip_name('${equip.equip_name}')"><label for="ck_1">${equip.equip_name}</label></li>
							</c:forEach>
								</ul>
							</div><!-- //checkbox_area scrollbar-inner -->
						</div><!-- //f_box -->
						<div class="f_box">
							<textarea cols="5" rows="3" id="create_command_sentence"></textarea>
						</div><!-- //f_box -->
					</div>
				</div><!-- //float_area -->
				
				<div class="btn_area">
					<a href="#" onclick="command_create_first()" id="btn_create" class="btn_l btn_co01">생성 완료</a>
					<a href="#" onclick="run_command_ready()" id="btn_run" class="btn_l btn_co01">Run</a>
				</div>
				
				<h3 class="title" id="result_msg_title"><span>Result Message</span> CRTE-PGW-GRP</h3>
				<div class="table_result">
					<table>
						<caption>입력 테이블</caption>
						<colgroup>
							<col span="1" style="width:20%;">
							<col span="1" style="width:*;">
						</colgroup>
						<tbody>
							<tr>
								<th>Target System</th>
								<td><input type="text" readonly name ="equip_name" value="PGW_혜화_1"></td>
							</tr>
							<tr>
								<th>Command</th>
								<td><input type="text" readonly  name="return_create_command_sentence" value="CRTE-PGW-GRP:PGWGRP=601,PGW=104,CAPA=100"></td>
							</tr>
							<tr>
								<th>Result Msg</th>
								<td><textarea cols="5" rows="3" name="exec_return_message" readonly wrap="off">400 Bad Request
Required information for the request was missing, 
or the request had syntactical errors, 
or the request contains a malformed access token or 
malformed credentials.
In the present document, this code is also used as 
"catch-all" code for client errors. 
</textarea></td>
							</tr>
						</tbody>	
					</table>
				</div><!-- //result_table -->
				
			</div><!-- //contents -->
			
		</div><!-- //contents_wrap -->
	</div><!-- //container -->
	
	<footer>
		<nav class="footer_nav">
			<ul>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">이용약관</a></li>
				<li><a href="#">개인정보처리방침</a></li>
				<li><a href="#">Sitemap</a></li>
			</ul>
		</nav><!-- //footer_nav -->
		<address>㈜케이티 대표이사 황창규 경기도 성남시 분당구 불정로 90 (정자동) 사업자등록번호 : 102-81-42945 통신판매업신고 : 2002-경기성남-0047</address>
	</footer>
	
</div><!-- //wrap -->

<div class="popup pop_app_connection" id="mme_pop">
	<div class="pop_tit" id="access_title">MME2 접속 승인</div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">
			
			<div class="table_form vTop">
				<table>
					<caption>입력 테이블</caption>
					<colgroup>
						<col span="1" style="width:30%;">
						<col span="1" style="width:*;">
					</colgroup>
					<tbody>
						<tr>
							<th><span>대상 장비</span></th>
							<td id="sys_pop_name">MME_구로_1</td>
						</tr>
						<tr>
							<th><span>EMS 비밀번호<i class="required">필수항목</i></span></th>
							<td><input type="password" name="ems_pwd" value="1234567"></td>
						</tr>
						<tr>
							<th><span>CLI 비밀번호<i class="required">필수항목</i></span></th>
							<td><input type="password" name="cli_pwd" value="1234567"></td>
						</tr>
						<tr>
							<th id="th_cmd_confirm"><span>명령어 Confirm</span></th>
							<td id="td_cmd_confirm"><input type="password" name="confirm_pwd" value="1234567"></td>
						</tr>
					</tbody>
				</table>
			</div><!-- //table_form -->
			
		</div><!-- //scrollbar-inner -->
		
		<div class="btn_area">
			<a href="#" onClick="run_command_first()" class="btn_l btn_co01">확인</a>
		</div>
	</div><!-- //pop_con_wrap -->
	<a href="javascript:void(0);" onclick="init_btn(false);" class="pop_close">팝업닫기</a>
</div><!-- //popup -->
<div class="popup" style="width:300px;" id="preload_pop">
	<div class="pop_tit" id="preload_title">실행중</div>
	<div class="pop_con_wrap">
	<table>
		<tbody>
			<tr>
				<td><marquee direction="right" style="width:200px;">실행중&gt;&gt;&gt;&gt;실행중&gt;&gt;&gt;&gt;실행중&gt;&gt;&gt;&gt;실행중&gt;&gt;&gt;&gt;</marquee></td>
			</tr>
		</tbody>
	</table>
	</div><!-- //pop_con_wrap -->
</div>
</body>
</html>