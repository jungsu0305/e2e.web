<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content= "text/html; charset=UTF-8">
<link rel="shortcut icon" href="/new_resources/resources/images/layout/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="/new_resources/resources/css/style.css">
<script type="text/javascript" src="/new_resources/resources/scripts/utils/jquery-1.12.4.js"></script>
<script type="text/javascript" src="/new_resources/resources/scripts/utils/jquery-ui.js"></script>
<script type="text/javascript" src="/new_resources/resources/scripts/utils/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="/new_resources/resources/scripts/utils/template.js"></script>


<title>사용자 관리</title>
<script>
//alert("${list}");
    function list(page){
    	var f=document.form;
        location.href="user?curPage="+page+"&searchOption=${searchOption}"+"&keyword=${keyword}";
    }
    
    function search(){
    	var f=document.form;
    	var searchOption=f.searchOption.value;
    	var keyword=f.keyword.value;
    	//alert(searchOption +"--"+keyword);
        location.href="user?curPage=1&searchOption=" + searchOption +"&keyword=" +keyword;
    }
    


    
//     $(document).ready(function(){
//         function showTags(node){
//             var children = node.childNodes;
//             for (var i=0; i < children.length; i++){
//                 if (children[i].nodeType == 1 /*Node.ELEMENT_NODE*/){
//                 	  console.log(children[i].tagName);
//                 	  //console.log(children[i].tagName[1].nodeText);
//                 }
//                 showTags(children[i]);
//             }
//         }
       // showTags(document.getElementById("tableA"));
       // var table = document.getElementById("tableA"); 
        
       /*alert(table.getElementsByTagName("td")[20].childNodes[0].nodeValue);
       
       table.getElementsByTagName("td")[20].childNodes[0].nodeValue='dddddd';
       
       alert(table.getElementsByTagName("td")[25].childNodes[0].nodeValue);
       
       table.getElementsByTagName("td")[25].childNodes[0].nodeValue='7774';
        
       alert(table.getElementsByTagName("a")[0].childNodes[0].nodeValue);
       
       table.getElementsByTagName("a")[0].childNodes[0].nodeValue='제목';*/
       
       // table.border = 3; // 속성 설정

//         });
    
  	//모두 체크/모두 미체크의 체크박스 처리
    function changeCheckBox(flag)
    {
    	if(flag)
   		{
    		 $('input:checkbox[name="checkNo"]').each(function() {
    		      this.checked = true; //checked 처리

    		 });


   		}else{
   			
	   		 $('input:checkbox[name="checkNo"]').each(function() {
			      this.checked = false; //checked 처리
			      if(this.checked){//checked 처리된 항목의 값
			            alert(this.value); 
			      }
			 });
   			
   		}
    }
    
    //체크된 로우 삭제처리
    function submitCheckDelete()
    {
    	var f=document.form;
    	checkBoolean=false;
    	
    	if (typeof(f.elements['checkNo']) != 'undefined') 
    	{
    		if (typeof(f.elements['checkNo'].length) == 'undefined')
   			{
    			if(f.elements['checkNo'].checked==true)
   				{
    				checkBoolean=true;
   				}
   			}else{
		        for (i=0; i<f.elements['checkNo'].length; i++)
		        {
		            if (f.elements['checkNo'][i].checked==true)
		            {
		            	checkBoolean=true;
		            	break;
		            }
		        }
   			}
	        
	        if(checkBoolean)
	       	{
	           f.action="/pr/delete/select";
	           f._method.value="DELETE";
	           f.submit();        	
	       	}else{
	       		alert('선택된 내용이 없습니다.');

	       	}

    	}
    	
    }
    
    // 정렬클릭시
    function listOrder(colName,ordOption){
    	var orderKey="";
    	var commaFlag="";
    	orderKey+="(";
  		 $('input:checkbox[name="checkNo"]').each(function() {
  			orderKey+=commaFlag+this.value
  			commaFlag=",";
		 });
  		orderKey+=")";
    	//alert(orderKey);
        $.ajax({
            type: "get",
           // contentType: "json", //==> 생략가능(RestController이기때문에 가능)
            url: "pr/jsondata?colName="+colName
            		  +"&ordOption="+ordOption+"&orderKey="+orderKey,
            success: function(result){

               
                	 var dataObj = JSON.parse(result);
                     var output = "";
                     for(var i in dataObj){

                    	 var myDate = new Date(dataObj[i].reg_date)
                    	 var yyyy= myDate.getFullYear();
                    	 var mm=String(myDate.getMonth() + 1);
                    	 var dd=String(myDate.getDate());
                    	 var h24=String(myDate.getHours());
                    	 var mi=String(myDate.getMinutes());
                    	 mm=(mm.length==1)?"0"+mm:mm;
                    	 dd=(dd.length==1)?"0"+dd:dd;
                    	 h24=(h24.length==1)?"0"+h24:h24;     
                    	 mi=(mi.length==1)?"0"+mi:mi;
                    	 var date = yyyy  + "/" +  mm+ "/" + dd + " " + h24 + ":" + mi;
                         output += "<tr>";
                         output += "<td><input type=checkbox name='checkNo' value='" + dataObj[i].bno +"'></td>";
                         output += "<td>" + dataObj[i].bno +"</td>";
                         output += "<td><a href=\"/board/" + dataObj[i].bno +"\">" + dataObj[i].subject +"</a></td>";
                         output += "<td>" + dataObj[i].writer +"</td>";
                         output += "<td>" + date +"</td>";                
                         output += "<td>" + dataObj[i].hit +"</td>";
                         output += "<tr>";
                        

                     }
                     output += "";

                $("#trData").html(output);
            }
        });
    }
</script>
</head>

<body>
	<form:form commandName="UserComponent" name="form" action="" onsubmit="return false;">
		<div id="wrap">

		    <header>
				<h1 class="logo"><a href="#">KT E2E Infra Orchestrator</a></h1>
				<nav id="gnb">
					<ul>
						<li><a href="#">DESIGN</a></li>
						<li><a href="#">PROVISIONING</a></li>
						<li><a href="#">MONITORING</a></li>
						<li><a href="#">INVENTORY</a></li>
						<li><a href="#">CONFIGURATION</a></li>
						<li><a href="#">P-LTE</a></li>
					</ul>
				</nav><!-- //gnb -->
				<div class="ad_info">
					<i><img src="/new_resources/resources/images/layout/img_ad_info.jpg" alt=""></i><!-- image size : 25*25px -->
					<p>admin</p>
				</div><!-- //ad_info -->
		    </header>
	
		    <div id="container">	
				
				<ul id="navigation">
					<li><img src="/new_resources/resources/images/layout/img_navigation_home.png" alt="home"></li>
					<li>사용자 관리</li>
				</ul><!-- //navigation -->
				
				<div id="contents_wrap">
					<div class="contents">
					
						<div class="search_area">
							<div class="inner">
								<table>
									<caption>검색 테이블</caption>
									<colgroup>
										<col span="1" style="width:15%;">
										<col span="1" style="width:*;">
									</colgroup>
									<tbody>
										<tr>
											<td>
												<select name="searchOption">
													<option value="subject" <c:if test='${searchOption=="subject"}'> selected </c:if> >사용자  ID</option>
													<option value="content" <c:if test='${searchOption=="content"}'> selected </c:if> >사용자 이름</option>
												</select>
											</td>
											<td><input type="text" name="keyword" value="${keyword}"></td>
										</tr>
									</tbody>	
								</table>
								<div class="btn_area">
									<a href="javascript:search()" class="btn_l btn_co01 on">조회하기</a>
								</div>
							</div>
						</div><!-- //search_area -->
						
						<div class="table_ty">
							<div class="table_head">
								<div class="count">Total Count <span>${count}</span></div>
								<div class="btn_area">
									<a href="javascript:submitCheckDelete()" class="btn_l btn_co01">선택탈퇴</a>
								</div>
							</div>
							<table>
								<caption>목록 테이블</caption>
								<colgroup>
									<col span="1" style="width:5%;">
									<col span="1" style="width:*;">
									<col span="6" style="width:10%;">
									<col span="1" style="width:15%;">
								</colgroup>
								<thead>
									<tr>
										<th scope="col">
											<input type=checkbox name="checkALL" value="" onchange="changeCheckBox(this.checked)">
										</th>
										<th scope="col">
											사용자 ID
											<div class="btn_align">
												<a href="javascript:listOrder('id','asc')" class="up">up</a>
												<a href="javascript:listOrder('id','desc')" class="down">down</a>
											</div>
										</th>
										<th scope="col">
											사용자 이름  
											<div class="btn_align">
												<a href="javascript:listOrder('id','asc')" class="up">up</a>
												<a href="javascript:listOrder('id','desc')" class="down">down</a>
											</div>
										</th>
										<th scope="col">
											부서 
											<div class="btn_align">
												<a href="javascript:listOrder('id','asc')" class="up">up</a>
												<a href="javascript:listOrder('id','desc')" class="down">down</a>
											</div>
										</th>
										<th scope="col">
											담당장비 
											<div class="btn_align">
												<a href="javascript:listOrder('id','asc')" class="up">up</a>
												<a href="javascript:listOrder('id','desc')" class="down">down</a>
											</div>
										</th>
										<th scope="col">연락처 </th>
										<th scope="col">권한그룹</th>
										<th scope="col">가입상태</th>
										<th scope="col">
											가입일자
											<div class="btn_align">
												<a href="javascript:listOrder('id','asc')" class="up">up</a>
												<a href="javascript:listOrder('id','desc')" class="down">down</a>
											</div>
										</th>
									</tr>
								</thead>
								<tbody>
<%-- 								<c:forEach var="user" items="${list}"> --%>
<!-- 										<td> -->
<%-- 											<input type=checkbox name="checkNo" value="${user.키값}"> --%>
<!-- 										</td> -->
<%-- 										<td>${user.사용자ID }</td><!-- 사용자ID --> --%>
<%-- 										<td>${user.사용자이름 }</td><!-- 사용자이름 --> --%>
<%-- 										<td>${user.부서 }</td><!-- 부서 --> --%>
<%-- 										<td>${user.담당장비 }</td><!-- 담당장비 --> --%>
<%-- 										<td>${user.연락처 }</td><!-- 연락처 --> --%>
<%-- 										<td>${user.권한그룹 }</td><!-- 권한그룹 --> --%>
<%-- 										<td class="point01">${user.가입상태 }</td><!-- 가입상태 --> --%>
<%-- 										<td>${user.가입일자 }</td><!-- 가입일자 -->					 --%>
<%-- 								</c:forEach> --%>
									<tr>
										<td><input type="checkbox"></td>
										<td>user1</td>
										<td>홍길동</td>
										<td>MME운용부</td>
										<td>MME</td>
										<td>01011112222</td>
										<td>admingrp1</td>
										<td class="point01">승인대기</td>
										<td>2017.12.11 11:00:00</td>
									</tr>
									<tr>
										<td><input type="checkbox"></td>
										<td>user1</td>
										<td>홍길동</td>
										<td>MME운용부</td>
										<td>MME</td>
										<td>01011112222</td>
										<td>admingrp1</td>
										<td class="point01">승인대기</td>
										<td>2017.12.11 11:00:00</td>
									</tr>
									<tr>
										<td><input type="checkbox"></td>
										<td>user1</td>
										<td>홍길동</td>
										<td>MME운용부</td>
										<td>MME</td>
										<td>01011112222</td>
										<td>admingrp1</td>
										<td class="point01">승인대기</td>
										<td>2017.12.11 11:00:00</td>
									</tr>
									<tr>
										<td><input type="checkbox"></td>
										<td>user1</td>
										<td>홍길동</td>
										<td>MME운용부</td>
										<td>MME</td>
										<td>01011112222</td>
										<td>admingrp1</td>
										<td class="point01">승인대기</td>
										<td>2017.12.11 11:00:00</td>
									</tr>
									<tr>
										<td><input type="checkbox"></td>
										<td>user1</td>
										<td>홍길동</td>
										<td>MME운용부</td>
										<td>MME</td>
										<td>01011112222</td>
										<td>admingrp1</td>
										<td class="point01">승인대기</td>
										<td>2017.12.11 11:00:00</td>
									</tr>
									<tr>
										<td><input type="checkbox"></td>
										<td>user1</td>
										<td>홍길동</td>
										<td>MME운용부</td>
										<td>MME</td>
										<td>01011112222</td>
										<td>admingrp1</td>
										<td>가입완료</td>
										<td>2017.12.11 11:00:00</td>
									</tr>
									<tr>
										<td><input type="checkbox"></td>
										<td>user1</td>
										<td>홍길동</td>
										<td>MME운용부</td>
										<td>MME</td>
										<td>01011112222</td>
										<td>admingrp1</td>
										<td>가입완료</td>
										<td>2017.12.11 11:00:00</td>
									</tr>
									<tr>
										<td><input type="checkbox"></td>
										<td>user1</td>
										<td>홍길동</td>
										<td>MME운용부</td>
										<td>MME</td>
										<td>01011112222</td>
										<td>admingrp1</td>
										<td>가입완료</td>
										<td>2017.12.11 11:00:00</td>
									</tr>
									<tr>
										<td><input type="checkbox"></td>
										<td>user1</td>
										<td>홍길동</td>
										<td>MME운용부</td>
										<td>MME</td>
										<td>01011112222</td>
										<td>admingrp1</td>
										<td>탈퇴</td>
										<td>2017.12.11 11:00:00</td>
									</tr>
									<tr>
										<td><input type="checkbox"></td>
										<td>user1</td>
										<td>홍길동</td>
										<td>MME운용부</td>
										<td>MME</td>
										<td>01011112222</td>
										<td>admingrp1</td>
										<td>탈퇴</td>
										<td>2017.12.11 11:00:00</td>
									</tr>
								</tbody>
							</table>
						</div><!-- //table_ty -->
						
				        <div class="paginate">
			                <!-- **처음페이지로 이동 : 현재 페이지가 1보다 크면  [처음]하이퍼링크를 화면에 출력-->
			                <c:if test="${commonPager.curBlock > 1}">
			                    <a class="first" href="javascript:list('1')">[처음]</a>
			                </c:if>
			                
			                <!-- **이전페이지 블록으로 이동 : 현재 페이지 블럭이 1보다 크면 [이전]하이퍼링크를 화면에 출력 -->
			                <c:if test="${commonPager.curBlock > 1}">
			                    <a class="prev" href="javascript:list('${commonPager.prevPage}')">[이전]</a>
			                </c:if>
			                
			                <ul>
			                
			                <!-- **하나의 블럭에서 반복문 수행 시작페이지부터 끝페이지까지 -->
			                <c:forEach var="num" begin="${commonPager.blockBegin}" end="${commonPager.blockEnd}">
			                    <!-- **현재페이지이면 하이퍼링크 제거 -->
			                    <c:choose>
			                        <c:when test="${num == commonPager.curPage}">
			                        	<li class="select"><a href="javascript:list('${num}')">${num}</a></li>
			                        </c:when>
			                        <c:otherwise>
			                        	<li><a href="javascript:list('${num}')">${num}</a></li>
			                        </c:otherwise>
			                    </c:choose>
			                </c:forEach>
			                
			                </ul>
			                
			                <!-- **다음페이지 블록으로 이동 : 현재 페이지 블럭이 전체 페이지 블럭보다 작거나 같으면 [다음]하이퍼링크를 화면에 출력 -->
			                <c:if test="${commonPager.curBlock <= commonPager.totBlock}">
			                    <a class="next" href="javascript:list('${commonPager.nextPage}')">[다음]</a>
			                </c:if>
			                
			                <!-- **끝페이지로 이동 : 현재 페이지가 전체 페이지보다 작거나 같으면 [끝]하이퍼링크를 화면에 출력 -->
			                <c:if test="${commonPager.curPage <= commonPager.totPage}">
			                    <a class="last" href="javascript:list('${commonPager.totPage}')">[끝]</a>
			                </c:if>
				        </div> <!-- //paginate -->
		
						
					</div><!-- //contents -->
				</div><!-- //contents_wrap -->
			</div><!-- //container -->
		
			<footer>
				<nav class="footer_nav">
					<ul>
						<li><a href="#">회사소개</a></li>
						<li><a href="#">이용약관</a></li>
						<li><a href="#">개인정보처리방침</a></li>
						<li><a href="#">Sitemap</a></li>
					</ul>
				</nav><!-- //footer_nav -->
				<address>㈜케이티 대표이사 황창규 경기도 성남시 분당구 불정로 90 (정자동) 사업자등록번호 : 102-81-42945 통신판매업신고 : 2002-경기성남-0047</address>
			</footer>
	
		</div><!-- //wrap -->
	</form:form>
</body>
</html>