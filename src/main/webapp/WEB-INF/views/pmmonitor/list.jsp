<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${paging.lists}" var="i" varStatus="status">
	<tr id="${i.objInstanceId}" style="cursor:pointer;" onclick="selectInstance('${i.objInstanceId}', '${i.objInstanceName}')">
		<td>${i.objInstanceName}</td>
		<td>INSTANTIATED</td>
		<td>${i.cpuUsage}</td>
		<td>${i.memoryUsage}</td>
		<td>${i.tps}</td>
	</tr>
</c:forEach>

<script type="text/javascript">
	$(document).ready(function() {
		setLastAlarmTime("${lastAlarmTime}");
	});
</script>