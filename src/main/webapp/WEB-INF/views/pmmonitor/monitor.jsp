<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%>
<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<title>KT E2E Infra Orchestrator</title>
<link rel="shortcut icon" href="/resources/images/layout/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="/resources/css/style.css">
<script type="text/javascript" src="/resources/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="/resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="/resources/js/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="/resources/js/template.js"></script>

<script src="/resources/scripts/amcharts_3.19.4/amcharts/amcharts.js"></script>
<script src="/resources/scripts/amcharts_3.19.4/amcharts/serial.js"></script>
<script src="/resources/scripts/amcharts_3.19.4/amcharts/pie.js"></script>

<script type="text/javascript" >

	var graphTime = 2;
//초기 동작
	$(document).ready(function() {
		fnInitializeInstance(true);
		var isInit = true;
	});

	var objId = "";
	var objInstanceName = "";
	var isInit = true;
	function selectInstance(objId, objInstanceName) {
		this.objId = objId;
		this.objInstanceName = objInstanceName;
		$("#title").text(objInstanceName + " Performance Graph");
		
		$('#performance_graph').removeClass('screen_no');
		$('#performance_graph').addClass('perform_graph');
		
		$("#today_check").prop("checked", true);
		$("#yesterday_check").prop("checked", true);
		$("#laskweek_check").prop("checked", true);
		
		isInit == true;
		makeTrafficResourceList(objId, objInstanceName, this.isInit);
		initWebsocket();
		//updateTrafficMonitoringData()
	}
	
	// 체크박스 전체선택/해제 기능
	function selectAll() {
		if($('#performance_graph').attr('class') ==  "screen_no")
		{
			alert("선택된 데이터가 없습니다.");
			$("#all_check").prop("checked", false);
			$("#today_check").prop("checked", true);
			$("#yesterday_check").prop("checked", true);
			$("#laskweek_check").prop("checked", true);
		}
		if ($("#all_check").is(":checked")) {
			$("#today_check").prop("checked", true);
			$("#yesterday_check").prop("checked", true);
			$("#laskweek_check").prop("checked", true);
		} else {
			$("#today_check").prop("checked", false);
			$("#yesterday_check").prop("checked", false);
			$("#laskweek_check").prop("checked", false);
		}
		
		makeTrafficResourceList(this.objId, this.objInstanceName, this.isInit);
	}
	
	function selectToday() {
		if($('#performance_graph').attr('class') ==  "screen_no")
		{
			alert("선택된 데이터가 없습니다.");
			$("#today_check").prop("checked", true);
		}
		
		makeTrafficResourceList(this.objId, this.objInstanceName, this.isInit);
	}	

	function selectYesterday() {
		if($('#performance_graph').attr('class') ==  "screen_no")
		{
			alert("선택된 데이터가 없습니다.");
			$("#yesterday_check").prop("checked", true);
		}
		
		makeTrafficResourceList(this.objId, this.objInstanceName, this.isInit);
	}
	
	function selectLastweek() {
		if($('#performance_graph').attr('class') ==  "screen_no")
		{
			alert("선택된 데이터가 없습니다.");
			$("#lastweek_check").prop("checked", true);
		}
		
		makeTrafficResourceList(this.objId, this.objInstanceName, this.isInit);
	}

	
	/**
	 * 페이지 이동
	 */
	function fnPageMove(value) {
		var page = $("input[name=realPage]").attr("value");
		var lastPage = $("input[name=lastPage]").attr("value");
		$("#paging_" + page).removeClass("select");
		
		
		page = Number(page);
		lastPage = Number(lastPage);
		//console.log(page +"/"+lastPage);
		if (value == "first") {
			$("input[name=page]").val(1);
		} else if (value == "before") {
			if (page > 1) $("input[name=page]").val(page-1);
		} else if (value == "next") {
			if (page < lastPage) $("input[name=page]").val(page+1);
		} else if (value == "last") {
			$("input[name=page]").val(lastPage);
		}
		
		page = $("input[name=page]").val();
		$("#paging_" + page).addClass("select");
		
		fnInitializeInstance(true);
		value = null;
		page = null;
		lastPage = null;
	}
	
	function list(page) {
		var realPage = $("input[name=realPage]").val();
		
		$("#paging_" + realPage).removeClass("select");
		$("#paging_" + page).addClass("select");
		
		$("input[name=realPage]").val(page);
		$("input[name=page]").val(page);
		
		fnInitializeInstance(true);
	}
	
	/**
	 * instance 조회
	 */
	function fnInitializeInstance(searchCheckFlag) {

		var param;
		// searchCheckFlag - search 한 이력 체크 후 알맞는 검색 정보를 Setting 해줌
		if (searchCheckFlag) {
			// 직전에 search한 내역을 그대로 넣어준다. (search 한 내역이 없다면 빈 문자열로 setting)
			// 이유 : refresh 되기 직전에 search 박스에 text 넣으면, search 버튼을 누르지 않더라도 해당 text로 검색이되며 refresh 되기 때문
			var searchStatus = $("input[name=searchStatus]").attr("value")
			// 최초 호출 시에는 해당 값을 읽어 올 수 없으므로 빈칸으로 세팅
			if(typeof searchStatus == "undefined"){
				searchStatus = "";
			}
			param = $("#tableForm :input[name!=search]").serialize();
			param += '&search=' + searchStatus;
			searchStatus = null;
		} else {
			// 검색 버튼을 클릭할 경우에는 직전에 선택했던 페이지를 제외하고 1로 setting 해준다
			param = $("#tableForm :input[name!=page]").serialize();
			param += '&page=1';
			$("#lastAlarmTime").val("");
		}

		param += '&lastAlarmTime='+$("#lastAlarmTime").val();
		
		//alert(param);
		
		var result = $.ajax({
			url : "/e2e/pm/list.ajax",
			dataType : "html",
			data : param,
			type : "POST",
			cache : false
		});

		result.done(function(data) {
			if (data != null) {
				//console.log(data);
				$("#instanceList").html(data);
			}
		});

		result.fail(function(xhr, status) {
			console.log(status);
		});

		param = null;
		result = null;
	}
	
	
	
	var refreshTrafficIntervalId;
	function updateTrafficMonitoringData() {
		refreshTrafficIntervalId = setInterval(function() {
			if(this.objId != "")
				makeTrafficResourceList(this.objId, this.objInstanceName, this.isInit);

			clearInterval(refreshTrafficIntervalId);
			refreshTrafficIntervalId = null;
			updateTrafficMonitoringData();
		}, 1000 * 60);
	}
	
	
	var pmTypeAttr = new Object();
	var thresholdArr;
	
	function makeTrafficResourceList(instanceId, objInstanceName, isInit) {

        var serviceSlicesDivId = instanceId+"_serviceSlicesDiv";

        $("#serviceSlicesListDiv>div").hide();

        if ($('#' + serviceSlicesDivId).length === 0) {
            var sHtml = '<div id="'+serviceSlicesDivId+'" style="width:100%;height:100%;"></div>';
            $("#serviceSlicesListDiv").append(sHtml);
            //showLoading("#"+serviceSlicesDivId);
            sHtml = null;
        }
        $('#' + serviceSlicesDivId).show();

        var param = new Object();

        $.ajax({
            url : '/e2e/pm/' + instanceId + '/listPmType.json',
            type : 'POST',
            data : param,
            success : function(data) {
            	makePmTypeGraph(data);
            	data = null;
            },
            error : function(xhr, status, error) {
                //ajaxErrorProc(xhr);
            },
            complete : function() {
                //hideLoading("#" + serviceSlicesDivId);
            }
        });
        
        function makePmTypeGraph(pmTypeAttr) {
	        var graphHtml = "";
	        if(this.isInit) 
        	{
		        for (var i=0; i<pmTypeAttr.length; i++) {
		        	var pmType = pmTypeAttr[i].replace(/\./g,"_") + "_graph";
		        	graphHtml += '<p style="padding-left:20px; padding-top:15px;font-weight:bold">'+pmTypeAttr[i]+'</p><div class="each" id="'+ pmType +'" style="width:100%;height:150px;padding-top:0px;"></div>'
		        }
		        
		        $("#performance_graph").html(graphHtml);
        	}
	        
	        
	        if(pmTypeAttr.length == 0)
        	{
	        	this.window.resizeTo(800, 760);
        	}
	        else if(pmTypeAttr.length == 1)
        	{
	        	this.window.resizeTo(800, 760);
        	}
	        else if(pmTypeAttr.length == 2)
        	{
	        	this.window.resizeTo(800, 910);
        	}
	        else if(pmTypeAttr.length == 3)
        	{
	        	this.window.resizeTo(800, 1060);
        	}
	        $.ajax({
	            url : '/e2e/pm/' + instanceId + '/'+ instanceId +'/resource/threshold.json',
	            type : 'POST',
	            data : param,
	            success : function(data) {
	            	thresholdArr = data;
	            	data = null;
	            },
	            error : function(xhr, status, error) {
	                ajaxErrorProc(xhr);
	            },
	            complete : function() {
	                //hideLoading("#" + serviceSlicesDivId);
	            }
	        });
	        
	        $.ajax({
	            //url : '/e2e/pm/' + instanceId + '/'+ objInstanceName +'/resource.json',
	            url : '/e2e/pm/' + instanceId + '/'+ instanceId +'/resource.json',
	            type : 'POST',
	            data : param,
	            success : function(data) {
	                successResource(data, isInit);
	            },
	            error : function(xhr, status, error) {
	                //ajaxErrorProc(xhr);
	            },
	            complete : function() {
	                //hideLoading("#" + serviceSlicesDivId);
	            }
	        });
        }
        function successResource(resourceData, isInit) {
        	if (resourceData.length == 0) {
        		$('#performance_graph').removeClass('perform_graph');
        		$('#performance_graph').addClass('screen_no');
            	$("#performance_graph").html('<div class="inner"><p class="txt">Graph 데이터가 없습니다.</p></div>');
            } else {

            	/* 화면 chart 중에서 더이상 정보가 존재하지 않는 metric 의 chart 를 remove */
            	/* var fullLen = $("#serviceSlicesListDiv .service_slice").length;
            	for(var i=fullLen; i>0; i--){
					var metricDivId = $("#serviceSlicesListDiv .service_slice:nth-child("+i+")").attr("id");
					var isExist = false;
					for(var j=0; j<resourceData.length;j++){
						var simpleMetricId = resourceData[j].metricsId.replace(/\./g, '_');
						if(metricDivId.indexOf(simpleMetricId) !== -1){
							isExist = true;
						}
					}
					if(!isExist){
						$("#serviceSlicesListDiv .service_slice:nth-child("+i+")").empty();
					}
					metricDivId = null;
					isExist = null;
				} */

            	var resourceDataLength = resourceData.length;
            	//console.log( " resourceDataLength >> " + resourceDataLength);
                for (var i=0;i<resourceData.length;i++){
                    var serviceChain = resourceData[i];
                    // div ID 에 '.'이 들어갈 경우, 셀렉터로 확인할 수 없음
                    serviceChain.simpleMetricsId =  serviceChain.metricsId.replace(/\./g, '_');
					
/*                     var data = {
                            serviceSlicesDivId : serviceSlicesDivId,
                            serviceChain : serviceChain,                             sNumber : (function() {
                                var num = i + 1;
                                var sNumber = "";
                                if (num < 10) {
                                    sNumber += "0";
                                }
                                sNumber += num;

                                return sNumber;
                            }()),
                            hitCount : (function() {
                                var hitCount = null;
                                if (g_serviceChainUsage != null) {
                                    hitCount = g_serviceChainUsage[serviceChain.sni_name];
                                }
                                if (hitCount == null) {
                                    hitCount = "-";
                                }

                                return hitCount;
                            }())
                    }; */

/*                      if ($("#"+serviceChain.simpleMetricsId+"_graph").length === 0) {
                        var srcTrafficMonitor = $("#vnfc-monitor-template").html();
                        var templateTrafficMonitor = Handlebars.compile(srcTrafficMonitor);
                        var sHtml = templateTrafficMonitor(data);
                        if(i == 0){
	                        $("#"+serviceSlicesDivId).html(sHtml);
                        } else {
                        	isInit = true;
	                        $("#"+serviceSlicesDivId).append(sHtml);
                        }
                        srcTrafficMonitor = null;
                    	templateTrafficMonitor = null;
                    	sHtml = null;
                    }
                    $("#"+serviceSlicesDivId+"_"+serviceChain.metricsId).data(serviceChain); */

                    var chartDatas = [];
                    var maxValue = 0;
                    for (var j = 0; j < serviceChain.timeLinePerformanceDatas.length; j++) {
                        var entryTimeLine = serviceChain.timeLinePerformanceDatas[j];

                        var cData = {};
                        //var addable = false; // 해당시간에 데이터가 없으면  표시하지 않는다.
                        cData["REG_DTS"] = entryTimeLine.metricTimestamp;

						for (var k = 0; k < entryTimeLine.performanceDatas.length; k++) {
							var entry = entryTimeLine.performanceDatas[k];

							if ($("#today_check").is(":checked")) {
								cData["Today"] = entry.value;
							} 
							if ($("#yesterday_check").is(":checked")) {
								cData["Yesterday"] = entry.yesterdayVal;
							} 
							if ($("#laskweek_check").is(":checked")) {
								cData["Last Week"] = entry.lastWeekVal;
							}
/* 							if ($("#today_check").is(":checked")) {
								cData[entry.instanceName] = entry.value;
							} 
							if ($("#yesterday_check").is(":checked")) {
								cData[entry.instanceName + "_전일"] = entry.yesterdayVal;
							} 
							if ($("#laskweek_check").is(":checked")) {
								cData[entry.instanceName + "_전주"] = entry.lastWeekVal;
							} */

							var timeLineMax = Math.max(entry.value, entry.yesterdayVal, entry.lastWeekVal);
							//var timeLineMax = Math.max(entry.value, entry.lastWeekVal);

							if (entry.metricId.indexOf("tps") != -1) {
								maxValue = Math.max(maxValue, timeLineMax);
							} else {
								maxValue = 100;
							}

							timeLineMax = null;
							entry = null;
						}
						chartDatas.push(cData);

                        timeLineMax = null;
                        cData = null;
                        entryTimeLine = null;
                    }

                    showTrendChart(serviceChain.simpleMetricsId+"_graph", chartDatas, isInit, maxValue, serviceChain.metricsId);
                    
                    this.isInit = false;

				 	chartDatas = null;
					maxValue = null;
                }

                fullLen = null;
                resourceDataLength = null;
            }
        }
        
        param = null;
    }
        
        
        
    var g_trendChart = {};

    function showTrendChart(divId, trendDatas, isInit, maxValue, metricId) {
    	//g_trendChart = {};
    	
    	if(this.isInit == true){
    		g_trendChart = {};
    		isInit = false;
    	}

        var graphConfigs = [];
        // 데이터 없을 경우 반복적으로 null 에 의한 에러 나오므로, 에러요소 제거
        if(trendDatas[0] == null ){
        	return;
        }

		Object.keys(trendDatas[0]).forEach(function(entry, index) {
			if (entry !== 'REG_DTS') {
				var grCfg = {
					"balloonText" : "[[title]] : [[value]]",
					"fillAlphas" : 0.2,
					"id" : "AmGraph-" + (index + 1),
					"lineAlpha" : 1,
					"lineThickness" : 2,
					"title" : entry,
					"valueField" : entry
				};
				graphConfigs.push(grCfg);
			}
		});

		var configs = {
			"type" : "serial",
			"categoryField" : "REG_DTS",
			"dataDateFormat" : "HH:NN:SS",
			"colors" : [
				"#32bbff",
				"#8ff7a7",
				"#E5D85C",
				"#5dcbc0",
				"#b4dbee",
				"#c0bfd7",
				"#BDBDBD",
				"#32bbff",
				"#8ff7a7",
				"#E5D85C",
				"#5dcbc0",
				"#b4dbee",
				"#c0bfd7",
				"#BDBDBD"
			],
			"marginLeft" : 0,
			"marginRight" : 0,
			"marginBottom" : 0,
			"marginTop" : 0,
			"categoryAxis" : {
				"minPeriod" : "ss",
				"parseDates" : true,
				"startOnAxis" : true,
				"equalSpacing" : true,
				"markPeriodChange" : false,
				"axisColor" : "#8b9099",
				"dashLength" : 0,
				"gridThickness" : 0,
				"ignoreAxisWidth" : true,
				"color" : "#333333",
				"tickLength" : 0,
				"boldLabels" : true,
				"labelOffset" : 0,
				"fontSize" : 0,
				"inside" : true,
				"minHorizontalGap" : 10,
				"minVerticalGap" : 20
			},
			"chartCursor" : {
				"categoryBalloonDateFormat" : "JJ:NN:SS",
				"bulletsEnabled" : true,
				"zoomable" : false,
				"zooming" : false,
				"cursorColor" : "#104553"
			},
			"trendLines" : [],
			/* "graphs" : [
				{
					"balloonText" : "[[title]] : [[value]]",
					"fillAlphas" : 0.4,
					"id" : "AmGraph-1",
					"title" : "RX",
					"valueField" : "RX"
				},
				{
					"balloonText" : "[[title]] : [[value]]",
					"fillAlphas" : 0.4,
					"id" : "AmGraph-2",
					"title" : "TX",
					"valueField" : "TX"
				}
			], */
			"guides" : [],
			"valueAxes" : [
				{
					// "gridAlpha": 1,
					// "dashLength": 3,
					// "ignoreAxisWidth": true,
					// "offset": 1,
					// "gridThickness": 0,
					// "tickLength": 4,
					"minimum" : 0,
					"inside" : true
				}
			],
			"allLabels" : [],
			/* "balloon" : {
				"borderThickness" : 0,
				"color" : "#FFFFFF",
				"fillColor" : "#141937",
				"shadowAlpha" : 0,
				"fontSize" : 2,
				"pointerWidth" : 5
			}, */
			"titles" : [],
			"graphTime" : 24,
			"dataProvider" : trendDatas
		};

		configs["graphs"] = graphConfigs;
		configs["dataDateFormat"] = "HH:NN:SS";
		configs["chartCursor"].categoryBalloonDateFormat = "JJ:NN:SS";

		configs["legend"] = {
			"color" : "#AAAAAA",
			"position" : "right",
			"rollOverColor" : "#3D4159",
			"valueWidth" : 0,
			"labelWidth" : 120,
			"markerSize" : 10,
			"markerLabelGap" : 8,
			"autoMargins" : false,
			"marginRight" : 30,
			"marginLeft" : 30,
			"marginBottom" : 0,
			"verticalGap" : 6,
			"maxColumns" : 100
		};

        configs["graphs"] = graphConfigs;
        configs["dataDateFormat"] = "HH:NN:SS";
        configs["chartCursor"].categoryBalloonDateFormat = "JJ:NN:SS";

   		// VNFC 상세 traffic chart - 표시해줘야 하는 threshold 만 분류해서 저장함
      	var guideLineArr = new Array();
		var guideConfigArr = new Array();
        var maximumValue = 0;

      	var guideLineValue = 0;
   		var titleValue;
		if(divId.indexOf("chartdiv_slice_vnfc") !== -1){
			if(divId.indexOf("tps") == -1 && divId.indexOf("network") == -1){
       			maximumValue = 100;
   			}
       		for(var i = 0; i < thresholdArr.length; i++){
       			var threshold = thresholdArr[i];
    			if(vnfcName.indexOf(threshold.vnfcName) != -1 && divId.indexOf(threshold.metricId) !== -1){
    				// VNFC 상세 Performance 정보 중 tps, network 는 그래프 폭(maximum) 변동으로 설정
		   			// 이외 metric 정보는 % 값을 가지므로 100 고정
    				if(divId.indexOf("tps") != -1 || divId.indexOf("network") != -1){
	    				if(maxValue > (threshold.value + (threshold.value/4))){
							maximumValue = maxValue;
		       			} else {
		       				maximumValue = threshold.value + (threshold.value/4);
		       			}
		   			}
    				var guideLineObj = new Object();
   					guideLineObj.id = threshold.metricId;
   					guideLineObj.value = threshold.value;
   					guideLineArr.push(guideLineObj);
    				//titleValue = threshold.metricId;
   					guideLineObj = null;
    			}
    			threshold = null;
    		}
		} else {
			configs.categoryAxis.minPeriod = "10ss";

			configs["valueAxes"][0]["minimum"] = 0;
			configs["valueAxes"][0]["maximum"] = maxValue;


			maximumValue = maxValue;
			//titleValue="tps,network";
			for(var i = 0; i < thresholdArr.length; i++){
       			var threshold = thresholdArr[i];
       			if(metricId == threshold.metricId){
					var guideLineObj = new Object();
					guideLineObj.id = threshold.metricId;
					if(threshold.metricId.indexOf("network") != -1){
						guideLineObj.value = changeByteUnit(threshold.value);
					} else {
						guideLineObj.value = threshold.value;
					}
					//console.log("set Obj  >>>>> " + guideLineObj.id);
					guideLineArr.push(guideLineObj);
					if (maximumValue < (guideLineObj.value + (guideLineObj.value / 4))) {
						maximumValue = guideLineObj.value + (guideLineObj.value / 4);
					}
					guideLineObj = null;
       			}
       			threshold = null;
    		} 
		}

		if(guideLineArr.length > 0){
	      	for(var i = 0; i < guideLineArr.length; i++){
	      		var guideLineObj = guideLineArr[i];
		    		var guideConfig = {
		    			"dashLength": 6,
		      	        "inside": true,
		      	        //"label": guideLineObj.id + " threshold : " + guideLineObj.value,
		      	        "lineAlpha": 1,
		      	        "value": guideLineObj.value,
		      	      	"metricId": guideLineObj.id,
		      	        "color" : "#FF2424",
			   			"lineColor": "#FF2424",
			   			"lineThickness": 1,
			   			//"fillAlpha": 0.1,
			   			"fillColor": "#FF2424"
		    		};
		    		guideConfigArr.push(guideConfig);
	      	}
	      	configs["valueAxes"][0]["guides"] = guideConfigArr;
      	}
      	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      	
		// 현재 상태의 maximum value 세팅은 가장 마지막에 한다.
		if(maximumValue > 0){
	   		configs["valueAxes"][0]["maximum"] = maximumValue;
		}

   		if (Object.keys(g_trendChart).indexOf(divId) !== -1) {
            // changed data check
            var changed = false;

            // get original maximum value
            var originMaxValue = 0;
       		if(typeof g_trendChart[divId].valueAxes[0].maximum != "undefined"){
       			originMaxValue = g_trendChart[divId].valueAxes[0].maximum;
       		}

       		if (g_trendChart[divId].graphTime != graphTime){
				g_trendChart[divId].graphTime = graphTime;
				/* if(graphTime == 24) {
					g_trendChart[divId].categoryAxis.minPeriod = "30mm";
				} else {
					g_trendChart[divId].categoryAxis.minPeriod = "mm";
				} */
				g_trendChart[divId].categoryAxis.minPeriod = "10ss";
				changed = true;
			}

       		// 이미 설정된 Max 값과 신규 Max 값이 다르다면 신규 Max 값으로 변경해준다.
    		if(maximumValue != originMaxValue){
      			g_trendChart[divId].valueAxes[0].maximum = maximumValue;
				changed=true;
    		}

       		// 만약 max value 가 다르고, guide line 기준 max value 보다 크다면 maximum value 변경해준다.
       		//if(typeof g_trendChart[divId].valueAxes[0].guides[0] != "undefined"){
       		if(typeof g_trendChart[divId].valueAxes[0].guides != "undefined"){
       			if(g_trendChart[divId].valueAxes[0].guides.length != guideLineArr.length){
     					g_trendChart[divId].valueAxes[0].guides = guideConfigArr;
       				changed = true;
       			} else {
       				// 현재 설정된 값과 신규 값을 비교하여 guide line 업데이트 실행 여부 확인
					for(var i = 0; i < guideLineArr.length; i++){
						var checkFlag = false;
	       				for(var j = 0; j < g_trendChart[divId].valueAxes[0].guides.length; j++){
	       					if(guideLineArr[i].id == g_trendChart[divId].valueAxes[0].guides[j].metricId
	       							&& guideLineArr[i].value == g_trendChart[divId].valueAxes[0].guides[j].value){
	       						checkFlag = true;
	       					}
    	       			}
	       				if(!checkFlag){
	       					g_trendChart[divId].valueAxes[0].guides = guideConfigArr;
	       					changed = true;
	       				}
	       				checkFlag = null;
					}
       			}
       		} else if(guideLineArr.length > 0){
				g_trendChart[divId].valueAxes[0].guides = guideConfigArr;
				changed = true;
       		}

    		if(typeof g_trendChart[divId].valueAxes[0].guides != "undefined"){
       			if(g_trendChart[divId].valueAxes[0].guides.length != guideLineArr.length){
     					g_trendChart[divId].valueAxes[0].guides = guideConfigArr;
       				changed = true;
       			} else {
       				// 현재 설정된 값과 신규 값을 비교하여 guide line 업데이트 실행 여부 확인
					for(var i = 0; i < guideLineArr.length; i++){
						var checkFlag = false;
	       				for(var j = 0; j < g_trendChart[divId].valueAxes[0].guides.length; j++){
	       					if(guideLineArr[i].id == g_trendChart[divId].valueAxes[0].guides[j].metricId
	       							&& guideLineArr[i].value == g_trendChart[divId].valueAxes[0].guides[j].value){
	       						checkFlag = true;
	       					}
    	       			}
	       				if(!checkFlag){
	       					g_trendChart[divId].valueAxes[0].guides = guideConfigArr;
	       					changed = true;
	       				}
	       				checkFlag = null;
					}
       			}
       		} else if(guideLineArr.length > 0){
				g_trendChart[divId].valueAxes[0].guides = guideConfigArr;
				changed = true;
       		} else if(guideLineArr.length < 1){
       			// guide line 제거
       			g_trendChart[divId].valueAxes[0].guides = [];
       			
				if (divId.indexOf("tps") != -1) {
					g_trendChart[divId].valueAxes[0].maximum = Math.max(maxValue, maxValue);
				} else {
					g_trendChart[divId].valueAxes[0].maximum = 100;
				}
                changed = true;
       		}

            var prevDatas = g_trendChart[divId].dataProvider;
            if (JSON.stringify(g_trendChart[divId].dataProvider) !== JSON.stringify(trendDatas)) {
                g_trendChart[divId].dataProvider = trendDatas;
                changed = true;
            }
            if (changed) {
                g_trendChart[divId].validateData();
            }

            changed = null;
            originMaxValue = null;
            prevDatas = null;
        } else {
            g_trendChart[divId] = AmCharts.makeChart(divId, configs);
        }

   		divId = null;
   		trendDatas = null;
   		//isInit = null;
   		maxValue = null;
   		graphConfigs = null;
   		configs = null;
   		guideLineArr = null;
   		guideConfigArr = null;
   		maximumValue = null;
    }        
    
    
	//####################################################
	//	Websocket 함수
	//####################################################

	//init Websocket
	var disconnWebsocket;
	var websocket = null;
	function initWebsocket() {
		var wsUri = "ws://" + "${e2eWebsocketAddress}" + "/websocket";

		// 재접속 요청시 중복 연결을 방지하기 위해 기존 Websocket close
		if (websocket != null) {
			websocket.close();
		}

		websocket = new WebSocket(wsUri);
		websocket.onopen = function(evt) {
			onOpen(evt)
		};
		websocket.onmessage = function(evt) {
			onMessage(evt)
		};
		websocket.onerror = function(evt) {
			onError(evt)
		};
		websocket.onclose = function(evt) {
			onClose(evt);
		};

		function onOpen(evt) {
			//clearInterval(disconnWebsocket);
			disconnWebsocket = null;
			//fnLoadingTry(false);
			writeToSocketStatus("websocket connected!");
		//doSend(textID.value);
		}
		function onMessage(evt) {
			writeToMonitor(evt.data);
		}
		function onError(evt) {
			writeToSocketStatus('ERROR: ' + evt.data);
		}
		function onClose(evt) {
			//fnLoadingTry(true, "VNFM Dashboard client VNFM-websocket not connected!", "VNFM");
		}	
		function doSend(message) {
			writeToSocketStatus("Message Sent: " + message);
			websocket.send(message);
		}
		function writeToSocketStatus(message) {
			console.log(" >> " + message);
		}
		function writeToMonitor(message) {
			var obj = JSON.parse(message);
			if (obj.type == "performance") {
				fnInitializeInstance(true);
				makeTrafficResourceList(this.objId, this.objInstanceName, this.isInit);
			}
		}
	}
	
	function setLastAlarmTime(timestamp){
		$("#lastAlarmTime").val(timestamp);
		timestamp = null;
	}  
</script>

</head>

<body class="wpop">
<div class="win_pop pop_perform_graph"><!-- 180119 수정 -->
	<div id='title' class="pop_tit">Performance Graph</div>
	<div class="pop_con_wrap">
		<form id="tableForm" name="tableForm" method="post">
		<input type="hidden" name="page" value="1" />
		<ul class="chk_board">
			<li><label><input id="all_check" type="checkbox" onclick="selectAll()">All</label></li>
			<!-- <li><label><input type="checkbox">MAX</label></li> -->
			<li><label><input id="today_check" type="checkbox" checked="checked" onclick="selectToday();">Today</label></li>
			<li><label><input id="yesterday_check" type="checkbox" checked="checked" onclick="selectYesterday();" >Yesterday</label></li>
			<li><label><input id="laskweek_check" type="checkbox" checked="checked" onclick="selectLastweek();">Last Week</label></li>
		</ul><!-- //chk_board -->
		<div id="performance_graph" class="screen_no">
			<div class="inner">
				<p class="txt">No Item.</p>
			</div>
		</div><!-- //screen_no -->
		<div class="table_ty scroll_equipment scrollbar-inner">
			<table>
				<caption>목록 테이블</caption>
				<colgroup>
					<col span="1" style="width:20%;">
					<col span="1" style="width:20%;">
					<col span="1" style="width:20%;">
					<col span="1" style="width:20%;">
					<col span="1" style="width:20%;">
				</colgroup>
				<thead>
					<tr>
						<th scope="col">Name</th>
						<th scope="col">Inst. Status</th>
						<th scope="col">CPU</th>
						<th scope="col">Memory</th>
						<th scope="col">TPS</th>
					</tr>
				</thead>
				<tbody id="instanceList">
				</tbody>
			</table>
		</div><!-- //table_ty -->
		<div class="paginate" id="paging">
			<a href="javascript:fnPageMove('first');" class="first">처음 페이지</a>
			<a href="javascript:fnPageMove('before');" class="prev">이전 페이지</a>
			<ul id="paging_ul">

                 <%-- <c:forEach var="num" begin="1" end="3"> --%>
                 <c:forEach var="num" begin="${paging.page}" end="${lastPage}">
                    <c:choose>
                        <c:when test="${num == paging.page}">
                            <li id="paging_${num}" class="select"><a href="#" onClick="list('${num}')">${num}</a></li>
                        </c:when>
                        <c:otherwise>
                            <li id="paging_${num}"><a href="#" onClick="list('${num}')">${num}</a></li>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>			

			</ul>
			<a href="javascript:fnPageMove('next');" class="next">다음 페이지</a>
			<a href="javascript:fnPageMove('last');" class="last">마지막 페이지</a>
		</div><!-- //paginate -->
		
		<div class="btn_area">
			<a href="javascript:self.close();" class="btn_l btn_co01">확인</a>
		</div>
		</form>
	</div><!-- //pop_con_wrap -->
</div><!-- //popup -->

<input type="hidden" id="lastAlarmTime" name="lastAlarmTime" value="" />
<input type="hidden" name="searchStatus" value="${searchStatus}" />
<input type="hidden" name="listSize" value="${fn:length(paging.lists)}" />
<input type="hidden" name="lastPage" value="${lastPage}" />
<input type="hidden" name="realPage" value="${paging.page}" />
<input type="hidden" name="totalcount" value="<fmt:formatNumber value="${paging.totalCount}" pattern="#,###"/>" />
</body>
</html>