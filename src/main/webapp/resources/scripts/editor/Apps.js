

e2e.Application = Class.extend({
    NAME : "e2e.Application",
    /**
     * @constructor
     * 
     */
    init : function(id, attr) 
    {
    	this.canvasId   = id;
    	
    	if(attr != null && attr) this.isReadOnly = true;
    	else                     this.isReadOnly = false;
    	
    	this.zoomEnabled = true;
    	
    	
    	this.setConfiguration('always');
    	
        this.view = new e2e.Canvas(this); /* canvas id in body(html) */
        this.viewEvent = new e2e.CanvasEvent(this.view);
        this.reader = new e2e.Reader();
        this.convertor = new e2e.Convertor();
        this.components = [];
        
        this.setConfiguration('connection');
        //this.setConfiguration('portDisappear');
        this.setConfiguration('readOnly');
        
    },

    //MUST BE CALLED AFTER configure 'component-container' html
    loadingComponent: function(id) {
    	this.containerId = id;
    	this.container = new e2e.Container(this.containerId, this.canvasId, this.view, this.components);
//    	console.info('loading component completed');
    },

    
    //configuration
    setConfiguration:function(c) {
    	var _this = this;
    	switch(c) {
    	case 'always':
    		
    		//for Connection-initiation
    		draw2d.Configuration.factory.createConnection = function(sourcePort, targetPort) {
	    		return new e2e.policy.Connection({
	    			source:sourcePort,
	    			target:targetPort
	    		});
	    	};
//	    	console.log('loading policy for createConnection');
	    	
	    	//for Port
	    	draw2d.Configuration.factory.createHybridPort = function(relatedFigure) {
	    		return new e2e.policy.Port({isReadOnly:_this.isReadOnly});
	    	};
//	    	console.log('loading policy for port');
	    	
	    	break;
    	case 'connection': //useful on draw2d.v6 libraries
    		this.view.installEditPolicy(new draw2d.policy.connection.DragConnectionCreatePolicy({
    			createConnection:function() {
    		    	return new e2e.policy.Connection();
    		    }
    		}));
//    		console.log('loading policy for dragConnection');
    		break;
	    	
    	case 'readOnly':
    		
    		if(!this.isReadOnly) return;
    		
    		this.view.installEditPolicy(new e2e.policy.Canvas.ReadOnly());
//    		console.log('loading policy for readOnly in canvas:: readOnly enabled');
    		break;
    		
    	case 'portDisappear':
    		this.setPortDisappearPolicy();
	    	break;
    	}
    },
    
    setPortDisappearPolicy:function() {
    	/* Port가.....*/
    	this.view.installEditPolicy(new draw2d.policy.canvas.CoronaDecorationPolicy({
    		diameterToBeVisible:100,
    		diameterToBeFullVisible:10
    	}));
//    	console.log('loading policy for port disappear:visible:100, FullVisible:10');
    },
    
    getArrowButton:function(type, link) {//next or prev
    	var attr = {path:'/images/layout/btn_'+type+'.png', width:70, height:70};
    	var hpath= '/images/layout/btn_'+type+'_on.png';
    	//../images/contents/btn_act02_on.png

    	var img = new draw2d.shape.basic.Image(attr);
    	img.on('click', function() { img.parent.fireEvent('click'); });
    	img.onMouseEnter = function() { img.setPath(hpath); }
    	img.onMouseLeave = function() { img.setPath(attr.path); }
    	
    	var btn = new draw2d.shape.basic.Circle({width:75,height:75,bgColor:'#ffffff'});
    	btn.add(img, new draw2d.layout.locator.CenterLocator());
    	btn.on('click', function() { location.href = link; });
    	
    	btn.installEditPolicy(new draw2d.policy.figure.VerticalEditPolicy());
		btn.installEditPolicy(new draw2d.policy.figure.HorizontalEditPolicy());
		btn.installEditPolicy(new draw2d.policy.figure.GlowSelectionFeedbackPolicy());
		
		return btn;
    },
    
    resizeCanvas:function(w,h) {
    	var rx = w/this.view.width();
    	var ry = h/this.view.height();
    	var r;
    	
    	if(rx < ry) r = ry;
    	else        r = rx;
    	
//    	console.log(w,h,rx,ry,r);
    	if(r !== 0) this.view.setZoom(r, true);
    	else        this.view.setZoom(1.0, true);
    },
    
    LEVEL:['','_normal','_minor','_major','_warning','_critical'],
    level:{
    	Default:0,
    	Normal:1,
    	Minor:2,
    	Major:3,
    	Warning:4,
    	Critical:5,
    },
    findIcon:function(key, level) {
		//find node
		var figures = app.view.figures;
		var node = figures.data.filter(function(e) {
			//TODO found with name 1st
			return e.NAME === 'e2e.component.Node'
				&& typeof e.name !== 'undefined' 
				&& e.name === key;			
		});
		
		if(node.length === 0) {
			alert("couldn't found your icon in canvas");			
			return;
		}
		
		//TODO 1st node only...
		var subfigures = node[0].getChildren();
		var icon = subfigures.data.filter(function(e) {
			return e.NAME === 'e2e.component.Icon' && typeof e.path !== 'undefined';
		});
		
		//set path
		var newPath = '/images/contents/' + key + this.LEVEL[level] + '.svg';
		node[0].path = newPath;
		icon[0].setPath(newPath);
    }
    
});


e2e.Canvas = draw2d.Canvas.extend({
	init:function(_this){
		this.id = _this.canvasId
		this.zoomEnabled = _this.zoomEnabled;
		
		this._super(this.id);
		this.clippboardFigure=null;
        //this.grid = new draw2d.policy.canvas.ShowGridEditPolicy(20);

        //this.installEditPolicy( this.grid);
//        this.installEditPolicy( new draw2d.policy.canvas.FadeoutDecorationPolicy());
//        this.installEditPolicy( new draw2d.policy.canvas.SnapToGeometryEditPolicy());
//        this.installEditPolicy( new draw2d.policy.canvas.SnapToCenterEditPolicy());
//        this.installEditPolicy( new draw2d.policy.canvas.SnapToInBetweenEditPolicy());
        
		this.setScrollArea("#"+this.id);
	},	
	
	setZoom:function(v,b) {
		if(!this.zoomEnabled) return;
		
		if(v === 1) this._super(v,b);
		else        this._super(this.getZoom()*v, b);

	},
	/**/
	width:function() {
		return $("#"+this.id).width();
	},
	
	height:function() {
		return $("#"+this.id).height();
	},
	/**/
	
	resize:function(win, hin) {
		//resize
		var w,h;
		if(win !== undefined) w = win; 
		else                  w = $("#"+this.id).width();
		if(hin !== undefined) h = hin;
		else                  h = $("#"+this.id).height();
		
		//console.log('canvas! width='+w+', height='+h);
		//console.log('initial width='+this.initialWidth+', height='+this.initialHeight);
		
		this.initialWidth = w;
		this.initialHeight = h;
		this.paper.setSize(w,h);
		
		//set constraint
		var newConst     = new draw2d.policy.figure.RegionEditPolicy(0,0,w,h);
		var currentConst = this.regionDragDropConstraint;
		
		//set in canvas
		this.regionDragDropConstraint = newConst;
		
		//set in all figures in canvas
		this.getFigures().each(function(i,f) {
			if( f!== null) {
				f.uninstallEditPolicy(currentConst);
				f.installEditPolicy(newConst);
			}
		});		
	},
});

e2e.CanvasEvent = Class.extend({
	init:function(canvas) {
		
		canvas.on('figure:add', function(emitter, event) {
			//console.log('canvas]figure:add event:', event.figure);
			
			var f = event.figure;
			//only if EdgeCloud
			if(f.isBack != null && !f.isBack) canvas.toBackFigure();
			
		});
		
		canvas.on('click', function(emitter, event) {
			//console.log('canvas]click event:',emitter, event);
		});
		
		canvas.installEditPolicy(new draw2d.policy.canvas.CanvasPolicy({
			onClick:function(figure) {
				if(figure != null) {
					//if(figure.backCnt != null) figure.toBack();
					//trick
					//console.log('backCnt:' + figure.backCnt + ", zorder:" + figure.getZOrder());
				}
			}
		}));
		
		canvas.toBackFigure = function() {
			var figures = this.getFigures();
	        figures.each(function(i,e){
	        	if(e.isBack) e.toBack();
	        });
		};
		
		canvas.getCommandStack().addEventListener(function(e){
		    if(e.isPostChangeEvent()){
//		    	console.log("icon moving");
		    // displayJSON(canvas); OR call your resize-handler-method
		    }
		});
	},
});

e2e.Container = Class.extend({
	NAME: "e2e.Container",
	init:function(id, target, view, components)
	{
		this.view = view;
		var _this = this;
		
		this.edgeCnt = 0;
		this.centralCnt = 0;

		//Draggable
		$("#" + id).draggable({
			appendTo:'body',
			containment:'body',
			//helper:"clone",
			helper:function(){ 
				return $("<img src='" + this.dataset.path + "' style='width:52px;height:52px;'>"); 
			},
			grid:[20,20],
			cursorAt:{top:1,left:1},
			stop:function(e,ui) {
				e = view._getEvent(e);
				
				var pos = view.fromDocumentToCanvasCoordinate(e.clientX, e.clientY);
				var component = _this.getComponent(components, e.target);

				if(component !== null) {
					view.add(component, pos.getX(), pos.getY());
				} else {
					//TODO check if error, logging 1st
					console.warn('error : icon is null, invalid param');
				}
				//console.log(component);
				return e;
			}
		});
	},
	
	getComponent: function(components, target) { // 드래그후
		var _view = this.view;
		var obj = null;

		return components.some(function(e) {
			if(e.name === target.dataset.id && e.type === target.dataset.type) {
				switch(e.name) {
				case 'Central': obj = new e2e.component.Cloud.Central({offset:this.centralCnt++}); break;
				case 'Edge': obj = new e2e.component.Cloud.Edge({offset:this.edgeCnt++}); break;
				default: // attr에 vfndId, vnfdName 추가 2018.01.23 최정수
					var attr = {name:e.name, path:e.path, userData:e, vnfdId:e.vnfd_id, vnfdName:e.vnfd_name};
					obj = new e2e.component.Node(attr);
				}
				
				return target.dataset.id;
			}
		}) ? obj : null;
		console.log(obj);
	},
});

/*********************************************************************************************
 * READER for unmarshal
 ********************************************************************************************/
e2e.Reader = Class.extend({
	NAME:"e2e.Reader",
	init:function() {
		;
	},
	custom_eval:function(v) {
		if(window.execScript) {
			return window.execScript("("+v+")");
		}
		return eval(v);
	},
	unmarshal:function(view, json) {
		var canvas = view;
		var _this = this;
		if(typeof json === "string") {
			json = JSON.parse(json);
		}
		
		console.debug('Reader]unmarshal.json:',json);
		/**/
		$.each(json, $.proxy(function(i, e) {
			try {
				var o = _this.createFigureFromType(e.type);
				//set Node or Cloud
				////
				// Node
				//     x|y|path|name|iconWidth|iconHeight|bgColor > userData
				// Cloud
				//     x|y|width|height
				///
				//set Connection
				////
				// Connection
				//     x|y
				//     source|target
				//	       node|port
				////
				var source = null;
				var target = null;
				var node   = null, port = null;
				for(k in e) {
					var v = e[k];
					if(k === "source" || k === "target") {
						node = canvas.getFigure(v.id);
						if(node === null) {
							throw k+" figure with id '" + v.id + "' not found";
						}
						
						port = node.getPort(v.port);
						if(port === null) {
							throw "unable to find " + k + " port '" + v.port + "' at figure '" + v.id + "'";
						}
						
						if(k === "source") source = port;
						else               target = port;
					}
				}
				if(source !== null && target !== null) {
					o.setSource(source);
					o.setTarget(target);
				}
				
				o.setPersistentAttributes(e);
				canvas.add(o);

			} catch(ex) {
				console.warn("Unable to instantiate figure type '" + e.type + "' with id '" + e.id +"'");
				console.warn(e);
				console.warn(ex);
			}
		}));
		/**/
	},
	
	createFigureFromType:function(type) {
		return eval("new "+type+"()");
	}
});


e2e.Convertor = Class.extend({
	NAME:"e2e.Convertor",
	init:function() {
		;
	},
	json:function(json) {
		var result = [];
		$.each(json, function(i, e) {
			var v = {};
			if(e.cssClass.indexOf("Cloud") != -1) {
				//cloud
				v = {
					id:e.id,
					type:e.type,
					name:e.name,
					width:e.width,
					height:e.height,
					x:e.x,
					y:e.y				
				};
			} else if(e.cssClass.indexOf("Node") != -1) {
				//node
				v = {
					id:e.id,
					type:e.type,
					name:e.name,
					path:e.path,
					x:e.x,
					y:e.y,
					bgColor:e.bgColor
				};
			} else if(e.cssClass.indexOf("Connection") != -1){
				//connection.....ASSERT
				v = {
					id:e.id,
					type:e.type,
					source:{
						id:e.source.node,
						port:e.source.port,
					},
					target:{
						id:e.target.node,
						port:e.target.port
					}
				};
			} else {
				return;
			}
			result.push(v);
		});
		return result;
	},
	jsonNode:function(json) { // json Node만 저장 및 key값 추가 2018.01.12 최정수
		var result = [];
		$.each(json, function(i, e) {
			var v = {};
			if(e.cssClass.indexOf("Node") != -1) {
//				console.log(e);
				//node
				v = {
					id:e.id,
					type:e.type,
					name:e.name,
					path:e.path,
					x:e.x,
					y:e.y,
					bgColor:e.bgColor,
					key:e.userData.key,
					vnfdName:e.userData.vnfd_name,
					vnfdId:e.userData.vnfd_id
				};
			} else {
				return;
			}
			result.push(v);
		});
		return result;
	}
	
});