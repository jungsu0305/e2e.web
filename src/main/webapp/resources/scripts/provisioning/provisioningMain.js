var app;
var formData 		= {};
var isTracking		= false;

function initProvision( menuObject, dataList, nsdList, nsdIdList, vnfdList, nsdData ){
	$("#canvas").css("height", 900);
	$("#canvas").css("width", window.innerWidth);
	
	app 					= new e2e.Application("canvas");

	formData.menuObject		= menuObject;
	formData.dataList		= dataList;
	
	formData.nsId			= $("#nsId").val();
	formData.nsName			= $("#nsName").val();
	formData.nsdId			= $("#nsdId").text();
	formData.nsDesc			= $("#nsDesc").val();
	formData.nsdUuid 		= $("#nsdUuid").val();
	formData.nsInsId		= $("#nsInsId").text();

	setProvisionButton( "manualScale" );
	setProvisionButton( "nsSave" );
	setProvisionButton( "nsProvisioning" );
	setProvisionButton( "enable" );
	setProvisionButton( "delete" );
	
	setNSListMenu( menuObject, dataList, formData.nsId );	
	setGeneralInfo( nsdList, nsdIdList );		 
	
	setEvent( "wheel" );
	
	if( formData.nsName != null && formData.nsName != "" ){
		$("#nsName").attr("readonly",true);
		setNSComponent( vnfdList, nsdData );
	 	setNSDeploymentFlavor( nsdData );
	 	getTargetNFVO( false );
	 	
	 	$("#nsdCombo option").not(":selected").attr("disabled", "disabled");
	 	
	}else{
		$("#nsName").attr("readonly",false);
	}
}

function setNSComponent( vnfdList, nsdData ){
	//var columnList = [ "ns_type_name", "vnfd_name", "vnfd_id", "affinityRule", "nfvi" ];
	//var columnList = [ "ns_type_name", "vnfd_name", "vnfProfileId" ];
	var columnList = [ "ns_type_name", "vnfProfileName", "vnfd_name" ];
	
	var tagDataList 		= [];
	var tagData 			= {};
		tagData.type 		= "select";
		tagData.index 		= 3;
		tagData.valueList 	= [ "None", "Affinity", "AntiAffinity" ];
		tagDataList.push( tagData );
	
	var tagData2 			= {};
		tagData2.type 		= "select";
		tagData2.index 		= 4;
		tagData2.valueList 	= [ "None", "NFVI Node", "NFVI Pop", "Etc" ];
		tagDataList.push( tagData2 );
	
		
	var nsdf = nsdData.nsdf;
	var dataObj = JSON.parse(nsdf);
	var nsdfList = dataObj.nsDf;
	
	//"2017-11-01 02:00:00"
	//var startTime = getDateTimeByOne(  );
	var startTime = $("#startTime_year").val() + "";
	formData.startTime = ""; //화면상에 시간  
	formData.nsInstantiationLevelId = ""; // 화면상의 levelId
	
	for(var i = 0 ; i < vnfdList.length ; i++){
		for( var j = 0 ; j < nsdfList.length ; j++ ){
			if( vnfdList[i].vnfd_id ==  nsdfList[j].vnfProfile[0].vnfdId ){
				vnfdList[i].vnfProfileId = nsdfList[j].vnfProfile[0].vnfProfileId;
				vnfdList[i].vnfProfileName = nsdfList[j].vnfProfile[0].vnfProfileName;
				break;
			}
		}
		
		$("#nsComponent").append( createCustomTrGenerator( vnfdList[i], columnList, null ) );
	}
}

function setNSDeploymentFlavor( nsdData ){
	var nsdf 			= nsdData.nsdf;
	var dataObj 		= JSON.parse(nsdf);
	var nsdfList 		= dataObj.nsDf;
	var nsdfNameList 	= [];
	var nsdfIdList 		= [];
	if( nsdfList != null && nsdfList.length > 0 ){
		for( var i = 0 ; i < nsdfList.length ; i++ ){
			console.log("#### "+ nsdfList[ i ].nsdfName  );
			var nsdfNameMap = nsdfList[ i ].nsdfName;
			var nsdfId = nsdfList[ i ].nsdfId;
			
			if( nsdfNameList[ nsdfNameMap ] === undefined || nsdfNameList[ nsdfNameMap ] == null ){
				nsdfNameList[ nsdfNameMap ] = nsdfNameMap;
				
				nsdfIdList[ nsdfNameMap ] = nsdfId;
			}
		}		
	}
	for( var key in nsdfNameList ){
		$("#nsDeploymentFlavor").append("<option value='"+nsdfIdList[ nsdfNameMap ]+"'>"+nsdfNameList[ key ]+"</option>");
	}
	formData.nsdfId = $("#nsDeploymentFlavor  option:selected").val();
	formData.nsFlavourId = $("#nsDeploymentFlavor  option:selected").val();
	
}

var curr;
function setEvent( type ){
	if( type =="wheel" ){
		$("#canvas").on("mousewheel", function( e ){
			 e.preventDefault();
             var delta = 0;
             if (!event) event = window.event;
             if (event.wheelDelta) {
                 delta = event.wheelDelta / 120;
                 if (window.opera) delta = -delta;
             } else if (event.detail) delta = -event.detail / 3;
             var moveTop = null;
             // 마우스휠을 위에서 아래로
             if (delta < 0) {
                 if ($(this).next() != undefined) {
                     //moveTop = $(this).next().offset().top;
                     console.log("mouse Down");
                     app.view.setZoom(1.0, true);
                 }
             // 마우스휠을 아래에서 위로
             } else {
                 if ($(this).prev() != undefined) {
                     //moveTop = $(this).prev().offset().top;
                     console.log("mouse Up");
                     app.view.setZoom(1.0, true);
                 }
             }
		});
		//this.view.setZoom(1.0, true);
	}
}

function setProvisionButton( buttonId ){
//	
//	$("#"+buttonId).click( function( event ){
//		event.preventDefault();
//	});
//	
	if( buttonId == "manualScale" ){
		$("#manualScale").click( function( event ){
			event.preventDefault();
			pop_open('#popManualScale');
			//buttonScale
			//buttonAdd
			//buttonDelete
			//scalePopTbody
		});
		
	}else if( buttonId == "nsSave" ){
		$("#nsSave").click( function( event ){
			event.preventDefault();
			var isValid = validateGeneralInfo("nsSave");		
				
			if( isValid ){
				var paramMap		= {};
					paramMap.nsId	= $("#nsId").val();
					paramMap.nsdId	= $("#nsdId").text();
					paramMap.nsName	= $("#nsName").val();
					paramMap.nsDesc	= $("#nsDesc").val();
				$.ajax({ 
					type: "get", 
					url : "/provisioning/saveGeneralInfo" + createParamString( paramMap ), 
					data: "", 
					dataType:"json", 
					success : function(data, status, xhr) {
						if( data != null && data != "" ){
							 if( data.errorCode == 0 || data.errorCode == "0" ){
								 $("#nsId").val( data.ns_id );
								 //NS List �߰�, dataList ��
								 var dataList 			= [];
								 var dataMap 			= {};
									 dataMap.id 		= data.ns_id;
									 dataMap.nsd_id 	= $("#nsdId").text();
									 dataMap.ns_name 	= $("#nsName").val();
									 dataMap.ns_state 	= data.ns_state;
									 
								 dataList.push( dataMap );
								 formData.nsId = data.ns_id;
								 $("#nsId").val( data.ns_id );
								 
								 setNSListMenu( formData.menuObject, dataList, data.ns_id );
								 alert("complete !!" + data.ns_id );
							 }else{
								 alert( "error : "+data.errorMessage );
							 }
						}
					}, 
					error: function(jqXHR, textStatus, errorThrown) { 
						alert("error : "+jqXHR.responseText); 
					} 
				});	
			}
		});
		
	}else if( buttonId == "nsProvisioning" ){
		$("#nsProvisioning").click( function( event ){
			event.preventDefault();
			provisionTracking.pollingStart = true;
			provisionTracking.PollimgRefresh = provisionTracking.provisioningPollimgTime / 1000;
			
			stopInterval( provisionTracking.intervalId );
			startInterval( provisionTracking.intervalId );
			
			var isValid = validateGeneralInfo("nsProvisioning");			
			if( isValid ){
				if( provisionTracking.status == "" ){
					$("#trackingState_createNS").removeClass("fail").removeClass("on");
					$("#trackingState_instantiateNS").removeClass("fail").removeClass("on");
					$("#trackingState_instantiateNSStart").removeClass("fail").removeClass("on");
					$("#trackingState_instantiateNSResult").removeClass("fail").removeClass("on");
					$("#trackingListTbody").html("");
					provisionTracking.provisioningStartTime = setCheckTime("provisioning");
					$("#provisioningStartTime").html( provisionTracking.provisioningStartTime );
					
					var paramMap = {};
						paramMap.nsdId  = formData.nsdId;
						paramMap.nsName = formData.nsName;
						paramMap.nsDesc = formData.nsDesc;
						paramMap.status = "";
						
					callCreateNS( paramMap );
				}else{
					if( provisionTracking.pollingStart ){
						alert("이미 처리중입니다.");
					}
				}

				isTracking = true;
			}
		});
	}else if( buttonId == "nsEnable" ){
		
	}else if( buttonId == "nsTerminate" ){
	}	
//	}else if( buttonId == "popOrderTracking" ){
//		$("#popOrderTracking").click( function( event ){
//			event.preventDefault();
//			pop_open('#popOrderTracking');
//		});
//	}
}

function setNSListMenu( menuObject, dataList, nsId ){
	//menuObject.html("");

	if( $("#nsId").val() == nsId && nsId != "newNsIns" ){
		//$("#"+nsId).html( createLITag( false, true, dataList[ 0 ], dataList[ 0 ].id, dataList[ 0 ].nsd_id ) );
	}else{
	}
	if( dataList != null && dataList.length > 0 ){
		for( var i = 0 ; i < dataList.length ; i++ ){
			menuObject.append( createLITag( true, true, dataList[ i ], dataList[ i ].id, dataList[ i ].nsd_id ) );
		}	
	}
	menuClassToggle( menuObject, nsId );

}

function createLITag( isLi, isLink, dataObject, nsId, nsdId ){
	var liString = "";
	if( isLi ){
		liString += "<li id='"+nsId+"'>";
	}
	if( isLink ){
		liString += "<a href='/provisioning/view/"+nsId+"/"+nsdId+"' onclick=''>";
		for( var key in dataObject ){
			if( key == "ns_name" ){
				liString += "<span class='tit'>";
				liString += dataObject[ key ];
				liString += "</span>";									
			}
			if( key == "ns_state" ){
				liString += "<span class='state'>";
				liString += dataObject[ "ns_state_name" ];
				liString += "</span>";									
			}
		}
		liString += "</a>";
	}
	if( isLi ){
		liString += "</li>";
	}
	
	return liString;
}

function menuClassToggle( menuObject, nsId ){
	var menuLIList = menuObject.find("li");
		for( var i = 0 ; i < menuLIList.length ; i++ ){
			if( menuLIList[i].id ==  nsId ){
				$(menuLIList[i]).addClass("on");
			}
		}
		menuLIList.click(function(){
		menuObject.find("li").removeClass("on");
		$(this).addClass("on");
		//Ŭ�� ��ũ 
	});
}

function setGeneralInfo( nsdList, nsdIdList ){
	if( nsdList != null && nsdList.length > 0 ){
		var nsdCombo = $("#nsdCombo");
		
		var currentNsdName = "";
		var currentNsInsId = "";
		var currentTargetNFVO = "";
		var currentNsDescription = "";
		
		nsdCombo.html("<option value='none'>None</option");
		for( var i = 0 ; i < nsdList.length ; i++ ){
			if( formData.nsdId == nsdList[ i ].nsd_id ){
				nsdCombo.append( "<option selected value='" + nsdList[ i ].nsd_id + "'>"+nsdList[i].nsd_name + "</option>" );
				currentNsdName 			= nsdList[i].nsd_name;
				currentNsInsId 			= nsdList[i].ns_ins_id;
				currentTargetNFVO 		= nsdList[i].target_nfvo;
				currentNsDescription 	= nsdList[i].ns_desc;
			}else{
				nsdCombo.append( "<option value='" + nsdList[ i ].nsd_id + "'>"+nsdList[i].nsd_name + "</option>" );
			}
		}
		
		addEvent( nsdCombo, nsdIdList );
		
		if( formData.nsdId == "" || formData.nsdId == "newNsIns" ){
			$("#nsdId").html("");		
			$("#nsName").html("");
			$("#nsInsId").html(""); 
			$("#targetNFVO").html("");
			$("#nsDesc").html("");
			$("#nsComponent").html("");
		}else{
			$("#nsName").html( formData.nsdId );
			editorReload( app, utils, formData.nsdId );

			$("#nsSave").removeClass("btn_co01").addClass("btn_co02");
			$("#nsProvisioning").removeClass("btn_co02").addClass("btn_co01");
			
		}
	}
	
}

function addEvent( nsdCombo, nsdIdList ){
	nsdCombo.change(function(){
		$("#nsdId").html( this.value );

		var nsdId = this.value;
		if( this.value == "none" ){
			editorInit( app, utils, nsdId );
			clearTargetNFVO();
		}else{
			editorReload( app, utils, nsdId );
			getTargetNFVO( false );
			
			// ajax 처리 DB에서 vnfd, nsd 정보 가져오기
			//setNSComponent(  vnfdList );
		 	//setNSDeploymentFlavor( nsdData );	

//			var nsdName = $("#nsName").val( nsdIdList[ this.value ].nsd_name );
//			
//			$.ajax({ 
//				type: "get", 
//				url : "/provisioning/nsd/" + nsdId, 
//				data: "", 
//				dataType:"json", 
//				success : function(data, status, xhr) { 
//					$("#nsComponent").html("");
//					if( data != null && data != "" ){
//						alert("success : "+data); 	
//					}
//				}, 
//				error: function(jqXHR, textStatus, errorThrown) { 
//					alert("error : "+jqXHR.responseText); 
//				} 
//			});				
		}
	});
}

function clearTargetNFVO(){
	$("#targetNfvo").html("");
}

function getTargetNFVO( isNew ){
	$.get("/oneView/design/targetList", function(dataFull) {
		var dataObj = JSON.parse(dataFull);
		var lth = dataObj.length;
		if(lth > 0) {
			var output = "";
			
			for(var i = 0; i < lth; i++) {
				output += "<option value=" + dataObj[i].nfvo_name +">" + dataObj[i].nfvo_id + "</option>";
			}
		}

		$("#targetNFVO").append(output);

		if(!isNew) {
			//app.lodingGeneralInfo();
			$("#targetNFVO").val(dataObj[0].nfvo_id).attr("selected", "selected");
		}
			
	});	
}

function validateGeneralInfo( type ){
	var isValid 	= true;
	var nsId 		= $( "#nsId" ).val();
	var nsdId		= $( "#nsdId" ).html();
	var nsName 		= $( "#nsName" ).val();
	
	if( nsdId == null || nsdId == "" || nsdId == "none" ){
		isValid = false;
		alert( "nsdId is Null!" );
		return false;
	}			
	if( nsName == null || nsName == "" ){
		isValid = false;
		alert( "nsName is Null!" );
		return false;
	}	
	if( type == "nsSave" ){
		
	}else{
		
	}
	return true;
}


var provisionTracking 							= {};
	provisionTracking.constSet 					= {};
	provisionTracking.constSet.DEFAULT 			= -1;
	provisionTracking.constSet.START 			= 0;
	provisionTracking.constSet.RESULT	 		= 1;
	provisionTracking.constSet.PROCESSING 		= 0;
	provisionTracking.constSet.COMPLETED 		= 1;
	provisionTracking.constSet.FAILED_TEMP 		= 2;
	provisionTracking.constSet.FAILED 			= 3;
	provisionTracking.constSet.ROLLING_BACK 	= 4;
	provisionTracking.constSet.ROLLED_BACK 		= 5;
	provisionTracking.data						= {};
	provisionTracking.data.msgCode				= "";
	provisionTracking.data.eventMsg				= "";
	provisionTracking.data.startTime			= "";
	provisionTracking.data.endTime				= "";
	provisionTracking.data.duration				= "";
	//provisionTracking.data.columnList			= ["msgCode", "eventMsg", "startTime", "endTime", "duration"];
	provisionTracking.data.columnList			= ["msgCode", "direction", "eventMsg", "startTime", "endTime", "duration"];
	provisionTracking.status					= "";
	provisionTracking.nsInsId					= "";
	provisionTracking.createNS					= {};
	provisionTracking.instantiateNS				= {};
	provisionTracking.instantiateNSStart 		= {};
	provisionTracking.instantiateNSResult	 	= {};
	provisionTracking.completed					= {};
	provisionTracking.intervalId				= null;
	provisionTracking.pollingStart				= false;
	provisionTracking.provisioningPollimgTime	= 10000;
	provisionTracking.PollimgRefresh			= provisionTracking.provisioningPollimgTime / 1000;
	
function callCreateNS( paramMap ){
	
	var callTime = setCheckTime( "createNS" );
	
	paramMap.url 			= "http://175.213.170.14:8500/e2e/nfvo/nfv1/nslcm/v1/ns_instances";
	paramMap.port 			= "8500";
	paramMap.method 		= "POST";
	paramMap.type 			= "createNS";

	provisionTracking.createNS.type 			= "createNS";
	provisionTracking.createNS.requestParam 	= paramMap;
	
	try {
		$.ajax({ 
			type: "get", 
			url : "/provisioning/restfulPost" + createParamString( paramMap ), 
			data: "", 
			dataType:"json", 
			success : function(data, status, xhr) {
				var commonObject = {};
					commonObject.msgCode	= "200";
					commonObject.eventMsg	= "createNS";
					commonObject.startTime = callTime;
					commonObject.endTime	= getDateTime( new Date() );
					commonObject.duration	= getCheckTime( "createNS" );

				var dataObj = getParsing( data, commonObject );
				// nsInstanceId 업데이트하기  nsId 필요함
				// 저장한 후에 아래부분 호출하기
				//	paramMap.nsId 			= formData.nsId;
				var saveMap			= {};
					saveMap.nsId 	= formData.nsId;
					saveMap.nsInsId = dataObj["nsInstanceId"];
					console.log("#### [ nsInstanceId : "+dataObj["nsInstanceId"]+" ] ####");
				
				saveNsInstance( saveMap )
					
				provisionTracking.createNS.responseParam = getParsing( data, {} );
				$("#trackingState_createNS").removeClass("fail").addClass("on");
				$("#trackingListTbody").append( createTrGenerator( dataObj, provisionTracking.data.columnList, true, "openPopOrderTracking", "createNS" ) );
				if( dataObj["nsInstanceId"] != null && dataObj["nsInstanceId"] != "" ){
					isTracking = true;
					provisionTracking.status = "createNS";
					provisionTracking.nsInsId = dataObj["nsInstanceId"];
					callInstantiate( saveMap );
				}else{
					isTracking = false;
					$("#trackingState_createNS").removeClass("on").addClass("fail");
				}				
				
//				}else{
//					isTracking = false;
//				}
			}, 
			error: function(data, textStatus, errorThrown) { 
				$("#trackingState_createNS").removeClass("on").addClass("fail");
				var commonObject = {};
				var dataObj = getParsing( data, commonObject );

				alert("responseText : "+data.responseText);
//				alert("error : "+jqXHR.responseText);sb.append("errorCode:-1,");
//				sb.append("errorMessage:"+"\""+e.getMessage()+"\"");
//				sb.append("errorLocalMessage:"+"\""+e.getLocalizedMessage()+"\"");
			} 
		});	
	} catch (e) {
		console.log(e)
	}
	
}

function saveNsInstance( paramMap ){
	var returnValue = false;
	console.log("###[ saveNsInstance ] paramMap: "+paramMap);
	$.ajax({ 
		type: "get", 
		url : "/provisioning/saveNsInstance" + createParamString( paramMap ), 
		data: "", 
		dataType:"json", 
		success : function(data, status, xhr) {
			console.log("### saveNsInstance Success ");
			returnValue = true;
		}, 
		error: function(jqXHR, textStatus, errorThrown) { 
			console.log("### saveNsInstance Error ");
			returnValue = false;
			provisionTracking.pollingStart = false;
		} 
	});	
	return returnValue;
}

function callInstantiate( paramMap ){
	paramMap.url 				= "http://175.213.170.14:8500/e2e/nfvo/nfv1/nslcm/v1/ns_instances/"+paramMap.nsInsId+"/instantiate";
	paramMap.port 				= "8500";
	paramMap.method 			= "POST";
	
	paramMap.nsFlavourId 		= formData.nsFlavourId;
	//	alert( "startTime_year : "+$("#startTime_year").val() );
	//paramMap.startTime			= getDateTimeByOne();
	paramMap.nsInstanceLevelId	= $("#nsInstanceLevelId").val();
	
	provisionTracking.instantiateNS.type 			= "instantiateNS";
	provisionTracking.instantiateNS.requestParam 	= paramMap;

	var callTime		= setCheckTime( "instantiateNS" );
	$.ajax({ 
		type: "get", 
		url : "/provisioning/restfulPost" + createParamString( paramMap ), 
		data: "", 
		dataType:"json", 
		success : function(data, textStatus, xhr) {
			var commonObject = {};
				commonObject.msgCode			= "200";
				commonObject.eventMsg			= "instantiateNS";
				commonObject.startTime			= callTime;
				commonObject.endTime			= getDateTime( new Date() );
				commonObject.duration			= getCheckTime( "instantiateNS" );

			var dataObj = getParsing( data, commonObject );

			var saveMap				= {};
			saveMap.nsId 			= formData.nsId;
			saveMap.nsInsId 		= paramMap.nsInsId;
			saveMap.lcOpOcc			= dataObj["lifecycleOperationOccurrenceId"];
			saveMap.flavourId		= paramMap.nsFlavourId;
			saveMap.nsInsLevelId	= paramMap.nsInstanceLevelId;
			
			console.log("#### [ lifecycleOperationOccurrenceId : "+dataObj["lifecycleOperationOccurrenceId"]+" ] ####");
			 if( saveNsInstance( saveMap ) ){
				  console.log("###[ saveNsInstance ] : "+saveMap);
			 }
			 
			provisionTracking.status = "instantiateNS";
			$("#trackingState_instantiateNS").removeClass("fail").addClass("on");
			$("#trackingListTbody").append( createTrGenerator( dataObj, provisionTracking.data.columnList, true, "openPopOrderTracking", "instantiateNS" ) );
			if( isTracking ){
				provisionTracking.data.msgCode			= "200";
				provisionTracking.data.eventMsg			= "instantiateNSStart";
				provisionTracking.data.startTime		= setCheckTime( "instantiateNSStart" );

				orderTracking( paramMap.nsInsId );	
			}	
			
			provisionTracking.instantiateNS.responseParam = getParsing( data, {} );
		
		}, 
		error: function(jqXHR, textStatus, errorThrown) { 
			$("#trackingState_instantiateNS").removeClass("on").addClass("fail");
			provisionTracking.pollingStart = false;
			alert("error : "+jqXHR.responseText);
			
		} 
	});		
}

function getParsing( data, commonObject ){
	var dataObj;
	if( typeof data == "string" ){
		data = data.replace(/\n/g,'');
		dataObj = JSON.parse( data );
	}else if( typeof data == "object" ){
		dataObj = {};
		for( var key in data ){
			dataObj[ key ] = data[ key ];
		}
	}
	//dataObj.requestParam = paramMap;	
	
	for( var key in commonObject ){
		dataObj[ key ] = commonObject[ key ];
	}
	return dataObj;
}

function orderTracking( nsInsId ){
	try {
		
		var paramMap 		= {};
		paramMap.type		= "orderTracking";
		paramMap.nsInsId 	= nsInsId;

		provisionTracking.instantiateNSStart.requestParam = paramMap;
		provisionTracking.instantiateNSResult.requestParam = paramMap;
		
		if( isTracking ){
			var startTime 	= setCheckTime( "instantiateNSStart" );
			$.ajax({ 
				type: "get", 
				url : "/provisioning/orderTracking" + createParamString( paramMap ), 
				data: "", 
				dataType:"json", 
				success : function(data, status, xhr) {
					var commonObject = {};
					
					dataObj = getParsing( data, commonObject );
					console.log("################################################## ");
					console.log("#### dataObj.nsInsId         : "+dataObj.nsInsId);
					console.log("#### dataObj.lcOpOcc         : "+dataObj.lcOpOcc);
					console.log("#### dataObj.operation       : "+dataObj.operation);
					console.log("#### dataObj.notiType        : "+dataObj.notiType);
					console.log("#### dataObj.notiStatus      : "+dataObj.notiStatus);
					console.log("#### dataObj.operationStatus : "+dataObj.operationStatus);
					console.log("#### dataObj.affectedVnf     : "+dataObj.affectedVnf);
					console.log("#### dataObj.affctedVl       : "+dataObj.affctedVl);
					console.log("#### dataObj.error           : "+dataObj.error);
					
					
					provisionTracking.instantiateNSStart.responseParam = getParsing( data, {} );
					provisionTracking.instantiateNSResult.responseParam = getParsing( data, {} );
					
					dataObj.msgCode			= "200";
					dataObj.startTime		= startTime;
					dataObj.endTime			= getDateTime( new Date() );
					dataObj.duration		= getCheckTime( "instantiateNSStart" );
					
					checkTrackingData( dataObj, nsInsId );
					console.log("@@@ data.operationStatus : "+dataObj.operationStatus);

				}, 
				error: function(jqXHR, textStatus, errorThrown) { 
					isTracking = false;
					$("#trackingState_instantiateNSStart").removeClass("on").addClass("fail");
					provisionTracking.pollingStart = false;
					alert("error : "+jqXHR.responseText); 
				} 
			});					
		}
	} catch (e) {
		alert(e);
	}

}

function checkTrackingData( data, nsInsId ){
	if( data.operationStatus != "" && data.operationStatus == provisionTracking.constSet.PROCESSING ){
		data.eventMsg	= "Processing";
	}else if( data.operationStatus == provisionTracking.constSet.COMPLETED ){
		data.eventMsg	= "Completed";
		$("#trackingState_instantiateNS").removeClass("fail").addClass("on");
		$("#trackingListTbody").append( createTrGenerator( data, provisionTracking.data.columnList, true, "openPopOrderTracking", "completed" ) );
		// DB의 상태값 저장하고 ns_state 값에 따른 화면값도 같이 변화
		// 버튼도 ns_state값에 따라 Enable / Disable 토글처리
		
	}else if( data.operationStatus == provisionTracking.constSet.FAILED_TEMP ){
		data.eventMsg	= "FailedTemp";
		$("#trackingState_instantiateNS").removeClass("on").addClass("fail");
	}else if( data.operationStatus == provisionTracking.constSet.FAILED ){
		data.eventMsg	= "failed";
		$("#trackingState_instantiateNS").removeClass("on").addClass("fail");
	}else if( data.operationStatus == provisionTracking.constSet.ROLLING_BACK ){
		data.eventMsg	= "RollingBack";
		$("#trackingState_instantiateNS").removeClass("fail").addClass("on");
	}else if( data.operationStatus == provisionTracking.constSet.ROLLED_BACK ){
		data.eventMsg	= "RolledBack";
		$("#trackingState_instantiateNS").removeClass("fail").addClass("on");
	}else{
		data.eventMsg	= "notNoti";
	}

	if( data.notiStatus == provisionTracking.constSet.START ){
		data.notiStatus = "Start";
		data.eventMsg	= "Start";
		$("#trackingState_instantiateNSStart").removeClass("fail").addClass("on");
		$("#trackingListTbody").append( createTrGenerator( data, provisionTracking.data.columnList, true, "openPopOrderTracking", "NSStart" ) );
	}else if( data.notiStatus == provisionTracking.constSet.RESULT ){
		data.notiStatus = "Result";
		data.eventMsg	= "NS Result";
		
		isTracking = false;
		$("#provisioningEndTime").html( getDateTime( new Date() ) );
		$("#provisioningDuration").html( getCheckTime("provisioning") +" Sec");
		
		$("#trackingState_instantiateNSStart").removeClass("fail").addClass("on");
		$("#trackingState_instantiateNSResult").removeClass("fail").addClass("on");
		$("#trackingListTbody").append( createTrGenerator( data, provisionTracking.data.columnList, true, "openPopOrderTracking", "NSResult" ) );
		provisionTracking.pollingStart = false;				
		stopInterval( provisionTracking.intervalId );
		alert("orderTracking Noti End !! ");
	}	
	if( data.eventMsg == "notNoti" ){
		console.log("### Noti is Null !! ");
	}else{
		
	}
//	if( isTracking ){
//		if( provisionTracking.provisioningPollimgTime == 0 ){
//			setTimeout(orderTracking, provisionTracking.provisioningPollimgTime, nsInsId);
//		}
//	}
}

function createTrGenerator( dataObject, columnList, isLink, functionName, type ){
	var returnString = "<tr>";
	if( dataObject != null){
		for( var i = 0 ; i < columnList.length ; i++ ){
			returnString+= "<td>";
			if( isLink ){
				var log = {};
				
				returnString+= "<a href='#' onclick="+functionName+"('"+type+"');>";				
			}
			
			returnString+= dataObject[ columnList[i] ];
			if( isLink ){
				returnString+= "</a>";
			}

			returnString+= "</td>";
		}
	}else{
		returnString+= "<td>";
		returnString+= "null"
		returnString+= "</td>";
	}
	
	returnString += "</tr>";
	return returnString;
}


function createCustomTrGenerator( dataObject, columnList, tagDataList ){
	var returnString = "<tr>";
	if( dataObject != null){
		for( var i = 0 ; i < columnList.length ; i++ ){
			returnString+= "<td>";
			if( tagDataList != null && tagDataList !== undefined && tagDataList.length > 0 ){
				var isFirst = true;
				for( var j = 0 ; j < tagDataList.length ; j++ ){
					if( i ==  tagDataList[ j ].index  ){
						switch( tagDataList[ j ].type ){
						case "select":
							returnString+= "<select>";
							if( tagDataList[ j ].valueList != null && tagDataList[ j ].valueList.length > 0 ){
								for( var k = 0 ; k < tagDataList[ j ].valueList.length ; k++ ){
									returnString += "<option>";
									returnString += tagDataList[ j ].valueList[ k ];
									returnString += "</option>";
								}
							}
							returnString+= "</select>";
							break;
						}
					}else{
						if( isFirst && dataObject[ columnList[i] ] !== undefined ){
							returnString+= dataObject[ columnList[i] ];
							isFirst = false;
						}
					}
				}
			}else{
				if( dataObject[ columnList[i] ] !== undefined ){
					returnString+= dataObject[ columnList[i] ];
				}
			}
			returnString+= "</td>";
		}
	}else{
		returnString+= "<td>";
		returnString+= "null"
		returnString+= "</td>";
	}
	
	returnString += "</tr>";
	return returnString;
}


function openPopOrderTracking( type ){
	pop_open('#popOrderTracking');	

	if( provisionTracking[ type ]  ){
		if( provisionTracking[ type ].requestParam ){
			parsingNewLine( "requestParam", provisionTracking[ type ].requestParam );
		}
		if( provisionTracking[ type ].responseParam ){
			parsingNewLine( "responseParam", provisionTracking[ type ].responseParam );
		}
	}
}

function parsingNewLine( targetId, jsonObject ){
	var str = (JSON.stringify( jsonObject ) );
		str = str.replace(/,/g, ',\n');
		str = str.replace(/{/g, '{\n');
		str = str.replace(/}/g, '\n}');
		
	$("#"+targetId).val( str );	
	return str;
}

function startInterval( intervalObject ){
	if( provisionTracking.intervalId == null ){
		provisionTracking.intervalId = setInterval(function(){
			checkCount();
		}, 1000);
	}
}

function stopInterval( intervalObject ){
	clearInterval( provisionTracking.intervalId )
	provisionTracking.intervalId = null;
}

function checkCount(){
	if( provisionTracking.pollingStart ){
		--provisionTracking.PollimgRefresh;
		$("#provisioningPollimgRefresh").html( provisionTracking.PollimgRefresh );
		console.log("checkCount : "+ provisionTracking.PollimgRefresh );
		
		if( provisionTracking.PollimgRefresh <= 0 ){
			if( provisionTracking.nsInsId != null && provisionTracking.nsInsId != "" ){
				orderTracking( provisionTracking.nsInsId );
				provisionTracking.PollimgRefresh = provisionTracking.provisioningPollimgTime / 1000;
			}
		}
	}
}
