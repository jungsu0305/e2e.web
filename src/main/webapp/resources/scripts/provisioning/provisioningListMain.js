function initProvisioningList( count, searchOption, dataList, columnList, pageList ){
	setButton( "search", "/provisioning" );
	setButton( "nsInsCreate", "/provisioning/view/newNsIns/newNsIns" );
 	setCount( count );
 	if(  searchOption == "" || searchOption == "ns_name" ){
 		$("#search_option option:eq(0)").prop("selected", true);
 	}else{
 		$("#search_option option:eq(1)").prop("selected", true);
 	}
 	
 	
	setList( "tbody", dataList, columnList, 3 );
	setPageList( pageList );
}


function setList( divId, dataList, columnList, detailIndex ){
	if( dataList != null && dataList.length > 0 ){
		var child = "";
		for( var i = 0 ; i < dataList.length ; i++ ){
			child += "<tr>";
			if( columnList != null && columnList.length > 0 ){
				for( var j = 0 ; j < columnList.length ; j++ ){
					if( detailIndex-1 == j ){
						child += createTDTag( true, dataList[ i ][ "key" ], dataList[ i ][ "nsd_id" ], dataList[ i ][ columnList[ j ] ] );
					}else{
						child += createTDTag( false, dataList[ i ][ "key" ], dataList[ i ][ "nsd_id" ], dataList[ i ][ columnList[ j ] ] );
					}
				}
			}else{
				for( var key in dataList[ i ] ){
					child += createTDTag( false, dataList[ i ][ "key" ], dataList[ i ][ "nsd_id" ], dataList[ i ][ key ] );
				}
			}
			child += "</tr>";
		}
		$( ( "#" + divId ) ).html( child );
	}
}

function createTDTag( isLink, key, nsd_id, value ){
	var tdString = "";
		tdString += "<td>";
		if( isLink ){
			tdString += "<a href='/provisioning/view/"+key+"/"+nsd_id+"/"+"' onclick='' >";
		}
		if( key == "ns_type" ){
			var type = "4G";
			if( value == 1 ){
				type = "5G";
			}else if( value == 2 ){
				type = "P-LTE";
			}
			tdString += type;
		}else{
			tdString += value;
		}
		if( isLink ){
			tdString += "</a>";
		}		
		tdString += "</td>";
	return tdString;
}

function listOrder(colName,ordOption)
{
	var orderKey="";
	var commaFlag="";
	orderKey+="(";
	 $('input:checkbox[name="checkNo"]').each(function() {
		orderKey+=commaFlag+this.value
		commaFlag=",";
	 });
	orderKey+=")";
	//alert(orderKey);
    $.ajax({
        type: "get",
       // contentType: "json", //==> ���(RestController�̱⶧���� ����)
        url: "provisioning/jsondata?colName="+colName
        		  +"&ordOption="+ordOption+"&orderKey="+orderKey,
        success: function(result){
            	 var dataObj = JSON.parse(result);
                 var output = "";
                 for(var i in dataObj){

                	 var myDate = new Date(dataObj[i].modify_dtm)
                	 var yyyy= myDate.getFullYear();
                	 var mm=String(myDate.getMonth() + 1);
                	 var dd=String(myDate.getDate());
                	 var h24=String(myDate.getHours());
                	 var mi=String(myDate.getMinutes());
                	 mm=(mm.length==1)?"0"+mm:mm;
                	 dd=(dd.length==1)?"0"+dd:dd;
                	 h24=(h24.length==1)?"0"+h24:h24;     
                	 mi=(mi.length==1)?"0"+mi:mi;
                	 var date = yyyy  + "/" +  mm+ "/" + dd + " " + h24 + ":" + mi;
                	 //var date = yyyy  + "." +  mm+ "." + dd;
                     output += "<tr>";
                     output += "<td><input type=checkbox name='checkNo' value='" + dataObj[i].gug_data_manage_no +"'></td>";
                     output += "<td><a href=\"/gugDataConfig/view/" +  dataObj[i].gug_data_manage_no + "/1\" onClick=\"\" title=\"" + dataObj[i].profile_nm+ "\" class=\"pop_btn\">" 
                                  + dataObj[i].profile_nm +"</a></td>";
                     output += "<td>" + dataObj[i].lte_id +"</td>";
                     output += "<td>" + dataObj[i].cust_nm +"</td>";
                     output += "<td>" + dataObj[i].work_state_nm +"</td>";
                     output += "<td>" + dataObj[i].work_ept_dt +" " + dataObj[i].work_ept_start_time + "</td>";
                     output += "<td>" + date +"</td>";                
                     output += "<td>" + ((typeof(dataObj[i].regist_id)=="undefined")?"":dataObj[i].regist_id) +"</td>";
                     output += "<tr>";
                     
                 }
                 output += "";

            $("#trData").html(output);
            }
        });
    }
    
    
    
 
    
function list(page){
	var f=document.form;
	location.href="gugDataConfig?curPage="+page+"&searchOption=${searchOption}" +
		             "&keyword_fdate=${keyword_fdate}&keyword_tdate=${keyword_tdate}"+
		             "&searchOption2=${searchOption2}&keyword2=${keyword2}"+
		             "&work_state_cd=${work_state_cd}";
}


function searchData(){
	var f				= document.form;
	var search_option	= f.search_option.value;
	var search_keyword	= f.search_keyword.value;
//     	var check4g 		= f.check4g.value;
//     	var check5g 		= f.check5g.value;
//     	var checkPlte 		= f.checkPlte.value;
	
    location.href="/provisioning?curPage=1&search_option=" + search_option +"&search_keyword=" +search_keyword
//                      +"&check4g=" +check4g+"&check5g=" +check5g+"&checkPlte=" +checkPlte;
}
    