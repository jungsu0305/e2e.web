/****/
$(function(){
	
	/** gnb **/
	$('#gnb > ul > li').on("mouseover focusin", function(){
		$(this).addClass("on");
		$(this).children('ul').stop().slideDown();
	});
	$('#gnb > ul > li').on("mouseout focusout", function(){
		$(this).removeClass("on");
		$(this).children('ul').stop().slideUp();
	});




	/** Lnb **/
	lnb_dep = $('.lnb_nav').outerHeight() - ($('.lnb_nav > li > a').outerHeight() * $('.lnb_nav > li').length);
	$('.lnb_dep').height(lnb_dep);
	$('.lnb_dep .scrollbar-inner').height(lnb_dep);
	$(window).resize(function() {
		lnb_dep = $('.lnb_nav').outerHeight() - ($('.lnb_nav > li > a').outerHeight() * $('.lnb_nav > li').length);
		$('.lnb_dep').height(lnb_dep);
		$('.lnb_dep .scrollbar-inner').height(lnb_dep);
	});
	$('.lnb_nav > li:first-child').find('.lnb_dep').slideDown(function(){
		$('.lnb_nav > li:first-child').addClass('on');
	});
	$('.lnb_nav > li > a').click(function(){
		if (!$(this).parent('li').hasClass('on')) {
			$(this).closest('.lnb_nav').find('.lnb_dep').slideUp(function(){
				$(this).closest('.lnb_nav').find('li').removeClass('on');
			});
			$(this).next('.lnb_dep').slideDown(function(){
				$(this).parent('li').addClass('on');
			});
		};
		return false;
	});
	
	
	/** tab **/
	$('.tab_nav a').click(function(){
		$(this).closest('.tab_nav').find('li').removeClass('on');
		$(this).parent('li').addClass('on');
		var tCon = $(this).attr("href");
		$(tCon).parent().find('.tab_con').removeClass('on');
		$(tCon).addClass('on');
		return false;
	});
	
	
	/** POP UP **/
	$('.pop_btn').click(function() {
		var popCon = $(this).attr("href");
		var popLeft = (($(window).width() - $(popCon).outerWidth()) / 2);
		var popTop = (($(window).height() - $(popCon).outerHeight()) / 2 );
		if(popLeft < 0) popLeft = 0;
		if(popTop < 0) popTop = 0;
		$('body').addClass('popup_visible');
		$('.popup_mask').fadeIn();
		$(popCon).css({"left":popLeft, "top":popTop}).fadeIn().addClass('on');
		return false;
	});
	$(window).resize(function() {
		var popCon = $(".popup.on")
		var popLeft = (($(window).width() - $(popCon).outerWidth()) / 2);
		var popTop = (($(window).height() - $(popCon).outerHeight()) / 2 );
		if(popLeft < 0) popLeft = 0;
		if(popTop < 0) popTop = 0;
		$(popCon).css({"left":popLeft, "top":popTop});
	});
	$('.popup .pop_close').click(function() {
		$(this).closest('.popup').fadeOut().removeClass('on');
		$('body').removeClass('popup_visible');
		$('.popup_mask').fadeOut();
		return false;
	});
	$('.popup_mask').click(function() {
		$('.popup').fadeOut().removeClass('on');
		$('.popup_mask').fadeOut();
		$('body').removeClass('popup_visible')
		return false;
	});


		/*** Layout ***/
	$(window).on("load",function(){
		//$(".contents").mCustomScrollbar({axis:"yx",theme:"minimal-dark",scrollInertia:300});
		$(".scrollbar-inner").mCustomScrollbar({theme:"minimal-dark",scrollInertia:300});
	});


	/*** datepicker ***/
	$(".select_date").datepicker({
		monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		dayNames: [ "일", "월", "화", "수", "목", "금", "토" ],
		dayNamesShort: [ "일", "월", "화", "수", "목", "금", "토" ],
		dayNamesMin: [ "일", "월", "화", "수", "목", "금", "토" ],
		dateFormat: 'yy-mm-dd',
		prevText: "이전 달",
		nextText: "다음 달",
		showOn: "button",
		buttonText: "달력보기",
	});

	//input click
	/*
	$('input.select_date').click(function(){
		$(this).datepicker('show');
	})
	*/

	//skip link list
	$('.skip_area a').click(function () {  
		if($(".skip_area .list").css("display") == "none"){   
			jQuery('.skip_area .list').show();  
		} else {  
			jQuery('.skip_area .list').hide();  
		}
	});


});

/** POP UP **/
function pop_close(obj){
	$(obj).fadeOut().removeClass('on');
	$('body').removeClass('popup_visible');
	$('.popup_mask').fadeOut();
}
function pop_open(obj){
	var popLeft = (($(window).width() - $(obj).outerWidth()) / 2);
	var popTop = (($(window).height() - $(obj).outerHeight()) / 2 );
	if(popLeft < 0) popLeft = 0;
	if(popTop < 0) popTop = 0;
	$('body').addClass('popup_visible');
	$('.popup_mask').fadeIn();
	$(obj).css({"left":popLeft, "top":popTop}).fadeIn().addClass('on');
	return false;
};