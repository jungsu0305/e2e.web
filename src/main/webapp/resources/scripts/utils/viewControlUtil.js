var param = {};

function checkNSType( ns_type ){
	var ns_type_name = "";
	if( ns_type != null && ns_type != "" ){
		if( ns_type == "0" || ns_type == 0 ){
			ns_type_name = "4G";
		}else if( ns_type == "1" || ns_type == 1 ){
			ns_type_name = "5G";
		}else if( ns_type == "2" || ns_type == 2 ){
			ns_type_name = "PLTE";
		}
	}
	return ns_type_name;
}

function checkNSState( ns_state ){
	var ns_state_name = "";
	if( ns_state != null && ns_state != "" ){
		if( ns_state == "0" || ns_state == 0 ){
			ns_state_name = "Disable";
		}else if( ns_state == "1" || ns_state == 1 ){
			ns_state_name = "Enable";
		}
	}
	return ns_state_name;
}

function setButton( id, url ){
	$( "#"+id ).find( "a" ).click( function( event ){
		event.preventDefault();
		var paramMap = {};
			if( id =="search" ){
				var curPage				= 1;
				var searchOption 		= $( "#search_option option:selected" ).val();
				var searchKeyword 		= $( "#search_keyword" ).val();
				paramMap.curPage		= curPage;				
				paramMap.search_option 	= searchOption; 
				paramMap.search_keyword = searchKeyword;
				
			}
		onSearchClick( url, paramMap );
	});
}

function onSearchClick( url, paramMap ){
	var href = url + createParamString( paramMap );
	location.href= href;
}

function createParamString( paramMap ){
	var param = "";
	var isFirst = true;
	
	if( paramMap != null ){
		for( var key in paramMap ){
			if( isFirst ){
				param += "?";
				isFirst = false;
			}else{
				param += "&";
			}
			if( typeof paramMap[ key ] == "number" ){
				param += key +"=" + paramMap[ key ] ;
			}else{
				param += key +"=" + encodeURIComponent( paramMap[ key ] );
			}
			
		}
	}
	return param;
}

function setCount( count ){
	$( ".count" ).find( 'span' ).html( count );
}


function setPageList( pageList ){
	if( pageList != null && pageList.length > 0 ){
		$( ".paginate" ).find( 'ul' ).html("");
		for( var i = 0 ; i < pageList.length ; i++ ){
			$( ".paginate" ).find( 'ul' ).append( pageList[ i ] );		
		}
	}
	
}

function setTestData( keyName, nsId ){
	app = new e2e.Application("canvas", true);
	var view = app.view;
	var reader = app.reader;
	
	var data = "";
	if( nsId == "NS_1" ){
		data = '[{"id":"12ee0c41-cd87-52b4-2e51-21d2be320ee7","type":"e2e.component.Node","name":"CU","path":"/images/contents/CU.svg","x":135,"y":154,"bgColor":"#FFFFFF"},{"id":"35427f29-4981-0838-d249-1071ccbb89d4","type":"e2e.component.Node","name":"GW-U","path":"/images/contents/GW_U.svg","x":323,"y":165,"bgColor":"#FFFFFF"},{"id":"b3bb3f08-b0f4-472e-92fa-401b93c9a6ee","type":"e2e.component.Node","name":"SM","path":"/images/contents/SM.svg","x":320,"y":405,"bgColor":"#FFFFFF"},{"id":"1367b050-3eb0-77eb-836c-2fa33f60597d","type":"e2e.component.Node","name":"CSCF","path":"/images/contents/CSCF.svg","x":492,"y":332,"bgColor":"#FFFFFF"},{"id":"bdb47cbf-a5eb-5231-d34c-bf8e380e3567","type":"e2e.policy.Connection","source":{"id":"35427f29-4981-0838-d249-1071ccbb89d4","port":"hybrid3"},"target":{"id":"b3bb3f08-b0f4-472e-92fa-401b93c9a6ee","port":"hybrid2"}},{"id":"f9c63940-224a-da0f-0887-8cff224c2e98","type":"e2e.policy.Connection","source":{"id":"12ee0c41-cd87-52b4-2e51-21d2be320ee7","port":"hybrid0"},"target":{"id":"35427f29-4981-0838-d249-1071ccbb89d4","port":"hybrid1"}},{"id":"9c5b82e8-c6dc-4e98-e0e8-32cc6b98cd0a","type":"e2e.policy.Connection","source":{"id":"b3bb3f08-b0f4-472e-92fa-401b93c9a6ee","port":"hybrid0"},"target":{"id":"1367b050-3eb0-77eb-836c-2fa33f60597d","port":"hybrid1"}}]';	
	}else if( nsId == "NS_2" ){
		data = '[{"id":"2fc495b2-6e92-71c0-731a-06eef4fc53e8","type":"e2e.component.Node","name":"CU","path":"/images/contents/CU.svg","x":118,"y":91,"bgColor":"#FFFFFF"},{"id":"325542a1-0adc-b720-f7d9-c693dd4aa2fb","type":"e2e.component.Node","name":"GW-C","path":"/images/contents/GW_C.svg","x":275,"y":93,"bgColor":"#FFFFFF"},{"id":"06391b96-e569-a124-8c38-2ce8cbece7db","type":"e2e.component.Node","name":"GW-U","path":"/images/contents/GW_U.svg","x":408,"y":111,"bgColor":"#FFFFFF"},{"id":"2609198d-7ef4-9ced-b11e-759580aa9a32","type":"e2e.component.Node","name":"CU","path":"/images/contents/CU.svg","x":140,"y":304,"bgColor":"#FFFFFF"},{"id":"8a3e976f-5fd8-b9f3-f8ea-b60de33acae6","type":"e2e.policy.Connection","source":{"id":"325542a1-0adc-b720-f7d9-c693dd4aa2fb","port":"hybrid0"},"target":{"id":"06391b96-e569-a124-8c38-2ce8cbece7db","port":"hybrid1"}},{"id":"abfd9fca-2b52-ef4a-9c8f-df72daa8a0db","type":"e2e.policy.Connection","source":{"id":"2fc495b2-6e92-71c0-731a-06eef4fc53e8","port":"hybrid3"},"target":{"id":"2609198d-7ef4-9ced-b11e-759580aa9a32","port":"hybrid2"}},{"id":"168b32b7-45ce-6c97-06a5-b8c2a5a7b4aa","type":"e2e.policy.Connection","source":{"id":"2fc495b2-6e92-71c0-731a-06eef4fc53e8","port":"hybrid0"},"target":{"id":"325542a1-0adc-b720-f7d9-c693dd4aa2fb","port":"hybrid1"}},{"id":"045fab45-ce94-bdca-0825-16d22db16cc6","type":"e2e.policy.Connection","source":{"id":"06391b96-e569-a124-8c38-2ce8cbece7db","port":"hybrid6"},"target":{"id":"2609198d-7ef4-9ced-b11e-759580aa9a32","port":"hybrid0"}}]';	
	}else if( nsId == "NS_2" ){
		data = '[{"id":"37ce241c-bee7-473a-e0e5-2ad3ac9108b1","type":"e2e.component.Node","name":"CU","path":"/images/contents/CU.svg","x":58,"y":127,"bgColor":"#FFFFFF"},{"id":"d168f807-9c7f-622d-4753-f38da69e2ab5","type":"e2e.component.Node","name":"GW-C","path":"/images/contents/GW_C.svg","x":200,"y":128,"bgColor":"#FFFFFF"},{"id":"10f3eebf-d6f1-cbae-2525-90d3d63d30e1","type":"e2e.component.Node","name":"GW-U","path":"/images/contents/GW_U.svg","x":67,"y":302,"bgColor":"#FFFFFF"},{"id":"623a3829-b291-1723-5647-fff6f0d624e4","type":"e2e.policy.Connection","source":{"id":"37ce241c-bee7-473a-e0e5-2ad3ac9108b1","port":"hybrid0"},"target":{"id":"d168f807-9c7f-622d-4753-f38da69e2ab5","port":"hybrid1"}},{"id":"6965e645-3d8d-0401-7f10-371f3335a3d9","type":"e2e.policy.Connection","source":{"id":"37ce241c-bee7-473a-e0e5-2ad3ac9108b1","port":"hybrid3"},"target":{"id":"10f3eebf-d6f1-cbae-2525-90d3d63d30e1","port":"hybrid2"}}]';	
	}
	
	if( nsId == "newNsIns" ){
		//??? ????
		
	}else{
		var obj = eval( "(" + data + ")" );
		reader.unmarshal( view, obj );		
	}

}

function editorInit( app, utils, nsdId ){
	app.view.clear();
}

function editorReload( app, utils, nsdId ){
	try {
		var view = app.view;
		var reader = app.reader;
		$.get('/topologyEditor/data/' + nsdId, function(ret) {
			jsonTotal = JSON.parse(ret);
			
			if(jsonTotal.err == 0) {
				view.clear();
				app.data = jsonTotal.data;
				json = JSON.parse(app.data);
				var max = {x:0,y:0};
				for(var i in json) {
					var e = json[i];			
					if(typeof e.x === 'number' && typeof e.width === 'number') {
						if(max.x === 0 || max.x < (e.x + e.width))  max.x = e.x + e.width;				
						if(max.y === 0 || max.y < (e.y + e.height)) max.y = e.y + e.height;
						
					} else if(typeof e.x === 'number') {
						if(max.x === 0 || max.x < (e.x + 50)) max.x = e.x + 50;//offset, 50 1st				
						if(max.y === 0 || max.y < (e.y + 50)) max.y = e.y + 50;
					}
				}
				
				//current window
				var w = view.width();
				var h = view.height();
				
				utils.canvas.set(max, {w:w,h:h});
				reader.unmarshal(view, jsonTotal.data);
			}
		});
	} catch (e) {
		console.log(e);
	}	
}


var timeList = [];
function setCheckTime( key ){
	var startDate = new Date();
	timeList[ key ] = startDate.getTime();
	console.log(" [ "+key+" ] check ");
	return getDateTime( startDate );
}

function getCheckTime( key ){
	var startTime = timeList[ key ];
	var endDate = new Date();
	var endTime = endDate.getTime();
	var diff, lastTime = null;
	if( startTime != null ){
		diff = endTime - startTime;
		lastTime = diff / 1000;
		console.log(" [ "+key+" ] checkTime : "+lastTime+" sec");
	}else{
		console.log("you need setCheckTime");
	}
	return lastTime;
}

function getDateTime( date ){
	
	return ( date.getFullYear() + '-' + pad( date.getMonth() + 1 ) + '-' + pad( date.getDate() ) + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() );
}

function getDateTimeByOne( year, month, day, hour, minute, second ){

	return ( year + '-' + pad( month ) + '-' + pad( day ) +" "+ pad( hour ) + ":" + pad( minute ) + ":" + pad( second ) );
}


function pad(num) {
    num = num + '';
    return num.length < 2 ? '0' + num : num;
}

