<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content= "text/html; charset=UTF-8">
<!-- BootStrap CDN -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<script type="text/javascript" src="/scripts/utils/jquery-1.12.4.js"></script>
<script type="text/javascript" src="/scripts/utils/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="/scripts/utils/template.js"></script>

<script src="/scripts/utils/jquery-ui-1.8.23.custom.min.js"></script>
<script src="/scripts/utils/jquery.browser.js"></script>
<script src="/scripts/utils/less-1.7.5.min.js"></script>
<title>게시글 목록</title>
<script>
//alert("${list}");
    function list(page){
    	var f=document.form;
        location.href="board?curPage="+page+"&searchOption=${searchOption}"+"&keyword=${keyword}";
    }
    
    function search(){
    	var f=document.form;
    	var searchOption=f.searchOption.value;
    	var keyword=f.keyword.value;
    	//alert(searchOption +"--"+keyword);
        location.href="board?curPage=1&searchOption=" + searchOption +"&keyword=" +keyword;
    }
    


    
    $(document).ready(function(){
        function showTags(node){
            var children = node.childNodes;
            for (var i=0; i < children.length; i++){
                if (children[i].nodeType == 1 /*Node.ELEMENT_NODE*/){
                	  console.log(children[i].tagName);
                	  //console.log(children[i].tagName[1].nodeText);
                }
                showTags(children[i]);
            }
        }
       // showTags(document.getElementById("tableA"));
       // var table = document.getElementById("tableA"); 
        
       /*alert(table.getElementsByTagName("td")[20].childNodes[0].nodeValue);
       
       table.getElementsByTagName("td")[20].childNodes[0].nodeValue='dddddd';
       
       alert(table.getElementsByTagName("td")[25].childNodes[0].nodeValue);
       
       table.getElementsByTagName("td")[25].childNodes[0].nodeValue='7774';
        
       alert(table.getElementsByTagName("a")[0].childNodes[0].nodeValue);
       
       table.getElementsByTagName("a")[0].childNodes[0].nodeValue='제목';*/
       
       // table.border = 3; // 속성 설정

        });
    
  //모두 체크/모두 미체크의 체크박스 처리
    function changeCheckBox(flag)
    {
    	if(flag)
   		{
    		 $('input:checkbox[name="checkNo"]').each(function() {
    		      this.checked = true; //checked 처리

    		 });


   		}else{
   			
	   		 $('input:checkbox[name="checkNo"]').each(function() {
			      this.checked = false; //checked 처리
			      if(this.checked){//checked 처리된 항목의 값
			            alert(this.value); 
			      }
			 });
   			
   		}
    }
    
    //체크된 로우 삭제처리
    function submitCheckDelete()
    {
    	var f=document.form;
    	checkBoolean=false;
    	
    	if (typeof(f.elements['checkNo']) != 'undefined') 
    	{
    		if (typeof(f.elements['checkNo'].length) == 'undefined')
   			{
    			if(f.elements['checkNo'].checked==true)
   				{
    				checkBoolean=true;
   				}
   			}else{
		        for (i=0; i<f.elements['checkNo'].length; i++)
		        {
		            if (f.elements['checkNo'][i].checked==true)
		            {
		            	checkBoolean=true;
		            	break;
		            }
		        }
   			}
	        
	        if(checkBoolean)
	       	{
	           f.action="/board/delete/select";
	           f._method.value="DELETE";
	           f.submit();        	
	       	}else{
	       		alert('선택된 내용이 없습니다.');

	       	}

    	}
    	
    }
    
    // 정렬클릭시
    function listOrder(colName,ordOption){
    	var orderKey="";
    	var commaFlag="";
    	orderKey+="(";
  		 $('input:checkbox[name="checkNo"]').each(function() {
  			orderKey+=commaFlag+this.value
  			commaFlag=",";
		 });
  		orderKey+=")";
    	//alert(orderKey);
        $.ajax({
            type: "get",
           // contentType: "json", //==> 생략가능(RestController이기때문에 가능)
            url: "board/jsondata?colName="+colName
            		  +"&ordOption="+ordOption+"&orderKey="+orderKey,
            success: function(result){

               
                	 var dataObj = JSON.parse(result);
                     var output = "";
                     for(var i in dataObj){

                    	 var myDate = new Date(dataObj[i].reg_date)
                    	 var yyyy= myDate.getFullYear();
                    	 var mm=String(myDate.getMonth() + 1);
                    	 var dd=String(myDate.getDate());
                    	 var h24=String(myDate.getHours());
                    	 var mi=String(myDate.getMinutes());
                    	 mm=(mm.length==1)?"0"+mm:mm;
                    	 dd=(dd.length==1)?"0"+dd:dd;
                    	 h24=(h24.length==1)?"0"+h24:h24;     
                    	 mi=(mi.length==1)?"0"+mi:mi;
                    	 var date = yyyy  + "/" +  mm+ "/" + dd + " " + h24 + ":" + mi;
                         output += "<tr>";
                         output += "<td><input type=checkbox name='checkNo' value='" + dataObj[i].bno +"'></td>";
                         output += "<td>" + dataObj[i].bno +"</td>";
                         output += "<td><a href=\"/board/" + dataObj[i].bno +"\">" + dataObj[i].subject +"</a></td>";
                         output += "<td>" + dataObj[i].writer +"</td>";
                         output += "<td>" + date +"</td>";                
                         output += "<td>" + dataObj[i].hit +"</td>";
                         output += "<tr>";
                        

                     }
                     output += "";

                $("#trData").html(output);
            }
        });
    }
</script>
</head>
<body>
    <h3>게시글 목록</h3>
    <form:form commandName="BoardComponent" name="form" action="" onsubmit="return false;">
    <input type="hidden" name="_method" value=""><!-- RESTFUL 메소드 오버라이드를 위해  -->
    <table  width=100% border=0><tr><td align=center>
     <table  border=0><tr>
     <td valign=middle><b>검색조건</b></td>
     <td width=10>&nbsp;</td>
     <td align=right >
     <select name=searchOption>
     	<option value="subject" <c:if test='${searchOption=="subject"}'> selected </c:if> >제목</option>
     	<option value="content" <c:if test='${searchOption=="content"}'> selected </c:if> >내용</option>
     	<option value="writer"  <c:if test='${searchOption=="writer"}'> selected </c:if> >작성자</option>
     </select>
    </td>
    <td width=10>&nbsp;</td>
    <td><input type=text name="keyword" value="${keyword}"></td>
    <td><button class="btn btn-primary" style="float : right;" onclick="javascript:search()">검색</button></td>
    </tr></table>
    </td></tr></table>
    <table  width=100% border=0><tr><td><font size=3>총갯수 :</font>&nbsp; <b>${count}</b></td>
    <td><button class="btn btn-primary" style="float : right;" onclick="submitCheckDelete()">선택삭제</button>
    <button class="btn btn-primary" style="float : right;" onclick="location.href='/board/post'">작성</button>
    </td></tr></table>

    <table class="table" id="tableA" border="1">
        <tr >
            <th style="vertical-align: middle;"><input type=checkbox name="checkALL" value="" onchange="changeCheckBox(this.checked)"></th>        
            <th><TABLE><TR><TD ROWSPAN=2>No</TD><TD ROWSPAN=2>&nbsp;</TD><TD><a href="javascript:listOrder('bno','asc')">▲</a></TD></TR>
                      <TR><TD><a href="javascript:listOrder('bno','desc')">▼</a></TD></TR>
            </TABLE></th>
            <th><TABLE><TR><TD ROWSPAN=2>제목</TD><TD ROWSPAN=2>&nbsp;</TD><TD><a href="javascript:listOrder('subject','asc')">▲</a></TD></TR>
                      <TR><TD><a href="javascript:listOrder('subject','desc')">▼</a></TD></TR>
            </TABLE></th>
            <th><TABLE><TR><TD ROWSPAN=2>작성자</TD><TD ROWSPAN=2>&nbsp;</TD><TD><a href="javascript:listOrder('writer','asc')">▲</a></TD></TR>
                      <TR><TD><a href="javascript:listOrder('writer','desc')">▼</a></TD></TR>
            </TABLE></th>
            <th style="vertical-align: middle;">작성날짜</th>
            <th style="vertical-align: middle;">조회수</th>
        </tr>
        <tbody id="trData" style="border:0px">
        <c:forEach var="board" items="${list}">
        <tr>
            <td><input type=checkbox name="checkNo" value="${board.bno}"></td>
            <td>${board.bno}</td>
            <td><a href="/board/${board.bno}">${board.subject}</a></td>
            <td>${board.writer}</td>
            <td><fmt:formatDate value="${board.reg_date}" pattern="yyyy/MM/dd HH:mm" /></td>
            <td>${board.hit}</td>
        </tr>

        </c:forEach>
         </tbody>
        <tbody style="border:0px">         
          <tr>
            <td colspan="6" align=center>
                <!-- **처음페이지로 이동 : 현재 페이지가 1보다 크면  [처음]하이퍼링크를 화면에 출력-->
                <c:if test="${commonPager.curBlock > 1}">
                    <a href="javascript:list('1')">[처음]</a>
                </c:if>
                
                <!-- **이전페이지 블록으로 이동 : 현재 페이지 블럭이 1보다 크면 [이전]하이퍼링크를 화면에 출력 -->
                <c:if test="${commonPager.curBlock > 1}">
                    <a href="javascript:list('${commonPager.prevPage}')">[이전]</a>
                </c:if>
                
                <!-- **하나의 블럭에서 반복문 수행 시작페이지부터 끝페이지까지 -->
                <c:forEach var="num" begin="${commonPager.blockBegin}" end="${commonPager.blockEnd}">
                    <!-- **현재페이지이면 하이퍼링크 제거 -->
                    <c:choose>
                        <c:when test="${num == commonPager.curPage}">
                            <span style="color: red">${num}</span>&nbsp;
                        </c:when>
                        <c:otherwise>
                            <a href="javascript:list('${num}')">${num}</a>&nbsp;
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                
                <!-- **다음페이지 블록으로 이동 : 현재 페이지 블럭이 전체 페이지 블럭보다 작거나 같으면 [다음]하이퍼링크를 화면에 출력 -->
                <c:if test="${commonPager.curBlock <= commonPager.totBlock}">
                    <a href="javascript:list('${commonPager.nextPage}')">[다음]</a>
                </c:if>
                
                <!-- **끝페이지로 이동 : 현재 페이지가 전체 페이지보다 작거나 같으면 [끝]하이퍼링크를 화면에 출력 -->
                <c:if test="${commonPager.curPage <= commonPager.totPage}">
                    <a href="javascript:list('${commonPager.totPage}')">[끝]</a>
                </c:if>
            </td>
        </tr>
        </tbody>
        
    </table>
    </form:form>
</body>
</html>
