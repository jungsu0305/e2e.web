<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" >
<meta name="format-detection" content="telephone=no">
<title>KT E2E Infra Orchestrator</title>
<link rel="stylesheet" type="text/css" href="/css/style.css">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<script type="text/javascript" src="/scripts/utils/jquery-1.12.4.js"></script>
<script type="text/javascript" src="/scripts/utils/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="/scripts/utils/template.js"></script>

<script src="/scripts/utils/jquery-ui-1.8.23.custom.min.js"></script>
<script src="/scripts/utils/jquery.browser.js"></script>
<script src="/scripts/utils/less-1.7.5.min.js"></script>
 
<script src="/scripts/draw2d.v6/shifty.js"></script>
<script src="/scripts/draw2d.v6/raphael.js"></script>
<script src="/scripts/draw2d.v6/jquery.autoresize.js"></script>
<script src="/scripts/draw2d.v6/jquery-touch_punch.js"></script>
<script src="/scripts/draw2d.v6/jquery.contextmenu.js"></script>
<script src="/scripts/draw2d.v6/rgbcolor.js"></script>
<script src="/scripts/draw2d.v6/canvg.js"></script>
<script src="/scripts/draw2d.v6/Class.js"></script>
<script src="/scripts/draw2d.v6/json2.js"></script>
<script src="/scripts/draw2d.v6/pathfinding-browser.min.js"></script>
<script src="/scripts/draw2d.v6/draw2d.js"></script>

<script src="/scripts/editor/Config.js"></script>
<script src="/scripts/editor/Components.js"></script>
<script src="/scripts/editor/Apps.js"></script>

<script type="text/javascript">
/*/
var app;
var nsdKey = 'data1';
$(window).load(function(){
	app = new e2e.Application("canvas", true);
	var view = app.view;
	var reader = app.reader;
	
	loading = function() {
		$.get('data/' + nsdKey, function(ret) {
			if(ret.err == 0) reader.unmarshal(view, ret.data);
		});
	};
	
	loading();
	
});
/**/
</script>

</head>

<body>
<!-- wrap -->
<div id="wrap">
    <!-- header -->
    <header>
		<h1 class="logo"><a href="/topologyEditor/">KT E2E Infra Orchestrator</a></h1>
		<!-- gnb -->
		<nav id="gnb">
			<ul>
				
				<li><a href="/topologyEditor/">DESIGN</a></li><!-- 활성화 class="on" -->
				<li><a href="/provisioning/">PROVISIONING </a></li>
				<li class="on"><a href="/monitoring/">MONITORING</a></li>
				<li class="hidden"><a href="#">INVENTORY</a></li>
				<li class="hidden"><a href="#">CONFIGURATION</a></li>
			</ul>
		</nav>
		<!-- //gnb -->
		<!-- ad_info -->
		<div class="ad_info">
			<i><img src="/images/layout/img_ad_info.jpg" alt=""></i><!-- image size : 25*25px -->
			<p>admin</p>
		</div>
		<!-- //ad_info -->
    </header>
    <!-- //header -->
    
    <!-- container -->
    <div id="container">
		<!-- cont_area -->
		<section class="cont_area">
			<h2 class="title">NSD_5G_PyeongChang_v1.0</h2>
			<!-- cont_btn -->
			<nav class="cont_btn">
				<a href="#" class="btn_ty">Monitoring</a>
			</nav>
			<!-- //cont_btn -->

			<!-- cont -->
			<div class="cont" id="canvas">
			</div>
			<!-- //cont -->
		</section>
		<!-- //cont_area -->
		
		<!-- under_menu -->
		<nav class="under_menu">
			<dl>
				<dt><a href="#"><img src="/images/layout/btn_under_menu.png" alt="MENU"></a></dt>
				<dd class="s1 hidden"><a href="#"><img src="/images/layout/btn_under_menu01.png" alt=""></a></dd>
				<dd class="s2"><a href="#"><img src="/images/layout/btn_under_menu02.png" alt=""></a></dd>
				<dd class="s3"><a href="#"><img src="/images/layout/btn_under_menu03.png" alt=""></a></dd>
			</dl>
		</nav>
		<!-- //under_menu -->
		
		<!-- side_state_cont -->
		<div class="side_state_cont">
			<ul>
				<li><img src="/images/contents/img_graph01.jpg" alt=""></li>
				<li><img src="/images/contents/img_graph01.jpg" alt=""></li>
				<li><img src="/images/contents/img_graph01.jpg" alt=""></li>
				<li><img src="/images/contents/img_graph01.jpg" alt=""></li>
			</ul>
		</div>
		<!-- //side_state_cont -->
		
		<!-- under_state_bar -->
		<div class="under_state_bar">
			<!-- state_bar -->
			<div class="state_bar">
				<ul>
					<li class="list s1">Type</li>
					<li class="list s2"><a href="#"><img src="/images/layout/btn_state_bar_s1.png" alt=""></a></li>
					<li class="list s3">
						<div class="inner">
							<input type="text" placeholder="Search">
							<a href="#" class="btn">검색</a>
						</div>
					</li>
					<li class="list s4">
						<!-- paginate -->
						<div class="paginate">
							<a href="#" class="btn first">처음 페이지</a>
							<a href="#" class="btn prev">이전 페이지</a>
							<ul>
								<li class="select"><input type="text" value="1"></li>
								<li> / 2</li>
							</ul>
							<a href="#" class="btn next">다음 페이지</a>
							<a href="#" class="btn last">마지막 페이지</a>
						</div>
						<!-- //paginate -->
					</li>
					<li class="list s5">
						<p>Total Count<strong>54</strong></p>
						<ul class="state">
							<li class="st1">0</li>
							<li class="st2">4</li>
							<li class="st3">0</li>
							<li class="st4">99</li>
							<li class="st5">100</li>
						</ul>
					</li>
					<li class="list s6">
						<ul class="c1">
							<li><a href="#">Mask</a></li><!-- 활성화 class="on" -->
							<li><a href="#">Unmask</a></li>
						</ul>
						<ul class="c2">
							<li><a href="#"><img src="/images/layout/btn_state_bar_c1.png" alt="확인"></a></li><!-- 버튼 이름 정확이 모릅니다. -->
							<li><a href="#"><img src="/images/layout/btn_state_bar_c2.png" alt="사운드"></a></li>
							<li><a href="#"><img src="/images/layout/btn_state_bar_c3.png" alt="정지"></a></li>
						</ul>
					</li>
					<li class="btn"><a href="#">저장</a></li><!-- 버튼 이름 정확이 모릅니다. -->
				</ul>
			</div>
			<!-- //state_bar -->

			<!-- under_state_cont -->
			<div class="under_state_cont">
				<!-- table_ty -->
				<div class="table_ty">
					<table>
						<caption>NSD_5G_PyeongChang_v1.0</caption>
						<colgroup>
							<col span="1" style="width:5%;">
							<col span="1" style="width:8%;">
							<col span="1" style="width:13%;">
							<col span="1" style="width:20%;">
							<col span="1" style="width:12%;">
							<col span="1" style="width:12%;">
							<col span="1" style="width:30%;">
						</colgroup>
						<thead>
							<tr>
								<th scope="col"><input type="checkbox"></th>
								<th scope="col" class="left">Mask</th>
								<th scope="col" class="left">Type</th>
								<th scope="col" class="left">Date/Time</th>
								<th scope="col" class="left">Instance name</th>
								<th scope="col" class="left">Event</th>
								<th scope="col" class="left">Event message</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><input type="checkbox"></td>
								<td class="left"></td>
								<td class="left"><i class="st1"></i>MAJOR</td><!-- 상태의 구분에 따른 색상 class st1 ~ st5 -->
								<td class="left">2017-09-29 15:47:58</td>
								<td class="left">NFVO</td>
								<td class="left">NFVO Notification</td>
								<td class="left">Failed to create sercers [reason : internal error : process...</td>
							</tr>
							<tr>
								<td><input type="checkbox"></td>
								<td class="left"></td>
								<td class="left"><i class="st2"></i>Information</td>
								<td class="left">2017-09-29 15:47:58</td>
								<td class="left">NFVO</td>
								<td class="left">NFVO Notification</td>
								<td class="left">Failed to create sercers [reason : internal error : process...</td>
							</tr>
							<tr>
								<td><input type="checkbox"></td>
								<td class="left"></td>
								<td class="left"><i class="st3"></i>Information</td>
								<td class="left">2017-09-29 15:47:58</td>
								<td class="left">NFVO</td>
								<td class="left">NFVO Notification</td>
								<td class="left">Failed to create sercers [reason : internal error : process...</td>
							</tr>
							<tr>
								<td><input type="checkbox"></td>
								<td class="left"></td>
								<td class="left"><i class="st4"></i>Information</td>
								<td class="left">2017-09-29 15:47:58</td>
								<td class="left">NFVO</td>
								<td class="left">NFVO Notification</td>
								<td class="left">Failed to create sercers [reason : internal error : process...</td>
							</tr>
							<tr>
								<td><input type="checkbox"></td>
								<td class="left"></td>
								<td class="left"><i class="st5"></i>Information</td>
								<td class="left">2017-09-29 15:47:58</td>
								<td class="left">NFVO</td>
								<td class="left">NFVO Notification</td>
								<td class="left">Failed to create sercers [reason : internal error : process...</td>
							</tr>
						</tbody>	
					</table>
				</div>
				<!-- //table_ty -->
			</div>
			<!-- //under_state_cont -->
		</div>
		<!-- //under_state_bar -->
	</div>
	<!-- //container -->
	
	<!-- footer -->
	<footer>
		<!-- footer_nav -->
		<nav class="footer_nav">
			<ul>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">이용약관</a></li>
				<li><a href="#">개인정보처리방침</a></li>
				<li><a href="#">Sitemap</a></li>
			</ul>
		</nav>
		<!-- //footer_nav -->
		<address>㈜케이티  대표이사 황창규   경기도 성남시 분당구 불정로 90 (정자동)   사업자등록번호 : 102-81-42945   통신판매업신고 : 2002-경기성남-0047</address>
	</footer>
	<!-- //footer -->
</div>
<!-- //wrap -->
</body>
</html>
