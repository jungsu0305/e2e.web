<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content= "text/html; charset=UTF-8"> 
<meta name="format-detection" content="telephone=no">
<title>KT E2E Infra Orchestrator</title>
<link rel="shortcut icon" href="/new_resources/resources/images/layout/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="/new_resources/resources/css/style.css">
<script type="text/javascript" src="/new_resources/resources/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/template.js"></script>
<script type="text/javascript" >
//모두 체크/모두 미체크의 체크박스 처리
function changeCheckBox(flag)
{
	if(flag)
		{
		 $('input:checkbox[name="checkNo"]').each(function() {
		      this.checked = true; //checked 처리
		 });


		}else{
		
   		 $('input:checkbox[name="checkNo"]').each(function() {
		      this.checked = false; //checked 처리
		      if(this.checked){//checked 처리된 항목의 값
		            alert(this.value); 
		      }
		 });
			
		}
}

//체크된 로우 삭제처리
function submitCheckDelete()
{
	var f=document.form;
	checkBoolean=false;
	
	if (typeof(f.elements['checkNo']) != 'undefined') 
	{
		if (typeof(f.elements['checkNo'].length) == 'undefined')
		{
			if(f.elements['checkNo'].checked==true)
			{
				checkBoolean=true;
			}
		}else{
	        for (i=0; i<f.elements['checkNo'].length; i++)
	        {
	            if (f.elements['checkNo'][i].checked==true)
	            {
	            	//alert(f.elements['checkNo'][i].value);
	            	checkBoolean=true;
	            	break;
	            }
	        }
		}
        
        if(checkBoolean)
       	{
           f.action="/gugDataConfig/delete/select";
           f._method.value="DELETE";
           f.submit();        	
       	}else{
       		alert('선택된 내용이 없습니다.');

       	}

	}
	
}

// 정렬클릭시
    function listOrder(colName,ordOption)
    {
    	var orderKey="";
    	var commaFlag="";
    	orderKey+="(";
  		 $('input:checkbox[name="checkNo"]').each(function() {
  			orderKey+=commaFlag+this.value
  			commaFlag=",";
		 });
  		orderKey+=")";
    	//alert(orderKey);
        $.ajax({
            type: "get",
           // contentType: "json", //==> 생략가능(RestController이기때문에 가능)
            url: "gugDataConfig/jsondata?colName="+colName
            		  +"&ordOption="+ordOption+"&orderKey="+orderKey,
            success: function(result){
                	 var dataObj = JSON.parse(result);
                     var output = "";
                     for(var i in dataObj){

                    	 var myDate = new Date(dataObj[i].modify_dtm)
                    	 var yyyy= myDate.getFullYear();
                    	 var mm=String(myDate.getMonth() + 1);
                    	 var dd=String(myDate.getDate());
                    	 var h24=String(myDate.getHours());
                    	 var mi=String(myDate.getMinutes());
                    	 mm=(mm.length==1)?"0"+mm:mm;
                    	 dd=(dd.length==1)?"0"+dd:dd;
                    	 h24=(h24.length==1)?"0"+h24:h24;     
                    	 mi=(mi.length==1)?"0"+mi:mi;
                    	 var date = yyyy  + "/" +  mm+ "/" + dd + " " + h24 + ":" + mi;
                    	 //var date = yyyy  + "." +  mm+ "." + dd;
                         output += "<tr>";
                         output += "<td><input type=checkbox name='checkNo' value='" + dataObj[i].gug_data_manage_no +"'></td>";
                         output += "<td><a href=\"#\" onClick=\"\" title=\"" + dataObj[i].profile_nm+ "\" class=\"pop_btn\">" 
                                      + dataObj[i].profile_nm +"</a></td>";
                         output += "<td>" + dataObj[i].lte_id +"</td>";
                         output += "<td>" + dataObj[i].cust_nm +"</td>";
                         output += "<td>" + dataObj[i].work_state_nm +"</td>";
                         output += "<td>" + dataObj[i].work_ept_dt +" " + dataObj[i].work_ept_start_time + "</td>";
                         output += "<td>" + date +"</td>";                
                         output += "<td>" + ((typeof(dataObj[i].regist_id)=="undefined")?"":dataObj[i].regist_id) +"</td>";
                         output += "<td><a href=\"#\" class=\"btn_s btn_co03 on pop_btn\">확인내역</a></td>";
                         output += "<tr>";

                     }
                     output += "";

                $("#trData").html(output);
            }
        });
    }
    
    
    
    // 국데이터 복사
    function dataCopy()
    {
     	var f=document.form;
    	var copyKey="";
    	var cnt=0	;
  		 $('input:checkbox[name="checkNo"]').each(function() {
  			 if(this.checked){
  				copyKey=this.value;
  				cnt=cnt+1;
  			 }
  			if(1 < cnt )
			{
			    return false;
			}

  			
		 });
  		 
		if(cnt <= 0 || 1 < cnt )
		{
 				alert("국데이타 복사를 위해서 첵크박스 하나만 체크하세요."+copyKey);
			
		}else{
        	location.href="gugDataConfig/copy/"+copyKey;
			
		}

     	
     
    }
    
 
    
    function list(page){
    	var f=document.form;
        location.href="gugDataConfig?curPage="+page+"&searchOption=${searchOption}" +
        		             "&keyword_fdate=${keyword_fdate}&keyword_tdate=${keyword_tdate}"+
        		             "&searchOption2=${searchOption2}&keyword2=${keyword2}"+
        		             "&work_state_cd=${work_state_cd}";
    }
    
    function list_state(state_gubun){
    	var f=document.form;
    	location.href="gugDataConfig?curPage=1" +
        "&work_state_cd="+state_gubun;
       /* location.href="gugDataConfig?curPage=1&searchOption=${searchOption}" +
        		             "&keyword_fdate=${keyword_fdate}&keyword_tdate=${keyword_tdate}"+
        		             "&searchOption2=${searchOption2}&keyword2=${keyword2}" +
        		             "&work_state_cd="+state_gubun; */
    }
    
    function searchData(){
    	var f=document.form;
    	var searchOption=f.searchOption.value;
    	var keyword_fdate=f.keyword_fdate.value;
    	var keyword_tdate=f.keyword_tdate.value;
    	var searchOption2=f.searchOption2.value;
    	var keyword2=f.keyword2.value;
    	//alert(searchOption +"--"+keyword);
        location.href="gugDataConfig?curPage=1&searchOption=" + searchOption +"&keyword_fdate=" +keyword_fdate
                     +"&keyword_tdate=" +keyword_tdate+"&searchOption2=" +searchOption2+"&keyword2=" +keyword2;
    }
    
</script>

</head>

<body>
<div id="wrap">
    
    <header>
		<h1 class="logo"><a href="#">KT E2E Infra Orchestrator</a></h1>
		<nav id="gnb">
			<ul>
				<li><a href="#">One-View</a></li>
				<li class="active"><!-- 활성화 class="active" -->
					<a href="#">P-LTE</a>
					<ul>
						<li class="active"><a href="#">국데이터 관리</a></li><!-- 활성화 class="active" -->
						<li><a href="#">명령 실행</a></li>
						<li><a href="#">명령 개별 실행</a></li>
						<li><a href="#">고객사 관리</a></li>
						<li><a href="#">장비 관리</a></li>
						<li><a href="#">명령어 관리</a></li>
						<li><a href="#">모니터링</a></li>
						<li><a href="#">이력조회</a></li>
					</ul>
				</li>
				<li><a href="#">통합NS관리</a></li>
				<li><a href="#">외부연동관리</a></li>
				<li><a href="#">로그관리</a></li>
			</ul>
		</nav><!-- //gnb -->
		<div class="ad_info">
			<i><img src="/new_resources/resources/images/layout/img_ad_info.jpg" alt=""></i><!-- image size : 25*25px -->
			<p>admin</p>
		</div><!-- //ad_info -->
    </header>
    
    <div id="container">	
		
		<ul id="navigation">
			<li><img src="/new_resources/resources/images/layout/img_navigation_home.png" alt="home"></li>
			<li>국데이터 조회</li>
		</ul><!-- //navigation -->
		
		<div id="contents_wrap">
			<div class="contents">
			 <form:form commandName="GugDataConfigComponent" name="form" action="" onsubmit="return false;">
			 <input type="hidden" name="_method" value=""><!-- RESTFUL 메소드 오버라이드를 위해  -->
				<div class="search_area">
					<div class="inner">
						<table>
							<caption>검색 테이블</caption>
							<colgroup>
								<col span="1" style="width:15%;">
								<col span="1" style="width:*;">
							</colgroup>
							<tbody>
								<tr>
									<td>
										<select name="searchOption">
									     	<option value="modify_dtm" <c:if test='${searchOption=="modify_dtm"}'> selected </c:if> >최종변경일</option>
	    	                                <option value="work_ept_dt" <c:if test='${searchOption=="work_ept_dt"}'> selected </c:if> >작업예정일</option>
 									    </select>	
									</td>
									<td class="date">
										<div class="date_area">
											<input type="text" name="keyword_fdate"  value="${keyword_fdate}" class="select_date">
											<span class="b_br">~</span>
											<input type="text" name="keyword_tdate"  value="${keyword_tdate}"   class="select_date">
										</div>
									</td>
								</tr>
								<tr>
									<td>
									<select name="searchOption2">
								     	<option value="profile_nm" <c:if test='${searchOption2=="profile_nm"}'> selected </c:if> >파일명</option>
    	                                <option value="lte_id" <c:if test='${searchOption2=="lte_id"}'> selected </c:if> >LTE ID</option>
								     	<option value="cust_nm" <c:if test='${searchOption2=="cust_nm"}'> selected </c:if> >고객사</option>
    	                                <option value="regist_id" <c:if test='${searchOption2=="regist_id"}'> selected </c:if> >등록자</option>									
									</select>

									</td>
									<td><input type="text" name="keyword2"  value="${keyword2}"></td>
								</tr>
							</tbody>	
						</table>
						<div class="btn_area">
							<a href="#" onClick="searchData()" class="btn_l btn_co01 on ty02">조회하기</a>
						</div>
					</div>
				</div><!-- //search_area -->
				
				<div class="table_ty">
					<div class="table_head">
						<div class="count">Total Count <span>${count}</span></div>
						<ul class="status_board">
							<li><a href="#" onClick="list_state('D010')">입력중 <strong>${summaryItem.summary_type_val_1}</strong></a></li>
							<li><a href="#" onClick="list_state('D030')">변경중 <strong>${summaryItem.summary_type_val_3}</strong></a></li>
							<li><a href="#" onClick="list_state('D020')">입력완료 <strong>${summaryItem.summary_type_val_2}</strong></a></li>
							<li><a href="#" onClick="list_state('D060')">삭제 <strong>${summaryItem.summary_type_val_6}</strong></a></li>
							<li><a href="#" onClick="list_state('D040')">작업대기 <strong>${summaryItem.summary_type_val_4}</strong></a></li>
							<li><a href="#" onClick="list_state('D050')">작업완료 <strong>${summaryItem.summary_type_val_5}</strong></a></li>
						</ul>
						 <div class="btn_area">
							<a href="/gugDataConfig/writeView" class="btn_l btn_co01">신규등록</a>
							<a href="#" onClick="dataCopy()" class="btn_l btn_co01">국데이터 복사</a>
							<a href="#" onClick="submitCheckDelete()" class="btn_l btn_co02">선택삭제</a>
						</div>
					</div>
					<table>
						<caption>목록 테이블</caption>
						<colgroup>
							<col span="1" style="width:5%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:15%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:12%;">
							<col span="1" style="width:12%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:10%;">
						</colgroup>
						<thead>
							<tr>
								<th scope="col"><input type="checkbox"  name="checkALL" value="" onchange="changeCheckBox(this.checked)"></th>
								<th scope="col">
									파일명
									<div class="btn_align">
										<a  href="javascript:listOrder('profile_nm','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('profile_nm','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">
									LTE ID
									<div class="btn_align">
										<a  href="javascript:listOrder('lte_id','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('lte_id','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">
									고객사
									<div class="btn_align">
										<a  href="javascript:listOrder('cust_nm','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('cust_nm','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">
									상태
									<div class="btn_align">
										<a  href="javascript:listOrder('work_state_nm','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('work_state_nm','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">작업(예정)일</th>
								<th scope="col">
									최종변경일
									<div class="btn_align">
										<a  href="javascript:listOrder('modify_dtm','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('modify_dtm','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">
									등록자
									<div class="btn_align">
										<a  href="javascript:listOrder('regist_id','asc')" title="올림차순" class="up">up</a>
										<a  href="javascript:listOrder('regist_id','desc')" title="내림차순"  class="down">down</a>
									</div>
								</th>
								<th scope="col">운용자확인</th>
							</tr>
						</thead>
						<tbody id="trData" style="border:0px">
				        <c:forEach var="board" items="${list}">
							<tr>
								<td><input type="checkbox" name="checkNo" value="${board.gug_data_manage_no}"></td>
								<td><a href="/gugDataConfig/view/${board.gug_data_manage_no}" onclick="" title="${board.profile_nm}">${board.profile_nm}</a></td>
								<td>${board.lte_id}</td>
								<td>${board.cust_nm}</td>
								<td>${board.work_state_nm} </td>
								<td>${board.work_ept_dt} ${board.work_ept_start_time}</td>
								<td><fmt:formatDate value="${board.modify_dtm}" pattern="yyyy.MM.dd hh:mm" /></td>
								<td>${board.regist_id}</td>
								<td><a href="#" class="btn_s btn_co03 on pop_btn">확인내역</a></td>
							</tr>
						 </c:forEach>
				         </tbody>	
						
						</tbody>
					</table>
				</div><!-- //table_ty -->
				
				<div class="paginate">
				    <!-- **처음페이지로 이동 : 현재 페이지가 1보다 크면  [처음]하이퍼링크를 화면에 출력-->
				    <c:if test="${commonPager.curBlock > 1}">
					<a href="#" onClick="list('1')" class="first">처음 페이지</a>
					</c:if>
				
					
					<!-- **이전페이지 블록으로 이동 : 현재 페이지 블럭이 1보다 크면 [이전]하이퍼링크를 화면에 출력 -->
				   <c:if test="${commonPager.curBlock > 1}">
					<a href="#" onClick="list('${commonPager.prevPage}')" class="prev">이전 페이지</a>
					</c:if>

					
					<ul>
						<!-- **하나의 블럭에서 반복문 수행 시작페이지부터 끝페이지까지 -->
		                <c:forEach var="num" begin="${commonPager.blockBegin}" end="${commonPager.blockEnd}">
		                    <!-- **현재페이지이면 하이퍼링크 제거 -->
		                    <c:choose>
		                        <c:when test="${num == commonPager.curPage}">
		                            <li class="select"><a href="#">${num}</a></li>
		                        </c:when>
		                        <c:otherwise>
		                            <li><a href="#" onClick="list('${num}')">${num}</a></li>
		                        </c:otherwise>
		                    </c:choose>
		                </c:forEach>					

					</ul>
					
					<!-- **다음페이지 블록으로 이동 : 현재 페이지 블럭이 전체 페이지 블럭보다 작거나 같으면 [다음]하이퍼링크를 화면에 출력 -->
	                <c:if test="${commonPager.curBlock < commonPager.totBlock}">
					<a href="#" onClick="list('${commonPager.nextPage}')" class="next">다음 페이지</a>
					</c:if>
					
					<!-- **끝페이지로 이동 : 현재 페이지가 전체 페이지보다 작거나 같으면 [끝]하이퍼링크를 화면에 출력 -->
	                <c:if test="${commonPager.curPage < commonPager.totPage}">
					<a href="#" onClick="list('${commonPager.totPage}')" class="last">마지막 페이지</a>
					</c:if>
				</div><!-- //paginate -->
				</form:form>
			</div><!-- //contents -->
		</div><!-- //contents_wrap -->
	</div><!-- //container -->
	
	<footer>
		<nav class="footer_nav">
			<ul>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">이용약관</a></li>
				<li><a href="#">개인정보처리방침</a></li>
				<li><a href="#">Sitemap</a></li>
			</ul>
		</nav><!-- //footer_nav -->
		<address>㈜케이티 대표이사 황창규 경기도 성남시 분당구 불정로 90 (정자동) 사업자등록번호 : 102-81-42945 통신판매업신고 : 2002-경기성남-0047</address>
	</footer>
	
</div><!-- //wrap -->
</body>
</html>