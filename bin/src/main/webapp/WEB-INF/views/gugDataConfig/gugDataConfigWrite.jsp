<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content= "text/html; charset=UTF-8"> 
<meta name="format-detection" content="telephone=no">
<title>KT E2E Infra Orchestrator</title>
<link rel="shortcut icon" href="/new_resources/resources/images/layout/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="/new_resources/resources/css/style.css">
<script type="text/javascript" src="/new_resources/resources/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/template.js"></script>
<script type="text/javascript">
function data_write(save_type)
{
	/*'D010','입력중'
	'D020','입력완료'
	'D030','변경중'
	'D040','작업대기'
	'D050','작업완료'*/
	var f=document.form;
	alert(f.gug_data_manage_no.value);
	if(f.gug_data_manage_no.value=="")
	{
		f.gug_data_manage_no.value="0";
	}
	if(f.gug_data_history_no.value=="")
	{
		f.gug_data_history_no.value="0";
	}
	if(f.parent_gug_data_manage_no.value=="")
	{
		f.parent_gug_data_manage_no.value="0";
	}
	/*
	if(f.ip_pool.value=="")
	{
		f.ip_pool.value="0";
	}
	
	if(f.id.value=="")
	{
		f.id.value="0";
	}
	
	if(f.ip_pool_id.value=="")
	{
		f.ip_pool.value="0";
	}
	if(f.vr_id.value=="")
	{
		f.vr_id.value="0";
	}
	if(f.vlan.value=="")
	{
		f.vlan.value="0";
	}
	*/

	f.work_state_cd.value=save_type;
	f._method.value="POST"
	f.method="POST";
	f.action="/gugDataConfig/post";
	
	f.submit();
}

function data_delete()
{
	/*'D001','입력중'
	'D002','입력완료'
	'D003','변경중'
	'D004','작업대기'
	'D005','작업완료'*/
	var f=document.form;
	f._method.value="DELETE"
	f.method="POST";
	f.action="/gugDataConfig/delete/${gugDataConfig.gug_data_manage_no}";
	
	f.submit();
}

//옵션추가 버튼 클릭시
function mmeAdd()
{
	// item 의 최대번호 구하기 
	//var lastItemNo = $("#mmeTrData tr:last").attr("class").replace("item", "");
	//var newitem = $("#mmeTrData tr:eq(1)").clone();
	//	var newitem = $("#mmeTrData tr:last").clone();
	// ("#mmeTrData").add(newitem);
	$.trClone = $("#mmeTrData tr:last").clone().html();
                $.newTr = $("<tr>"+$.trClone+"</tr>");
 
                // append
                $("#mmeTrData").append($.newTr);


}



</script>

</head>

<body>
<div id="wrap">
    
    <header>
		<h1 class="logo"><a href="#">KT E2E Infra Orchestrator</a></h1>
		<nav id="gnb">
			<ul>
				<li><a href="#">One-View</a></li>
				<li class="active"><!-- 활성화 class="active" -->
					<a href="#">P-LTE</a>
					<ul>
						<li class="active"><a href="#">국데이터 관리</a></li><!-- 활성화 class="active" -->
						<li><a href="#">명령 실행</a></li>
						<li><a href="#">명령 개별 실행</a></li>
						<li><a href="#">고객사 관리</a></li>
						<li><a href="#">장비 관리</a></li>
						<li><a href="#">명령어 관리</a></li>
						<li><a href="#">모니터링</a></li>
						<li><a href="#">이력조회</a></li>
					</ul>
				</li>
				<li><a href="#">통합NS관리</a></li>
				<li><a href="#">외부연동관리</a></li>
				<li><a href="#">로그관리</a></li>
			</ul>
		</nav><!-- //gnb -->
		<div class="ad_info">
			<i><img src="/new_resources/resources/images/layout/img_ad_info.jpg" alt=""></i><!-- image size : 25*25px -->
			<p>admin</p>
		</div><!-- //ad_info -->
    </header>
    
    <div id="container">	
		
		<ul id="navigation">
			<li><img src="/new_resources/resources/images/layout/img_navigation_home.png" alt="home"></li>
			<li>국데이터 조회</li>
			<li>PROFILE#1</li>
		</ul><!-- //navigation -->
		
		<div id="contents_wrap">
			<div class="contents">
			<form:form commandName="GugDataConfigComponent" name="form" action="" onsubmit="return false;">
	    	<input type="hidden" name="_method" value=""><!-- RESTFUL 메소드 오버라이드를 위해  -->
			<input type="hidden" name="gug_data_manage_no" value="${gugDataConfig.gug_data_manage_no}">
			<input type="hidden" name="gug_data_history_no" value="${gugDataConfig.gug_data_history_no}">
			<input type="hidden" name="parent_gug_data_manage_no" value="${gugDataConfig.parent_gug_data_manage_no}">
			
			<input type="hidden" name="cust_manage_no" value="1">
		    <input type="hidden" name="work_state_cd" value="">

				<h3 class="title"><span>고객사 정보</span></h3>
				<div class="table_form bg">
					<table>
						<caption>검색 테이블</caption>
						<colgroup>
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
						</colgroup>
						<tbody>
							<tr>
								<th><span>고객사명</span></th>
								<td>
									<div class="btn_list">
										<input type="text" name="cust_nm" value="${gugDataConfig.cust_nm}">
										<a href="#" class="btn_m btn_co03 on">검색</a>
									</div>
								</td>
								<td>&nbsp;</td>
								<th><span>LTE ID</span></th>
								<td>
									<div class="btn_list">
										<input type="text" name="lte_id" value="${gugDataConfig.lte_id}">
										<a href="#" class="btn_m btn_co03 on">중복확인</a>
									</div>
								</td>
								<td>&nbsp;</td>
								<th><span>vAPN</span></th>
								<td>
									<div class="btn_list">
										<input type="text" name="cust_apn" value="${gugDataConfig.cust_apn}">
										<a href="#" class="btn_m btn_co03 on">중복확인</a>
									</div>
								</td>
							</tr>
							<tr>
								<th><span>IP 유형</span></th>
								<td>
									<select name="ip_type">
										<option value="고정" <c:if test='${gugDataConfig.ip_type=="고정IP"}'> selected </c:if> >고정 IP</option>
									</select>
								</td>
								<td>&nbsp;</td>
								<th><span>서비스타입</span></th>
								<td>
									<select name="service_type">
										<option value="전국형" <c:if test='${gugDataConfig.service_type=="전국형"}'> selected </c:if> >전국형</option>
									</select>
								</td>
								<td>&nbsp;</td>
								<th><span>전용회선 사용여부</span></th>
								<td>
									<select name="ded_line_use_yn">
										<option value="전용회선" <c:if test='${gugDataConfig.ded_line_use_yn=="전용회선"}'> selected </c:if> >전용회선</option>										
									</select>
								</td>
							</tr>
						</tbody>	
					</table>
				</div><!-- //table_form bg -->

				<div class="table_form02">
					<table>
						<caption>검색 테이블</caption>
						<colgroup>
							<col span="1" style="width:8%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:8%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:58%;">
						</colgroup>
						<tbody>
							<tr>
								<th><span>FILE NAME</span></th>
								<td>
									<input type="text" name="profile_nm" value="${gugDataConfig.profile_nm}">
								</td>
								<td>&nbsp;</td>
								<th><span>작업예정일</span></th>
								<td>
									<div class="date_area02">
										<input type="text" name="work_ept_dt" value="${gugDataConfig.work_ept_dt}" class="select_date">
									</div>
								</td>
								<td>
									<div class="time_area">
										<input type="text" name="work_ept_start_time" value="${gugDataConfig.work_ept_start_time}">
										<span class="b_br">~</span>
										<input type="text" name="work_ept_close_time" value="${gugDataConfig.work_ept_close_time}">
									</div>
								</td>
							</tr>
						</tbody>	
					</table>
				</div><!-- //table_form02 -->

				<h5 class="title"><label><input type="checkbox">MME</label></h5>
				<div class="table_form ty_flex">
					<table>
						<caption>입력 테이블</caption>
						<colgroup>
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
						</colgroup>
						<tbody>
							<tr>
								<th><span>APN-FQDN</span></th>
								<td><input type="text" name="apn_fqdn" value="${gugDataConfig.apn_fqdn}"></td>
								<td>&nbsp;</td>
								<th><span>PGWGRP</span></th>
								<td><input type="text" name="pgwgrp" value="${gugDataConfig.pgwgrp}"></td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="table_form ty02 ty_flex bgc">
					<table>
						<caption>입력 테이블</caption>
						<colgroup>
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
						</colgroup>
						<tbody id="mmeTrData" style="border:0px">
							<tr>
								<th><span>PGW</span></th>
								<td><input type="text" name="pgw" value="${gugDataConfig.pgw}"><input type="hidden" name="mme_det_no" value="1"></td>
								<td>&nbsp;</td>
								<th><span>IP</span></th>
								<td><input type="text" name="ip" value="${gugDataConfig.ip}"></td>
								<td>&nbsp;</td>
								<th><span>NAME</span></th>
								<td><input type="text" name="name" value="${gugDataConfig.name}"></td>
								<td>&nbsp;</td>
								<th><span>CAPA</span></th>
								<td><input type="text" name="capa" value="${gugDataConfig.capa}"></td>
							</tr>
						</tbody>
					</table>
					<div class="btn_area">
						<a href="#" onClick="mmeAdd()" class="btn_m btn_co03 on">추가</a>
					</div>
				</div>
				<!-- //table_form -->

				<h5 class="title"><label><input type="checkbox">ePCC</label></h5>
				<div class="table_form ty_flex">
					<table>
						<caption>입력 테이블</caption>
						<colgroup>
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
						</colgroup>
						<tbody>
							<tr>
								<th><span>PGW IP</span></th>
								<td><input type="text" name="pgw_ip" value="${gugDataConfig.pgw_ip}"></td>
								<td>&nbsp;</td>
								<th><span>SMS 수신</span></th>
								<td><input type="text" name="sms_recv" value="${gugDataConfig.sms_recv}"></td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="table_form ty02 ty_flex bgc">
					<table>
						<caption>입력 테이블</caption>
						<colgroup>
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
						</colgroup>
						<tbody>
							<tr>
								<th><span>IP POOL</span><input type="hidden" name="epcc_det_no" value="1"></th>
								<td><input type="text" name="ip_pool" value="${gugDataConfig.ip_pool}"></td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
							</tr>
						</tbody>
					</table>
					<table>
						<caption>입력 테이블</caption>
						<colgroup>
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
						</colgroup>
						<tbody>
							<tr>
								<th><span>IP POOL</span></th>
								<td><input type="text" name="ip_pool2" value=""></td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
							</tr>
						</tbody>
					</table>
					<div class="btn_area">
						<a href="#" class="btn_m btn_co03 on">추가</a>
					</div>
				</div>
				<!-- //table_form -->

				<h5 class="title"><label><input type="checkbox">PGW </label></h5>
				<div class="table_form ty03 ty_flex bgc">
					<table>
						<caption>입력 테이블</caption>
						<colgroup>
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
						</colgroup>
						<tbody id="pgwTrData" style="border:0px">
						<c:forEach var="pgwLst" items="${pgwLst}">
							<tr>
								<th><span>IP POOL ID</span></th>
								<td><input type="text" name="ip_pool_id" value="${pgwLst.ip_pool_id}"><input type="hidden" name="pgw_seq_no" value="1"></td>
								<td>&nbsp;</td>
								<th><span>VR_ID</span></th>
								<td><input type="text" name="vr_id" value="${pgwLst.vr_id}"></td>
								<td>&nbsp;</td>
								<th><span>POOL TYPE</span></th>
								<td>
									<select name="pool_type_cd">
									<c:forEach var="cbd" items="${combo_pool_type_cd}" varStatus="status">
										<option value="${cbd.cd}" ${cbd.cd==pgwLst.pool_type_cd?"selected":""} >${cbd.nm}</option>
									</c:forEach>
									<!-- option value="ddddd" selected >dddd</option> -->
									</select>
								</td>
								<td>&nbsp;</td>
								<th><span>STATIC</span></th>
								<td>
									<select name="static_cd">
									<c:forEach var="cbd" items="${combo_static_cd}">
										<option value="${cbd.cd}" ${cbd.cd==pgwLst.static_cd?"selected":""} >${cbd.nm}</option>
									</c:forEach>
									</select>
								</td>
							</tr>
							<tr>
								<th><span>TUNNEL</span></th>
								<td>
									<select name="tunnel_cd">
									<c:forEach var="cbd" items="${combo_tunnel_cd}">
										<option value="${cbd.cd}" ${cbd.cd==pgwLst.tunnel_cd?"selected":""} >${cbd.nm}</option>
									</c:forEach>
									</select>
								</td>
								<td>&nbsp;</td>
								<th><span>START ADDR</span></th>
								<td><input type="text" name="start_addr" value="${pgwLst.start_addr}"></td>
								<td>&nbsp;</td>
								<th><span>VLAN</span></th>
								<td><input type="text" name="vlan" value="${pgwLst.vlan}"></td>
								<td>&nbsp;</td>
								<th><span>IP ADDR</span></th>
								<td><input type="text" name="ip_addr" value="${pgwLst.ip_addr}"></td>
							</tr>
							<tr>
								<th><span>NETWORK</span></th>
								<td><input type="text" name="network" value="${pgwLst.network}"></td>
								<td>&nbsp;</td>
								<th><span>GATEWAY</span></th>
								<td><input type="text" name="gateway" value="${pgwLst.gateway}"></td>
								<td>&nbsp;</td>
								<th><span>우선순위</span></th>
								<td>
									<select name="prirank_cd">
									<c:forEach var="cbd" items="${combo_prirank_cd}">
										<option value="${cbd.cd}" ${cbd.cd==pgwLst.prirank_cd?"selected":""} >${cbd.nm}</option>
									</c:forEach>
									</select>
								</td>
								<td>&nbsp;</td>
								<th><span>I / F</span></th>
								<td><input type="text" name="ifc" value="${pgwLst.ifc}"></td>
							</tr>
							<tr>
								<th><span>APN ID</span></th>
								<td><input type="text" name="apn_id" value="${pgwLst.apn_id}"></td>
								<td>&nbsp;</td>
								<th><!--<span>VR ID</span>--></th>
								<td><!--<input type="text" name="vr_id2">--></td>
								<td>&nbsp;</td>
								<th><span>APN NAME</span></th>
								<td><input type="text" name="apn_name" value="${pgwLst.apn_name}"></td>
								<td>&nbsp;</td>
								<th><span>IP ALLOC</span></th>
								<td>
									<select name="ip_alloc_cd">
									<c:forEach var="cbd" items="${combo_ip_alloc_cd}">
										<option value="${cbd.cd}" ${cbd.cd==pgwLst.ip_alloc_cd?"selected":""} >${cbd.nm}</option>
									</c:forEach>									
									</select>
								</td>
							</tr>
							<tr>
								<th><span>RAD IP ALLOC</span></th>
								<td>
									<select name="rad_ip_alloc_cd">
									<c:forEach var="cbd" items="${combo_rad_ip_alloc_cd}">
										<option value="${cbd.cd}" ${cbd.cd==pgwLst.rad_ip_alloc_cd?"selected":""} >${cbd.nm}</option>
									</c:forEach>	
									</select>
								</td>
								<td>&nbsp;</td>
								<th><span>AUTH / PCRF /PCC</span></th>
								<td>
									<select name="auth_pcrf_pcc_cd">
									<c:forEach var="cbd" items="${combo_auth_pcrf_pcc_cd}">
										<option value="${cbd.cd}" ${cbd.cd==pgwLst.auth_pcrf_pcc_cd?"selected":""} >${cbd.nm}</option>
									</c:forEach>	
									</select>
								</td>
								<td>&nbsp;</td>
								<th><span>ACCT/REACT</span></th>
								<td>
									<select name="acct_react_cd">
									<c:forEach var="cbd" items="${combo_acct_react_cd}">
										<option value="${cbd.cd}" ${cbd.cd==pgwLst.acct_react_cd?"selected":""} >${cbd.nm}</option>
									</c:forEach>	
									</select>
								</td>
								<td>&nbsp;</td>
								<th><span>PRI DNS V4</span></th>
								<td><input type="text" name="pri_dns_v4" value="${pgwLst.pri_dns_v4}"></td>
							</tr>
							<tr>
								<th><span>RAD AUTH ID</span></th>
								<td>
									<input type="text" name="rad_auth_id" value="${pgwLst.rad_auth_id}">
									<ul class="inp_list">
										<li class="li">
											<em class="s_tit">ACT</em>
											<div><input type="text" name="rad_auth_act" value="${pgwLst.rad_auth_act}"></div>
										</li>
										<li class="li">
											<em class="s_tit">SBY</em>
											<div><input type="text" name="rad_auth_sby" value="${pgwLst.rad_auth_sby}"></div>
										</li>
									</ul>
								</td>
								<td>&nbsp;</td>
								<th><span>RAD ACCT ID</span></th>
								<td>
									<input type="text" name="rad_acct_id" value="${pgwLst.rad_acct_id}">
									<ul class="inp_list">
										<li class="li">
											<em class="s_tit">ACT</em>
											<div><input type="text" name="rad_acct_act" value="${pgwLst.rad_acct_act}"></div>
										</li>
										<li class="li">
											<em class="s_tit">SBY</em>
											<div><input type="text" name="rad_acct_sby" value="${pgwLst.rad_acct_sby}"></div>
										</li>
									</ul>
								</td>
								<td>&nbsp;</td>
								<th><span>DIAM PCFR ID</span></th>
								<td>
									<input type="text" name="diam_pcfr_id" value="${pgwLst.diam_pcfr_id}">
									<ul class="inp_list">
										<li class="li">
											<em class="s_tit">ACT</em>
											<div><input type="text" name="diam_pcfr_act" value="${pgwLst.diam_pcfr_act}"></div>
										</li>
										<li class="li">
											<em class="s_tit">SBY</em>
											<div><input type="text" name="diam_pcfr_sby" value="${pgwLst.diam_pcfr_sby}"></div>
										</li>
									</ul>
								</td>
								<td>&nbsp;</td>
								<th><span>LOCAL PCC PF ID</span></th>
								<td>
									<input type="text" name="local_pcc_pf_id" value="${pgwLst.local_pcc_pf_id}">
									<ul class="inp_list">
										<li class="li">
											<em class="s_tit">ACT</em>
											<div><input type="text" name="local_pcc_pf_act" value="${pgwLst.local_pcc_pf_act}"></div>
										</li>
										<li class="li">
											<em class="s_tit">SBY</em>
											<div><input type="text" name="local_pcc_pf_sby" value="${pgwLst.local_pcc_pf_sby}"></div>
										</li>
									</ul>
								</td>
							</tr>
							<tr>
								<th><span>SEC DNS V4</span></th>
								<td><input type="text" name="sec_dns_v4" value="${pgwLst.sec_dns_v4}"></td>
								<td>&nbsp;</td>
								<th><span>ARP MAP HIGH</span></th>
								<td><input type="text" name="arp_map_high" value="${pgwLst.arp_map_high}"></td>
								<td>&nbsp;</td>
								<th><span>ARP MAP MED</span></th>
								<td><input type="text" name="arp_map_med" value="${pgwLst.arp_map_med}"></td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
					<div class="btn_area">
						<a href="#" class="btn_m btn_co03 on">추가</a>
					</div>
				</div>
				<!-- //table_form -->

				<h5 class="title"><label><input type="checkbox">HSS</label></h5>
				<div class="table_form ty_flex">
					<table>
						<caption>입력 테이블</caption>
						<colgroup>
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:2%;">
							<col span="1" style="width:10%;">
							<col span="1" style="width:*;">
						</colgroup>
						<tbody>
							<tr>
								<th><span>ID</span></th>
								<td><input type="text" name="id" value="${gugDataConfig.id}"></td>
								<td>&nbsp;</td>
								<th><span>APN</span></th>
								<td><input type="text" name="apn" value="${gugDataConfig.apn}"></td>
								<td>&nbsp;</td>
								<th><span>APNOIREPL</span></th>
								<td><input type="text" name="apnoirepl" value="${gugDataConfig.apnoirepl}"></td>
								<td>&nbsp;</td>
								<th><span>PLTECC</span></th>
								<td><input type="text" name="pltcc" value="${gugDataConfig.pltcc}"></td>
							</tr>
							<tr>
								<th><span>UUTYPE</span></th>
								<td><input type="text" name="uutype" value="${gugDataConfig.uutype}"></td>
								<td>&nbsp;</td>
								<th><span>MPNAME</span></th>
								<td>
									<select name="mpname_cd">
									<c:forEach var="cbd" items="${combo_mpname_cd}">
										<option value="${cbd.cd}" ${cbd.cd==gugDataConfig.mpname_cd?"selected":""} >${cbd.nm}</option>
									</c:forEach>	
									</select>
								</td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
								<td>&nbsp;</td>
								<th></th>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- //table_form ${gugDataConfig.view_gubun}-->
				<div class="btn_area ty02">
				    <c:if test="${gugDataConfig.view_gubun =='CREATE'}">
					        <a href="#" onClick="data_write('D010')" class="btn_l btn_co01">임시저장</a>
					</c:if>
					<c:if test="${gugDataConfig.view_gubun =='EDIT'}">
					<c:if test="${gugDataConfig.work_state_cd eq 'D020' || gugDataConfig.work_state_cd eq 'D030'}">
					        <a href="#" onClick="data_write('D030')" class="btn_l btn_co01">변경저장</a>
					</c:if>
					<c:if test="${gugDataConfig.work_state_cd =='D010' || gugDataConfig.work_state_cd =='D030' }">
					
							<a href="#" onClick="data_write('D020')"class="btn_l btn_co01">입력완료</a>
					</c:if>
					<a href="#" class="btn_l btn_co01">국데이터 복사</a>
					<a href="#" onClick="data_delete()" class="btn_l btn_co02">삭제</a>
					</c:if>
				</div><!-- //btn_area -->

				<h3 class="title"><span>이력조회</span></h3>
				<div class="table_ty">
					<table>
						<caption>목록 테이블</caption>
						<colgroup>
							<col span="1" style="width:15%;">
							<col span="1" style="width:*;">
							<col span="5" style="width:12%;">
						</colgroup>
						<thead>
							<tr>
								<th scope="col">국데이터 상태</th>
								<th scope="col">변경일자</th>
								<th scope="col">변경자</th>
								<th scope="col">항목</th>
								<th scope="col">변경전</th>
								<th scope="col">변경후</th>
								<th scope="col">변경사유</th>
							</tr>
						</thead>
						<tbody id="trData" style="border:0px">
				        <c:forEach var="board" items="${his_list}">
							<tr>
							    <td>${board.work_state_nm}</td>
								<td><fmt:formatDate value="${board.modify_dtm}" pattern="yyyy.MM.dd hh:mm" /></td>
								<td>${board.modify_id}</td>
								<td>IP Pool</td>
								<td>10.3.71.0/24</td>
								<td>10.3.72.0/24</td>
								<td><span title="${board.change_reason}">${board.change_reason}</span></td>
							</tr>
						 </c:forEach>
				         </tbody>	

					</table>
				</div><!-- //table_ty -->
				<div class="paginate">
					<a href="#" class="first">처음 페이지</a>
					<a href="#" class="prev">이전 페이지</a>
					<ul>
						<li class="select"><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#">6</a></li>
						<li><a href="#">7</a></li>
						<li><a href="#">8</a></li>
						<li><a href="#">9</a></li>
						<li><a href="#">10</a></li>
					</ul>
					<a href="#" class="next">다음 페이지</a>
					<a href="#" class="last">마지막 페이지</a>
				</div><!-- //paginate -->
				</form:form>
			</div><!-- //contents -->
		</div><!-- //contents_wrap -->
	</div><!-- //container -->
	
	<footer>
		<nav class="footer_nav">
			<ul>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">이용약관</a></li>
				<li><a href="#">개인정보처리방침</a></li>
				<li><a href="#">Sitemap</a></li>
			</ul>
		</nav><!-- //footer_nav -->
		<address>㈜케이티 대표이사 황창규 경기도 성남시 분당구 불정로 90 (정자동) 사업자등록번호 : 102-81-42945 통신판매업신고 : 2002-경기성남-0047</address>
	</footer>
	
</div><!-- //wrap -->

</body>
</html>