<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="ko">
<head>

<meta charset="utf-8">
<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
<meta http-equiv="Content-Type" content= "text/html; charset=UTF-8">
<meta name="format-detection" content="telephone=no"> 
<title>KT E2E Infra Orchestrator</title>
<link rel="stylesheet" type="text/css" href="/new_resources/resources/css/style.css">
<link rel="shortcut icon" href="/new_resources/resources/images/layout/favicon.ico" type="image/x-icon">

<script type="text/javascript" src="/new_resources/resources/js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="/new_resources/resources/js/template.js"></script>

<script>	
/*$(function(){
	
	$(function(){pop_open('#pop03')});

});
*/



function data_write()
{
	var f=document.form;
	
	f.cust_nm.value=$("input[name='cust_nm_reg']").val();
	f.apn.value=$("input[name='apn_reg']").val();
	f.lte_id.value=$("input[name='lte_id_reg']").val();
	f.cust_description.value=$("textarea[name='cust_description_reg']").text();
	f.method="POST";
	f.action="/cust/post";
	
	f.submit();
	pop_close('#pop03_0');
}


function data_update()
{
	var f=document.form;
	
	f.cust_manage_no.value=$("input[name='cust_manage_no_mod']").val();
	f.cust_nm.value=$("input[name='cust_nm_mod']").val();
	f.apn.value=$("input[name='apn_mod']").val();
	f.lte_id.value=$("input[name='lte_id_mod']").val();
	f.cust_description.value=$("textarea[name='cust_description_mod']").text();
	f.method="POST";
	f._method.value="PUT";
	f.action="/cust/post/"+$("input[name='cust_manage_no_mod']").val();
	
	f.submit();
	pop_close('#pop03');
}

function data_delete()
{
	var f=document.form;
	
	f.cust_manage_no.value=$("input[name='cust_manage_no_mod']").val();
	f.method="POST";
	f._method.value="DELETE";
	f.action="/cust/delete/"+$("input[name='cust_manage_no_mod']").val();

	f.submit();
	pop_close('#pop03');
}

//모두 체크/모두 미체크의 체크박스 처리
function changeCheckBox(flag)
{
	if(flag)
		{
		 $('input:checkbox[name="checkNo"]').each(function() {
		      this.checked = true; //checked 처리
		 });


		}else{
		
   		 $('input:checkbox[name="checkNo"]').each(function() {
		      this.checked = false; //checked 처리
		      if(this.checked){//checked 처리된 항목의 값
		            alert(this.value); 
		      }
		 });
			
		}
}

//체크된 로우 삭제처리
function submitCheckDelete()
{
	var f=document.form;
	f.cust_manage_no.value="0";
	checkBoolean=false;
	
	if (typeof(f.elements['checkNo']) != 'undefined') 
	{
		if (typeof(f.elements['checkNo'].length) == 'undefined')
		{
			if(f.elements['checkNo'].checked==true)
			{
				checkBoolean=true;
			}
		}else{
	        for (i=0; i<f.elements['checkNo'].length; i++)
	        {
	            if (f.elements['checkNo'][i].checked==true)
	            {
	            	alert(f.elements['checkNo'][i].value);
	            	checkBoolean=true;
	            	break;
	            }
	        }
		}
        
        if(checkBoolean)
       	{
           f.action="/cust/delete/select";
           f._method.value="DELETE";
           f.submit();        	
       	}else{
       		alert('선택된 내용이 없습니다.');

       	}

	}
	
}
//action="/board/post/${board.bno}" method="PUT">

// 정렬클릭시
    function listOrder(colName,ordOption)
    {
    	var orderKey="";
    	var commaFlag="";
    	orderKey+="(";
  		 $('input:checkbox[name="checkNo"]').each(function() {
  			orderKey+=commaFlag+this.value
  			commaFlag=",";
		 });
  		orderKey+=")";
    	//alert(orderKey);
        $.ajax({
            type: "get",
           // contentType: "json", //==> 생략가능(RestController이기때문에 가능)
            url: "cust/jsondata?colName="+colName
            		  +"&ordOption="+ordOption+"&orderKey="+orderKey,
            success: function(result){

              // alert(result)
                	 var dataObj = JSON.parse(result);
                     var output = "";
                     for(var i in dataObj){

                    	 var myDate = new Date(dataObj[i].regist_dtm)
                    	 var yyyy= myDate.getFullYear();
                    	 var mm=String(myDate.getMonth() + 1);
                    	 var dd=String(myDate.getDate());
                    	 var h24=String(myDate.getHours());
                    	 var mi=String(myDate.getMinutes());
                    	 mm=(mm.length==1)?"0"+mm:mm;
                    	 dd=(dd.length==1)?"0"+dd:dd;
                    	 h24=(h24.length==1)?"0"+h24:h24;     
                    	 mi=(mi.length==1)?"0"+mi:mi;
                    	// var date = yyyy  + "/" +  mm+ "/" + dd + " " + h24 + ":" + mi;
                    	 var date = yyyy  + "." +  mm+ "." + dd;
                         output += "<tr>";
                         output += "<td><input type=checkbox name='checkNo' value='" + dataObj[i].cust_manage_no +"'></td>";
                         output += "<td><a href=\"#\" onClick=\"popup_control('2','open','" + dataObj[i].cust_manage_no +"');\" title=\"" + dataObj[i].cust_nm+ "\" class=\"pop_btn\">" 
                                      + dataObj[i].cust_nm +"</a></td>";
                         //<td><a href="#pop03" title="${board.cust_nm}" class="pop_btn">${board.cust_nm}</a></td>
                         output += "<td>" + dataObj[i].lte_id +"</td>";
                         output += "<td>" + dataObj[i].apn +"</td>";
                         output += "<td>" + date +"</td>";                
                         output += "<td>" + ((typeof(dataObj[i].regist_id)=="undefined")?"":dataObj[i].regist_id) +"</td>";
                         output += "<tr>";
                      

                     }
                     output += "";

                $("#trData").html(output);
            }
        });
    }
    

function popup_control(gubun,oflag,okey)
{
	
	try {
	
	var f=document.form;
	if(gubun=="1")
	{
		if(oflag=="open")
		{
			pop_open('#pop03_0');
			$("input[name='cust_nm_reg']").focus();

		}else{
			pop_close('#pop03_0');
		}
	}else if(gubun=="2"){
		if(oflag=="open")
		{
			pop_open('#pop03');
			 $.ajax({
		            type: "get",
		           // contentType: "json", //==> 생략가능(RestController이기때문에 가능)
		            url: "cust/jsondata/"+okey,
		            success: function(result){
	                	 var dataObj = JSON.parse(result);
	                	 $("input[name='cust_manage_no_mod']").val(dataObj.cust_manage_no);
	                	 $("input[name='cust_nm_mod']").val(dataObj.cust_nm);
	                	 $("input[name='apn_mod']").val(dataObj.apn);
	                	 $("input[name='lte_id_mod']").val(dataObj.lte_id);
	                	 $("textarea[name='cust_description_mod']").text(dataObj.cust_description);
	                		                     
		            }
		        });
			
		}else{
			pop_close('#pop03');
		}
	}
	}
	catch(err) {
	    alert(err.message);
	}finally {
        
    }
	
}

function list(page){
	var f=document.form;
    location.href="cust?curPage="+page+"&searchOption=${searchOption}"+"&keyword=${keyword}";
}

function searchData(){
	var f=document.form;
	var searchOption=f.searchOption.value;
	var keyword=f.keyword.value;
	//alert(searchOption +"--"+keyword);
    location.href="cust?curPage=1&searchOption=" + searchOption +"&keyword=" +keyword;
}
</script>

</head>

<body>

<div id="wrap">

    <header>
		<h1 class="logo"><a href="#">KT E2E Infra Orchestrator</a></h1>
		<nav id="gnb">
			<ul>
				<li><a href="#">One-View</a></li>
				<li class="active"><!-- 활성화 class="active" -->
					<a href="#">P-LTE</a>
					<ul>
						<li><a href="/new_resources/html/E2E-PLTE-0001.html">국데이터 관리</a></li><!-- 활성화 class="active" -->
						<li><a href="/new_resources/html/E2E-PLTE-0007.html">명령 실행</a></li>
						<li><a href="/new_resources/html/E2E-PLTE-0011.html">명령 개별 실행</a></li>
						<li class="active"><a href="/new_resources/html/E2E-PLTE-0013.html">고객사 관리</a></li>
						<li><a href="/new_resources/html/E2E-PLTE-0015.html">장비 관리</a></li>
					</ul>
				</li>
				<li><a href="#">통합NS관리</a></li>
				<li><a href="#">외부연동관리</a></li>
				<li><a href="#">로그관리</a></li>
			</ul>
		</nav><!-- //gnb -->
		<div class="ad_info">
			<i><img src="/new_resources/resources/images/layout/img_ad_info.jpg" alt=""></i><!-- image size : 25*25px -->
			<p>admin</p>
		</div><!-- //ad_info -->
    </header>
    
    <div id="container">	
		
		<ul id="navigation">
			<li><img src="/new_resources/resources/images/layout/img_navigation_home.png" alt="home"></li>
			<li>고객사 관리</li>
		</ul><!-- //navigation -->
		
		<div id="contents_wrap">
			<div class="contents">
			    <form:form commandName="CustComponent" name="form" action="" onsubmit="return false;">
		    	<input type="hidden" name="_method" value=""><!-- RESTFUL 메소드 오버라이드를 위해  -->
				<input type="hidden" name="cust_manage_no" value="0"><!-- 숫자형으므로 기본 숫자값 줘야함  -->
				<input type="hidden" name="cust_nm">
				<input type="hidden" name="apn">
				<input type="hidden" name="lte_id">
				<textarea name="cust_description" cols="" rows="" style="display:none;"></textarea>
				<div class="search_area">
					<table>
						<caption></caption>
						<colgroup>
							<col span="1" style="width:15%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:150px;">
						</colgroup>
						<tbody>
							<tr> 
								<td>
									<select name="searchOption">
								     	<option value="cust_nm" <c:if test='${searchOption=="cust_nm"}'> selected </c:if> >고객사명</option>
    	                                <option value="lte_id" <c:if test='${searchOption=="lte_id"}'> selected </c:if> >LTE ID</option>
									</select>
								</td>
								<td><input name="keyword" type="text" value="${keyword}"></td>
								<td><a href="#" onClick="searchData()" class="btn_l btn_co01 on">조회하기</a></td>
							</tr>
						</tbody>	
					</table>
				</div><!-- //search_area -->
				
				<div class="table_ty">
					<div class="count">Total Count <span>${count}</span></div>
					<div class="btn_area">
						<a href="#" onClick="popup_control('1','open','0');" class="btn_m btn_co01 pop_btn">고객사등록</a>
						<a href="#" onClick="submitCheckDelete()" class="btn_m btn_co02">선택삭제</a>
					</div>
					<table>
						<caption></caption>
						<colgroup>
							<col span="1" style="width:5%;">
							<col span="1" style="width:*;">
							<col span="1" style="width:15%;">
							<col span="1" style="width:15%;">
							<col span="1" style="width:15%;">
							<col span="1" style="width:15%;">
						</colgroup>
						<thead>
							<tr>
								<th scope="col"><input type="checkbox" name="checkALL" value="" onchange="changeCheckBox(this.checked)"></th>
								<th scope="col">고객사명 <a href="javascript:listOrder('cust_nm','desc')" title="내림차순" class="down" style="height:10px;"></a><a href="javascript:listOrder('cust_nm','asc')" title="올림차순" class="up" style="height:10px;">up</a></th>
								<th scope="col">LTE ID <a href="javascript:listOrder('lte_id','desc')" title="내림차순" class="down" style="height:10px;"></a><a href="javascript:listOrder('lte_id','asc')" title="올림차순" class="up" style="height:10px;">up</a></th>
								<th scope="col">APN</th>
								<th scope="col">등록일자</th>
								<th scope="col">등록자</th>
							</tr>
						</thead>
						<tbody id="trData" style="border:0px">
				        <c:forEach var="board" items="${list}">
				        <tr>
				            <td><input type=checkbox name="checkNo" value="${board.cust_manage_no}"></td>
				            <td><a href="#" onClick="popup_control('2','open','${board.cust_manage_no}');" title="${board.cust_nm}" class="pop_btn">${board.cust_nm}</a></td>
				            <td>${board.lte_id}</td>
				            <td>${board.apn}</td>
				            <td><fmt:formatDate value="${board.regist_dtm}" pattern="yyyy.MM.dd" /></td>
				            <td>${board.regist_id}</td>
				        </tr>
				
				        </c:forEach>
				         </tbody>
					</table>
				</div><!-- //table_ty -->
				
				<div class="paginate">
				    <!-- **처음페이지로 이동 : 현재 페이지가 1보다 크면  [처음]하이퍼링크를 화면에 출력-->
				    <c:if test="${commonPager.curBlock > 1}">
					<a href="#" onClick="list('1')" class="first">처음 페이지</a>
					</c:if>
				
					
					<!-- **이전페이지 블록으로 이동 : 현재 페이지 블럭이 1보다 크면 [이전]하이퍼링크를 화면에 출력 -->
				   <c:if test="${commonPager.curBlock > 1}">
					<a href="#" onClick="list('${commonPager.prevPage}')" class="prev">이전 페이지</a>
					</c:if>

					
					<ul>
						<!-- **하나의 블럭에서 반복문 수행 시작페이지부터 끝페이지까지 -->
		                <c:forEach var="num" begin="${commonPager.blockBegin}" end="${commonPager.blockEnd}">
		                    <!-- **현재페이지이면 하이퍼링크 제거 -->
		                    <c:choose>
		                        <c:when test="${num == commonPager.curPage}">
		                            <li class="select"><a href="#">${num}</a></li>
		                        </c:when>
		                        <c:otherwise>
		                            <li><a href="#" onClick="list('${num}')">${num}</a></li>
		                        </c:otherwise>
		                    </c:choose>
		                </c:forEach>					

					</ul>
					
					<!-- **다음페이지 블록으로 이동 : 현재 페이지 블럭이 전체 페이지 블럭보다 작거나 같으면 [다음]하이퍼링크를 화면에 출력 -->
	                <c:if test="${commonPager.curBlock < commonPager.totBlock}">
					<a href="#" onClick="list('${commonPager.nextPage}')" class="next">다음 페이지</a>
					</c:if>
					
					<!-- **끝페이지로 이동 : 현재 페이지가 전체 페이지보다 작거나 같으면 [끝]하이퍼링크를 화면에 출력 -->
	                <c:if test="${commonPager.curPage < commonPager.totPage}">
					<a href="#" onClick="list('${commonPager.totPage}')" class="last">마지막 페이지</a>
					</c:if>
				</div><!-- //paginate -->
				
			</div><!-- //contents -->
		</div><!-- //contents_wrap -->
	</div><!-- //container -->
	
	<footer>
		<nav class="footer_nav">
			<ul>
				<li><a href="#">회사소개</a></li>
				<li><a href="#">이용약관</a></li>
				<li><a href="#">개인정보처리방침</a></li>
				<li><a href="#">Sitemap</a></li>
			</ul>
		</nav><!-- //footer_nav -->
		<address>㈜케이티 대표이사 황창규 경기도 성남시 분당구 불정로 90 (정자동) 사업자등록번호 : 102-81-42945 통신판매업신고 : 2002-경기성남-0047</address>
	</footer>
	
</div><!-- //wrap -->

<div class="popup_mask"></div>

<div class="popup" id="pop03">
	<div class="pop_tit">고객사 정보 변경</div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">

			<div class="table_form vTop">
				<table>
					<caption></caption>
					<colgroup>
						<col span="1" style="width:30%;">
						<col span="1" style="width:*;">
					</colgroup>
					<tbody>
					  <input type="hidden" name="cust_manage_no_mod" value="">
						<tr>
							<th><span>고객사명</span></th>
							<td><input name="cust_nm_mod" type="text" value="현대중공업"></td>
						</tr>
						<tr>
							<th><span>APN</span></th>
							<td><input name="apn_mod" type="text" value="PNC_BUSAN_001"></td>
						</tr>
						<tr>
							<th><span>LTE ID</span></th>
							<td><input name="lte_id_mod" type="text" value="h.ktlet.com"></td>
						</tr>
						<tr>
							<th><span>Description</span></th>
							<td><textarea name="cust_description_mod" cols="" rows="">현대중공업 고객사 정보 변경 2017.11.11</textarea></td>
						</tr>
					</tbody>	
				</table>
			</div><!-- //table_form -->
			
		</div><!-- //scrollbar-inner -->
		
		<div class="btn_area">
			<a href="#" onClick="data_update();pop_close('#pop03');" class="btn_m btn_co01">변경</a>
			<a href="javascript:void(0);" onclick="data_delete();" class="btn_m btn_co02">삭제</a>
		</div>
	</div><!-- //pop_con_wrap -->
	<a href="javascript:void(0);" class="pop_close" title="close">close</a>
</div><!-- //popup -->

<div class="popup" id="pop03_0">
	<div class="pop_tit">고객사 신규 등록</div>
	<div class="pop_con_wrap">
		<div class="scrollbar-inner">
			
			<div class="table_form vTop">
				<table>
					<caption></caption>
					<colgroup>
						<col span="1" style="width:30%;">
						<col span="1" style="width:*;">
					</colgroup>
					<tbody>

						<tr>
							<th><span>고객사명</span></th>
							<td>
							<input type="text" name="cust_nm_reg"  id="cust_nm_reg"  value="">
							</td>
						</tr>
						<tr>
							<th><span>APN</span></th>
							<td><input name="apn_reg" type="text" value=""></td>
						</tr>
						<tr>
							<th><span>LTE ID</span></th>
							<td><input name="lte_id_reg" type="text" value=""></td>
						</tr>
						<tr>
							<th><span>Description</span></th>
							<td><textarea name="cust_description_reg" cols="" rows=""></textarea></td>
						</tr>
					</tbody>	
				</table>
			</div><!-- //table_form -->
			
		</div><!-- //scrollbar-inner -->
		
		<div class="btn_area">
			<a href="#" onClick="data_write();" class="btn_m btn_co01">등록</a>
		</div>
	</div><!-- //pop_con_wrap -->
	<a href="javascript:void(0);" class="pop_close" title="close">close</a>

</div><!-- //popup -->
	</form:form>
</body>
</html>
