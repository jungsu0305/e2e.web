<!DOCTYPE html>
<head>
<title>Topology Editor Demo</title>
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport" content="width=device-width, minimum-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="/exam/style.css"></link><!-- sample css -->
	
<script src="/scripts/utils/jquery-1.12.4.js"></script>
<script src="/scripts/utils/jquery-ui-1.8.23.custom.min.js"></script>
<script src="/scripts/utils/jquery.browser.js"></script>
<script src="/scripts/utils/less-1.7.5.min.js"></script>
 
<script src="/scripts/draw2d.v6/shifty.js"></script>
<script src="/scripts/draw2d.v6/raphael.js"></script>
<script src="/scripts/draw2d.v6/jquery.autoresize.js"></script>
<script src="/scripts/draw2d.v6/jquery-touch_punch.js"></script>
<script src="/scripts/draw2d.v6/jquery.contextmenu.js"></script>
<script src="/scripts/draw2d.v6/rgbcolor.js"></script>
<script src="/scripts/draw2d.v6/canvg.js"></script>
<script src="/scripts/draw2d.v6/Class.js"></script>
<script src="/scripts/draw2d.v6/json2.js"></script>
<script src="/scripts/draw2d.v6/pathfinding-browser.min.js"></script>
<script src="/scripts/draw2d.v6/draw2d.js"></script>

<script src="/scripts/Config.js"></script>
<script src="/scripts/Apps.js"></script>
<script src="/scripts/Components.js"></script>

     
<!-- template -->
<script src="/scripts/utils/jsrender.js"></script>
<script id="component-template" type="text/x-jsrender">
	<div id="{{:name}}" class="component" data-id="{{:name}}">
		<img src="{{:path}}">
		<span>{{:name}}</span>
	</div>
</script>

<script type="text/javascript">

// loading app
var app;
var edgeCnt = 0;
var centralCnt = 0;
var isNew = "${isNew}";

$(window).load(function() {
	app = new e2e.Application("canvas");
	var view = app.view;

	//FROM DB
	var containers = ["component-container", "component-container2"];
	$.get("/test/data", function(dataFull){
		var dataObj = JSON.parse(dataFull);
		$.each(dataObj, function(idx, data) {
			$.each(data, function(i, e) { app.components.push(e); });
			$("#"+containers[idx]).html($.templates("#component-template").render(data));
			app.loadingComponent(containers[idx] + " > .component");
		});
		
		
	});
	/*///FROM JsonTypeFile
	var keys = ["/exam/components1.json","/exam/components2.json"];
	var containers = ["component-container", "component-container2"];
	$.each(keys, function(idx, key) {
		$.getJSON(key, function(data) {
			data.forEach(function(el) {
				el.path = conf.path.icon + '/' + el.img;
			});
			
			$.each(data, function(i, e) {
				app.components.push(e);
			});
			
			$("#" + containers[idx]).html($.templates("#component-template").render(data));
			app.loadingComponent(containers[idx] + " > .component");
		});
	});
	/**/
	
	$("#logo span:first").on('click', function() {
		//location.reload();
		var w = new draw2d.io.json.Writer();
		w.marshal(view, function(json) {
			console.log(json);
		});
	});
	
	$("#logo span:last").on('click', function() {
		var convertor = app.convertor;
		var store = {};
		var writer = new draw2d.io.json.Writer();
		writer.marshal(view, function(json) {
			console.log(json);
			//converting
			var jv = convertor.json(json);
			console.log(jv);
			
			store = JSON.stringify(jv);
			
			//post& callback
			$.post('/test/save/data',{data:store}, function(data) {
				console.log('saved:',data);
			});
			
		});
	});
	
	//add cloud
	$("#logo > span:nth-child(2)").on("click", function() {
		view.add(new e2e.component.Cloud.Edge({offset:edgeCnt++}));		
	});
	$("#logo > span:nth-child(3)").on("click", function() {
		view.add(new e2e.component.Cloud.Central({offset:centralCnt++}));
	});
	
	//for ZOOM
	/*/
	$("#logo > span:nth-child(4)").on("click", function() {
		view.setZoom(view.getZoom()*0.7, true);
	});
	$("#logo > span:nth-child(5)").on("click", function() {
		view.setZoom(1.0,true);
	});
	$("#logo > span:nth-child(6)").on("click", function() {
		view.setZoom(view.getZoom()*1.3,true);
	});
	/**/
});
</script>

</head>
<body>
	<div id="logo">
		<span>Editor</span>
		<span>Edge</span>
		<span>Central</span>
		<!-- 
		<span>[-]</span>
		<span>=</span>
		<span>[+]</span>
		 -->
		<span id="save-id" name="save-name" class="save save-class">Save</span>
	</div>
	
	
	<div id="component-container"></div>
	<div id="component-container2"></div>
	
	<div id="canvas"></div>
</body>
</html>