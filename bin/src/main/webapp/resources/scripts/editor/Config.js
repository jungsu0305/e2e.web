
// declare the namespace for this example
var conf = {
	path: {
		data:{},
		img:'/img',
		icon:'/exam',
		saveJson:'/temp/saved_sample.json'
	},
	icon: {
		width: {'default':70,min:70,max:70,bg:75},
		height:{'default':70,min:70,max:70,bg:75}
	},
	canvas: {
		width:802,
		height:802
	},
	contents: {
		storage:'container',
		editable:'editableContents',
		statical:'staticContents',
		oapip:[],
		ippool:[]
	},
	componentTypes : ['vnf','pnf','conn','cloud'],
	dialogCloseStates: {
		CLOSE:'close',
		UPDATE:'update'
	}
	
};
