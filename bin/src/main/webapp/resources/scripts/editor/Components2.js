var ariel = {
		fiveG:{
			icon:{
				figure:{
					max_width:conf.icon.width.max,
					max_height:conf.icon.height.max,
					defaultWidth:conf.icon.width.default,
					defaultHeight:conf.icon.height.default,
					bgWidth:conf.icon.width.bg,
					bgHeight:conf.icon.height.bg,
				},
				name:{},
				path:{},
				
			},
			cloud:{}
		}
};

var RIGHT_LOCATOR = new draw2d.layout.locator.RightLocator();
var LEFT_LOCATOR = new draw2d.layout.locator.LeftLocator();
var TOP_LOCATOR = new draw2d.layout.locator.TopLocator();
var BOTTOM_LOCATOR = new draw2d.layout.locator.BottomLocator();
var CENTER_LOCATOR = new draw2d.layout.locator.CenterLocator();

if(typeof e2e === 'undefined') e2e = {};

/************************************************************************
 *  Canvas Policy
 **/
e2e.policy = {};
e2e.policy.canvas = {};
e2e.policy.canvas.ReadOnly = {};
ariel.fiveG.Policy = draw2d.policy.canvas.ReadOnlySelectionPolicy.extend({
	NAME:"ariel.fiveG.Policy",
	WIDTH:200,
	HEIGHT:200,
	
	onDoubleClick: function(figure, mouseX, mouseY, shiftKey, ctrKey) {
		console.log('readonlypolicy clicked');
		console.log(typeof figure.onDoubleClick);
		if(typeof figure.onDoubleClick === 'function') {
			console.log('call?', figure);
			
			figure.onDoubleClick();
		}
	}
});


/*********************************************
 * CONNECTION
 */
e2e.policy.Connection = {};
ariel.fiveG.Connection = draw2d.Connection.extend({
	NAME:'ariel.fiveG.Connection',
	init:function(attr, setter, getter) {
		this._super($.extend({
			color:"#00a8f0",
			stroke:3,
			outlineStroke:1,
			outlineColor:'#303030'
		},attr), setter, getter);
		this.setRouter(new draw2d.layout.connection.DirectRouter());
		/*/
		this.setOutlineStroke(1);
		this.setOutlineColor('#303030');
		this.setStroke(3);
		this.setColor("#00a8f0");
		/**/
	},
	
	onClick:function() {
		alert('line clicked');
	}
});


/***********************************************************
 * ICON(IMAGE)
 */
e2e.component = {};
e2e.component.Icon = {};
ariel.fiveG.icon.Base = draw2d.shape.basic.Image.extend({
	NAME:'ariel.fiveG.icon.Base',
	init:function(attr,setter,getter) {
		this._super(attr, setter, getter);
		if(attr == null) attr = {};
		this.path  = attr.path;
		this.width = attr.width;
		this.height= attr.height;
	},
	getPersistentAttributes:function() {
		var m = this._super();
		m.path  = this.path;
		m.width = this.width;
		m.height= this.height;
		return m;
	},
	setPersistentAttributes:function(m) {
		this._super(m);
		this.path  = m.path;
		this.width = m.width;
		this.height= m.height;
	},
	onDoubleClick:function() {
		this.parent.onDoubleClick();
	}

});



/*****************************************************************
 * Bottom CenterLocator
 */
e2e.component.Locator = {};
ariel.fiveG.LabelLocator = draw2d.layout.locator.Locator.extend({
	NAME:"ariel.fiveG.LabelLocator",
	init:function() {
		this._super();
	},
	
	relocate:function(index, target) {
		var parent = target.getParent();
		var boundingBox = parent.getBoundingBox();

	   // TODO: instanceof is always a HACK. ugly. Redirect the call to the figure instead of
	   // determine the position with a miracle.
	   //
		if(target instanceof draw2d.Port){
			target.setPosition(boundingBox.w/2,boundingBox.h/2);
		}
		else{
			var targetBoundingBox = target.getBoundingBox();
			//target.setPosition(((boundingBox.w/2-targetBoundingBox.w/2)|0)+0.5,((boundingBox.h/2-(targetBoundingBox.h/2))|0)+0.5);
			target.setPosition(((boundingBox.w-targetBoundingBox.w)|0)+0.5,((boundingBox.h-(targetBoundingBox.h))|0)+0.5);
		}
	}
});



/************************************************************
 * NODE(Component)
 */
e2e.component.Node = {};
ariel.fiveG.icon.Node = draw2d.shape.basic.Circle.extend({
	NAME:'ariel.fiveG.icon.Node',
	WIDTH: ariel.fiveG.icon.figure.bgWidth,
	HEIGHT:ariel.fiveG.icon.figure.bgHeight,
	MAX_WIDTH: ariel.fiveG.icon.figure.max_width,
	MAX_HEIGHT: ariel.fiveG.icon.figure.max_height,
	IWIDTH:ariel.fiveG.icon.figure.defaultWidth,
	IHEIGHT:ariel.fiveG.icon.figure.defaultHeight,
	DEFAULT_COLOR: "#ffffff",
	init:function(attr, setter, getter) {
		this._super(attr, setter, getter);
		
		this.width = this.WIDTH;
		this.height= this.HEIGHT;
		this.setResizeable(false);
		this.setStroke(0);
		
		//set figure
		if(attr == null) attr = {};
		
		this.path  = attr.path;
		this.name  = attr.name;
		
		if(attr.userData == null) attr.userData = {};
		var userData = attr.userData;
		
		if(userData.figure == null) userData.figure = {};
		var figure = userData.figure;
		
		if(figure.bgColor != null && figure.bgColor != "") this.setBackgroundColor(figure.bgColor);
		else this.setBackgroundColor(this.DEFAULT_COLOR);
		
		if(figure.width != null && figure.width <= this.MAX_WIDTH) {
			this.iconWidth = figure.width;
			if(this.iconWidth >= this.MAX_WIDTH) this.iconWidth = this.MAX_WIDTH;
		} else this.iconWidth = this.IWIDTH; 
		
		if(figure.height != null) {
			this.iconHeight = figure.height;
			if(this.iconHeight >= this.MAX_HEIGHT) this.iconHeight = this.MAX_HEIGHT;
		} else this.iconHeight = this.IHEIGHT;
		
		if(this.path != null && this.path != "") {
			var iconAttr = {
				width:this.iconWidth,
				height:this.iconHeight,
				path:this.path
			};
			this.add(new ariel.fiveG.icon.Base(iconAttr),
					CENTER_LOCATOR);
		}
		
		this.createPort("hybrid", RIGHT_LOCATOR);
		this.createPort("hybrid", LEFT_LOCATOR);
		this.createPort("hybrid", TOP_LOCATOR);
		this.createPort("hybrid", BOTTOM_LOCATOR);
		
		//TODO this.installEditPolicy(new draw2d.policy.figure.GlowSelectionFeedbackPolicy());
		//set.push(this.canvas.paper.image(this.path, 0,0, this.width, this.height));
	},
	
	setPersistentAttributes:function(m) {
		this._super(m);
		
		if(m.id != null) this.id = m.id;
		if(m.x != null) this.x = m.x;
		if(m.y != null) this.y = m.y;
		if(m.path != null) this.path = m.path;
		if(m.name != null) this.name = m.name;
		if(m.bgColor != null) this.setBackgroundColor(m.bgColor);
		if(m.iconWidth != null) this.iconWidth = m.iconWidth;
		if(m.iconHeight != null) this.iconHeight = m.iconHeight;
		
		if(this.path != null && this.path != "") {
			var iconAttr = {
				width:this.iconWidth,
				height:this.iconHeight,
				path:this.path
			};
			this.add(new ariel.fiveG.icon.Base(iconAttr),
					CENTER_LOCATOR);
		}
		
		this.userData = $.extend(m.userData, m);
	},
	
	getPersistentAttributes:function() {
		var m = this._super();
		var attr = {
				id	: this.id,
				path: this.path,
				name: this.name,
				iconWidth: this.iconWidth,
				iconHeight:this.iconHeight,
				bgColor	: this.getBackgroundColor().hashString,
				x		: this.x,
				y		: this.y,
				userData: this.userData
			};
		console.log('bgColor:',attr.bgColor);
		console.log('bgColor2:',this.getBackgroundColor().hashString);
		console.log('bgColor3:',this.getBackgroundColor());
		console.log('getPersistentAttr2:',attr);
		return $.extend(m, attr);	
	},
	
	// DOUBLE CLICK!!!
	onDoubleClick: function() {
		alert(this.name + ' double2 clicked');
	},
	
});


/*************************************************************
 * EDGE CLOUD
 */
e2e.component.Cloud = {};
e2e.component.Cloud.Edge = {};
ariel.fiveG.cloud.Edge = draw2d.shape.composite.Raft.extend({
	NAME:'ariel.fiveG.cloud.Edge',
	WIDTH:200,
	HEIGHT:200,
	POS_X:50,
	POS_Y:50,
	OFFSET:20,
	LABEL:'Edge Cloud',
	init:function(attr, setter, getter) {
		this._super($.extend({alpha:0.4}, attr), setter, getter);
		var offset = 0;
		
		if(attr == null) attr = {};
		if(attr.offset != null) offset = attr.offset; 
		
		this.x = attr.x != null ? attr.x:(this.POS_X + (offset*this.OFFSET));
		this.y = attr.y != null ? attr.y:(this.POS_Y + (offset*this.OFFSET));
		this.width = this.WIDTH;
		this.height= this.HEIGHT;
		
		this.name = this.LABEL;
		if(offset > 0) {
			this.name = this.LABEL+offset;
		}
		
		this.addLabel();
		console.log("init edge:", this);
		
	},
	
	addLabel:function() {
		this.label = null;
		this.label = new draw2d.shape.basic.Label({
			text:this.name,
			stroke:0,
			fontColor:"#5856d6",			
		});
		this.add(this.label, new ariel.fiveG.LabelLocator());
		//this.label.installEditor(new draw2d.ui.LabelInplaceEditor());
	},
	
	onDoubleClick:function() {
		alert('edge cloud : '+ this.name + ' clicked');
	},
	
	setPersistentAttributes:function(m) {
		this._super(m);
		
		if(m.x != null) this.x = m.x;
		if(m.y != null) this.y = m.y;
		if(m.width != null) this.width = m.width;
		if(m.height!= null) this.height= m.height;
		if(m.name  != null) this.name  = m.name;
		this.label.text = this.name;
	},
	
	getPersistentAttributes:function() {
		var m = this._super();
		m.x = this.x;
		m.y = this.y;
		m.width = this.width;
		m.height= this.height;
		m.name  = this.name;
		return m;
	}
});



/*****************************************************************
 * CENTRAL CLOUD
 */
e2e.component.Cloud.Cetral = {};
ariel.fiveG.cloud.Central = draw2d.shape.composite.Raft.extend({
	NAME:'ariel.fiveG.cloud.Central',
	WIDTH:200,
	HEIGHT:200,
	POS_X:350,
	POS_Y:50,
	OFFSET:20,
	LABEL:'Central Cloud',
	init:function(attr, setter, getter) {
		this._super($.extend({alpha:0.4}, attr), setter, getter);
		var offset = 0;
		
		if(attr == null) attr = {};
		if(attr.offset != null) offset = attr.offset; 
		
		this.x = attr.x != null ? attr.x:(this.POS_X + (offset*this.OFFSET));
		this.y = attr.y != null ? attr.y:(this.POS_Y + (offset*this.OFFSET));
		this.width = this.WIDTH;
		this.height= this.HEIGHT;
		
		this.name = this.LABEL;
		if(offset > 0) {
			this.name = this.LABEL+offset;
		}
		
		this.addLabel();
		console.log("init central:", this);
		
	},
	
	addLabel:function() {
		this.label = null;
		this.label = new draw2d.shape.basic.Label({
			text:this.name,
			stroke:0,
			fontColor:"#5856d6",			
		});
		this.add(this.label, new ariel.fiveG.LabelLocator());
		//this.label.installEditor(new draw2d.ui.LabelInplaceEditor());
	},
	
	onDoubleClick:function() {
		alert('central cloud : '+ this.name + ' clicked');
	},
	
	setPersistentAttributes:function(m) {
		this._super(m);
		
		if(m.x != null) this.x = m.x;
		if(m.y != null) this.y = m.y;
		if(m.width != null) this.width = m.width;
		if(m.height!= null) this.height= m.height;
		if(m.name  != null) this.name  = m.name;
		this.label.text = this.name;
	},
	
	getPersistentAttributes:function() {
		var m = this._super();
		m.x = this.x;
		m.y = this.y;
		m.width = this.width;
		m.height= this.height;
		m.name  = this.name;
		return m;
	}
});





