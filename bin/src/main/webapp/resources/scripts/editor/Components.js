
var e2e = {
	component:{},
	policy:{
		Canvas:{}
	}
};


/************************************************************************
 *  Canvas Policy
 **/
e2e.policy.Canvas.ReadOnly = draw2d.policy.canvas.ReadOnlySelectionPolicy.extend({
	NAME:"e2e.policy.canvas.ReadOnly",
	
	init:function(){ this._super(); },
	
	onDoubleClick: function(figure, mouseX, mouseY, shiftKey, ctrKey) {
		if(figure === null) {
			console.log('figure is null...');
			return;
		}
		
		console.log("figure's type = " + typeof figure);
		if(typeof figure.onDoubleClick === 'function') {
			console.log('ReadOnlyPolicy]dblClicked: call?', figure);
			
			//figure.onDoubleClick();
		}
	},
	
	onMouseDown: function(figure, mouseX, mouseY, shiftKey, ctrKey) {
		if(figure !== null) {
			if(figure instanceof draw2d.HybridPort) console.log('hibridPort');
			if(figure instanceof draw2d.shape.basic.Rectangle) console.log('rectangle');
			if(figure instanceof draw2d.shape.basic.Circle) console.log('circle');
			console.log('mousedown typeof..:',typeof figure);
			return false;
		}
	}
});



/*********************************************
 * CONNECTION
 */
e2e.policy.Connection = draw2d.Connection.extend({
	NAME:'e2e.policy.Connection',
	init:function(attr, setter, getter) {
		this._super($.extend({
			/*color:"#00a8f0",*/
			color:"#666666",
			stroke:3,
			outlineStroke:1,
			outlineColor:'#303030'
		},attr), setter, getter);
		this.setRouter(new draw2d.layout.connection.DirectRouter());
	},
	
	onClick:function() {
		alert('line clicked');
	}
});

/*********************************************
 * PORT
 */
e2e.policy.Port = draw2d.HybridPort.extend({
	NAME:"e2e.policy.Port",
	init:function(attr, setter, getter) {
		this._super(attr, setter, getter);
		this.isReadOnly = false;
		if(attr != null && attr.isReadOnly) this.isReadOnly = true;
		
		var alpha = 0.5;
		if(this.isReadOnly) {
			alpha = 0;
			this.cursor = 'default';
		}
		this.setAlpha(alpha);
		this.setStroke(1);
		this.setColor(new draw2d.util.Color(31,30,52));
		this.setBackgroundColor("#ffffff");
		this.useGradient = false;
	},

});

/***********************************************************
 * ICON(IMAGE)
 */
e2e.component.Icon = draw2d.shape.basic.Image.extend({
	NAME:'e2e.component.Icon',
	init:function(attr,setter,getter) {
		this._super(attr, setter, getter);
		if(attr == null) attr = {};
		this.path  = attr.path;
		this.width = attr.width;
		this.height= attr.height;
	},
	getPersistentAttributes:function() {
		var m = this._super();
		m.path  = this.path;
		m.width = this.width;
		m.height= this.height;
		return m;
	},
	setPersistentAttributes:function(m) {
		this._super(m);
		this.path  = m.path;
		this.width = m.width;
		this.height= m.height;
	},
	onDoubleClick:function() {
		this.parent.onDoubleClick();
	}

});



/*****************************************************************
 * Top CenterLocator
 */
e2e.component.LocatorType = {
	CLOUD:"Cloud",
	TOP_LEFT:"TopLeft",
	TOP_RIGHT:"TopRight",
	BOTTOM_LEFT:"BottomLeft",
	BOTTOM_RIGHT:"BottomRight"
};
e2e.component.Locator = draw2d.layout.locator.Locator.extend({
	NAME:"e2e.component.Locator",
	TYPE:["Cloud","TopLeft","TopRight","BottomLeft","BottomRight"],
	init:function(type) {
		this._super();
		this.type = 0; // default 'Cloud'
		if(type != null) {
			this.type = this.TYPE.indexOf(type); 
		}
	},
	
	relocate:function(index, target) {
		var parent = target.getParent();
		var boundingBox = parent.getBoundingBox();

	   // TODO: instanceof is always a HACK. ugly. Redirect the call to the figure instead of
	   // determine the position with a miracle.
	   //
		if(target instanceof draw2d.Port){
			
			var horizontalPosition = boundingBox.w/2;
			var verticalPosition   = boundingBox.h/2;
			if(this.type > 0) {
				
				if(parent instanceof draw2d.shape.basic.Circle) {
					//Circle Type
					var r = boundingBox.w/2; 
					var offset = this.getOffset(r);
					var l1 = r-offset;
					var l2 = r+ parseFloat(offset);
					horizontalPosition = [1,l1, l2, l1, l2][this.type];
					verticalPosition   = [1,l1, l1, l2, l2][this.type];
				} else {
					
					//Rectangle Type that is default type
					horizontalPosition = [1,0, boundingBox.w, 0, boundingBox.w][this.type];
					verticalPosition   = [1,0,0,boundingBox.h, boundingBox.h][this.type];
				}
			}
			target.setPosition(horizontalPosition,verticalPosition);
		}
		else{
			var targetBoundingBox = target.getBoundingBox();
			//target.setPosition(((boundingBox.w/2-targetBoundingBox.w/2)|0)+0.5,((boundingBox.h/2-(targetBoundingBox.h/2))|0)+0.5);
			//target.setPosition(((boundingBox.w-targetBoundingBox.w)|0)+0.5,((boundingBox.h-(targetBoundingBox.h))|0)+0.5);
			target.setPosition(1,1);
		}
	},
	getOffset:function(r) {
		//var pie = 1.41421;
		var pie = 1.414
		return parseFloat(r/pie).toFixed(3);
	}
});


var RIGHT_LOCATOR = new draw2d.layout.locator.RightLocator();
var LEFT_LOCATOR = new draw2d.layout.locator.LeftLocator();
var TOP_LOCATOR = new draw2d.layout.locator.TopLocator();
var BOTTOM_LOCATOR = new draw2d.layout.locator.BottomLocator();
var CENTER_LOCATOR = new draw2d.layout.locator.CenterLocator();
var TL_LOCATOR = new e2e.component.Locator(e2e.component.LocatorType.TOP_LEFT);
var TR_LOCATOR = new e2e.component.Locator(e2e.component.LocatorType.TOP_RIGHT);
var BL_LOCATOR = new e2e.component.Locator(e2e.component.LocatorType.BOTTOM_LEFT);
var BR_LOCATOR = new e2e.component.Locator(e2e.component.LocatorType.BOTTOM_RIGHT);

/************************************************************
 * NODE(Component)
 */
e2e.component.Node = draw2d.shape.basic.Rectangle.extend({
	NAME:'e2e.component.Node',
	WIDTH: conf.icon.width.bg,
	HEIGHT:conf.icon.height.bg,
	MAX_WIDTH: conf.icon.width.max,
	MAX_HEIGHT: conf.icon.height.max,
	IWIDTH:conf.icon.width.default,
	IHEIGHT:conf.icon.height.default,
	DEFAULT_COLOR: "#ffffff",
	init:function(attr, setter, getter) {
		this._super($.extend({radius:10},attr), setter, getter);
		
		this.width = this.WIDTH;
		this.height= this.HEIGHT;
		this.setResizeable(false);
		this.setStroke(0);
		this.setAlpha(0.2);
		
		//set figure
		if(attr == null) attr = {};
		
		this.path  = attr.path;
		this.name  = attr.name;
		
		//set label
		this.setNodeName(this.name);
		
		if(attr.userData == null) attr.userData = {};
		var userData = attr.userData;
		
		if(userData.figure == null) userData.figure = {};
		var figure = userData.figure;
		
		if(figure.bgColor != null && figure.bgColor != "") this.setBackgroundColor(figure.bgColor);
		else this.setBackgroundColor(this.DEFAULT_COLOR);
		
		if(figure.width != null && figure.width <= this.MAX_WIDTH) {
			this.iconWidth = figure.width;
			if(this.iconWidth >= this.MAX_WIDTH) this.iconWidth = this.MAX_WIDTH;
		} else this.iconWidth = this.IWIDTH; 
		
		if(figure.height != null) {
			this.iconHeight = figure.height;
			if(this.iconHeight >= this.MAX_HEIGHT) this.iconHeight = this.MAX_HEIGHT;
		} else this.iconHeight = this.IHEIGHT;
		
		if(this.path != null && this.path != "") {
			var iconAttr = {
				width:this.iconWidth,
				height:this.iconHeight,
				path:this.path
			};
			this.add(new e2e.component.Icon(iconAttr),
					CENTER_LOCATOR);
		}
		
		this.createPort("hybrid", RIGHT_LOCATOR);
		this.createPort("hybrid", LEFT_LOCATOR);
		this.createPort("hybrid", TOP_LOCATOR);
		this.createPort("hybrid", BOTTOM_LOCATOR);
		this.createPort("hybrid", TL_LOCATOR);
		this.createPort("hybrid", TR_LOCATOR);
		this.createPort("hybrid", BL_LOCATOR);
		this.createPort("hybrid", BR_LOCATOR);
		
		//TODO this.installEditPolicy(new draw2d.policy.figure.GlowSelectionFeedbackPolicy());
		//set.push(this.canvas.paper.image(this.path, 0,0, this.width, this.height));
	},
	
	LABEL_COLOR:"#0d0d0d",
	LABEL_FONT_COLOR:"#2a3139",
	setNodeName:function(name) {
		if(name != "") {
			this.label = new draw2d.shape.basic.Label({text:name, bold:true, color:this.LABEL_COLOR, fontColor:this.LABEL_FONT_COLOR});
			this.label.setFontSize(18);
			this.label.setStroke(0);
			this.add(this.label, new draw2d.layout.locator.BottomLocator());
			this.label.installEditor(new draw2d.ui.LabelInplaceEditor());
		}
	},
	
	setPersistentAttributes:function(m) {
		this._super(m);
		
		if(m.id != null) this.id = m.id;
		if(m.x != null) this.x = m.x;
		if(m.y != null) this.y = m.y;
		if(m.path != null) this.path = m.path;
		if(m.name != null) this.name = m.name;
		if(m.bgColor != null) this.setBackgroundColor(m.bgColor);
		if(m.iconWidth != null) this.iconWidth = m.iconWidth;
		if(m.iconHeight != null) this.iconHeight = m.iconHeight;
		
		if(this.path != null && this.path != "") {
			var iconAttr = {
				width:this.iconWidth,
				height:this.iconHeight,
				path:this.path
			};
			this.add(new e2e.component.Icon(iconAttr),
					CENTER_LOCATOR);
		}
		
		this.setNodeName(this.name);
		
		this.userData = $.extend(m.userData, m);
	},
	
	getPersistentAttributes:function() {
		var m = this._super();
		var attr = {
				id	: this.id,
				path: this.path,
				name: this.label.text,
				iconWidth: this.iconWidth,
				iconHeight:this.iconHeight,
				bgColor	: this.getBackgroundColor().hashString,
				x		: this.x,
				y		: this.y,
				userData: this.userData
			};
		return $.extend(m, attr);	
	},
	
	// DOUBLE CLICK!!!
	onDoubleClick: function() {
		/*/
		var _this = this;
		var networkService = $("#"+conf.contents.dataKey).data('networkService');
		var componentList  = $("#"+conf.contents.dataKey).data('componentList');
		var basekey  = "." + conf.contents.pclass;
		
		componentList.some(function(e) {
			//TODO, 1st 'name'
			if(e.serverType === _this.name) {
				$(basekey + " .vnfConfig dl dd").html(e.serverType);		//serverType
				$(basekey + " .vnfConfig ul li:first-child input").val(e.name);	//name //TODO temporary
				$(basekey + " .vnfConfig ul li:nth-child(2) input").val(e.vnfpkg + " ver"+e.version); //VNF Package
				$(basekey + " .vnfConfig ul li:nth-child(3) input").val(e.oapip);//OAP IP
				$(basekey + " .vnfConfig ul li:last-child select").val(e.scale); //scale option!
				$(basekey).dialog('open');
				return true;
			}
		});
		/**/
		utils.vnfConfig.load(this);
	},
	
});


/*************************************************************
 * EDGE CLOUD
 */
e2e.component.Cloud = {};
e2e.component.Cloud.Edge = draw2d.shape.composite.Raft.extend({
	NAME:'e2e.component.Cloud.Edge',
	WIDTH:200,
	HEIGHT:200,
	POS_X:50,
	POS_Y:50,
	OFFSET:20,
	LABEL:'Edge Cloud',
	TYPE:"commmm",
	init:function(attr, setter, getter) {
		this._super($.extend({bgColor:"#cddfff",alpha:0.14}, attr), setter, getter);
		var offset = 0;
		
		if(attr == null) attr = {};
		if(attr.offset != null) offset = attr.offset; 
		
		this.x = attr.x != null ? attr.x:(this.POS_X + (offset*this.OFFSET));
		this.y = attr.y != null ? attr.y:(this.POS_Y + (offset*this.OFFSET));
		this.width = this.WIDTH;
		this.height= this.HEIGHT;
		
		this.isBack = false;
		this.name = this.LABEL;
		if(offset > 0) {
			this.name = this.LABEL+offset;
		}
		
		this.addLabel();
	},
	
	addLabel:function() {
		this.label = null;
		this.label = new draw2d.shape.basic.Label({
			text:this.name,
			bold:true,
			color:"#ffffff",
			stroke:0,
			fontColor:"#5856d6",			
		});
		this.label.setFontSize(20);
		this.label.setStroke(0);
		this.add(this.label, new e2e.component.Locator());
		this.label.installEditor(new draw2d.ui.LabelInplaceEditor());
	},
	
	onDoubleClick:function() {
		alert('edge cloud : '+ this.name + ' clicked, zOrder=' + this.getZOrder());
	},
	
	setPersistentAttributes:function(m) {
		this._super(m);
		
		if(m.x != null) this.x = m.x;
		if(m.y != null) this.y = m.y;
		if(m.width != null) this.width = m.width;
		if(m.height!= null) this.height= m.height;
		if(m.name  != null) this.name  = m.name;
		
		this.isBack = false;
		this.label.text = this.name;
	},
	
	getPersistentAttributes:function() {
		var m = this._super();
		m.x = this.x;
		m.y = this.y;
		m.width = this.width;
		m.height= this.height;
		m.name  = this.label.getText();
		return m;
	}
});



/*****************************************************************
 * CENTRAL CLOUD
 */
e2e.component.Cloud.Central = draw2d.shape.composite.Raft.extend({
	NAME:'e2e.component.Cloud.Central',
	WIDTH:200,
	HEIGHT:200,
	POS_X:350,
	POS_Y:50,
	OFFSET:20,
	LABEL:'Central Cloud',
	init:function(attr, setter, getter) {
		this._super($.extend({bgColor:"#cddfff",alpha:0.14}, attr), setter, getter);
		var offset = 0;
		
		if(attr == null) attr = {};
		if(attr.offset != null) offset = attr.offset; 
		
		this.x = attr.x != null ? attr.x:(this.POS_X + (offset*this.OFFSET));
		this.y = attr.y != null ? attr.y:(this.POS_Y + (offset*this.OFFSET));
		this.width = this.WIDTH;
		this.height= this.HEIGHT;
		
		this.isBack = true;		
		this.name = this.LABEL;
		if(offset > 0) {
			this.name = this.LABEL+offset;
		}
		
		this.addLabel();
	},
	
	addLabel:function() {
		this.label = null;
		this.label = new draw2d.shape.basic.Label({
			text:this.name,
			bold:true,
			color:"#ffffff",
			stroke:0,
			fontColor:"#5856d6",
			width:120,
		});
		this.label.setFontSize(20);
		this.label.setStroke(0);
		this.add(this.label, new e2e.component.Locator());
		this.label.installEditor(new draw2d.ui.LabelInplaceEditor());
	},
	
	onDoubleClick:function() {
		alert('central cloud : '+ this.name + ' clicked, zOrder=' + this.getZOrder());
		
	},
	
	setPersistentAttributes:function(m) {
		this._super(m);
		
		if(m.x != null) this.x = m.x;
		if(m.y != null) this.y = m.y;
		if(m.width != null) this.width = m.width;
		if(m.height!= null) this.height= m.height;
		if(m.name  != null) this.name  = m.name;
		
		this.isBack = true;
		this.label.text = this.name;
	},
	
	getPersistentAttributes:function() {
		var m = this._super();
		m.x = this.x;
		m.y = this.y;
		m.width = this.width;
		m.height= this.height;
		m.name  = this.label.getText();
		return m;
	}
});





