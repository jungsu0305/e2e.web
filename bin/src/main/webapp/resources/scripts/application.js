/**
 * 
 */

var utils = {
	//FOR EDITABLE CONTENTS - POPUP Configuration
    setContentsClass:function(editable, statical, storage) {
    	conf.contents.editable = editable;
    	if(typeof statical !== 'undefined') conf.contents.statical = statical;
    	if(typeof storage !== 'undefined') conf.contents.storage= storage;
    },
    
    getEditableContents:function() { return conf.contents.editable; },
    getStaticContents:function() { return conf.contents.statical; },
    getStorage:function() { return conf.contents.storage; },
    
    canvas:{
    	node:{ x:0, y:0 },
    	window:{ w:0, h:0 },
    	set:function(node, window) {
    		this.node.x = node.x;
    		this.node.y = node.y;
    		this.window.w = window.w;
    		this.window.h = window.h
    	},
    },
    
};